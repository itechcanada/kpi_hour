﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Shared_site : System.Web.UI.MasterPage
{
    //Bookmark _book = new Bookmark();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ltScripts.Text = UIHelper.GetScriptTags("jquery,jqueryui,json,jsload,messagebox,jqgrid,framedialog,jqchosen", 4);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    sdsMenu.SelectCommand = _book.GetSql(sdsMenu.SelectParameters, CurrentUser.UserID);
        //    txtName.Text = Page.Title;
        //    txtUrl.Text = Request.Url.ToString();


        //    if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS") || (BusinessUtility.GetString(Session["InvokeSrc"]) == "POS"))
        //    {
        //        Session["InvokeSrc"] = "POS";
        //        dvReturnToPos.Visible = true;

        //        string  sUrl = "../POS/POS.aspx";
        //        if (BusinessUtility.GetString(Request.QueryString["srchKey"]) != "")
        //        {
        //            sUrl += "?srchKey=" + BusinessUtility.GetString(Request.QueryString["srchKey"]);
        //        }
        //        hrfRedirectPOS.HRef = sUrl;
        //    }
        //    else
        //    {
        //        Session["InvokeSrc"] = null;
        //    }
        //}
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {        
        //Session.Abandon();
        //AppConfiguration.ClearCacheOnLogout();
        //Response.Redirect("~/AdminLogin.aspx?lang=logout");
    }

    protected void changeLanguage_Command(object sender, CommandEventArgs e)
    {
        //Globals.SetCultureInfo(e.CommandArgument.ToString());
        //Response.Redirect(Request.RawUrl);
    }

    protected void AddBookmark(object sender, EventArgs e)
    {
        //try
        //{
        //    _book.BookmarkName = txtName.Text;
        //    _book.BookmarkUrl = txtUrl.Text;
        //    _book.UserID = CurrentUser.UserID;
        //    _book.Insert();

        //    Response.Redirect(Request.RawUrl);
        //}
        //catch (Exception ex)
        //{
        //    if (ex.Message == "BOOK_MARK_ALREADY_ESISTS")
        //    {
        //        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgBookmarkAlreadyExists);
        //    }
        //}
    }

    //protected string GetCurrentLanguage()
    //{
    //    //switch (Globals.CurrentAppLanguageCode)
    //    //{
    //    //    case AppLanguageCode.FR:
    //    //        return Resources.Resource.French;
    //    //    case AppLanguageCode.EN:
    //    //    default:
    //    //        return Resources.Resource.English;            
    //    //}
    //}
}
