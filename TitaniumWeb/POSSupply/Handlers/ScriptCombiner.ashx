﻿<%@ WebHandler Language="C#" Class="ScriptCombiner" %>

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using iTECH.InbizERP.BusinessLogic;

public class ScriptCombiner : IHttpHandler
{
    private readonly static TimeSpan CACHE_DURATION = TimeSpan.FromDays(30);
    private HttpContext context;

    public void ProcessRequest(HttpContext context)
    {
        this.context = context;
        HttpRequest request = context.Request;

        // Read setName, version from query string
        string setName = request["s"] ?? string.Empty;
        string version = request["v"] ?? string.Empty;
        string lang = request["lang"] ?? "en";

        // Decide if browser supports compressed response
        bool isCompressed = this.CanGZip(context.Request);

        // If the set has already been cached, write the response directly from
        // cache. Otherwise generate the response and cache it
        if (!this.WriteFromCache(setName, version, isCompressed, lang))
        {
            using (MemoryStream memoryStream = new MemoryStream(8092))
            {
                // Decide regular stream or gzip stream based on whether the response can be compressed or not
                //using (Stream writer = isCompressed ?  (Stream)(new GZipStream(memoryStream, CompressionMode.Compress)) : memoryStream)
                //using (Stream writer = isCompressed ? (Stream)(new ICSharpCode.SharpZipLib.GZip.GZipOutputStream(memoryStream)) : memoryStream)
                using (Stream writer = isCompressed ? (Stream)(new GZipStream(memoryStream, CompressionMode.Compress)) : memoryStream)
                {
                    // Read the files into one big string
                    StringBuilder allScripts = new StringBuilder();
                    foreach (string fileName in GetScriptFileNames(setName, lang))
                        allScripts.Append(File.ReadAllText(context.Server.MapPath(fileName)));

                    // Minify the combined script files and remove comments and white spaces
                    var minifier = new JavaScriptMinifier();
                    string minified = minifier.Minify(allScripts.ToString());

                    // Send minfied string to output stream
                    byte[] bts = Encoding.UTF8.GetBytes(minified);
                    writer.Write(bts, 0, bts.Length);
                }

                // Cache the combined response so that it can be directly written
                // in subsequent calls 
                byte[] responseBytes = memoryStream.ToArray();
                context.Cache.Insert(GetCacheKey(setName, version, isCompressed, lang),
                    responseBytes, null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                    CACHE_DURATION);

                // Generate the response
                this.WriteBytes(responseBytes, isCompressed);
            }
        }
    }
    private bool WriteFromCache(string setName, string version, bool isCompressed, string lang)
    {                
        byte[] responseBytes = context.Cache[GetCacheKey(setName, version, isCompressed, lang)] as byte[];

        if (responseBytes == null || responseBytes.Length == 0)
            return false;

        this.WriteBytes(responseBytes, isCompressed);
        return true;
    }

    private void WriteBytes(byte[] bytes, bool isCompressed)
    {
        HttpResponse response = context.Response;

        response.AppendHeader("Content-Length", bytes.Length.ToString());
        response.ContentType = "application/x-javascript";
        if (isCompressed)
            response.AppendHeader("Content-Encoding", "gzip");
        else
            response.AppendHeader("Content-Encoding", "utf-8");

        context.Response.Cache.SetCacheability(HttpCacheability.Public);
        context.Response.Cache.SetExpires(DateTime.Now.Add(CACHE_DURATION));
        context.Response.Cache.SetMaxAge(CACHE_DURATION);

        response.ContentEncoding = Encoding.Unicode;
        response.OutputStream.Write(bytes, 0, bytes.Length);
        response.Flush();
    }

    private bool CanGZip(HttpRequest request)
    {
        string acceptEncoding = request.Headers["Accept-Encoding"];
        if (!string.IsNullOrEmpty(acceptEncoding) &&
             (acceptEncoding.Contains("gzip") || acceptEncoding.Contains("deflate")))
            return true;
        return false;
    }

    private string GetCacheKey(string setName, string version, bool isCompressed, string lang)
    {
        return "HttpCombiner." + MD5(setName) + "." + version + "." + isCompressed + "." + "en";
    }

    public bool IsReusable
    {
        get { return true; }
    }

    // private helper method that return an array of file names inside the text file stored in App_Data folder
    private static string[] GetScriptFileNames(string setName, string lang)
    {
        var scripts = new System.Collections.Generic.List<string>();
        //string setPath = HttpContext.Current.Server.MapPath(String.Format("~/App_Data/{0}.txt", setName));
        //using (var setDefinition = File.OpenText(setPath))
        //{
        //    string fileName = null;
        //    while (setDefinition.Peek() >= 0)
        //    {
        //        fileName = setDefinition.ReadLine();
        //        if (!String.IsNullOrEmpty(fileName))
        //            scripts.Add(fileName);
        //    }
        //}

        string[] arrScripts = setName.Split(',');
        List<string> lstScripts = new List<string>(arrScripts);
        if (lstScripts.Contains("jquery"))
        {
            scripts.Add("~/lib/scripts/jquery-1.5.1.min.js");
            scripts.Add("~/lib/scripts/jquery-plugins/jquery.cookie.js");
        }
        if (lstScripts.Contains("jqueryui"))
        {
            scripts.Add("~/lib/scripts/jquery-ui-1.8.11.custom.min.js");
            
            //Add localize file
            if (!string.IsNullOrEmpty(lang))
            {
                scripts.Add(string.Format("~/lib/scripts/i18n/jquery.ui.datepicker-en.js", lang.Trim()));
            }                                  
        }
        if (lstScripts.Contains("json"))
        {
            scripts.Add("~/lib/scripts/json2.js");
        }
        if (lstScripts.Contains("jsload"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/jquery-load.js");
        }
        if (lstScripts.Contains("messagebox"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/jquery.messagebox.js");
        }
        if (lstScripts.Contains("jqgrid"))
        {
            //Add localize file
            //if (!string.IsNullOrEmpty(lang))
            //{
            //    scripts.Add(string.Format("~/lib/scripts/jquery.jqGrid-4.0.0/js/i18n/grid.locale-{0}.js", lang.Trim()));
            //}
            //else
            //{
            //    scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/js/i18n/grid.locale-en.js");
            //}
            scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/js/i18n/grid.locale-en.js");
            //scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/js/jquery.jqGrid.min.js");
            scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/js/jquery.jqGrid.src.js");
            scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/plugins/grid.postext.js");
            scripts.Add("~/lib/scripts/jquery.jqGrid-4.0.0/plugins/jquery.jqGrid.fluid.js");
            scripts.Add("~/lib/scripts/jquery-plugins/jqgrid.helper.js");
        }
        if (lstScripts.Contains("framedialog"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/jquery-framedialog-1.1.2.js");
        }
        if (lstScripts.Contains("jqchosen"))
        {
            scripts.Add("~/lib/scripts/chosen.jquery/chosen.jquery.min.js");
        }
        if (lstScripts.Contains("blockui"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/jquery.blockUI.js");
        }
        if (lstScripts.Contains("momentjs"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/moment.min.js");
        }
        if (lstScripts.Contains("placeholder"))
        {
            scripts.Add("~/lib/scripts/jquery-plugins/jquery.addplaceholder.js");
        }

        return scripts.ToArray();

    }

    private string MD5(string str)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] result = md5.ComputeHash(Encoding.ASCII.GetBytes(str));
        str = BitConverter.ToString(result);

        return str.Replace("-", "");
    }   
} 