﻿<%@ WebHandler Language="C#" Class="AsyncMessageHandler" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Text;
using System.IO;
using System.Web.UI;
using iTECH.WebControls;

public class AsyncMessageHandler : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/HTML";
        if (MessageState.HasMessage)
        {
            StringBuilder sbControl = new StringBuilder();
            string messegeContainerClass = MessageState.Current.MessageType == (MessageType.Critical | MessageType.Failure) ? "ui-state-error" : "ui-state-highlight";
            if (MessageState.Current.MessageType == MessageType.Critical || MessageState.Current.MessageType == MessageType.Failure)
            {
                messegeContainerClass = "ui-state-error";
            }
            else if (MessageState.Current.MessageType == MessageType.Success)
            {
                messegeContainerClass = "ui-state-success";
            }
            else if (MessageState.Current.MessageType == MessageType.Information)
            {
                messegeContainerClass = "ui-state-highlight";
            }
            else
            {
                messegeContainerClass = "ui-state-error";
            }
            string messageClass = MessageState.Current.MessageType == (MessageType.Critical | MessageType.Failure) ? "ui-icon-alert" : "ui-icon-info";
            if (MessageState.Current.MessageType == MessageType.Failure)
            {
                messageClass = "ui-icon-alert";
            }
            else if (MessageState.Current.MessageType == MessageType.Critical)
            {
                messageClass = "ui-icon-circle-close";
            }
            else if (MessageState.Current.MessageType == MessageType.Success)
            {
                messageClass = "ui-icon-check";
            }
            else
            {
                messageClass = "ui-icon-alert";
                //messageClass = "ui-icon-info";
            }
            sbControl.Append(@"<div class=""error_theme""><div class=""ui-widget"" style=""margin-top:5px;"">")
                .AppendFormat(@"<div style=""padding: 0 .7em;"" class=""{0} ui-corner-all"">", messegeContainerClass)
                //.AppendFormat(@"<table><tr><td vAlign=""top"" style=""width:20px""><span style=""margin-right: .3em;"" class=""ui-icon {0}""></span></td>", messageClass)
                .AppendFormat(@"<p><span style=""float: left; margin-right: .3em;"" class=""ui-icon {0}""></span>", messageClass)
                //.AppendFormat(@"<td vAlign=""top"" style=""font-size:12px;""><strong>{0}</strong> {1}</td>", string.Empty, MessageState.Current.Message)
                .AppendFormat(@"<strong>{0}</strong> {1}", string.Empty, MessageState.Current.Message)
                //.Append(@"</table></div></div></div>");
                .Append(@"</p></div></div></div>");
            LiteralControl lt = new LiteralControl(sbControl.ToString());
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                {
                    lt.RenderControl(textWriter);
                }
            }
            MessageState.Clear();
            context.Response.Write(sb);
        }
        else
        {
            context.Response.Write(404);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}