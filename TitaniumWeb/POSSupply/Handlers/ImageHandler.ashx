﻿<%@ WebHandler Language="C#" Class="ImagesGalleriesPage" %>

using System;
using System.Configuration;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Security.Cryptography;
using System.Text;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public class ImagesGalleriesPage : IHttpHandler
{
    private readonly static TimeSpan CACHE_DURATION = TimeSpan.FromDays(30);
    HttpContext _current;
    
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            _current = context;            

            if (!this.WriteFromCache(this.ImagePath, this.Width, this.Height)) 
            {
                if (FileManager.IsValidImageFile(this.ImagePath))
                {
                    if (File.Exists(_current.Server.MapPath(this.ImagePath)))
                    {                        
                        using (FileStream fs = File.Open(_current.Server.MapPath(this.ImagePath), FileMode.Open, FileAccess.Read, FileShare.None))
                        {
                            using (System.Drawing.Bitmap originalBMP = new System.Drawing.Bitmap(fs))
                            {
                                // Calculate the new image dimensions                               
                                int width = originalBMP.Width; //actual width
                                int height = originalBMP.Height; //actual height                                
                                int widthDiff = (width - this.Width); //how far off maxWidth?
                                int heightDiff = (height - this.Height); //how far off maxHeight?

                                //figure out which dimension is further outside the max size
                                bool doWidthResize = (this.Width > 0 && width > this.Width && widthDiff > -1 && (widthDiff > heightDiff || this.Height.Equals(0)));
                                bool doHeightResize = (this.Height > 0 && height > this.Height && heightDiff > -1 && (heightDiff > widthDiff || this.Width.Equals(0)));                                                                

                                //only resize if the image is bigger than the max or where image is square, and the diffs are the same
                                if (doWidthResize || doHeightResize || (width.Equals(height) && widthDiff.Equals(heightDiff) && (!width.Equals(widthDiff) || !height.Equals(heightDiff))))
                                {
                                    int iStart;
                                    Decimal divider;
                                    if (doWidthResize)
                                    {
                                        iStart = width;
                                        divider = Math.Abs((Decimal)iStart / (Decimal)this.Width);
                                        width = this.Width;
                                        height = (int)Math.Round((Decimal)(height / divider));
                                    }
                                    else 
                                    {
                                        iStart = height;
                                        divider = Math.Abs((Decimal)iStart / (Decimal)this.Height);
                                        height = this.Height;
                                        width = (int)Math.Round((Decimal)(width / divider));
                                    }
                                }

                                using (System.Drawing.Bitmap newBMP = new System.Drawing.Bitmap(originalBMP, width, height))
                                {
                                    using (System.Drawing.Graphics oGraphics = System.Drawing.Graphics.FromImage(newBMP))
                                    {
                                        oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                                        oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                        oGraphics.DrawImage(originalBMP, 0, 0, width, height);

                                        using (MemoryStream memoryStream = new MemoryStream())
                                        {
                                            newBMP.Save(memoryStream, ImageFormat.Png);

                                            // Cache the combined response so that it can be directly written
                                            // in subsequent calls 
                                            byte[] responseBytes = memoryStream.ToArray();
                                            context.Cache.Insert(GetCacheKey(this.ImagePath, this.Width, this.Height),
                                                           responseBytes, null, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                                           CACHE_DURATION);

                                            this.WriteBytes(responseBytes);                                                                                       
                                        }
                                    }
                                }
                            }
                        }
                    }
                }    
            }
        }
        catch
        { 
          
        }       
    }

    private bool WriteFromCache(string imgPath, int width, int height)
    {
        byte[] responseBytes = _current.Cache[GetCacheKey(imgPath, width, height)] as byte[];

        if (responseBytes == null || responseBytes.Length == 0)
            return false;

        this.WriteBytes(responseBytes);
        return true;
    }

    private string GetCacheKey(string imgPath, int width, int height)
    {
        return "HttpCombiner." + MD5(imgPath) + "." + width + "." + height;
    }

    private void WriteBytes(byte[] bytes)
    {
        HttpResponse response = _current.Response;

        response.AppendHeader("Content-Length", bytes.Length.ToString());
        response.ContentType = "image/jpeg";

        _current.Response.Cache.SetCacheability(HttpCacheability.Public);
        _current.Response.Cache.SetExpires(DateTime.Now.Add(CACHE_DURATION));
        _current.Response.Cache.SetMaxAge(CACHE_DURATION);

        response.ContentEncoding = Encoding.Unicode;
        response.OutputStream.Write(bytes, 0, bytes.Length);
        response.Flush();
    }

    private string MD5(string str)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] result = md5.ComputeHash(Encoding.ASCII.GetBytes(str));
        str = BitConverter.ToString(result);

        return str.Replace("-", "");
    }   

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public int Width
    {
        get
        {
            int w = 0;
            int.TryParse(_current.Request.QueryString["w"], out w);
            return w;
        }
    }

    public int Height
    {
        get
        {
            int h = 0;
            int.TryParse(_current.Request.QueryString["h"], out h);
            return h;
        }
    }

    public string ImagePath
    {
        get
        {
            return BusinessUtility.GetString(_current.Request.QueryString["path"]);
        }
    }
}