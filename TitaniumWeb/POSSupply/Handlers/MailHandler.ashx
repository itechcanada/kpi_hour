﻿<%@ WebHandler Language="C#" Class="Handler" Debug="true" %>

using System;
using System.Web;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Configuration;

public class Handler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string emailSentTo = ConfigurationManager.AppSettings["NoReplyEmail"];
            string email = context.Request.Form.Get("email");
            string subject = context.Request.Form.Get("subject");
            string messageBody = context.Request.Form.Get("message");
            email = email.Replace(string.Format("{0}", "\n"), " ");
            subject = subject.Replace(string.Format("{0}", "\n"), " ");
            messageBody = messageBody.Replace(string.Format("{0}", "\n"), " ");
            iTECH.Library.Utilities.EmailHelper.SendEmail(emailSentTo, email, messageBody, subject, true);


        }
        catch (SmtpException e)
        {
            using (System.IO.StreamWriter sw = System.IO.File.AppendText(HttpContext.Current.Server.MapPath("~") + @"/" + "Log.txt"))
            {
                sw.WriteLine("------------------------");
                sw.WriteLine("----- Mail Sending Failure --------");
                sw.WriteLine("Date time : " + DateTime.Now + "SMTP Err Msg : " + e.Message);
                sw.WriteLine("------------------------");
                sw.Dispose();
            }

        }
    }
    public bool IsReusable { get { return false; } }
}