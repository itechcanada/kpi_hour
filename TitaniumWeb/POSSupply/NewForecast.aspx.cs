﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class NewForcast : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsJqGridCall(jgdvForecast) && !IsPostBack)
        {
            //txtFromDate.Text = "";
            //Session["fdate"] = "";
        }
    }
    protected void jgdvForecast_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 6) //Edit popup setup
        {
            e.CellHtml = BusinessUtility.GetDouble(e.RowValues[6]).ToString().Replace(",",".");
        }
        if (e.ColumnIndex == 8) //Edit popup setup
        {
            e.CellHtml = string.Format(@"<a class=""edit-Taxs""  href=""javascript:void(0);"" onclick=""editPopup('{0}','{1}')"">{2}</a>", e.RowValues[0], e.RowValues[1], "Edit");
        }
    }
    protected void jgdvForecast_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        ForecastResult fc = new ForecastResult();
        //if (!(Session["fdate"].ToString() == ""))
        //{
        //    string fDate = BusinessUtility.GetString(Session["fdate"]);
        //    jgdvForecast.DataSource = fc.GetForecastValue(fDate);
        //}
        //else
        //{
        //    string fdate = DateTime.Now.ToString("yyyy-MM");
        jgdvForecast.DataSource = fc.GetForecastValue();    //fdate
        //}
    }
    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string fDate = BusinessUtility.GetDateTime(txtFromDate.Text, DateFormat.MMddyyyy).ToString("yyyy-MM");
    //        Session["fdate"] = fDate;
    //    }
    //    catch (Exception ex)
    //    {
    //        MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
    //    }
    //}

    protected void btnGoToSSRS_OnClick(object sender, EventArgs e)
    {
        string url = BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["AMReport_SSRS_URL"]);
        Response.Redirect(url);
    }
}