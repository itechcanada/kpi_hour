﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.Library.Web;
using iTECH.InbizERP.BusinessLogic;
using jq = Trirand.Web.UI.WebControls;

public class BasePage : System.Web.UI.Page
{

    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        //if (!CurrentUser.IsAutheticated)
        //{
        //    CurrentUser.RemoveUserFromSession();
        //    //Response.Redirect("~/AdminLogin.aspx");
        //    Response.Redirect("~/logout.aspx");
        //}
    }

    //Initialize Culture
    protected override void InitializeCulture() 
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();

        //if (Session["Language"] == null || string.IsNullOrEmpty(Session["Language"].ToString()))
        //{
        //    Session["Language"] = "en-CA"; 
        //}
        //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["Language"].ToString());
        //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["Language"].ToString());
    }

    public bool IsPagePostBack(params jq.JQGrid[] grids) {
        bool isPostBack = IsPostBack;
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None) {
                isPostBack = true;
                break;
            }
        }
        return isPostBack;
    }

    public bool IsJqGridCall(params jq.JQGrid[] grids) {
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
            {
                return true;
            }
        }
        return false;
    }
}

