﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using Newtonsoft.Json;
using Trirand.Web.UI.WebControls;

/// <summary>
/// Summary description for InventoryHelper
/// </summary>
public class UIHelper
{
    public static void SetJqGridDefaults(JQGrid grid) {
        grid.Height = new System.Web.UI.WebControls.Unit("400px");
        grid.AutoWidth = false;

        grid.PagerSettings.PageSize = 30;
        grid.PagerSettings.PageSizeOptions = "[30,50,100]";

        grid.AppearanceSettings.AlternateRowBackground = true;
        grid.AppearanceSettings.HighlightRowsOnHover = true;

        grid.ToolBarSettings.ShowEditButton = false;
        grid.ToolBarSettings.ShowRefreshButton = true;
        grid.ToolBarSettings.ShowAddButton = false;
        grid.ToolBarSettings.ShowDeleteButton = false;
        grid.ToolBarSettings.ShowSearchButton = false;        
    }

    public static string GetScriptTags(string setName, int version)
    {
        string result = string.Empty;
        result += String.Format(@"<script type=""text/javascript"" src=""{0}?s={1}&v={2}&lang={3}""></script>", VirtualPathUtility.ToAbsolute("~/Handlers/ScriptCombiner.ashx"), setName, version, iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode);
        //#if (DEBUG)
        //            foreach (string fileName in GetScriptFileNames(setName))
        //            {
        //                result += String.Format("\n<script type=\"text/javascript\" src=\"{0}?v={1}\"></script>", VirtualPathUtility.ToAbsolute(fileName), version);
        //            }
        //#else
        //        result += String.Format(@"<script type=""text/javascript"" src=""ScriptCombiner.axd?s={1}&v={2}""></script>", setName, version);
        //#endif
        return result;
    }

    public static string GetRoomType(int idVal)
    {
        switch (idVal)
        {
            case (int)StatusRoomType.Private:
                return Resources.Resource.lblPrivate;
            case (int)StatusRoomType.Shared:
                return Resources.Resource.lblShared;
            case (int)StatusRoomType.Dormitory:
                return Resources.Resource.lblDormitory;
            case (int)StatusRoomType.Cabin:
                return Resources.Resource.lblCabin;
            case (int)StatusRoomType.Tent:
                return Resources.Resource.lblTent;
            case (int)StatusRoomType.SCabin:
                return Resources.Resource.lblSCabin;
            default:
                return string.Empty;
        }
    }

    public static void FormatCurrencyLabel(System.Web.UI.WebControls.Label lbl, double money)
    {
        lbl.Text = string.Format("{0:C}", money);
        if (money > 0)
        {
            lbl.Font.Bold = true;
            lbl.ForeColor = System.Drawing.Color.Green;
            lbl.Font.Size = new System.Web.UI.WebControls.FontUnit("16px");
        }
        else
        {

        }       
    }

    public static void FormatNumberLabel(System.Web.UI.WebControls.Label lbl, string format, int val)
    {
        if (!string.IsNullOrEmpty(format))
        {
            lbl.Text = string.Format(format, val);
        }
        else
        {
            lbl.Text = val.ToString();
        }
        if (val > 0)
        {
            lbl.Font.Bold = true;
            lbl.ForeColor = System.Drawing.Color.Green;
            lbl.Font.Size = new System.Web.UI.WebControls.FontUnit("16px");
            lbl.Font.Names = new string[] { "Tahoma" };
        }
    }

    public static void FormatNumberLabel(System.Web.UI.WebControls.Label lbl, string format, double val)
    {
        if (!string.IsNullOrEmpty(format))
        {
            lbl.Text = string.Format(format, val);
        }
        else
        {
            lbl.Text = val.ToString();
        }
        if (val > 0)
        {
            lbl.Font.Bold = true;
            lbl.ForeColor = System.Drawing.Color.Green;
            lbl.Font.Size = new System.Web.UI.WebControls.FontUnit("16px");
        }
    }
}

public enum InventorySectionKey
{
    Details = 0,
    Description = 1,
    ProductColors = 2,
    ProducImages = 3,
    ProductQuantity = 4,
    ProductKit = 5,
    ProductsAssocaitions = 6,
    ProductSalesPrice = 7,
    AssociateVendors = 8,
    Ammenities = 9,
    ViewProducts = 10,
    ProductDiscount = 11,
    ProductBlackOut = 12
}

public enum ProductCategories
{
    Website = 1,
    Neckline = 2,
    Sleeve = 4,
    Extra = 3,
    Silhouette = 5,
    Trend = 6,
    Keywords = 7,
    WebsiteSubCategory = 8,
    Gauge=9
}

public enum CRMSectionKey
{ 
    CustomerDetails = 0,
    CustomerContacts = 1,    
    CustomerNotes = 3,
    CustomerActivity = 4,
    CustomerSoHistory = 5,
    CustomerLoyaltyProgram = 6
}

public class ReservationCalendar
{
    public int BedID { get; set; }
    public int RoomID { get; set; }
    public int BuildingID { get; set; }
    public string BedTitle { get; set; }
    public string RoomTitle { get; set; }
    public string BuildingTitle { get; set; }

    public ReservationCalendar()
    {
        this.ListDateColumns = new List<DateTime>();
    }

    public List<DateTime> ListDateColumns { get; private set; }
}

public enum UserSectionKey
{
    Details = 0,
    Commission = 1,
    Role = 2,
    SalesGoal = 3,
    AccessIPs = 4,
    View = 5,
    Whs = 6,
}



public class SearchFilter
{
    public void Add(string key, string data) {
        Dictionary<string, string> tmpDic = FilterDictionary;
        tmpDic[key] = data;
        HttpContext.Current.Session["SearchFilterDictionary"] = tmpDic;
    }

    public void Remove(string key) {
        Dictionary<string, string> tmpDic = FilterDictionary;
        tmpDic.Remove(key);
        HttpContext.Current.Session["SearchFilterDictionary"] = tmpDic;
    }

    private static Dictionary<string, string> FilterDictionary
    {
        get {
            if (HttpContext.Current.Session["SearchFilterDictionary"] == null)
            {
                Dictionary<string, string> tmpDic = new Dictionary<string, string>();
                HttpContext.Current.Session["SearchFilterDictionary"] = tmpDic;
            }
            return (Dictionary<string, string>)HttpContext.Current.Session["SearchFilterDictionary"];            
        }
    }

    public string GetData(string key)
    {
        Dictionary<string, string> tmpDic = FilterDictionary;
        if (tmpDic.ContainsKey(key))
        {
            return tmpDic[key];            
        }
        return null;
    }

    public string this[string key]
    {
        get {
            return this.GetData(key);
        }
        set {
            this.Add(key, value);
        }
    }

    public void SetSerealizedData(string serealizedString)
    {
        Dictionary<string, string> tmpDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(serealizedString);
        foreach (string key in tmpDic.Keys)
        {
            this[key] = tmpDic[key];
        }
    }

    public void Clear()
    {
        HttpContext.Current.Session.Remove("SearchFilterDictionary");
    }
}

public class PosProductSearchColumn
{
    public int ProductID { get; set; }
    public string ProductName { get; set; }
    public string UPCCode { get; set; }
    public string FormatedPrice { get { return string.Format("{0:F}", this.ProductPrice); } }
    public double ProductPrice { get; set; }
}

public class PosSearchResponse
{
    public int page { get; set; }
    public int total { get; set; }
    public int records { get; set; }
    public List<PosProductSearchColumn> rows { get; set; }
    public PosSearchResponse() {
        this.rows = new List<PosProductSearchColumn>();
    }
}
