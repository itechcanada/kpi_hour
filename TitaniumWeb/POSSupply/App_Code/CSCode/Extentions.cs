﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Net.Mail;
using System.Text;

using iTECH.Library.Utilities;

public static class Extensions
{
    /// <summary>
    /// Convert the passed datetime into client timezone.
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static DateTime ToClientTime(this DateTime dt)
    {
        var timeOffSet = HttpContext.Current.Session["timezoneoffset"];  // read the value from session

        if (timeOffSet != null)
        {
            var offset = int.Parse(timeOffSet.ToString());
            dt = dt.AddMinutes(-1 * offset);

            return dt;
        }

        // if there is no offset in session return the datetime in server timezone
        return dt.ToLocalTime();
    }

    
}

public class TestClass
{
    public static bool SendAppointment(string from, string to, string body, string sub, string location, DateTime starUtctDate, DateTime endUtcDate)
    {
        try
        {
            SmtpClient sc = new SmtpClient("mail.itechcanada.com");
            sc.Host = "mail.itechcanada.com";
            sc.Credentials = new System.Net.NetworkCredential("erp@itechcanada.com", "erp890");
            sc.Port = 2525;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(from);
            msg.To.Add(new MailAddress(to));
            msg.Subject = sub;
            msg.Body = body;

            StringBuilder str = new StringBuilder();
            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//Appointment");
            str.AppendLine("VERSION:2.0");
            str.AppendLine("METHOD:REQUEST");
            str.AppendLine("BEGIN:VTIMEZONE")
                .AppendLine("TZID:(GMT-06.00) Central Time (US & Canada)")
                .AppendLine("BEGIN:STANDARD")
                .AppendLine("DTSTART:16010101T020000")
                .AppendLine("TZOFFSETFROM:-0500")
                .AppendLine("TZOFFSETTO:-0600")
                .AppendLine("RRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU")
                .AppendLine("END:STANDARD")
                .AppendLine("BEGIN:DAYLIGHT")
                .AppendLine("DTSTART:16010101T020000")
                .AppendLine("TZOFFSETFROM:-0600")
                .AppendLine("TZOFFSETTO:-0500")
                .AppendLine("RRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU")
                .AppendLine("END:DAYLIGHT")
                .AppendLine("END:VTIMEZONE");
            str.AppendLine("BEGIN:VEVENT");

            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", starUtctDate));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endUtcDate));
            str.AppendLine(string.Format("LOCATION: {0}", location));
            str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
            str.AppendLine(string.Format("DESCRIPTION:{0}", BusinessUtility.StripHtml(msg.Body)));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
            str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
            str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));

            str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");
            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
            ct.Parameters.Add("method", "REQUEST");
            AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
            msg.AlternateViews.Add(avCal);
            AlternateView avBody = AlternateView.CreateAlternateViewFromString(msg.Body, new System.Net.Mime.ContentType("text/html"));
            msg.AlternateViews.Add(avBody);


            sc.Send(msg);
            return true;
        }
        catch
        {

            throw;
        }
    }
}
