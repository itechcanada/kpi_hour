﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class PaypalConstants
{
    public static string API_USERNAME = ConfigurationManager.AppSettings["PP_API_USERNAME"];
    public static string API_PASSWORD = ConfigurationManager.AppSettings["PP_API_PASSWORD"];
    public static string API_SIGNATURE = ConfigurationManager.AppSettings["PP_API_SIGNATURE"];
    public static string CERTIFICATE = ConfigurationManager.AppSettings["PP_CERTIFICATE"];
    public static string PRIVATE_KEY_PASSWORD = ConfigurationManager.AppSettings["PP_PRIVATE_KEY_PASSWORD"];
    public static string SUBJECT = ConfigurationManager.AppSettings["PP_MAIL_SUBJECT"];
    public static string ENVIRONMENT = ConfigurationManager.AppSettings["PP_ENVIRONMENT"];

    public const string ECURLLOGIN = "https://developer.paypal.com";
    public const string GET_TRANSACTION_DETAILS_SESSION_KEY = "GetTransactionDetailsResponseType";
    public const string TRANSACTION_SEARCH_SESSION_KEY = "TransactionSearchResponseType";
    public const string RESPONSE_SESSION_KEY = "payPalResponse";
    public const string PROFILE_KEY = "Profile";
    public const string PAYMENTACTIONTYPE_SESSION_KEY = "PaymentActionType";
    public const string TRANSACTIONID_PARAM = "trxID";
    public const string REFUND_TYPE_PARAM = "refundType";
    public const string PAYMENT_AMOUNT_PARAM = "amount";
    public const string PAYMENT_CURRENCY_PARAM = "currency";
    public const string BUYER_LAST_NAME_PARAM = "buyerLastName";
    public const string BUYER_FIRST_NAME_PARAM = "buyerFirstName";
    public const string BUYER_ADDRESS1_PARAM = "buyerAddress1";
    public const string BUYER_ADDRESS2_PARAM = "buyerAddress2";
    public const string BUYER_CITY_PARAM = "buyerCity";
    public const string BUYER_STATE_PARAM = "buyerState";
    public const string BUYER_ZIPCODE_PARAM = "buyerZipCode";
    public const string CREDIT_CARD_TYPE_PARAM = "creditCardType";
    public const string CREDIT_CARD_NUMBER_PARAM = "creditCardNumber";
    public const string CVV2_PARAM = "cvv2";
    public const string EXP_MONTH_PARAM = "expMonth";
    public const string EXP_YEAR_PARAM = "expYear";
    public const string TOKEN_PARAM = "token";
    public const string PAYERID_PARAM = "payerID";
    public const string AUTHORIZATIONID_PARAM = "authorizationID";

    public const string PAYMENT_ACTION_PARAM = "paymentAction";
    public const string REGULAR_RATE = "a3";
    public const string REGULAR_BILLING_CYCLE = "p3";
    public const string REGULAR_BILLING_CYCLE_UNITS = "t3";
    public const string RECURRING_PAYMENTS = "src";
    public const string MODIFICATION_BEHAVIOR = "modify";
    public const string RECURRING_TIMES = "srt";
    public const string REATTEMPT_ON_FAILURE = "sra";
    public const string BUSINESS = "business";
    public const string CURRENCY_CODE = "currency_code";
    public const string ITEM_NAME = "item_name";

    public const string INVOICE = "invoice";
}