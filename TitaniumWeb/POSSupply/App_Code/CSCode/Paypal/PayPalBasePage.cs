﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Threading;
using System.Globalization;
using com.paypal.soap.api;
using System.Web.UI;
using System.Configuration;
using iTECH.Library.Web;
using iTECH.InbizERP.BusinessLogic;
using jq = Trirand.Web.UI.WebControls;

public class PayPalBasePage : Page
{
    //Initialize Culture
    protected override void InitializeCulture()
    {        
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    //check is postback if jqgrid on page
    public bool IsPagePostBack(params jq.JQGrid[] grids)
    {
        bool isPostBack = IsPostBack;
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
            {
                isPostBack = true;
                break;
            }
        }
        return isPostBack;
    }

    public AbstractResponseType PayPalResponse
    {
        get
        {
            return (AbstractResponseType)(HttpContext.Current.Session[PaypalConstants.RESPONSE_SESSION_KEY]);
            
        }
        set
        {
            HttpContext.Current.Session[PaypalConstants.RESPONSE_SESSION_KEY] = value;
        }
    }

    public bool IsTransactionSuccessful
    {
        get
        {
            return IsSuccessfull(this.PayPalResponse.Ack);
        }
    }

    protected string TransactionID
    {
        get
        {
            return HttpContext.Current.Request.QueryString.Get(PaypalConstants.TRANSACTIONID_PARAM);
        }
    }

    public PaymentActionCodeType PaymentAction
    {
        get
        {
            if ((HttpContext.Current.Session[PaypalConstants.PAYMENTACTIONTYPE_SESSION_KEY] == null))
            {
                this.PaymentAction = PaymentActionCodeType.Sale;
            }
            return (PaymentActionCodeType)(HttpContext.Current.Session[PaypalConstants.PAYMENTACTIONTYPE_SESSION_KEY]);
        }
        set
        {
            HttpContext.Current.Session[PaypalConstants.PAYMENTACTIONTYPE_SESSION_KEY] = value;
        }
    }

    public static string AppendTransactionId(string page, string transactionId)
    {
        return page + "?"+ PaypalConstants.TRANSACTIONID_PARAM + "=" + transactionId;
    }

    public static bool IsSuccessfull(AckCodeType ack)
    {
        return ack == AckCodeType.Success || ack == AckCodeType.SuccessWithWarning;
    }
}