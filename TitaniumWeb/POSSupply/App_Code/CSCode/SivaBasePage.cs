﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Threading;
using System.Globalization;
using System.Web.UI;
using System.Configuration;
using iTECH.Library.Web;
using iTECH.InbizERP.BusinessLogic;
using jq = Trirand.Web.UI.WebControls;

/// <summary>
/// Summary description for SivaBasePage
/// </summary>
public class SivaBasePage : Page
{
    //Initialize Culture
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    //check is postback if jqgrid on page
    public bool IsPagePostBack(params jq.JQGrid[] grids)
    {
        bool isPostBack = IsPostBack;
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
            {
                isPostBack = true;
                break;
            }
        }
        return isPostBack;
    }
}