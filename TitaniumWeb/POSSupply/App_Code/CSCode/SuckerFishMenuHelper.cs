﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Resources;
using System.Reflection;

/// <summary>
/// Summary description for SuckerFishMenuHelper
/// </summary>
public class SuckerFishMenuHelper : MenuHelperRoot
{
    Page pg;
    int iSeq = 0;
    private string _titleCssClass = "sf-title";

    public SuckerFishMenuHelper(Page page)
    {
        pg = page;
        //If user is not authenticated then stop building menu
        if (!CurrentUser.IsAutheticated)
        {
            return;
        }
        DbHelper dbHelp = new DbHelper(true);
        try
        {

            var lst = new InbizMenuItem().GetItems(dbHelp, 0, CurrentUser.UserID);
            string url = string.Empty;
            string txt = string.Empty;
            foreach (var item in lst)
            {
                url = item.ItemNavigationUrl.StartsWith("~/") ? pg.ResolveUrl(item.ItemNavigationUrl) : item.ItemNavigationUrl;
                txt = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? item.ItemTextFr : item.ItemTextEn;
                SuckerMenuItem menuItem = new SuckerMenuItem(url, txt, this, item.ItemCssClass, 0, "");
                this.FillMenuItems(dbHelp, item, menuItem);
                this.Items.Add(menuItem);
            }

        }
        catch { throw; }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void FillMenuItems(DbHelper dbHelp, InbizMenuItem itemObj, SuckerMenuItem menuItem)
    {
        try
        {
            var lst = new InbizMenuItem().GetItems(dbHelp, itemObj.ItemID, CurrentUser.UserID);
            string url = string.Empty;
            string txt = string.Empty;
            foreach (var item in lst)
            {
                url = item.ItemNavigationUrl.StartsWith("~/") ? pg.ResolveUrl(item.ItemNavigationUrl) : item.ItemNavigationUrl;
                txt = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? item.ItemTextFr : item.ItemTextEn;
                SuckerMenuItem newMenuItem = new SuckerMenuItem(url, txt, this, item.ItemCssClass, 0, "");
                menuItem.Items.Add(newMenuItem);
                FillMenuItems(dbHelp, item, newMenuItem);
            }
        }
        catch
        {
            throw;
        }
    }
}

public class InbizMenuItem
{
    public int ItemID { get; set; }
    public string ItemGroup { get; set; }
    public string ItemKey { get; set; }
    public int ParentItem { get; set; }
    public string ItemNavigationUrl { get; set; }
    public string ItemCssClass { get; set; }
    public string ItemTextEn { get; set; }
    public string ItemTextFr { get; set; }
    public bool IsActive { get; set; }
    public int ItemSeq { get; set; }
    public string RoleID { get; set; }

    public List<RoleID> InRoles { get; private set; }
    public List<RoleID> NotInRoles { get; private set; }

    public InbizMenuItem()
    {
        this.InRoles = new List<RoleID>();
        this.NotInRoles = new List<RoleID>();
    }

    public List<InbizMenuItem> GetItems(DbHelper dbHelp, int parentID, int userID)
    {
        string sql = "SELECT DISTINCT m.* FROM z_menu m INNER JOIN z_menu_role mr ON mr.MenuItemID=m.ItemID WHERE m.IsActive=1";
        sql += " AND IFNULL(m.ParentItem, 0)=@ParentItem AND mr.RoleID IN (SELECT RoleID FROM z_security_user_roles WHERE UserID=@UserID) ORDER BY Seq";

        //string sql = "SELECT DISTINCT m.* FROM z_menu m WHERE m.IsActive=1 AND IFNULL(m.ParentItem, 0)=@ParentItem ORDER BY Seq";

        string sqlRoles = "SELECT * FROM z_menu_role mr WHERE mr.MenuItemID=@MenuItemID";
        var vResult = new List<InbizMenuItem>();
        using (var dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { DbUtility.GetParameter("ParentItem", parentID, MyDbType.Int), DbUtility.GetParameter("UserID", userID, MyDbType.Int) }))
        {
            while (dr.Read())
            {
                InbizMenuItem i = new InbizMenuItem();
                i.IsActive = true;
                i.ItemCssClass = BusinessUtility.GetString(dr["ItemCssClass"]);
                i.ItemGroup = BusinessUtility.GetString(dr["ItemGroup"]);
                i.ItemID = BusinessUtility.GetInt(dr["ItemID"]);
                i.ItemKey = BusinessUtility.GetString(dr["ItemKey"]);
                i.ItemNavigationUrl = BusinessUtility.GetString(dr["ItemNavigationUrl"]);
                i.ItemTextEn = BusinessUtility.GetString(dr["ItemTextEn"]);
                i.ItemTextFr = BusinessUtility.GetString(dr["ItemTextFr"]);
                i.ParentItem = BusinessUtility.GetInt(dr["ParentItem"]);
                vResult.Add(i);
            }
        }

        foreach (var item in vResult)
        {
            using (var dr = dbHelp.GetDataReader(sqlRoles, System.Data.CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[]{
                DbUtility.GetParameter("MenuItemID", item.ItemID, MyDbType.Int)
            }))
            {
                while (dr.Read())
                {
                    if (BusinessUtility.GetBool(dr["NotInRole"]))
                    {
                        item.NotInRoles.Add((RoleID)BusinessUtility.GetInt(dr["RoleID"]));
                    }
                    else
                    {
                        item.InRoles.Add((RoleID)BusinessUtility.GetInt(dr["RoleID"]));
                    }
                }
            }
        }
        return vResult;
    }
}
