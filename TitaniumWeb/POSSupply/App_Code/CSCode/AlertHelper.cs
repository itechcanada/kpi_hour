﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public class AlertHelper
{
    #region Collection Alerts
    //CollectionAssignedNotification
    public static void SetCollectionAgentAlert(DbHelper dbHelp, DateTime folloupDateTime, int invID, int alertToUserID, int alertByUserId)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            Invoice inv = new Invoice();
            inv.PopulateObject(dbHelp, invID);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, inv.InvCustID);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(alertByUserId, dbHelp);
            string fromAddress = ConfigurationManager.AppSettings["NoReplyEmail"];
            Alert al = new Alert();
            al.AlertPartnerContactID = invID;
            al.AlertComID = alertByUserId;
            al.AlertDateTime = DateTime.Now;
            al.AlertUserID = alertToUserID;

            string strNote = "";
            strNote = Resources.Resource.alrtlblInvAsgnForCollection + "<br><br>";
            //Invoice assigned to you for collection.
            strNote += Resources.Resource.alrtlblInvNo + " " + invID + "<br>";
            //Invoice No.
            strNote += Resources.Resource.alrtlblCustmer + " " + part.PartnerLongName + "<br>";
            //Assigned by:
            strNote += Resources.Resource.alrtlblAsgnBy + " " + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
            al.AlertNote = strNote;
            al.AlertRefType = "COL";
            al.Insert(dbHelp);

            InbizUser alertTo = new InbizUser();
            //alertTo.PopulateObject(alertToUserID, dbHelp);

            //New Alert Implementation
            NewAlert nal = new NewAlert();
            nal.PopulateByAlertName(dbHelp, "CollectionAssignedNotification", Globals.CurrentAppLanguageCode);
            var lstUsersToAlert = nal.GetAlertUserList(dbHelp, "CollectionAssignedNotification");
            string customeMsg = nal.AlertBody.Replace("#INVOICE_NO#", invID.ToString())
                .Replace("CUSTOMER_NAME", part.PartnerLongName)
                .Replace("ASSIGNED_BY", usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName);
            //.Replace("", "")
            //.Replace("", "");
            string emailTo = string.Empty;
            foreach (var item in lstUsersToAlert)
            {
                emailTo = alertTo.GetUserName(dbHelp, item.UserID);
                EmailHelper.SendAppointment(fromAddress, emailTo, strNote, nal.AlertSubject, "", folloupDateTime.ToUniversalTime(), folloupDateTime.AddHours(3).ToUniversalTime());
            }

            //Send calendar emial notification if
            //if (alertTo.ReceiveNotificationViaEmail)
            //{
            //EmailHelper.SendAppointment(fromAddress, alertTo.UserEmail, strNote, string.Format("Collection Alert"), "", folloupDateTime.ToUniversalTime(), folloupDateTime.AddHours(3).ToUniversalTime());
            //}
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }

    //CollectionLogCreated
    public static void SetCollectionLogAlert(DbHelper dbHelp, DateTime folloupDateTime, int invID, int alertToUserID, int alertByUserId)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            Invoice inv = new Invoice();
            inv.PopulateObject(dbHelp, invID);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, inv.InvCustID);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(alertByUserId, dbHelp);
            string fromAddress = ConfigurationManager.AppSettings["NoReplyEmail"];

            Alert al = new Alert();
            al.AlertPartnerContactID = invID;
            al.AlertComID = alertByUserId;
            al.AlertDateTime = folloupDateTime;
            al.AlertUserID = alertToUserID;

            string strNote = "";
            strNote = Resources.Resource.alrtlblColFollowupActivity + "<br><br>";
            //Invoice assigned to you for collection.
            strNote += Resources.Resource.alrtlblInvNo + " " + invID + "<br>";
            //Invoice No.
            strNote += Resources.Resource.alrtlblCustmer + " " + part.PartnerLongName + "<br>";
            //Assigned by:
            strNote += Resources.Resource.alrtlblAsgnBy + " " + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
            al.AlertNote = strNote;
            al.AlertRefType = "COL";
            al.Insert(dbHelp);

            InbizUser alertTo = new InbizUser();
            alertTo.PopulateObject(alertToUserID, dbHelp);

            //New Alert Implementation
            NewAlert nal = new NewAlert();
            nal.PopulateByAlertName(dbHelp, "CollectionLogCreated", Globals.CurrentAppLanguageCode);
            var lstUsersToAlert = nal.GetAlertUserList(dbHelp, "CollectionLogCreated");
            string customeMsg = nal.AlertBody.Replace("#INVOICE_NO#", invID.ToString())
                .Replace("CUSTOMER_NAME", part.PartnerLongName)
                .Replace("ASSIGNED_BY", usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName);
            //.Replace("", "")
            //.Replace("", "");
            string emailTo = string.Empty;
            foreach (var item in lstUsersToAlert)
            {
                emailTo = alertTo.GetUserName(dbHelp, item.UserID);
                EmailHelper.SendAppointment(fromAddress, emailTo, strNote, nal.AlertSubject, "", folloupDateTime.ToUniversalTime(), folloupDateTime.AddHours(3).ToUniversalTime());
            }

            //Send calendar emial notification if
            //if (alertTo.ReceiveNotificationViaEmail)
            //{
            //EmailHelper.SendAppointment(fromAddress, alertTo.UserEmail, strNote, string.Format("Collection Log Alert"), "", folloupDateTime.ToUniversalTime(), folloupDateTime.AddHours(3).ToUniversalTime());
            //}
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }
    #endregion

    #region Sales Order Alerts
    //
    public static void SetAlertQuotationToApprove(DbHelper dbHelp, int orderID, int userID)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);
            List<InbizUser> lstUsr = ProcessUsers.GetUsersByRole(dbHelp, RoleID.ADMINISTRATOR, RoleID.QUOTATION_APPROVER);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(userID, dbHelp);

            string fromAddress = ConfigurationManager.AppSettings["NoReplyEmail"];

            foreach (var item in lstUsr)
            {
                Alert al = new Alert();
                al.AlertPartnerContactID = ord.OrdID;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = item.UserID;
                string note = Resources.Resource.lblQoWaitingForYouApproval + "<br><br>";
                note += Resources.Resource.alrtlblSOID + ord.OrdID + "<br>";
                note += Resources.Resource.alrtlblCustName + part.PartnerLongName + "<br>";
                note += Resources.Resource.alrtlblAsgnBy + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "SO";
                al.Insert(dbHelp);

                //Send calendar emial notification if
                if (item.ReceiveNotificationViaEmail)
                {
                    EmailHelper.SendAppointment(fromAddress, item.UserEmail, note, string.Format("SO Alert"), "", DateTime.Now.ToUniversalTime(), DateTime.Now.AddMinutes(15).ToUniversalTime());
                }
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }

    public static void SetAlertQuotationApproved(int orderID, int userID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);
            List<InbizUser> lstUsr = ProcessUsers.GetUsersByRole(dbHelp, RoleID.ADMINISTRATOR, RoleID.QUOTATION_APPROVER);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(userID, dbHelp);
            foreach (var item in lstUsr)
            {
                Alert al = new Alert();
                al.AlertPartnerContactID = ord.OrdID;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = item.UserID;
                string note = Resources.Resource.lblQoWaitingForYouApproval + "<br><br>";
                note += Resources.Resource.alrtlblSOID + ord.OrdID + "<br>";
                note += Resources.Resource.alrtlblCustName + part.PartnerLongName + "<br>";
                note += Resources.Resource.alrtlblAsgnBy + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "SOA";
                al.Insert(dbHelp);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static void SetAlertForReadyToReceive(DbHelper dbHelp, int orderID, int userID)
    {
        bool mustClose = false;
        if (dbHelp != null)
        {
            dbHelp = new DbHelper(true);
            mustClose = true;
        }
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);
            List<InbizUser> lstUsr = ProcessUsers.GetUsersByRole(dbHelp, RoleID.ADMINISTRATOR, RoleID.SHIPPING_MANAGER);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(userID, dbHelp);

            string fromAddress = ConfigurationManager.AppSettings["NoReplyEmail"];

            foreach (var item in lstUsr)
            {
                Alert al = new Alert();
                al.AlertPartnerContactID = ord.OrdID;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = item.UserID;
                string note = Resources.Resource.alertReadyFoShipping + "<br><br>";
                note += Resources.Resource.alrtlblSOID + ord.OrdID + "<br>";
                note += Resources.Resource.alrtlblCustName + part.PartnerLongName + "<br>";
                note += Resources.Resource.alrtlblAsgnBy + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "SO";
                //al.Insert(dbHelp);

                //Send calendar emial notification if
                if (item.ReceiveNotificationViaEmail)
                {
                    EmailHelper.SendAppointment(fromAddress, item.UserEmail, note, string.Format("Ready For Ship Alert"), "", DateTime.Now.ToUniversalTime(), DateTime.Now.AddHours(3).ToUniversalTime());
                }
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }

    public static void SetTaskAssignedAlert(DbHelper dbHelp, DateTime folloupDateTime, int alerToUsreID, int alertByUsrID, params int[] orderid)
    {
        bool mustClose = false;
        if (dbHelp != null)
        {
            dbHelp = new DbHelper(true);
            mustClose = true;
        }
        try
        {
            Orders ord;
            Partners part;
            InbizUser alertTo = new InbizUser();
            InbizUser alertBy = new InbizUser();
            alertTo.PopulateObject(alerToUsreID, dbHelp);
            alertBy.PopulateObject(alertByUsrID, dbHelp);
            string fromAddress = ConfigurationManager.AppSettings["NoReplyEmail"];
            string note = string.Empty;
            foreach (var oid in orderid)
            {
                Alert al = new Alert();
                ord = new Orders();
                part = new Partners();
                ord.PopulateObject(dbHelp, oid);
                part.PopulateObject(dbHelp, ord.OrdCustID);

                al.AlertPartnerContactID = ord.OrdID; //Partner ID
                al.AlertComID = oid;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = alerToUsreID; //Followup userID
                note = Resources.Resource.alrtlblOrderAssigntoJobPlanningUser + "<br><br>";
                note += Resources.Resource.alrtlblSOID + ord.OrdID + "<br>";
                note += Resources.Resource.alrtlblPartnerNo + " " + ord.OrdCustID + "<br>"; //Partner No.
                note += Resources.Resource.alrtlblCustmer + part.PartnerLongName + "<br>"; //Customer:
                note += Resources.Resource.alrtlblAsgnBy + alertBy.UserSalutation + " " + alertBy.UserFirstName + " " + alertBy.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "JOB";
                al.Insert(dbHelp);

                //Send calendar emial notification if
                //if (alertTo.ReceiveNotificationViaEmail)
                //{
                EmailHelper.SendAppointment(fromAddress, alertTo.UserEmail, note, string.Format("Task Alert"), "", folloupDateTime.ToUniversalTime(), folloupDateTime.AddHours(3).ToUniversalTime());
                //}
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose)
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
    #endregion

    #region Alerts On Purchase Order
    public static void SetAlertPoCreatedAlert(DbHelper dbHelp, int poId, int userID)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            PurchaseOrders po = new PurchaseOrders();
            po.PopulateObject(dbHelp, poId);
            Vendor v = new Vendor();
            v.PopulateObject(dbHelp, po.PoVendorID);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(userID, dbHelp);

            List<InbizUser> lstUsr = ProcessUsers.GetUsersByRole(dbHelp, RoleID.ADMINISTRATOR, RoleID.PO_APPROVER);
            foreach (var item in lstUsr)
            {
                Alert al = new Alert();
                al.AlertPartnerContactID = poId;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = item.UserID;
                string note = Resources.Resource.lblPOWaitingForYouApproval + "<br><br>";
                note += Resources.Resource.alrtlblPOID + poId + "<br>";
                note += Resources.Resource.alrtlblVendorName + v.VendorName + "<br>";
                note += Resources.Resource.alrtlblAsgnBy + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "PO";
                al.Insert(dbHelp);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }

    public static void SetAlertOnReceiving(DbHelper dbHelp, int poItemId, int recId, int userID)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            PurchaseOrderItems poItems = new PurchaseOrderItems();
            poItems.PopulateObject(dbHelp, poItemId);
            int poId = poItems.PoID;

            double remainingQty = poItems.GetRemainingPOItemsQty(dbHelp, poItemId);

            if (remainingQty <= 0) //No need to generate alert
            {
                return;
            }

            Receiving rcv = new Receiving();
            rcv.PopulateObject(dbHelp, recId);

            PurchaseOrders po = new PurchaseOrders();
            po.PopulateObject(dbHelp, poId);
            Vendor v = new Vendor();
            v.PopulateObject(dbHelp, po.PoVendorID);
            InbizUser usr = new InbizUser();
            usr.PopulateObject(userID, dbHelp);

            List<InbizUser> lstUsr = ProcessUsers.GetUsersByRole(dbHelp, RoleID.PROCURMENT_MANAGER);
            foreach (var item in lstUsr)
            {
                Alert al = new Alert();
                al.AlertPartnerContactID = rcv.RecID;
                al.AlertComID = rcv.RecPOID;
                al.AlertDateTime = DateTime.Now;
                al.AlertUserID = item.UserID;
                string note = Resources.Resource.alrtRcvPOQty + "<br><br>";
                note += Resources.Resource.alrtlblPOID + poId + "<br>";
                note += Resources.Resource.alrtRcvPOQty + poItems.PoQty + "<br>";
                note += Resources.Resource.alrtRcvRcvQty + poItems.PoRcvdQty + "<br>";
                note += Resources.Resource.alrtlblAsgnBy + usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName;
                al.AlertNote = note;
                al.AlertRefType = "RCV";
                al.Insert(dbHelp);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }
    #endregion

}