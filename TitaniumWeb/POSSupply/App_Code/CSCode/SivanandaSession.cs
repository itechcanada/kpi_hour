﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.InbizERP.BusinessLogic;

/// <summary>
/// Summary description for SivanandaSession
/// </summary>
public class SivanandaSession
{
    public static Partners CurrentGuest
    {
        get
        {
            return HttpContext.Current.Session["SIVANANDA_GUEST"] != null ? (Partners)HttpContext.Current.Session["SIVANANDA_GUEST"] : null;
        }
    }

    public static bool IsAuthenticated
    {
        get
        {
            return SivanandaSession.CurrentGuest != null;
        }
    }

    public static void SetSession(Partners partner)
    {
        HttpContext.Current.Session["SIVANANDA_GUEST"] = partner;
    }

    public static void ClearSession()
    {
        HttpContext.Current.Session.Remove("SIVANANDA_GUEST");
    }

    public static string SearchedHash
    {
        get
        {            
            return HttpContext.Current.Session["WEB_SEARCH_HASH"] != null ? (string)HttpContext.Current.Session["WEB_SEARCH_HASH"] : string.Empty;
        }
        set {
            HttpContext.Current.Session["WEB_SEARCH_HASH"] = value;
        }
    }
}