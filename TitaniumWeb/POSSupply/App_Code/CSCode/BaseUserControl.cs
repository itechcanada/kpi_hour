﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using iTECH.Library.Web;
using iTECH.InbizERP.BusinessLogic;
using jq = Trirand.Web.UI.WebControls;

public class BaseUserControl : System.Web.UI.UserControl
{
    public bool IsPagePostBack(params jq.JQGrid[] grids)
    {
        bool isPostBack = IsPostBack;
        foreach (jq.JQGrid item in grids)
        {
            if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
            {
                isPostBack = true;
                break;
            }
        }
        return isPostBack;
    }
}