﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddEditForecast.aspx.cs" Inherits="AddEditForecast" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
 <div id="contentBottom" 
          <%--style="padding: 5px; height: 103px; overflow: auto; text-align:center" 
          onkeypress="return disableEnterKey(event)">--%>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />
 
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
        <td class="text" style="font-weight:bold; font-size:20px;">
        <asp:Label ID="lblDatabaseBranch" Text=" " runat="server"  />
        </td>
             <td></td> 
        </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblBudget" Text="Budget" runat="server" />
                </td>
                <td >
                    <asp:TextBox ID="txtBudget" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"></asp:TextBox>
                    <asp:RangeValidator runat="server" ID="rngDate" ControlToValidate="txtBudget" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
                <td class="text">
                    <asp:Label ID="lblForecast" Text="Forecast" runat="server" />
                </td>
                <td  >
                    <asp:TextBox ID="txtForecast" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"  />
                    <asp:RangeValidator runat="server" ID="RangeValidator1" ControlToValidate="txtForecast" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblGPBudget" Text="GPBudget" runat="server" />
                </td>
                <td >
                    <asp:TextBox ID="txtGPBudget" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"></asp:TextBox>
                    <asp:RangeValidator runat="server" ID="RangeValidator2" ControlToValidate="txtGPBudget" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
               <td class="text">
                    <asp:Label ID="lblLBSShippedPlan" Text="LBSShippedPlan" runat="server" />
                </td>
                <td  >
                    <asp:TextBox ID="txtLBSShippedPlan" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"  />
                    <asp:RangeValidator runat="server" ID="RangeValidator5" ControlToValidate="txtLBSShippedPlan" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
            </tr>
            <tr>
             <td class="text">
                    <asp:Label ID="lblGPForecast" Text="GPForecast" runat="server" />
                </td>
                <td  >
                    <asp:TextBox ID="txtGPForecast" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"  />
                    <asp:RangeValidator runat="server" ID="RangeValidator3" ControlToValidate="txtGPForecast" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
               <td class="text">
                    <asp:Label ID="lblGPForecastPct" Text="GPForecast %" runat="server" />
                </td>
                <td  >
                    <asp:TextBox ID="txtGPForecastPct" MaxLength="20" runat="server" CssClass="numericTextField" Width="150px"  />
                    <asp:RangeValidator runat="server" ID="RangeValidator4" ControlToValidate="txtGPForecastPct" Type="Double" ValidationGroup="RegisterUser"
                        MinimumValue="0" MaximumValue="999999999999999999" ErrorMessage="Please Enter Double value." />
                </td>
            </tr>
        </table>
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Text="Save" ID="btnSave" runat="server" class="ui-button ui-widget ui-state-default ui-corner-all"
            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
        <asp:Button Text="Cancel" ID="btnCancel" runat="server" class="ui-button ui-widget ui-state-default ui-corner-all"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="RegisterUser" />

        <script type = "text/javascript">
            $(document).ready(function () {
                //$("#lblCategoryName").html($.getParamValue('CategoryName'));
            });

            $.extend({
                getParamValue: function (paramName) {
                    parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                    var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                    var regex = new RegExp(pattern);
                    var matches = regex.exec(window.location.href);
                    if (matches == null) return '';
                    else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
                }
            });

        </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>

