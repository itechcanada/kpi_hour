﻿(function ($) {
    // Internal function used to generate a random ID
    function randomID() {
        var id = "";
        for (var i = 1; i <= 10; i++)
            id += (Math.floor(Math.random() * 10) + 1);
        return id;
    }

    // Show an alert dialog
    $.alert = function (message) {
        var buttons = {};
        buttons["Ok"] = function () {
            $(this).dialog("close");
        };

        var _defaultOptions = {
            title: 'Message',
            closeOnEscape: true,
            position: 'center',
            width: '400',
            height: 'auto',
            buttons: buttons
        };

        var messagecont = $("<div />")
                .html(message);


        var ret = $("<div />")
                .attr("id", randomID())
                .css("display", "none")
                .append(messagecont)
				.appendTo(document.body).dialog(_defaultOptions);
        setTimeout(function () {
            ret.dialog("close");
        }, 5000);

    };

    //Show Confirm dialog
    $.confirm = function (message, yes, no) {
        var buttons = {};
        buttons["Yes"] = function () {
            (yes || $.noop).call();
            $(this).dialog("close");
        };

        buttons["No"] = function () {
            (no || $.noop).call();
            $(this).dialog("close");
        };

        var _defaultOptions = {
            title: 'Confirm',
            closeOnEscape: false,
            position: 'center',
            buttons: buttons
        };

        var messagecont = $("<div />")
                .html(message);


        var ret = $("<div />")
                .attr("id", randomID())
                .css("display", "none")
                .append(messagecont)
				.appendTo(document.body).dialog(_defaultOptions);

    };

})(jQuery);