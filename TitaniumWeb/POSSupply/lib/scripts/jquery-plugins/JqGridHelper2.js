﻿//Initialize the custom search panel to perform search on jqGrid.
/// <reference path="../jquery-1.5.1.min.js" />
/// <reference path="../jquery-ui-1.8.11.custom.min.js" />
/// <reference path="../jquery.jqGrid-4.0.0/js/jquery.jqGrid.min.js" />

(function ($) {
    $.fn.extend({
        initGridHelper: function (options) {
            var defaults = {
                searchPanelID: "pnlSearchID",
                searchButtonID: "btnSearch",
                gridWrapPanleID: "grid_wrapper",
                fixedSearchOptions: {}
            };
            var options = $.extend(defaults, options);

            var $grid = jQuery(this);
            var $srcPnl = jQuery("#" + options.searchPanelID);
            var $btnSrc = jQuery("#" + options.searchButtonID);

            /*setTimeout(function () {
            $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
            $grid.setGridParam({
            loadComplete: function (data) {
            $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
            }
            })
            }, 1000);*/

            $srcPnl.keydown(function (e) {
                if (e.keyCode == 13) {
                    $btnSrc.trigger("click");
                    return false;
                }
            });

            $btnSrc.click(function () {
                var filterArr = {};
                var iCount = 0;
                $(".filter-key", $srcPnl).each(function () {
                    var ele = document.getElementById($(this).attr('for'));

                    if ($('#' + $(this).attr('for')).is('input[type=text]')) {
                        filterArr[ele.id] = ele.value;
                        iCount++;
                    }
                    else if ($('#' + $(this).attr('for')).is('select')) {
                        var sSelectedValue = ''
                        $("select#" + $(this).attr('for') + " option:selected").each(function () {
                            var val = $(this).val();
                            if (val != 'undefined') {
                                if (sSelectedValue == '') {
                                    sSelectedValue = val;
                                }
                                else {
                                    sSelectedValue += "," + val;
                                }
                            }
                        });
                        if (sSelectedValue == 'undefined') {
                            sSelectedValue= '';
                        }
//                        alert(sSelectedValue);
                        filterArr[ele.id] = sSelectedValue;
                        iCount++;
                    }
                    else if ($('#' + $(this).attr('for')).is('input[type=radio]')) {
                        if (ele.checked) {
                            filterArr[ele.id] = ele.value;
                            iCount++;
                        }
                    }
                });
                filterArr["_history"] = "0";
                $grid.appendPostData(filterArr);
                $grid.trigger("reloadGrid", [{ page: 1}]);

                return false;
            });

            $(window).resize(function () {
                $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
            });
        },
        jqResizeAfterLoad: function (gridWrapPanleID, resizeOffset) {
            jQuery(this).fluidGrid({ example: "#" + gridWrapPanleID, offset: resizeOffset });
        },
        jqCustomSearch: function (objPostDictionaryData) {
            jQuery(this).appendPostData(objPostDictionaryData);
            jQuery(this).trigger("reloadGrid", [{ page: 1}]);
        }
    });
})(jQuery);

