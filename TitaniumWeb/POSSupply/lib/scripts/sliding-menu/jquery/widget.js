(function($) {

  $.fn.sideBar = function() {
    var index = 1;
    return $(this).each(function() {
      
      $("body").mouseup(function(){
	  unFocus();
      })


var node = "<div class='slmenu-content-wrapper'></div>";
      $(this).append(node);

      node = "<div class='slmenu-header-wrapper'></div>";
      $(this).append(node);

      $(this).find('.slmenu-widget-set').each(function () {
	$(this).children().each(function(){
	    $(this).attr('id', 'slmenu-widget-' + index);
	})
	index++;
      })

$(this).find('.slmenu-widget-set .slmenu-widget-content').each(function () {
	var content = $(this);
	$(this).parent().parent().children(".slmenu-content-wrapper").append(content);
      })

$(this).find('.slmenu-widget-set .slmenu-widget-title').each(function () {
	var content = $(this);
	$(this).parent().parent().children(".slmenu-header-wrapper").append(content);
	$(this).click(onClick)
	
      })

$(this).find('.slmenu-widget-set').each(function () {
	$(this).remove();
      })
      
    });
    function onClick(){
        $(".slmenu-active").removeClass("slmenu-active");
        $(this).addClass("slmenu-active");
      var p = $(this).position();
      var left = p.left + $(this).width();
      var top = p.top;
      var id = $(this).attr('id');
      $(this).parent().parent().children(".slmenu-content-wrapper").children().each(function () {
	$(this).hide();
      });
$(this).parent().parent().children(".slmenu-content-wrapper").css({
	'left': left+"px",
	'top': top+'px'
      });
$(this).parent().parent().children(".slmenu-content-wrapper").children("#" + id).css({
	'left': left+"px",
	'top': top+'px'
      })
$(this).parent().parent().children(".slmenu-content-wrapper").children("#" + id).show();
      return false;
    }
    
    function unFocus(){
        var id = $(".slmenu-active").attr('id')
        $(".slmenu-active").parent().siblings(".slmenu-content-wrapper").children("#" + id).hide();
        $(".slmenu-active").removeClass("slmenu-active");
      return false;
    }
  };
})(jQuery);