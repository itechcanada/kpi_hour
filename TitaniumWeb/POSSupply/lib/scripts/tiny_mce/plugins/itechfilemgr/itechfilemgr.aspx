﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="itechfilemgr.aspx.cs" Inherits="Scripts_tinymce_plugins_itechfilemgr_itechfilemgr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>File Manager</title>
    <link href="css/images.css" rel="stylesheet" type="text/css" />
    <link href="css/south-street/jquery-ui-1.8.16.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="js/tipsy/jquery-smallipop.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>   
    <script src="../../tiny_mce_popup.js" type="text/javascript"></script>
    <script src="js/itechfilemgr.js" type="text/javascript"></script>
    <script src="js/jquery.jstree.js" type="text/javascript"></script>    
    <script src="js/tipsy/jquery-smallipop-0.1.1.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2">
                <div id="spanPath" class="ui-widget-content" style="height: 20px; clear: both; vertical-align: top;
                    margin-bottom: 5px;">
                </div>                
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 200px;">
                <div id="treeDiv" style="height: 480px; display: block;" class="ui-widget-content">
                    <div id="tree">
                    </div>
                    <script type="text/javascript">
                        function LoadTree() {
                            $("#tree").jstree({
                                "core": { "initially_open": ["root2"] },
                                "html_data": {
                                    "ajax": {
                                        "url": "files.aspx?funcation=LoadFilesHTML" + "&sessionid=" + tinyMCEPopup.getWindowArg("sessionid"),
                                        "data": function (n) {                                            
                                            return { Path: n.attr ? n.attr("path") : 0 };
                                        }
                                    }
                                },
                                "plugins": ["themes", "html_data"]

                            });
                        }

                        $(function () { LoadTree(); });
                    </script>
                </div>
            </td>
            <td valign="top">
                <div id="FolderAndFiles" style="height: 480px; margin-left: 5px; position: relative;"
                    class="ui-widget-content">
                    <div id="divProcess" style="display: none; position: absolute; background-color: White;
                        top: 0px; z-index: 101; left: 0px; padding: 5px;">
                        <img src="icons/21-0.gif" />please wait ...
                    </div>
                    <ul class="nav">
                        <li><a class="selected">Select File</a></li>
                        <li><a>Upload Files</a></li>
                    </ul>
                    <div id="body">
                        <div class="tab">                            
                            <ul id="gallery" style="height: 440px; margin-top: 0px;">
                            </ul>
                        </div>
                        <div class="tab" style="display: none;">
                            <div style="margin-top: 5px; clear: both;">
                                <iframe id="IframUpload" onload="IframOnload()" marginheight="0px" marginwidth="0px"
                                    align="left" frameborder="0" style="height:400px;" scrolling="no"></iframe>
                                <%--<div style="vertical-align: middle;" id="Uploadingimage">
                                    <img alt="" src="icons/21-0.gif" />
                                    uploading...
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>        
    </table>
    
    <script type="text/javascript">
        var txtPath;
        var CurentPath = '<%=ConfigurationManager.AppSettings["FileManagerRootPath"] %>';
        var Copy = "";
        var Cut = "";
        function LoadFiles(Path) {
            //  alert(tinyMCEPopup.getWindowArg("sessionid"));
            //document.getElementById("spanPath").innerHTML = " &nbsp;&nbsp;" + Path;
            txtPath.value = Path + "/";
            CurentPath = Path + "/";
            document.getElementById("divProcess").style.display = "";
            UpdatePathBar(Path);
            $.ajax({
                url: "files.aspx",
                type: "POST",
                contentType: "application/x-www-form-urlencoded;charset=UTF8",
                dataType: 'html',
                data: "funcation=FilesULLI&Path=" + Path + "&sessionid=" + tinyMCEPopup.getWindowArg("sessionid"),
                success: callback
            });
        }

        function valid_File(File) {
            if (File == "") {
                alert("please select file");
                return false;
            }
            /** If you want valiat file name do this here **/
            return true
        }

        function IframOnload() {
            $ifram = $('#IframUpload');
            ifram = document.getElementById('IframUpload')
            upload_img_process = document.getElementById("divProcess");
            subButton = ifram.contentWindow.document.getElementById("bntUpload");
            uploder = ifram.contentWindow.document.getElementById("fuImage1_fuUpload");
            Label_message = ifram.contentWindow.document.getElementById("spanerror");
            txtPath = ifram.contentWindow.document.getElementById("txtPath");
            $ibody = $ifram.contents().find("body");
            $ifram.css({ width: $ifram.parent().parent().parent().width() });
            upload_img_process.style.display = 'none';
            if (subButton != null) {
                subButton.onclick = function () {
                    upload_img_process.style.display = '';
                    /*var isvalid = valid_File(uploder.value);
                    if (isvalid) {
                        upload_img_process.style.display = '';
                    }
                    return isvalid;*/
                }
            }
            LoadFiles(CurentPath);
        }

        /* Update Path Bar */

        function UpdatePathBar(Path) {
            var PathArray = Path.split('/');
            var ParHTML = "";
            var rootPath = '<%=ConfigurationManager.AppSettings["FileManager"] %>';
            var CurentPath = "";
            for (index = 0; index < PathArray.length; index++) {
                CurentPath = CurentPath + PathArray[index] + "/";
                if (index > 2 && PathArray[index] != "") {
                    ParHTML += "<a onclick=LoadFiles('" + CurentPath + "') href='javascript:;'>" + PathArray[index].replace("%20", " ") + "</a>" + "&nbsp;/";
                }
                else if(index == 0) {
                    ParHTML += "<a onclick=LoadFiles('" + rootPath + "') href='javascript:;'>Root</a>" + "&nbsp;/";
                }
            }

            document.getElementById("spanPath").innerHTML = " &nbsp;&nbsp;" + ParHTML;
        }

        function callback(data, textStatus) {
            document.getElementById("divProcess").style.display = "none";
            $("#gallery").html(data);
            $('.file_info').smallipop({
                theme: 'intransit',
                invertAnimation: true,
                popupDistance: 5
            });             
        }

        $(document).ready(function () {
            $("#IframUpload").attr("src", "Uploader.aspx?sessionid=" + tinyMCEPopup.getWindowArg("sessionid"));
            //$('.jui_tip').tipsy({ live: true, fade: true, html: true });                                  
        });
    </script>
    </form>
</body>
</html>
