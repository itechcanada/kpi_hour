﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;

public partial class AddEditForecast : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string database = this.Database;
                string branch = this.Branch;
                ForecastResult fc = new ForecastResult();
                List<ForecastResult> fresult = new List<ForecastResult>();
                if (!string.IsNullOrEmpty(this.Database) && !string.IsNullOrEmpty(this.Branch))
                {
                    fresult = fc.GetForecastDetails(this.Database, this.Branch);    //, this.GetYearMonth
                    if (fresult.Count > 0)
                    {
                        txtBudget.Text = BusinessUtility.GetString(fresult[0].Budget);
                        txtForecast.Text = BusinessUtility.GetString(fresult[0].Forecast);
                        txtGPBudget.Text = BusinessUtility.GetString(fresult[0].GPBudget);
                        txtGPForecast.Text = BusinessUtility.GetString(fresult[0].GPForecast);
                        txtGPForecastPct.Text = BusinessUtility.GetString(fresult[0].GPForecastPct);
                        txtLBSShippedPlan.Text = BusinessUtility.GetString(fresult[0].LBSShippedPlan);
                    }
                }
                txtBudget.Focus();
            }
        }

        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (BusinessUtility.GetString(Request.QueryString["status"]) == "INDIVIDUAL")
            {
                ForecastResult fc = new ForecastResult();
                fc.Database = this.Database;
                fc.Branch = this.Branch;
                fc.Budget = BusinessUtility.GetDouble(txtBudget.Text);
                fc.Forecast = BusinessUtility.GetDouble(txtForecast.Text);
                fc.GPBudget = BusinessUtility.GetDouble(txtGPBudget.Text);
                fc.GPForecast = BusinessUtility.GetDouble(txtGPForecast.Text);
                fc.GPForecastPct = BusinessUtility.GetDouble(txtGPForecastPct.Text);
                fc.LBSShippedPlan = BusinessUtility.GetDouble(txtLBSShippedPlan.Text);
                if (!string.IsNullOrEmpty(this.Database) && !string.IsNullOrEmpty(this.Branch))
                {
                    fc.UpdateForecast();    //this.GetYearMonth
                }
            }

            Globals.RegisterCloseDialogScript(Page, string.Format("parent.reloadGrid();"), true);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    public string Database
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Database"]);
        }
    }

    public string GetYearMonth
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["UDate"]) != "")
            {
                return BusinessUtility.GetDateTime(BusinessUtility.GetString(Request.QueryString["UDate"]), DateFormat.MMddyyyy).ToString("yyyy-MM");
            }
            else
            {
                return DateTime.Now.ToString("yyyy-MM");
            }
        }
    }
    

    public string Branch
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Branch"]);
        }
    }
}