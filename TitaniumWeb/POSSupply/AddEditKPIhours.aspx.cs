﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using System.Web.Services;

public partial class AddEditKPIhours : System.Web.UI.Page
{
    KPIHours KPIObj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            KPIObj = new KPIHours();
            KPIObj.FillDatabaseControl(null, ddlDataBaseName, new ListItem { Text = Resources.Resource.lblselect, Value = "0" });
        }

        this.RemoveItemFromDropdown();
    }

    private void RemoveItemFromDropdown()
    {
        ddlDataBaseName.Items.Remove(ddlDataBaseName.Items.FindByText("Norway"));
        ddlDataBaseName.Items.Remove(ddlDataBaseName.Items.FindByText("Pierce"));
        ddlDataBaseName.Items.Remove(ddlDataBaseName.Items.FindByText("22"));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        hdnBranchID.Value = string.Empty;
        KPIHours objKPI = new KPIHours();
        string resultData = string.Empty;
        try
        {
            objKPI.DataBase = BusinessUtility.GetString(ddlDataBaseName.SelectedValue);
             
            hdnBranchID.Value = objKPI.KPIBranch = BusinessUtility.GetString(Request["ddlBranchList"]);
              
            objKPI.KPIDate = BusinessUtility.GetDateTime(txtKPIDate.Text, DateFormat.MMddyyyy);
            objKPI.KPIhours = BusinessUtility.GetInt(txtKPIHour.Text);
            resultData = objKPI.KPIhourinsert();
            if (resultData == Hourmsg.INSERTSUCCESS)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + Resources.Resource.lblHousuccessmsg + " ');", true);
            }
            else if (resultData == Hourmsg.UPDATESUCCESS)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + Resources.Resource.lblHouUptmsg + " ');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + Resources.Resource.lblOperationUnsuccess + " ');", true);
            }

            txtKPIDate.Text = string.Empty;
            txtKPIHour.Text = string.Empty;
            hdnBranchID.Value = string.Empty;




        }
        catch(Exception ex)
        {
            throw;
        }
        finally { }
    }
    [WebMethod]
    public static List<BranchDetails> BindBranchDropdown(string dataBaseID)
    {
        KPIHours objKPI = new KPIHours();
        try
        {
            return objKPI.getBranch(dataBaseID);
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }


    [WebMethod]
    public static List<KpiDetails> GetKpiData(string dataBaseID, string sBranch)
    {
        KPIHours objKPI = new KPIHours();
        try
        {
            return objKPI.PopulateKpiData(dataBaseID, sBranch);
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtKPIDate.Text = string.Empty;
        txtKPIHour.Text = string.Empty;
    }

    [WebMethod]
    public static List<KpiDetails> GetKpiALLData(string dataBaseID, string sBranch)//,string idate
    {
        KPIHours objKPI = new KPIHours();
        try
        {
            return objKPI.PopulateKpiData(dataBaseID, sBranch);
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
    protected void btnGetKPIDataOnDateSelected_Click(object sender, EventArgs e)
    {
        KPIHours objKPI = new KPIHours();
        try
        {
            objKPI.PopulateKpiData(BusinessUtility.GetString(ddlDataBaseName.SelectedValue), BusinessUtility.GetString(Request["ddlBranchList"]), BusinessUtility.GetDateTime(txtKPIDate.Text, DateFormat.MMddyyyy));
            txtKPIHour.Text = BusinessUtility.GetString(objKPI.KPIhours);
        }
        catch (Exception ex) { throw ex; }
        finally { }
    }
}