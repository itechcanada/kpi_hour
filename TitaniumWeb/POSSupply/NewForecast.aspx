﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="NewForecast.aspx.cs" Inherits="NewForcast" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

<link href="lib/css/style.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/main.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <%--<link href="../lib/scripts/superfishmenu/css/superfish.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" rel="stylesheet" media="all" href="../Css/popup.css" />
    <!--Include css files ends here.-->    
    <%--<script type="text/javascript" src="../Handlers/script_loader.aspx?load=jquery,jqueryui,json,jsload,messagebox,jqgrid,framedialog,jqchosen"></script>--%>    
    <asp:Literal ID="ltScripts" Text="" runat="server" />   
    <script src="lib/scripts/superfishmenu/js/superfish.js" type="text/javascript"></script>
    <script src="lib/scripts/sliding-menu/jquery/widget.js" type="text/javascript"></script>
    <script src="lib/scripts/jquery-plugins/jquery.numeric.js" type="text/javascript"></script>
    <script src="lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
    <script type="text/javascript">
        var gAJAXReturnData = ""; function calliAJAX(b, d, c, a) { if (document.getElementById("loading") != null) { $("#loading").css("visibility", "visible") } gAJAXReturnData = ""; $.ajax({ type: "GET", async: a, contentType: "application/x-www-form-urlencoded; charset=UTF-8", url: b, data: d, error: function () { calliAlert(c) }, success: function (e) { if (document.getElementById("loading") != null) { $("#loading").css("visibility", "hidden") } gAJAXReturnData = $.trim(e) } }) } function calliAlert(a) { };
    </script>
    <script src="../jQuery-plugins/popup.js" language="JavaScript" type="text/javascript"></script>    
    <script type="text/javascript">
        var ddSmoothImagePath = { downImage: '<%=ResolveUrl("~/lib/scripts/ddmenu/arrows-down.png")%>', rightImage: '<%=ResolveUrl("~/lib/scripts/ddmenu/arrows-right.png")%>' };
    </script>
    <link href="lib/scripts/ddmenu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />    
    <script src="lib/scripts/ddmenu/ddsmoothmenu.js" type="text/javascript"></script>

<script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
<script type="text/javascript" src="jQuery-plugins/popup.js"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
    ga('send', 'pageview');

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
<div style="width: 100%; padding-left: 3px;">
                <div style="float: left;">
                    <b><span id="lblSelectedCollection" style="font-size: 22px;">&nbsp;</span></b>
                </div>
                <div style="float: right; margin-right:7Px;">
                    <asp:Button ID="btnPublishStyles" runat="server" Text="Run Report"
                        OnClick="btnGoToSSRS_OnClick" />
                </div>
            </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">

    <div style="padding: 5px;">
        
        <asp:Panel runat="server" ID="SearchPanel">
           <%-- <table>
                <tr>
                    <td style="text-align: right" class="text">
                        <asp:Label ID="lblFromDate" runat="server" CssClass="lblBold" Text="MM/YYYY">
                        </asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtFromDate" CssClass="datepicker " Enabled="true"
                            runat="server" Width="90px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalFromDate" runat="server" ControlToValidate="txtFromDate"
                            ErrorMessage="*" SetFocusOnError="true" ValidationGroup="RegisterUser" style="color:Red;"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button Text="Go" ID="btnSave" runat="server" class="ui-button ui-widget ui-state-default ui-corner-all"
                            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>--%>
        </asp:Panel>
        <h3>
            Forcecasting Details
        </h3>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvForecast" Height="400px" AutoWidth="True"
                    OnCellBinding="jgdvForecast_CellBinding" OnDataRequesting="jgdvForecast_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="Databases" HeaderText="Database" Editable="false"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="Branch" HeaderText="Branch" Editable="false" />
                        <trirand:JQGridColumn DataField="Budget" HeaderText="Budget" Editable="false" TextAlign="Right" />
                        <trirand:JQGridColumn DataField="Forecast" HeaderText="Forecast" Editable="false" TextAlign="Right"/>
                        <trirand:JQGridColumn DataField="GPBudget" HeaderText="GPBudget" Editable="false" TextAlign="Right"/>
                        <trirand:JQGridColumn DataField="GPForecast" HeaderText="GPForecast" Editable="false" TextAlign="Right"/>
                        <trirand:JQGridColumn DataField="GPForecastPct" HeaderText="GPForecastPct" Editable="false" TextAlign="Right"  />
                        <trirand:JQGridColumn DataField="LBSShippedPlan" HeaderText="LBSShippedPlan" Editable="false" TextAlign="Right"/>
                        <trirand:JQGridColumn HeaderText="Edit" DataField="Branch" Sortable="false" TextAlign="Center"
                            Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //Variables
        $grid = $("#<%=jgdvForecast.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            //            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });



        function jqGridResize() {
            $("#<%=jgdvForecast.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function reloadGrid() {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function editPopup(dbName, branch) {
            var url = "AddEditForecast.aspx";
            var queryData = {};
            queryData.Database = dbName;
            queryData.Branch = branch;
            queryData.Status = "INDIVIDUAL";
            var t = "";
            var height = 260;
            t = "Edit Forecast Value";

            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 750,
                height: height,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>
