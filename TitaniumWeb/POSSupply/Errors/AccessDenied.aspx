﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="AccessDenied.aspx.cs" Inherits="Error_AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <h1>Access Denied!</h1>
    <p>
       Sorry you are trying to access a resource that you do not have access to.
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>

