var gAJAXReturnData="";function calliAJAX(b,d,c,a){if(document.getElementById("loading")!=null){$("#loading").css("visibility","visible")}gAJAXReturnData="";$.ajax({type:"GET",async:a,contentType:"application/x-www-form-urlencoded; charset=UTF-8",url:b,data:d,error:function(){calliAlert(c)},success:function(e){if(document.getElementById("loading")!=null){$("#loading").css("visibility","hidden")}gAJAXReturnData=$.trim(e)}})}function calliAlert(a){};

function replaceAll(ostr,ochs,nchs){
    var str=ostr;
    if(!(typeof str=="undefined")){
        var index=str.indexOf(ochs);
        while(index!=-1){
            str=str.replace(ochs,nchs);
            index=str.indexOf(ochs);
        }
    }
    return str;
}

function replaceSpecialCh(val){
    // alert("replace "+val);
    val=replaceAll(val,",","__COMMA__");
    val=replaceAll(val,"$","__DOLLAR__");
    val=replaceAll(val,"&","__AND__");
    val=replaceAll(val,"%","__PERCENT__");
    val=replaceAll(val,"'","__QUOTE__");
    val=replaceAll(val,'"',"__DQUOTE__");
    val=replaceAll(val,'\\',"__BSLASH__");
    val=replaceAll(val,"\n","__NEWLINE__N__");
    val=replaceAll(val,"\r","__NEWLINE__R__");
    return val;
}
/**
The function is to restore special special character in the input string
 */
function restoreSpecialCh(controlValue){
    // alert("restore : "+ controlValue);
    controlValue=replaceAll(controlValue,"__COMMA__",",");
    controlValue=replaceAll(controlValue,"__DOLLAR__","$");
    controlValue=replaceAll(controlValue,"__AND__","&");
    controlValue=replaceAll(controlValue,"__PERCENT__","%");
    controlValue=replaceAll(controlValue,"__QUOTE__","'");
    controlValue=replaceAll(controlValue,"__DQUOTE__",'"');
    controlValue=replaceAll(controlValue,"__BSLASH__",'\\');
    controlValue=replaceAll(controlValue,"__NEWLINE__N__","\n");
    controlValue=replaceAll(controlValue,"__NEWLINE__R__","\r");
    return controlValue;
}