﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="AddEditKPIhours.aspx.cs" Inherits="AddEditKPIhours"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">  
          <link href="lib/css/style.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/main.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <link href="lib/css/error_theme/jquery-ui-1.8.20.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <link href="lib/scripts/sliding-menu/css/widget.css" rel="stylesheet" type="text/css" />
    <link href="lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <%--<link href="../lib/scripts/superfishmenu/css/superfish.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" rel="stylesheet" media="all" href="../Css/popup.css" />
    <!--Include css files ends here.-->    
    <%--<script type="text/javascript" src="../Handlers/script_loader.aspx?load=jquery,jqueryui,json,jsload,messagebox,jqgrid,framedialog,jqchosen"></script>--%>    
    <asp:Literal ID="ltScripts" Text="" runat="server" />   
    <script src="lib/scripts/superfishmenu/js/superfish.js" type="text/javascript"></script>
    <script src="lib/scripts/sliding-menu/jquery/widget.js" type="text/javascript"></script>
    <script src="lib/scripts/jquery-plugins/jquery.numeric.js" type="text/javascript"></script>
    <script src="lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
    <script type="text/javascript">
        var gAJAXReturnData = ""; function calliAJAX(b, d, c, a) { if (document.getElementById("loading") != null) { $("#loading").css("visibility", "visible") } gAJAXReturnData = ""; $.ajax({ type: "GET", async: a, contentType: "application/x-www-form-urlencoded; charset=UTF-8", url: b, data: d, error: function () { calliAlert(c) }, success: function (e) { if (document.getElementById("loading") != null) { $("#loading").css("visibility", "hidden") } gAJAXReturnData = $.trim(e) } }) } function calliAlert(a) { };
    </script>
    <script src="../jQuery-plugins/popup.js" language="JavaScript" type="text/javascript"></script>    
    <script type="text/javascript">
        var ddSmoothImagePath = { downImage: '<%=ResolveUrl("~/lib/scripts/ddmenu/arrows-down.png")%>', rightImage: '<%=ResolveUrl("~/lib/scripts/ddmenu/arrows-right.png")%>' };
    </script>
    <link href="lib/scripts/ddmenu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />    
    <script src="lib/scripts/ddmenu/ddsmoothmenu.js" type="text/javascript"></script>

<script type="text/javascript" src="lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
<script type="text/javascript" src="jQuery-plugins/popup.js"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
    ga('send', 'pageview');

    </script>

       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">      
    </div>
    <div class="col2" style="width: 200px;">
    </div>
    <div class="clear"></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
     
    </asp:Panel>
    <div class="clear">
    </div>
    <div class="inner" style="display:none;">
        <h2>
            <%=Resources.Resource.lblSearchOptions%></h2>
        <p>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
    </div>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
 <div id="contentBottom" 
          <%--style="padding: 5px; height: 103px; overflow: auto; text-align:center" 
          onkeypress="return disableEnterKey(event)">--%>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />
        <br />
        <h3>
               
            </h3>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">               
                <tr>
                    <td class="text">
                       <%= Resources.Resource.lblKPIDatabase %> 
                    </td>
                    <td class="input"> 
                           <asp:DropDownList ID="ddlDataBaseName" runat="server"  onchange="GetBranchList(this.value);"></asp:DropDownList>
                        <span class="style1"> 
                            </span> 
                    </td> 
      
                    <td class="text" id="Branchlabel" style="display:none;" runat="server">
                       <%= Resources.Resource.lblKPIBranch %>   
                    </td>
                    <td class="input" id="Branchdrown" style="display:none;" runat="server">
                        <select id="ddlBranchList" name="ddlBranchList"  onchange="GetData(this.value);"></select>                              
                    </td>
                  
                </tr>   
                <tr id="trDateHours" style="display:none;" runat="server">
                    <td class="text">
                       <%= Resources.Resource.lblKPITheWeekof %>   
                    </td>
                    <td class="input">
                     <asp:TextBox ID="txtKPIDate"   runat="server"  Width="174px" CssClass="datepicker1"></asp:TextBox>   
     <asp:RequiredFieldValidator ID ="rtRequired"  runat="server" ControlToValidate ="txtKPIDate" ErrorMessage="*"  ValidationGroup="Registerhour"> 
     </asp:RequiredFieldValidator>
                    </td>
                      <td class="text">
                        <%= Resources.Resource.lblKPIHour %>    
                    </td>
                    <td class="input">
                          <asp:TextBox ID="txtKPIHour"  runat="server" CssClass="numericTextField" Width="174px"  />      
                          <asp:RequiredFieldValidator ID ="RequiredFieldValidator1"  runat="server" ControlToValidate ="txtKPIHour" ErrorMessage="*" ValidationGroup="Registerhour"></asp:RequiredFieldValidator>    
                    </td>
                </tr>  
            </table>   
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Text="Save" ID="btnSave" runat="server" class="ui-button ui-widget ui-state-default ui-corner-all"
            ValidationGroup="Registerhour" OnClick="btnSave_Click" OnClientClick="return confirmmsg();" />
        <asp:Button Text="Cancel" ID="btnCancel" runat="server" class="ui-button ui-widget ui-state-default ui-corner-all" OnClick="btnCancel_Click"/>         
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
         ShowSummary ="false" ValidationGroup="RegisterUser" />
    
    <asp:HiddenField  ID="hdnBranchID"  runat="server"/>
    <asp:Button ID="btnGetKPIDataOnDateSelected" runat="server" OnClick="btnGetKPIDataOnDateSelected_Click" style="display:none;" />

        <script type = "text/javascript">
            $(document).ready(function () {
                //$("#lblCategoryName").html($.getParamValue('CategoryName'));
            });

            $.extend({
                getParamValue: function (paramName) {
                    parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                    var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                    var regex = new RegExp(pattern);
                    var matches = regex.exec(window.location.href);
                    if (matches == null) return '';
                    else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
                }
            });


            function GetBranchList(ddlDatabase) {

                if (ddlDatabase == "0") {
                    $("#<%=Branchlabel.ClientID%>").hide();
                    $("#<%=Branchdrown.ClientID%>").hide();
                    $("#<%=trDateHours.ClientID %>").hide();

                }
                else {
                    $("#<%=Branchlabel.ClientID%>").show();
                    $("#<%=Branchdrown.ClientID%>").show();
                    $("#<%=trDateHours.ClientID %>").hide();
                }



               

                var BranchList = $("[id*=ddlBranchList]");
                BranchList.empty();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "AddEditKPIhours.aspx/BindBranchDropdown",
                    data: "{dataBaseID:'" + ddlDatabase + "'}",
                    dataType: "json",
                    success: function (data) {
                        var BranchList = $("[id*=ddlBranchList]");
                        if (data.d != "") {
                            BranchList.append($("<option></option>").val(0).html("<%=Resources.Resource.lblBranchSelect%>"));
                            $.each(data.d, function () {
                                BranchList.append($("<option></option>").val(this['BranchID']).html(this['BranchName']));
                            });

                            if ($("#<%=hdnBranchID.ClientID%>").val() == "") {
                                // GetData(BranchList.val());
                            }
                            else {
                                BranchList.val($("#<%=hdnBranchID.ClientID%>").val());
                                // GetData($("#<%=hdnBranchID.ClientID%>").val());
                            }
                        }
                        else {
                            $("#<%=txtKPIDate.ClientID %>").val("");
                            $("#<%=txtKPIHour.ClientID %>").val("");
                        }

                        
                    },
                    error: function (result) {
                        // alert("Error");
                    }
                });
            }

            function GetData(ddlDatabase) {
                if (ddlDatabase != "") {
                    if (ddlDatabase == "0") {
                        $("#<%=trDateHours.ClientID %>").hide();
                    }
                    else {
                        $("#<%=trDateHours.ClientID %>").show();
                    }

                    // $("#<%=trDateHours.ClientID %>").show(); 
                    var BranchList = $("[id*=ddlBranchList]");
                    var db = $('#ctl00_ctl00_cphFullWidth_cphMaster_ddlDataBaseName :selected').val();
                    var sDate = document.getElementById("ctl00_ctl00_cphFullWidth_cphMaster_txtKPIDate");
                    var shour = document.getElementById("ctl00_ctl00_cphFullWidth_cphMaster_txtKPIHour");

                    // BranchList.empty();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "AddEditKPIhours.aspx/GetKpiData",
                        data: "{dataBaseID:'" + db + "', sBranch:'" + ddlDatabase + "'}",
                        dataType: "json",
                        success: function (data) {
                            var BranchList = $("[id*=ddlBranchList]");
                            if (data.d != "") {
                                $.each(data.d, function () {
                                    sDate.value = this['KpiDate'];
                                    shour.value = this['KpiHour'];
                                    //BranchList.append($("<option></option>").val(this['BranchID']).html(this['BranchName']));
                                });
                            }
                            else {
                                var DDate = GetLastWeekStart();
                               
                                $("#<%=txtKPIDate.ClientID %>").val((DDate.getMonth() + 1).format("D2") + "/" + DDate.getDate().format("D2") + "/" + DDate.getFullYear());

                                //sDate.value = "";  
                                shour.value = "";
                            }

                            
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    //alert("Hello");

                    var DDate = GetLastWeekStart();

                    //alert((DDate.getMonth() + 1) + "/" + DDate.getDate() + "/" + DDate.getFullYear());
                    $("#<%=txtKPIDate.ClientID %>").val((DDate.getMonth() + 1) + "/" + DDate.getDate() + "/" + DDate.getFullYear());


                    $("#<%=txtKPIHour.ClientID %>").val("");
                }
            }

            $(document).ready(function () {
                var dataBaseID = $("[id*=ddlDataBaseName]");
                if (dataBaseID.val() != "") {
                    GetBranchList(dataBaseID.val());
                }
            });

            function RemoveElement() {
                $("#ddlBranchList option[value='ROS']").remove();
                $("#ddlBranchList option[value='CRP']").remove();
                $("#ddlBranchList option[value='EXP']").remove();
                $("#ddlBranchList option[value='ZZZ']").remove();
                $("#ddlBranchList option[value='STH']").remove();
            }

            $(document).ready(function () {
                $(".datepicker1").datepicker({
                    defaultDate: -14, minDate: -60,
                    beforeShowDay: function (date) {
                        var day = date.getDay();
                        return [day == 1, ""];
                    }

                });
            });

           /* $(".datepicker1").datepicker({
                onSelect: function (dateText, inst) {
                   // var date = $(this).val();
                    var db = $('#ctl00_ctl00_cphFullWidth_cphMaster_ddlDataBaseName :selected').val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",

                        url: "AddEditKPIhours.aspx/GetKpiALLData",
                        data: "{dataBaseID:'" + db + "', sBranch:'" + db + "'}",

                        dataType: "json",
                        success: function (data) {
                            var BranchList = $("[id*=ddlBranchList]");
                            if (data.d != "") {
                                $.each(data.d, function () {
                                    sDate.value = this['KpiDate'];
                                    shour.value = this['KpiHour'];
                                });
                            }
                            else {
                                alert("Else Part");
                                //sDate.value = "";  
                                shour.value = "";
                            }


                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                    
                }
            });  */

            function confirmmsg() {
                var myDate = new Date($("#<%=txtKPIDate.ClientID %>").val());
                if (myDate.getDay() != 1)
                {
                    alert("<%=Resources.Resource.lblKPIMsgDayShouldMonday %>");
                    return false;
                }
                

                if (confirm("<%=Resources.Resource.lblConfirmmsg %>")) {
                    return true;
                }
                else {
                    return false;
                }
            }



            function GetLastWeekStart() {
                var curr = new Date; // get current date
                var first = curr.getDate() - curr.getDay() - 6; // Gets day of the month (e.g. 21) - the day of the week (e.g. wednesday = 3) = Sunday (18th) - 6
                var last = first + 6; // last day is the first day + 6
                var startDate = new Date(curr.setDate(first));
                var endDate = new Date(curr.setDate(last));

                return startDate;
            }
         </script>
       

</asp:Content>
 

