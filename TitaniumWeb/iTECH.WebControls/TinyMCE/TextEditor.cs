﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;
using System.Text;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [ToolboxData("<{0}:TextEditor runat=server></{0}:TextEditor>")]
    public class TextEditor : System.Web.UI.WebControls.TextBox
    {
        private const string TinyMCEScriptKey = "TinyMCEFull";
        private const string EditorClass = "FullTextEditor";
        private string _scriptKey, _selectorClass;

        protected override void OnPreRender(EventArgs e)
        {
            _scriptKey = TinyMCEScriptKey + this.Layout.ToString();
            _selectorClass = EditorClass + this.Layout.ToString();

            if (!Page.ClientScript.IsStartupScriptRegistered(_scriptKey))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), _scriptKey, GetInitScript());
            }

            if (!CssClass.Contains(_selectorClass)) //probably this is not the best way how to add the css class but I do not know any beter way
            {
                if (CssClass.Length > 0)
                {
                    CssClass += " ";
                }
                CssClass += _selectorClass;
            }
            base.OnPreRender(e);
        }

        private string GetInitScript()
        {
            return GetAdvanceScript();
        }

        private string GetSimpleScript()
        {
            StringBuilder simpleScript = new StringBuilder();
            simpleScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");
            simpleScript.Append("tinyMCE.init({\n");
            simpleScript.Append("mode: \"textareas\",\n");
            simpleScript.Append("theme: \"simple\",\n");
            simpleScript.AppendFormat("editor_selector: \"{0}\",\n", EditorClass);
            simpleScript.AppendFormat("theme_advanced_toolbar_location: \"{0}\",\n", this.ThemeAdvancedToolbarLocation.ToString().ToLower());
            simpleScript.Append("});\n");
            simpleScript.Append("</script>");

            return simpleScript.ToString();
        }

        private string GetAdvanceScript()
        {
            StringBuilder advScript = new StringBuilder();
            advScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");
            advScript.Append("tinyMCE.init({\n");
            advScript.Append("mode: \"textareas\",\n");
            advScript.Append("theme: \"advanced\",\n");
            advScript.AppendFormat("editor_selector: \"{0}\",\n", _selectorClass);
            if (!string.IsNullOrEmpty(this.Plugins))
            {
                advScript.AppendFormat("plugins: \"{0}\",\n", this.Plugins);
            }
            
            //advScript.Append("document_base_url : \"http://localhost:10696\",\n");
            
            advScript.AppendFormat("relative_urls : {0},\n", this.RelativeUrls ? "true" : "false");            
            advScript.AppendFormat("remove_script_host : {0},\n", this.RemoveScriptHost? "true" : "false");

            if (!string.IsNullOrEmpty(this.SpellCheckerRpcUrl))
            {
                advScript.AppendFormat("spellchecker_rpc_url: \"{0}\",\n", Page.ResolveUrl(this.SpellCheckerRpcUrl));
                advScript.Append("language: \"en\",\n");
                advScript.Append("spellchecker_languages:\"+English=en,French=fr,Spanish=es\",\n");
            }

            switch (this.Layout)
            {
                case TextEditorLayout.Default:
                    advScript.AppendFormat("theme_advanced_buttons1: \"{0}\",\n", ButtonSets.DefaultButtons1);
                    advScript.AppendFormat("theme_advanced_buttons2: \"{0}\",\n", ButtonSets.DefaultButtons2);
                    advScript.AppendFormat("theme_advanced_buttons3: \"{0}\",\n", ButtonSets.DefaultButtons2);
                    advScript.AppendFormat("theme_advanced_buttons4:\"{0}\",\n", ButtonSets.DefaultButtons4);
                    break;
                case TextEditorLayout.WordPress:
                    advScript.AppendFormat("theme_advanced_buttons1: \"{0}\",\n", ButtonSets.WPAdvanceButtons1);
                    advScript.AppendFormat("theme_advanced_buttons2: \"{0}\",\n", ButtonSets.WPAdvanceButtons2);
                    advScript.AppendFormat("theme_advanced_buttons3: \"{0}\",\n", ButtonSets.WPAdvanceButtons3);
                    advScript.AppendFormat("theme_advanced_buttons4:\"{0}\",\n", ButtonSets.WPAdvanceButtons4);
                    break;
                case TextEditorLayout.Custom:
                    break;
                default:
                    goto case TextEditorLayout.Default;
            }
            
            advScript.AppendFormat("theme_advanced_toolbar_location: \"{0}\",\n", this.ThemeAdvancedToolbarLocation.ToString().ToLower());
            advScript.AppendFormat("theme_advanced_toolbar_align: \"{0}\",\n", this.ThemeAdvancedToolbarAlign.ToString().ToLower());
            advScript.AppendFormat("theme_advanced_statusbar_location : \"{0}\",\n", this.ThemeAdvancedStatusbarLocation.ToString().ToLower());
            advScript.AppendFormat("theme_advanced_resizing: {0},\n", this.ThemeAdvancedResizing ? "true" : "false");
            advScript.Append(@"theme_advanced_font_sizes : ""8px,10px,12px,14px,18px,24px,36px"",");
            advScript.AppendFormat("skin: \"{0}\",\n", this.LayoutSkin.ToString().ToLower());
            advScript.AppendFormat("skin_variant: \"{0}\",\n", this.SkinVariant.ToString().ToLower());
            if (!string.IsNullOrEmpty(this.ContentCssUrl))
                advScript.AppendFormat("content_css: \"{0}\",\n", Page.ResolveUrl(this.ContentCssUrl));
            if (!string.IsNullOrEmpty(this.TemplateListUrl))
                advScript.AppendFormat("template_external_list_url: \"{0}\",\n", Page.ResolveUrl(this.TemplateListUrl));
            if (!string.IsNullOrEmpty(this.LinkListUrl))
                advScript.AppendFormat("external_link_list_url: \"{0}\",\n", Page.ResolveUrl(this.LinkListUrl));
            if (!string.IsNullOrEmpty(this.ImageListUrl))
                advScript.AppendFormat("external_image_list_url: \"{0}\",\n", Page.ResolveUrl(this.ImageListUrl));
            if (!string.IsNullOrEmpty(this.MediaListUrl))
                advScript.AppendFormat("media_external_list_url: \"{0}\",\n", Page.ResolveUrl(this.MediaListUrl));
            advScript.AppendFormat("width: \"{0}\",\n", this.Width);
            advScript.AppendFormat("height: \"{0}\"\n", this.Height);
            advScript.Append("});\n");
            advScript.Append("</script>");
            return advScript.ToString();
        }

        public override TextBoxMode TextMode
        {
            get
            {
                return TextBoxMode.MultiLine;
            }
        }

        [DefaultValue(TextEditorLayout.Default)]
        public TextEditorLayout Layout
        {
            get
            {
                return ViewState["Layout"] == null ? TextEditorLayout.Default : (TextEditorLayout)ViewState["Layout"];
            }
            set
            {
                ViewState["Layout"] = value;
            }
        }

        public enum TextEditorLayout
        {
            Default = 1,
            WordPress = 2,
            Custom = 3
        }

        [DefaultValue(false)]
        public bool RelativeUrls
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool RemoveScriptHost
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string TemplateListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string LinkListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string ImageListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string MediaListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string ContentCssUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string SpellCheckerRpcUrl
        {
            get;
            set;
        }

        [Category("Appearance")]
        [DefaultValue(SkinVariants.Silver)]
        public SkinVariants SkinVariant
        {
            get
            {                
                return ViewState["SkinVariant"] == null ? SkinVariants.Silver : (SkinVariants)ViewState["SkinVariant"];
            }
            set
            {
                ViewState["SkinVariant"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(Skins.Default)]
        public Skins LayoutSkin
        {
            get
            {
                return ViewState["LayoutSkin"] == null ? Skins.Default : (Skins)ViewState["LayoutSkin"];
            }
            set
            {
                ViewState["LayoutSkin"] = value;
            }
        }

        [DefaultValue(true)]
        public bool ThemeAdvancedResizing
        {
            get;
            set;
        }

        [Category("Appearance")]
        [DefaultValue(StyleAttributes.Top)]
        public StyleAttributes ThemeAdvancedToolbarLocation
        {
            get
            {
                return ViewState["ThemeAdvancedToolbarLocation"] == null ? StyleAttributes.Top : (StyleAttributes)ViewState["ThemeAdvancedToolbarLocation"];
            }
            set
            {
                ViewState["ThemeAdvancedToolbarLocation"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(StyleAttributes.Left)]
        public StyleAttributes ThemeAdvancedToolbarAlign
        {
            get
            {
                return ViewState["ThemeAdvancedToolbarAlign"] == null ? StyleAttributes.Left : (StyleAttributes)ViewState["ThemeAdvancedToolbarAlign"];
            }
            set
            {
                ViewState["ThemeAdvancedToolbarAlign"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(StyleAttributes.Bottom)]
        public StyleAttributes ThemeAdvancedStatusbarLocation
        {
            get
            {                
                return ViewState["ThemeAdvancedStatusbarLocation"] == null ? StyleAttributes.Bottom : (StyleAttributes)ViewState["ThemeAdvancedStatusbarLocation"];
            }
            set
            {
                ViewState["ThemeAdvancedStatusbarLocation"] = value;
            }
        }

        [DefaultValue("")]
        [Editor("System.ComponentModel.Design.MultilineStringEditor", typeof(UITypeEditor))]
        public string Plugins
        {
            get;
            set;
        }

    }

    public enum SkinVariants
    {
         Silver = 1
    }

    public enum Skins
    {
         Default = 1,
         Cirkuit = 2,
         HighContrast = 3,
         O2K7 = 4
    }

    public enum StyleAttributes
    {
         Top = 1,
         Bottom = 2,
         Left = 3,
         Right = 4
    }

    public struct ButtonSets
    {
        public const string DefaultButtons1 = "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect";
        public const string DefaultButtons2 = "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor";
        public const string DefaultButtons3 = "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen";
        public const string DefaultButtons4 = "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage";
        public const string WPAdvanceButtons1 = "bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyfull,justifyright,|,link,unlink,|,spellchecker,fullscreen";
        public const string WPAdvanceButtons2 = "underline,forecolor,backcolor,styleprops,|,pastetext,pasteword,removeformat,|,charmap,|,outdent,indent,|,undo,redo,help";
        public const string WPAdvanceButtons3 = "formatselect,|,styleselect,|,fontsizeselect,|, cleanup,code,|,image,template,itechfilemgr ";
        public const string WPAdvanceButtons4 = "";
    }
}

