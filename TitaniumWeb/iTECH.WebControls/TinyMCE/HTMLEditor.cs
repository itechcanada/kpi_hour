﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;
using System.Text;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [ToolboxData("<{0}:HTMLEditor runat=server></{0}:HTMLEditor>")]
    public class HTMLEditor : System.Web.UI.WebControls.TextBox
    {
        string _selectorClass = "tinymce";

        protected override void OnPreRender(EventArgs e)
        {            
            if (!Page.ClientScript.IsStartupScriptRegistered("___HTML_EDITOR"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "___HTML_EDITOR", GetAdvanceScript());
            }

            if (!CssClass.Contains(_selectorClass)) //probably this is not the best way how to add the css class but I do not know any beter way
            {
                if (CssClass.Length > 0)
                {
                    CssClass += " ";
                }
                CssClass += _selectorClass;
            }
            base.OnPreRender(e);
        }

        private string GetAdvanceScript()
        {
            StringBuilder advScript = new StringBuilder();
            advScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");

            advScript.AppendFormat("$('textarea.{0}')", _selectorClass).Append(".tinymce({");
            // Location of TinyMCE script
            if (!string.IsNullOrEmpty(this.ScriptUrl))
                advScript.AppendFormat("script_url: \"{0}\",\n", Page.ResolveUrl(this.ScriptUrl));
            

            // General options
            advScript.Append("theme: \"advanced\",\n");
            if (!string.IsNullOrEmpty(this.Plugins))
            {
                advScript.AppendFormat("plugins: \"{0}\",\n", this.Plugins);
            }

            // Theme options
            advScript.AppendFormat("skin: \"{0}\",\n", this.LayoutSkin.ToString().ToLower());
            advScript.AppendFormat("skin_variant: \"{0}\",\n", this.SkinVariant.ToString().ToLower());
            advScript.AppendFormat("theme_advanced_buttons1: \"{0}\",\n", ButtonSets.WPAdvanceButtons1);
            advScript.AppendFormat("theme_advanced_buttons2: \"{0}\",\n", ButtonSets.WPAdvanceButtons2);
            advScript.AppendFormat("theme_advanced_buttons3: \"{0}\",\n", ButtonSets.WPAdvanceButtons3);
            advScript.AppendFormat("theme_advanced_buttons4:\"{0}\",\n", ButtonSets.WPAdvanceButtons4);
            advScript.AppendFormat("theme_advanced_toolbar_location: \"{0}\",\n", "top");
            advScript.AppendFormat("theme_advanced_toolbar_align: \"{0}\",\n", "left");
            advScript.AppendFormat("theme_advanced_statusbar_location : \"{0}\",\n", "bottom");
            advScript.AppendFormat("theme_advanced_resizing: {0},\n", this.ThemeAdvancedResizing ? "true" : "false");
            advScript.Append(@"theme_advanced_font_sizes : ""8px,10px,12px,14px,18px,24px,36px"",");
            
            // Example content CSS (should be your site CSS)
            if (!string.IsNullOrEmpty(this.ContentCssUrl))
                advScript.AppendFormat("content_css: \"{0}\",\n", Page.ResolveUrl(this.ContentCssUrl));

            // Drop lists for link/image/media/template dialogs
            if (!string.IsNullOrEmpty(this.TemplateListUrl))
                advScript.AppendFormat("template_external_list_url: \"{0}\",\n", Page.ResolveUrl(this.TemplateListUrl));
            if (!string.IsNullOrEmpty(this.LinkListUrl))
                advScript.AppendFormat("external_link_list_url: \"{0}\",\n", Page.ResolveUrl(this.LinkListUrl));
            if (!string.IsNullOrEmpty(this.ImageListUrl))
                advScript.AppendFormat("external_image_list_url: \"{0}\",\n", Page.ResolveUrl(this.ImageListUrl));
            if (!string.IsNullOrEmpty(this.MediaListUrl))
                advScript.AppendFormat("media_external_list_url: \"{0}\",\n", Page.ResolveUrl(this.MediaListUrl));

            advScript.AppendFormat("relative_urls : {0},\n", this.RelativeUrls ? "true" : "false");
            advScript.AppendFormat("remove_script_host : {0},\n", this.RemoveScriptHost ? "true" : "false");

            if (!string.IsNullOrEmpty(this.SpellCheckerRpcUrl))
            {
                advScript.AppendFormat("spellchecker_rpc_url: \"{0}\",\n", Page.ResolveUrl(this.SpellCheckerRpcUrl));
                advScript.Append("language: \"en\",\n");
                advScript.Append("spellchecker_languages:\"+English=en,French=fr,Spanish=es\",\n");
            }


            advScript.Append("});\n");
            advScript.Append("</script>");                                   
            
            return advScript.ToString();
        }

        public override TextBoxMode TextMode
        {
            get
            {
                return TextBoxMode.MultiLine;
            }
        }

        [Category("Appearance")]
        [DefaultValue(Skins.Default)]
        public Skins LayoutSkin
        {
            get
            {
                return ViewState["LayoutSkin"] == null ? Skins.Default : (Skins)ViewState["LayoutSkin"];
            }
            set
            {
                ViewState["LayoutSkin"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue(SkinVariants.Silver)]
        public SkinVariants SkinVariant
        {
            get
            {
                return ViewState["SkinVariant"] == null ? SkinVariants.Silver : (SkinVariants)ViewState["SkinVariant"];
            }
            set
            {
                ViewState["SkinVariant"] = value;
            }
        }

        [DefaultValue("")]
        [Editor("System.ComponentModel.Design.MultilineStringEditor", typeof(UITypeEditor))]
        public string Plugins
        {
            get;
            set;
        }

        [DefaultValue(true)]
        public bool ThemeAdvancedResizing
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool RelativeUrls
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool RemoveScriptHost
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string TemplateListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string LinkListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string ImageListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string MediaListUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string ContentCssUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string SpellCheckerRpcUrl
        {
            get;
            set;
        }

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string ScriptUrl
        {
            get;
            set;
        }
    }
}
