/*
 * $Id: SpellCheckerModule.cs,v 1.1 2012/06/04 06:19:50 cvslive Exp $
 *
 * @author Moxiecode
 * @copyright Copyright � 2004-2007, Moxiecode Systems AB, All rights reserved.
 */

using System;
using System.Web;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using iTECH.WebControls.Utils;

namespace iTECH.WebControls.TinyMCE.SpellChecker
{
	/// <summary>
	/// Description of HttpHandler.
	/// </summary>
    public class SpellCheckerModule : iTECH.WebControls.TinyMCE.IModule
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		public void ProcessRequest(HttpContext context) {
			HttpRequest request = context.Request;
			HttpResponse response = context.Response;
			JSONRpcCall call = JSON.ParseRPC(new System.IO.StreamReader(request.InputStream));
			object result = null;
			ISpellChecker spellchecker = new GoogleSpellChecker();

			switch (call.Method) {
				case "checkWords":
					result = spellchecker.CheckWords((string) call.Args[0], (string[]) ((ArrayList) call.Args[1]).ToArray(typeof(string)));
					break;

				case "getSuggestions":
					result = spellchecker.GetSuggestions((string) call.Args[0], (string) call.Args[1]);
					break;
			}

			// Serialize RPC output
			JSON.SerializeRPC(
				call.Id,
				null,
				result,
				response.OutputStream
			);
		}
	}
}
