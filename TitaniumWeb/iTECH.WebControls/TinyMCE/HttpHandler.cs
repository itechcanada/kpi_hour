﻿using System;
using System.Web;

namespace iTECH.WebControls.TinyMCE
{
    public class HttpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            IModule module = null;

            // This might include some form of dynamic class loading later
            switch (request.QueryString["module"])
            {
                case "GzipModule":
                    module = new iTECH.WebControls.TinyMCE.Compression.GzipModule();
                    break;

                case "SpellChecker":
                    module = new iTECH.WebControls.TinyMCE.SpellChecker.SpellCheckerModule();
                    break;
            }

            if (module != null)
                module.ProcessRequest(context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
