﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;
using System.Text;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [ToolboxData("<{0}:IframeDialog runat=server></{0}:IframeDialog>")]
    public class IframeDialog : Literal
    {
        public IframeDialog()
        {
            this.AutoOpen = false;
            this.IsModal = true;
            this.OnCloseClientFunction = string.Empty;
            this.Resizable = false;
            this.Dragable = true;
            this.TriggerSelectorMode = JquerySelectorMode.ID;
            this.Title = string.Empty;
            this.TriggerControlID = string.Empty;
            this.TriggerSelectorClass = string.Empty;
            this.Url = string.Empty;
            this.UrlSelector = DialogUrlSource.FromProperty;
        }

        protected override void OnPreRender(EventArgs e)
        {
            string scriptKey = this.TriggerSelectorMode == JquerySelectorMode.ID ? this.ClientID : this.TriggerSelectorClass;

            if (!Page.ClientScript.IsStartupScriptRegistered(scriptKey))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), scriptKey, GetIframeDialogScript());
            }

            base.OnPreRender(e);
        }

        private string GetIframeDialogScript()
        {
            string triggerSelector = "#tiggerID";
            string url = string.Empty;
            string title = string.Empty;
            title = this.Title; //.Replace(@"'", @"\'");
            title = string.Format(@"""{0}""", title);

            StringBuilder initScript = new StringBuilder();

            initScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");
            initScript.Append(" $(document).ready(function () {");

            switch (this.TriggerSelectorMode)
            {
                case JquerySelectorMode.ID:
                    Control ctl = Parent.FindControl(TriggerControlID);
                    string triggerControlID = ctl == null ? this.TriggerControlID : ctl.ClientID;
                    triggerSelector = string.Format("#{0}", triggerControlID);
                    break;
                case JquerySelectorMode.Class:
                    triggerSelector = string.Format(".{0}", this.TriggerSelectorClass);
                    break;
            }
            initScript.AppendFormat(@"$(""{0}"")", triggerSelector);

            initScript.Append(@".live(""click"",function (e) {");
            initScript.Append("var $dialog = jQuery.FrameDialog.create({");            
            
            if (this.UrlSelector == DialogUrlSource.FromProperty && !string.IsNullOrEmpty(this.Url))
            {
                url = Page.ResolveUrl(this.Url);//.Replace(@"'", @"\'");
                url = string.Format(@"""{0}""", url);
            }
            if (this.UrlSelector == DialogUrlSource.TriggerControlHrefAttribute)
            {
                url = "this.href";
            }
            initScript.AppendFormat(@"url: {0},", url);
            initScript.AppendFormat(@"title: ($(this).attr(""title"")) ? $(this).attr(""title"") : __DialogBoxTitle__ ").Replace("__DialogBoxTitle__", title).Append(",");
            initScript.Append(@"loadingClass: ""loading-image"",");
            if (!string.IsNullOrEmpty(this.OnCloseClientFunction))
                initScript.AppendFormat(" close: {0},", this.OnCloseClientFunction);
            initScript.AppendFormat(" modal: {0},", this.IsModal ? "true" : "false");
            initScript.AppendFormat(@" width: ""{0}"",", this.Width);
            initScript.AppendFormat(@" height: ""{0}"",", this.Height);
            initScript.AppendFormat(" autoOpen: {0}", "false");
            initScript.Append(@"});");
            initScript.Append(@"$dialog.dialog('open');");
            initScript.Append(@"return false;");
            initScript.Append(@"});}); ");
            initScript.Append("\n</script>");

            return initScript.ToString();
        }


        /// <summary>
        /// Trigger element id.
        /// </summary>
        [Category("Accessibility")]
        [Themeable(false)]
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
        [DefaultValue("")]
        [Description("Trigger element id.")]
        public string TriggerControlID { get; set; }

        [Category("Accessibility")]
        [CssClassProperty]
        [DefaultValue("")]
        [Description("Trigger selector class to be used as selector within \n Jquery function while \n TriggerSelectorMode property is set to Class.")]
        public string TriggerSelectorClass { get; set; }

        [Category("Accessibility")]
        [DefaultValue(JquerySelectorMode.ID)]
        public JquerySelectorMode TriggerSelectorMode
        {
            get
            {
                return ViewState["TriggerSelectorMode"] == null ? JquerySelectorMode.ID : (JquerySelectorMode)ViewState["TriggerSelectorMode"];
            }
            set
            {
                ViewState["TriggerSelectorMode"] = value;
            }
        }


        [Category("Accessibility")]
        [DefaultValue(DialogUrlSource.FromProperty)]
        public DialogUrlSource UrlSelector
        {
            get
            {
                return ViewState["UrlSelector"] == null ? DialogUrlSource.FromProperty : (DialogUrlSource)ViewState["UrlSelector"];
            }
            set
            {
                ViewState["UrlSelector"] = value;
            }
        }

        [Category("Accessibility")]
        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string Url { get; set; }

        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Title { get; set; }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int Height { get; set; }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int Width { get; set; }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool Dragable { get; set; }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool Resizable { get; set; }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool IsModal { get; set; }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool AutoOpen { get; set; }

        [Category("Accessibility")]
        [DefaultValue("")]
        public string OnCloseClientFunction { get; set; }
    }
}
