﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.WebControls
{
    public enum JquerySelectorMode
    {
        ID,
        Class
    }

    public enum UIDialogMode
    {
        Inline,
        Iframe
    }

    public enum UIDialogTriggerRenderMode
    {
        HyperLink,
        Button,
        ScriptOnly
    }

    public enum DialogUrlSource
    {
        FromProperty,
        TriggerControlHrefAttribute
    }

    public enum MessageType
    {
        None,
        Critical,        
        Failure,
        Information,
        Success
    }
}
