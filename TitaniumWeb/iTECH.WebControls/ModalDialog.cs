﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;
using System.Text;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [ToolboxData("<{0}:ModalDialog runat=server></{0}:ModalDialog>")]
    public class ModalDialog : Literal
    {

        public ModalDialog()
        {
            this.AutoOpen = false;
            this.IsModal = true;
            this.OnCloseClientFunction = string.Empty;
            this.Resizable = true;
            this.TriggerSelectorMode = JquerySelectorMode.ID;
            this.InlinePanelSelectorMode = JquerySelectorMode.ID;
            this.Title = string.Empty;
            this.TriggerControlID = string.Empty;
            this.TriggerSelectorClass = string.Empty;
            this.Url = string.Empty;
            this.UrlSelector = DialogUrlSource.FromProperty;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(this.ClientID))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, GetInlineDialogScript());
            }

            base.OnPreRender(e);
        }

        

        private string GetInlineDialogScript()
        {
            string dialogSelector = "#dialog";
            StringBuilder initScript = new StringBuilder();
            initScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");
            initScript.Append(" $(document).ready(function () {");
            switch (this.InlinePanelSelectorMode)
            {
                case JquerySelectorMode.ID:
                    Control pnl = Parent.FindControl(this.InlinePanleID);
                    string panleID = "pnlID";
                    if (pnl != null)
                    {
                        panleID = pnl.ClientID;
                    }
                    initScript.AppendFormat(" $('#{0}')", panleID);
                    dialogSelector = "#" + panleID;
                    break;
                case JquerySelectorMode.Class:
                    initScript.AppendFormat(" $('.{0}')", this.InlinePanelSelectorClass);
                    dialogSelector = "." + this.InlinePanelSelectorClass;
                    break;
                default: goto case JquerySelectorMode.ID;
            }
            initScript.Append(".dialog({");
            initScript.AppendFormat(" autoOpen: {0},", this.AutoOpen ? "true" : "false");
            if (!string.IsNullOrEmpty(this.Url))
                initScript.AppendFormat(" url: '{0}',", Page.ResolveUrl(this.Url));
            if (!string.IsNullOrEmpty(this.Title))
                initScript.AppendFormat(" title: '{0}',", this.Title);
            if (this.Height > 0)
                initScript.AppendFormat(" height: '{0}',", this.Height);
            if (this.Height > 0)
                initScript.AppendFormat(" width: '{0}',", this.Width);
            initScript.AppendFormat(" draggable: {0},", this.Dragable ? "true" : "false");
            initScript.AppendFormat(" resizable: {0},", this.Resizable ? "true" : "false");
            if (!string.IsNullOrEmpty(this.OnCloseClientFunction))
                initScript.AppendFormat(" close: {0},", this.OnCloseClientFunction);
            initScript.AppendFormat(" modal: {0}", this.IsModal ? "true" : "false");
            initScript.Append(" });");

            switch (this.TriggerSelectorMode)
            {
                case JquerySelectorMode.ID:
                    Control ctl = Parent.FindControl(TriggerControlID);
                    string triggerControlID = "ctrlID";
                    if (ctl != null)
                    {
                        triggerControlID = ctl.ClientID;
                    }
                    initScript.AppendFormat(" $('#{0}')", triggerControlID);
                    break;
                case JquerySelectorMode.Class:
                    initScript.AppendFormat(" $('.{0}')", this.TriggerSelectorClass);
                    break;
                default: goto case JquerySelectorMode.ID;
            }
            initScript.Append(".live('click', function () {");
            initScript.AppendFormat("$('{0}').dialog('open');", dialogSelector);
            initScript.Append(" return false;");
            initScript.Append(" });");
            initScript.Append(" });");
            initScript.Append("\n</script>");

            return initScript.ToString();
        }

        /// <summary>
        /// Trigger element id.
        /// </summary>
        [Category("Accessibility")]
        [Themeable(false)]
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
        [DefaultValue("")]
        [Description("Trigger element id.")]
        public string TriggerControlID { get; set; }

        [Category("Accessibility")]
        [CssClassProperty]
        [DefaultValue("")]
        [Description("Trigger selector class to be used as selector within \n Jquery function while \n TriggerSelectorMode property is set to Class.")]
        public string TriggerSelectorClass { get; set; }

        [Category("Accessibility")]
        [CssClassProperty]
        [DefaultValue("")]
        public string InlinePanelSelectorClass { get; set; }


        [Category("Accessibility")]
        [DefaultValue(JquerySelectorMode.ID)]
        public JquerySelectorMode TriggerSelectorMode 
        {
            get
            {
                return ViewState["TriggerSelectorMode"] == null ? JquerySelectorMode.ID : (JquerySelectorMode)ViewState["TriggerSelectorMode"];
            }
            set
            {
                ViewState["TriggerSelectorMode"] = value;
            }
        }

        [Category("Accessibility")]
        [DefaultValue(JquerySelectorMode.ID)]
        public JquerySelectorMode InlinePanelSelectorMode
        {
            get
            {
                return ViewState["InlinePanelSelectorMode"] == null ? JquerySelectorMode.ID : (JquerySelectorMode)ViewState["InlinePanelSelectorMode"];
            }
            set
            {
                ViewState["InlinePanelSelectorMode"] = value;
            }
        }

        [Category("Accessibility")]
        [DefaultValue(DialogUrlSource.FromProperty)]
        public DialogUrlSource UrlSelector
        {
            get
            {
                return ViewState["UrlSelector"] == null ? DialogUrlSource.FromProperty : (DialogUrlSource)ViewState["UrlSelector"];
            }
            set
            {
                ViewState["UrlSelector"] = value;
            }
        }

        [Category("Accessibility")]
        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string Url { get; set; }

        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Title { get; set; }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int Height { get; set; }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int Width { get; set; }

        [DefaultValue(false)]
        [Category("Behavior")]        
        public bool Dragable { get; set; }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool Resizable { get; set; }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool IsModal { get; set; }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool AutoOpen { get; set; }

        [Category("Accessibility")]
        [DefaultValue("")]
        public string OnCloseClientFunction { get; set; }

        [Category("Accessibility")]
        [Themeable(false)]
        [IDReferenceProperty]
        [TypeConverter(typeof(AssociatedControlConverter))]
        [DefaultValue("")]
        public string InlinePanleID { get; set; }  
    }    
}
