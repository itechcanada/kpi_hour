﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MessageCenter runat=server></{0}:MessageCenter>")]
    public class MessageCenter : Panel
    {
        #region Member Variables

        //protected Panel pnlMessageCenter = new Panel();
        protected Panel pnlValidation;

        //protected ValidationSummary validationSummary;

        protected Panel pnlSuccess;
        protected Label lblSuccessMessage;

        protected Panel pnlInformation;
        protected Label lblInformationMessage;

        protected Panel pnlFailure;
        protected Label lblFailureMessage;

        protected Panel pnlCritical;
        protected Label lblCriticalMessage;

        #endregion

        #region Constructors

        public MessageCenter()
        {
            base.EnableViewState = false;
            //this.ValidationGroup = string.Empty;
        }

        #endregion

        #region Methods

        #region Public

        protected override void OnInit(EventArgs e)
        {
            ResetPanelsVisibility();
            base.OnInit(e);
        }

        /// <summary>
        /// Displays the information message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplayInformationMessage(string message)
        {
            EnsureChildControls();
            //ResetPanelsVisibility();
            pnlInformation.Visible = true;
            lblInformationMessage.Text = message;
        }

        /// <summary>
        /// Displays the success message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplaySuccessMessage(string message)
        {
            EnsureChildControls();
            //ResetPanelsVisibility();
            pnlSuccess.Visible = true;
            lblSuccessMessage.Text = message;
        }

        /// <summary>
        /// Displays the failure message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplayFailureMessage(string message)
        {
            EnsureChildControls();
            //ResetPanelsVisibility();
            pnlFailure.Visible = true;
            lblFailureMessage.Text = message;
        }

        /// <summary>
        /// Displays the critical message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplayCriticalMessage(string message)
        {
            EnsureChildControls();
            //ResetPanelsVisibility();
            pnlCritical.Visible = true;
            lblCriticalMessage.Text = message;
        }


        /// <summary>
        /// Resets the panels visibility.
        /// </summary>
        public void ResetPanelsVisibility()
        {
            EnsureChildControls();
            pnlValidation.Visible = true;
            pnlInformation.Visible = false;
            pnlSuccess.Visible = false;
            pnlFailure.Visible = false;
            pnlCritical.Visible = false;
        }



        #endregion

        #region Protected

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();

            pnlValidation = new Panel();
            //pnlValidation.Controls.Add(new LiteralControl("&nbsp;"));
            /*if (!IsAsyncRequest)
            {
                validationSummary = new ValidationSummary();
                validationSummary.DisplayMode = ValidationSummaryDisplayMode.BulletList;
                validationSummary.CssClass = "validationSummary";
                validationSummary.ValidationGroup = this.ValidationGroup;
                validationSummary.ShowMessageBox = true;
                validationSummary.ShowSummary = false;
                pnlValidation.Controls.Add(validationSummary);
            }*/

            pnlSuccess = new Panel();
            pnlSuccess.Visible = false;
            pnlSuccess.CssClass = "messagecenterSuccess ";
            lblSuccessMessage = new Label();
            pnlSuccess.Controls.Add(new LiteralControl("<div class=\"messageCenterIcon\">"));
            pnlSuccess.Controls.Add(new LiteralControl("</div>"));
            pnlSuccess.Controls.Add(lblSuccessMessage);
            pnlSuccess.Controls.Add(new LiteralControl("<div style=\"clear:both;\"></div>"));

            pnlFailure = new Panel();
            pnlFailure.Visible = false;
            pnlFailure.CssClass = "messagecenterFailure";
            pnlFailure.Controls.Add(new LiteralControl("<div class=\"messageCenterIcon\">"));
            pnlFailure.Controls.Add(new LiteralControl("</div>"));
            lblFailureMessage = new Label();
            pnlFailure.Controls.Add(lblFailureMessage);
            pnlFailure.Controls.Add(new LiteralControl("<div style=\"clear:both;\"></div>"));

            pnlInformation = new Panel();
            pnlInformation.Visible = false;
            pnlInformation.CssClass = "messagecenterInformation ";
            pnlInformation.Controls.Add(new LiteralControl("<div class=\"messageCenterIcon\">"));
            pnlInformation.Controls.Add(new LiteralControl("</div>"));
            lblInformationMessage = new Label();
            pnlInformation.Controls.Add(lblInformationMessage);
            pnlInformation.Controls.Add(new LiteralControl("<div style=\"clear:both;\"></div>"));

            pnlCritical = new Panel();
            pnlCritical.Visible = false;
            pnlCritical.CssClass = "messagecenterCritical ";
            pnlCritical.Controls.Add(new LiteralControl("<div class=\"messageCenterIcon\">"));
            pnlCritical.Controls.Add(new LiteralControl("</div>"));
            lblCriticalMessage = new Label();
            pnlCritical.Controls.Add(lblCriticalMessage);
            pnlCritical.Controls.Add(new LiteralControl("<div style=\"clear:both;\"></div>"));

            this.Controls.Add(pnlInformation);
            this.Controls.Add(pnlSuccess);
            this.Controls.Add(pnlFailure);
            this.Controls.Add(pnlCritical);
            this.Controls.Add(pnlValidation);

            //this.Controls.Add(this);
            base.CreateChildControls();
        }

        private bool IsAsyncRequest
        {
            get
            {
                return Page == null;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!IsAsyncRequest && !string.IsNullOrEmpty(this.AsyncMessageUrl))
            {
                StringBuilder asynchScript = new StringBuilder();

                asynchScript.Append(@"<script type=""text/javascript"">");
                asynchScript.Append(@"function getGlobalMessage(){");
                asynchScript.Append(@"$.ajax({");
                asynchScript.AppendFormat(@"url: ""{0}"",", Page.ResolveUrl(this.AsyncMessageUrl));
                asynchScript.Append(@"global: false,");
                asynchScript.Append(@"type: ""POST"",");
                asynchScript.Append(@"dataType: ""html"",");
                asynchScript.Append(@"async: false,");
                asynchScript.Append(@"success: function (msg) {");
                asynchScript.Append(@"if (msg != 404) {");
                asynchScript.Append(@"$.alert(msg);");
                asynchScript.Append(@"}}});}");
                asynchScript.Append(@"$(document).ready(function () {");
                asynchScript.Append(@"getGlobalMessage();");
                asynchScript.Append(@"});");
                asynchScript.Append(@"</script>");
                if (!Page.ClientScript.IsClientScriptBlockRegistered("MessageCenter"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "MessageCenter", asynchScript.ToString());
                }
            }
        }

        /// <summary>
        /// Renders the control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"></see> object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {            
            base.Render(writer);
        }

        #endregion

        #endregion

        #region Properties



        /// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
        public override ControlCollection Controls
        {
            get
            {
                EnsureChildControls();
                return base.Controls;
            }
        }
        

        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string AsyncMessageUrl
        {
            get;
            set;
        }

        public override bool EnableViewState
        {
            get
            {
                return base.EnableViewState;
            }
        }

        //[DefaultValue("")]
        //public string ValidationGroup
        //{
        //    get;
        //    set;
        //}

        #endregion

    }

    public class MessageState
    {
        private const string _library_message_state_key = "_library_message_state";
        private MessageType _messageType;
        private string _message;

        public MessageType MessageType
        {
            get { return _messageType; }
            set { _messageType = value; }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public static MessageState Current {
            get {
                if (HttpContext.Current.Session[_library_message_state_key] != null)
                {
                    return (MessageState)HttpContext.Current.Session[_library_message_state_key];
                }
                else
                {
                    HttpContext.Current.Session[_library_message_state_key] = new MessageState();
                    return (MessageState)HttpContext.Current.Session[_library_message_state_key];
                }
            }
            internal set {
                if (value != null)
                {
                    HttpContext.Current.Session[_library_message_state_key] = value;
                }
                else
                {
                    HttpContext.Current.Session.Remove(_library_message_state_key);
                }
            }
        }

        public static void SetGlobalMessage(MessageType msgType, string message)
        {
            MessageState state = MessageState.Current;
            state.MessageType = msgType;
            state.Message = message;
            MessageState.Current = state;
        }

        public static void Clear() {
            MessageState.Current.Message = string.Empty;
        }

        public static bool HasMessage {
            get {
                return !string.IsNullOrEmpty(MessageState.Current.Message);
            }
        }
    }
}
