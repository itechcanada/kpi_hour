﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;
using System.Text;

[assembly: TagPrefix("iTECH.WebControls", "iCtrl")]
namespace iTECH.WebControls
{
    [ToolboxData(@"<{0}:DialogControl runat=""server"" Text=""Label""></{0}:DialogControl>")]
    [DefaultProperty("Text")]
    [PersistChildren(false)]
    [ParseChildren(true, "Text")]    
    public class DialogControl : WebControl, ITextControl
    {
        public DialogControl()
        {            
            this.DisplayMode = UIDialogTriggerRenderMode.ScriptOnly;
            this.DialogMode = UIDialogMode.Iframe;
            this.Selector = JquerySelectorMode.ID;
            this.Url = string.Empty;
        }

        protected override void OnPreRender(EventArgs e)
        {
            string scriptKey = this.ClientID;

            if (!Page.ClientScript.IsStartupScriptRegistered(scriptKey))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), scriptKey, GetIframeDialogScript());
            }

            base.OnPreRender(e);
        }


        private string GetIframeDialogScript()
        {            
            string url = string.Empty;            

            StringBuilder initScript = new StringBuilder();

            initScript.Append("<script language=\"javascript\" type=\"text/javascript\">\n");
            initScript.Append(" $(document).ready(function () {");            
            
            initScript.AppendFormat(@"$(""{0}"")", this.GetJquerySelector());

            initScript.Append(@".live(""click"",function (e) {");
            initScript.Append("var $dialog = jQuery.FrameDialog.create({");            
            
            if (!string.IsNullOrEmpty(this.Url))
            {
                url = Page.ResolveUrl(this.Url);//.Replace(@"'", @"\'");
                url = string.Format(@"""{0}""", url);
                initScript.Append(@"url: $(this).attr(""dUrl""),");
            }

            initScript.Append(@"title: $(this).attr(""title""),");                        
            if (!string.IsNullOrEmpty(this.OnCloseClientFunction))
                initScript.AppendFormat(" close: {0},", this.Attributes["dOnCloseClientFunction"]);
            if (this.IsModal == true)
            {
                initScript.AppendFormat(" modal: true,");   
            }
            if (this.DialogWidth != 250 && !string.IsNullOrEmpty(this.Attributes["dWidth"]))
            {
                initScript.AppendFormat(@" width: ""{0}"",", this.Attributes["dWidth"]);
            }
            if (this.DialogWidth != 250 && !string.IsNullOrEmpty(this.Attributes["dHeight"]))
            {
                initScript.AppendFormat(@" height: ""{0}"",", this.Attributes["dHeight"]);
            }
            if (!string.IsNullOrEmpty(this.Attributes["dCloseOnEscape"]))
            {
                initScript.AppendFormat(@" closeOnEscape: {0},", this.Attributes["dCloseOnEscape"]);
            }
            if (!string.IsNullOrEmpty(this.Attributes["dAutoOpen"]))
            {
                initScript.AppendFormat(@" autoOpen: {0},", this.Attributes["dAutoOpen"]);
            }             
            initScript.Append(@"loadingClass: ""loading-image""");
            initScript.Append(@"});");
            initScript.Append(@"$dialog.dialog('open');");
            initScript.Append(@"return false;");
            initScript.Append(@"});}); ");
            initScript.Append("\n</script>");            

            return initScript.ToString();
        }

        
        [Category("Accessibility")]
        [CssClassProperty]
        [DefaultValue("")]
        [Description("Trigger selector class to be used as selector within \n Jquery function while.")]
        public string TriggerClass
        {
            get
            {
                if (ViewState["TriggerClass"] == null)
                {
                    return string.Empty;
                }
                return (string)ViewState["TriggerClass"];                
            }
            set
            {
                ViewState["TriggerClass"] = value;
            }
        }


        [Description("DialogTriggerDisplayMode")]
        [Category("Behavior")]
        public UIDialogTriggerRenderMode DisplayMode
        {
            get
            {
                if (ViewState["DisplayMode"] == null)
                {
                    ViewState["DisplayMode"] = UIDialogTriggerRenderMode.ScriptOnly;
                }
                return (UIDialogTriggerRenderMode)ViewState["DisplayMode"];
            }
            set
            {
                ViewState["DisplayMode"] = value;
            }
        }

        [Description("DialogTriggerDisplayMode")]
        [Category("Behavior")]
        public JquerySelectorMode Selector
        {
            get
            {
                if (ViewState["Selector"] == null)
                {
                    ViewState["Selector"] = JquerySelectorMode.ID;
                }
                return (JquerySelectorMode)ViewState["Selector"];
            }
            set
            {
                ViewState["Selector"] = value;
            }
        }

        [Description("DialogMode")]
        [Category("Behavior")]
        public UIDialogMode DialogMode
        {
            get
            {
                if (ViewState["DialogMode"] == null)
                {
                    ViewState["DialogMode"] = UIDialogMode.Iframe;
                }
                return (UIDialogMode)ViewState["DialogMode"];                
            }
            set
            {
                ViewState["DialogMode"] = value;
            }
        }

        [Category("Accessibility")]
        [DefaultValue("")]
        [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [UrlProperty]
        public string Url
        {
            get
            {
                return this.Attributes["dUrl"];
            }
            set
            {
                this.Attributes["dUrl"] = value;
            }
        }
        
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [PersistenceMode(PersistenceMode.Attribute)]
        public string DialogTitle
        {
            get
            {                
                return this.ToolTip;
            }
            set
            {                
                this.ToolTip = value;
            }
        }
        
        [Category("Appearance")]
        [Localizable(true)]
        [DefaultValue("")]
        [PersistenceMode(PersistenceMode.EncodedInnerDefaultProperty)]
        public virtual string Text
        {
            get
            {
                if (ViewState["Text"] == null)
                {
                    return string.Empty;
                }
                return (string)ViewState["Text"];                
            }
            set
            {
                ViewState["Text"] = value;
            }
        }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int DialogHeight
        {             
            get
            {               
                int h = 250;
                int.TryParse(this.Attributes["dHeight"], out h);
                return h <= 0 ? 250 : h;
            }
            set
            {
                this.Attributes["dHeight"] = value.ToString();               
            }
        }

        [Category("Layout")]
        [DefaultValue(typeof(int), "")]
        public int DialogWidth
        {
            get
            {
                int h = 250;
                int.TryParse(this.Attributes["dWidht"], out h);
                return h <= 0 ? 250 : h;
            }
            set
            {
                this.Attributes["dWidht"] = value.ToString();                
            }
        }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool Dragable
        {           
            get
            {               
                return this.Attributes["dDragable"] == "true";
            }
            set
            {         
                this.Attributes["dDragable"] =  value ? "true" : "false";                
            }
        }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool Resizable
        {
             get
            {               
                return this.Attributes["dResizable"] == "true";
            }
            set
            {         
                this.Attributes["dResizable"] =  value ? "true" : "false";                
            }
        }

        [DefaultValue(true)]
        [Category("Behavior")]
        public bool IsModal
        {
            get
            {
                return this.Attributes["dIsModal"] == "true";
            }
            set
            {
                this.Attributes["dIsModal"] = value ? "true" : "false";
            }
        }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool AutoOpen
        {
            get
            {
                return this.Attributes["dAutoOpen"] == "true";
            }
            set
            {
                this.Attributes["dAutoOpen"] = value ? "true" : "false";
            }
        }

        [DefaultValue(false)]
        [Category("Behavior")]
        public bool CloseOnEscape
        {
            get
            {
                return this.Attributes["dCloseOnEscape"] == "true";
            }
            set
            {
                this.Attributes["dCloseOnEscape"] = value ? "true" : "false";
            }
        }

        [Category("Accessibility")]
        [DefaultValue("")]
        public string OnCloseClientFunction {
            get
            {
                return this.Attributes["dOnCloseClientFunction"];
            }
            set
            {
                this.Attributes["dOnCloseClientFunction"] = value;
            }            
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                switch (this.DisplayMode)
                {
                    case UIDialogTriggerRenderMode.HyperLink:
                        return HtmlTextWriterTag.A;
                    case UIDialogTriggerRenderMode.Button:
                        return HtmlTextWriterTag.Input;
                    default:
                        return HtmlTextWriterTag.A;
                }                
            }
        }


        protected override void Render(HtmlTextWriter writer)
        {
            //AddAttributesToRender(writer);
            switch (this.DisplayMode)
            {
                case UIDialogTriggerRenderMode.HyperLink:
                    this.Attributes["href"] = Page.ResolveUrl(this.Url);
                    break;
                case UIDialogTriggerRenderMode.Button:
                    this.Attributes["type"] = "button";
                    this.Attributes["value"] = this.Text;
                    break;
                case UIDialogTriggerRenderMode.ScriptOnly:
                    this.Style[HtmlTextWriterStyle.Display] = "none";
                    break;
                default:
                    this.Style[HtmlTextWriterStyle.Display] = "none";
                    break;
            }
            base.Render(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (this.DisplayMode != UIDialogTriggerRenderMode.Button)
            {
                writer.WriteEncodedText(this.Text);
            }
            else
            {
                writer.WriteEncodedText("");
            }
        }

        private void SetDialogAttribute(string key, string value)
        {
            this.Attributes[key] = value;            
        }

        private string GetDialogAttribute(string key)
        {
            return this.Attributes[key];            
        }

        private string GetJquerySelector()
        {            
            if (this.Selector == JquerySelectorMode.ID)
            {
                return string.Format("#{0}", this.ClientID);
            }
            return string.Format(".{0}", this.TriggerClass.Length > 0 ? this.TriggerClass : "dialogTrigger");   
        }
    }
}
