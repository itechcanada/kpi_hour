﻿using System.Linq;

namespace iTECH.Library
{
    public static class ObjectExtensions
    {
        #region Class Methods

        public static bool IsNull<T>(this T obj)
        { 
            return Equals(obj, null);
        }

        #endregion
    }
}
