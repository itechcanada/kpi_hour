﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Web;

using FreeImageAPI;
using iTECH.Library;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

namespace iTECH.Library.Web
{
    public class HttpImageHandler : NativeImageTool, IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string key = context.Request.Url.ToString().ToLowerInvariant();
            string eTag = "\"" + key.GetHashCode() + "\"";
            if (CheckETag(context, eTag))
            {
                return;
            }

            ImageFormat imageFormat = GetImageFormat(context);

            byte[] imageData = this.GetImageData(context, imageFormat);
            if (imageData.IsNull())
            {
                return;
            }

            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetETag(eTag);
            context.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
            context.Response.ContentType = "image/" + imageFormat.ToString().ToLower();
            context.Response.OutputStream.Write(imageData, 0, imageData.Length);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Takes care of resizeing image from url parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="imageFormat"></param>
        /// <returns></returns>
        private byte[] GetImageData(HttpContext context, ImageFormat imageFormat)
        {
            string src = context.Request["src"];

            byte[] data = File.ReadAllBytes(context.Server.MapPath(src));
            if (data.IsNull())
            {
                return null;
            }

            using (MemoryStream sourceImageData = new MemoryStream(data))
            {
                Bitmap outputImg = (Bitmap)Image.FromStream(sourceImageData);
                bool modified = this.Process(context, ref outputImg);

                using (outputImg)
                {
                    // Encode image
                    return this.Encode(outputImg, imageFormat);
                }
            }
        }

        public bool Process(HttpContext context, ref Bitmap bitmap)
        {
            string widthParameter = context.Request["width"];
            string heightParameter = context.Request["height"];

            if (!string.IsNullOrEmpty(widthParameter) || !string.IsNullOrEmpty(heightParameter))
            {
                int imageResizeWidth = 0;
                int imageResizeHeight = 0;

                // Preserve aspect only if either width or height is specified, not both.
                if (!string.IsNullOrEmpty(widthParameter))
                {
                    int width;
                    if (int.TryParse(widthParameter, out width))
                    {
                        imageResizeWidth = width;
                    }
                }

                if (!string.IsNullOrEmpty(heightParameter))
                {
                    int height;
                    if (int.TryParse(heightParameter, out height))
                    {
                        imageResizeHeight = height;
                    }
                }

                if (imageResizeWidth == 0 && imageResizeHeight == 0)
                {
                    imageResizeWidth = bitmap.Width;
                    imageResizeHeight = bitmap.Height;
                }

                bitmap = this.Resize(bitmap, imageResizeWidth, imageResizeHeight, true);
                return true;
            }

            return false;
        }

        #region Class Methods

        /// <summary>
        /// Check if the ETag that sent from the client is match to the current ETag.
        /// If so, set the status code to 'Not Modified' and stop the response.
        /// </summary>
        private static bool CheckETag(HttpContext context, string eTagCode)
        {
            string ifNoneMatch = context.Request.Headers["If-None-Match"];
            if (eTagCode.Equals(ifNoneMatch, StringComparison.Ordinal))
            {
                context.Response.AppendHeader("Content-Length", "0");
                context.Response.StatusCode = (int)HttpStatusCode.NotModified;
                context.Response.StatusDescription = "Not modified";
                context.Response.SuppressContent = true;
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                context.Response.Cache.SetETag(eTagCode);
                context.Response.Cache.SetExpires(DateTime.Now.AddYears(1));
                context.Response.End();
                return true;
            }

            return false;
        }

        private static ImageFormat GetImageFormat(HttpContext context)
        {
            string imageTypeParameter = context.Request["format"];
            if (!string.IsNullOrEmpty(imageTypeParameter))
            {
                switch (imageTypeParameter.ToLowerInvariant())
                {
                    case "png":
                        return ImageFormat.Png;
                    case "gif":
                        return ImageFormat.Gif;
                    case "jpg":
                        return ImageFormat.Jpeg;
                    case "tif":
                        return ImageFormat.Tiff;
                }
            }

            return ImageFormat.Jpeg;
        }

        #endregion


        #region Instance Methods

        public override byte[] Encode(Bitmap source, ImageFormat imageFormat)
        {
            FIBITMAP dib = FreeImageAPI.FreeImage.CreateFromBitmap(source);

            if (dib.IsNull)
            {
                return base.Encode(source, imageFormat);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                if (!FreeImageAPI.FreeImage.SaveToStream(
                    ref dib, ms,
                    ImageUtility.ConvertToFreeImageFormat(imageFormat),
                    ImageUtility.ConvertToFreeImageSaveFlags(imageFormat),
                    FREE_IMAGE_COLOR_DEPTH.FICD_AUTO,
                    true))
                {
                    return base.Encode(source, imageFormat);
                }

                return ms.ToArray();
            }
        }

        #endregion   
    }
}
