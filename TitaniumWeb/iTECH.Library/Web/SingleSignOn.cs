﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace iTECH.Library.Web
{
    public class SingleSignOn
    {
        // this creates a new token for the currently authenticated user
        // this would be called from an authenticated page that want to        
        public static string CreateToken(string userData)
        {
            AuthToken t = new AuthToken(userData);
            Token tk = t.ToToken();            
            SingleSignOn obj = new SingleSignOn();
            string serializeData = Newtonsoft.Json.JsonConvert.SerializeObject(tk, Newtonsoft.Json.Formatting.None);
            return Convert.ToBase64String(UTF8Encoding.ASCII.GetBytes(serializeData));
        }

        // this creates a new token for the currently authenticated user
        // this would be called from an authenticated page that want to        
        public static string CreateToken(string userData, DateTime expiredOn)
        {
            AuthToken t = new AuthToken(userData, expiredOn);
            Token tk = t.ToToken();
            SingleSignOn obj = new SingleSignOn();
            string serializeData = Newtonsoft.Json.JsonConvert.SerializeObject(tk, Newtonsoft.Json.Formatting.None);
            return Convert.ToBase64String(UTF8Encoding.ASCII.GetBytes(serializeData));
        }

        // this is the web service entry point, the password we get back from server
        // will be what we generated in the CreateToken call above
        public static bool Authenticate(string userData,
                                   string password,
                                   string sourceIp)
        {
            if (userData.Length <= 0)
                return false;
            
            Token tk = (Token)Newtonsoft.Json.JsonConvert.DeserializeObject<Token>(UTF8Encoding.ASCII.GetString(Convert.FromBase64String(password)));
            AuthToken t = tk.ToAuthToken();
            if ((t == null) || (t.Username != userData))
                return false;

            return usedids.IsNewId(t.TokenNumber, t.Expires);
        }

        private static UsedIds usedids = new UsedIds();
    }

    // a helper class that keeps track of the set of token number's we've seen
    // and removes them from the list once they expire	
    public class UsedIds
    {
        private Hashtable ids, sync_ids;
        private ReaderWriterLock rwl;
        public UsedIds()
        {
            ids = new Hashtable();
            sync_ids = Hashtable.Synchronized(ids);
            rwl = new ReaderWriterLock();
            Thread cleanupThread = new Thread(new ThreadStart(this.Cleanup));
            cleanupThread.IsBackground = true;
            cleanupThread.Start();
        }

        // returns true if this id hasn't been used before, false if it has
        public bool IsNewId(long id, DateTime expires)
        {
            try
            {
                rwl.AcquireReaderLock(Timeout.Infinite);
                if (sync_ids.ContainsKey(id))
                    return false;
                sync_ids[id] = expires;
                return true;
            }
            finally
            {
                rwl.ReleaseReaderLock();
            }
        }

        public void Cleanup()
        {
            ArrayList expired = new ArrayList();
            while (true)
            {
                Thread.Sleep(60000);
                DateTime now = DateTime.Now;
                try
                {
                    rwl.AcquireWriterLock(Timeout.Infinite);
                    foreach (DictionaryEntry e in ids)
                    {
                        if (now > ((DateTime)e.Value))
                            expired.Add(e.Key);
                    }
                }
                finally
                {
                    rwl.ReleaseWriterLock();
                }
                foreach (long id in expired)
                    sync_ids.Remove(id);
                expired.Clear();
            }
        }
    }

    [Serializable]
    public class AuthToken
    {
        public AuthToken(string username)
        {
            this.token_number = Interlocked.Increment(ref m_next_token_number);
            this.expires = DateTime.Now.AddMinutes(5);
            this.username = username;
        }

        public AuthToken(string username, DateTime expiredOn)
        {
            this.token_number = Interlocked.Increment(ref m_next_token_number);
            this.expires = expiredOn;
            this.username = username;
        }

        public string Username
        {
            get { return username; }
        }

        public DateTime Expires
        {
            get { return expires; }
        }
        public bool Expired
        {
            get { return DateTime.Now > expires; }
        }
        public long TokenNumber
        {
            get { return token_number; }
        }

        private long token_number;
        private DateTime expires;
        private string username;

        private static long m_next_token_number = 42;

        public Token ToToken()
        {
            Token t = new Token();
            t.Expired = this.Expired;
            t.Expires = this.Expires;
            t.TokenNumber = this.TokenNumber;
            t.UserName = this.Username;

            return t;
        }
    }

    public class Token
    {
        public string UserName
        {
            get;
            set;
        }
        public DateTime Expires
        {
            get;
            set;
        }
        public long TokenNumber
        {
            get;
            set;
        }
        public bool Expired
        {
            get;
            set;
        }

        public AuthToken ToAuthToken()
        {
            return new AuthToken(this.UserName);
        }
    }
}
