﻿//  Created by Hitendra Malviya 
//  On: 26-Nov-2010
//  For: iTECH Canada Inc.
// Modified By: Hitendra
// Last Modified Date: 09-Jun-2011
using System;
using System.Xml;
using System.Collections;

using System.Data;
using System.Data.SqlClient;

namespace iTECH.Library.DataAccess.MsSql
{
    /// <summary>
    /// The SqlHelper class is intended to encapsulate scalable best practices for 
    /// common uses of Sql.
    /// </summary>
    class SqlHelper
    {
        /// <summary>
        /// Call ExecuteReader Method of SQlCommand Object.
        /// </summary>
        public static SqlDataReader ExecuteReader(SqlConnection con, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {  
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;            
            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                SqlDataReader dr = cmd.ExecuteReader();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return dr;
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a SqlCommand and returns no of rows affected.
        /// </summary>
        public static int ExecuteNonQuery(SqlConnection con, string commandText, CommandType cmdType, SqlParameter[] parameters, bool mustClose)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                int rowsEffected = cmd.ExecuteNonQuery();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return rowsEffected;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }


        /// <summary>
        /// Execute a SqlCommand (that returns a 1x1 resultset)
        /// </summary>
        public static object ExecuteScalar(SqlConnection con, string commandText, CommandType cmdType, SqlParameter[] parameters, bool mustClose)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) { con.Open(); }
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                object scalar = cmd.ExecuteScalar();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return scalar;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// To returns dataset from database.
        /// </summary>
        public static DataSet ExecuteDataset(SqlConnection con, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                da.Fill(ds);
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return ds;
            }
            catch
            {
                throw;
            }
        }



        public static DataTable GetPagedDataTable(int startRecord, int maxRecord, SqlConnection con, string commandText, CommandType cmdType, SqlParameter[] parameters) {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                da.Fill(startRecord, maxRecord, dt);
                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Call ExecuteReader Method of SQlCommand Object.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static SqlDataReader ExecuteReader(SqlTransaction trans, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteReader();
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a SqlCommand and returns no of rows affected.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static int ExecuteNonQuery(SqlTransaction trans, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a SqlCommand (that returns a 1x1 resultset)
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static object ExecuteScalar(SqlTransaction trans, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteScalar();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To returns dataset from database.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>        
        public static DataSet ExecuteDataset(SqlTransaction trans, string commandText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                da.Fill(ds);
                return ds;
            }
            catch
            {
                throw;
            }
        }
    }
}
