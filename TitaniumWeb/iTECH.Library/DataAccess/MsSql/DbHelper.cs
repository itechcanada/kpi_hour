﻿//  Created by Hitendra Malviya 
//  On: 26-Nov-2010
//  For: iTECH Canada Inc.

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;


namespace iTECH.Library.DataAccess.MsSql
{
    /// <summary>
    /// DataAccess class will allow user to perform high scalable & secure operation on database.
    /// This class interacts with database and performs manipulations on database. 
    /// Note:(1) Always practice to stored procedure or parameterized queries. 
    /// This will automatically provide full proof security to application from Sql Injection like threats.
    /// (2) Don't forget to call CloseDatabaseConnection() mentod if KeepConnectionOpen property is set to true.
    /// </summary>
    public class DbHelper
    {
        #region Private Members
        private const string _connectionStringKey = "NewConnectionString";
        private SqlConnection _connection;
        private bool _keepConnectionOpen;
        
        #endregion

        #region Public Properties
        public bool KeepConnectionOpen
        {
            get { return _keepConnectionOpen; }
            set { _keepConnectionOpen = value; }
        }
        public SqlConnection Connection
        {
            get { return _connection; }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Initialize DataAccess class with default values.
        /// </summary>
        public DbHelper()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringKey].ConnectionString);
            _keepConnectionOpen = false;               
        }

        /// <summary>
        /// Initialize DataAccess class with explicit connection string.
        /// </summary>
        /// <param name="connectionString"></param>
        public DbHelper(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _keepConnectionOpen = false;            
        }

        /// <summary>
        /// Initialize DataAccess class with options to keep connection open.
        /// </summary>
        /// <param name="keepConnectionOpen"></param>
        public DbHelper(bool keepConnectionOpen)
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringKey].ConnectionString);
            _keepConnectionOpen = keepConnectionOpen;
        }

        public DbHelper(string connectionString, bool keepConnectionOpen)
        {
            _connection = new SqlConnection(connectionString);
            _keepConnectionOpen = keepConnectionOpen;
        } 
        #endregion

        #region Public Methods
        public void OpenDatabaseConnection()
        {
            if (_connection.State != ConnectionState.Open) _connection.Open();
        }

        public void CloseDatabaseConnection()
        {
            _connection.Dispose();
        }

        public void CloseDatabaseConnection(IDataReader dr)
        {
            if (dr != null)
            {
                dr.Dispose();
            }
            _connection.Dispose();
        }

        /// <summary>
        /// To returns datase from database in disconnected mode.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>           
        public DataSet GetDataSet(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {                
                return SqlHelper.ExecuteDataset(_connection, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        public DataTable GetPagedDataTable(int startRecord, int maxRecords, string strSql, CommandType cmdType, SqlParameter[] parameters) {
            try
            {
                return SqlHelper.GetPagedDataTable(startRecord, maxRecords, _connection, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To returns datasource in forms of DataTable  from database in disconnected mode.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary> 
        public DataTable GetDataTable(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                DataSet ds = this.GetDataSet(strSql, cmdType, parameters);
                DataTable dt = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To retrieve data from database in connected mode.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        public SqlDataReader GetDataReader(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteReader(_connection, strSql, cmdType, parameters);              
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To perform insert, update, and delete operation on database table.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        public int ExecuteNonQuery(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(_connection, strSql, cmdType, parameters, !_keepConnectionOpen);             
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get scalar value.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        /// <param name="spName">a valid string stored procedure which exists on database server.</param>
        /// <param name="parameters">an array of SqlParameters to be associated with the command.</param>
        /// <returns>Identity or ReturnValue.</returns>
        public object GetValue(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteScalar(_connection, strSql, cmdType, parameters, !_keepConnectionOpen);            
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}

//public void RunSqlTransaction(string myConnString) 
// {
//    SqlConnection myConnection = new SqlConnection(myConnString);
//    myConnection.Open();

//    SqlCommand myCommand = myConnection.CreateCommand();
//    SqlTransaction myTrans;

//    // Start a local transaction
//    myTrans = myConnection.BeginTransaction();
//    // Must assign both transaction object and connection
//    // to Command object for a pending local transaction
//    myCommand.Connection = myConnection;
//    myCommand.Transaction = myTrans;

//    try
//    {
//      myCommand.CommandText = "Insert into Region (RegionID, RegionDescription) VALUES (100, 'Description')";
//      myCommand.ExecuteNonQuery();
//      myCommand.CommandText = "Insert into Region (RegionID, RegionDescription) VALUES (101, 'Description')";
//      myCommand.ExecuteNonQuery();
//      myTrans.Commit();
//      Console.WriteLine("Both records are written to database.");
//    }
//    catch(Exception e)
//    {
//      try
//      {
//        myTrans.Rollback();
//      }
//      catch (SqlException ex)
//      {
//        if (myTrans.Connection != null)
//        {
//          Console.WriteLine("An exception of type " + ex.GetType() +
//                            " was encountered while attempting to roll back the transaction.");
//        }
//      }
    
//      Console.WriteLine("An exception of type " + e.GetType() +
//                        " was encountered while inserting the data.");
//      Console.WriteLine("Neither record was written to database.");
//    }
//    finally 
//    {
//      myConnection.Close();
//    }
//}

