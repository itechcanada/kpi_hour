﻿//  Created by Hitendra Malviya 
//  On: 09-Jun-2011
//  For: iTECH Canada Inc.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;

namespace iTECH.Library.DataAccess.MsSql
{
    public class DbTransactionHelper
    {
        #region Private Members
        private const string _connectionStringKey = "NewConnectionString";
        private SqlConnection _connection;
        private SqlTransaction _currentTransaction;
        #endregion

        #region Constructors

        /// <summary>
        /// Initialize DataAccess class with default values.
        /// </summary>
        public DbTransactionHelper()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings[_connectionStringKey].ConnectionString);
        }

        /// <summary>
        /// Initialize DataAccess class with explicit connection string.
        /// </summary>
        /// <param name="connectionString"></param>
        public DbTransactionHelper(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Initialize DataAccess class with already initialized connection.
        /// </summary>
        /// <param name="keepConnectionOpen"></param>
        public DbTransactionHelper(SqlConnection con)
        {
            _connection = con;
        }

        #endregion


        public void OpenDatabaseConnection()
        {
            if (_connection.State != ConnectionState.Open) _connection.Open();
        }

        public void CloseDatabaseConnection()
        {
            if (_connection.State != ConnectionState.Closed) _connection.Close();
        }

        #region Transaction Methods


        /// <summary>
        /// To returns datase from database in disconnected mode within current transaction.
        /// If query doesn't have any parameter then pass parameters as null.
        /// Note- Before using this method must call the BeginTransaction() Method to initialize the Transaction object.
        /// </summary>           
        public DataSet GetDataSet(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteDataset(_currentTransaction, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To returns datasource in forms of DataTable  from database in disconnected mode.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary> 
        public DataTable GetDataTable(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                DataSet ds = this.GetDataSet(strSql, cmdType, parameters);
                DataTable dt = null;
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

                return dt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To retrieve data from database in connected mode.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        public SqlDataReader GetDataReader(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteReader(_currentTransaction, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To perform insert, update, and delete operation on database table.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        public int ExecuteNonQuery(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteNonQuery(_currentTransaction, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get scalar value.
        /// If query doesn't have any parameter then pass parameters as null.
        /// </summary>
        /// <param name="spName">a valid string stored procedure which exists on database server.</param>
        /// <param name="parameters">an array of SqlParameters to be associated with the command.</param>
        /// <returns>Identity or ReturnValue.</returns>
        public object GetValue(string strSql, CommandType cmdType, SqlParameter[] parameters)
        {
            try
            {
                return SqlHelper.ExecuteScalar(_currentTransaction, strSql, cmdType, parameters);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Last Inserted Identity. 
        /// Note:- Database connection must be live which was used to insert new record.
        /// </summary>
        /// <returns></returns>
        public int GetLastInsertID()
        {
            object scalarData = 0;

            try
            {
                scalarData = SqlHelper.ExecuteScalar(_currentTransaction, "SELECT LAST_INSERT_ID()", CommandType.Text, null);
                return Convert.ToInt32(scalarData);
            }
            catch
            {

                throw;
            }
        }

        /// <summary>
        /// Call this method during if you wants to execute more then one commands in Trasaction.
        /// </summary>
        public void BeginTransaction()
        {
            OpenDatabaseConnection();
            _currentTransaction = _connection.BeginTransaction();
        }

        /// <summary>
        /// Call this method at the end of the execution of all commands in Trasaction.
        /// </summary>
        public void CommitTransaction()
        {
            if (_currentTransaction == null)
                throw new NullReferenceException("Transaction not initialized.");
            _currentTransaction.Commit();
        }

        /// <summary>
        /// Call this method in case of exception occured (in catch block) in between trnsaction (commands execution).
        /// </summary>
        public void RollBackTransaction()
        {
            if (_currentTransaction == null)
                throw new NullReferenceException("Transaction not initialized.");
            _currentTransaction.Rollback();
        }

        #endregion
    }
}
