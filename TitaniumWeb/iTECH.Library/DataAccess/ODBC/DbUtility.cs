﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Odbc;
using iTECH.Library.Utilities;

namespace iTECH.Library.DataAccess.ODBC
{
    public class DbUtility
    {
        public static OdbcParameter GetParameter(string name, object value, Type t)
        {
            if (t == typeof(string))
            {
                return GetStringParameter(name, BusinessUtility.GetString(value));
            }
            else if (t == typeof(int))
            {
                return GetIntParameter(name, BusinessUtility.GetInt(value));
            }
            else if (t == typeof(double))
            {
                return GetDoubleParameter(name, BusinessUtility.GetDouble(value));
            }
            else if (t == typeof(decimal))
            {
                return GetDecimalParameter(name, BusinessUtility.GetDecimal(value));
            }
            else if (t == typeof(float))
            {
                return GetFloatParameter(name, BusinessUtility.GetFloat(value));
            }
            else if (t == typeof(DateTime))
            {
                return GetDateTimeParameter(name, BusinessUtility.GetDateTime((DateTime)value));
            }
            else
            {
                return new OdbcParameter(BusinessUtility.GetParameterName(name), value);
            }
        }

        public static OdbcParameter GetStringParameter(string name, string value, bool convertEmptyStringToNull)
        {
            object val = convertEmptyStringToNull && string.IsNullOrEmpty(value) ? DBNull.Value : (object)value;
            return new OdbcParameter(BusinessUtility.GetParameterName(name), val);
        }

        public static OdbcParameter GetStringParameter(string name, string value)
        {
            return GetStringParameter(name, value, true);
        }

        public static OdbcParameter GetIntParameter(string name, int? value)
        {
            OdbcParameter p = new OdbcParameter(BusinessUtility.GetParameterName(name), SqlDbType.Int);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static OdbcParameter GetDoubleParameter(string name, double? value)
        {
            OdbcParameter p = new OdbcParameter(BusinessUtility.GetParameterName(name), SqlDbType.Float);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static OdbcParameter GetDecimalParameter(string name, decimal? value)
        {
            OdbcParameter p = new OdbcParameter(BusinessUtility.GetParameterName(name), SqlDbType.Decimal);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static OdbcParameter GetFloatParameter(string name, float? value)
        {
            OdbcParameter p = new OdbcParameter(BusinessUtility.GetParameterName(name), SqlDbType.Float);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static OdbcParameter GetDateTimeParameter(string name, DateTime? value)
        {
            OdbcParameter p = new OdbcParameter(BusinessUtility.GetParameterName(name), SqlDbType.DateTime);
            p.Value = !value.HasValue || value.Value == DateTime.MinValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static OdbcParameter GetObjectParameter(string name, bool value)
        {
            return new OdbcParameter(BusinessUtility.GetParameterName(name), value);
        }
    }
}
