﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.Library.Utilities
{
    public class TableHtmlBuilder
    {
        string _tableBuilder = "<table {0}>{1}{2}{3}{4}</table>";
        Dictionary<string, string> _attributes = new Dictionary<string, string>();

        ColGroupBuilder _colGroup = new ColGroupBuilder();
        TBodyBuilder _tHead = new TBodyBuilder(TableHeadRenderMode.Thead);
        TBodyBuilder _tbody = new TBodyBuilder(TableHeadRenderMode.Tbody);
        TBodyBuilder _tfoot = new TBodyBuilder(TableHeadRenderMode.Tfooter);

        public ColGroupBuilder ColGroup
        {
            get { return _colGroup; }            
        }

        public TBodyBuilder THead
        {
            get { return _tHead; }
        }

        public TBodyBuilder Tbody
        {
            get { return _tbody; }
        }

        public TBodyBuilder Tfoot
        {
            get { return _tfoot; }
        }        

        public TableHtmlBuilder AddAttribute(string key, string val)
        {
            _attributes[key] = val;            
            return this;
        }

        public string Output {
            get {
                return string.Format(_tableBuilder, this.GetAttributes(), this._colGroup.Output, this._tHead.Output, this._tbody.Output, this._tfoot.Output);                
            }
        }

        public string OutputWithoutGroups
        {
            get {                
                StringBuilder sb = new StringBuilder();
                foreach (var item in this._tHead.TrCollection)
                {
                    sb.Append(item.Output);
                }
                foreach (var item in this._tbody.TrCollection)
                {
                    sb.Append(item.Output);
                }
                foreach (var item in this._tfoot.TrCollection)
                {
                    sb.Append(item.Output);
                }
                return string.Format(_tableBuilder, this.GetAttributes(), sb.ToString(), string.Empty, string.Empty, string.Empty);                
            }
        }

        public override string ToString()
        {
            return this.Output;
        }

        private string GetAttributes() {
            StringBuilder attrs = new StringBuilder();

            foreach (string key in _attributes.Keys)
            {
                attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
            }

            return attrs.ToString();
        }
    }

    public class ColGroupBuilder
    {       
        List<ColBuilder> _cols = new List<ColBuilder>();
        Dictionary<string, string> _attributes = new Dictionary<string, string>();

        public ColGroupBuilder AddAttribute(string key, string val) {
            _attributes[key] = val;
            return this;
        }

        public List<ColBuilder> Cols
        {
            get { return _cols; }
        }

        public string Output
        {
            get
            {
                StringBuilder _exp = new StringBuilder();
                foreach (ColBuilder item in _cols)
                {
                    _exp.Append(item.Output);
                }

                if (_exp.Length > 0) {
                    return string.Format("<colgroup {0}>{1}</colgroup>", this.GetAttributes(), _exp.ToString());
                }

                return string.Empty;
            }
        }

        public override string ToString()
        {
            return this.Output;
        }

        private string GetAttributes()
        {
            StringBuilder attrs = new StringBuilder();

            foreach (string key in _attributes.Keys)
            {
                attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
            }

            return attrs.ToString();
        }
    }

    public class ColBuilder
    {
        Dictionary<string, string> _attributes = new Dictionary<string, string>();

         public ColBuilder AddAttribute(string key, string val)
         {
             _attributes[key] = val;
             return this;
         }

         public string Output {
             get {
                 return string.Format("<col {0} />", this.GetAttributes());
             }
         }

         public override string ToString()
         {
             return this.Output;
         }

         private string GetAttributes()
         {
             StringBuilder attrs = new StringBuilder();

             foreach (string key in _attributes.Keys)
             {
                 attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
             }

             return attrs.ToString();
         }
    }    

    public class TBodyBuilder
    {        
        List<TrBuilder> _tr = new List<TrBuilder>();
        Dictionary<string, string> _attributes = new Dictionary<string, string>();
        TableHeadRenderMode _tag = TableHeadRenderMode.Tbody;

        public TBodyBuilder() { }
        public TBodyBuilder(TableHeadRenderMode renderTag) {
            _tag = renderTag;
        }

        public TBodyBuilder AddAttribute(string key, string val)
        {
            _attributes[key] = val;
            return this;
        }

        public List<TrBuilder> TrCollection
        {
            get { return _tr; }
        }

        public string Output
        {
            get
            {
                StringBuilder _exp = new StringBuilder();
                string tag = "tbody";
                switch (_tag)
                {
                    case TableHeadRenderMode.Thead:
                        tag = "thead";
                        break;
                    case TableHeadRenderMode.Tbody:
                        tag = "tbody";
                        break;
                    case TableHeadRenderMode.Tfooter:
                        tag = "tfoot";
                        break;
                    default:
                        tag = "tbody";
                        break;
                }

                foreach (TrBuilder item in _tr)
                {
                    _exp.Append(item.Output);
                }

                if (_exp.Length > 0)
                {
                    return string.Format("<{0} {1}>{2}</{0}>", tag, this.GetAttributes(), _exp.ToString());
                }

                return string.Empty;
            }
        }

        public override string ToString()
        {
            return this.Output;
        }

        private string GetAttributes()
        {
            StringBuilder attrs = new StringBuilder();

            foreach (string key in _attributes.Keys)
            {
                attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
            }

            return attrs.ToString();
        }
    }

    public class TdBuilder
    {
        string _htmlData = string.Empty;
        Dictionary<string, string> _attributes = new Dictionary<string, string>();
        TableCoulmRenderMode _tag = TableCoulmRenderMode.Td;
        public TdBuilder() { }
        public TdBuilder(TableCoulmRenderMode tagName) {
            _tag = tagName;
        }

        public TdBuilder SetHtml(string html)
        {
            _htmlData = html;
            return this;
        }

        public TdBuilder AddAttribute(string key, string val)
        {
            _attributes[key] = val;
            return this;
        }

        public string Output
        {
            get
            {
                string tag = "td";
                switch (_tag)
                {
                    case TableCoulmRenderMode.Td:
                        tag = "td";
                        break;
                    case TableCoulmRenderMode.Th:
                        tag = "th";
                        break;
                    default:
                        break;
                }

                return string.Format("<{0} {1}>{2}</{0}>", tag, this.GetAttributes(), _htmlData);
            }
        }

        public override string ToString()
        {
            return this.Output;
        }

        private string GetAttributes()
        {
            StringBuilder attrs = new StringBuilder();

            foreach (string key in _attributes.Keys)
            {
                attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
            }

            return attrs.ToString();
        }
    }

    public class TrBuilder
    {        
        List<TdBuilder> _td = new List<TdBuilder>();
        Dictionary<string, string> _attributes = new Dictionary<string, string>();

        public TrBuilder AddAttribute(string key, string val)
        {
            _attributes[key] = val;
            return this;
        }

        public List<TdBuilder> TdCollection
        {
            get { return _td; }
        }

        public string Output
        {
            get
            {
                StringBuilder _exp = new StringBuilder();

                foreach (TdBuilder item in _td)
                {
                    _exp.Append(item.Output);
                }

                if (_exp.Length > 0)
                {
                    return string.Format("<tr {0}>{1}</tr>", this.GetAttributes(), _exp.ToString());
                }

                return string.Empty;
            }
        }

        public override string ToString()
        {
            return this.Output;
        }

        private string GetAttributes()
        {
            StringBuilder attrs = new StringBuilder();

            foreach (string key in _attributes.Keys)
            {
                attrs.AppendFormat(@" {0}=""{1}""", key, _attributes[key]);
            }

            return attrs.ToString();
        }
    }

    public enum TableCoulmRenderMode { 
        Td,
        Th
    }

    public enum TableHeadRenderMode { 
        Thead,
        Tbody,
        Tfooter
    }
}
