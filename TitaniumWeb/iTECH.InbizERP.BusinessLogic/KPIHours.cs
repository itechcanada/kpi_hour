﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using iTECH.Library.DataAccess.MsSql;
using System.Data.SqlClient;
using iTECH.Library.Utilities;
using System.Web.UI.WebControls;



namespace iTECH.InbizERP.BusinessLogic
{
    public class KPIHours
    {
        public string DataBase { get; set; }
        public string KPIBranch { get; set; }
        public DateTime KPIDate { get; set; }
        public int KPIhours { get; set; }

        private String m_sSql = string.Empty;

        public DataTable GetForecastValue() //string yyyyMM
        {
            DataTable dt = new DataTable();
            DbHelper dbhelp = new DbHelper();

            try
            {

                string sql = "Select Databases,Branch,Budget,Forecast,GPBudget,ISNULL(GPForecast,0) as GPForecast,ISNULL(GPForecastPct,0.0) as GPForecastPct,LBSShippedPlan from tbl_itech_Forecast_AM where YearMonth = convert(varchar(7),GetDate(), 126) order by Databases desc";
                dt = dbhelp.GetDataTable(sql, CommandType.Text, null);
                //new SqlParameter[]{
                //DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))});
                if (dt.Rows.Count == 0)
                {
                    string sqlinsert = "INSERT INTO [tbl_itech_Forecast_AM] ([Budget],[Forecast],[GPBudget],[GPForecast],[GPForecastPct],[LBSShippedPlan],[WarehseFees],[Branch],[Databases],[YearMonth],[Date],[InvtValBrh]) ";
                    sqlinsert += " select 0,0,0,0,0.0,0,0,[Branch],[Databases],convert(varchar(7),GetDate(), 126),convert(varchar(10),GetDate(), 126),InvtValBrh from [tbl_itech_Forecast_AM] where YearMonth = convert(varchar(7),dateadd(month, datediff(month, 0, getdate()) - 1, 0), 126) ";
                    dbhelp.ExecuteNonQuery(sqlinsert, CommandType.Text, null);
                    dt = dbhelp.GetDataTable(sql, CommandType.Text, null);
                    //    new SqlParameter[]{
                    //DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))});
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
            return dt;

        }
        public void UpdateForecast()    //string yyyyMM
        {
            //DbHelper dbhelp = new DbHelper();
            //try
            //{
            //    string sql = " UPDATE tbl_itech_Forecast_AM set Budget = @Budget, Forecast = @Forecast, GPBudget = @GPBudget, GPForecast = @GPForecast,GPForecastPct = @GPForecastPct, LBSShippedPlan = @LBSShippedPlan, YearMonth = convert(varchar(7),GetDate(), 126), Date=@Date";
            //    sql += " WHERE Branch=@Branch and Databases=@Databases and YearMonth = convert(varchar(7),GetDate(), 126)";
            //    dbhelp.ExecuteNonQuery(sql, CommandType.Text, new SqlParameter[]{
            //        DbUtility.GetParameter("Budget", this.Budget, typeof(double)),
            //        DbUtility.GetParameter("Forecast", this.Forecast, typeof(double)),
            //        DbUtility.GetParameter("GPBudget", this.GPBudget, typeof(double)),
            //        DbUtility.GetParameter("GPForecast", this.GPForecast, typeof(double)),
            //        DbUtility.GetParameter("GPForecastPct", this.GPForecastPct, typeof(double)),
            //        DbUtility.GetParameter("LBSShippedPlan", this.LBSShippedPlan, typeof(double)),
            //        DbUtility.GetParameter("Branch", this.Branch, typeof(string)),
            //        DbUtility.GetParameter("Databases", this.Database, typeof(string)),
            //        DbUtility.GetParameter("Date", DateTime.Now, typeof(DateTime))  //,    DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))
            //    });
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    dbhelp.CloseDatabaseConnection();
            //}
        }



        public string KPIhourinsert()
        {
            DbHelper dbhelp = new DbHelper();
            string sqlInsert = string.Empty;
            object ID = null;
            bool isUpdate = false;
            bool isAdded = false;

            try
            {

                DateTime dtToAdd = new DateTime();
                int i = 0;
                int iDaysCountToAdd = 0;
                while (i <= iDaysCountToAdd)
                {
                    dtToAdd = this.KPIDate.AddDays(i);
                    sqlInsert = "Select ID from tbl_itech_MonthlyKPI where DatabasePrefix = @DatabasePrefix and BranchPrefix = @BranchPrefix and  KPIDate = @KPIDate  ";

                    ID = (object)dbhelp.GetValue(sqlInsert, CommandType.Text, new SqlParameter[]{
                 DbUtility.GetParameter("DatabasePrefix", this.DataBase, typeof(string)),
                 DbUtility.GetParameter("BranchPrefix", this.KPIBranch, typeof(string)),
                 //DbUtility.GetParameter("KPIDate", this.KPIDate, typeof(DateTime))                
                 DbUtility.GetParameter("KPIDate", dtToAdd, typeof(DateTime))                
                });

                    if (BusinessUtility.GetInt(ID) > 0)
                    {
                        sqlInsert = "update tbl_itech_MonthlyKPI  set  KPIHour = @KPIHour , KPIDate =@KPIDate  where ID = @ID";
                        dbhelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new SqlParameter[]{
                 DbUtility.GetParameter("ID", ID, typeof(Int32)), 
                 //DbUtility.GetParameter("KPIDate", this.KPIDate, typeof(DateTime)),
                 DbUtility.GetParameter("KPIDate", dtToAdd, typeof(DateTime)) ,
                 DbUtility.GetParameter("KPIHour", this.KPIhours, typeof(Int32))
                });

                        //return Hourmsg.UPDATESUCCESS;

                        if (i == 0)
                        {
                            isUpdate = true;
                        }

                    }
                    else
                    {

                        sqlInsert = "insert  into tbl_itech_MonthlyKPI (DatabasePrefix, BranchPrefix, KPIDate, KPIHour, CreatedDateTime) values(@DatabasePrefix, @BranchPrefix, @KPIDate, @KPIHour,GETDATE())";
                        dbhelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new SqlParameter[]{
                 DbUtility.GetParameter("DatabasePrefix", this.DataBase, typeof(string)),
                 DbUtility.GetParameter("BranchPrefix", this.KPIBranch, typeof(string)),
                 //DbUtility.GetParameter("KPIDate", this.KPIDate, typeof(DateTime)),
                 DbUtility.GetParameter("KPIDate", dtToAdd, typeof(DateTime)), 
                 DbUtility.GetParameter("KPIHour", this.KPIhours, typeof(Int32))
                });
                        //return Hourmsg.INSERTSUCCESS;

                        if (i == 0)
                        {
                            isAdded = true;
                        }
                    }

                    i += 1;
                }


                if (isUpdate == true)
                {
                    return Hourmsg.UPDATESUCCESS;
                }
                else
                {
                    return Hourmsg.INSERTSUCCESS;
                }


            }
            catch (Exception ex) {
                throw;
            //    return Hourmsg.FAILED; 
            
            }
            finally { dbhelp.CloseDatabaseConnection(); }
        }
        public void FillDatabaseControl(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            string sqlString = string.Empty;
            DataTable dtTable = new DataTable();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                sqlString = "select Prefix, Name  from  [dbo].[tbl_itech_DatabaseName_PS]";
                dtTable = dbHelp.GetDataTable(sqlString, CommandType.Text, null);
                if (dtTable.Rows.Count > 0)
                {
                    lCtrl.DataSource = dtTable;
                    lCtrl.DataTextField = "Name";
                    lCtrl.DataValueField = "Prefix";
                    lCtrl.DataBind();
                    if (rootItem != null)
                    {
                        lCtrl.Items.Insert(0, rootItem);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public List<BranchDetails> getBranch(string databasePrefix)
        {
            string sqlString = string.Empty;
            DataTable dtTable = new DataTable();
            DbHelper dbHelp = new DbHelper(true);
            List<BranchDetails> objBranch = new List<BranchDetails>();
            try
            {
                dtTable = dbHelp.GetDataTable("sp_itech_GetBranches", CommandType.StoredProcedure, new SqlParameter[]{
                    DbUtility.GetParameter("DBNAME", databasePrefix , typeof(string))  });

                foreach (DataRow dr in dtTable.Rows.Cast<DataRow>().Skip(1))
                {
                    if (BusinessUtility.GetString(dr["Value"]) != "CRP" && BusinessUtility.GetString(dr["Value"]) != "EXP" && BusinessUtility.GetString(dr["Value"]) != "ZZZ" && BusinessUtility.GetString(dr["Value"]) != "ROS" && BusinessUtility.GetString(dr["Value"]) != "STH")
                    {
                        objBranch.Add(new BranchDetails { BranchID = BusinessUtility.GetString(dr["Value"]), BranchName = BusinessUtility.GetString(dr["text"]) });
                    }

                }
            }
            catch(Exception ex) {
                throw;
            }
            finally { dbHelp.CloseDatabaseConnection(); }
            return objBranch;
        }


        public List<KpiDetails> PopulateKpiData(string sDatabasePrefix, string sBranchPrefix)
        {
            DbHelper dbhelp = new DbHelper();
            List<KpiDetails> lstKpiDetail = new List<KpiDetails>();
            m_sSql = "Select ID,KPIDate,KPIHour from tbl_itech_MonthlyKPI where DatabasePrefix = @DatabasePrefix and BranchPrefix = @BranchPrefix and KPIDate = DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 0) ";
            try
            {

                DataTable dtKpiDetail = dbhelp.GetDataTable(m_sSql, CommandType.Text, new SqlParameter[]{
                DbUtility.GetParameter("DatabasePrefix", sDatabasePrefix, typeof(string)),
                DbUtility.GetParameter("BranchPrefix", sBranchPrefix, typeof(string))                
                });

                foreach (DataRow oDataRow in dtKpiDetail.Rows)
                {
                    // lstKpiDetail.Add(new KpiDetails { KpiHour = BusinessUtility.GetString(oDataRow["KPIHour"]), KpiDate = BusinessUtility.GetDateTime(oDataRow["KPIDate"]).ToString("MM/dd/yyyy") });
                    lstKpiDetail.Add(new KpiDetails { KpiHour = BusinessUtility.GetString(oDataRow["KPIHour"]), KpiDate = BusinessUtility.GetDateTimeString(BusinessUtility.GetDateTime(oDataRow["KPIDate"]), DateFormat.MMddyyyy) });
                }

            }
            catch (Exception ex)
            { }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
            return lstKpiDetail;

        }

        public List<KpiDetails> PopulateKpiData(string sDatabasePrefix, string sBranchPrefix, DateTime dtKPIDate)
        {
            DbHelper dbhelp = new DbHelper();
            List<KpiDetails> lstKpiDetail = new List<KpiDetails>();
            m_sSql = "Select ID,KPIDate,KPIHour from tbl_itech_MonthlyKPI where DatabasePrefix = @DatabasePrefix and BranchPrefix = @BranchPrefix and KPIDate = @KPIDate ";
            try
            {

                DataTable dtKpiDetail = dbhelp.GetDataTable(m_sSql, CommandType.Text, new SqlParameter[]{
                DbUtility.GetParameter("DatabasePrefix", sDatabasePrefix, typeof(string)),
                DbUtility.GetParameter("BranchPrefix", sBranchPrefix, typeof(string)),
                DbUtility.GetParameter("KPIDate", dtKPIDate, typeof(DateTime))
                });

                foreach (DataRow oDataRow in dtKpiDetail.Rows)
                {
                    // lstKpiDetail.Add(new KpiDetails { KpiHour = BusinessUtility.GetString(oDataRow["KPIHour"]), KpiDate = BusinessUtility.GetDateTime(oDataRow["KPIDate"]).ToString("MM/dd/yyyy") });
                    lstKpiDetail.Add(new KpiDetails { KpiHour = BusinessUtility.GetString(oDataRow["KPIHour"]), KpiDate = BusinessUtility.GetDateTimeString(BusinessUtility.GetDateTime(oDataRow["KPIDate"]), DateFormat.MMddyyyy) });
                }

            }
            catch (Exception ex)
            { }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
            return lstKpiDetail;

        }

    }
    public class BranchDetails
    {
        public string BranchID { get; set; }
        public string BranchName { get; set; }
    }

    public class KpiDetails
    {
        public string KpiDate { get; set; }
        public string KpiHour { get; set; }
    }


}
