﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Addresses
    {
        #region Private Member
        private string _addressLine1;
        private string _addressLine2;
        private string _addressLine3;
        private string _addressType;
        private string _addressCity;
        private string _addressCountry;
        private string _addressPostalCode;
        private string _addressState;
        private string _addressRef;

        private int _addressSourceID;
        private int _addressID;
        #endregion

        public Addresses()
        {
            this.AddressCity = string.Empty;
            this.AddressCountry = string.Empty;
            this.AddressID = 0;
            this.AddressLine1 = string.Empty;
            this.AddressLine2 = string.Empty;
            this.AddressLine3 = string.Empty;
            this.AddressPostalCode = string.Empty;
            this.AddressRef = string.Empty;
            this.AddressSourceID = 0;
            this.AddressState = string.Empty;
            this.AddressType = string.Empty;
            this.AddressID = 0;
            this.AddressSourceID = 0;
            this.FirstName = "";
            this.LastName = "";
        }

        #region property
        public int AddressSourceID
        {
            get { return _addressSourceID; }
            set { _addressSourceID = value; }
        }
        public int AddressID
        {
            get { return _addressID; }
            set { _addressID = value; }
        }

        public string AddressRef
        {
            get { return _addressRef; }
            set { _addressRef = value; }
        }
        public string AddressType
        {
            get { return _addressType; }
            set { _addressType = value; }
        }
        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        public string AddressLine3
        {
            get { return _addressLine3; }
            set { _addressLine3 = value; }
        }
        public string AddressCity
        {
            get { return _addressCity; }
            set { _addressCity = value; }
        }
        public string AddressState
        {
            get { return _addressState; }
            set { _addressState = value; }
        }
        public string AddressCountry
        {
            get { return _addressCountry; }
            set { _addressCountry = value; }
        }
        public string AddressPostalCode
        {
            get { return _addressPostalCode; }
            set { _addressPostalCode = value; }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        #endregion

        #region Functions
        public void GetAddress(int SourcePrimaryKey, string ReferenceTable, string AddressType)
        {
            string strSQL = null;
            strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM addresses WHERE addressSourceID = @SourcePrimaryKey and addressRef LIKE CONCAT('%', @ReferenceTable, '%')  and addressType LIKE CONCAT('%', @AddressType, '%') ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("SourcePrimaryKey", SourcePrimaryKey,typeof(int) ),
                                     DbUtility.GetParameter("ReferenceTable", ReferenceTable,typeof(string)),
                                     DbUtility.GetParameter("AddressType", AddressType,typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader objDr = default(MySqlDataReader);
            try
            {

                objDr = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this._addressLine1 = BusinessUtility.GetString(objDr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(objDr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(objDr["addressLine3"]);
                    this._addressType = BusinessUtility.GetString(objDr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(objDr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(objDr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(objDr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(objDr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(objDr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(objDr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
            }

        }

        public void PopulateObject(int sourcePk, string addrRef, string addrType)
        {
            string strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM addresses WHERE addressSourceID = @addressSourceID and addressRef=@addressRef  and addressType=@addressType";
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader dr = null;
            try
            {

                dr = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[]{ 
                                     DbUtility.GetParameter("addressSourceID", sourcePk, MyDbType.Int),
                                     DbUtility.GetParameter("addressRef", addrRef, MyDbType.String),
                                     DbUtility.GetParameter("addressType", addrType, MyDbType.String),
                                 });
                if (dr.Read())
                {
                    this._addressID = BusinessUtility.GetInt(dr["addressID"]);
                    this._addressLine1 = BusinessUtility.GetString(dr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(dr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(dr["addressLine3"]);
                    this._addressType = BusinessUtility.GetString(dr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(dr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(dr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(dr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(dr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection(dr);
            }
        }

        public void PopulateObject(DbHelper dbHelp, int sourcePk, string addrRef, string addrType)
        {
            string strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM addresses WHERE addressSourceID = @addressSourceID and addressRef=@addressRef  and addressType=@addressType";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[]{ 
                                     DbUtility.GetParameter("addressSourceID", sourcePk, MyDbType.Int),
                                     DbUtility.GetParameter("addressRef", addrRef, MyDbType.String),
                                     DbUtility.GetParameter("addressType", addrType, MyDbType.String),
                                 });
                if (dr.Read())
                {
                    this._addressID = BusinessUtility.GetInt(dr["addressID"]);
                    this._addressLine1 = BusinessUtility.GetString(dr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(dr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(dr["addressLine3"]);
                    this._addressType = BusinessUtility.GetString(dr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(dr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(dr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(dr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(dr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }



        public bool Save()
        {
            DbHelper dbHelp = new DbHelper(true);
            string sqlCheckcount = "SELECT COUNT(*) FROM addresses WHERE addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType";
            string sqlInsert = "INSERT INTO addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode,addressItem, FirstName , LastName)";
            sqlInsert += " VALUES ( @addressRef, @addressType, @addressSourceID, @addressLine1, @addressLine2, @addressLine3, @addressCity, @addressState, @addressCountry, @addressPostalCode, @addressItem, @FirstName, @LastName)";
            string sqlUpdate = "UPDATE addresses SET addressRef=@addressRef, addressType=@addressType, addressSourceID=@addressSourceID, addressLine1=@addressLine1, addressLine2=@addressLine2, addressLine3=@addressLine3, addressCity=@addressCity, addressState=@addressState, addressCountry=@addressCountry, addressPostalCode=@addressPostalCode, FirstName = @FirstName, LastName = @LastName  WHERE  addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType";

            try
            {
                object check = dbHelp.GetValue(sqlCheckcount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                    DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                    DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                });
                if (BusinessUtility.GetInt(check) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                                                DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                        DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("addressItem", 1, MyDbType.Int),
                                                DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                        DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                    });
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Save(DbHelper dbHelper)
        {
            string sqlCheckcount = "SELECT COUNT(*) FROM addresses WHERE addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType";

            string sqlInsert = "INSERT INTO addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, addressItem, FirstName , LastName )";
            sqlInsert += " VALUES ( @addressRef, @addressType, @addressSourceID, @addressLine1, @addressLine2, @addressLine3, @addressCity, @addressState, @addressCountry, @addressPostalCode, @addressItem, @FirstName, @LastName)";

            string sqlUpdate = "UPDATE addresses SET addressRef=@addressRef, addressType=@addressType, addressSourceID=@addressSourceID, addressLine1=@addressLine1, addressLine2=@addressLine2, addressLine3=@addressLine3, addressCity=@addressCity, addressState=@addressState, addressCountry=@addressCountry, addressPostalCode=@addressPostalCode, FirstName = @FirstName, LastName = @LastName  WHERE  addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType";

            string sqlUpdateAddress = "UPDATE addresses SET addressRef=@addressRef, addressType=@addressType, addressSourceID=@addressSourceID, addressLine1=@addressLine1, addressLine2=@addressLine2, addressLine3=@addressLine3, addressCity=@addressCity, addressState=@addressState, addressCountry=@addressCountry, addressPostalCode=@addressPostalCode, FirstName = @FirstName, LastName = @LastName  WHERE  addressID = @addressID";

            try
            {
                if (this.AddressID > 0)
                {
                    dbHelper.ExecuteNonQuery(sqlUpdateAddress, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                    DbUtility.GetParameter("LastName", this.LastName, MyDbType.String),
                    DbUtility.GetParameter("addressID", this.AddressID, MyDbType.Int),
                    });
                }
                else
                {
                    object check = dbHelper.GetValue(sqlCheckcount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                    DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                    DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                });
                    if (BusinessUtility.GetInt(check) > 0)
                    {
                        dbHelper.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                    DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                    });
                    }
                    else
                    {
                        dbHelper.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("addressItem", 1, MyDbType.Int),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                        DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                    });
                    }
                }
                return true;
            }
            catch
            {
                throw;
            }
        }


        public bool SaveCustomerAddresse()
        {
            DbHelper dbHelp = new DbHelper(true);
            string sqlCheckcount = "SELECT COUNT(*) FROM addresses WHERE addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType AND AddressID = @addressID";

            string sqlMaxAddItem = "SELECT COALESCE(MAX(AddressItem) , 0) +1 FROM addresses WHERE addressSourceID=@addressSourceID and addressRef=@addressRef and addressType=@addressType "; //AND AddressID = @addressID

            string sqlInsert = "INSERT INTO addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, AddressItem, FirstName , LastName)";
            sqlInsert += " VALUES ( @addressRef, @addressType, @addressSourceID, @addressLine1, @addressLine2, @addressLine3, @addressCity, @addressState, @addressCountry, @addressPostalCode, @AddressItem,  @FirstName, @LastName)";

            string sqlUpdate = "UPDATE addresses SET addressRef=@addressRef, addressType=@addressType, addressSourceID=@addressSourceID, addressLine1=@addressLine1, addressLine2=@addressLine2, addressLine3=@addressLine3, addressCity=@addressCity, addressState=@addressState, addressCountry=@addressCountry, addressPostalCode=@addressPostalCode, FirstName = @FirstName, LastName = @LastName  WHERE  AddressID = @addressID";

            try
            {
                object check = dbHelp.GetValue(sqlCheckcount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                    DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                    DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                    DbUtility.GetParameter("addressID", this.AddressID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(check) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("addressID", this.AddressID, MyDbType.Int),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                    DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                    });
                }
                else
                {
                    object maxAddressItem = dbHelp.GetValue(sqlMaxAddItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                    DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                    DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                    //DbUtility.GetParameter("addressID", this.AddressID, MyDbType.Int)
                    });

                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", this._addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", this._addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", this._addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", this._addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", this._addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", this._addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", this._addressState, MyDbType.String),
                        DbUtility.GetParameter("addressSourceID", this._addressSourceID, MyDbType.Int),
                        DbUtility.GetParameter("addressRef", this._addressRef, MyDbType.String),
                        DbUtility.GetParameter("addressType", this._addressType, MyDbType.String),
                        DbUtility.GetParameter("addressItem", BusinessUtility.GetInt(maxAddressItem), MyDbType.Int),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                    DbUtility.GetParameter("LastName", this.LastName, MyDbType.String)
                        
                    });
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void GetAddress(int addressID)
        {
            DbHelper dbHelp = new DbHelper(true);
            string strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM addresses WHERE addressID = @addressID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[]{ 
                                     DbUtility.GetParameter("addressID", addressID, MyDbType.Int)
                                 });
                if (dr.Read())
                {
                    this._addressID = BusinessUtility.GetInt(dr["addressID"]);
                    this._addressLine1 = BusinessUtility.GetString(dr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(dr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(dr["addressLine3"]);
                    this._addressType = BusinessUtility.GetString(dr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(dr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(dr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(dr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(dr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void GetOrderAddress(int ordID, string addType)
        {
            DbHelper dbHelp = new DbHelper(true);
            string strSQL = "SELECT  addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM orderaddress WHERE ordID = @ordID AND addType = @addType";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[]{ 
                                     DbUtility.GetParameter("ordID", ordID, MyDbType.Int),
                                     DbUtility.GetParameter("addType", addType, MyDbType.String),
                                 });
                if (dr.Read())
                {
                    //this._addressID = BusinessUtility.GetInt(dr["addressID"]);
                    this._addressLine1 = BusinessUtility.GetString(dr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(dr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(dr["addressLine3"]);
                    //this._addressType = BusinessUtility.GetString(dr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(dr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(dr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(dr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(dr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }


        public void GetCustomerAddress(int custID, string addType)
        {
            DbHelper dbHelp = new DbHelper(true);
            string strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, FirstName, LastName FROM addresses WHERE addressSourceID = @addressSourceID AND addressType = @addressType ORDER BY addressItem LIMIT 1";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[]{ 
                                     DbUtility.GetParameter("addressSourceID", custID, MyDbType.Int),
                                      DbUtility.GetParameter("addressType", addType, MyDbType.String)
                                 });
                if (dr.Read())
                {
                    this.AddressID = BusinessUtility.GetInt(dr["addressID"]);
                    this._addressLine1 = BusinessUtility.GetString(dr["addressLine1"]);
                    this._addressLine2 = BusinessUtility.GetString(dr["addressLine2"]);
                    this._addressLine3 = BusinessUtility.GetString(dr["addressLine3"]);
                    this._addressType = BusinessUtility.GetString(dr["addressType"]);
                    this._addressCity = BusinessUtility.GetString(dr["addressCity"]);
                    this._addressCountry = BusinessUtility.GetString(dr["addressCountry"]);
                    this._addressState = BusinessUtility.GetString(dr["addressState"]);
                    this._addressPostalCode = BusinessUtility.GetString(dr["addressPostalCode"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                dbHelp.CloseDatabaseConnection();
            }
        }


        public string ToHtml()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(!string.IsNullOrEmpty(this.AddressLine1) ? this.AddressLine1 + "<br />" : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressLine2) ? this.AddressLine2 + "<br />" : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressLine3) ? this.AddressLine3 + "<br />" : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressCity) ? this.AddressCity + "," : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressState) ? this.AddressState + "<br />" : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressCountry) ? this.AddressCountry + "," : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressPostalCode) ? this.AddressPostalCode : "");

            return sb.ToString().TrimEnd(',');
        }


        public string GetSql(ParameterCollection pCol, int customerID, string addressType)
        {
            pCol.Clear();
            pCol.Add("@customerID", customerID.ToString());
            pCol.Add("@addressType", addressType.ToString());
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT addressID, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, addressItem, FirstName, LastName ");
            sbQuery.Append(" FROM Addresses WHERE addressSourceID = @customerID  AND addressType = @addressType AND ( addressRef = 'D' OR  addressRef = 'E' ) ");

            return sbQuery.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(!string.IsNullOrEmpty(this.AddressLine1) ? this.AddressLine1 + Environment.NewLine : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressLine2) ? this.AddressLine2 + Environment.NewLine : "");
            sb.Append(!string.IsNullOrEmpty(this.AddressLine3) ? this.AddressLine3 + Environment.NewLine : "");
            sb.Append(this.AddressCity + string.Format(" ({0})", this.AddressState) + Environment.NewLine);
            sb.Append(this.AddressCountry + Environment.NewLine);
            sb.Append(!string.IsNullOrEmpty(this.AddressPostalCode) ? "Postal Code: " + this.AddressPostalCode : "");

            return sb.ToString();
        }

        #endregion
    }
}
