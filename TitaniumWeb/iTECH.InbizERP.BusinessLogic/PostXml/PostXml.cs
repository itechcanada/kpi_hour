﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PostXml
    {
        public int PostXmlID { get; set; }
        public DateTime PostedDate { get; set; }
        public string PostedXmlData { get; set; }
        public PostType PostType { get; set; }
        public PostDirection PostDirection { get; set; }
        public string Response { get; set; }

        //Return postXmlID PK
        public int SetPostedData(DbHelper dbHelp, string xmlData, PostType postType, PostDirection postDirection)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_post_xml(PostedDate,PostedXmlData,PostType,PostDirection) VALUES(@PostedDate,@PostedXmlData,@PostType,@PostDirection)";
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("PostedDate", DateTime.Now, MyDbType.DateTime),
                DbUtility.GetParameter("PostedXmlData", xmlData, MyDbType.String),
                DbUtility.GetParameter("PostType", postType.ToString(), MyDbType.String),
                DbUtility.GetParameter("PostDirection", postDirection.ToString(), MyDbType.String)
            });
                return dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void SetResponse(DbHelper dbHelp, string response, int xmlPostID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE z_post_xml SET Response=@Response WHERE PostXmlID=@PostXmlID";
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PostXmlID", xmlPostID, MyDbType.Int),
                    DbUtility.GetParameter("Response", response, MyDbType.String)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetResponse(string urlToPost, string dataToPost)
        {
            //Logic below for evo post
            string url = urlToPost;
            string strResponseData = string.Empty;
            try
            {                
                //Setup the http request.
                HttpWebRequest wrWebRequest = WebRequest.Create(url) as HttpWebRequest;
                wrWebRequest.Method = "POST";
                wrWebRequest.ContentLength = dataToPost.Length;
                wrWebRequest.ContentType = "text/xml";
                CookieContainer cookieContainer = new CookieContainer();
                wrWebRequest.CookieContainer = cookieContainer;

                using (StreamWriter swRequestWriter = new StreamWriter(wrWebRequest.GetRequestStream()))
                {
                    swRequestWriter.Write(dataToPost);
                    swRequestWriter.Close();

                    // Get the response.
                    HttpWebResponse hwrWebResponse = (HttpWebResponse)wrWebRequest.GetResponse();

                    //Read the response
                    using (StreamReader srResponseReader = new StreamReader(hwrWebResponse.GetResponseStream()))
                    {
                        strResponseData = srResponseReader.ReadToEnd();
                    }
                }
                return strResponseData;
            }
            catch (Exception ex)
            {
                return ex.Message + Environment.NewLine + ex.StackTrace;
            }
            finally
            {
                url = null;
            }
        }

        public void PopulateObject(DbHelper dbHelp, int xmlPostID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_post_xml WHERE PostXmlID=@PostXmlID";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("PostXmlID", xmlPostID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.PostDirection = (BusinessLogic.PostDirection)Enum.Parse(typeof(BusinessLogic.PostDirection), BusinessUtility.GetString(dr["PostDirection"]));
                        this.PostedDate = BusinessUtility.GetDateTime(dr["PostedDate"]);
                        this.PostedXmlData = BusinessUtility.GetString(dr["PostedXmlData"]);
                        this.PostType = (BusinessLogic.PostType)Enum.Parse(typeof(BusinessLogic.PostType), BusinessUtility.GetString(dr["PostType"]));
                        this.PostXmlID = BusinessUtility.GetInt(dr["PostXmlID"]);
                        this.Response = BusinessUtility.GetString(dr["Response"]);                        
                    }
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public enum PostType
    {
        /// <summary>
        /// Purchase Order
        /// </summary>
        PO,
        /// <summary>
        /// Invoice
        /// </summary>
        IN
    }

    public enum PostDirection
    {
        /// <summary>
        /// Internal
        /// </summary>
        INT,
        /// <summary>
        /// External
        /// </summary>
        EXT
    }

    public class MyXmlReader
    {
        XmlDocument xmlDoc = new XmlDocument();

        public MyXmlReader(string xmlString)
        {
            xmlDoc.LoadXml(xmlString);
        }

        //Get Node Text
        public string GetNodeText(string node)
        {
            XmlNode xmlNode = xmlDoc.SelectSingleNode(node);
            return xmlNode.InnerText;
        }

        public XmlNode GetNode(string node)
        {
            return xmlDoc.SelectSingleNode(node);
        }

        //Get Node List
        public string[] GetNodeListArray(string node)
        {
            XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);
            List<string> lstNodes = new List<string>();
            foreach (XmlNode xmlNode in xmlNodeList)
            {
                if (xmlNode.ChildNodes.Count > 0)
                {
                    lstNodes.Add(xmlNode.ChildNodes[0].InnerText);
                }
            }
            return lstNodes.ToArray();
        }

        public XmlNodeList GetNodeList(string node)
        {
            return xmlDoc.SelectNodes(node);
        }

        //Get Node Count
        public int GetNodeCount(string node)
        {
            XmlNodeList xmlNodeList = default(XmlNodeList);
            xmlNodeList = xmlDoc.SelectNodes(node);

            int Count = xmlNodeList.Count;
            return Count;
        }

        //Get Node Attribute
        public string GetNodeAttribute(string node, int item, string attribute)
        {
            XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);

            XmlNode xmlNode = xmlNodeList.Item(item);
            if ((xmlNode.Attributes[attribute] != null))
            {
                string Value = xmlNode.Attributes[attribute].Value;
                return Value;
            }
            return "";
        }

        //Get Node Child
        public string GetNodeChild(string node, int item, string child)
        {
            XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);

            XmlNode xmlNode = xmlNodeList.Item(item);
            if ((xmlNode[child] != null))
            {
                string Value = xmlNode[child].InnerText;
                return Value;
            }
            return "";
        }
    }
}
