﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PreAccountRcv
    {
        public int RcvID { get; set; }
        public int OrderID { get; set; }
        public int CustomerID { get; set; }
        public double AmountDeposit { get; set; }
        public double WriteOffAmount { get; set; }
        public int PaymentMethod { get; set; }
        public DateTime DateReceived { get; set; }
        public string ReceiptNo { get; set; }
        public string Notes { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_pre_account_rcv (OrderID,CustomerID,AmountDeposit,WriteOffAmount,PaymentMethod,DateReceived,ReceiptNo,Notes) VALUES(@OrderID,@CustomerID,@AmountDeposit,@WriteOffAmount,@PaymentMethod,@DateReceived,@ReceiptNo,@Notes)";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                    DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                    DbUtility.GetParameter("AmountDeposit", this.AmountDeposit, MyDbType.Double),
                    DbUtility.GetParameter("PaymentMethod", this.PaymentMethod, MyDbType.Int),
                    DbUtility.GetParameter("DateReceived", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ReceiptNo", this.ReceiptNo, MyDbType.String),
                    DbUtility.GetParameter("Notes", this.Notes, MyDbType.String),
                    DbUtility.GetParameter("WriteOffAmount", this.WriteOffAmount, MyDbType.Double)
                });
                this.RcvID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " UPDATE z_pre_account_rcv SET AmountDeposit = @AmountDeposit, DateReceived = @DateReceived WHERE RcvID = @RcvID ";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RcvID", this.RcvID, MyDbType.Int),
                    DbUtility.GetParameter("AmountDeposit", this.AmountDeposit, MyDbType.Double),
                    DbUtility.GetParameter("DateReceived", DateTime.Now, MyDbType.DateTime),
                });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public int GetRcvID(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " SELECT RcvID FROM z_pre_account_rcv WHERE OrderID = @OrderID AND CustomerID  = @CustomerID AND PaymentMethod = @PaymentMethod";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                    DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                    DbUtility.GetParameter("PaymentMethod", this.PaymentMethod, MyDbType.Int),

                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<PreAccountRcv> GetAllByOrderID(DbHelper dbHelp, int orderID)
        {
            string sql = "SELECT * FROM z_pre_account_rcv WHERE OrderID=@OrderID";
            MySqlDataReader dr = null;

            List<PreAccountRcv> lResult = new List<PreAccountRcv>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    PreAccountRcv rs = new PreAccountRcv();
                    rs.AmountDeposit = BusinessUtility.GetDouble(dr["AmountDeposit"]);
                    rs.CustomerID = BusinessUtility.GetInt(dr["CustomerID"]);
                    rs.DateReceived = BusinessUtility.GetDateTime(dr["DateReceived"]);
                    rs.OrderID = BusinessUtility.GetInt(dr["OrderID"]);
                    rs.PaymentMethod = BusinessUtility.GetInt(dr["PaymentMethod"]);
                    rs.RcvID = BusinessUtility.GetInt(dr["RcvID"]);
                    rs.ReceiptNo = BusinessUtility.GetString(dr["ReceiptNo"]);
                    rs.Notes = BusinessUtility.GetString(dr["Notes"]);
                    rs.WriteOffAmount = BusinessUtility.GetDouble(dr["WriteOffAmount"]);
                    lResult.Add(rs);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public DataTable GetOrderPartialPaymentList(int orderID, string langID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT * FROM z_pre_account_rcv a ");
            sbQuery.Append(" INNER JOIN  (   ");
            sbQuery.Append(" SELECT sysAppDesc,sysAppLogicCode   ");
            sbQuery.Append(" FROM sysstatus where  sysAppCodeLang =@lang and sysAppCodeActive=1 and sysAppPfx='AR' and sysAppCode='dlRcv'   ");
            sbQuery.Append(" order by sysAppCodeSeq) AS pMode On pMode.sysAppLogicCode =  a.PaymentMethod  ");
            sbQuery.Append(" WHERE OrderID=@OrderID ");
            try
            {
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int),
                    DbUtility.GetParameter("lang", langID, MyDbType.String)
                });
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void MoveToActualAR(DbHelper dbHelp, int userID, int orderNo, int invoiceNo)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO accountreceivable (ARInvoiceNo,ARAmtRcvd,ARAmtRcvdDateTime,ARAmtRcvdVia,ARRcvdBy,ARWriteOff,ARReceiptNo,ARNote) ";
            sql += "SELECT {0} AS ARInvoiceNo, AmountDeposit AS ARAmtRcvd, DateReceived AS ARAmtRcvdDateTime, PaymentMethod AS ARAmtRcvdVia, {1} AS ARRcvdBy,";
            sql += " IFNULL(WriteOffAmount, 0) AS ARWriteOff, ReceiptNo AS ARReceiptNo, Notes AS ARNote FROM z_pre_account_rcv WHERE OrderID={2}";
            sql = string.Format(sql, invoiceNo, userID, orderNo);

            string sqlDeleteFromAdvPayments = string.Format("DELETE FROM z_pre_account_rcv WHERE OrderID={0}", orderNo);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, null);
                dbHelp.ExecuteNonQuery(sqlDeleteFromAdvPayments, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
