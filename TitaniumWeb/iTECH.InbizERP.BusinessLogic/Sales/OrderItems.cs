﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderItems
    {
        #region Property
        public int OrderItemID { get; set; }
        public int OrdID { get; set; }
        public int OrdProductID { get; set; }
        public int OrdProductTaxGrp { get; set; }
        public int OrdProductDiscount { get; set; }
        public double OrdProductQty { get; set; }
        public double OrdProductUnitPrice { get; set; }
        public string OrderItemDesc { get; set; }
        public string OrdProductDiscountType { get; set; }
        public int OrdGuestID { get; set; }
        public bool OrdIsItemCanceled { get; set; }
        public int OrdReservationItemID { get; set; }

        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }

        #endregion

        #region Save Methods

        public void AddOrderItems(DbHelper dbHelp, List<OrderItems> items)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                foreach (OrderItems item in items)
                {
                    item.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddReservationItems(DbHelper dbHelp, int soID, int reservationID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ReservationItems> rsvItems = new ReservationItems().GetListByReservationID(dbHelp, reservationID);

                foreach (var item in rsvItems)
                {
                    OrderItems oitem = new OrderItems();
                    oitem.OrderItemDesc = item.ToString();
                    oitem.OrdID = soID;
                    oitem.OrdProductDiscount = 0;
                    oitem.OrdProductDiscountType = "P";
                    oitem.OrdProductID = item.BedID;
                    oitem.OrdProductQty = item.TotalNights;
                    oitem.OrdProductTaxGrp = 0;//primaryPartner.PartnerTaxCode; //Do not apply any tax for reservation
                    oitem.OrdProductUnitPrice = item.PricePerDay;
                    oitem.OrdGuestID = item.GuestID;
                    oitem.OrdReservationItemID = item.ReservationItemID;
                    oitem.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddReservationItems(DbHelper dbHelp, int soID, int reservationID, int customerID, string whsCode, List<ReservationServices> lstServices)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ReservationItems> rsvItems = new ReservationItems().GetListByReservationID(dbHelp, reservationID);

                foreach (var item in rsvItems)
                {
                    OrderItems oitem = new OrderItems();
                    oitem.OrderItemDesc = item.ToString();
                    oitem.OrdID = soID;
                    oitem.OrdProductDiscount = 0;
                    oitem.OrdProductDiscountType = "P";
                    oitem.OrdProductID = item.BedID;
                    oitem.OrdProductQty = item.TotalNights;
                    oitem.OrdProductTaxGrp = 0;//primaryPartner.PartnerTaxCode; //apply 
                    oitem.OrdProductUnitPrice = item.PricePerDay;
                    oitem.OrdGuestID = item.GuestID;
                    oitem.OrdReservationItemID = item.ReservationItemID;
                    oitem.Insert(dbHelp);
                }

                //Add Service Products
                foreach (var item in lstServices)
                {
                    OrderItems oitem = new OrderItems();
                    oitem.OrderItemDesc = item.ProcessDesc;
                    oitem.OrdID = soID;
                    oitem.OrdProductDiscount = 0;
                    oitem.OrdProductDiscountType = "P";
                    oitem.OrdProductID = item.ProcessID;
                    oitem.OrdProductQty = item.ProcessUnits;
                    oitem.OrdProductTaxGrp = 0;//primaryPartner.PartnerTaxCode;
                    oitem.OrdProductUnitPrice = item.ProcessCost;
                    oitem.OrdGuestID = customerID;
                    oitem.OrdReservationItemID = 0;
                    oitem.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO orderitems(ordID, ordProductID, ordProductQty, ordProductUnitPrice,ordProductTaxGrp,ordProductDiscount, ";
            sql += " orderItemDesc, ordProductDiscountType,ordReservationItemID, ordGuestID, ordIsItemCanceled) ";
            sql += " VALUES(@ordID, @ordProductID, @ordProductQty, @ordProductUnitPrice,@ordProductTaxGrp,@ordProductDiscount,";
            sql += " @orderItemDesc, @ordProductDiscountType,@ordReservationItemID,@ordGuestID,@ordIsItemCanceled)";
            try
            {
                int? taxCode = this.OrdProductTaxGrp > 0 ? (int?)this.OrdProductTaxGrp : null;
                int? guestID = this.OrdGuestID > 0 ? (int?)this.OrdGuestID : null;
                int? rsvItemID = this.OrdReservationItemID > 0 ? (int?)this.OrdReservationItemID : null;
                //Insert Iten to OrderITems Table
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ordID", this.OrdID, MyDbType.Int),
                        DbUtility.GetParameter("ordProductID", this.OrdProductID, MyDbType.Int),
                        DbUtility.GetParameter("ordProductQty", this.OrdProductQty, MyDbType.Double),
                        DbUtility.GetParameter("ordProductUnitPrice", this.OrdProductUnitPrice, MyDbType.Double),
                        DbUtility.GetIntParameter("ordProductTaxGrp", taxCode),
                        DbUtility.GetParameter("ordProductDiscount", this.OrdProductDiscount, MyDbType.Int),
                        DbUtility.GetParameter("orderItemDesc", this.OrderItemDesc, MyDbType.String),
                        DbUtility.GetParameter("ordProductDiscountType", this.OrdProductDiscountType, MyDbType.String),
                        DbUtility.GetIntParameter("ordReservationItemID", rsvItemID),
                        DbUtility.GetIntParameter("ordGuestID", guestID),
                        DbUtility.GetParameter("ordIsItemCanceled", false, MyDbType.Boolean)
                    });

                this.OrderItemID = dbHelp.GetLastInsertID();

                if (this.OrderItemID > 0)
                {
                    this.UpdateTax(dbHelp, this.OrderItemID, this.OrdProductTaxGrp);
                }

                //Adjust Product Quantity in Inventory Commented on 2012-12-20 Now we are retreiving this infor from orders directly based on its status
                //if (adjustProductQtyInInventory)
                //{
                //    ProductQuantity pQty = new ProductQuantity();
                //    pQty.UpdateProductQuantityForQuoat(dbHelp, this.OrdProductID, whsCode, this.OrdProductQty, this.OrdProductTaxGrp);
                //}
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateTax(DbHelper dbHelp, int orderItemID, int taxGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE orderitems SET ordProductTaxGrp=@TaxGroupID, Tax1=@Tax1, TaxDesc1=@TaxDesc1,";
                sql += "Tax2=@Tax2, TaxDesc2=@TaxDesc2,Tax3=@Tax3, TaxDesc3=@TaxDesc3,Tax4=@Tax4, TaxDesc4=@TaxDesc4,";
                sql += "Tax5=@Tax5,TaxDesc5=@TaxDesc5, TaxCalculated1=@TaxCalculated1,TaxCalculated2=@TaxCalculated2,TaxCalculated3=@TaxCalculated3,TaxCalculated4=@TaxCalculated4,TaxCalculated5=@TaxCalculated5 WHERE orderItemID=@OrderItemID";
                string sqlAmount = "SELECT TaxGroupID, Amount FROM  vw_order_item_amount WHERE OrderItemID=@OrderItemID";
                Tax tx = new Tax();
                double amount = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderItemID", orderItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        amount = BusinessUtility.GetDouble(dr["Amount"]);
                        if (taxGroupID <= 0)
                        {
                            taxGroupID = BusinessUtility.GetInt(dr["TaxGroupID"]);
                        }
                    }
                }                
                if (taxGroupID > 0)
                {
                    tx.PopulateObject(dbHelp, taxGroupID, amount);
                }
                int? txGrp = taxGroupID > 0 ? (int?)taxGroupID : null;                
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("TaxGroupID", txGrp),
                    DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                    DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                    DbUtility.GetParameter("Tax3", tx.Tax3, MyDbType.Double),
                    DbUtility.GetParameter("Tax4", tx.Tax4, MyDbType.Double),
                    DbUtility.GetParameter("Tax5", tx.Tax5, MyDbType.Double),
                    DbUtility.GetParameter("TaxDesc1", tx.TaxDesc1, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc2", tx.TaxDesc2, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc3", tx.TaxDesc3, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc4", tx.TaxDesc4, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc5", tx.TaxDesc5, MyDbType.String),
                    DbUtility.GetParameter("OrderItemID", orderItemID, MyDbType.Int),
                    DbUtility.GetParameter("TaxCalculated1", tx.TaxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", tx.TaxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", tx.TaxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", tx.TaxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", tx.TaxCalculated5, MyDbType.Double)
                });

                //Set Tax Info with current object 
                this.OrdProductTaxGrp = taxGroupID;
                this.Tax1 = tx.Tax1;
                this.Tax2 = tx.Tax2;
                this.Tax3 = tx.Tax3;
                this.Tax4 = tx.Tax4;
                this.Tax5 = tx.Tax5;
                this.TaxDesc1 = tx.TaxDesc1;
                this.TaxDesc2 = tx.TaxDesc2;
                this.TaxDesc3 = tx.TaxDesc3;
                this.TaxDesc4 = tx.TaxDesc4;
                this.TaxDesc5 = tx.TaxDesc5;
                this.TaxCalculated1 = tx.TaxCalculated1;
                this.TaxCalculated2 = tx.TaxCalculated2;
                this.TaxCalculated3 = tx.TaxCalculated3;
                this.TaxCalculated4 = tx.TaxCalculated4;
                this.TaxCalculated5 = tx.TaxCalculated5;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlIsTaxChanged = "SELECT COUNT(*) FROM orderitems WHERE orderItemID=@OrderItemID AND IFNULL(ordProductTaxGrp, 0)=@NewTaxGroup";
            string sqlUpdate = " UPDATE orderitems set ordProductQty=@ordProductQty, ordProductUnitPrice=@ordProductUnitPrice,";
            sqlUpdate += " ordProductTaxGrp=@ordProductTaxGrp,ordProductDiscount=@ordProductDiscount,orderItemDesc=@orderItemDesc,";
            sqlUpdate += " ordProductDiscountType=@ordProductDiscountType WHERE orderItemID=@orderItemID";
            try
            {
                //var oTax = dbHelp.GetValue(sqlIsTaxChanged, CommandType.Text, new MySqlParameter[] { 
                //    DbUtility.GetParameter("OrderItemID", this.OrderItemID, MyDbType.Int),
                //    DbUtility.GetParameter("NewTaxGroup", this.OrdProductTaxGrp, MyDbType.Int)
                //});
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordProductQty", this.OrdProductQty, MyDbType.Double),
                    DbUtility.GetParameter("ordProductUnitPrice", this.OrdProductUnitPrice, MyDbType.Double),
                    DbUtility.GetParameter("ordProductTaxGrp", this.OrdProductTaxGrp, MyDbType.Int),
                    DbUtility.GetParameter("ordProductDiscount", this.OrdProductDiscount, MyDbType.Int),
                    DbUtility.GetParameter("orderItemDesc", this.OrderItemDesc, MyDbType.String),
                    DbUtility.GetParameter("ordProductDiscountType", this.OrdProductDiscountType, MyDbType.String),
                    DbUtility.GetParameter("orderItemID", this.OrderItemID, MyDbType.Int)
                });
                this.UpdateTax(dbHelp, this.OrderItemID, this.OrdProductTaxGrp); //Do not chck for tax code upgradation just update whatever
                //if (BusinessUtility.GetInt(oTax) == 0) //Tax was updated need to update tax information in order item
                //{
                //    this.UpdateTax(dbHelp, this.OrderItemID, this.OrdProductTaxGrp);
                //}
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void ChangeGuest(DbHelper dbHelp, int guestID, int reservationItemID)
        {
            string sql = "UPDATE orderitems SET ordGuestID=@ordGuestID WHERE ordReservationItemID=@ordReservationItemID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordGuestID", guestID, MyDbType.Int),
                    DbUtility.GetParameter("ordReservationItemID", reservationItemID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
        }

        public void CancelReservationItem(int orderItemID)
        {
            string sqlOrderItem = "UPDATE orderitems oi,z_reservation_items ri  SET oi.ordProductUnitPrice=0, oi.ordIsItemCanceled=1,ri.PricePerDay=0, ri.IsCanceled=1  ";
            sqlOrderItem += " WHERE ri.ReservationItemID=oi.ordReservationItemID AND oi.orderItemID=@orderItemID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlOrderItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemID", orderItemID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        public void Delete(int orderItemID)
        {
            string sqlDelete = "DELETE FROM orderitems WHERE orderItemID=@orderItemID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemID", orderItemID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Retreive Methods
        public string GetItemsSql(ParameterCollection pCol, int soID)
        {
            pCol.Clear();
            pCol.Add("@ordID", soID.ToString());
            string strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
            strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
            strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType, i.ordGuestID FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp where i.ordID=@ordID";
            return strSQL;
        }

        //public DataTable GetOrderItems(DbHelper dbHelp, int soid)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }

        //    string strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
        //    strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
        //    strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType, i.ordGuestID, ri.ReservationID, ri.ReservationItemID, i.ordID,i.ordIsItemCanceled, ";
        //    strSQL += " o.ordDiscount, o.ordDiscountType";
        //    strSQL += " FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp ";
        //    strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.ordReservationItemID";
        //    strSQL += "  WHERE i.ordID=@ordID";
        //    try
        //    {
        //        return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", soid, MyDbType.Int) });
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public IDataReader GetOrderItemsReader(DbHelper dbHelp, int soid)
        {
            //string strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
            //strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
            //strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType, i.ordGuestID, ri.ReservationID, ri.ReservationItemID, i.ordID,i.ordIsItemCanceled, ";
            //strSQL += " o.ordDiscount, o.ordDiscountType,p.prdName AS OriginalPrdName, p.prdType AS ProductType, prt.PartnerLongName,";
            //strSQL += " i.Tax1,i.Tax2,i.Tax3,i.Tax4,i.Tax5,i.TaxCalculated1,i.TaxCalculated2,i.TaxCalculated3,i.TaxCalculated4,i.TaxCalculated5,";
            //strSQL += "i.TaxDesc1,i.TaxDesc2,i.TaxDesc3,i.TaxDesc4,i.TaxDesc5, pc.Color{0} AS Color, Size{0} AS Size ";
            //strSQL += " FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp ";
            //strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.ordReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.ordGuestID";
            //strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = p.SizeID INNER JOIN ProductColor AS pc ON pc.ColorID = p.ColID ";
            //strSQL += "  WHERE i.ordID=@ordID";
            string strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
            strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
            strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType, i.ordGuestID, ri.ReservationID, ri.ReservationItemID, i.ordID,i.ordIsItemCanceled, ";
            strSQL += " o.ordDiscount, o.ordDiscountType,p.prdName AS OriginalPrdName, p.prdType AS ProductType, prt.PartnerLongName,";
            strSQL += " i.Tax1,i.Tax2,i.Tax3,i.Tax4,i.Tax5,i.TaxCalculated1,i.TaxCalculated2,i.TaxCalculated3,i.TaxCalculated4,i.TaxCalculated5,";
            strSQL += "i.TaxDesc1,i.TaxDesc2,i.TaxDesc3,i.TaxDesc4,i.TaxDesc5, pc.Color{0} AS Color, Size{0} AS Size ";
            strSQL += " FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp ";
            strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.ordReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.ordGuestID ";
            strSQL += " INNER JOIN  ProductClothDesc AS PsDsc ON PsDsc.ProductID = p.productID ";
            strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = PsDsc.Size INNER JOIN ProductColor AS pc ON pc.ColorID = PsDsc.Color ";
            strSQL += "  WHERE i.ordID=@ordID";

            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);

            try
            {
                return dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", soid, MyDbType.Int) });
            }
            catch
            {
                throw;
            }
        }


        public DataTable GetOrderItemsTable(DbHelper dbHelp, int soid)
        {
            string strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
            strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
            //strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice ) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice ) ELSE (ordProductQty * ordProductUnitPrice ) - ((ordProductQty * ordProductUnitPrice ) * i.ordProductDiscount/100) END End as  amount, ";
            strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType, i.ordGuestID, ri.ReservationID, ri.ReservationItemID, i.ordID,i.ordIsItemCanceled, ";
            strSQL += " o.ordDiscount, o.ordDiscountType,p.prdName AS OriginalPrdName, p.prdType AS ProductType, prt.PartnerLongName,";
            strSQL += " i.Tax1,i.Tax2,i.Tax3,i.Tax4,i.Tax5,i.TaxCalculated1,i.TaxCalculated2,i.TaxCalculated3,i.TaxCalculated4,i.TaxCalculated5,";
            strSQL += "i.TaxDesc1,i.TaxDesc2,i.TaxDesc3,i.TaxDesc4,i.TaxDesc5, pc.Color{0} AS Color, Size{0} AS Size, (select Sum(ordProductQty) from orders ord join orderitems oi on oi.ordID = ord.ordID where ParentOrderID = @ordID and oi.ordProductID = p.productID) as BalanceQty ";
            strSQL += " FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp ";
            strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.ordReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.ordGuestID ";
            strSQL += " INNER JOIN  ProductClothDesc AS PsDsc ON PsDsc.ProductID = p.productID ";
            strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = PsDsc.Size INNER JOIN ProductColor AS pc ON pc.ColorID = PsDsc.Color ";
            strSQL += "  WHERE i.ordID=@ordID";

            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", soid, MyDbType.Int) });
            }
            catch
            {
                throw;
            }
        }


        public int GetOrderItemID(DbHelper dbHelp, int orderid, int productID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT orderItemID FROM orderitems WHERE ordID=@ordID AND ordProductID=@ordProductID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", orderid, MyDbType.Int),
                    DbUtility.GetParameter("ordProductID", productID, MyDbType.Int) 
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string ItemDescription(DbHelper dbHelp, int reservationItemID)
        {
            string sql = "SELECT orderItemDesc FROM orderitems WHERE ordReservationItemID=@ordReservationItemID LIMIT 1";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordReservationItemID", reservationItemID, MyDbType.Int)
                });

                return BusinessUtility.GetString(val);
            }
            catch
            {

                throw;
            }
        }

        public void PopulateObject(DbHelper dbHelp, int orderItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECt * FROM orderitems WHERE orderItemID=@orderItemID";
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemID", orderItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.OrderItemID = BusinessUtility.GetInt(dr["orderItemID"]);
                        this.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                        this.OrdProductID = BusinessUtility.GetInt(dr["ordProductID"]);
                        this.OrdProductQty = BusinessUtility.GetDouble(dr["ordProductQty"]);

                        this.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]);
                        this.OrdProductTaxGrp = BusinessUtility.GetInt(dr["ordProductTaxGrp"]);
                        this.OrdProductDiscount = BusinessUtility.GetInt(dr["ordProductDiscount"]);
                        this.OrderItemDesc = BusinessUtility.GetString(dr["orderItemDesc"]);
                        this.OrdProductDiscountType = BusinessUtility.GetString(dr["ordProductDiscountType"]);
                        this.OrdGuestID = BusinessUtility.GetInt(dr["ordGuestID"]);
                        this.OrdIsItemCanceled = BusinessUtility.GetBool(dr["ordIsItemCanceled"]);

                        this.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        this.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        this.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        this.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        this.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);

                        this.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        this.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        this.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        this.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        this.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    }
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }




        public void PopulateReservationOrderItem(DbHelper dbHelp, int rsvItemId)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECt orderItemID FROM orderitems WHERE ordReservationItemID=@ordReservationItemID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordReservationItemID", rsvItemId, MyDbType.Int) });
                this.PopulateObject(dbHelp, BusinessUtility.GetInt(val));
                //using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                //    DbUtility.GetParameter("ordReservationItemID", rsvItemId, MyDbType.Int)
                //}))
                //{
                //    if (dr.Read())
                //    {
                //        this.OrderItemID = BusinessUtility.GetInt(dr["orderItemID"]);
                //        this.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                //        this.OrdProductID = BusinessUtility.GetInt(dr["ordProductID"]);
                //        this.OrdProductQty = BusinessUtility.GetDouble(dr["ordProductQty"]);

                //        this.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]);
                //        this.OrdProductTaxGrp = BusinessUtility.GetInt(dr["ordProductTaxGrp"]);
                //        this.OrdProductDiscount = BusinessUtility.GetInt(dr["ordProductDiscount"]);
                //        this.OrderItemDesc = BusinessUtility.GetString(dr["orderItemDesc"]);
                //        this.OrdProductDiscountType = BusinessUtility.GetString(dr["ordProductDiscountType"]);
                //        this.OrdGuestID = BusinessUtility.GetInt(dr["ordGuestID"]);
                //        this.OrdIsItemCanceled = BusinessUtility.GetBool(dr["ordIsItemCanceled"]);
                //        this.OrdReservationItemID = BusinessUtility.GetInt(dr["ordReservationItemID"]);
                //    }
                //}
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        public double GetItemSubTotal(int ordeItemID)
        {
            string sql = "SELECT CASE i.ordProductDiscountType WHEN 'A' THEN (i.ordProductQty * i.ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount ELSE";
            sql += "   CASE i.ordProductDiscount WHEN 0 THEN (i.ordProductQty * i.ordProductUnitPrice * o.ordCurrencyExRate) ";
            sql += " ELSE (i.ordProductQty * i.ordProductUnitPrice * o.ordCurrencyExRate) - ((i.ordProductQty * i.ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END ";
            sql += " END AS  Amount";
            sql += " FROM orderitems i INNER JOIN orders o ON o.ordID=i.ordID WHERE i.orderItemID=@ItemID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ItemID", ordeItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        public List<OrderItems> GetOrderItemList(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<OrderItems> lResult = new List<OrderItems>();
            string sql = "SELECT * FROM orderitems WHERE ordID=@ordID";
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", ordID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        OrderItems it = new OrderItems();
                        it.OrderItemDesc = BusinessUtility.GetString(dr["orderItemDesc"]);
                        it.OrderItemID = BusinessUtility.GetInt(dr["orderItemID"]);
                        it.OrdGuestID = BusinessUtility.GetInt(dr["ordGuestID"]);
                        it.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                        it.OrdIsItemCanceled = BusinessUtility.GetBool(dr["ordIsItemCanceled"]);
                        it.OrdProductDiscount = BusinessUtility.GetInt(dr["ordProductDiscount"]);
                        it.OrdProductDiscountType = BusinessUtility.GetString(dr["ordProductDiscountType"]);
                        it.OrdProductID = BusinessUtility.GetInt(dr["ordProductID"]);
                        it.OrdProductQty = BusinessUtility.GetDouble(dr["ordProductQty"]);
                        it.OrdProductTaxGrp = BusinessUtility.GetInt(dr["ordProductTaxGrp"]);
                        it.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]);
                        it.OrdReservationItemID = BusinessUtility.GetInt(dr["ordReservationItemID"]);

                        it.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        it.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        it.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        it.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        it.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);

                        it.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        it.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        it.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        it.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        it.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);

                        it.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        it.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        it.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        it.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        it.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        lResult.Add(it);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        public List<OrderItems> GetOrderItemList(int ordID, int productID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<OrderItems> lResult = new List<OrderItems>();
            string sql = "SELECT * FROM orderitems WHERE ordID=@ordID AND ordProductID = @productID";
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", ordID, MyDbType.Int), DbUtility.GetParameter("productID", productID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        OrderItems it = new OrderItems();
                        it.OrderItemDesc = BusinessUtility.GetString(dr["orderItemDesc"]);
                        it.OrderItemID = BusinessUtility.GetInt(dr["orderItemID"]);
                        it.OrdGuestID = BusinessUtility.GetInt(dr["ordGuestID"]);
                        it.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                        it.OrdIsItemCanceled = BusinessUtility.GetBool(dr["ordIsItemCanceled"]);
                        it.OrdProductDiscount = BusinessUtility.GetInt(dr["ordProductDiscount"]);
                        it.OrdProductDiscountType = BusinessUtility.GetString(dr["ordProductDiscountType"]);
                        it.OrdProductID = BusinessUtility.GetInt(dr["ordProductID"]);
                        it.OrdProductQty = BusinessUtility.GetDouble(dr["ordProductQty"]);
                        it.OrdProductTaxGrp = BusinessUtility.GetInt(dr["ordProductTaxGrp"]);
                        it.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]);
                        it.OrdReservationItemID = BusinessUtility.GetInt(dr["ordReservationItemID"]);

                        it.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        it.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        it.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        it.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        it.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);

                        it.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        it.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        it.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        it.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        it.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);

                        it.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        it.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        it.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        it.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        it.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        lResult.Add(it);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        //public void Insert(string whsCode, bool adjustProductQtyInInventory)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        this.Insert(dbHelp, whsCode, adjustProductQtyInInventory);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}



        //public void MoveReservation(DbHelper dbHelp)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    string sqlUpdate = "UPDATE orderitems SET ordProductID=@ordProductID, orderItemDesc=@orderItemDesc, ordProductUnitPrice=@ordProductUnitPrice WHERE orderItemID=@orderItemID";            
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("ordProductID", this.OrdProductID, typeof(int)),                    
        //            DbUtility.GetParameter("orderItemDesc", this.OrderItemDesc, typeof(string)),
        //            DbUtility.GetParameter("orderItemID", this.OrderItemID, typeof(int)),
        //            DbUtility.GetParameter("ordProductUnitPrice", this.OrdProductUnitPrice, MyDbType.Double)
        //        });
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if(mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}





        //Required while Canceling reservation for specific bed & customer




        //public void PopulateReservationOrderItem(int rsvItemId)
        //{
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        this.PopulateReservationOrderItem(dbHelp, rsvItemId);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        #region Return Module Features
        //public void UpdateInventoryForFullReturns(DbHelper dbHelp, int ordID, int userID, List<ReturnCart> lstToReturn)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        Orders ord = new Orders();
        //        ord.PopulateObject(dbHelp, ordID);

        //        SysWarehouses whs = new SysWarehouses();
        //        whs.PopulateObject(ord.OrdShpWhsCode, dbHelp);

        //        Invoice inv = new Invoice();
        //        int invID = inv.GetInvoiceID(dbHelp, ordID);

        //        Return rt = new Return();

        //        List<Return> lstReturns = new List<Return>();
        //        string sql = "SELECT * FROM orderitems WHERE ordID=@ordID";
        //        using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
        //            DbUtility.GetParameter("ordID", ordID, MyDbType.Int)
        //        }))
        //        {
        //            while (dr.Read())
        //            {
        //                Return rtn = new Return();
        //                rtn.ReturnDate = DateTime.Now;
        //                rtn.ReturnOrderID = BusinessUtility.GetInt(dr["ordID"]);
        //                rtn.ReturnOrderItemID = BusinessUtility.GetInt(dr["orderItemID"]);
        //                rtn.ReturnProcessBy = userID;
        //                rtn.ReturnProductID = BusinessUtility.GetInt(dr["ordProductID"]);
        //                rtn.ReturnProductQty = BusinessUtility.GetDouble(dr["ordProductQty"]);
        //                rtn.ProductInOrderQty = BusinessUtility.GetDouble(dr["ordProductQty"]);
        //                lstReturns.Add(rtn);
        //            }
        //        }

        //        double returnedQty = 0;                
        //        foreach (var item in lstReturns)
        //        {
        //            returnedQty = rt.GetPreviousReturnedQty(dbHelp, item.ReturnOrderItemID);
        //            item.ReturnProductQty = item.ProductInOrderQty - returnedQty;
        //        }

        //        //Enter Returns records
        //        rt.Insert(dbHelp, lstReturns);

        //        ProductQuantity pQty = new ProductQuantity();
        //        foreach (var item in lstReturns)
        //        {
        //            if (item.ReturnProductQty > 0)
        //            {
        //                if (!string.IsNullOrEmpty(whs.ShippingWarehouse))
        //                {
        //                    pQty.UpdateQuantityOnReturn(dbHelp, item.ReturnProductID, item.ReturnProductQty, whs.ShippingWarehouse);
        //                }
        //                else
        //                {
        //                    pQty.UpdateQuantityOnReturn(dbHelp, item.ReturnProductID, item.ReturnProductQty, ord.OrdShpWhsCode);
        //                }
        //            }                    
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}                

        #endregion

        #region Post Xml Related Methods        
        public void PopulateOrderItem(DbHelper dbHelp, int orderID, int partnerID, string externalID, string prdName, double unitPrice, string whsCode, double prdQty, int txGrp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                Product prd = new Product();
                prd.PopulateProduct(dbHelp, externalID, prdName, unitPrice, whsCode, prdQty, userID);
                if (prd.ProductID > 0 && orderID > 0)
                {                    
                    this.OrderItemDesc = prdName;
                    this.OrdGuestID = partnerID;
                    this.OrdID = orderID;
                    this.OrdIsItemCanceled = false;
                    this.OrdProductDiscount = 0;
                    this.OrdProductDiscountType = "P";
                    this.OrdProductID = prd.ProductID;
                    this.OrdProductQty = prdQty;
                    this.OrdProductTaxGrp = txGrp;
                    this.OrdProductUnitPrice = unitPrice;                    
                    this.Insert(dbHelp);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}
