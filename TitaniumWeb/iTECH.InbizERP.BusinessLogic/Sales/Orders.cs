﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Orders
    {
        #region property
        public string OrdType { get; set; }
        public string OrdCustType { get; set; }
        public string OrdCreatedFromIP { get; set; }
        public string OrdStatus { get; set; }

        public bool OrdSaleWeb { get; set; }
        public string OrdComment { get; set; }
        public string OrdShpWhsCode { get; set; }
        public string OrdShpTrackNo { get; set; }
        public string OrdShpCode { get; set; }
        public string OrdCurrencyCode { get; set; }
        public double OrdCurrencyExRate { get; set; }
        public string OrdCustPO { get; set; }
        public string OrdNetTerms { get; set; }
        public int OrdCompanyID { get; set; }
        public string OrderRejectReason { get; set; }
        public int OrdID { get; set; }
        public int OrdCustID { get; set; }
        public int OrdSalesRepID { get; set; }
        public int OrdLastUpdateBy { get; set; }
        public string OrdShippingTerms { get; set; }
        public int OrdCreatedBy { get; set; }
        public int OrderTypeCommission { get; set; }
        public int InvRefNo { get; set; }
        public double OrdShpCost { get; set; }
        public bool OrdShpBlankPref { get; set; }
        public bool OrdVerified { get; set; }
        public int OrdVerifiedBy { get; set; }
        public DateTime OrdCreatedOn { get; set; }
        public DateTime OrdShpDate { get; set; }
        public DateTime OrdLastUpdatedOn { get; set; }
        public DateTime QutExpDate { get; set; }
        public DateTime InvDate { get; set; }
        public double OrdDiscount { get; set; }
        public string OrdDiscountType { get; set; }

        public int QuotePrintCounter { get; set; }
        public int SalesOrderPrintCounter { get; set; }
        public int PackingListPrintCounter { get; set; }
        public int PostedPOXmlID { get; set; }
        public string OrdShpToWhsCode { get; set; }
        public string OrdRegCode { get; set; }

        public string OrdCustName { get; set; }
        public string OrdCustSaleName { get; set; }

        public int ParentOrdID { get; set; }

        #endregion

        #region Public Functions

        //public bool Insert(int userid) 
        //{
        //    string sql = "INSERT INTO orders( ordType, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified,";
        //    sql += "ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn,";
        //    sql += "ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,";
        //    sql += "ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy,orderTypeCommission,orderRejectReason,InvRefNo, invDate,ordDiscount,ordDiscountType) ";
        //    sql += "VALUES(@ordType, @ordCustType, @ordCustID, @ordCreatedOn, @ordCreatedFromIP, @ordStatus, @ordVerified, @ordVerifiedBy,";
        //    sql += "@ordSalesRepID, @ordSaleWeb, @ordComment, @ordShpDate, @ordShpBlankPref, @ordShpWhsCode, @ordLastUpdatedOn, @ordLastUpdateBy,";
        //    sql += "@ordShpTrackNo, @ordShpCost, @ordShpCode, @ordCurrencyCode, @ordCurrencyExRate, @ordCustPO, @qutExpDate,@ordCompanyID,";
        //    sql += "@ordShippingTerms,@ordNetTerms,@ordCreatedBy,@orderTypeCommission,@orderRejectReason,@InvRefNo, @invDate,@ordDiscount,@ordDiscountType)";
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        int? invRefNo = this.InvRefNo > 0 ? (int?)this.InvRefNo : null;
        //        string disType = BusinessUtility.GetString(this.OrdDiscountType).ToUpper().Trim() == "A" ? "A" : "P";
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("ordType", this.OrdType, MyDbType.String),
        //            DbUtility.GetParameter("ordCustType", this.OrdCustType, MyDbType.String),
        //            DbUtility.GetParameter("ordCustID", this.OrdCustID, MyDbType.Int),
        //            DbUtility.GetParameter("ordCreatedOn", DateTime.Now, MyDbType.DateTime),
        //            DbUtility.GetParameter("ordCreatedFromIP", this.OrdCreatedFromIP, MyDbType.String),
        //            DbUtility.GetParameter("ordStatus", this.OrdStatus, MyDbType.String),
        //            DbUtility.GetParameter("ordVerified", this.OrdVerified, MyDbType.Boolean),
        //            DbUtility.GetParameter("ordVerifiedBy", userid, MyDbType.Int),
        //            DbUtility.GetParameter("ordSalesRepID", this.OrdSalesRepID, MyDbType.Int),
        //            DbUtility.GetParameter("ordSaleWeb", this.OrdSaleWeb, MyDbType.Boolean),
        //            DbUtility.GetParameter("ordComment", this.OrdComment, MyDbType.String),
        //            DbUtility.GetParameter("ordShpDate", this.OrdShpDate, MyDbType.DateTime),
        //            DbUtility.GetParameter("ordShpBlankPref", this.OrdShpBlankPref, MyDbType.Boolean),
        //            DbUtility.GetParameter("ordShpWhsCode", this.OrdShpWhsCode, MyDbType.String),
        //            DbUtility.GetParameter("ordLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
        //            DbUtility.GetParameter("ordLastUpdateBy", userid, MyDbType.Int),
        //            DbUtility.GetParameter("ordShpTrackNo", this.OrdShpTrackNo, MyDbType.String),
        //            DbUtility.GetParameter("ordShpCost", this.OrdShpCost, MyDbType.Double),
        //            DbUtility.GetParameter("ordShpCode", this.OrdShpCode, MyDbType.String),
        //            DbUtility.GetParameter("ordCurrencyCode", this.OrdCurrencyCode, MyDbType.String),
        //            DbUtility.GetParameter("ordCurrencyExRate", this.OrdCurrencyExRate, MyDbType.Double),
        //            DbUtility.GetParameter("ordCustPO", this.OrdCustPO, MyDbType.String),
        //            DbUtility.GetParameter("qutExpDate", this.QutExpDate, MyDbType.DateTime),
        //            DbUtility.GetParameter("ordCompanyID", this.OrdCompanyID, MyDbType.Int),
        //            DbUtility.GetParameter("ordShippingTerms", this.OrdShippingTerms, MyDbType.String),
        //            DbUtility.GetParameter("ordNetTerms", this.OrdNetTerms, MyDbType.String),
        //            DbUtility.GetParameter("ordCreatedBy", userid, MyDbType.String),
        //            DbUtility.GetParameter("orderTypeCommission", this.OrderTypeCommission, MyDbType.Int),
        //            DbUtility.GetParameter("orderRejectReason", this.OrderRejectReason, MyDbType.String),
        //            DbUtility.GetIntParameter("InvRefNo", invRefNo),
        //            DbUtility.GetParameter("invDate", this.InvDate, MyDbType.DateTime),
        //            DbUtility.GetParameter("ordDiscount", this.OrdDiscount, MyDbType.Double),
        //            DbUtility.GetParameter("ordDiscountType", disType, MyDbType.String)
        //        });

        //        this.OrdID = dbHelp.GetLastInsertID();
        //        return this.OrdID > 0;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }

        //}

        public bool Insert(DbHelper dbHelp, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO orders( ordType, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified,";
            sql += "ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn,";
            sql += "ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,";
            sql += "ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy,orderTypeCommission,orderRejectReason,InvRefNo, invDate,ordDiscount,ordDiscountType, ordShpToWhsCode, ordRegCode, ParentOrderID) ";
            sql += "VALUES(@ordType, @ordCustType, @ordCustID, @ordCreatedOn, @ordCreatedFromIP, @ordStatus, @ordVerified, @ordVerifiedBy,";
            sql += "@ordSalesRepID, @ordSaleWeb, @ordComment, @ordShpDate, @ordShpBlankPref, @ordShpWhsCode, @ordLastUpdatedOn, @ordLastUpdateBy,";
            sql += "@ordShpTrackNo, @ordShpCost, @ordShpCode, @ordCurrencyCode, @ordCurrencyExRate, @ordCustPO, @qutExpDate,@ordCompanyID,";
            sql += "@ordShippingTerms,@ordNetTerms,@ordCreatedBy,@orderTypeCommission,@orderRejectReason,@InvRefNo, @invDate,@ordDiscount,@ordDiscountType, @ordShpToWhsCode, @ordRegCode, @ParentOrderID)";
            try
            {
                int? invRefNo = this.InvRefNo > 0 ? (int?)this.InvRefNo : null;
                string disType = BusinessUtility.GetString(this.OrdDiscountType).ToUpper().Trim() == "A" ? "A" : "P";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordType", this.OrdType, MyDbType.String),
                    DbUtility.GetParameter("ordCustType", this.OrdCustType, MyDbType.String),
                    DbUtility.GetParameter("ordCustID", this.OrdCustID, MyDbType.Int),
                    DbUtility.GetParameter("ordCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ordCreatedFromIP", this.OrdCreatedFromIP, MyDbType.String),
                    DbUtility.GetParameter("ordStatus", this.OrdStatus, MyDbType.String),
                    DbUtility.GetParameter("ordVerified", this.OrdVerified, MyDbType.Boolean),
                    DbUtility.GetParameter("ordVerifiedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ordSalesRepID", this.OrdSalesRepID, MyDbType.Int),
                    DbUtility.GetParameter("ordSaleWeb", this.OrdSaleWeb, MyDbType.Boolean),
                    DbUtility.GetParameter("ordComment", this.OrdComment, MyDbType.String),
                    DbUtility.GetParameter("ordShpDate", this.OrdShpDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordShpBlankPref", this.OrdShpBlankPref, MyDbType.Boolean),
                    DbUtility.GetParameter("ordShpWhsCode", this.OrdShpWhsCode, MyDbType.String),
                    DbUtility.GetParameter("ordLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ordLastUpdateBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ordShpTrackNo", this.OrdShpTrackNo, MyDbType.String),
                    DbUtility.GetParameter("ordShpCost", this.OrdShpCost, MyDbType.Double),
                    DbUtility.GetParameter("ordShpCode", this.OrdShpCode, MyDbType.String),
                    DbUtility.GetParameter("ordCurrencyCode", this.OrdCurrencyCode, MyDbType.String),
                    DbUtility.GetParameter("ordCurrencyExRate", this.OrdCurrencyExRate, MyDbType.Double),
                    DbUtility.GetParameter("ordCustPO", this.OrdCustPO, MyDbType.String),
                    DbUtility.GetParameter("qutExpDate", this.QutExpDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordCompanyID", this.OrdCompanyID, MyDbType.Int),
                    DbUtility.GetParameter("ordShippingTerms", this.OrdShippingTerms, MyDbType.String),
                    DbUtility.GetParameter("ordNetTerms", this.OrdNetTerms, MyDbType.String),
                    DbUtility.GetParameter("ordCreatedBy", userid, MyDbType.String),
                    DbUtility.GetParameter("orderTypeCommission", this.OrderTypeCommission, MyDbType.Int),
                    DbUtility.GetParameter("orderRejectReason", this.OrderRejectReason, MyDbType.String),
                    DbUtility.GetIntParameter("InvRefNo", invRefNo),
                    DbUtility.GetParameter("invDate", this.InvDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordDiscount", this.OrdDiscount, MyDbType.Double),
                    DbUtility.GetParameter("ordDiscountType", disType, MyDbType.String),
                    DbUtility.GetParameter("ordShpToWhsCode", this.OrdShpToWhsCode, MyDbType.String),
                    DbUtility.GetParameter("ordRegCode", this.OrdRegCode, MyDbType.String),
                    DbUtility.GetParameter("ParentOrderID", this.ParentOrdID, MyDbType.Int),
                });

                this.OrdID = dbHelp.GetLastInsertID();
                return this.OrdID > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }

        }

        //public void Update(int userid)
        //{
        //    try
        //    {
        //        this.Update(null, userid);
        //    }
        //    catch
        //    {
        //        throw;
        //    }            
        //}

        public void Update(DbHelper dbHelp, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sqlUpdate = "UPDATE orders SET ordType=@ordType, ordCustType=@ordCustType, ordCustID=@ordCustID, ";
            sqlUpdate += "ordStatus=@ordStatus,ordSalesRepID=@ordSalesRepID,ordComment=@ordComment,";
            sqlUpdate += "ordShpDate=@ordShpDate,ordShpBlankPref=@ordShpBlankPref,ordShpWhsCode=@ordShpWhsCode,ordLastUpdatedOn=@ordLastUpdatedOn,";
            sqlUpdate += "ordLastUpdateBy=@ordLastUpdateBy,ordShpTrackNo=@ordShpTrackNo,ordShpCost=@ordShpCost,ordShpCode=@ordShpCode,";
            sqlUpdate += "ordCurrencyCode=@ordCurrencyCode,ordCurrencyExRate=@ordCurrencyExRate,ordCustPO=@ordCustPO,qutExpDate=@qutExpDate,";
            sqlUpdate += "ordCompanyID=@ordCompanyID,ordShippingTerms=@ordShippingTerms,ordNetTerms=@ordNetTerms,orderTypeCommission=@orderTypeCommission,";
            sqlUpdate += "orderRejectReason=@orderRejectReason,InvRefNo=@InvRefNo,invDate=@invDate,ordCreatedOn=@ordCreatedOn,ordShpToWhsCode=@ordShpToWhsCode WHERE ordID=@ordID";

            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordType", this.OrdType, MyDbType.String),
                    DbUtility.GetParameter("ordCustType", this.OrdCustType, MyDbType.String),
                    DbUtility.GetParameter("ordCustID", this.OrdCustID, MyDbType.Int),
                    DbUtility.GetParameter("ordStatus", this.OrdStatus, MyDbType.String),
                    DbUtility.GetParameter("ordSalesRepID", this.OrdSalesRepID, MyDbType.Int),
                    DbUtility.GetParameter("ordSaleWeb", this.OrdSaleWeb, MyDbType.Boolean),
                    DbUtility.GetParameter("ordComment", this.OrdComment, MyDbType.String),
                    DbUtility.GetParameter("ordShpDate", this.OrdShpDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordShpBlankPref", this.OrdShpBlankPref, MyDbType.Boolean),
                    DbUtility.GetParameter("ordShpWhsCode", this.OrdShpWhsCode, MyDbType.String),
                    DbUtility.GetParameter("ordLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ordLastUpdateBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ordShpTrackNo", this.OrdShpTrackNo, MyDbType.String),
                    DbUtility.GetParameter("ordShpCost", this.OrdShpCost, MyDbType.Double),
                    DbUtility.GetParameter("ordShpCode", this.OrdShpCode, MyDbType.String),
                    DbUtility.GetParameter("ordCurrencyCode", this.OrdCurrencyCode, MyDbType.String),
                    DbUtility.GetParameter("ordCurrencyExRate", this.OrdCurrencyExRate, MyDbType.Double),
                    DbUtility.GetParameter("ordCustPO", this.OrdCustPO, MyDbType.String),
                    DbUtility.GetParameter("qutExpDate", this.QutExpDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordCompanyID", this.OrdCompanyID, MyDbType.Int),
                    DbUtility.GetParameter("ordShippingTerms", this.OrdShippingTerms, MyDbType.String),
                    DbUtility.GetParameter("ordNetTerms", this.OrdNetTerms, MyDbType.String),
                    DbUtility.GetParameter("orderTypeCommission", this.OrderTypeCommission, MyDbType.Int),
                    DbUtility.GetParameter("orderRejectReason", this.OrderRejectReason, MyDbType.String),
                    DbUtility.GetParameter("InvRefNo", this.InvRefNo, MyDbType.Int),
                    DbUtility.GetParameter("invDate", this.InvDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ordID", this.OrdID, MyDbType.Int),
                    DbUtility.GetParameter("ordCreatedOn", this.OrdCreatedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("ordShpToWhsCode", this.OrdShpToWhsCode, MyDbType.String),
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void ApplyDiscount(int orderID, double discountToApply, string discountType)
        {
            string sql = "UPDATE orders SET ordDiscount=@ordDiscount, ordDiscountType=@ordDiscountType WHERE ordID=@ordID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", orderID, MyDbType.Int),
                    DbUtility.GetParameter("ordDiscount", discountToApply, MyDbType.Double),
                    DbUtility.GetParameter("ordDiscountType", discountType, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int ordID)
        {
            DbHelper dbHelp = new DbHelper(true);
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                this.PopulateObject(dbHelp, ordID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int ordID)
        {
            string sql = "SELECT * FROM orders WHERE ordID=@ordID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", ordID, MyDbType.Int) });
                if (dr.Read())
                {
                    this.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                    this.OrdType = BusinessUtility.GetString(dr["ordType"]);
                    this.OrdCustType = BusinessUtility.GetString(dr["ordCustType"]);
                    this.OrdCustID = BusinessUtility.GetInt(dr["ordCustID"]);
                    this.OrdCreatedOn = BusinessUtility.GetDateTime(dr["ordCreatedOn"]);
                    this.OrdCreatedFromIP = BusinessUtility.GetString(dr["ordCreatedFromIP"]);
                    this.OrdStatus = BusinessUtility.GetString(dr["ordStatus"]);
                    this.OrdVerified = BusinessUtility.GetBool(dr["ordVerified"]);
                    this.OrdVerifiedBy = BusinessUtility.GetInt(dr["ordVerifiedBy"]);
                    this.OrdSalesRepID = BusinessUtility.GetInt(dr["ordSalesRepID"]);
                    this.OrdSaleWeb = BusinessUtility.GetBool(dr["ordSaleWeb"]);
                    this.OrdComment = BusinessUtility.GetString(dr["ordComment"]);
                    this.OrdShpDate = BusinessUtility.GetDateTime(dr["ordShpDate"]);
                    this.OrdShpBlankPref = BusinessUtility.GetBool(dr["ordShpBlankPref"]);
                    this.OrdShpWhsCode = BusinessUtility.GetString(dr["ordShpWhsCode"]);
                    this.OrdLastUpdatedOn = BusinessUtility.GetDateTime(dr["ordLastUpdatedOn"]);
                    this.OrdLastUpdateBy = BusinessUtility.GetInt(dr["ordLastUpdateBy"]);
                    this.OrdShpTrackNo = BusinessUtility.GetString(dr["ordShpTrackNo"]);
                    this.OrdShpCost = BusinessUtility.GetDouble(dr["ordShpCost"]);
                    this.OrdShpCode = BusinessUtility.GetString(dr["ordShpCode"]);
                    this.OrdCurrencyCode = BusinessUtility.GetString(dr["ordCurrencyCode"]);
                    this.OrdCurrencyExRate = BusinessUtility.GetDouble(dr["ordCurrencyExRate"]);
                    this.OrdCustPO = BusinessUtility.GetString(dr["ordCustPO"]);
                    this.QutExpDate = BusinessUtility.GetDateTime(dr["qutExpDate"]);
                    this.OrdCompanyID = BusinessUtility.GetInt(dr["ordCompanyID"]);
                    this.OrdShippingTerms = BusinessUtility.GetString(dr["ordShippingTerms"]);
                    this.OrdNetTerms = BusinessUtility.GetString(dr["ordNetTerms"]);
                    this.OrdCreatedBy = BusinessUtility.GetInt(dr["ordCreatedBy"]);
                    this.OrderTypeCommission = BusinessUtility.GetInt(dr["orderTypeCommission"]);
                    this.OrderRejectReason = BusinessUtility.GetString(dr["orderRejectReason"]);
                    this.InvRefNo = BusinessUtility.GetInt(dr["InvRefNo"]);
                    this.InvDate = BusinessUtility.GetDateTime(dr["invDate"]);
                    this.OrdDiscount = BusinessUtility.GetDouble(dr["ordDiscount"]);
                    this.OrdDiscountType = BusinessUtility.GetString(dr["ordDiscountType"]);
                    this.QuotePrintCounter = BusinessUtility.GetInt(dr["QuotePrintCounter"]);
                    this.SalesOrderPrintCounter = BusinessUtility.GetInt(dr["SalesOrderPrintCounter"]);
                    this.PackingListPrintCounter = BusinessUtility.GetInt(dr["PackingListPrintCounter"]);
                    this.PostedPOXmlID = BusinessUtility.GetInt(dr["PostedPOXmlID"]);
                    this.OrdShpToWhsCode = BusinessUtility.GetString(dr["ordShpToWhsCode"]);
                    this.OrdRegCode = BusinessUtility.GetString(dr["ordRegCode"]);
                    this.ParentOrdID = BusinessUtility.GetInt(dr["ParentOrderID"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        public string GetSalesSql(ParameterCollection pCol, string searchField, string ordStatus, string searchText, int partnerID, int userid, int ordType, bool isSalesRestricted)
        {
            pCol.Clear();
            string strSQL = "";
            if (isSalesRestricted)
            {
                strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, CASE partners.PartnerType When 2 Then CONCAT_WS(' ',partners.PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',partners.PartnerLongName,'(V)') ELSE partners.PartnerLongName END as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID=@UserID  inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
                pCol.Add("@UserID", userid.ToString());
            }
            else
            {
                strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, CASE partners.PartnerType When 2 Then CONCAT_WS(' ',partners.PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',partners.PartnerLongName,'(V)') ELSE partners.PartnerLongName END as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
            }

            strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission  left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission Where 1=1 ";
            if (partnerID > 0)
            {
                strSQL += " And (PartnerID=@PartnerID OR i.ordGuestID=@PartnerID)";

                pCol.Add("@PartnerID", partnerID.ToString());
            }

            if (!string.IsNullOrEmpty(ordStatus))
            {
                strSQL += " AND o.ordStatus=@pOStatus ";
                pCol.Add("@pOStatus", ordStatus);
            }

            if (searchField == ProductSearchFields.ProductBarCode && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND pro.prdUPCCode = @searchKeyWord";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == ProductSearchFields.ProductName && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " and pro.prdName like concat( '%' , @searchKeyWord ,'%') ";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == OrderSearchFields.OrderNo && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " and o.ordID = @searchKeyWord ";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == CustomerSearchFields.ContactName && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " and (PartnerLongName like  concat( '%' , @searchKeyWord ,'%')  OR PartnerAcronyme like  concat( '%' , @searchKeyWord ,'%'))";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == OrderSearchFields.CustomerPO && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " and o.ordCustPO like Concat( '%' , @searchKeyWord ,'%')";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == CustomerSearchFields.Acronyme && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND PartnerAcronyme LIKE  Concat( '%' , @searchKeyWord ,'%')  ";
                pCol.Add("@searchKeyWord", searchText);
            }
            if (ordType > 0)
            {
                if (ordType == 1)
                {
                    //strSQL += " AND orderTypeCommission IN(1,5)";
                    strSQL += " AND orderTypeCommission=@orderTypeCommission";
                    pCol.Add("@orderTypeCommission", ordType.ToString());
                }
                else if (ordType == 6)
                { }
                else
                {
                    strSQL += " AND orderTypeCommission=@orderTypeCommission";
                    pCol.Add("@orderTypeCommission", ordType.ToString());
                }

            }
            strSQL += " AND PartnerActive=1";
            strSQL += " group by o.ordID order by o.ordID desc, CustomerName ";
            return strSQL;
        }



        public double GetProcessCost(int orderID)
        {
            string sql = "SELECT sum(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits))*o.ordCurrencyExRate as ProcessCost FROM orderitemprocess i inner join orders o on o.ordID=i.ordID where i.ordID=@ordID group by i.ordID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        //public DataTable GetSearchedProductsForQuotation(string srcField, string srcText, StatusProductType prdType, string lang)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        List<MySqlParameter> pCol = new List<MySqlParameter>();
        //        string sql = "SELECT p.productID,'' AS prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, pd.prdSmallDesc, pd.prdLargeDesc,";
        //        sql += " 0 AS prdOhdQty, p.prdEndUserSalesPrice as Price,0 AS prdTaxCode,p.prdDiscount,p.prdDiscountType";
        //        sql += "  FROM products as p INNER JOIN prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang";
        //        sql += " AND p.prdType=@prdType";

        //        pCol.Add(DbUtility.GetParameter("descLang", lang, MyDbType.String));
        //        pCol.Add(DbUtility.GetParameter("prdType", (int)prdType, MyDbType.String));

        //        string onHeadQty = "SELECT SUM(pq.prdOhdQty) AS prdOhdQty, pq.prdWhsCode, pq.prdTaxCode  FROM prdquantity pq WHERE pq.prdID=@prdID GROUP BY pq.prdID";
        //        dbHelp.OpenDatabaseConnection();

        //        if (!string.IsNullOrEmpty(srcText))
        //        {
        //            switch (srcField)
        //            {
        //                case ProductSearchFields.ProductID:
        //                    sql += " AND p.productID = @SearchText";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.Int));
        //                    break;
        //                case ProductSearchFields.ProductName:
        //                    sql += " AND p.prdName LIKE CONCAT('%', @SearchText, '%')";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    break;
        //                case ProductSearchFields.ProductInternalID:
        //                    sql += " AND p.prdIntID = @SearchText";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    break;
        //                case ProductSearchFields.ProductExternalID:
        //                    sql += " AND p.prdExtID = @SearchText";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    break;
        //                case ProductSearchFields.ProductBarCode:
        //                    sql += " AND p.prdUPCCode = @SearchText";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    break;
        //                case ProductSearchFields.ProductDescription:
        //                    sql += " AND (prdSmallDesc LIKE CONCAT('%', @SearchText, '%')  OR prdLargeDesc LIKE CONCAT('%', @SearchText, '%')";
        //                    pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    break;
        //                case ProductSearchFields.ALPHA:
        //                    if (srcText != "All")
        //                    {
        //                        sql += "  And p.prdName LIKE CONCAT(@SearchText, '%')";
        //                        pCol.Add(DbUtility.GetParameter("@SearchText", srcText, MyDbType.String));
        //                    }
        //                    break;
        //                case ProductSearchFields.MY_FAV:
        //                    sql += "   And p.prdIsSpecial = 1";
        //                    break;
        //            }
        //        }
        //        sql += "  GROUP BY p.productID order by p.prdName ";
        //        DataTable dtReslut = dbHelp.GetDataTable(sql, CommandType.Text, pCol.ToArray());
        //        if (srcField == ProductSearchFields.CUSTOMER_FAV) {
        //            string sqlCustFav = "SELECT DISTINCT oi.ordProductID FROM orders o INNER JOIN orderitems oi ON oi.ordID=o.ordID WHERE o.ordCustID=@CustID";
        //            List<int> lstPrdID = new List<int>();
        //            lstPrdID.Add(0);
        //            using (MySqlDataReader dr = dbHelp.GetDataReader(sqlCustFav, CommandType.Text, new MySqlParameter[]{
        //                DbUtility.GetParameter("CustID", srcText, MyDbType.Int)
        //            }))
        //            {
        //                while (dr.Read())
        //                {
        //                    lstPrdID.Add(BusinessUtility.GetInt(dr[0]));
        //                }
        //                dr.Close();
        //                dr.Dispose();
        //            }
        //            dtReslut.DefaultView.RowFilter = string.Format(" productID IN ({0})", StringUtil.GetJoinedString(",", lstPrdID.ToArray()));
        //        }
        //        foreach (DataRow item in dtReslut.Rows)
        //        {
        //            using (MySqlDataReader dr = dbHelp.GetDataReader(onHeadQty, CommandType.Text, new MySqlParameter[]{
        //                DbUtility.GetParameter("prdID", item["productID"], MyDbType.Int)
        //            }))
        //            {
        //                if (dr.Read())
        //                {
        //                    item["prdOhdQty"] = dr["prdOhdQty"];
        //                    item["prdWhsCode"] = dr["prdWhsCode"];
        //                    item["prdTaxCode"] = dr["prdTaxCode"];
        //                }
        //                dr.Close();
        //                dr.Dispose();
        //            }		           
        //        }
        //        return dtReslut;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //public string GetSqlSearchProductForQuoation(ParameterCollection pCol, string srcField, string srcText, int comID, StatusProductType prdType, string lang)
        //{
        //    pCol.Clear();
        //    //We should show available quantity instead on head
        //    string sql = "SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, sum(pq.prdOhdQty) as prdOhdQty, ";
        //    sql += "p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
        //    if (prdType == StatusProductType.ServiceProduct)
        //    {
        //        sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID WHERE ";
        //    }
        //    else
        //    {
        //        sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
        //    }
        //    pCol.Add("@descLang", lang);
        //    sql += " p.prdType=@prdType";
        //    pCol.Add("@prdType", DbType.Int32, ((int)prdType).ToString());
        //    if (comID > 0)
        //    {
        //        sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
        //        pCol.Add("@WarehouseCompanyID", comID.ToString());
        //    }

        //    if (!string.IsNullOrEmpty(srcText))
        //    {
        //        switch (srcField)
        //        {
        //            case ProductSearchFields.ProductID:
        //                sql += " AND p.productID = @productID";
        //                pCol.Add("@productID", srcText);
        //                break;
        //            case ProductSearchFields.ProductName:
        //                sql += " AND p.prdName LIKE CONCAT('%', @prdName, '%')";
        //                pCol.Add("@prdName", srcText);
        //                break;
        //            case ProductSearchFields.ProductInternalID:
        //                sql += " AND p.prdIntID = @prdIntID";
        //                pCol.Add("@prdIntID", srcText);
        //                break;
        //            case ProductSearchFields.ProductExternalID:
        //                sql += " AND p.prdExtID = @prdExtID";
        //                pCol.Add("@prdExtID", srcText);
        //                break;
        //            case ProductSearchFields.ProductBarCode:
        //                sql += " AND p.prdUPCCode = @prdUPCCode";
        //                pCol.Add("@prdUPCCode", srcText);
        //                break;
        //            case ProductSearchFields.ProductDescription:
        //                sql += " AND (prdSmallDesc LIKE CONCAT('%', @srcData, '%')  OR prdLargeDesc LIKE CONCAT('%', @srcData, '%')";
        //                pCol.Add("@srcData", srcText);
        //                break;
        //            case ProductSearchFields.ALPHA:
        //                if (srcText != "All")
        //                {
        //                    sql += "  And p.prdName LIKE CONCAT(@srcData, '%')";
        //                    pCol.Add("@srcData", srcText);
        //                }                        
        //                break;
        //            case ProductSearchFields.CUSTOMER_FAV:
        //                sql += "  And p.productID IN (SELECT oi.ordProductID FROM orderitems oi INNER JOIN orders o ON o.ordID = oi.ordID AND o.ordCustID = @ordCustID)";
        //                pCol.Add("@ordCustID", srcText);
        //                break;
        //            case ProductSearchFields.MY_FAV:
        //                sql += "   And p.prdIsSpecial = 1";
        //                break;
        //        }
        //    }

        //    sql += "  GROUP BY p.productID order by p.prdName ";
        //    return sql;
        //}

        //public DataRow GetProductDetailsForQuoation(int productID, string lang) {
        //    string sql = "SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, sum(pq.prdOhdQty) as prdOhdQty, p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang inner join prdquantity pq on pq.prdID=p.productID WHERE  p.productID=@productID   group by p.productID order by p.prdName";            
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("descLang", lang, MyDbType.String), DbUtility.GetParameter("productID", productID, MyDbType.Int) });
        //        if (dt != null && dt.Rows.Count > 0) {
        //            return dt.Rows[0];
        //        }
        //        return null;
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void UpdateQuotePrintCounter(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE orders SET QuotePrintCounter = IFNULL(QuotePrintCounter, 0) + 1 WHERE ordID=@OrderID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", ordID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSalesOrderPrintCounter(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE orders SET SalesOrderPrintCounter = IFNULL(SalesOrderPrintCounter, 0) + 1 WHERE ordID=@OrderID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", ordID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdatePackingListPrintCounter(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE orders SET PackingListPrintCounter = IFNULL(PackingListPrintCounter, 0) + 1 WHERE ordID=@OrderID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", ordID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateInventoryOnShipping(DbHelper dbHelp, int ordID, string shpWhs, string invMovSrc, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<OrderItems> lItems = new OrderItems().GetOrderItemList(dbHelp, ordID);
                ProductQuantity pq = new ProductQuantity();
                InventoryMovment objIM = new InventoryMovment();
                foreach (var item in lItems)
                {
                    pq.UpdateQuantityOnShipping(dbHelp, item.OrdProductID, item.OrdProductQty, shpWhs);
                    objIM.AddMovment(shpWhs, userID, item.OrdProductID, invMovSrc, BusinessUtility.GetInt(item.OrdProductQty), BusinessUtility.GetString(InvMovmentUpdateType.DIC));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion

        #region Sivananda Specific Functions

        public DataTable GetGuestOrders(int partnerID, string lang)
        {
            List<MySqlParameter> pCol = new List<MySqlParameter>();

            string sqlDummy = "SELECT 0 AS OrderID, NOW() AS OrderDate, 0 AS NoOfBeds, 0 AS NoOfNights, '' AS Building, '' AS Beds, NOW() AS CheckInDate, 0.00 AS OrderAmount,";
            sqlDummy += "0.00 AS ReceivedAmount, 0.00 AS Balance;";

            string strSQL = "";
            strSQL = "SELECT distinct o.ordID, ordCreatedOn as ordDate, ordStatus FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
            strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
            strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission  left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission Where 1=1 ";

            if (partnerID > 0)
            {
                strSQL += " And (PartnerID=@PartnerID OR i.ordGuestID=@PartnerID)";
                pCol.Add(DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int));
            }
            strSQL += " AND PartnerActive=1";
            strSQL += " group by o.ordID order by o.ordID desc ";

            DbHelper dbHelp = new DbHelper(true);
            DataTable dtFinal;
            Reservation rsv = new Reservation();
            try
            {
                dtFinal = dbHelp.GetDataTable(sqlDummy, CommandType.Text, null);
                dtFinal.Rows.RemoveAt(0); //Remove dummy row

                DataTable dtTempOrders = dbHelp.GetDataTable(strSQL, CommandType.Text, pCol.ToArray());
                foreach (DataRow item in dtTempOrders.Rows)
                {
                    DataRow newRow = dtFinal.NewRow();
                    newRow["OrderID"] = item["ordID"];
                    newRow["OrderDate"] = item["ordDate"];

                    int rsvID = rsv.GetReservationID(dbHelp, BusinessUtility.GetInt(item["ordID"]));
                    DataTable dtRev = this.GetReservationItems(dbHelp, rsvID, lang);

                    newRow["NoOfBeds"] = dtRev.Rows.Count;
                    newRow["NoOfNights"] = dtRev.Rows.Count > 0 ? dtRev.Rows[0]["TotalNights"] : DBNull.Value;
                    newRow["Building"] = dtRev.Rows.Count > 0 ? dtRev.Rows[0]["BuildingTitle"] : DBNull.Value;
                    newRow["Beds"] = dtRev.Rows.Count > 0 ? dtRev.Rows[0]["RoomTitle"] : DBNull.Value;
                    newRow["CheckInDate"] = dtRev.Rows.Count > 0 ? dtRev.Rows[0]["CheckInDate"] : DBNull.Value;

                    TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(item["ordID"]));

                    newRow["OrderAmount"] = ts.GrandTotal;
                    newRow["ReceivedAmount"] = ts.TotalAmountReceived;
                    newRow["Balance"] = ts.OutstandingAmount;

                    dtFinal.Rows.Add(newRow);
                }
                return dtFinal;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        private DataTable GetReservationItems(DbHelper dbHelp, int reservationID, string lang)
        {
            string SQL = "SELECT ri.ReservationItemID,ri.ReservationID,ri.BedID,ri.CheckInDate,ri.CheckOutDate,ri.PricePerDay,ri.TotalNights," + "\r\n" +
                "ri.IsCanceled,vw_beds.BedTitle,vw_beds.Building#LANG# AS BuildingTitle,vw_beds.Room#LANG# AS RoomTitle" + "\r\n" +
                "FROM    z_reservation_items ri INNER JOIN vw_beds vw_beds ON (ri.BedID = vw_beds.BedID)" + "\r\n" +
                "WHERE ri.IsCanceled = 0 AND ri.ReservationID=@ReservationID";
            SQL = SQL.Replace("#LANG#", !string.IsNullOrEmpty(lang) ? lang : AppLanguageCode.DEFAULT);
            try
            {
                return dbHelp.GetDataTable(SQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
        }

        public void CancelReservationOrder(int orderID, string reasontoReject)
        {
            DbTransactionHelper dbHelp = new DbTransactionHelper();
            string sqlOrd = "UPDATE orders SET ordStatus=@Status, orderRejectReason=@orderRejectReason WHERE ordID=@OrderID";
            string sqloItem = "UPDATE orderitems SET ordProductUnitPrice=0, ordIsItemCanceled=1 WHERE ordID=@OrderID AND ordReservationItemID > 0";
            string sqlReservationItem = "UPDATE z_reservation_items ri, orderitems oi  SET ri.PricePerDay=0, ri.IsCanceled=1 WHERE ri.ReservationItemID=oi.ordReservationItemID AND oi.ordID=@OrderID";
            try
            {
                dbHelp.BeginTransaction();
                dbHelp.ExecuteNonQuery(sqlOrd, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Status", SOStatus.RESERVATION_CANCELED, MyDbType.String),
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int),
                    DbUtility.GetParameter("orderRejectReason", reasontoReject, MyDbType.String)
                });
                dbHelp.ExecuteNonQuery(sqloItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                });
                dbHelp.ExecuteNonQuery(sqlReservationItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                });
                dbHelp.CommitTransaction();
            }
            catch
            {
                try
                {
                    dbHelp.RollBackTransaction();
                }
                finally
                {

                }
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Shipping Page Methods

        public int GetOrderIDToShip(DbHelper dbHelp, int oid)
        {
            bool mustClost = false;
            if (dbHelp == null)
            {
                mustClost = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT o.ordID FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID left join users u on u.userID=o.ordSalesRepID where (ordStatus='R' or ordStatus='Z') ";
                sql += " AND o.ordID=@ordID AND (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = @ordID) = 0";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", oid, MyDbType.Int)
                });

                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClost) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool CheckShippingForOrder(string searchField, string txtSearchData, string whsCode)
        {
            List<MySqlParameter> lstP = new List<MySqlParameter>();
            string strSQL = "";
            strSQL = "SELECT count(*) FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID left join users u on u.userID=o.ordSalesRepID where (ordStatus='R' or ordStatus='Z') and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";
            if (!string.IsNullOrEmpty(whsCode) && whsCode != "ALL")
            {
                strSQL += " AND o.ordShpWhsCode =@whsCode";
                lstP.Add(DbUtility.GetParameter("whsCode", whsCode, typeof(string)));
            }
            if (searchField == "ON" && !string.IsNullOrEmpty(txtSearchData))
            {
                strSQL += " AND o.ordID = @txtSearchData";
                lstP.Add(DbUtility.GetParameter("txtSearchData", txtSearchData, typeof(string)));
            }
            else if (searchField == "CN" && !string.IsNullOrEmpty(txtSearchData))
            {
                strSQL += " AND (PartnerLongName like concat('%',@txtSearchData,'%') ) ";
                lstP.Add(DbUtility.GetParameter("txtSearchData", txtSearchData, typeof(string)));
            }
            else if (searchField == "AN" && !string.IsNullOrEmpty(txtSearchData))
            {
                strSQL += " AND(concat(userFirstName,' ',userLastName) like concat('%',@txtSearchData,'%') ) ";
                lstP.Add(DbUtility.GetParameter("txtSearchData", txtSearchData, typeof(string)));
            }
            else if (searchField == "TN" && !string.IsNullOrEmpty(txtSearchData))
            {
                strSQL += " AND ordShpTrackNo = @txtSearchData";
                lstP.Add(DbUtility.GetParameter("txtSearchData", txtSearchData, typeof(string)));
            }

            int nDOW = (int)DateTime.Now.DayOfWeek;
            int nFirst = 0;
            int nLast = 0;

            nFirst = -nDOW;
            nLast = nFirst + 6;
            if (searchField == "T") //Today
            {
                strSQL += " AND ordShpDate LIKE concat('',@Today,'%') ";
                lstP.Add(DbUtility.GetParameter("Today", DateTime.Now.ToString("yyyy-MM-dd"), typeof(string)));
            }
            else if (searchField == "TW") //This Week
            {
                strSQL += " AND (ordShpDate BETWEEN @FirstWeek AND @LastWeek) ";
                lstP.Add(DbUtility.GetParameter("FirstWeek", DateTime.Today.AddDays(nFirst), typeof(DateTime)));
                lstP.Add(DbUtility.GetParameter("LastWeek", DateTime.Today.AddDays(nLast), typeof(DateTime)));
            }
            else if (searchField == "NW") //Next Week
            {
                if (nDOW == 0)
                    nFirst = nFirst + 7;
                else
                    nFirst = nFirst + 7;
                nLast = nFirst + 6;
                strSQL += " AND ( ordShpDate BETWEEN @FirstWeek AND @LastWeek )";
                lstP.Add(DbUtility.GetParameter("FirstWeek", DateTime.Today.AddDays(nFirst), typeof(DateTime)));
                lstP.Add(DbUtility.GetParameter("LastWeek", DateTime.Today.AddDays(nLast), typeof(DateTime)));
            }
            strSQL += " GROUP BY o.ordID ORDER BY o.ordID ";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(strSQL, CommandType.Text, lstP.ToArray());
                return BusinessUtility.GetInt(o) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetShippingSQL(ParameterCollection pCol, string statusData, string searchText, string whsCode)
        {
            pCol.Clear();
            string strSQL = "";
            strSQL = "SELECT DISTINCT o.ordID,PartnerAcronyme,  PartnerLongName AS CustomerName, concat(userFirstName,' ',userLastName) AS AgentName, ";
            strSQL += " ordShpTrackNo, CASE ordVerified WHEN 1 THEN 'green.gif' ELSE 'red.gif' END AS ordVerified, ordStatus, DATE_FORMAT(ordShpDate,'%m-%d-%Y') AS ordShpDate, ";
            strSQL += " IFNULL(o.QuotePrintCounter, 0) AS QuotePrintCounter,IFNULL(o.SalesOrderPrintCounter, 0) AS SalesOrderPrintCounter,";
            strSQL += " IFNULL(o.PackingListPrintCounter, 0) AS PackingListPrintCounter";
            strSQL += "  FROM orders o INNER JOIN orderitems i ON i.ordID=o.ordID LEFT JOIN partners  ON partners.PartnerID =o.ordCustID LEFT JOIN ";
            strSQL += " users u ON u.userID=o.ordSalesRepID WHERE (ordStatus='R' or ordStatus='Z') and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";
            if (!string.IsNullOrEmpty(whsCode) && whsCode != "ALL")
            {
                strSQL += " AND o.ordShpWhsCode =@whsCode";
                pCol.Add("@whsCode", whsCode);
            }
            if (statusData == "ON" && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND o.ordID = @searchText";
                pCol.Add("@searchText", searchText);
            }
            else if (statusData == "CN" && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND (PartnerLongName LIKE concat('%', @searchText ,'%') OR PartnerAcronyme LIKE concat('%', @searchText ,'%') )";
                pCol.Add("@searchText", searchText);
            }
            else if (statusData == "AN" && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND concat(userFirstName,' ',userLastName) LIKE concat('%', @searchText ,'%') ";
                pCol.Add("@searchText", searchText);
            }
            else if (statusData == "TN" && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " AND  ordShpTrackNo = @searchText";
                pCol.Add("@searchText", searchText);
            }

            Int16 nDOW = default(Int16);
            nDOW = Convert.ToInt16(DateTime.Now.DayOfWeek);
            Int16 nFirst = default(Int16);
            Int16 nLast = default(Int16);

            nFirst = Convert.ToInt16(-nDOW);
            nLast = Convert.ToInt16(nFirst + 6);

            if (statusData == "T")
            {
                strSQL += " AND ordShpDate LIKE concat('',@Today,'%') ";
                pCol.Add("@Today", DbType.String, DateTime.Now.ToString("yyyy-MM-dd"));
            }
            else if (statusData == "TM")
            {
                strSQL += " AND ordShpDate LIKE concat('',@TOMORROW,'%') ";
                pCol.Add("@TOMORROW", DbType.String, DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));
            }
            else if (statusData == "TW")
            {
                strSQL += " AND (ordShpDate BETWEEN @FirstWeek AND @LastWeek) ";
                pCol.Add("@FirstWeek", DbType.DateTime, DateTime.Now.AddDays(nFirst).ToString());
                pCol.Add("@LastWeek", DbType.DateTime, DateTime.Now.AddDays(nLast).ToString());
            }
            else if (statusData == "NW")
            {
                if (nDOW == 0)
                    nFirst = Convert.ToInt16(nFirst + 7);
                else
                    nFirst = Convert.ToInt16(nFirst + 7);
                nLast = Convert.ToInt16(nFirst + 6);
                strSQL += " AND ( ordShpDate BETWEEN @FirstWeek AND @LastWeek )";
                pCol.Add("@FirstWeek", DbType.DateTime, DateTime.Now.AddDays(nFirst).ToString());
                pCol.Add("@LastWeek", DbType.DateTime, DateTime.Now.AddDays(nLast).ToString());
            }

            strSQL += " group by o.ordID order by o.ordID, CustomerName ";
            return strSQL;
        }
        #endregion

        #region Refund Page Specific
        public DataTable GetOrderGuests(DbHelper dbHelp, int orderID)
        {
            string sql = "SELECT DISTINCT oi.ordGuestID AS GuestID,pe.FirstName,pe.LastName, p.PartnerLongName FROM (z_customer_extended pe";
            sql += " INNER JOIN partners p ON (pe.PartnerID = p.PartnerID))";
            sql += " INNER JOIN orderitems oi ON (oi.ordGuestID = pe.PartnerID) WHERE oi.ordID=@ordID AND IFNULL(oi.ordGuestID, 0) > 0";

            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", orderID, MyDbType.Int) 
                });
            }
            catch
            {

                throw;
            }
        }

        public DataTable GetOrderGuests(int orderID)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                return this.GetOrderGuests(dbHelp, orderID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        public void UpdateOrderStatus(DbHelper dbHelp, int orderID, string status)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                dbHelp = new DbHelper(true);
                mustClose = true;
            }
            string sql = "UPDATE orders SET ordStatus=@ordStatus,ordLastUpdatedOn=@ordLastUpdatedOn WHERE ordID=@ordID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordStatus", status, MyDbType.String),
                    DbUtility.GetParameter("ordLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ordID", orderID, MyDbType.Int),
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<int> GetOrderSalesRepUsers(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (mustClose)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<int> lResult = new List<int>();
            try
            {
                string sql = "SELECT DISTINCT SalesRepID FROM z_order_sales_rep WHERE OrderID=@OrderID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(BusinessUtility.GetInt(dr[0]));
                    }
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void SetOrderSalesRepUsers(DbHelper dbHelp, int orderID, int[] srUsers)
        {
            //bool mustClose = false;
            //if (mustClose)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}
            bool mustClose = false;
            if (dbHelp == null)
            {
                dbHelp = new DbHelper(true);
                mustClose = true;
            }

            try
            {
                string sqlDelete = "DELETE FROM z_order_sales_rep WHERE OrderID=@OrderID";
                string sql = "INSERT INTO z_order_sales_rep (OrderID,SalesRepID) VALUES (@OrderID,@SalesRepID)";
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                foreach (var item in srUsers)
                {
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int),
                        DbUtility.GetParameter("SalesRepID", item, MyDbType.Int)
                    });
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void FillSalesRepresentativesCommission(DbHelper dbHelp, ListControl lCtrl, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlGetCommition = "SELECT getUserCommission(@OrderID, @UserID);";
                int userID = 0;
                double commission = 0.0D;
                double commissionAmt = 0.00D;
                foreach (ListItem item in lCtrl.Items)
                {
                    commission = 0.00D;
                    commissionAmt = 0.00D;
                    if (int.TryParse(item.Value, out userID) && userID > 0)
                    {
                        object val = dbHelp.GetValue(sqlGetCommition, CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("OrderID", orderID, MyDbType.Int),
                            DbUtility.GetParameter("UserID", userID, MyDbType.Int)
                        });
                        commission = BusinessUtility.GetDouble(val);
                        //double calAmt = CalculationHelper.GetAmount(ts.ItemSubTotal + ts.ProcessSubTotal - ts.AdditionalDiscount);
                        //if (commission > 0)
                        //{
                        //    commissionAmt = CalculationHelper.GetAmount(calAmt * commission / 100.00D);
                        //}
                    }
                    item.Text = string.Format("{0} ({1} {2:F})", item.Text, OrdCurrencyCode, commission); //Added by mukesh 20130607
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void SetReasontoPutOnHold(DbHelper dbHelp, int orderid, OrderHeldReasonCode reasonCode, string reason)
        {
            bool mustClose = false;
            if (mustClose)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlDelete = "DELETE FROM z_order_held_reason WHERE OrderID=@OrderID";
                string sql = "INSERT INTO z_order_held_reason (OrderID,OrdHeldReasonCode,OtherReason) VALUES (@OrderID,@OrdHeldReasonCode,@OtherReason)";
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderid, MyDbType.Int) });
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("OrderID", orderid, MyDbType.Int),
                        DbUtility.GetParameter("OrdHeldReasonCode", (int)reasonCode, MyDbType.Int),
                        DbUtility.GetParameter("OtherReason", reason, MyDbType.String)
                    });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public OrderHoldReason GetHoldReason(DbHelper dbHelp, int orderID)
        {
            try
            {
                OrderHoldReason reson = new OrderHoldReason();
                reson.PopulateObject(dbHelp, orderID);
                return reson;
            }
            catch
            {
                throw;
            }
        }

        #region Batch Print Feature
        //Get List of orders which 
        public DataTable GetSoForBatchPrint(DbHelper dbHelp, DateTime fDate, DateTime tDate, int comID, string status, int userID, bool isSalesRistricted, bool isReprint, int[] excludeOrderType)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string strSQL = null;
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            if (!isSalesRistricted)
            {
                strSQL = "SELECT DISTINCT o.ordID, i.invID, i.InvRefNo FROM orders o LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'  WHERE 1=1";
                //strSQL += " WHERE ordStatus IN(SELECT OrderStatus FROM z_company_ord_status_to_invoiced WHERE AllowInvoiceGeneration=1) ";
            }
            else
            {
                strSQL = "SELECT DISTINCT o.ordID, i.invID, i.InvRefNo FROM orders o INNER JOIN salesrepcustomer s ON s.CustomerID=o.ordCustID";
                strSQL += " AND s.CustomerType=o.ordCustType AND s.UserID=@UserID ";
                strSQL += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'";
                strSQL += " WHERE 1=1";
                //strSQL += " WHERE ordStatus IN(SELECT OrderStatus FROM z_company_ord_status_to_invoiced WHERE AllowInvoiceGeneration=1) ";
                lParams.Add(DbUtility.GetParameter("UserID", userID, MyDbType.Int));
            }
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " AND (o.ordCreatedOn BETWEEN @FDate AND @TDate)";
                lParams.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                lParams.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
            }
            if (comID > 0)
            {
                strSQL += " AND o.ordCompanyID =@ComID";
                lParams.Add(DbUtility.GetParameter("ComID", comID, MyDbType.Int));
            }
            if (!string.IsNullOrEmpty(status))
            {
                strSQL += " AND o.ordStatus=@Status";
                lParams.Add(DbUtility.GetParameter("Status", status, MyDbType.String));
            }
            //else
            //{
            //    strSQL += " and (ordStatus='A' OR ordStatus='P' OR ordStatus='D' OR ordStatus='R')";
            //}
            if (excludeOrderType != null)
            {
                string[] oType = Array.ConvertAll<int, string>(excludeOrderType, o => o.ToString());
                strSQL += string.Format(" AND o.orderTypeCommission NOT IN({0})", string.Join(",", oType));
            }
            if (isReprint)
            {
                strSQL += " AND IFNULL(i.InvoicePrintCounter, 0) > 0";
            }
            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, lParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<int> GetSoForBatchRePrint(DateTime fDate, DateTime tDate, int comID, string status, int userID, bool isSalesRistricted)
        {
            string strSQL = null;
            List<int> lResult = new List<int>();
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            if (!isSalesRistricted)
            {
                strSQL = "SELECT distinct ordID FROM orders WHERE 1=1";
            }
            else
            {
                strSQL = "SELECT distinct ordID FROM orders INNER JOIN salesrepcustomer s ON s.CustomerID=orders.ordCustID AND s.CustomerType=orders.ordCustType AND s.UserID=@UserID WHERE 1=1";
                lParams.Add(DbUtility.GetParameter("UserID", userID, MyDbType.Int));
            }
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " AND (ordCreatedOn >=@FDate AND ordCreatedOn <=@TDate)";
                lParams.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                lParams.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
            }
            if (comID > 0)
            {
                strSQL += " AND ordCompanyID =@ComID";
                lParams.Add(DbUtility.GetParameter("ComID", comID, MyDbType.Int));
            }
            if (!string.IsNullOrEmpty(status))
            {
                strSQL += " AND ordStatus=@Status";
                lParams.Add(DbUtility.GetParameter("Status", status, MyDbType.String));
            }
            else
            {
                strSQL += " AND (ordStatus='A' OR ordStatus='P' OR ordStatus='D' OR ordStatus='R')";
            }

            string strSQLMain = "SELECT InvRefNo FROM invoices WHERE invForOrderNo IN (" + strSQL + ")";

            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQLMain, CommandType.Text, lParams.ToArray());
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr["InvRefNo"]));
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
        #endregion

        #region Duplicate Order

        /// <summary>
        /// Create duplicate order & return new orderid
        /// </summary>
        /// <param name="targetOrderID"></param>
        /// <returns></returns>
        public int CreateDuplicateOrder(int targetOrderID)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                string sqlInsert = "INSERT INTO orders(ordType, ordCustType, ordCustID, ordCreatedOn,ordCreatedFromIP,ordStatus,ordVerified,";
                sqlInsert += "ordVerifiedBy,ordSalesRepID,ordSaleWeb,ordComment,ordShpDate,ordShpBlankPref,ordShpWhsCode,ordLastUpdatedOn,";
                sqlInsert += "ordLastUpdateBy,ordShpTrackNo,ordShpCost,ordShpCode,ordCurrencyCode,ordCurrencyExRate,ordCustPO,qutExpDate,";
                sqlInsert += "ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy,orderTypeCommission,orderRejectReason,InvRefNo,invDate,ordDiscount,ordDiscountType)";
                sqlInsert += " SELECT o.ordType, o.ordCustType, o.ordCustID, o.ordCreatedOn,o.ordCreatedFromIP,'N' AS ordStatus,o.ordVerified,";
                sqlInsert += "o.ordVerifiedBy,o.ordSalesRepID,o.ordSaleWeb,o.ordComment,o.ordShpDate,o.ordShpBlankPref,o.ordShpWhsCode,o.ordLastUpdatedOn,";
                sqlInsert += "o.ordLastUpdateBy,o.ordShpTrackNo,o.ordShpCost, NULL AS ordShpCode,o.ordCurrencyCode,o.ordCurrencyExRate,o.ordCustPO,o.qutExpDate,";
                sqlInsert += "o.ordCompanyID,o.ordShippingTerms,o.ordNetTerms,o.ordCreatedBy,o.orderTypeCommission,o.orderRejectReason,o.InvRefNo,o.invDate,o.ordDiscount,o.ordDiscountType";
                sqlInsert += " FROM orders o WHERE o.ordID=@ordID";
                string sqlWhs = "SELECT ordShpWhsCode FROM orders WHERE ordID=@ordID";

                string sqlInsertProcess = "INSERT INTO orderitemprocess(ordID, ordItemProcCode, ordItemProcFixedPrice, ordItemProcPricePerHour, ordItemProcHours, ";
                sqlInsertProcess += "ordItemProcPricePerUnit, ordItemProcUnits,sysTaxCodeDescID,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,";
                sqlInsertProcess += " TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5)";
                sqlInsertProcess += " SELECT @NewOrderID AS ordID, p.ordItemProcCode, p.ordItemProcFixedPrice, p.ordItemProcPricePerHour, p.ordItemProcHours,";
                sqlInsertProcess += " p.ordItemProcPricePerUnit, p.ordItemProcUnits,p.sysTaxCodeDescID,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,";
                sqlInsertProcess += " TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5 FROM orderitemprocess p  WHERE p.ordID=@ordID";

                string sqlCopySalesRep = "INSERT INTO z_order_sales_rep(OrderID, SalesRepID) ";
                sqlCopySalesRep += "SELECT @NewOrderID AS OrderID, SalesRepID FROM z_order_sales_rep WHERE OrderID=@TargetOrderID";

                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", targetOrderID, MyDbType.Int) });
                int id = dbHelp.GetLastInsertID();
                object whs = dbHelp.GetValue(sqlWhs, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", targetOrderID, MyDbType.Int) });
                if (id > 0)
                {
                    List<OrderItems> lItems = new OrderItems().GetOrderItemList(dbHelp, targetOrderID);
                    foreach (var item in lItems)
                    {
                        item.OrdID = id;
                        item.Insert(dbHelp);
                    }
                    dbHelp.ExecuteNonQuery(sqlInsertProcess, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ordID", targetOrderID, MyDbType.Int),
                        DbUtility.GetParameter("NewOrderID", id, MyDbType.Int)
                    });

                    dbHelp.ExecuteNonQuery(sqlCopySalesRep, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("TargetOrderID", targetOrderID, MyDbType.Int),
                        DbUtility.GetParameter("NewOrderID", id, MyDbType.Int)
                    });
                }
                return id;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region Dropdown Help Functions
        public void FillOrderType(DbHelper dbHelp, ListControl lCtrl, string lang, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT ordertypeID,orderTypeDesc from ordertype  where orderTypeLang=@orderTypeLang and ordertypeID !=8 ";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeLang", lang)
                                                                      
                                 };
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(strSQL, System.Data.CommandType.Text, p);
                lCtrl.DataTextField = "orderTypeDesc";
                lCtrl.DataValueField = "ordertypeID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void FillOrderALLType(DbHelper dbHelp, ListControl lCtrl, string lang, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT ordertypeID,orderTypeDesc from ordertype  where orderTypeLang=@orderTypeLang";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeLang", lang)
                                                                      
                                 };
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(strSQL, System.Data.CommandType.Text, p);
                lCtrl.DataTextField = "orderTypeDesc";
                lCtrl.DataValueField = "ordertypeID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Returns Module Functions
        public string GetSalesToReturnSql(ParameterCollection pCol, string searchField, string searchText, int userid, bool isSalesRestricted)
        {
            pCol.Clear();
            string strSQL = "";
            if (isSalesRestricted)
            {
                strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID=@UserID  inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
                pCol.Add("@UserID", userid.ToString());
            }
            else
            {
                strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
            }

            strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission  left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission ";
            strSQL += " LEFT OUTER JOIN invoices inv ON inv.invForOrderNo=o.ordID WHERE 1=1";

            //if (!string.IsNullOrEmpty(ordStatus))
            //{
            //    strSQL += " AND o.ordStatus=@pOStatus ";
            //    pCol.Add("@pOStatus", ordStatus);
            //}
            int partID = 0;
            int invNo = 0;
            if (searchField == "ON" && !string.IsNullOrEmpty(searchText)) //Check for order no
            {
                strSQL += " and o.ordID = @searchKeyWord ";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == "CI" && !string.IsNullOrEmpty(searchText) && int.TryParse(searchText, out partID) && partID > 0)
            {
                strSQL += " And (PartnerID=@PartnerID OR i.ordGuestID=@PartnerID)";
                pCol.Add("@PartnerID", partID.ToString());
            }
            else if (searchField == "CN" && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " and (PartnerLongName like  concat( '%' , @searchKeyWord ,'%')  OR PartnerAcronyme like  concat( '%' , @searchKeyWord ,'%'))";
                pCol.Add("@searchKeyWord", searchText);
            }
            else if (searchField == "IN" && !string.IsNullOrEmpty(searchText) && int.TryParse(searchText, out invNo) && invNo > 0)
            {
                strSQL += " AND inv.InvID = @InvID";
                pCol.Add("@InvID", invNo.ToString());
            }
            else
            {
                strSQL += " AND 1=0";
            }

            //if (ordType > 0)
            //{
            //    strSQL += " AND orderTypeCommission=@orderTypeCommission";
            //    pCol.Add("@orderTypeCommission", ordType.ToString());
            //}
            //strSQL += " AND PartnerActive=1 AND o.ordStatus='S'"; //Only Show shipped status
            strSQL += " AND PartnerActive=1 AND o.ordStatus IN('S','T')"; //Only Show shipped status
            strSQL += " group by o.ordID order by o.ordID desc, CustomerName ";
            return strSQL;
        }
        public int GetReturnsApplicableRecordCount(DbHelper dbHelp, string searchField, string searchText, int userid, bool isSalesRestricted)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<MySqlParameter> pCol = new List<MySqlParameter>();
                string strSQL = "";
                if (isSalesRestricted)
                {
                    strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID=@UserID  inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                    strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
                    pCol.Add(DbUtility.GetParameter("UserID", userid, MyDbType.Int));
                }
                else
                {
                    strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID ";
                    strSQL += " inner join products as pro on i.ordProductID = pro.productID ";
                }

                strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission  left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission ";
                strSQL += " LEFT OUTER JOIN invoices inv ON inv.invForOrderNo=o.ordID WHERE 1=1";

                //if (!string.IsNullOrEmpty(ordStatus))
                //{
                //    strSQL += " AND o.ordStatus=@pOStatus ";
                //    pCol.Add("@pOStatus", ordStatus);
                //}
                int partID = 0;
                int invNo = 0;
                if (searchField == "ON" && !string.IsNullOrEmpty(searchText)) //Check for order no
                {
                    strSQL += " and o.ordID = @searchKeyWord ";
                    pCol.Add(DbUtility.GetParameter("searchKeyWord", searchText, MyDbType.String));
                }
                else if (searchField == "CI" && !string.IsNullOrEmpty(searchText) && int.TryParse(searchText, out partID) && partID > 0)
                {
                    strSQL += " And (PartnerID=@PartnerID OR i.ordGuestID=@PartnerID)";
                    pCol.Add(DbUtility.GetParameter("PartnerID", partID, MyDbType.Int));
                }
                else if (searchField == "CN" && !string.IsNullOrEmpty(searchText))
                {
                    strSQL += " and (PartnerLongName like  concat( '%' , @searchKeyWord ,'%')  OR PartnerAcronyme like  concat( '%' , @searchKeyWord ,'%'))";
                    pCol.Add(DbUtility.GetParameter("searchKeyWord", searchText, MyDbType.String));
                }
                else if (searchField == "IN" && !string.IsNullOrEmpty(searchText) && int.TryParse(searchText, out invNo) && invNo > 0)
                {
                    strSQL += " AND inv.InvID = @InvID";
                    pCol.Add(DbUtility.GetParameter("InvID", invNo, MyDbType.Int));
                }
                else
                {
                    return 0;
                }

                //if (ordType > 0)
                //{
                //    strSQL += " AND orderTypeCommission=@orderTypeCommission";
                //    pCol.Add("@orderTypeCommission", ordType.ToString());
                //}

                //strSQL += " AND PartnerActive=1 AND o.ordStatus='S'"; //Only Show shipped status
                strSQL += " AND PartnerActive=1 AND o.ordStatus IN('S','T')"; //Only Show shipped status
                strSQL += " group by o.ordID order by o.ordID desc, CustomerName ";

                object val = dbHelp.GetValue(strSQL, CommandType.Text, pCol.ToArray());
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Posted PO Xml Related Methods
        public void SetPostedXmlID(DbHelper dbHelp, int orderID, int postedXmlID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE orders SET PostedPOXmlID=@PostedPOXmlID WHERE ordID=@OrderID";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PostedPOXmlID", postedXmlID, MyDbType.Int),
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        //Added by mukesh 20130603 start
        public string GetSoBatchDetails(ParameterCollection lParams, DateTime fDate, DateTime tDate, int comID, string status, int userID, bool isSalesRistricted, bool isReprint, int[] excludeOrderType, bool srchByOrder)
        {
            lParams.Clear();

            string strSQL = null;

            strSQL = "Select q1.ordID,q1.CustomerName,q1.ordDate,DATE_FORMAT(ifnull(q1.ordShpDate,''),'%m-%d-%Y') AS ordShpDate,(ifnull(q1.itemamt,0) + ifnull(p.processamt,0)) as amtBeforeTax,";
            strSQL += "(ifnull(q1.itemamt,0) + ifnull(p.processamt,0) + ifnull(q1.itemTax,0) + ifnull(p.processTax,0)) as TotalAmt,q1.invID,q1.invRefNo,DATE_FORMAT(ifnull(q1.invDate, ''),'%m-%d-%Y') as invDate,ifnull(q1.printcount,0) AS printcount ";
            strSQL += " from ( ";
            if (!isSalesRistricted)
            {
                strSQL += "SELECT distinct i.invID as invID, i.InvRefNo as invRefNo,partners.PartnerLongName as CustomerName,o.ordShpDate as ordShpDate,";
                strSQL += "SUM((oit.ordProductQty * oit.ordProductUnitPrice) - (oit.ordProductQty * oit.ordProductUnitPrice)*oit.ordProductDiscount/100) as itemamt,";
                strSQL += "Sum(oit.TaxCalculated1 + oit.TaxCalculated2 + oit.TaxCalculated3 + oit.TaxCalculated4 + oit.TaxCalculated5) as itemTax,";
                strSQL += "o.ordID as ordID,i.invCreatedOn as invDate,i.InvoicePrintCounter as printcount,DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate ";
                strSQL += " FROM orders o inner join orderitems oit on oit.ordID=o.ordID LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'";
                strSQL += "left join partners  on partners.PartnerID =o.ordCustID WHERE 1=1 and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";

            }
            else
            {
                strSQL += "SELECT distinct i.invID as invID, i.InvRefNo as invRefNo,partners.PartnerLongName as CustomerName,o.ordShpDate as ordShpDate,";
                strSQL += "SUM((oit.ordProductQty * oit.ordProductUnitPrice) - (oit.ordProductQty * oit.ordProductUnitPrice)*oit.ordProductDiscount/100) as itemamt,";
                strSQL += "Sum(oit.TaxCalculated1 + oit.TaxCalculated2 + oit.TaxCalculated3 + oit.TaxCalculated4 + oit.TaxCalculated5) as itemTax,";
                strSQL += "o.ordID as ordID,i.invCreatedOn as invDate,i.InvoicePrintCounter as printcount,DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate ";
                strSQL += " FROM orders o inner join orderitems oit on oit.ordID=o.ordID INNER JOIN salesrepcustomer s ON s.CustomerID=o.ordCustID";
                strSQL += " AND s.CustomerType=o.ordCustType AND s.UserID=@UserID ";
                strSQL += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'";
                strSQL += "left join partners  on partners.PartnerID =o.ordCustID WHERE 1=1 and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";

                lParams.Add("@UserID", DbType.Int16, userID.ToString());
            }
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " AND (o.ordCreatedOn BETWEEN @FDate AND @TDate)";
                lParams.Add("@FDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                lParams.Add("@TDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            if (comID > 0)
            {
                strSQL += " AND o.ordCompanyID =@ComID";
                lParams.Add("@ComID", DbType.Int16, comID.ToString());
            }
            if (!string.IsNullOrEmpty(status))
            {
                strSQL += " AND o.ordStatus=@Status";
                lParams.Add("@Status", status);
            }

            if (excludeOrderType != null)
            {
                string[] oType = Array.ConvertAll<int, string>(excludeOrderType, o => o.ToString());
                strSQL += string.Format(" AND o.orderTypeCommission NOT IN({0})", string.Join(",", oType));
            }
            if (isReprint)
            {
                strSQL += " AND IFNULL(i.InvoicePrintCounter, 0) > 0 ";
            }
            else
            {
                strSQL += " AND IFNULL(i.InvoicePrintCounter, 0) = 0 ";
            }
            strSQL += " group by oit.ordID)as q1 left join (SELECT sum(oip.ordItemProcFixedPrice) as processamt,o.ordID as ordID,";
            strSQL += "Sum(oip.TaxCalculated1 + oip.TaxCalculated2 + oip.TaxCalculated3 + oip.TaxCalculated4 + oip.TaxCalculated5) as processTax ";
            strSQL += " FROM orders o INNER join orderitemprocess oip on oip.ordID = o.ordID ";
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " WHERE (o.ordCreatedOn BETWEEN @FDate AND @TDate)";
            }
            strSQL += "group by oip.ordID ) as p on p.ordID = q1.ordID ";
            if (srchByOrder)
            {
                strSQL += " order by ordShpDate ";
            }
            return strSQL;
        }

        public DataTable GetSoBatchDetailsDT(DbHelper dbHelp, DateTime fDate, DateTime tDate, int comID, string status, int userID, bool isSalesRistricted, bool isReprint, int[] excludeOrderType, bool srchByOrder)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string strSQL = null;
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            strSQL = "Select q1.ordID,q1.CustomerName,q1.ordDate,DATE_FORMAT(ifnull(q1.ordShpDate,''),'%m-%d-%Y') AS ordShpDate,(ifnull(q1.itemamt,0) + ifnull(p.processamt,0)) as amtBeforeTax,";
            strSQL += "(ifnull(q1.itemamt,0) + ifnull(p.processamt,0) + ifnull(q1.itemTax,0) + ifnull(p.processTax,0)) as TotalAmt,q1.invID,q1.invRefNo,DATE_FORMAT(ifnull(q1.invDate, ''),'%m-%d-%Y') as invDate,ifnull(q1.printcount,0) AS printcount ";
            strSQL += " from ( ";
            if (!isSalesRistricted)
            {
                strSQL += "SELECT distinct i.invID as invID, i.InvRefNo as invRefNo,partners.PartnerLongName as CustomerName,o.ordShpDate as ordShpDate,";
                strSQL += "SUM((oit.ordProductQty * oit.ordProductUnitPrice) - (oit.ordProductQty * oit.ordProductUnitPrice)*oit.ordProductDiscount/100) as itemamt,";
                strSQL += "Sum(oit.TaxCalculated1 + oit.TaxCalculated2 + oit.TaxCalculated3 + oit.TaxCalculated4 + oit.TaxCalculated5) as itemTax,";
                strSQL += "o.ordID as ordID,i.invCreatedOn as invDate,i.InvoicePrintCounter as printcount,DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate ";
                strSQL += " FROM orders o inner join orderitems oit on oit.ordID=o.ordID LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'";
                strSQL += "left join partners  on partners.PartnerID =o.ordCustID WHERE 1=1 and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";

            }
            else
            {
                strSQL += "SELECT distinct i.invID as invID, i.InvRefNo as invRefNo,partners.PartnerLongName as CustomerName,o.ordShpDate as ordShpDate,";
                strSQL += "SUM((oit.ordProductQty * oit.ordProductUnitPrice) - (oit.ordProductQty * oit.ordProductUnitPrice)*oit.ordProductDiscount/100) as itemamt,";
                strSQL += "Sum(oit.TaxCalculated1 + oit.TaxCalculated2 + oit.TaxCalculated3 + oit.TaxCalculated4 + oit.TaxCalculated5) as itemTax,";
                strSQL += "o.ordID as ordID,i.invCreatedOn as invDate,i.InvoicePrintCounter as printcount,DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate ";
                strSQL += " FROM orders o inner join orderitems oit on oit.ordID=o.ordID INNER JOIN salesrepcustomer s ON s.CustomerID=o.ordCustID";
                strSQL += " AND s.CustomerType=o.ordCustType AND s.UserID=@UserID ";
                strSQL += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID AND i.invRefType='IV'";
                strSQL += "left join partners  on partners.PartnerID =o.ordCustID WHERE 1=1 and (Select case COUNT( ParentOrderID) when 0 then 0 else count(parentOrderID) end from orders ord where ord.ParentOrderID = o.ordID) = 0 ";

                lParams.Add(DbUtility.GetParameter("UserID", userID, MyDbType.Int));
            }
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " AND (o.ordCreatedOn BETWEEN @FDate AND @TDate)";

                lParams.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                lParams.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
            }
            if (comID > 0)
            {
                strSQL += " AND o.ordCompanyID =@ComID";
                lParams.Add(DbUtility.GetParameter("ComID", comID, MyDbType.Int));
            }
            if (!string.IsNullOrEmpty(status))
            {
                strSQL += " AND o.ordStatus=@Status";
                lParams.Add(DbUtility.GetParameter("Status", status, MyDbType.String));
            }

            if (excludeOrderType != null)
            {
                string[] oType = Array.ConvertAll<int, string>(excludeOrderType, o => o.ToString());
                strSQL += string.Format(" AND o.orderTypeCommission NOT IN({0})", string.Join(",", oType));
            }
            if (isReprint)
            {
                strSQL += " AND IFNULL(i.InvoicePrintCounter, 0) > 0 ";
            }
            else
            {
                strSQL += " AND IFNULL(i.InvoicePrintCounter, 0) = 0 ";
            }
            strSQL += " group by oit.ordID)as q1 left join (SELECT sum(oip.ordItemProcFixedPrice) as processamt,o.ordID as ordID,";
            strSQL += "Sum(oip.TaxCalculated1 + oip.TaxCalculated2 + oip.TaxCalculated3 + oip.TaxCalculated4 + oip.TaxCalculated5) as processTax ";
            strSQL += " FROM orders o INNER join orderitemprocess oip on oip.ordID = o.ordID ";
            if (fDate != DateTime.MinValue && tDate != DateTime.MinValue)
            {
                strSQL += " WHERE (o.ordCreatedOn BETWEEN @FDate AND @TDate)";
            }
            strSQL += "group by oip.ordID ) as p on p.ordID = q1.ordID ";
            if (srchByOrder)
            {
                strSQL += " order by ordShpDate ";
            }
            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, lParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetShipmentHistoryDT(DbHelper dbHelp, int parentOrdID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string strSQL = null;
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            strSQL += "select ordID as OrderID, u.userFirstName as CreatedBy , o.ordCreatedOn as CreatedOn, s.sysAppDesc as OrderStatus,i.invID as InvoiceNo from orders o ";
            strSQL += " join users u on u.userID = o.ordCreatedBy join sysstatus s on s.sysAppLogicCode = o.ordStatus left outer join invoices i on i.invForOrderNo = o.ordID ";
            strSQL += "where ParentOrderID = @parentOrdID and sysAppCodeLang =@lang and sysAppCodeActive=1 and sysAppPfx='SO' and sysAppCode='SOSts' ";

            lParams.Add(DbUtility.GetParameter("parentOrdID", parentOrdID, MyDbType.Int));
            lParams.Add(DbUtility.GetParameter("lang", lang, MyDbType.String));

            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, lParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //Added end

        public void GetOrderCustSaleRepName(int orderID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "";
            sql += " SELECT ptr.PartnerLongName AS CustName, usr.UserFirstName AS SaleRep FROM  ";
            sql += " orders AS o  ";
            sql += " Inner JOIN Partners AS ptr ON ptr.PartnerID = o.ordCustID ";
            sql += " INNER JOIN users AS usr ON usr.userID = o.ordSalesRepID ";
            sql += " WHERE o.ordID = @ordID ";
            //ON o.ordID = @ordID
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("ordID", orderID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.OrdCustName = BusinessUtility.GetString(dr["CustName"]);
                    this.OrdCustSaleName = BusinessUtility.GetString(dr["SaleRep"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ChkISParentID(int orderID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "";
            List<MySqlParameter> lstP = new List<MySqlParameter>();
            sql += " select COUNT(OrdID) from orders where parentOrderID = @parentOrderID";
            lstP.Add(DbUtility.GetParameter("parentOrderID", orderID, typeof(int)));
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, lstP.ToArray());

                if (BusinessUtility.GetInt(o) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {

                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool SaveOrderAddresse(int ordID, string addressLine1, string addressLine2, string addressLine3, string addressCity, string addressState, string addressCountry, string addressPostalCode, string addType)
        {
            DbHelper dbHelp = new DbHelper(true);
            string sqlCheckcount = "SELECT COUNT(*) FROM OrderAddress WHERE ordID=@ordID AND addType = @addType ";
            string sqlInsert = "INSERT INTO OrderAddress (ordID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, addType )";
            sqlInsert += " VALUES ( @ordID, @addressLine1, @addressLine2, @addressLine3, @addressCity, @addressState, @addressCountry, @addressPostalCode,  @addType  )";
            string sqlUpdate = "UPDATE OrderAddress SET addressLine1 = @addressLine1, addressLine2 = @addressLine2, addressLine3 = @addressLine3, addressCity = @addressCity, addressState = @addressState, addressCountry = @addressCountry, addressPostalCode = @addressPostalCode, addType = @addType  WHERE ordID = @ordID AND addType = @addType  ";
            try
            {
                object check = dbHelp.GetValue(sqlCheckcount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", ordID, MyDbType.Int),
                    DbUtility.GetParameter("addType", addType, MyDbType.String),
                });
                if (BusinessUtility.GetInt(check) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", addressState, MyDbType.String),
                        DbUtility.GetParameter("ordID", ordID, MyDbType.Int),
                        DbUtility.GetParameter("addType", addType, MyDbType.String),
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("addressLine1", addressLine1, MyDbType.String),
                        DbUtility.GetParameter("addressLine2", addressLine2, MyDbType.String),
                        DbUtility.GetParameter("addressLine3", addressLine3, MyDbType.String),                                  
                        DbUtility.GetParameter("addressCity", addressCity, MyDbType.String),
                        DbUtility.GetParameter("addressCountry", addressCountry, MyDbType.String),
                        DbUtility.GetParameter("addressPostalCode", addressPostalCode, MyDbType.String),
                        DbUtility.GetParameter("addressState", addressState, MyDbType.String),
                        DbUtility.GetParameter("ordID", ordID, MyDbType.Int),
                        DbUtility.GetParameter("addType", addType, MyDbType.String),
                    });
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool HasOrderToShiftQty(int orderID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            StringBuilder sbQuery = new StringBuilder();
            List<MySqlParameter> lstP = new List<MySqlParameter>();
            sbQuery.Append(" SELECT i.ordID, ");
            sbQuery.Append(" ordProductQty- IFNULL( (SELECT SUM(ordProductQty) FROM orders ord JOIN orderitems oi ON oi.ordID = ord.ordID ");
            sbQuery.Append(" WHERE ParentOrderID = @parentOrderID AND oi.ordProductID = p.productID),0) AS BalanceQty ");
            sbQuery.Append(" FROM orderitems i ");
            sbQuery.Append(" INNER JOIN orders o ON o.ordID=i.ordID ");
            sbQuery.Append(" INNER JOIN products p ON i.ordProductID=p.productID ");
            sbQuery.Append(" HAVING i.ordID = @parentOrderID AND BalanceQty >0 ");
            lstP.Add(DbUtility.GetParameter("parentOrderID", orderID, typeof(int)));
            try
            {
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString( sbQuery), CommandType.Text, lstP.ToArray());

                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {

                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public class OrderHoldReason
    {
        public int OrderID { get; set; }
        public OrderHeldReasonCode OrdHeldReasonCode { get; set; }
        public string OtherReason { get; set; }

        public void PopulateObject(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_order_held_reason WHERE OrderID=@OrderID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderId", orderID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.OrderID = BusinessUtility.GetInt(dr["OrderID"]);
                        this.OrdHeldReasonCode = (OrderHeldReasonCode)BusinessUtility.GetInt(dr["OrdHeldReasonCode"]);
                        this.OtherReason = BusinessUtility.GetString(dr["OtherReason"]);
                    }
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }


}
