﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InvoiceCollection
    {
        public int CollectionID { get; set; }
        public int ColInvID { get; set; }
        public int ColUserAssignedTo { get; set; }
        public DateTime ColUserAssignedDatetime { get; set; }
        public int ColUserAssignedByManager { get; set; }
        public DateTime ColLastActivity { get; set; }
        public string ColNotes { get; set; }
        public string ColStatus { get; set; }

        public bool Insert(DbHelper dbHelp, int assignedBy)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(*) FROM collection WHERE ColInvID=@ColInvID";
            string sqlInsert = "INSERT INTO collection(ColInvID,ColUserAssignedTo,ColUserAssignedDatetime,ColUserAssignedByManager,ColLastActivity,ColNotes,ColStatus)";
            sqlInsert += " VALUES(@ColInvID,@ColUserAssignedTo,@ColUserAssignedDatetime,@ColUserAssignedByManager,@ColLastActivity,@ColNotes,@ColStatus)";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ColInvID", this.ColInvID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    return false;
                }
                return dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ColInvID", this.ColInvID, MyDbType.Int),
                    DbUtility.GetParameter("ColUserAssignedTo", this.ColUserAssignedTo, MyDbType.Int),
                    DbUtility.GetParameter("ColUserAssignedDatetime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColUserAssignedByManager", this.ColUserAssignedByManager, MyDbType.Int),
                    DbUtility.GetParameter("ColLastActivity", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColNotes", this.ColNotes, MyDbType.String),
                    DbUtility.GetParameter("ColStatus", this.ColStatus, MyDbType.String)
                }) > 0;                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetAllInvoiceToBeCollection(ParameterCollection pCol, string searchBy, int daysRange, string custName, string custPhone, int invNo, string assignTo, int userID, bool isSalesRestricted)
        {
            pCol.Clear();
            string strSQL = string.Empty;
            if (!isSalesRestricted)
            {
                strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, ";
                strSQL += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo,";
                strSQL += " ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, ";
                strSQL += " o.invRefType, funGetInvoiceTotal(o.InvID) AS InvAmount, funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID)";
                strSQL += " AS BalanceAmount FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  ";
                strSQL += " on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID  ";
            }
            else
            {
                strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, ";
                strSQL += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, ";
                strSQL += " ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, ";
                strSQL += " o.invRefType, funGetInvoiceTotal(o.InvID) AS InvAmount, funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID)";
                strSQL += " AS BalanceAmount FROM invoices o Inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType";
                strSQL += " and s.UserID=@UserID inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID";
                strSQL += " left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID  ";
                pCol.Add("@UserID", userID.ToString());
            }
            if (!string.IsNullOrEmpty(assignTo))
            {
                strSQL += " Inner join users u on u.userID=c.ColUserAssignedTo";
            }
            else
            {
                strSQL += " left join users u on u.userID=c.ColUserAssignedTo";
            }
            strSQL += " WHERE o.invStatus<>@Status AND funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID) > 0";
            pCol.Add("@Status", InvoicesStatus.PAYMENT_RECEIVED);
            if (searchBy == "NA")
            {
                strSQL += " AND c.ColUserAssignedTo IS NULL ";
            }
            else if (searchBy == "AS")
            {
                strSQL += " AND c.ColUserAssignedTo IS NOT NULL ";
            }
            else { }
            if (!string.IsNullOrEmpty(custName))
            {
                strSQL += " AND (PartnerLongName LIKE CONCAT('%', @CustName, '%') OR PartnerAcronyme LIKE CONCAT('%', @CustName, '%')) ";
                pCol.Add("@CustName", custName);
            }
            if (!string.IsNullOrEmpty(custPhone))
            {
                strSQL += " AND (PartnerPhone LIKE CONCAT('%', @CustPhone, '%')) ";
                pCol.Add("@CustPhone", custPhone);
            }
            if (invNo > 0)
            {
                strSQL += " and InvRefNo=@InvRefNo";
                pCol.Add("@InvRefNo", invNo.ToString());
            }
            if (!string.IsNullOrEmpty(assignTo))
            {
                strSQL += " AND CONCAT(u.userFirstName,' ', IFNULL(u.userLastName, '')) LIKE CONCAT('%', @AssignTo, '%') ";
                pCol.Add("@AssignTo", assignTo);
            }
            switch (daysRange)
            {
                case 10:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 10)  and o.invCreatedOn < now())";
                    break;
                case 15:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 15)  and o.invCreatedOn < SUBDATE(now(), 10))";
                    break;
                case 30:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 30)  and o.invCreatedOn < SUBDATE(now(), 15))";
                    break;
                case 60:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 60)  and o.invCreatedOn < SUBDATE(now(), 30))";
                    break;
                case 90:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 90)  and o.invCreatedOn < SUBDATE(now(), 60))";
                    break;
                case 180:
                    strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 180)  and o.invCreatedOn < SUBDATE(now(), 90))";
                    break;
            }
            strSQL += " GROUP BY o.invID, a.ARInvoiceNo ORDER BY o.invCreatedOn, o.invID DESC,  CustomerName ";
            return strSQL;
        }

        public string GetAllInvoiceToBeFollowup(ParameterCollection pCol, string searchBy, int daysRange, string custName, string custPhone, int invNo, string assignTo, int userID)
        {
            pCol.Clear();
            string strSQL = string.Empty;            
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, ";
            strSQL += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, ";
            strSQL += " ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, ";
            strSQL += " o.invRefType, funGetInvoiceTotal(o.InvID) AS InvAmount, ";
            strSQL += " funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID) AS BalanceAmount ";
            strSQL += " FROM invoices o INNER JOIN invoiceitems i ON i.invoices_invID=o.invID LEFT JOIN partners  ";
            strSQL += " ON partners.PartnerID =o.invCustID LEFT JOIN accountreceivable a on a.ARInvoiceNo=o.invID LEFT JOIN collection c ON c.ColInvID=o.invID  ";
            //if (CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            //{
            //    strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, ";
            //    strSQL += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, ";
            //    strSQL += " ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, ";
            //    strSQL += " o.invRefType, funGetInvoiceTotal(o.InvID) AS InvAmount, ";
            //    strSQL += " funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID) AS BalanceAmount ";
            //    strSQL += " FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID LEFT JOIN partners  ";
            //    strSQL += " ON partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID  ";
            //}
            //else
            //{
            //    strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, ";
            //    strSQL += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, ";
            //    strSQL += " ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, ";
            //    strSQL += " o.invRefType, funGetInvoiceTotal(o.InvID) AS InvAmount, ";
            //    strSQL += " funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID) AS BalanceAmount ";
            //    strSQL += " FROM invoices o INNER JOIN salesrepcustomer s ON s.CustomerID=o.invCustID AND s.CustomerType=o.invCustType AND s.UserID=@UserID ";
            //    strSQL += " INNER JOIN invoiceitems i ON i.invoices_invID=o.invID LEFT JOIN partners  ON partners.PartnerID =o.invCustID ";
            //    strSQL += " LEFT JOIN accountreceivable a on a.ARInvoiceNo=o.invID LEFT JOIN collection c ON c.ColInvID=o.invID  ";
            //    pCol.Add("@UserID", userID.ToString());
            //}
            if (!string.IsNullOrEmpty(assignTo))
            {
                strSQL += " INNER JOIN users u ON u.userID=c.ColUserAssignedTo";
            }
            else
            {
                strSQL += " LEFT JOIN users u ON u.userID=c.ColUserAssignedTo";
            }
            strSQL += " WHERE o.invStatus<>@Status AND funGetInvoiceTotal(o.InvID) - funGetReceivedAmountByInvoiceID(o.InvID) > 0";
            pCol.Add("@Status", InvoicesStatus.PAYMENT_RECEIVED);
            if (!CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                strSQL += " AND u.userID=@UserID";
                pCol.Add("@UserID", userID.ToString());
            }
            if (searchBy == "NA")
            {
                strSQL += " AND c.ColUserAssignedTo IS NULL ";
            }
            else if (searchBy == "AS")
            {
                strSQL += " AND c.ColUserAssignedTo IS NOT NULL ";
            }
            else { }
            if (!string.IsNullOrEmpty(custName))
            {
                strSQL += " AND (PartnerLongName LIKE CONCAT('%', @CustName, '%') OR PartnerAcronyme LIKE CONCAT('%', @CustName, '%')) ";
                pCol.Add("@CustName", custName);
            }
            if (!string.IsNullOrEmpty(custPhone))
            {
                strSQL += " AND (PartnerPhone LIKE CONCAT('%', @CustPhone, '%')) ";
                pCol.Add("@CustPhone", custPhone);
            }
            if (invNo > 0)
            {
                strSQL += " and InvRefNo=@InvRefNo";
                pCol.Add("@InvRefNo", invNo.ToString());
            }
            if (!string.IsNullOrEmpty(assignTo))
            {
                strSQL += " AND CONCAT(u.userFirstName,' ', IFNULL(u.userLastName, '')) LIKE CONCAT('%', @AssignTo, '%') ";
                pCol.Add("@AssignTo", assignTo);
            }
            DateTime dtNow, fDate, tDate;
            dtNow = DateTime.Now;
            switch (daysRange)
            {
                case 10:
                    fDate = dtNow.Date.AddDays(-10);
                    tDate = dtNow;
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 10)  and o.invCreatedOn < now())";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case 15:
                    fDate = dtNow.Date.AddDays(-15);
                    tDate = dtNow.Date.AddDays(-10).AddHours(23).AddMinutes(59).AddSeconds(59);
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 15)  and o.invCreatedOn < SUBDATE(now(), 10))";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case 30:
                    fDate = dtNow.Date.AddDays(-30);
                    tDate = dtNow.Date.AddDays(-15).AddHours(23).AddMinutes(59).AddSeconds(59);
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 30)  and o.invCreatedOn < SUBDATE(now(), 15))";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case 60:
                    fDate = dtNow.Date.AddDays(-60);
                    tDate = dtNow.Date.AddDays(-30).AddHours(23).AddMinutes(59).AddSeconds(59);
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 60)  and o.invCreatedOn < SUBDATE(now(), 30))";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case 90:
                    fDate = dtNow.Date.AddDays(-90);
                    tDate = dtNow.Date.AddDays(-60).AddHours(23).AddMinutes(59).AddSeconds(59);
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 90)  and o.invCreatedOn < SUBDATE(now(), 60))";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case 180:
                    fDate = dtNow.Date.AddDays(-180);
                    tDate = dtNow.Date.AddDays(-90).AddHours(23).AddMinutes(59).AddSeconds(59);
                    //strSQL += " AND (o.invCreatedOn >= SUBDATE(now(), 180)  and o.invCreatedOn < SUBDATE(now(), 90))";
                    strSQL += " AND o.invCreatedOn BETWEEN @FromDate AND @ToDate";
                    pCol.Add("@FromDate", fDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    pCol.Add("@ToDate", tDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
            }
            strSQL += " GROUP BY o.invID, a.ARInvoiceNo ORDER BY o.invCreatedOn, o.invID DESC,  CustomerName ";
            return strSQL;
        }
    }
}
