﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
namespace iTECH.InbizERP.BusinessLogic
{
    public class CollectionLog
    {
        public int CollectionLogID { get; set; }
        public int ColHeaderID { get; set; }
        public DateTime ColFollowup { get; set; }
        public int ColFollowupUserID { get; set; }
        public string ColLogText { get; set; }
        public DateTime ColActivityDatetime { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }

            string sql = "INSERT INTO collectionlog (ColHeaderID,ColFollowup,ColFollowupUserID,ColLogText,ColActivityDatetime) VALUES(@ColHeaderID,@ColFollowup,@ColFollowupUserID,@ColLogText,@ColActivityDatetime)";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ColHeaderID", this.ColHeaderID, MyDbType.Int),
                    DbUtility.GetParameter("ColFollowup", this.ColFollowup, MyDbType.DateTime),
                    DbUtility.GetParameter("ColFollowupUserID", this.ColFollowupUserID, MyDbType.Int),
                    DbUtility.GetParameter("ColLogText", this.ColLogText, MyDbType.String),
                    DbUtility.GetParameter("ColActivityDatetime", DateTime.Now, MyDbType.DateTime)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetAllCollectionLog(ParameterCollection pCol, int custID, int colID)
        {
            pCol.Clear();
            string strSQL = "SELECT  o.InvRefNo, CollectionLogID, ColHeaderID, ColFollowup, ColFollowupUserID, ColLogText, ColActivityDatetime FROM collectionlog  left join invoices o on o.invID=collectionlog.ColHeaderID ";
            if (custID > 0)
            {
                strSQL += " WHERE invCustID=@CustID";
                strSQL += " ORDER BY o.invID DESC ";
                pCol.Add("@CustID", custID.ToString());
            }
            else
            {
                strSQL += " WHERE ColHeaderID=@ColID";
                strSQL += " ORDER BY ColActivityDatetime DESC ";
                pCol.Add("@ColID", colID.ToString());
            }            
            return strSQL;
        }
    }
}
