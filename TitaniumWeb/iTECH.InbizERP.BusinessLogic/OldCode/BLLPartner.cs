﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;

namespace iTECH.InbizERP.BusinessLogic
{
    public class BLLPartner
    {
        public BLLPartner()
        {
 
        }

        public DataTable GetSearchResult(string what, string where, string sBy)
        {
            DbHelper objData = new DbHelper();

            StringBuilder sqlSearch = new StringBuilder();
            sqlSearch.Append("SELECT PartnerID, PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,");
            sqlSearch.Append("PartnerActive AS PartnerActiveForCsv, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy,");
            sqlSearch.Append("PartnerType, PartnerNote FROM  partners WHERE 1=1");
            switch (sBy)
            {
                case SearchByWhat.CATEGORY:
                    sqlSearch.Append(" AND (PartnerID IN (SELECT DISTINCT cdl_customercategories.CustomerID");
                    sqlSearch.Append(" FROM cdl_customercategories INNER JOIN cdl_syscategories ON cdl_customercategories.CategoryID = cdl_syscategories.CategoryID");
                    sqlSearch.Append(" INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID");
                    sqlSearch.Append(" WHERE (cdl_textdescriptor.TextDescription LIKE CONCAT('%', @What, '%'))))");
                    break;
                case SearchByWhat.BUSINESS:
                    sqlSearch.Append(" AND ((PartnerLongName LIKE CONCAT('%', @What, '%')) OR (PartnerPhone LIKE CONCAT('%', @What, '%')))");
                    break;                
                default:
                    sqlSearch.Append(" AND (PartnerID IN (SELECT DISTINCT cdl_customercategories.CustomerID");
                    sqlSearch.Append(" FROM cdl_customercategories INNER JOIN cdl_syscategories ON cdl_customercategories.CategoryID = cdl_syscategories.CategoryID");
                    sqlSearch.Append(" INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID");
                    sqlSearch.Append(" WHERE (cdl_textdescriptor.TextDescription LIKE CONCAT('%', @What, '%'))))");
                    break;
            }

            sqlSearch.Append(" AND (PartnerID IN (SELECT DISTINCT addressSourceID");
            sqlSearch.Append(" FROM  addresses");
            sqlSearch.Append(" WHERE  (addressRef = @addressRef) AND (addressType = @addressType)");
            sqlSearch.Append(" AND ((addressCountry LIKE CONCAT('%', @Where, '%')) OR");
            sqlSearch.Append("  (addressState LIKE CONCAT('%', @Where, '%')) OR");
            sqlSearch.Append("  (addressCity LIKE CONCAT('%', @Where, '%')) OR");
            sqlSearch.Append(" (addressPostalCode LIKE CONCAT('%', @Where, '%')))))");
            sqlSearch.Append(" ORDER BY partners.PartnerLongName");

            List<MySqlParameter> objParams = new List<MySqlParameter>();
            if (sqlSearch.ToString().Contains("@What"))
            {
                objParams.Add(new MySqlParameter("@What", what));
            }
            objParams.Add(new MySqlParameter("@addressRef", AddressReference.DISTRIBUTER));
            objParams.Add(new MySqlParameter("@addressType", AddressType.BILL_TO_ADDRESS));
            objParams.Add(new MySqlParameter("@Where", where));

            return objData.GetDataTable(sqlSearch.ToString(), CommandType.Text, objParams.ToArray());
        }

        public DataTable GetBusinessDetails(int custId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT partners.PartnerLongName, partners.PartnerPhone, partners.PartnerFax, partners.PartnerEmail, partners.PartnerWebsite,");
            sbSql.Append("addresses.addressLine1, addresses.addressLine2, addresses.addressLine3, addresses.addressCity, addresses.addressState,");
            sbSql.Append("addresses.addressCountry, addresses.addressPostalCode FROM  partners INNER JOIN addresses ON partners.PartnerID = addresses.addressSourceID");
            sbSql.Append(" WHERE  (partners.PartnerID = @PartnerID) AND (addresses.addressRef = @addressRef) AND (addresses.addressType = @addressType)");

            MySqlParameter[] p = {
                                     new MySqlParameter("@PartnerID", custId),
                                     new MySqlParameter("@addressRef", AddressReference.DISTRIBUTER),
                                     new MySqlParameter("@addressType", AddressType.BILL_TO_ADDRESS)
                                 };

            return new DbHelper().GetDataTable(sbSql.ToString(), CommandType.Text, p);
        }

        public DataTable GetBusinessAddress(int custId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT addresses.addressLine1, addresses.addressLine2, addresses.addressLine3, addresses.addressCity, addresses.addressState,");
            sbSql.Append("addresses.addressCountry, addresses.addressPostalCode FROM  addresses");
            sbSql.Append(" WHERE  (addresses.addressSourceID = @PartnerID) AND (addresses.addressRef = @addressRef) AND (addresses.addressType = @addressType)");

            MySqlParameter[] p = {
                                     new MySqlParameter("@PartnerID", custId),
                                     new MySqlParameter("@addressRef", AddressReference.DISTRIBUTER),
                                     new MySqlParameter("@addressType", AddressType.BILL_TO_ADDRESS)
                                 };

            return new DbHelper().GetDataTable(sbSql.ToString(), CommandType.Text, p);
        }

        public List<string> GetAutoCompleteBusinessNames(string startsWith, int appLang)
        {
            List<string> strList = new List<string>();
            string sql = "SELECT PartnerLongName FROM partners WHERE (PartnerLongName LIKE CONCAT(@StartsWith, '%') OR PartnerPhone LIKE CONCAT(@StartsWith, '%'))";
            MySqlParameter[] parameters = {
                                              new MySqlParameter("@StartsWith", startsWith)
                                          };
            DbHelper objDataAccess = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = objDataAccess.GetDataReader(sql, CommandType.Text, parameters);
                while (dr.Read())
                {
                    strList.Add(dr.GetString(0));
                }
                dr.Close();
                dr.Dispose();

                List<string> lstCat = new BLLCategories().GetAutoCompleteCategories(startsWith, appLang);
                strList.AddRange(lstCat);
                strList.Sort();
            }
            catch
            {

                throw;
            }
            finally
            {                
                objDataAccess.CloseDatabaseConnection(dr);
            }
           
            return strList;
        }

        public List<string> GetAutoCompleteLocations(string startsWith)
        {
            List<string> strList = new List<string>();
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("SELECT DISTINCT  addressCity AS ResultText FROM addresses");
            sbSQL.Append(" WHERE addressCity LIKE CONCAT(@StartsWith, '%')");
            sbSQL.Append(" UNION SELECT DISTINCT  addressState AS ResultText FROM addresses");
            sbSQL.Append(" WHERE addressState LIKE CONCAT(@StartsWith, '%')");
            sbSQL.Append(" UNION SELECT DISTINCT  addressCountry AS ResultText FROM addresses");
            sbSQL.Append(" WHERE addressCountry LIKE CONCAT(@StartsWith, '%')");
            sbSQL.Append(" UNION SELECT DISTINCT  addressPostalCode AS ResultText FROM addresses");
            sbSQL.Append(" WHERE addressPostalCode LIKE CONCAT(@StartsWith, '%')");
            MySqlParameter[] parameters = {
                                              new MySqlParameter("@StartsWith", startsWith)
                                          };
            DbHelper objDataAccess = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = objDataAccess.GetDataReader(sbSQL.ToString(), CommandType.Text, parameters);
                while (dr.Read())
                {
                    strList.Add(dr.GetString(0));
                }
            }
            catch
            {
                throw;
            }
            finally
            {                
                objDataAccess.CloseDatabaseConnection(dr);
            }

            return strList;
        }
    }
}
