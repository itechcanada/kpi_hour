﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
namespace iTECH.InbizERP.BusinessLogic
{
    public class SysFiles
    {        
        public int FileID { get; set; }
        public string FileTableRef { get; set; }
        public int FileRefID { get; set; }
        public string FileType { get; set; }
        public string FileFormat { get; set; }
        public string FilePath { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public int GetFilesCount(DbHelper dbHelp, string refTable, int refID, string fileType) {
            string sqlFileCount = "SELECT COUNT(*) FROM z_sys_files WHERE FileTableRef=@FileTableRef AND FileRefID=@FileRefID AND FileType=@FileType";

            try
            {
                object fCount = dbHelp.GetValue(sqlFileCount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FileTableRef", refTable, MyDbType.String),
                    DbUtility.GetParameter("FileRefID", refID, MyDbType.Int),                    
                    DbUtility.GetParameter("FileType", fileType, MyDbType.String)
                });

                return BusinessUtility.GetInt(fCount);
            }
            catch 
            {
                throw;
            }            
        }

        public int GetFilesCount(string refTable, int refID, string fileType)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.GetFilesCount(dbHelp, refTable, refID, fileType);
            }
            catch 
            {                
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Insert(int userID, int maxFileCount)
        {            
            string sqlExists = "SELECT COUNT(*) FROM z_sys_files WHERE FileTableRef=@FileTableRef AND FileRefID=@FileRefID AND FilePath=@FilePath AND FileType=@FileType";

            string sql = "INSERT INTO z_sys_files (FileTableRef,FileRefID, FileType,FileFormat,FilePath,CreatedBy,CreatedOn)";
            sql += "  VALUES(@FileTableRef,@FileRefID, @FileType,@FileFormat,@FilePath,@CreatedBy,@CreatedOn)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int fCount = this.GetFilesCount(dbHelp, this.FileTableRef, this.FileRefID, this.FileType);

                if (fCount >= maxFileCount)
                {
                    throw new Exception(CustomExceptionCodes.MAX_FILE_COUNT_EXCEEDED);
                }

                object val = dbHelp.GetValue(sqlExists, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FileTableRef", this.FileTableRef, MyDbType.String),
                    DbUtility.GetParameter("FileRefID", this.FileRefID, MyDbType.Int),
                    DbUtility.GetParameter("FilePath", this.FilePath, MyDbType.String),
                    DbUtility.GetParameter("FileType", this.FileType, MyDbType.String)
                });

                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.FILE_ALREAY_EXISTS);
                }

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FileTableRef", this.FileTableRef, MyDbType.String),
                    DbUtility.GetParameter("FileRefID", this.FileRefID, MyDbType.Int),
                    DbUtility.GetParameter("FileFormat", this.FileFormat, MyDbType.String),
                    DbUtility.GetParameter("FilePath", this.FilePath, MyDbType.String),
                    DbUtility.GetParameter("CreatedBy", this.CreatedBy, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("FileType", this.FileType, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int fileID)
        {
            string sqlFile = "SELECT FilePath FROM z_sys_files WHERE FileID=@FileID";
            string sqlDelete = "DELETE FROM z_sys_files WHERE FileID=@FileID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object fl = dbHelp.GetValue(sqlFile, CommandType.Text, new MySqlParameter[] { 
                   DbUtility.GetParameter("FileID", fileID, MyDbType.Int)
                });

                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                   DbUtility.GetParameter("FileID", fileID, MyDbType.Int)
                });

                //Need to delete file from file system
                string fn = CommonFilePath.USER_FILES + BusinessUtility.GetString(fl);
                fn = HttpContext.Current.Server.MapPath(fn);
                if (System.IO.File.Exists(fn))
                {
                    System.IO.File.Delete(fn);
                }
            }
            catch 
            {
                
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, string tblRef, int refID) {
            pCol.Clear();
            string sql = "SELECT * FROM z_sys_files WHERE FileTableRef=@FileTableRef AND FileRefID=@FileRefID";
            pCol.Add("@FileTableRef", tblRef);
            pCol.Add("@FileRefID", refID.ToString());

            return sql;
        }

        //For Superpagin 
        public static string GetLogoImagePath(int customerID) {
            string sql = "SELECT * FROM z_sys_files WHERE FileTableRef=@FileTableRef AND FileRefID=@FileRefID LIMIT 0,1"; //Assume first file as logo for customer
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FileTableRef", TableRef.CUSTOMER, MyDbType.String),
                    DbUtility.GetParameter("FileRefID", customerID, MyDbType.Int)
                });
                SysFiles f = new SysFiles();
                if (dr.Read())
                {
                    
                    f.CreatedBy = BusinessUtility.GetInt(dr["CreatedBy"]);
                    f.CreatedOn = BusinessUtility.GetDateTime(dr["CreatedOn"]);
                    f.FileFormat = BusinessUtility.GetString(dr["FileFormat"]);
                    f.FileID = BusinessUtility.GetInt(dr["FileID"]);
                    f.FilePath = BusinessUtility.GetString(dr["FilePath"]);
                    f.FileRefID = BusinessUtility.GetInt(dr["FileRefID"]);
                    f.FileTableRef = BusinessUtility.GetString(dr["FileTableRef"]);
                    f.FileType = BusinessUtility.GetString(dr["FileType"]);                    
                }

                return f.FilePath;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public static string GetMenuImagePath(int customerID)
        {
            string sql = "SELECT * FROM z_sys_files WHERE FileTableRef=@FileTableRef AND FileRefID=@FileRefID LIMIT 1,1"; //Assume 2nd file as menu Image for customer
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FileTableRef", TableRef.CUSTOMER, MyDbType.String),
                    DbUtility.GetParameter("FileRefID", customerID, MyDbType.Int)
                });
                SysFiles f = new SysFiles();
                if (dr.Read())
                {

                    f.CreatedBy = BusinessUtility.GetInt(dr["CreatedBy"]);
                    f.CreatedOn = BusinessUtility.GetDateTime(dr["CreatedOn"]);
                    f.FileFormat = BusinessUtility.GetString(dr["FileFormat"]);
                    f.FileID = BusinessUtility.GetInt(dr["FileID"]);
                    f.FilePath = BusinessUtility.GetString(dr["FilePath"]);
                    f.FileRefID = BusinessUtility.GetInt(dr["FileRefID"]);
                    f.FileTableRef = BusinessUtility.GetString(dr["FileTableRef"]);
                    f.FileType = BusinessUtility.GetString(dr["FileType"]);
                }

                return f.FilePath;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}
