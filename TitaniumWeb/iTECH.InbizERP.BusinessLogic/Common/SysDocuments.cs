﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysDocuments
    {
        public int SysDocId { get; set; }
        public string SysDocType { get; set; }
        public string SysDocDistType { get; set; }
        public DateTime SysDocDistDateTime { get; set; }
        public string SysDocPath { get; set; }
        public string SysDocEmailTo { get; set; }
        public string SysDocFaxToPhone { get; set; }
        public string SysDocFaxToAttention { get; set; }
        public string SysDocBody { get; set; }
        public string SysDocSubject { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO sysdocuments(sysDocType,sysDocDistType,sysDocDistDateTime,sysDocPath,sysDocEmailTo,sysDocFaxToPhone,sysDocFaxToAttention,sysDocBody,sysDocSubject)";
            sql += " VALUES(@sysDocType,@sysDocDistType,@sysDocDistDateTime,@sysDocPath,@sysDocEmailTo,@sysDocFaxToPhone,@sysDocFaxToAttention,@sysDocBody,@sysDocSubject)";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysDocType", this.SysDocType, MyDbType.String),
                    DbUtility.GetParameter("sysDocDistType", this.SysDocDistType, MyDbType.String),
                    DbUtility.GetParameter("sysDocDistDateTime", this.SysDocDistDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("sysDocPath", this.SysDocPath, MyDbType.String),
                    DbUtility.GetParameter("sysDocEmailTo", this.SysDocEmailTo, MyDbType.String),
                    DbUtility.GetParameter("sysDocFaxToPhone", this.SysDocFaxToPhone, MyDbType.String),
                    DbUtility.GetParameter("sysDocFaxToAttention", this.SysDocFaxToAttention, MyDbType.String),
                    DbUtility.GetParameter("sysDocBody", this.SysDocBody, MyDbType.String),
                    DbUtility.GetParameter("sysDocSubject", this.SysDocSubject, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
