﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public class GridViewState
    {
        //private List<SearchKeyValue> _searchHash = new List<SearchKeyValue>();
        //public List<SearchKeyValue> SearchHash
        //{
        //    get { return _searchHash; }
        //    set { _searchHash = value; }
        //}

        private Dictionary<string, string> _queryStringKeyVal;
        public Dictionary<string, string> QueryStringKeyVal
        {
            get { return _queryStringKeyVal; }
            set { _queryStringKeyVal = value; }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set { _currentPage = value; }
        }

        private int _pageSize;
        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }

        private string _sortExpression;
        public string SortExpression
        {
            get { return _sortExpression; }
            set { _sortExpression = value; }
        }

        private string _sortDirection;
        public string SortDirection
        {
            get { return _sortDirection; }
            set { _sortDirection = value; }
        }

        public GridViewState()
        {
            _queryStringKeyVal = new Dictionary<string, string>();
            _currentPage = 1;
            _pageSize = 20;
            _sortDirection = "asc";
            _sortExpression = string.Empty;
        }
    }
}
