﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderActionLog
    {
        public int ActionLogID { get; set; }
        public int OrderID { get; set; }
        public DateTime ActionDate { get; set; }
        public string ActionName { get; set; }
        public string ActionBy { get; set; }
        public double OrderTotal { get; set; }
        public double ReceivedAmount { get; set; }
        public double OutstandingBalance { get; set; }
        public string Comments { get; set; }
        public string RawData { get; set; }        

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "INSERT INTO z_order_action_log(OrderID,ActionDate,ActionName,ActionBy,OrderTotal,ReceivedAmount,OutstandingBalance,Comments,RawData)";
                sql += " VALUES(@OrderID,@ActionDate,@ActionName,@ActionBy,@OrderTotal,@ReceivedAmount,@OutstandingBalance,@Comments,@RawData)";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                    DbUtility.GetParameter("ActionDate", this.ActionDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ActionName", this.ActionName, MyDbType.String),
                    DbUtility.GetParameter("ActionBy", this.ActionBy, MyDbType.String),
                    DbUtility.GetParameter("OrderTotal", this.OrderTotal, MyDbType.Double),
                    DbUtility.GetParameter("ReceivedAmount", this.ReceivedAmount, MyDbType.Double),
                    DbUtility.GetParameter("OutstandingBalance", this.OutstandingBalance, MyDbType.Double),
                    DbUtility.GetParameter("Comments", this.Comments, MyDbType.String),
                    DbUtility.GetParameter("RawData", this.RawData, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddLog(DbHelper dbHelp, int by, int orderID, string action, string comments)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                dbHelp.OpenDatabaseConnection();
                string sqlOrder = "SELECT * FROM orders WHERE ordID=@OrderID";
                string sqlOrderItems = "SELECT * FROM orderitems WHERE ordID=@OrderID";
                string sqlOrderProcess = "SELECT * FROM orderitemprocess WHERE ordID=@OrderID";
                string sqlHeldReason = "SELECT * FROM z_order_held_reason WHERE OrderID=@OrderID";
                string sqlSalesRep = "SELECT * FROM z_order_sales_rep WHERE OrderID=@OrderID";
                string sqlOrderTransaction = "SELECT * FROM z_order_transaction WHERE OrderID=@OrderID";

                InbizUser usr = new InbizUser();

                this.ActionBy = usr.GetUserName(dbHelp, by);
                this.ActionDate = DateTime.Now;
                this.ActionName = action;
                this.Comments = comments;
                this.OrderID = orderID;

                TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);
                this.OrderTotal = ts.GrandTotal;
                this.OutstandingBalance = ts.OutstandingAmount;
                this.ReceivedAmount = ts.TotalAmountReceived;

                Dictionary<OrderMetaDataKey, DataTable> rawData = new Dictionary<OrderMetaDataKey, DataTable>();
                rawData[OrderMetaDataKey.OrderInfo] = dbHelp.GetDataTable(sqlOrder, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                rawData[OrderMetaDataKey.OrderItems] = dbHelp.GetDataTable(sqlOrderItems, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                rawData[OrderMetaDataKey.OrderProcessItems] = dbHelp.GetDataTable(sqlOrderProcess, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                rawData[OrderMetaDataKey.OrderHeldReason] = dbHelp.GetDataTable(sqlHeldReason, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                rawData[OrderMetaDataKey.OrderSalesRep] = dbHelp.GetDataTable(sqlSalesRep, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                rawData[OrderMetaDataKey.OrderTransactions] = dbHelp.GetDataTable(sqlOrderTransaction, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });

                this.RawData = JsonConvert.SerializeObject(rawData);
                this.Insert(dbHelp);
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public enum OrderMetaDataKey { 
        OrderInfo,
        OrderItems,
        OrderProcessItems,
        OrderHeldReason,
        OrderSalesRep,
        OrderTransactions
    }
}
