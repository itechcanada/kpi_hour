﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class NewAlert
    {
        public string AlertName { get; set; }
        public string AlertLang { get; set; }
        public string AlertDesc { get; set; }
        public string AlertSubject { get; set; }
        public string AlertBody { get; set; }

        public void Save(DbHelper dbHelp) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                string sql = "UPDATE z_alert SET AlertSubject=@AlertSubject, AlertBody=@AlertBody WHERE AlertName=@AlertName AND (AlertLang=@AlertLang)";                
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("AlertBody", this.AlertBody, MyDbType.String),
                    DbUtility.GetParameter("AlertLang", this.AlertLang, MyDbType.String),
                    DbUtility.GetParameter("AlertName", this.AlertName, MyDbType.String),
                    DbUtility.GetParameter("AlertSubject", this.AlertSubject, MyDbType.String)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public void FillAllAlerts(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem, string lang)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    string sql = "SELECT * FROM z_alert WHERE AlertLang=@AlertLang";
        //    try
        //    {
        //        lCtrl.Items.Clear();
        //        lCtrl.DataSource = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("AlertLang", lang, MyDbType.String) });
        //        lCtrl.DataTextField = "AlertName";
        //        lCtrl.DataValueField = "AlertName";
        //        lCtrl.DataBind();

        //        if (rootItem != null)
        //        {
        //            lCtrl.Items.Insert(0, rootItem);
        //        }
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public string GetSql(ParameterCollection pCol, string searchText, string lang)
        {
            pCol.Clear();
            string sql = "SELECT DISTINCT a.AlertName AS ID, a.AlertDesc FROM z_alert a WHERE AlertLang=@AlertLang";
            pCol.Add("@AlertLang", lang);
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " AND a.AlertDesc LIKE CONCAT('%', @SearchText ,'%')";
                pCol.Add("@SearchText", searchText);
            }
            return sql;
        }

        //public object GetAlertUsersJsoneObject(DbHelper dbHelp)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        string sql = "SELECT DISTINCT a.AlertName FROM z_alert a";
        //        var lstAl = new List<string>();
        //        using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
        //        {
        //            while (dr.Read())
        //            {
        //                lstAl.Add(BusinessUtility.GetString(dr[0]));
        //            }
        //        }

        //        var oList = new List<object>();
        //        string sqlUsers = "SELECT UserID FROM z_alert_user WHERE AlertName=@AlertName";
        //        foreach (var item in lstAl)
        //        {
        //            var uLst = new List<int>();
        //            using (MySqlDataReader dr = dbHelp.GetDataReader(sqlUsers, CommandType.Text, new MySqlParameter[]{ DbUtility.GetParameter("AlertName", item, MyDbType.String) }))
        //            {
        //                while (dr.Read())
        //                {
        //                    uLst.Add(BusinessUtility.GetInt(dr[0]));
        //                }
        //            }
        //            oList.Add(new { AName = item, UList = uLst.ToArray() });
        //        }

        //        return oList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if(mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public List<AlertUser> GetAlertUserList(DbHelper dbHelp, string alertType)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<AlertUser> lResult = new List<AlertUser>();
                string sql = "SELECT au.UserID, au.AlertName FROM z_alert_user au WHERE (IFNULL(@AlertName, '') = '' OR au.AlertName=@AlertName)";                
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("AlertName", alertType, MyDbType.String)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new AlertUser { AlertName = BusinessUtility.GetString(dr["AlertName"]), UserID = BusinessUtility.GetInt(dr["UserID"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
      
        public void PopulateByAlertName(DbHelper dbHelp, string alertType, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_alert WHERE AlertName=@AlertName AND AlertLang=@AlertLang";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("AlertName", alertType, MyDbType.String),
                    DbUtility.GetParameter("AlertLang", lang, MyDbType.String)
                }))
                {
                    if (dr.Read())
                    {
                        this.AlertBody = BusinessUtility.GetString(dr["AlertBody"]);
                        this.AlertLang = BusinessUtility.GetString(dr["AlertLang"]);
                        this.AlertName = BusinessUtility.GetString(dr["AlertName"]);
                        this.AlertSubject = BusinessUtility.GetString(dr["AlertSubject"]);
                        this.AlertDesc = BusinessUtility.GetString(dr["AlertDesc"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose)
                {
                    dbHelp.CloseDatabaseConnection();
                }
            }
        }

        public void AssignUsersToAlert(DbHelper dbHelp, string alertName, int[] users)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlDelete = "DELETE FROM z_alert_user WHERE AlertName=@AlertName";
                string sqlInsert = "INSERT INTO z_alert_user (AlertName, UserID) VALUES(@AlertName,@UserID)";
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("AlertName", alertName, MyDbType.String)
                });
                foreach (var item in users)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("AlertName", alertName, MyDbType.String),
                        DbUtility.GetParameter("UserID", item, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UnAssignUser(DbHelper dbHelp, string alertName, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlDelete = "DELETE FROM z_alert_user WHERE AlertName=@AlertName AND UserID=@UserID";                
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("AlertName", alertName, MyDbType.String),
                    DbUtility.GetParameter("UserID", userID, MyDbType.Int)
                });                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public class AlertUser
    {
        public string AlertName { get; set; }
        public int UserID { get; set; }
    }
}
