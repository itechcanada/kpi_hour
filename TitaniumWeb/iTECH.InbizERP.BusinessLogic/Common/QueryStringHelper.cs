﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Globalization;

using iTECH.Library.Utilities;
using iTECH.Library.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class QueryStringHelper
    {
        public static string GetPartnerType(string queryStringKey)
        {
            string pType = BusinessUtility.GetString(HttpContext.Current.Request.QueryString[queryStringKey]);
            switch (pType.ToUpper())
            {
                case StatusCustomerTypes.DISTRIBUTER:
                    return StatusCustomerTypes.DISTRIBUTER;
                case StatusCustomerTypes.END_CLINET:
                    return StatusCustomerTypes.END_CLINET;
                default:
                    return StatusCustomerTypes.DEFAULT;
            }
        }

        public static StatusGuestType GetGuestType(string queryStringKey)
        {
            int gType = BusinessUtility.GetInt(HttpContext.Current.Request.QueryString[queryStringKey]);
            switch (gType)
            {
                case (int)StatusGuestType.Guest:
                    return StatusGuestType.Guest;
                case (int)StatusGuestType.SpecialGuest:
                    return StatusGuestType.SpecialGuest;
                case (int)StatusGuestType.Staff:
                    return StatusGuestType.Staff;
                case (int)StatusGuestType.CourseParticipants:
                    return StatusGuestType.CourseParticipants;
                default:
                    return StatusGuestType.None;
            }     
        }

        public static StatusProductType GetProductType(string queryStringKey)
        {
            int pType = BusinessUtility.GetInt(HttpContext.Current.Request.QueryString[queryStringKey]);
            if (Enum.IsDefined(typeof(StatusProductType), pType))
            {
                return (StatusProductType)pType;
            }
            return StatusProductType.Default;
        }

        //public static InbizReports GetReportID(string queryStringKey)
        //{
        //    int id = BusinessUtility.GetInt(HttpContext.Current.Request.QueryString[queryStringKey]);
        //    if (Enum.IsDefined(typeof(InbizReports), id))
        //    {
        //        return (InbizReports)id;
        //    }
        //    return InbizReports.Default;
        //}
    }
}
