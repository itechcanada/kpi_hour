﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CalculationHelper
    {

        #region Obsolated Printing Total Functions (Were used in clsPrintHTML class in App_Code)

        public static string GetOrderTotalSummaryToPrint(int orderID)
        {
            try
            {
                string rowTemplate = "<tr><td valign='top' style='text-align:right;'><font style='font:Verdana;font-size:12px;font-weight:200;'>{0}:</font></td><td valign='top' style='text-align:right;'><font style='font:Verdana;font-size:12px;font-weight:200;'>{1}</font></td></tr>";
                TotalSummary lTotal = CalculationHelper.GetOrderTotal(orderID);
                string strHtml = "<table cellpadding='2' cellspacing='2' border='0'  align='right'>";

                strHtml += string.Format(rowTemplate, "Item Sub Total", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ItemSubTotal));

                foreach (var item in lTotal.ItemTaxList)
                {
                    strHtml += string.Format(rowTemplate, item.TaxCode, string.Format("{0} {1:F}", lTotal.CurrencyCode, item.CalculatedPrice));
                }
                strHtml += string.Format(rowTemplate, "Process Sub Total", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ProcessSubTotal));

                foreach (var item in lTotal.ProcessTaxList)
                {
                    strHtml += string.Format(rowTemplate, item.TaxCode, string.Format("{0} {1:F}", lTotal.CurrencyCode, item.CalculatedPrice));
                }
                strHtml += "<tr><td colspan='2' style='text-align:right;'><div style='border-top:1px dashed #000; height:2px;'>&nbsp;</div></td></tr>";
                strHtml += string.Format(rowTemplate, "Total", string.Format("{0} {1:F} ({2} {3:F})", lTotal.CurrencyCode, lTotal.GrandTotal, lTotal.BaseCurrencyCode, lTotal.GrandTotalBase));

                foreach (var item in lTotal.AmountReceivedList)
                {
                    strHtml += string.Format(rowTemplate, string.Format("Partial Payment On {0} Via {1}", BusinessUtility.GetDateTimeString(item.ReceivingDateTime, DateFormat.MMddyyyy), Globals.GetPaymentVia(item.ReceivedVia)), string.Format("{0} {1:F}", lTotal.CurrencyCode, item.Amount));
                }

                strHtml += string.Format(rowTemplate, "Balance Amount", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.OutstandingAmount));
                strHtml += "</table>";
                return strHtml;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetInvoiceTotalSummaryToPrint(int invoiceID)
        {
            try
            {
                string rowTemplate = "<tr><td valign='top' style='text-align:right;'><font style='font:Verdana;font-size:12px;font-weight:200;'>{0}:</font></td><td valign='top' style='text-align:right;'><font style='font:Verdana;font-size:12px;font-weight:200;'>{1}</font></td></tr>";
                TotalSummary lTotal = CalculationHelper.GetInvoiceTotal(invoiceID);
                string strHtml = "<table cellpadding='2' cellspacing='2' border='0'  align='right'>";

                strHtml += string.Format(rowTemplate, "Item Sub Total", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ItemSubTotal));
                foreach (var item in lTotal.ItemTaxList)
                {
                    strHtml += string.Format(rowTemplate, item.TaxCode, string.Format("{0} {1:F}", lTotal.CurrencyCode, item.CalculatedPrice));
                }
                strHtml += string.Format(rowTemplate, "Process Sub Total", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ProcessSubTotal));

                foreach (var item in lTotal.ProcessTaxList)
                {
                    strHtml += string.Format(rowTemplate, item.TaxCode, string.Format("{0} {1:F}", lTotal.CurrencyCode, item.CalculatedPrice));
                }
                strHtml += "<tr><td colspan='2' style='text-align:right;'><div style='border-top:1px dashed #000; height:2px;'>&nbsp;</div></td></tr>";
                strHtml += string.Format(rowTemplate, "Total", string.Format("{0} {1:F} ({2} {3:F})", lTotal.CurrencyCode, lTotal.GrandTotal, lTotal.BaseCurrencyCode, lTotal.GrandTotalBase));

                foreach (var item in lTotal.AmountReceivedList)
                {
                    strHtml += string.Format(rowTemplate, string.Format("Partial Payment On {0} Via {1}", BusinessUtility.GetDateTimeString(item.ReceivingDateTime, DateFormat.MMddyyyy), Globals.GetPaymentVia(item.ReceivedVia)), string.Format("{0} {1:F}", lTotal.CurrencyCode, item.Amount));
                }

                strHtml += string.Format(rowTemplate, "Balance Amount", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.OutstandingAmount));
                strHtml += "</table>";
                return strHtml;
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion

        #region New Methods

        public static TotalSummary GetOrderTotal(DbHelper dbHelp, int orderID)
        {
            try
            {
                TotalSummary ts = new TotalSummary();
                ts.InitOrderTotal(dbHelp, orderID);
                return ts;
            }
            catch
            {

                throw;
            }
        }

        public static TotalSummary GetOrderTotal(int orderID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return CalculationHelper.GetOrderTotal(dbHelp, orderID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static TotalSummary GetOrderTotal(DbHelper dbHelp, SalesOrderCart cart)
        {
            try
            {
                TotalSummary ts = new TotalSummary();
                ts.InitOrderTotal(dbHelp, cart);
                return ts;
            }
            catch
            {
                throw;
            }
        }

        public static TotalSummary GetPoOrderTotal(DbHelper dbHelp, PurchaseOrderCart cart)
        {
            try
            {
                TotalSummary ts = new TotalSummary();
                ts.InitPoOrderTotal(dbHelp, cart);
                return ts;
            }
            catch
            {
                throw;
            }
        }

        public static TotalSummary GetInvoiceTotal(DbHelper dbHelp, int invID)
        {
            try
            {
                TotalSummary ts = new TotalSummary();
                ts.InitInvoiceTotal(dbHelp, invID);
                return ts;
            }
            catch
            {
                throw;
            }
        }

        public static TotalSummary GetInvoiceTotal(int invID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return CalculationHelper.GetInvoiceTotal(dbHelp, invID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static double GetAmount(double amountInFraction)
        {
            if (Globals.IsAllowedRoundingDecimal)
            {
                return Math.Round(amountInFraction, Globals.DecimalPlaceToBeRound, MidpointRounding.AwayFromZero); //As mysql worked similar to this overload
            }
            return amountInFraction;
        }
        #endregion
    }

    public class TotalSummary
    {
        public double ItemSubTotal { get; private set; }
        public List<TaxItem> ItemTaxList { get; private set; }
        public double ProcessSubTotal { get; private set; }
        public List<TaxItem> ProcessTaxList { get; private set; }
        public string CurrencyCode { get; private set; }
        public string BaseCurrencyCode { get; private set; }
        public double GrandTotal { get; private set; }
        public double ExchangeRate { get; private set; }
        /// <summary>
        /// On Base of Base Currency
        /// </summary>
        public double GrandTotalBase { get; private set; }
        public List<AmountReceivedItem> AmountReceivedList { get; private set; }
        public double TotalAmountReceived { get; private set; }
        public double OutstandingAmount { get; private set; }
        public double AdditionalDiscount { get; private set; }
        public double TotalItemTax { get { return this.GetTotalItemTax(); } }
        public double TotalProcessTax { get { return this.GetTotalProcessTax(); } }
        public double TotalAdminFeeApplied { get; private set; }
        public double TotalReturnedAmount { get; private set; }

        public double ReturnedOverPaidAmount { get; private set; }
        public double ReturnedItemReturnAmount { get; private set; }
        //private Dictionary<LabelKey, string> Labels { get; set; }

        public TotalSummary()
        {

        }

        #region Private Methods
        public double GetBasePrice(double exchangeRate, double amount)
        {
            return amount / exchangeRate;
        }

        private double GetTotalAmountReceived()
        {
            double amt = 0.0D;
            foreach (var item in this.AmountReceivedList)
            {
                amt = CalculationHelper.GetAmount(amt + item.Amount);
            }
            return amt;
        }

        public double GetTotalWriteOff()
        {
            double amt = 0.0D;
            foreach (var item in this.AmountReceivedList)
            {
                amt = CalculationHelper.GetAmount(amt + item.WriteOff);
            }
            return amt;
        }

        private double GetTotalItemTax()
        {
            double amt = 0.0D;
            foreach (var item in this.ItemTaxList)
            {
                amt = CalculationHelper.GetAmount(amt + item.CalculatedPrice);
            }
            return amt;
        }

        private double GetTotalProcessTax()
        {
            double amt = 0.0D;
            foreach (var item in this.ProcessTaxList)
            {
                amt = CalculationHelper.GetAmount(amt + item.CalculatedPrice);
            }
            return amt;
        }

        #endregion

        public void InitOrderTotal(DbHelper dbHelp, int orderID)
        {
            try
            {
                this.ItemTaxList = new List<TaxItem>();
                this.ProcessTaxList = new List<TaxItem>();
                this.AmountReceivedList = new List<AmountReceivedItem>();

                Orders objOrder = new Orders();
                objOrder.PopulateObject(dbHelp, orderID);

                if (objOrder.OrdID <= 0)
                {
                    return;
                }

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(objOrder.OrdCompanyID, dbHelp);

                //string sqlOrderItems = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,sysTaxCodeDescText, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
                //sqlOrderItems += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
                //sqlOrderItems += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp where i.ordID=@ordID";
                string sqlOrderItems = "SELECT * FROM vw_order_item_amount WHERE OrderID=@OrderID";

                DataTable dt = dbHelp.GetDataTable(sqlOrderItems, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                Product prdAdminFee = new Product();
                int taxGrp;
                prdAdminFee.PopulateAdminFeeProduct(dbHelp);
                Tax tx;
                foreach (DataRow dr in dt.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ItemTaxList);
                    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + BusinessUtility.GetDouble(dr["Amount"]));

                    //Add to admin fee total
                    if (BusinessUtility.GetInt(dr["ProductID"]) == prdAdminFee.ProductID)
                    {
                        this.TotalAdminFeeApplied = CalculationHelper.GetAmount(this.TotalAdminFeeApplied + BusinessUtility.GetDouble(dr["Amount"]));
                    }
                }

                //string sqlProcessCost = "SELECT (ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits))*o.ordCurrencyExRate as ProcessCost, sysTaxCodeDescID FROM orderitemprocess i inner join orders o on o.ordID=i.ordID where i.ordID=@ordID";
                string sqlProcessCost = "SELECT * FROM vw_order_process_item_amount WHERE OrderID=@OrderID";
                DataTable dtProcess = dbHelp.GetDataTable(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                this.ProcessSubTotal = 0.0D;

                foreach (DataRow dr in dtProcess.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ProcessTaxList);
                    this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + BusinessUtility.GetDouble(dr["Amount"]));
                }


                //object val = dbHelp.GetValue(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
                //this.ProcessSubTotal = BusinessUtility.GetDouble(val);
                ////Process Tax Calculation
                //int processTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, objOrder.OrdShpWhsCode);

                //this.FillProcessTaxList(dbHelp, processTaxGrp, this.ProcessSubTotal);


                //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax; //Level one only for Items
                this.GrandTotal = this.ItemSubTotal; //Do not apply discount on taxes just exculde tax & process & their tax

                //Apply Additional Discount on products only not on services
                if (objOrder.OrdDiscountType.ToUpper().Trim() == "A")
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount(objOrder.OrdDiscount);
                }
                else
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)objOrder.OrdDiscount / 100.00D)));
                }

                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax); //Level to add process total                

                this.CurrencyCode = objOrder.OrdCurrencyCode;
                this.BaseCurrencyCode = ci.CompanyBasCur;
                this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(objOrder.OrdCurrencyExRate, this.GrandTotal));

                string sqlGetInvoiceID = "SELECT invID FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
                object obInvID = dbHelp.GetValue(sqlGetInvoiceID, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("invForOrderNo", orderID, MyDbType.Int),
                            DbUtility.GetParameter("invRefType", InvoiceReferenceType.INVOICE, MyDbType.String)
                        });
                int invID = BusinessUtility.GetInt(obInvID);
                if (invID > 0)
                {
                    //Get ReceivedAmount
                    string sqlGetRcvd = "SELECT ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARWriteOff, ARReceiptNo FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo";
                    MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    });

                    while (drRcvd.Read())
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                        this.AmountReceivedList.Add(ri);
                    }
                    if (drRcvd != null && !drRcvd.IsClosed)
                    {
                        drRcvd.Close();
                        drRcvd.Dispose();
                    }

                    this.TotalAmountReceived = this.GetTotalAmountReceived();

                    //Since Write is the amount that customer doesn't want to pay anymore against the order 
                    //So terhefore WriteOff amount should be deduct from effective total.
                    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                    //Set Returned Amount based on Invoice
                    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_inv WHERE ReturnInvoiceID = @InvoiceID";
                    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM z_invoice_return_process ip WHERE ip.ReturnInvoiceID=@InvoiceID) AS t1";
                    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }));
                }
                else
                {
                    //Get list of advance paymemts made against an order                
                    var lstPayments = new PreAccountRcv().GetAllByOrderID(dbHelp, orderID);

                    foreach (var item in lstPayments)
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = item.AmountDeposit;
                        ri.ReceivedVia = item.PaymentMethod;
                        ri.ReceivingDateTime = item.DateReceived;
                        ri.WriteOff = item.WriteOffAmount;
                        ri.ReceiptNo = item.ReceiptNo;
                        this.AmountReceivedList.Add(ri);
                    }

                    this.TotalAmountReceived = this.GetTotalAmountReceived();
                    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                    //Set Returned Amount based on Invoice
                    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_ord WHERE ReturnOrderID=@OrderID";
                    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(op.ReturnAmount) AS ReturnAmount FROM z_order_return_process op WHERE op.ReturnOrderID=@OrderID) AS t1";
                    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) }));
                }
            }
            catch
            {
                throw;
            }
        }

        public void InitOrderTotal(int orderID)
        {
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                this.ItemTaxList = new List<TaxItem>();
                this.ProcessTaxList = new List<TaxItem>();
                this.AmountReceivedList = new List<AmountReceivedItem>();

                Orders objOrder = new Orders();
                objOrder.PopulateObject(dbHelp, orderID);

                if (objOrder.OrdID <= 0)
                {
                    return;
                }

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(objOrder.OrdCompanyID, dbHelp);

                //string sqlOrderItems = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,sysTaxCodeDescText, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
                //sqlOrderItems += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
                //sqlOrderItems += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp where i.ordID=@ordID";
                string sqlOrderItems = "SELECT * FROM vw_order_item_amount WHERE OrderID=@OrderID";

                DataTable dt = dbHelp.GetDataTable(sqlOrderItems, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                Product prdAdminFee = new Product();
                int taxGrp;
                prdAdminFee.PopulateAdminFeeProduct(dbHelp);
                Tax tx;
                foreach (DataRow dr in dt.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ItemTaxList);
                    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + BusinessUtility.GetDouble(dr["Amount"]));

                    //Add to admin fee total
                    if (BusinessUtility.GetInt(dr["ProductID"]) == prdAdminFee.ProductID)
                    {
                        this.TotalAdminFeeApplied = CalculationHelper.GetAmount(this.TotalAdminFeeApplied + BusinessUtility.GetDouble(dr["Amount"]));
                    }
                }

                //string sqlProcessCost = "SELECT (ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits))*o.ordCurrencyExRate as ProcessCost, sysTaxCodeDescID FROM orderitemprocess i inner join orders o on o.ordID=i.ordID where i.ordID=@ordID";
                string sqlProcessCost = "SELECT * FROM vw_order_process_item_amount WHERE OrderID=@OrderID";
                DataTable dtProcess = dbHelp.GetDataTable(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                this.ProcessSubTotal = 0.0D;

                foreach (DataRow dr in dtProcess.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ProcessTaxList);
                    this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + BusinessUtility.GetDouble(dr["Amount"]));
                }


                //object val = dbHelp.GetValue(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
                //this.ProcessSubTotal = BusinessUtility.GetDouble(val);
                ////Process Tax Calculation
                //int processTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, objOrder.OrdShpWhsCode);

                //this.FillProcessTaxList(dbHelp, processTaxGrp, this.ProcessSubTotal);


                //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax; //Level one only for Items
                this.GrandTotal = this.ItemSubTotal; //Do not apply discount on taxes just exculde tax & process & their tax

                //Apply Additional Discount on products only not on services
                if (objOrder.OrdDiscountType.ToUpper().Trim() == "A")
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount(objOrder.OrdDiscount);
                }
                else
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)objOrder.OrdDiscount / 100.00D)));
                }

                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax); //Level to add process total                

                this.CurrencyCode = objOrder.OrdCurrencyCode;
                this.BaseCurrencyCode = ci.CompanyBasCur;
                this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(objOrder.OrdCurrencyExRate, this.GrandTotal));

                string sqlGetInvoiceID = "SELECT invID FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
                object obInvID = dbHelp.GetValue(sqlGetInvoiceID, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("invForOrderNo", orderID, MyDbType.Int),
                            DbUtility.GetParameter("invRefType", InvoiceReferenceType.INVOICE, MyDbType.String)
                        });
                int invID = BusinessUtility.GetInt(obInvID);
                if (invID > 0)
                {
                    //Get ReceivedAmount
                    string sqlGetRcvd = "SELECT ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARWriteOff, ARReceiptNo FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo";
                    MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    });

                    while (drRcvd.Read())
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                        this.AmountReceivedList.Add(ri);
                    }
                    if (drRcvd != null && !drRcvd.IsClosed)
                    {
                        drRcvd.Close();
                        drRcvd.Dispose();
                    }

                    this.TotalAmountReceived = this.GetTotalAmountReceived();

                    //Since Write is the amount that customer doesn't want to pay anymore against the order 
                    //So terhefore WriteOff amount should be deduct from effective total.
                    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                    //Set Returned Amount based on Invoice
                    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_inv WHERE ReturnInvoiceID = @InvoiceID";
                    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM z_invoice_return_process ip WHERE ip.ReturnInvoiceID=@InvoiceID) AS t1";
                    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }));
                }
                else
                {
                    //Get list of advance paymemts made against an order                
                    var lstPayments = new PreAccountRcv().GetAllByOrderID(dbHelp, orderID);

                    foreach (var item in lstPayments)
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = item.AmountDeposit;
                        ri.ReceivedVia = item.PaymentMethod;
                        ri.ReceivingDateTime = item.DateReceived;
                        ri.WriteOff = item.WriteOffAmount;
                        ri.ReceiptNo = item.ReceiptNo;
                        this.AmountReceivedList.Add(ri);
                    }

                    this.TotalAmountReceived = this.GetTotalAmountReceived();
                    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                    //Set Returned Amount based on Invoice
                    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_ord WHERE ReturnOrderID=@OrderID";
                    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(op.ReturnAmount) AS ReturnAmount FROM z_order_return_process op WHERE op.ReturnOrderID=@OrderID) AS t1";
                    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) }));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void InitInvoiceTotal(DbHelper dbHelp, int invID)
        {
            try
            {
                this.ItemTaxList = new List<TaxItem>();
                this.ProcessTaxList = new List<TaxItem>();
                this.AmountReceivedList = new List<AmountReceivedItem>();

                Invoice obbjInv = new Invoice();
                obbjInv.PopulateObject(dbHelp, invID);

                if (obbjInv.InvID <= 0)
                {
                    return;
                }

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(obbjInv.InvCompanyID, dbHelp);

                //string sqlOrderItems = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, ";
                //sqlOrderItems += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, ";
                //sqlOrderItems += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp where i.invoices_invID=@invoices_invID";
                string sqlOrderItems = "SELECT * FROM vw_invoice_item_amount WHERE InvoiceID=@InvoiceID";

                DataTable dt = dbHelp.GetDataTable(sqlOrderItems, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) });
                Product prdAdminFee = new Product();
                int taxGrp;
                prdAdminFee.PopulateAdminFeeProduct(dbHelp);
                Tax tx;
                foreach (DataRow dr in dt.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ItemTaxList);
                    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + BusinessUtility.GetDouble(dr["Amount"]));

                    //Add to admin fee total
                    if (BusinessUtility.GetInt(dr["ProductID"]) == prdAdminFee.ProductID)
                    {
                        this.TotalAdminFeeApplied = CalculationHelper.GetAmount(this.TotalAdminFeeApplied + BusinessUtility.GetDouble(dr["Amount"]));
                    }
                }

                //string sqlProcessCost = "SELECT (invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost, sysTaxCodeDescID FROM invitemprocess inner join invoices on invID=invoices_invID where invoices_invID=@invoices_invID ";
                string sqlProcessCost = "SELECT * FROM vw_invoice_process_item_amount WHERE InvoiceID=@InvoiceID";
                DataTable dtProcess = dbHelp.GetDataTable(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) });
                this.ProcessSubTotal = 0.0D;

                foreach (DataRow dr in dtProcess.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ProcessTaxList);
                    this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + BusinessUtility.GetDouble(dr["Amount"]));
                }


                //object val = dbHelp.GetValue(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int) });
                //this.ProcessSubTotal = BusinessUtility.GetDouble(val);                
                ////Process Tax Calculation
                //int processTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, obbjInv.InvShpWhsCode);                
                //this.FillProcessTaxList(dbHelp, processTaxGrp, this.ProcessSubTotal);

                //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax;
                this.GrandTotal = this.ItemSubTotal;

                //Apply Additional Discount
                if (obbjInv.InvDiscountType.ToUpper().Trim() == "A")
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount(obbjInv.InvDiscount);
                }
                else
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)obbjInv.InvDiscount / 100.00D)));
                }

                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax);

                this.CurrencyCode = obbjInv.InvCurrencyCode;
                this.BaseCurrencyCode = ci.CompanyBasCur;
                this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(obbjInv.InvCurrencyExRate, this.GrandTotal));

                //Get ReceivedAmount
                string sqlGetRcvd = "SELECT ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARWriteOff,ARReceiptNo FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo and ARAmtRcvd>0 ";
                using (MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    }))
                {
                    while (drRcvd.Read())
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                        this.AmountReceivedList.Add(ri);
                    }
                }



                //Get ReceivedAmount
                sqlGetRcvd = "SELECT  SUM(ARAmtRcvd) FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo and ARAmtRcvd<0  AND ARWriteOff =0";
                this.ReturnedOverPaidAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    }));


                sqlGetRcvd = "SELECT  SUM(ARAmtRcvd) FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo and ARAmtRcvd<0  AND ARWriteOff >0";
                this.ReturnedItemReturnAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    }));

                //using (MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                //        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                //    }))
                //{
                //    while (drRcvd.Read())
                //    {
                //        AmountReceivedItem ri = new AmountReceivedItem();
                //        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                //        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                //        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                //        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                //        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                //        this.AmountReceivedList.Add(ri);
                //    }
                //}






                this.TotalAmountReceived = this.GetTotalAmountReceived();

                //Since Write is the amount that customer doesn't want to pay anymore against the order 
                //So terhefore WriteOff amount should be deduct from effective total.
                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                //Set Returned Amount based on Invoice
                string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_inv WHERE ReturnInvoiceID = @InvoiceID";
                sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM z_invoice_return_process ip WHERE ip.ReturnInvoiceID=@InvoiceID) AS t1";
                this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }));
            }
            catch
            {
                throw;
            }
        }

        public void InitInvoiceTotal(int invID)
        {
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                this.ItemTaxList = new List<TaxItem>();
                this.ProcessTaxList = new List<TaxItem>();
                this.AmountReceivedList = new List<AmountReceivedItem>();

                Invoice obbjInv = new Invoice();
                obbjInv.PopulateObject(dbHelp, invID);

                if (obbjInv.InvID <= 0)
                {
                    return;
                }

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(obbjInv.InvCompanyID, dbHelp);

                //string sqlOrderItems = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, ";
                //sqlOrderItems += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, ";
                //sqlOrderItems += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp where i.invoices_invID=@invoices_invID";
                string sqlOrderItems = "SELECT * FROM vw_invoice_item_amount WHERE InvoiceID=@InvoiceID";

                DataTable dt = dbHelp.GetDataTable(sqlOrderItems, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) });
                Product prdAdminFee = new Product();
                int taxGrp;
                prdAdminFee.PopulateAdminFeeProduct(dbHelp);
                Tax tx;
                foreach (DataRow dr in dt.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ItemTaxList);
                    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + BusinessUtility.GetDouble(dr["Amount"]));

                    //Add to admin fee total
                    if (BusinessUtility.GetInt(dr["ProductID"]) == prdAdminFee.ProductID)
                    {
                        this.TotalAdminFeeApplied = CalculationHelper.GetAmount(this.TotalAdminFeeApplied + BusinessUtility.GetDouble(dr["Amount"]));
                    }
                }

                //string sqlProcessCost = "SELECT (invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost, sysTaxCodeDescID FROM invitemprocess inner join invoices on invID=invoices_invID where invoices_invID=@invoices_invID ";
                string sqlProcessCost = "SELECT * FROM vw_invoice_process_item_amount WHERE InvoiceID=@InvoiceID";
                DataTable dtProcess = dbHelp.GetDataTable(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) });
                this.ProcessSubTotal = 0.0D;

                foreach (DataRow dr in dtProcess.Rows)
                {
                    tx = new Tax();
                    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ProcessTaxList);
                    this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + BusinessUtility.GetDouble(dr["Amount"]));
                }


                //object val = dbHelp.GetValue(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int) });
                //this.ProcessSubTotal = BusinessUtility.GetDouble(val);                
                ////Process Tax Calculation
                //int processTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, obbjInv.InvShpWhsCode);                
                //this.FillProcessTaxList(dbHelp, processTaxGrp, this.ProcessSubTotal);

                //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax;
                this.GrandTotal = this.ItemSubTotal;

                //Apply Additional Discount
                if (obbjInv.InvDiscountType.ToUpper().Trim() == "A")
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount(obbjInv.InvDiscount);
                }
                else
                {
                    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)obbjInv.InvDiscount / 100.00D)));
                }

                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax);

                this.CurrencyCode = obbjInv.InvCurrencyCode;
                this.BaseCurrencyCode = ci.CompanyBasCur;
                this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(obbjInv.InvCurrencyExRate, this.GrandTotal));

                //Get ReceivedAmount
                string sqlGetRcvd = "SELECT ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARWriteOff,ARReceiptNo FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo";
                using (MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                    }))
                {
                    while (drRcvd.Read())
                    {
                        AmountReceivedItem ri = new AmountReceivedItem();
                        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                        this.AmountReceivedList.Add(ri);
                    }
                }

                this.TotalAmountReceived = this.GetTotalAmountReceived();

                //Since Write is the amount that customer doesn't want to pay anymore against the order 
                //So terhefore WriteOff amount should be deduct from effective total.
                this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                //Set Returned Amount based on Invoice
                string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_inv WHERE ReturnInvoiceID = @InvoiceID";
                sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM z_invoice_return_process ip WHERE ip.ReturnInvoiceID=@InvoiceID) AS t1";
                this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }));
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void InitOrderTotal(DbHelper dbHelp, SalesOrderCart cart)
        {
            int orderID = cart.OrderID;

            try
            {
                if (orderID > 0)
                {
                    this.InitOrderTotal(dbHelp, orderID);
                }
                else
                {
                    this.ItemTaxList = new List<TaxItem>();
                    this.ProcessTaxList = new List<TaxItem>();
                    this.AmountReceivedList = new List<AmountReceivedItem>();

                    SysCompanyInfo ci = new SysCompanyInfo();
                    ci.PopulateObject(cart.OrderCompanyID, dbHelp);

                    Partners part = new Partners();
                    part.PopulateObject(dbHelp, cart.OrderCustomerID);

                    if (cart.Items.Count > 0)
                    {
                        foreach (var dr in cart.Items)
                        {
                            //int taxGrp = dr.TaxGrp;
                            this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + dr.ItemTotal);
                            TaxCalculationHelper.FillItemTaxList(dbHelp, dr.ItemTotal, dr.ItemTaxes, this.ItemTaxList);
                        }
                    }
                    if (cart.ProcessItems.Count > 0)
                    {
                        foreach (var dr in cart.ProcessItems)
                        {
                            //int taxGrp = dr.TaxGrpID;
                            this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + dr.ProcessTotal);
                            TaxCalculationHelper.FillItemTaxList(dbHelp, dr.ProcessTotal, dr.ItemTaxes, this.ProcessTaxList);
                        }
                    }

                    //int whsTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, cart.OrderWarehouseCode);
                    //this.FillProcessTaxList(dbHelp, whsTaxGrp, this.ProcessSubTotal);

                    //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax;
                    this.GrandTotal = this.ItemSubTotal;

                    //Apply Additional Discount                
                    if (cart.OrdDiscountType == "A")
                    {
                        this.AdditionalDiscount = CalculationHelper.GetAmount(cart.OrdDiscount);
                    }
                    else
                    {
                        this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)cart.OrdDiscount / 100.00D)));
                    }

                    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax);
                    this.OutstandingAmount = this.GrandTotal;
                    this.CurrencyCode = part.PartnerCurrencyCode;
                    this.BaseCurrencyCode = ci.CompanyBasCur;
                    this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(cart.ExchangeRate, this.GrandTotal));
                }
            }
            catch
            {

                throw;
            }
        }

        public void InitPoOrderTotal(DbHelper dbHelp, int poOrderID)
        {
            try
            {
                PurchaseOrderItems pi = new PurchaseOrderItems();
                DataTable dt = pi.GetPurchaseOrderItems(dbHelp, poOrderID);
                foreach (DataRow item in dt.Rows)
                {
                    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + (BusinessUtility.GetDouble(item["poUnitPrice"]) * BusinessUtility.GetDouble(item["poQty"])));
                }

                #region Comment

                this.ItemTaxList = new List<TaxItem>();
                this.ProcessTaxList = new List<TaxItem>();
                this.AmountReceivedList = new List<AmountReceivedItem>();

                PurchaseOrders po = new PurchaseOrders();
                po.PopulateObject(dbHelp, poOrderID);
                this.CurrencyCode = po.PoCurrencyCode;
                this.ExchangeRate = po.PoCurrencyExRate;
                //this.BaseCurrencyCode = po.Po;

                //this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(objOrder.OrdCurrencyExRate, this.GrandTotal));


                //Orders objOrder = new Orders();
                //objOrder.PopulateObject(dbHelp, orderID);

                //if (objOrder.OrdID <= 0)
                //{
                //    return;
                //}

                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(objOrder.OrdCompanyID, dbHelp);

                ////string sqlOrderItems = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,sysTaxCodeDescText, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, ";
                ////sqlOrderItems += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, ";
                ////sqlOrderItems += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp where i.ordID=@ordID";
                //string sqlOrderItems = "SELECT * FROM vw_order_item_amount WHERE OrderID=@OrderID";

                //DataTable dt = dbHelp.GetDataTable(sqlOrderItems, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                //Product prdAdminFee = new Product();
                //int taxGrp;
                //prdAdminFee.PopulateAdminFeeProduct(dbHelp);
                //Tax tx;
                //foreach (DataRow dr in dt.Rows)
                //{
                //    tx = new Tax();
                //    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                //    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                //    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                //    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                //    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                //    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                //    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                //    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                //    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                //    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                //    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                //    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                //    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                //    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                //    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                //    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                //    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ItemTaxList);
                //    this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + BusinessUtility.GetDouble(dr["Amount"]));

                //    //Add to admin fee total
                //    if (BusinessUtility.GetInt(dr["ProductID"]) == prdAdminFee.ProductID)
                //    {
                //        this.TotalAdminFeeApplied = CalculationHelper.GetAmount(this.TotalAdminFeeApplied + BusinessUtility.GetDouble(dr["Amount"]));
                //    }
                //}

                ////string sqlProcessCost = "SELECT (ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits))*o.ordCurrencyExRate as ProcessCost, sysTaxCodeDescID FROM orderitemprocess i inner join orders o on o.ordID=i.ordID where i.ordID=@ordID";
                //string sqlProcessCost = "SELECT * FROM vw_order_process_item_amount WHERE OrderID=@OrderID";
                //DataTable dtProcess = dbHelp.GetDataTable(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) });
                //this.ProcessSubTotal = 0.0D;

                //foreach (DataRow dr in dtProcess.Rows)
                //{
                //    tx = new Tax();
                //    taxGrp = BusinessUtility.GetInt(dr["TaxGroupID"]);
                //    tx.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                //    tx.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                //    tx.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                //    tx.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                //    tx.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                //    tx.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                //    tx.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                //    tx.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                //    tx.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                //    tx.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                //    tx.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                //    tx.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                //    tx.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                //    tx.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                //    tx.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                //    TaxCalculationHelper.FillItemTaxList(dbHelp, BusinessUtility.GetDouble(dr["Amount"]), tx, this.ProcessTaxList);
                //    this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + BusinessUtility.GetDouble(dr["Amount"]));
                //}


                ////object val = dbHelp.GetValue(sqlProcessCost, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
                ////this.ProcessSubTotal = BusinessUtility.GetDouble(val);
                //////Process Tax Calculation
                ////int processTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, objOrder.OrdShpWhsCode);

                ////this.FillProcessTaxList(dbHelp, processTaxGrp, this.ProcessSubTotal);


                ////this.GrandTotal = this.ItemSubTotal + this.TotalItemTax; //Level one only for Items
                //this.GrandTotal = this.ItemSubTotal; //Do not apply discount on taxes just exculde tax & process & their tax

                ////Apply Additional Discount on products only not on services
                //if (objOrder.OrdDiscountType.ToUpper().Trim() == "A")
                //{
                //    this.AdditionalDiscount = CalculationHelper.GetAmount(objOrder.OrdDiscount);
                //}
                //else
                //{
                //    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)objOrder.OrdDiscount / 100.00D)));
                //}

                //this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax); //Level to add process total                

                //this.CurrencyCode = objOrder.OrdCurrencyCode;
                //this.BaseCurrencyCode = ci.CompanyBasCur;
                //this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(objOrder.OrdCurrencyExRate, this.GrandTotal));

                //string sqlGetInvoiceID = "SELECT invID FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
                //object obInvID = dbHelp.GetValue(sqlGetInvoiceID, CommandType.Text, new MySqlParameter[] { 
                //            DbUtility.GetParameter("invForOrderNo", orderID, MyDbType.Int),
                //            DbUtility.GetParameter("invRefType", InvoiceReferenceType.INVOICE, MyDbType.String)
                //        });
                //int invID = BusinessUtility.GetInt(obInvID);
                //if (invID > 0)
                //{
                //    //Get ReceivedAmount
                //    string sqlGetRcvd = "SELECT ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARWriteOff, ARReceiptNo FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo";
                //    MySqlDataReader drRcvd = dbHelp.GetDataReader(sqlGetRcvd, CommandType.Text, new MySqlParameter[] { 
                //        DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                //    });

                //    while (drRcvd.Read())
                //    {
                //        AmountReceivedItem ri = new AmountReceivedItem();
                //        ri.Amount = BusinessUtility.GetDouble(drRcvd["ARAmtRcvd"]);
                //        ri.ReceivedVia = BusinessUtility.GetInt(drRcvd["ARAmtRcvdVia"]);
                //        ri.ReceivingDateTime = BusinessUtility.GetDateTime(drRcvd["ARAmtRcvdDateTime"]);
                //        ri.WriteOff = BusinessUtility.GetDouble(drRcvd["ARWriteOff"]);
                //        ri.ReceiptNo = BusinessUtility.GetString(drRcvd["ARReceiptNo"]);
                //        this.AmountReceivedList.Add(ri);
                //    }
                //    if (drRcvd != null && !drRcvd.IsClosed)
                //    {
                //        drRcvd.Close();
                //        drRcvd.Dispose();
                //    }

                //    this.TotalAmountReceived = this.GetTotalAmountReceived();

                //    //Since Write is the amount that customer doesn't want to pay anymore against the order 
                //    //So terhefore WriteOff amount should be deduct from effective total.
                //    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                //    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                //    //Set Returned Amount based on Invoice
                //    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_inv WHERE ReturnInvoiceID = @InvoiceID";
                //    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM z_invoice_return_process ip WHERE ip.ReturnInvoiceID=@InvoiceID) AS t1";
                //    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }));
                //}
                //else
                //{
                //    //Get list of advance paymemts made against an order                
                //    var lstPayments = new PreAccountRcv().GetAllByOrderID(dbHelp, orderID);

                //    foreach (var item in lstPayments)
                //    {
                //        AmountReceivedItem ri = new AmountReceivedItem();
                //        ri.Amount = item.AmountDeposit;
                //        ri.ReceivedVia = item.PaymentMethod;
                //        ri.ReceivingDateTime = item.DateReceived;
                //        ri.WriteOff = item.WriteOffAmount;
                //        ri.ReceiptNo = item.ReceiptNo;
                //        this.AmountReceivedList.Add(ri);
                //    }

                //    this.TotalAmountReceived = this.GetTotalAmountReceived();
                //    this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.GetTotalWriteOff());
                //    this.OutstandingAmount = CalculationHelper.GetAmount(this.GrandTotal - this.TotalAmountReceived);

                //    //Set Returned Amount based on Invoice
                //    string sqlReturnedAmount = "SELECT SUM(t1.ReturnAmount) FROM (SELECT 1 AS Uk, getAmount(ReturnAmount) AS ReturnAmount FROM vw_return_history_ord WHERE ReturnOrderID=@OrderID";
                //    sqlReturnedAmount += " UNION ALL SELECT 2 AS Uk, getAmount(op.ReturnAmount) AS ReturnAmount FROM z_order_return_process op WHERE op.ReturnOrderID=@OrderID) AS t1";
                //    this.TotalReturnedAmount = BusinessUtility.GetDouble(dbHelp.GetValue(sqlReturnedAmount, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) }));
                //}
                #endregion
            }
            catch
            {
                throw;
            }
        }

        public void InitPoOrderTotal(DbHelper dbHelp, PurchaseOrderCart cart)
        {
            int orderID = cart.PurchaseOrderID;// cart.OrderID;

            try
            {
                if (orderID > 0)
                {
                    this.InitPoOrderTotal(dbHelp, orderID);
                }
                else
                {
                    this.ItemTaxList = new List<TaxItem>();
                    this.ProcessTaxList = new List<TaxItem>();
                    this.AmountReceivedList = new List<AmountReceivedItem>();

                    //SysCompanyInfo ci = new SysCompanyInfo();
                    //ci.PopulateObject(cart.OrderCompanyID, dbHelp);

                    //Partners part = new Partners();
                    //part.PopulateObject(dbHelp, cart.OrderCustomerID);

                    if (cart.Items.Count > 0)
                    {
                        foreach (var dr in cart.Items)
                        {
                            //int taxGrp = dr.TaxGrp;
                            this.ItemSubTotal = CalculationHelper.GetAmount(this.ItemSubTotal + (BusinessUtility.GetDouble(dr.Quantity) * BusinessUtility.GetDouble(dr.Price)));
                        }
                    }
                    //if (cart.ProcessItems.Count > 0)
                    //{
                    //    foreach (var dr in cart.ProcessItems)
                    //    {
                    //        //int taxGrp = dr.TaxGrpID;
                    //        this.ProcessSubTotal = CalculationHelper.GetAmount(this.ProcessSubTotal + dr.ProcessTotal);
                    //        TaxCalculationHelper.FillItemTaxList(dbHelp, dr.ProcessTotal, dr.ItemTaxes, this.ProcessTaxList);
                    //    }
                    //}

                    //int whsTaxGrp = this.GetTaxGroup(dbHelp, -1, false, null, -1, cart.OrderWarehouseCode);
                    //this.FillProcessTaxList(dbHelp, whsTaxGrp, this.ProcessSubTotal);

                    //this.GrandTotal = this.ItemSubTotal + this.TotalItemTax;
                    this.GrandTotal = this.ItemSubTotal;

                    ////Apply Additional Discount                
                    //if (cart.OrdDiscountType == "A")
                    //{
                    //    this.AdditionalDiscount = CalculationHelper.GetAmount(cart.OrdDiscount);
                    //}
                    //else
                    //{
                    //    this.AdditionalDiscount = CalculationHelper.GetAmount((this.GrandTotal * ((double)cart.OrdDiscount / 100.00D)));
                    //}
                    //this.GrandTotal = CalculationHelper.GetAmount(this.GrandTotal - this.AdditionalDiscount + this.TotalItemTax + this.ProcessSubTotal + this.TotalProcessTax);
                    //this.OutstandingAmount = this.GrandTotal;
                    //this.CurrencyCode = part.PartnerCurrencyCode;
                    //this.BaseCurrencyCode = ci.CompanyBasCur;
                    //this.GrandTotalBase = CalculationHelper.GetAmount(this.GetBasePrice(cart.ExchangeRate, this.GrandTotal));
                }
            }
            catch
            {

                throw;
            }
        }

        public List<TaxItem> GetCombiendTaxList()
        {
            var lstTaxes = new List<TaxItem>();
            foreach (var item in this.ItemTaxList)
            {
                var v = lstTaxes.Where(i => i.TaxCode == item.TaxCode).FirstOrDefault();
                if (v != null)
                {
                    v.CalculatedPrice = CalculationHelper.GetAmount(v.CalculatedPrice + item.CalculatedPrice);
                }
                else
                {
                    lstTaxes.Add(new TaxItem { TaxCode = item.TaxCode, CalculatedPrice = item.CalculatedPrice });
                }
            }

            foreach (var item in this.ProcessTaxList)
            {
                var v = lstTaxes.Where(i => i.TaxCode == item.TaxCode).FirstOrDefault();
                if (v != null)
                {
                    v.CalculatedPrice = CalculationHelper.GetAmount(v.CalculatedPrice + item.CalculatedPrice);
                }
                else
                {
                    lstTaxes.Add(new TaxItem { TaxCode = item.TaxCode, CalculatedPrice = item.CalculatedPrice });
                }
            }

            return lstTaxes;
        }
    }

    public class TaxItem
    {
        public string TaxCode { get; set; }
        public double CalculatedPrice { get; set; }
    }

    public class AmountReceivedItem
    {
        public DateTime ReceivingDateTime { get; set; }
        public int ReceivedVia { get; set; }
        public double Amount { get; set; }
        public double WriteOff { get; set; }
        public string ReceiptNo { get; set; }
    }

    public class TaxCalculationHelper
    {
        public static int GetTaxGroup(DbHelper dbHelp, int productid, int partnerID, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            int taxGroup = 0;
            try
            {
                taxGroup = BusinessUtility.GetInt(dbHelp.GetValue("SELECT getTaxGroupID(NULL, @ProductID, @PartnerID, @WhsCode)", System.Data.CommandType.Text,
                            new MySqlParameter[] { 
                              DbUtility.GetParameter("ProductID", productid, MyDbType.Int),
                              DbUtility.GetParameter("WhsCode", whsCode, MyDbType.String),
                              DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                        }));

                /*if (productid > 0 && !string.IsNullOrEmpty(whsCode))
                {
                    taxGroup = BusinessUtility.GetInt(dbHelp.GetValue("SELECT getTaxGroupID(NULL, @ProductID, NULL, @WhsCode)", System.Data.CommandType.Text,
                            new MySqlParameter[] { 
                              DbUtility.GetParameter("ProductID", productid, MyDbType.Int),
                              DbUtility.GetParameter("WhsCode", whsCode, MyDbType.String)
                        }));
                }
                if (taxGroup <= 0 && prdIsPOS && !string.IsNullOrEmpty(posRegCode))
                {
                    taxGroup = BusinessUtility.GetInt(dbHelp.GetValue("SELECT funGetTaxGroupByRegister(@RegCode)", System.Data.CommandType.Text,
                                  new MySqlParameter[] {                                           
                                          DbUtility.GetParameter("RegCode", posRegCode, MyDbType.String)
                                }));
                }
                if (taxGroup <= 0 && partnerID > 0)
                {
                    taxGroup = BusinessUtility.GetInt(dbHelp.GetValue("SELECT getTaxGroupID(NULL, NULL, @PartnerID, NULL)", System.Data.CommandType.Text,
                                  new MySqlParameter[] { 
                                          DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                                }));
                }
                if (taxGroup <= 0 && !string.IsNullOrEmpty(whsCode))
                {
                    taxGroup = BusinessUtility.GetInt(dbHelp.GetValue("SELECT getTaxGroupID(NULL, NULL, NULL, @WhsCode)", System.Data.CommandType.Text,
                               new MySqlParameter[] { 
                                          DbUtility.GetParameter("WhsCode", whsCode, MyDbType.String)
                                }));
                }*/

                return taxGroup;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static void FillItemTaxList(DbHelper dbHelp, double amount, Tax tx, List<TaxItem> targetListToFill)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                //double txAmt = 0.00D;
                //string sqlGetTax = "SELECT getTaxCalculated(@Amount, @Tax1, @TaxCalculated1,@Tax2, @TaxCalculated2,@Tax3, @TaxCalculated3,@Tax4, @TaxCalculated4,@Tax5, @TaxCalculated5)";
                //Tax1
                if (!string.IsNullOrEmpty(tx.TaxDesc1))
                {

                    TaxItem ti = targetListToFill.Where(t => t.TaxCode == tx.TaxDesc1).FirstOrDefault();
                    if (ti == null)
                    {
                        ti = new TaxItem { TaxCode = tx.TaxDesc1, CalculatedPrice = 0.0D };
                        targetListToFill.Add(ti);
                    }
                    ti.CalculatedPrice = CalculationHelper.GetAmount(ti.CalculatedPrice + tx.TaxCalculated1);
                }
                //Tax2
                if (!string.IsNullOrEmpty(tx.TaxDesc2))
                {

                    TaxItem ti = targetListToFill.Where(t => t.TaxCode == tx.TaxDesc2).FirstOrDefault();
                    if (ti == null)
                    {
                        ti = new TaxItem { TaxCode = tx.TaxDesc2, CalculatedPrice = 0.0D };
                        targetListToFill.Add(ti);
                    }
                    ti.CalculatedPrice = CalculationHelper.GetAmount(ti.CalculatedPrice + tx.TaxCalculated2);
                }
                //Tax3
                if (!string.IsNullOrEmpty(tx.TaxDesc3))
                {

                    TaxItem ti = targetListToFill.Where(t => t.TaxCode == tx.TaxDesc3).FirstOrDefault();
                    if (ti == null)
                    {
                        ti = new TaxItem { TaxCode = tx.TaxDesc3, CalculatedPrice = 0.0D };
                        targetListToFill.Add(ti);
                    }
                    ti.CalculatedPrice = CalculationHelper.GetAmount(ti.CalculatedPrice + tx.TaxCalculated3);
                }
                //Tax4
                if (!string.IsNullOrEmpty(tx.TaxDesc4))
                {

                    TaxItem ti = targetListToFill.Where(t => t.TaxCode == tx.TaxDesc4).FirstOrDefault();
                    if (ti == null)
                    {
                        ti = new TaxItem { TaxCode = tx.TaxDesc4, CalculatedPrice = 0.0D };
                        targetListToFill.Add(ti);
                    }
                    ti.CalculatedPrice = CalculationHelper.GetAmount(ti.CalculatedPrice + tx.TaxCalculated4);
                }
                //Tax5
                if (!string.IsNullOrEmpty(tx.TaxDesc5))
                {
                    TaxItem ti = targetListToFill.Where(t => t.TaxCode == tx.TaxDesc5).FirstOrDefault();
                    if (ti == null)
                    {
                        ti = new TaxItem { TaxCode = tx.TaxDesc5, CalculatedPrice = 0.0D };
                        targetListToFill.Add(ti);
                    }
                    ti.CalculatedPrice = CalculationHelper.GetAmount(ti.CalculatedPrice + tx.TaxCalculated5);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Only For LPB Instance
        public static void FillItemTaxList(DbHelper dbHelp, int taxGrp, double price, List<TaxItem> targetListToFill)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            if (targetListToFill == null)
            {
                targetListToFill = new List<TaxItem>();
            }
            if (taxGrp <= 0) return;
            try
            {
                Tax tx = new Tax();
                tx.PopulateObject(dbHelp, taxGrp, price);
                FillItemTaxList(dbHelp, price, tx, targetListToFill);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        //public static double GetCalculatedPrice(DbHelper dbHelp, int taxGrp, double price)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = false;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        List<TaxItem> targetList = new List<TaxItem>();
        //        FillItemTaxList(dbHelp, taxGrp, price, targetList);
        //        double lPrice = price;
        //        foreach (var item in targetList)
        //        {
        //            lPrice += item.CalculatedPrice;
        //        }
        //        return CalculationHelper.GetAmount(lPrice);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}
    }
}
