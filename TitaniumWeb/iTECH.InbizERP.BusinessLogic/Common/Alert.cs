﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Alert
    {
        public int AlertID { get; set; }
        public int AlertPartnerContactID { get; set; }
        public int AlertComID { get; set; }
        public DateTime AlertDateTime { get; set; }
        public int AlertUserID { get; set; }
        public string AlertNote { get; set; }
        public int AlertViewCount { get; set; }
        public DateTime AlertLastViewedOn { get; set; }
        public string AlertRefType { get; set; }

        public bool Insert(DbHelper dbHelp) {
            string sql = "INSERT INTO alerts (AlertPartnerContactID,AlertComID,AlertDateTime,AlertUserID,AlertNote,AlertViewCount,AlertLastViewedOn,AlertRefType)";
            sql += " VALUES(@AlertPartnerContactID,@AlertComID,@AlertDateTime,@AlertUserID,@AlertNote,IF(@AlertViewCount > 0, @AlertViewCount, NULL),@AlertLastViewedOn,@AlertRefType)";            
            try
            {                 
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("AlertPartnerContactID", this.AlertPartnerContactID, MyDbType.Int),
                    DbUtility.GetParameter("AlertComID", this.AlertComID, MyDbType.Int),
                    DbUtility.GetParameter("AlertDateTime", this.AlertDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("AlertUserID", this.AlertUserID, MyDbType.Int),
                    DbUtility.GetParameter("AlertNote", this.AlertNote, MyDbType.String),
                    DbUtility.GetParameter("AlertViewCount", this.AlertViewCount, MyDbType.Int),
                    DbUtility.GetParameter("AlertLastViewedOn", this.AlertLastViewedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("AlertRefType", this.AlertRefType, MyDbType.String)
                });
                this.AlertID = dbHelp.GetLastInsertID();
                return true;
            }
            catch
            {
                throw;
            }           
        }
    }
}
