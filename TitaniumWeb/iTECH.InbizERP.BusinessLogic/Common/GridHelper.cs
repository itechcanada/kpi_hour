﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class GridHelper
    {
        static Dictionary<string, GridViewState> _grdViewStateCollection;

        private static string GetUniqueKey(string gridID)
        {
            return string.Format("{0}#{1}", HttpContext.Current.Request.Path, gridID).ToLower();
        }

        public static Dictionary<string, GridViewState> GrdViewStateCollection
        {
            get { return ((HttpContext.Current.Session["_grdViewStateKey"] != null) ? (Dictionary<string, GridViewState>)HttpContext.Current.Session["_grdViewStateKey"] : new Dictionary<string, GridViewState>()); }
            set { HttpContext.Current.Session["_grdViewStateKey"] = value; }
        }

        public static void SetGridViewStateToCallection(Control srchContainer, JQGrid sender, JQGridDataRequestEventArgs e)
        {
            _grdViewStateCollection = GrdViewStateCollection;
            GridViewState grdState = null;
            if (_grdViewStateCollection.ContainsKey(GetUniqueKey(sender.ID)))
            {
                grdState = _grdViewStateCollection[GetUniqueKey(sender.ID)];
            }
            else
            {
                grdState = new GridViewState();                
            }

            grdState.CurrentPage = e.NewPageIndex;
            grdState.SortDirection = e.SortDirection.ToString();
            grdState.SortExpression = e.SortExpression;
            grdState.PageSize = sender.PagerSettings.PageSize;

            foreach (string key in HttpContext.Current.Request.QueryString.AllKeys)
            {
                grdState.QueryStringKeyVal[key] = HttpContext.Current.Request.QueryString[key];
            }

            _grdViewStateCollection[GetUniqueKey(sender.ID)] = grdState;
            GrdViewStateCollection = _grdViewStateCollection;
        }

        public static void GetGridState(JQGrid grd, Control searchContainer)
        {
            string strTargetJgGridId = System.Web.HttpContext.Current.Request.QueryString["jqGridID"];
            if (string.IsNullOrEmpty(strTargetJgGridId))
            {
                if (!(System.Web.HttpContext.Current.Request.QueryString["default"] == "1"))
                {
                    _grdViewStateCollection = GrdViewStateCollection;
                    GridViewState grdState = null;
                    if (_grdViewStateCollection.ContainsKey(GetUniqueKey(grd.ID)))
                    {
                        grdState = _grdViewStateCollection[GetUniqueKey(grd.ID)];
                        if ((grdState != null))
                        {
                            foreach (string key in grdState.QueryStringKeyVal.Keys)
                            {
                                SetControlValues(searchContainer, key, grdState.QueryStringKeyVal[key]);
                            }

                            grd.SortSettings.InitialSortColumn = grdState.SortExpression;
                            if (Enum.IsDefined(typeof(Trirand.Web.UI.WebControls.SortDirection), grdState.SortDirection))
                            {
                                grd.SortSettings.InitialSortDirection = (Trirand.Web.UI.WebControls.SortDirection)Enum.Parse(typeof(Trirand.Web.UI.WebControls.SortDirection), grdState.SortDirection);
                            }
                            grd.PagerSettings.CurrentPage = grdState.CurrentPage;
                            grd.PagerSettings.PageSize = grdState.PageSize;
                        }
                    }
                }
                else
                {
                    GrdViewStateCollection = new Dictionary<string, GridViewState>();
                }
            }           
        }

        public static void GetGridState(JQGrid grd, JQGridDataRequestEventArgs e)
        {
            _grdViewStateCollection = GrdViewStateCollection;
            GridViewState grdState = null;
            if (_grdViewStateCollection.ContainsKey(GetUniqueKey(grd.ID)))
            {
                grdState = _grdViewStateCollection[GetUniqueKey(grd.ID)];
                if ((grdState != null))
                {
                    e.SortExpression = grdState.SortExpression;
                    if (Enum.IsDefined(typeof(Trirand.Web.UI.WebControls.SortDirection), grdState.SortDirection))
                    {
                        e.SortDirection = (Trirand.Web.UI.WebControls.SortDirection)Enum.Parse(typeof(Trirand.Web.UI.WebControls.SortDirection), grdState.SortDirection);
                    }
                    e.NewPageIndex = grdState.CurrentPage;
                    grd.PagerSettings.PageSize = grdState.PageSize;
                }
            }            
        }

        public static void SetControlValues(Control container, string name, object value)
        {
            if (container == null)
            {
                return;
            }
            Control ctl = FindControlByClientID(name, container);
            if ((ctl != null))
            {
                if (ctl is TextBox)
                {
                    ((TextBox)ctl).Text = (string)value;
                }
                if (ctl is DropDownList)
                {
                    ((DropDownList)ctl).SelectedValue = (string)value;
                }
                if (ctl is CheckBox)
                {
                    ((CheckBox)ctl).Checked = Convert.ToBoolean(value);
                }
                if (ctl is RadioButtonList)
                {
                    ((RadioButtonList)ctl).SelectedValue = (string)value;
                }
            }
        }

        public static Control FindControlByClientID(string clientID, Control container)
        {
            foreach (Control ctrl in container.Controls)
            {
                if (ctrl.Controls.Count > 0)
                {
                    FindControlByClientID(clientID, ctrl);
                }
                else
                {
                    if (ctrl.ClientID == clientID)
                    {
                        return ctrl;
                    }
                }
            }
            return null;
        }

        public static string GetSearchFilterParameterValue(string gridID, string clientID)
        {
            string retValue = string.Empty;
            _grdViewStateCollection = GrdViewStateCollection;
            GridViewState grdState = null;
            if (_grdViewStateCollection.ContainsKey(GetUniqueKey(gridID)))
            {
                grdState = _grdViewStateCollection[GetUniqueKey(gridID)];

                foreach (string key in grdState.QueryStringKeyVal.Keys)
                {
                    if (key == clientID)
                    {
                        retValue = grdState.QueryStringKeyVal[key];
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }

            return retValue;
        }
    }
}