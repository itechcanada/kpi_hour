﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Tax
    {
        public int TaxGroupID { get; set; }
        public double Amount { get; set; }
        public double Tax1 { get; set; }        
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }

        public void PopulateObject(DbHelper dbHelp, int taxGroupID, double amount)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                this.Amount = amount;
                SysTaxCode tc = new SysTaxCode();
                var listTax = tc.GetAllTaxes(dbHelp, taxGroupID);
                this.TaxGroupID = taxGroupID;

                foreach (var item in listTax)
                {                                        
                    if (string.IsNullOrEmpty(this.TaxDesc1) && !string.IsNullOrEmpty(item.sysTaxDesc))
                    {
                        this.TaxDesc1 = item.sysTaxDesc;
                        this.Tax1 = item.sysTaxPercentage;
                        this.TaxCalculated1 = this.GetCalculatedTax(dbHelp, item, this.Amount);
                    }
                    else if (string.IsNullOrEmpty(this.TaxDesc2) && !string.IsNullOrEmpty(item.sysTaxDesc))
                    {
                        this.TaxDesc2 = item.sysTaxDesc;
                        this.Tax2 = item.sysTaxPercentage;
                        this.TaxCalculated2 = this.GetCalculatedTax(dbHelp, item, this.Amount);
                    }
                    else if (string.IsNullOrEmpty(this.TaxDesc3) && !string.IsNullOrEmpty(item.sysTaxDesc))
                    {
                        this.TaxDesc3 = item.sysTaxDesc;
                        this.Tax3 = item.sysTaxPercentage;
                        this.TaxCalculated3 = this.GetCalculatedTax(dbHelp, item, this.Amount);
                    }
                    else if (string.IsNullOrEmpty(this.TaxDesc4) && !string.IsNullOrEmpty(item.sysTaxDesc))
                    {
                        this.TaxDesc4 = item.sysTaxDesc;
                        this.Tax4 = item.sysTaxPercentage;
                        this.TaxCalculated4 = this.GetCalculatedTax(dbHelp, item, this.Amount);
                    }
                    else if (string.IsNullOrEmpty(this.TaxDesc5) && !string.IsNullOrEmpty(item.sysTaxDesc))
                    {
                        this.TaxDesc5 = item.sysTaxDesc;
                        this.Tax5 = item.sysTaxPercentage;
                        this.TaxCalculated5 = this.GetCalculatedTax(dbHelp, item, this.Amount);
                    }
                }
                /*string sql = "SELECT tc.sysTaxCode AS TaxGroupID, tc.sysTaxDesc AS TaxCodeDesc, tc.sysTaxOnTotal AS IsOnTotal,tc.sysTaxPercentage AS TaxToApply  FROM sysTaxCode tc";
                sql += " WHERE tc.sysTaxCode=@TaxGroupID ORDER BY tc.sysTaxSequence";

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("TaxGroupID", taxGroupID, MyDbType.Int)
                }))
                {
                    string txDesc = string.Empty;
                    bool isOnTotal = false;
                    while (dr.Read())
                    {
                        this.TaxGroupID = BusinessUtility.GetInt(dr["TaxGroupID"]);
                        txDesc = BusinessUtility.GetString(dr["TaxCodeDesc"]);
                        isOnTotal = BusinessUtility.GetBool(dr["IsOnTotal"]);
                        if (string.IsNullOrEmpty(this.TaxDesc1) && !string.IsNullOrEmpty(txDesc))
                        {
                            this.TaxDesc1 = txDesc;
                            this.Tax1 = BusinessUtility.GetDouble(dr["TaxToApply"]);
                            this.TaxCalculated1 = isOnTotal;
                        }
                        else if (string.IsNullOrEmpty(this.TaxDesc2) && !string.IsNullOrEmpty(txDesc))
                        {
                            this.TaxDesc2 = txDesc;
                            this.Tax2 = BusinessUtility.GetDouble(dr["TaxToApply"]);
                            this.TaxCalculated2 = isOnTotal;
                        }
                        else if (string.IsNullOrEmpty(this.TaxDesc3) && !string.IsNullOrEmpty(txDesc))
                        {
                            this.TaxDesc3 = txDesc;
                            this.Tax3 = BusinessUtility.GetDouble(dr["TaxToApply"]);
                            this.TaxCalculated3 = isOnTotal;
                        }
                        else if (string.IsNullOrEmpty(this.TaxDesc4) && !string.IsNullOrEmpty(txDesc))
                        {
                            this.TaxDesc4 = txDesc;
                            this.Tax4 = BusinessUtility.GetDouble(dr["TaxToApply"]);
                            this.TaxCalculated4 = isOnTotal;
                        }
                        else if (string.IsNullOrEmpty(this.TaxDesc5) && !string.IsNullOrEmpty(txDesc))
                        {
                            this.TaxDesc5 = txDesc;
                            this.Tax5 = BusinessUtility.GetDouble(dr["TaxToApply"]);
                            this.TaxCalculated5 = isOnTotal;
                        }
                    }
                }*/
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        private double GetCalculatedTax(DbHelper dbHelp, SysTaxCode tc, double amount)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                double calculatedAmount = 0.00D;
                if (tc.sysTaxOnTotal && !string.IsNullOrEmpty(tc.TaxOnTotalTaxCode))
                {                    
                    double tx1 = CalculationHelper.GetAmount(amount * this.GetPercentageTax(tc.TaxOnTotalTaxCode) / 100.00D);
                    calculatedAmount = CalculationHelper.GetAmount((amount + tx1) * tc.sysTaxPercentage / 100.00D);
                }
                else
                {
                    calculatedAmount = CalculationHelper.GetAmount(amount * tc.sysTaxPercentage / 100.00D);
                }
                return calculatedAmount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        private double GetPercentageTax(string txCode)
        {
            if (string.IsNullOrEmpty(txCode))
            {
                return 0.00D;
            }

            if (txCode.Equals(this.TaxDesc1))
            {
                return this.Tax1;
            }
            if (txCode.Equals(this.TaxDesc2))
            {
                return this.Tax2;
            }
            if (txCode.Equals(this.TaxDesc3))
            {
                return this.Tax3;
            }
            if (txCode.Equals(this.TaxDesc4))
            {
                return this.Tax4;
            }
            if (txCode.Equals(this.TaxDesc5))
            {
                return this.Tax5;
            }
            return 0.00D;
        }
    }
}
