﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class AppConfiguration
    {
        public static AppSettingCollection AppSettings {
            get {
                if (HttpRuntime.Cache["HostSettings"] != null)
                {
                    return (AppSettingCollection)HttpRuntime.Cache["HostSettings"];
                }
                else
                {
                    SetAllHostSettingsToCache();
                    return (AppSettingCollection)HttpRuntime.Cache["HostSettings"];
                }
            }
        }

        private static void SetAllHostSettingsToCache()
        {
            string sql = "SELECT * FROM app_settings";
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper(true);
            AppSettingCollection appSettings = new AppSettingCollection();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
                string key = string.Empty;
                string val = string.Empty;
                while (dr.Read())
                {
                    key = BusinessUtility.GetString(dr["Key"]);
                    val = BusinessUtility.GetString(dr["Value"]);

                    if (Enum.IsDefined(typeof(AppSettingKey), key))
                    {
                        appSettings.Add((AppSettingKey)Enum.Parse(typeof(AppSettingKey), key), val);
                    }
                }
                HttpRuntime.Cache["HostSettings"] = appSettings;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public static void ClearCacheOnLogout()
        {
            HttpRuntime.Cache.Remove("HostSettings");
        }
    }

    public class AppSetting
    {
        public AppSettingKey Key { get; set; }
        public string Value { get; set; }
    }

    public class AppSettingCollection : CollectionBase
    {
        public void Add(AppSetting setting)
        {
            base.List.Add(setting);
        }
        public void Add(AppSettingKey key, string val)
        {
            base.List.Add(new AppSetting { Key = key, Value = val });
        }
        public string this[AppSettingKey key]
        {
            get
            {
                string settingValue = null;
                foreach (AppSetting item in base.List)
                {
                    if (item.Key != key)
                        continue;
                    settingValue = item.Value;
                }
                return settingValue;
            }
        }
    }

    public enum AppSettingKey
    {
        ShippingProcessID,
        BarcodePrefix,
        BarcodePostfix,
        VerificationPassword,
        DefaultEmailSender,
        AllowRoundingDecimalAmount,
        DecimalPlacesUpToRound,
        PostXmlDefaultCompanyID,
        PostXmlDefaultUserID,
        PostXmlWhsCode,
        PostXmlInvoiceUrl
    }

    /*public class AppSetting
    {        
        //<add key="SivanandaLogoPath" value="http://live2.itechcanada.com/sivananda/sivananda_logo.png"/>    
        public string LogoPathToPrint { get; internal set; }
        //<add key="VertualDirectoryAlias" value="/InbizWebDvlp/"/>
        public string ERPVertualDirectoryAlias { get; internal set; }
        //<add key="BarCodeFont" value="IDAutomationHC39M"/>
        public string BarCodeFont { get; internal set; }
        //<add key="FileManagerRootPath" value="~/Upload/FileManagerUploads/"/>
        public string FileManagerRootPath { get; internal set; }
        //<add key="CMS_ACCESS_URL" value="http://localhost:8795/EcomWebDvlp/cmsadmin/authenticate.aspx"/>
        public string CmsAuthenticationUrl { get; internal set; }
        //<add key="RelativePath" value="http://localhost:7103/InbizWeb4.0/upload/Product/"/>
        public string ProductImageWebsitePath { get; internal set; }
        //<add key="PhysicalPath" value="C:\inetpub\wwwroot\InBizERP\upload\Product\"/>
        public string ProductImagePhysicalPath { get; internal set; }
        //<add key="PDFPath" value="http://localhost:7103/InbizWeb4.0/pdf/"/>
        public string PdfFolderWebsitePath { get; internal set; }
        //<add key="QOFormat" value="A"/> Don't know what was the purpose to use this option
        public string QoutationFormat { get; internal set; }
        //<add key="RelativePathForCategory" value="http://localhost:7103/InbizWeb4.0/upload/Category/"/>
        public string CategoryImageWebsitePath { get; internal set; }
        //<add key="RelativePathForSubCategory" value="http://localhost:7103/InbizWeb4.0/upload/SubCategory/"/>
        public string SubCategoryImageWebsitePath { get; internal set; }
        //<add key="CompanyLogoPath" value="http://localhost:7103/InbizWeb4.0/upload/companyLogo/"/>
        public string CompanyLogoImagePath { get; internal set; }
        //<!--Setting for Credit card Transaction(Federated_Canada_API.pdf)-->
        //<add key="CreditCardAPI" value="tr"/>
        public string POSCreditCartAPI { get; internal set; }
        //Setting for Credit card Transaction(Federated_Canada_API.pdf)     
        //<add key="MSIPostURL" value="http://secure.federatedgateway.com/api/transact.php?"/>
        public string MSIPostUrl { get; internal set; }
        //<add key="MSIUserID" value="demo"/>
        public string MSIUserID { get; internal set; }
        //<add key="MSIUserPassword" value="password"/>
        public string MSIUserPassword { get; internal set; }
        //Setting for Credit card Transaction(Virtual Merchant)
        //<add key="VMPostURL" value="http://www.myvirtualmerchant.com/VirtualMerchant/processxml.do?xmldata="/>
        public string VMPostURL { get; internal set; }
        //<add key="VMUserID" value="demo"/>
        public string VMUserID { get; internal set; }
        //<add key="VMUserPassword" value="password"/>
        public string VMUserPassword { get; internal set; }
        //<add key="MerchantID" value="Yes"/>
        public string IsPosMerchantID { get; internal set; }
        //<add key="MultipleCompanies" value="True"/>
        public string AllowMultipleCompanies { get; internal set; }
        //<add key="CSVPartnerPath" value="/upload/Partner/"/>
        public string PartnerCsvFilePath { get; internal set; }
        //<add key="CSVContactPath" value="/upload/Contact/"/>
        public string PartnerContactCsvFilePath { get; internal set; }
        //<add key="unsubscribeEmailURL" value="http://localhost:7103/InbizWeb4.0/unsubscribe.aspx"/>
        public string UnsubscribeEmailUrl { get; internal set; }
        //<add key="ShowPrdPOSCatg" value="true"/>
        public string ShowProductPosCategory { get; internal set; }
        //<add key="imgPathPOSCatg" value="http://localhost:7103/InbizWeb4.0/upload/POSCategory/"/>
        public string PosCategoryImagePath { get; internal set; }
        //<add key="PrintURL" value="http://localhost:7103/InbizWeb4.0/POS/PinpadPrint.aspx?card=yes"/>
        public string PosPinpadPrintUrl { get; internal set; }
        //<add key="YellowSR" value="No"/>
        public string IsYellowServer { get; internal set; }
        //<add key="YellowSRUpdate" value="http://thepuertoricoyellowpages.com/Admin/AddEditBusiness.aspx?"/>
        public string YellowServerUpdatePageUrl { get; internal set; }
        //<add key="AppletLog" value="yes"/>
        public string AllowPosAppletToCreateLog { get; internal set; }
        //<add key="IsReceipt" value="yes"/>
        public string IsPosReceipt { get; internal set; }
        //<add key="POSCatLocation" value="C:\inetpub\wwwroot\InBizERP\upload\POSCategory\"/>
        public string PosCategoryImagePhysicalPath { get; internal set; }
        //Invoice Reference Information for multiple company(If sequential invID then SeparateInvID=No else SeparateInvID=yes)
        //<add key="SeparateInvID" value="Yes"/>
        public string AllowSeparateInvoiceID { get; internal set; }
        //<add key="BtnEmailVisible" value="yes"/>
        public string ShowCallManagerEmailButton { get; internal set; }
        //<add key="EmailLink" value="http://callmgmt.itechcanada.com/"/>
        public string CallManagerUrl { get; internal set; }
        //<add key="KitPercentage" value="10"/> Don' know what purpose of using this
        public string KitPercentage { get; internal set; }
        //<add key="skypeImage" value="http://localhost:7103/InbizWeb4.0/images/call_24.png"/>
        public string SkypeImageUrl { get; internal set; }
        //<add key="AllowCustomInvoiceEntry" value="false"/>
        public string AllowCustomInvoiceEntry { get; internal set; }
        //<add key="UsePayPal" value="true"/>
        public string UsePayPal { get; internal set; }
        //<add key="PP_API_USERNAME" value="hitend_1300354600_biz_api1.itechcanada.com"/>
        public string PayPalApiUserName { get; internal set; }
        //<add key="PP_API_PASSWORD" value="1300354613"/>
        public string PayPalApiPassword { get; internal set; }
        //<add key="PP_API_SIGNATURE" value="AFcWxV21C7fd0v3bYYYRCpSSRl31Ae1KYAIXkGEFMmW0icnBdxRNDFRx"/>
        public string PayPalApiSignature { get; internal set; }
        //<add key="PP_ENVIRONMENT" value="sandbox"/> set below live or sandbox
        public string PayPalEnvironment { get; internal set; }
        //<add key="UseSandbox" value="true"/>
        public string PayPalUseSendBox { get; internal set; }
        //<add key="PP_Certificate" value=""/>
        public string PayPalCertificate { get; internal set; }
        //<add key="PP_PRIVATE_KEY_PASSWORD" value=""/>
        public string PayPalCurrencyCode { get; internal set; }
        //<add key="DocumentHeaderInfo" value="COM"/>  Setting Used In Print Template
        public string BuildPrintHeaderTemplateFrom { get; internal set; } //COM for company & WHS for warehouse
        //<add key="WebSaleWhsCode" value="VMN"/>
        public string EcommerceWebWarehouse { get; internal set; }
        //<add key="EVO_MERCHANT_USER" value="demo"/>
        public string EvoCanadaMerchantUser { get; internal set; }
        //<add key="EVO_MERCHANT_PASSWORD" value="password"/>
        public string EvoCanadaMerchantPassword { get; internal set; }
        //<add key="DuplicatEmailAddress" value="noname@email.no"/> Duplicat Email Address To Allow For Partner
        public string NoEmailAddress { get; internal set; }
        //<add key="AllowAmountToBeRounded" value="true"/>
        public string AllowAmountToBeRounded { get; internal set; }


        public string DecimalPlacesToRoundPrice { get; internal set; }

        public string ShippingProcessID { get; internal set; }
        //To Do to add more app settings
    }

    public class HostSetting
    {
        // /// <summary>
        ///// Gets the host setting.
        ///// </summary>
        ///// <param name="key">The key.</param>
        ///// <returns></returns>
        //public static string GetHostSetting(HostSettingKey key)
        //{
        //    if(GetHostSettings().Contains(key.ToString()))
        //    {
        //        return Convert.ToString(GetHostSettings()[key.ToString()]);
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
        //}

        ///// <summary>
        ///// Gets the host settings.
        ///// </summary>
        ///// <returns></returns>
        //private static Hashtable GetHostSettings()
        //{     
        //    Hashtable h = (Hashtable)HttpRuntime.Cache["HostSettings"];
        //    if(h == null)
        //    {
        //        SetAllHostSettingsToCache();
        //        h = (Hashtable)HttpRuntime.Cache["HostSettings"];
        //    }

        //    return h;
        //}

        public static AppSetting AppSettings
        {
            get
            {
                if (HttpRuntime.Cache["HostSettings"] != null)
                {
                    return (AppSetting)HttpRuntime.Cache["HostSettings"];
                }
                else
                {
                    SetAllHostSettingsToCache();
                    return (AppSetting)HttpRuntime.Cache["HostSettings"];
                }
            }
        }

        private static void SetAllHostSettingsToCache()
        {
            string sql = "SELECT * FROM app_host_settings";
            AppSetting appSetting = new AppSetting();
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper();
            //Hashtable h = new Hashtable();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
                string key = string.Empty;
                string val = string.Empty;
                while (dr.Read())
                {
                    key = BusinessUtility.GetString(dr["SettingName"]);
                    val = BusinessUtility.GetString(dr["SettingValue"]);
                    switch (key)
                    {
                        case "DecimalPlacesToRoundPrice":
                            appSetting.DecimalPlacesToRoundPrice = BusinessUtility.GetInt(val);
                            break;
                        //case "TestBool":
                        //    appSetting.TestBool = val.Trim().ToLower() == "true" || val.Trim().ToLower() == "yes";
                        //    break;
                    }
                }
                HttpRuntime.Cache["HostSettings"] = appSetting;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public static void UpdateSetting(int id, string value)
        {
            string sql = "UPDATE app_host_settings SET SettingValue=@SettingValue WHERE HostSettingID=@HostSettingID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("SettingValue", value, MyDbType.String),
                    DbUtility.GetParameter("HostSettingID", id, MyDbType.Int)
                });
                HttpRuntime.Cache.Remove("HostSettings");
                //HttpRuntime.Cache.Remove("HostSectionUrl");
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void ClearCacheOnLogout()
        {
            //HttpRuntime.Cache.Remove("HostSectionUrl");
            HttpRuntime.Cache.Remove("HostSettings");
        }
    }*/
}
