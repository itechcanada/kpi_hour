﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace iTECH.InbizERP.BusinessLogic
{
    #region Enums

    public enum RESTAURANTMENU_ORDERSTATUS
    {
        NEW = 9,
        ACCEPT = 10,
        HELD = 11,
        READY = 12,
        PICKEDUP = 13
    }

    public enum RESTAURANTMENU_PAYMENTSTATUS
    {
        PAID = 1,
        NOTPAID = 2,
        PAIDWITHPOINTS = 3,
        POINTSPICKUP = 4,
        POINTSCREDIT = 5
    }
    /// <summary>
    /// Keep synchronize with data table cdl_sysapplanguages.
    /// </summary>
    public enum ApplicationLanguage
    {
        English = 1,
        French = 2
    }

    //Added by Hitendra 
    public enum HostSettingKey
    {
        //RelativePath,
        //PhysicalPath,
        //PDFPath,
        //SMTPServer,
        //SMTPUserEmail,
        //SMTPPassword,
        //SMTPPort,
        //IsErrorTrapRequired,
        //ErrorEmailFrom,
        //ErrorEmailTo,
        //ErrorSubject,
        //IsAsynchronousMail,
        //UtilityPath,
        //CrystalImageCleaner_AutoStart,
        //CrystalImageCleaner_Sleep,
        //CrystalImageCleaner_Age,
        //QOFormat,
        //RelativePathForCategory,
        //RelativePathForSubCategory,
        //ERPServiceUrl,
        //CompanyLogoPath,
        //CreditCardAPI,
        //MSIPostURL,
        //MSIUserID,
        //MSIUserPassword,
        //VMPostURL,
        //VMUserID,
        //VMUserPassword,
        //MerchantID,
        //EmailFrom,
        //MultipleCompanies,
        //CSVPartner,
        //CSVPartnerPath,
        //CSVContact,
        //CSVContactPath,
        //eComWebPageURL,
        //unsubscribeEmailURL,
        //ShowPrdPOSCatg,
        //imgPathPOSCatg,
        //PrintURL,
        //YellowSR,
        //YellowSRUpdate,
        //AppletLog,
        //IsReceipt,
        //POSCatLocation,
        //SeparateInvID,
        //BtnEmailVisible,
        //EmailLink,
        //KitPercentage,
        //skypeImage,
        //AllowCustomInvoiceEntry,
        //Site,
        //Format,
        //Currency,
        //Condition,
        //Duration,
        //Category1,
        //StoreCategory,
        //AcceptPayPal,
        //PayPalEmailAddress,
        //ShippingType,
        //SpecifyingShippingCosts,
        //InternationalShippingType,
        //ReturnsAccepted,
        //LocationCityState,
        //LocationCountry,
        //DispatchTimeMax,
        //ShippingServiceOptions,
        //InternationalShippingServiceOptions,
        //VertualDirectoryAlias,
        //ReservationProcessDefaultCurrencyCode,
        //ApplicationName,
        //AdminFee
    }

    public enum OperationResult
    {
        None,
        Success,
        Error,
        AlreadyExists,
        Failuer
    }

    /// <summary>
    /// Check partnertype table. Need to keep syncronize values with table data keys
    /// </summary>
    public enum PartnerTypeIDs
    {
        Distributer = 1, //Institution
        EndClient = 2,  //Guest or Staff for now
        Reseller = 3,
        Leads = 4
    }

    /// <summary>
    /// Check ordertype table. Need to keep syncronize values with table data keys
    /// </summary>
    public enum OrderCommission
    {
        Sales = 1,
        Transfer = 2,
        Gift = 3,
        Reservation = 4,
        POS = 5,
        Web = 7,
        Shipment = 8
    }

    public enum StatusAmountReceivedVia
    {
        Cash = 0,
        Cheque = 1,
        CreditCard = 2,
        MoneyOrder = 3,
        DirectWireTransfer = 4,
        DebitCard = 5,
        WriteOff = 6,
        SwipeCard = 7,
        Paypal = 8,
        Credit = 9,
        Refund = 10,
        GiftCard = 11,
        GiftFree = 12,
        InStoreCredit = 13,
        Visa = 14,
        MC = 15,
        AmEx = 16
    }

    public enum StatusProductType
    {
        Product = 1,
        Accommodation = 2,
        ServiceProduct = 3,
        AdminFee = 4,
        ChildUnder12 = 5,
        CourseProduct = 6,
        Default = 1
    }

    public enum StatusGuestType
    {
        None = 0,
        Guest = 1,
        SpecialGuest = 2,
        Staff = 3,
        CourseParticipants = 4,
        Default = 0
    }

    public enum StatusReservation
    {
        New = 1,
        Processed = 2,
        Closed = 3
    }

    /// <summary>
    /// See sysstatus table SELECT * FROM sysstatus WHERE sysApppfx='CT' AND sysAppCode='dlSGp'
    /// Keep values syncronize
    /// </summary>
    public enum StatusRoomType
    {
        //Single = 1,
        //Double = 2,
        //Triple = 3,
        //Quadruple = 4,
        //Cabin = 5,
        //Scabin = 6,
        //Dormitory = 7,
        //Tent = 8
        Private = 1,
        Shared = 2,
        Dormitory = 3,
        Cabin = 4,
        Tent = 5,
        SCabin = 6
    }



    public enum ProductDiscountType
    {
        DateRange = 1,
        Day = 2
    }

    public enum CustomFieldType
    {
        Integer = 1,
        Double = 2,
        Currency = 3,
        StringSingleLine = 4,
        StringMultiline = 5,
        BooleanCheckBox = 6
    }

    public enum CustomFieldApplicableSection
    {
        CustomerAddEdit = 1,
        SalesOrderAddEdit = 2,
        InvoiceAddEdit = 3
    }

    public enum POSTransactionBy
    {
        Cash = 1,
        Visa = 2,
        MasterCard = 3,
        Amex = 4,
        Interac = 5,
        ExactCash = 6,
        Waste = 7,
        Gift = 8,
        StaffGift = 9
    }

    public enum SexForAccommodation
    {
        Any = 1,
        Male = 2,
        Female = 3
    }

    public enum OrderHeldReasonCode
    {
        InsufficientInventory = 1,
        IncompleteAddress = 2,
        OrderNotValidated = 3,
        Other = 4
    }

    public enum InvMovmentSrc
    {
        POS = 0, // POS
        SHP = 1, // Ship,
        RTN = 2,// Return,
        TRN = 3,// Transfer
        IMPT = 4, // Import
        SUEXL = 5, // StyleUpdateEXCEL
        SU = 6, // StyleUpdate
        PO = 7,// PO
        PM = 8,// ProductManual
        MV = 9,  //WarehouseMovement
        QIU = 10// Quick Inventory Update
    }

    public enum InvMovmentUpdateType
    {
        INC = 0, //"Increment",
        ORWT = 1,// Overwrite
        DIC = 2, //Decrement
    }

    #endregion

    #region Structs
    public struct AppCulture
    {
        public const string ENGLISH_CANADA = "en-CA";
        public const string FRENCH_CANADA = "fr-CA";
        public const string DEFAULT = "en-CA";
    }

    public struct AppLanguageCode
    {
        public const string EN = "en";
        public const string FR = "fr";
        public const string DEFAULT = "en";
    }

    public struct DeleteCommands
    {
        public const string DELETE_USER = "user";
        public const string DELETE_COMPANY = "company";
        public const string DELETE_CURRENCY = "curency";
        public const string DELETE_PRCESS_GROUP = "processgrup";
        public const string DELETE_WAREHOUSE = "whs";
        public const string DELETE_POS = "pos";
        public const string DELETE_VENDOR = "vendor";
        public const string DELETE_PRODUCT = "prd";
        public const string DELETE_CUSTOMER = "cust";

        public const string DELETE_PRODUCT_QTY = "prdqty";
        public const string DELETE_CATEGORY = "categ";
        public const string DELETE_ATTRIBUTE = "attr";

        public const string DELETE_PRODUCT_SALE_PRICE = "prdsaleprice";
        public const string DELETE_PRD_VENDOR_ASSIGN = "prdvendassing";
        //Added by mukesh 20130417
        public const string DELETE_LOYALTY_PROGRAM = "loyaltyprogram";
    }

    public struct ImageUploadPath
    {
        public const string PRODUCT_IMAGES = "~/Upload/Product/";
        public const string ATTRIBUTE_IMAGE_ICONS = "~/Upload/Attributes/";
        public const string COMPANY_LOGO_UPLOAD_PATH = "~/Upload/companylogo/";
    }

    public struct ThumbSizePrefix
    {
        public const string _16x16 = "16x16_";
        public const string _24x24 = "24x24_";
        public const string _32x32 = "32x32_";
        public const string _48x48 = "48x48_";
        public const string _64x64 = "64x64_";
        public const string _100x100 = "100x100_";
        public const string _120x120 = "120x120_";
        public const string _150x150 = "150x150_";
        public const string _200x200 = "200x200_";
    }

    public struct AlertRefType
    {
        public const string SALES_ORDER_ALERT = "SO";
    }

    public struct AddressReference
    {
        /// <summary>
        /// Internal User
        /// </summary>
        public const string INTERNAL_USER = "I";
        /// <summary>
        /// Distributer
        /// </summary>
        public const string DISTRIBUTER = "D";
        /// <summary>
        /// End Client
        /// </summary>
        public const string END_CLIENT = "E";
        /// <summary>
        /// Reseller
        /// </summary>
        public const string RESELLER = "R";

        /// <summary>
        /// Leads
        /// </summary>
        public const string LEADS = "L";
        /// <summary>
        /// Customer Contact
        /// </summary>
        public const string CUSTOMER_CONTACT = "C";
        /// <summary>
        /// Vendor
        /// </summary>
        public const string VENDOR = "V";
    }

    public struct AddressType
    {
        public const string HEAD_OFFICE = "H";
        public const string BILL_TO_ADDRESS = "B";
        public const string SHIP_TO_ADDRESS = "S";
    }

    public struct SearchByWhat
    {
        public const string CATEGORY = "categ";
        public const string BUSINESS = "bnm";
    }

    public struct StatusCollectionSearchOption
    {
        public const string NULL_VALUE = "NA";
        public const string NOT_NULL_VALUE = "AS";
        public const string ALL_VALUE = "AL";
    }

    public struct InvoicesStatus
    {
        public const string PAYMENT_RECEIVED = "P";
        public const string PARTIAL_PAYMENT_RECEIVED = "Z";
        public const string SUBMITTED = "S";
        public const string RE_SUBMITTED = "R";
        public const string OTHER_STATUS = "B";
    }

    public struct InvoiceReferenceType
    {
        public const string INVOICE = "IV";
        public const string CREDIT_NOTE = "CN";
    }

    public struct POStatus
    {
        public const string ACCEPTED = "A";
        public const string CLOSED = "C";
        public const string RECEIVED = "Y";
        public const string HELD = "H";
        public const string NEW = "N";
        public const string READY_FOR_RECEIVING = "R";
        public const string SUBMITTED = "S";
        public const string NOT_ACCEPTED = "Z";
        public const string RECEIVED_AND_PAID = "RP";
        public const string PAID = "p";
    }

    public struct SOStatus
    {
        public const string APPROVED = "A";
        public const string CLOSED = "C";
        public const string HELD = "H";
        public const string NEW = "N";
        public const string INVOICED = "I";
        public const string IN_PROGRESS = "P";
        public const string SHIPPED = "S";
        public const string SHIPPING_HELD = "Z";
        public const string READY_FOR_SHIPPING = "R";
        public const string CLOSED_INVOICED = "D";
        public const string RESERVATION_CANCELED = "K";
        public const string TRANSFERRED = "T";
    }

    public struct SOType
    {
        public const string QUOTATION = "QT";
    }

    public struct ProductSearchFields
    {
        public const string ProductID = "PI";

        /// <summary>
        /// For Product Name 
        /// </summary>
        public const string ProductName = "PN";

        /// <summary>
        /// For Product Internal ID 
        /// </summary>
        public const string ProductInternalID = "IN";

        /// <summary>
        /// For Product External ID 
        /// </summary>
        public const string ProductExternalID = "EX";

        /// <summary>
        /// For Product Bar Code
        /// </summary>
        public const string ProductBarCode = "UC";
        /// <summary>
        /// For Product Description
        /// </summary>
        public const string ProductDescription = "PD";

        /// For Material
        /// </summary>
        public const string ProductMaterial = "MT";
        /// For Product color
        /// </summary>
        public const string ProductColor = "PC";
        /// For Product size
        /// </summary>
        public const string ProductSize = "PS";
        /// For Product Keywords
        /// </summary>
        public const string ProductKeywords = "KW";

        /// <summary>
        /// Product Tag field
        /// </summary>
        public const string ProductTag = "TI";

        public const string ALPHA = "AL";

        public const string CUSTOMER_FAV = "CF";

        public const string MY_FAV = "MF";


        public const string ProductStyle = "ST";
    }

    public struct CustomerSearchFields
    {
        /// <summary>
        /// Partner ID
        /// </summary>
        public const string PartnerId = "PI";
        /// <summary>
        /// PN is Denoted to Partner Name
        /// </summary>
        public const string PartnerName = "PN";
        /// <summary>
        ///  CN is Denoted to Contact Name
        /// </summary>
        public const string ContactName = "CN";

        /// <summary>
        /// For Partner Phone Number
        /// </summary>
        public const string PartnerPhone = "PH";

        /// <summary>
        /// For Contact Last Name
        /// </summary>
        public const string CustomerFullName = "CA";

        /// <summary>
        /// For Acronyme 
        /// </summary>
        public const string Acronyme = "AY";

        /// <summary>
        /// For Customer Phone Number
        /// </summary>
        public const string CustomerPhoneNumber = "CP";

        public const string INVOICE_NO = "IN";
        public const string CUSTOMER_NAME = "CN";
        public const string EMAIL_ADDRESS = "EM";
    }

    public struct SearchFieldAccountReceivable
    {
        public const string ORDER_NO = "ON";
        public const string INVOICE_NO = "IN";
        public const string CUSTOMER_NAME = "CN";
        public const string CUSTOMER_PHONE = "CP";
        public const string CUSTOMER_PO = "PO";
        public const string CUSTOMER_ACRONYM = "AY";
    }

    public struct OrderSearchFields
    {
        /// <summary>
        /// For Order Number
        /// </summary>
        public const string OrderNo = "ON";
        /// <summary>
        /// For Invoice Number
        /// </summary>
        public const string InvoiceNo = "IN";
        /// <summary>
        /// For Customer Purchase Order Number
        /// </summary>
        public const string CustomerPO = "PO";
        public const string Register_Code = "RC";

    }

    public struct POSearchField
    {
        public const string PRODUCT_NAME = "PN";
        public const string PRODUCT_BAR_CODE = "UC";
        public const string PO_ID = "PO";
        public const string VENDOR_NAME = "VN";
    }

    public struct StatusCategoryGroup
    {
        public const string ALL = "ALL";
        public const string BUILDING = "B";
        public const string ROOM = "R";
    }

    public struct StatusCustomerTypes
    {
        public const string DISTRIBUTER = "D";
        public const string END_CLINET = "E";
        public const string RESELLER = "R";
        public const string LEADS = "L";
        public const string DEFAULT = "D";
    }

    public struct StatusPartnerStatus
    {
        public const string ACTIVE = "A";
        public const string ALUMNI = "AL";
        public const string CANDIDATE = "C";
        public const string WAITING_FOR_VALIDATION = "W";
        public const string EXPIRED = "X";
    }

    public struct StatusSalesOrderType
    {
        public const string QUOTATION = "QT";
    }

    public struct StatusInvoicePrefrences
    {
        public const string EMAIL = "E";
        public const string MAIL = "M";
        public const string FAX = "F";
        public const string DEFAULT = "E";
    }

    public struct TableRef
    {
        public const string CUSTOMER = "CUS";
        public const string VENDOR = "VND";
        public const string SALES_ORDER = "SO";
        public const string PURCHASE_ORDER = "PO";
    }

    public struct UserFileType
    {
        public const string LOGO = "Logo";
    }

    public struct CommonFilePath
    {
        public const string USER_FILES = "~/Upload/UserUploads/";
    }

    public struct GiftCardTransType
    {
        public const string Sale = "S";
        public const string Buy = "B";
    }

    #endregion

    #region Security Module
    public enum Modules
    {
        Administration = 1,
        CRM = 2,
        Inventory = 3,
        Procurement = 4,
        Receiving = 5,
        Sales = 6,
        Invoice = 7,
        AccountReceivable = 8,
        Collection = 9,
        Shipping = 10,
        Ecommerce = 11,
        Reports = 12,
        Reservation = 13,
        OperationModule = 14,
        AssemblerAndPickerModule = 15,
        POS = 16,
        Dashboard = 17
    }

    public enum Permissions
    {
        //Administration
        AddInternalUser = 1,
        EditInternalUser = 2,
        DeleteInternalUser = 3,
        AddCompany = 4,
        EditCompany = 5,
        DeleteCompany = 6,
        AddExchngeRate = 7,
        EditExchangeRate = 8,
        DeleteExchangeRate = 9,
        AddGroupTex = 10,
        EditGroupTex = 11,
        DeleteGroupTex = 12,
        AddProcessGroup = 13,
        EditProcessGroup = 14,
        DeleteProcessGroup = 15,
        AddWarehouse = 16,
        EditWareHouse = 17,
        DeleteWareHouse = 18,
        AddPosUser = 19,
        EditPosUser = 20,
        DeletePosUser = 21,
        ImportCustomer = 22,
        ImportProduct = 23,
        AddVendor = 24,
        EditVendor = 25,
        DeleteVendor = 26,
        AddPurchaseOrder = 27,
        EditPurchaseOrder = 28,
        DeletePurchaseOrder = 29,
        UpdateReceiving = 30,
        CloseReceiving = 31,
        AddCustomer = 32,
        EditCustomer = 33,
        DeleteCustomer = 34,
        AddContact = 35,
        EditContact = 36,
        DeleteContact = 37,
        FollowUpActivity = 38,
        AddCRMCategory = 39,
        EditCRMCategory = 40,
        DeleteCRMCategory = 41,
        AddProduct = 42,
        EditProduct = 43,
        DeleteProduct = 44,
        AddProductCategory = 45,
        EditProductCategory = 46,
        DeleteProductCategory = 47,
        UpdatePhysicalInventory = 48,
        AddSalesOrder = 49,
        EditSalesOrder = 50,
        DeleteSalesOrder = 51,
        CreateInvoice = 52,
        EditInvoice = 53,
        BatchPrint = 54,
        BatchStatement = 55,
        AllowReceiving = 56,
        AddCollectionActivity = 57,
        AllowToSell = 58,
        AllowToRefund = 59,
        AllowPauseTransaction = 60,
        EdtiPosUserOpeningClosingAmount = 61,
        SetupNewEcomSite = 62,
        AddECOMCategory = 63,
        EditECOMCategory = 64,
        DeleteECOMCategory = 65,
        AssignProduct = 66,
        AddPage = 67,
        EditPage = 68,
        DeletePage = 69,
        EditPageContent = 70,
        ManageShipping = 71,
        ModuleManagement = 72,
        AddBaseCurrency = 73
    }

    public enum RoleID
    {
        //DEFAULT_ROLE = 28,
        ADMINISTRATOR = 1,
        VENDOR_MANAGER = 2,
        CRM_MANAGER = 3,
        INVENTORY_MANAGER = 4,
        PROCURMENT_MANAGER = 5,
        PO_APPROVER = 6,
        RECEIVING_MANAGER = 7,
        SALES_MANAGER = 8,
        SALES_REPRESENTATIVE = 9,
        QUOTATION_APPROVER = 10,
        SHIPPING_MANAGER = 11,
        INVOICE_GENERATION = 12,
        FAX_INBOX_USER = 13,
        DOCUMENT_MANAGER = 14,
        MESSAGE_MANAGER = 15,
        POS_USER = 16,
        INVENTORY_READ_ONLY = 17,
        ECOMMERCE_MANAGER = 18,
        ACCOUNT_RECEIVABLE = 19,
        COLLECTION_MANAGER = 20,
        COLLECTION_AGENT = 21,
        ENTERPRISE_PARAMETER_MANAGER = 22,
        POS_MANAGER = 23,
        REPORTER = 24,
        LEADS_MANAGER = 25,
        JOB_PLANNER = 26,
        JOB_PLANNING_MANAGER = 27,
        RESERVATION_MANAGER = 28,
        DASHBOARD_MANAGER = 29,

        //Report Roles
        RPT_SALES_BY_REGISTER = 30,
        RPT_SALES_BY_PRODUCTS_FOR_VENDOR = 31,
        RPT_USER_COMMISSION = 32,
        RPT_GROSS_PROFIT = 33,
        RPT_SALES_REGISTER_BY_PRODUCT = 34,
        RPT_SALES_REPORT_EXCLUDING_POS = 35,
        RPT_SALES_BY_WAREHOUSE = 36,
        RPT_SALES_BY_SALES_PERSON = 37,
        RPT_SALES_BY_PRODUCTS = 38,
        RPT_SALES_BY_CUSTOMER = 39,
        RPT_MAGENTO_EXPORT = 40,

        //RPT_OPEN_ORDER_FULL_FILLMENT = 41,
        //RPT_TODAYS_FOLLOWUPS = 42,
        //RPT_INVENTORY_BY_PRODUCT = 43,
        //RPT_VENDOR_PO_HISTORY = 44,
        //RPT_PREVIOUS_WEEK_COUNT = 45,
        //RPT_MEAL_COUNT_REPORT_AM = 46,
        //RPT_MEAL_COUNT_REPORT_PM = 47,
        //RPT_SPECIAL_GUEST_LIST = 48,
        //RPT_HOUSEKEEPING = 49,
        //RPT_GUEST_LIST = 50,
        //RPT_STAFF_LIST = 51,
        //RPT_SHUTTLE_SERVICE = 52,
        //RPT_TAX_COLLECTED_ON_POS = 53,
        //RPT_TAX_COLLECTED_ON_INVOICE = 54
    }

    //public struct RoleNames
    //{
    //    public const string ADMINISTRATOR = "Administrator";
    //    public const string VENDOR_MANAGER = "Vendor Manager";
    //    public const string CRM_MANAGER = "CRM Manager";
    //    public const string INVENTORY_MANAGER = "Inventory Manager";
    //    public const string PROCURMENT_MANAGER = "Procurment Manager";
    //    public const string PO_APPROVER = "PO Approver";
    //    public const string RECEIVING_MANAGER = "Receiving Manager";
    //    public const string SALES_MANAGER = "Sales Manager";
    //    public const string SALES_REPRESENTATIVE = "Sales Representative";
    //    public const string QUOTATION_APPROVER = "Quotation Approver";
    //    public const string SHIPPING_MANAGER = "Shipping Manager";
    //    public const string INVOICE_GENERATION = "Invoice Generation";
    //    public const string FAX_INBOX_USER = "Fax Inbox User";
    //    public const string DOCUMENT_MANAGER = "Document Manager";
    //    public const string MESSAGE_MANAGER = "Message Manager";
    //    public const string POS_USER = "POS User";
    //    public const string INVENTORY_READ_ONLY = "Inventory Read Only";
    //    public const string E_COMMERCE_MANAGER = "E-Commerce Manager";
    //    public const string ACCOUNT_RECEIVABLE = "Account Receivable";
    //    public const string COLLECTION_MANAGER = "Collection Manager";
    //    public const string COLLECTION_AGENT = "Collection Agent";
    //    public const string ENTERPRISE_PARAMETER_MANAGER = "Enterprise Parameter Manager";
    //    public const string POS_MANAGER = "POS Manager";
    //    public const string REPORTER = "Reporter";
    //    public const string LEADS_MANAGER = "Leads Manager";
    //    public const string JOB_PLANNER = "Job Planner";
    //    public const string JOB_PLANNING_MANAGER = "Job Planning Manager";
    //    public const string DEFAULT_ROLE = "Default Role";
    //}

    #endregion

    #region CustomExceptionCodes

    public struct CustomExceptionCodes
    {
        //Some Common 
        public const string ALREADY_EXISTS = "ALREAY_EXISTS";

        public const string PROCESS_CODE_ALREADY_EXISTS = "PCAE";
        public const string PROCESS_DESCRIPTION_ALREADY_EXISTS = "PDAE";
        public const string WAREHOUSE_CODE_ALREADY_EXISTS = "WCAE";
        public const string WAREHOUSE_DESCRIPTION_ALREADY_EXISTS = "WDAE";
        public const string VENDOR_ALREADY_EXISTS = "VAE";
        public const string PRODUCT_ALREADY_EXISTS = "PRODUCT_ALREADY_EXISTS";
        public const string INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER = "INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER";
        public const string PRODUCT_QTY_RANGE_ALREADY_EXISTS = "PRODUCT_QTY_RANGE_ALREADY_EXISTS";
        public const string VENDOR_ALREADY_ASSIGN_TO_PRODUCT = "VENDOR_ALREADY_ASSIGN_TO_PRODUCT";

        public const string DISCOUNT_DETAIL_ALREADYE_EXISTS = "DISCOUNT_DETAIL_ALREADYE_EXISTS";
        public const string BLACK_OUT_DATE_REANGE_ALREADY_EXISTS = "BLACK_OUT_DATE_REANGE_ALREADY_EXISTS";

        public const string FILE_ALREAY_EXISTS = "FILE_ALREAY_EXISTS";
        public const string MAX_FILE_COUNT_EXCEEDED = "MAX_FILE_COUNT_EXCEEDED";

        public const string ACCOMMODATION_NOT_AVAILABLE = "ACCOMMODATION_NOT_AVAILABLE";
        public const string INVALID_DATE_RANGE = "INVALID_DATE_RANGE";
        public const string INVALID_EMAIL_ID = "INVALID_EMAIL_ID";

        //Partners
        public const string DUPLICATE_KEY = "DUPLICATE_KEY";
    }

    #endregion

    #region SOME_PREDEFINED_PRODUCTS

    public struct PreDefinedProductsInternalID
    {
        public const string ID_CANCEL = "CANCEL00001";
        public const string ID_DISCOUNT = "DISCOUNT00001";
    }

    #endregion

    public enum ImportProductErrorCode
    {
        InValidSKUCode = 401,
        SKUCodeNotFound = 402,
        UnkownError = 403,
        COLORNOTFOUND = 404,
        DUPLICATE = 405,
        COLORCODEREPEATEDINGORUP = 406,
        PRODUCTANDCOLORNOTFOUND = 407

    }
    public struct Hourmsg
    {
        public const string INSERTSUCCESS = "INSERTSUCCESS";
        public const string UPDATESUCCESS = "UPDATESUCCESS";
        public const string FAILED = "FAILED";

    }
     
}
