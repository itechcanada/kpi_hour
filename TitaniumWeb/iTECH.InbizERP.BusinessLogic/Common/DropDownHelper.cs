﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class DropDownHelper
    {
        public static void FillDropdown(ListControl lCtrl, string sysAppPfx, string sysAppCode, string lang, ListItem defaultRootItem)
        {           
            try
            {
                FillDropdown(null, lCtrl, sysAppPfx, sysAppCode, lang, defaultRootItem);   
            }
            catch 
            {
                throw;
            }            
        }

        public static void FillDropdown(DbHelper dbHelp, ListControl lCtrl, string sysAppPfx, string sysAppCode, string lang, ListItem defaultRootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(false);
            }

            string strSQL = string.Empty;
            strSQL = "SELECT sysAppDesc,sysAppLogicCode FROM sysstatus where ";
            strSQL += " sysAppCodeLang =@Lang and sysAppCodeActive=1 and sysAppPfx=@sysAppPfx and sysAppCode=@sysAppCode order by sysAppCodeSeq";

            try
            {
                MySqlParameter[] p = { 
                                         DbUtility.GetParameter("sysAppPfx", sysAppPfx, MyDbType.String),
                                         DbUtility.GetParameter("sysAppCode", sysAppCode, MyDbType.String),
                                         DbUtility.GetParameter("Lang", lang, MyDbType.String)
                                     };

                lCtrl.DataSource = dbHelp.GetDataTable(strSQL, CommandType.Text, p);
                lCtrl.DataTextField = "sysAppDesc";
                lCtrl.DataValueField = "sysAppLogicCode";
                lCtrl.DataBind();

                if (defaultRootItem != null)
                {
                    lCtrl.Items.Insert(0, defaultRootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose)
                {
                    dbHelp.CloseDatabaseConnection();
                }
            }
        }

        //public static void FillWharehouse(ListControl lCtrl, string userWhs, int companyID, ListItem defaultRootItem)
        //{
        //    string strSQL = null;
        //    List<MySqlParameter> p = new List<MySqlParameter>();
        //    strSQL = "SELECT WarehouseCode, WarehouseDescription, concat(upper(WarehouseCode),'::',substring(WarehouseDescription,1,25)) as WD FROM syswarehouses WHERE WarehouseActive='1'  ";            
        //    if (companyID > 0)
        //    {
        //        strSQL += " and WarehouseCompanyID =@WarehouseCompanyID";
        //        p.Add(DbUtility.GetParameter("WarehouseCompanyID", companyID, MyDbType.Int));
        //    }
        //    strSQL += " order by WarehouseDescription";
        //    DbHelper dbHelper = new DbHelper();
        //    try
        //    {
        //        lCtrl.Items.Clear();
        //        lCtrl.DataSource = dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text, p.ToArray());
        //        lCtrl.DataTextField = "WD";
        //        lCtrl.DataValueField = "WarehouseCode";
        //        lCtrl.DataBind();
        //        if (defaultRootItem != null)
        //        {
        //            lCtrl.Items.Insert(0, defaultRootItem);
        //        }
        //        if (!string.IsNullOrEmpty(userWhs))
        //        {
        //            lCtrl.SelectedValue = userWhs;
        //        }               
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelper.CloseDatabaseConnection();
        //    }
        //}

        //public static void FillWhsCode(ListControl lCtrl, string userWhs, ListItem defaultRootItem)
        //{
        //    string strSQL = null;
        //    List<MySqlParameter> p = new List<MySqlParameter>();
        //    strSQL = "SELECT WarehouseCode, concat(upper(WarehouseCode),'::',substring(WarehouseDescription,1,15)) as WD FROM syswarehouses WHERE WarehouseActive='1'  ";
        //    strSQL += " order by WarehouseCode";
        //    DbHelper dbHelper = new DbHelper();
        //    try
        //    {
        //        lCtrl.Items.Clear();
        //        lCtrl.DataSource = dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text, p.ToArray());
        //        lCtrl.DataTextField = "WD";
        //        lCtrl.DataValueField = "WarehouseCode";
        //        lCtrl.DataBind();
        //        if (defaultRootItem != null)
        //        {
        //            lCtrl.Items.Insert(0, defaultRootItem);
        //        }
        //        if (userWhs != null)
        //        {
        //            lCtrl.SelectedValue = userWhs;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelper.CloseDatabaseConnection();
        //    }
        //}

        public static void FillOrderType(ListControl lCtrl, string lang, ListItem rootItem)
        {
            string strSQL = null;
            strSQL = "SELECT ordertypeID,orderTypeDesc from ordertype  where orderTypeLang=@orderTypeLang AND isActive = 1  ORDER BY SeqNo ";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeLang", lang)
                                                                      
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource= dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text, p);
                lCtrl.DataTextField = "orderTypeDesc";
                lCtrl.DataValueField = "ordertypeID";
                lCtrl.DataBind();
                if (rootItem != null) {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillCurrencyDropDown(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT CurrencyCode FROM syscurrencies WHERE CurrencyActive='1' order by CurrencyCode ";            
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "CurrencyCode";
                lCtrl.DataValueField = "CurrencyCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillCurrencyDropDown(ListControl lCtrl, int defaultcompID, ListItem rootItem)
        {            
            string sql = "SELECT CurrencyCode FROM syscurrencies WHERE CurrencyActive='1' order by CurrencyCode ";
            DbHelper dbHelper = new DbHelper(true);
            try
            {                               
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "CurrencyCode";
                lCtrl.DataValueField = "CurrencyCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
                if (defaultcompID > 0)
                {
                    SysCompanyInfo ci = new SysCompanyInfo();
                    ci.PopulateObject(defaultcompID, dbHelper);
                    lCtrl.SelectedValue = ci.CompanyBasCur;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        //public static void FillCompany(ListControl lCtrl, ListItem rootItem)
        //{
        //    string sql = "SELECT companyID,companyName FROM sysCompanyInfo order by companyName asc ";
        //    DbHelper dbHelper = new DbHelper();
        //    try
        //    {
        //        lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
        //        lCtrl.DataTextField = "companyName";
        //        lCtrl.DataValueField = "companyID";
        //        lCtrl.DataBind();
        //        if (rootItem != null)
        //        {
        //            lCtrl.Items.Insert(0, rootItem);
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelper.CloseDatabaseConnection();
        //    }
        //}

        //public static void FillTaxGroup(ListControl lCtrl, ListItem rootItem)
        //{
        //    string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc ";
        //    DbHelper dbHelper = new DbHelper();
        //    try
        //    {
        //        lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
        //        lCtrl.DataTextField = "sysTaxCodeDescText";
        //        lCtrl.DataValueField = "sysTaxCodeDescID";
        //        lCtrl.DataBind();
        //        if (rootItem != null)
        //        {
        //            lCtrl.Items.Insert(0, rootItem);
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelper.CloseDatabaseConnection();
        //    }
        //}

        public static void FillTaxGroup(ListControl lCtrl, string whsCode, ListItem rootItem)
        {
            string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc ";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "sysTaxCodeDescText";
                lCtrl.DataValueField = "sysTaxCodeDescID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
                if (!string.IsNullOrEmpty(whsCode))
                {
                    SysWarehouses w = new SysWarehouses();
                    w.PopulateObject(whsCode, dbHelper);
                    int val = w.WarehouseRegTaxCode;
                    ListItem li = lCtrl.Items.FindByValue(val.ToString());
                    if (li != null)
                    {
                        lCtrl.SelectedValue = val.ToString();
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillPosRegister(ListControl lCtrl, string whsCode, ListItem rootItem)
        {
            string sql = "SELECT sysRegCode, sysRegDesc from sysregister where sysRegActive=1 AND sysRegWhsCode=@sysRegWhsCode ";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("sysRegWhsCode", whsCode, MyDbType.String) });
                lCtrl.DataTextField = "sysRegDesc";
                lCtrl.DataValueField = "sysRegCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillPosRegister(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT sysRegCode, sysRegDesc from sysregister where sysRegActive=1 ";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "sysRegDesc";
                lCtrl.DataValueField = "sysRegCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillProductKitDropdown(ListControl lCtrl, int parentProductId, string lang, ListItem rootItem)
        {
            string sql = "SELECT Pdes.prdName as ProductName,ProductID, concat(ifnull(prdIntID,''),' ',Pdes.prdName) as ProductForKit from products  ";
            sql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where  descLang=@descLang and prdISActive=1  and ProductID != @ProductID order by ProductID";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("descLang", lang, MyDbType.String), DbUtility.GetParameter("ProductID", parentProductId, MyDbType.Int) });
                lCtrl.DataTextField = "ProductForKit";
                lCtrl.DataValueField = "ProductID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillPosCategory(ListControl lCtrl, int productid)
        {
            string sql = string.Format("SELECT pc.prdPOSCatgID, pc.prdPOSCatgDesc FROM prdPOSCatg pc WHERE pc.prdPOSCatgActive ='1' and pc.prdPOSCatgID Not IN (SELECT ps.prdPOSCatgID FROM prdPOSCatgSel ps WHERE ps.productID = {0})", productid);
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "prdPOSCatgDesc";
                lCtrl.DataValueField = "prdPOSCatgID";
                lCtrl.DataBind();                
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillPosCategoryAssigned(ListControl lCtrl, int productid)
        {
            string sql = string.Format("SELECT p.prdPOSCatgID, p.prdPOSCatgDesc FROM prdposcatg p INNER JOIN prdposcatgsel ps ON p.prdPOSCatgID = ps.prdPOSCatgID WHERE prdPOSCatgActive = '1' and ps.productID = {0}", productid);
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "prdPOSCatgDesc";
                lCtrl.DataValueField = "prdPOSCatgID";
                lCtrl.DataBind();
            }
            catch
            {                
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
        //
        public static void FillVendors(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT vendorName,vendorID from vendor where vendorActive=1 ORDER BY vendorName";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "vendorName";
                lCtrl.DataValueField = "vendorID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }       

        public static void FillBlackOutReasonDDL(ListControl lCtrl, ListItem rootItem, string lang)
        {
            string sql = "SELECT ID, ReasonEn,ReasonFr,IsActive FROM z_blackout_reasons WHERE IsActive=1";
            DbHelper dbHelp = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "Reason" + StringUtil.ToTitleCase(lang);
                lCtrl.DataValueField = "ID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void FillBuildings(ListControl lCtrl, ListItem rootItem, string lang)
        {
            string sql = "SELECT BuildingID, BuildingTitleEn,BuildingTitleFr,IsActive FROM z_accommodation_buildings WHERE IsActive=1 ORDER BY Seq";
            DbHelper dbHelp = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "BuildingTitle" + StringUtil.ToTitleCase(lang);
                lCtrl.DataValueField = "BuildingID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void FillRooms(ListControl lCtrl, ListItem rootItem, int buildingID, string lang)
        {
            string sql = "SELECT RoomID, RoomTitleEn,RoomTitleEn,IsActive FROM z_accommodation_rooms WHERE IsActive=1 ";
            if (buildingID > 0)
            {
               sql += " AND BuildingID=@BuildingID "; 
            }
            sql += " ORDER BY Seq";

            DbHelper dbHelp = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("BuildingID", buildingID, MyDbType.Int) });
                lCtrl.DataTextField = "RoomTitle" + StringUtil.ToTitleCase(lang);
                lCtrl.DataValueField = "RoomID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void FillReasonForGift(ListControl lCtrl, ListItem rootItem, string lang)
        {
            string sql = string.Format("SELECT ReasonID, Reason{0} AS ReasonText FROM z_reason_for_gift WHERE IsActive=1 ", lang);           
            sql += " ORDER BY Seq";

            DbHelper dbHelp = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "ReasonText";
                lCtrl.DataValueField = "ReasonID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void FillCourses(ListControl lCtrl, ListItem rootItem, string lang)
        {
            string sql = string.Format("SELECT p.productID, pd.prdName FROM products p INNER JOIN prddescriptions pd ON pd.id=p.productID AND pd.descLang='{0}' WHERE prdIsActive=1 AND prdType={1}", lang, (int)StatusProductType.CourseProduct);
            sql += " ORDER BY pd.prdName";

            DbHelper dbHelp = new DbHelper();
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "prdName";
                lCtrl.DataValueField = "productID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        #region Various type User's Dropdowns
        public static void FillSalesRepresentatives(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT DISTINCT usr.userID, concat(usr.userFirstName, ' ', IFNULL(usr.userLastName, '')) as Name FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RoleID", (int)RoleID.SALES_REPRESENTATIVE, MyDbType.Int)
                });
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillJobPlanningUser(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT DISTINCT usr.userID, concat(usr.userFirstName, ' ', IFNULL(usr.userLastName, '')) as Name FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RoleID", (int)RoleID.JOB_PLANNER, MyDbType.Int)
                });
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public static void FillCollectionAgentUser(ListControl lCtrl, ListItem rootItem)
        {
            string sql = "SELECT DISTINCT usr.userID, concat(usr.userFirstName, ' ', IFNULL(usr.userLastName, '')) as Name FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            DbHelper dbHelper = new DbHelper();
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RoleID", (int)RoleID.COLLECTION_AGENT, MyDbType.Int)
                });
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        } 
        #endregion
    }
}
