﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    [Serializable]
    public class SearchKeyValue
    {
        string _key, _value;

        public SearchKeyValue()
        {
            _key = string.Empty;
            _value = string.Empty;
        }

        public SearchKeyValue(string key, string value)
        {
            _key = key;
            _value = value;
        }

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
