﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Product
    {
        private int _productID;
        private string _prdIntID;
        private string _prdExtID;
        private int _prdType;
        private string _prdUPCCode;
        private string _prdName;
        private bool _prdIsKit;
        private int _prdMinQtyPerSO;
        private string _prdWeight;
        private string _prdWeightPkg;
        private string _prdInoclTerms;
        private int _prdCreatedUserID;
        private DateTime _prdCreatedOn;
        private int _prdLastUpdatedByUserID;
        private DateTime _prdLastUpdatedOn;
        private double _prdSalePricePerMinQty;
        private double _prdEndUserSalesPrice;
        private bool _prdIsSpecial;
        private int _prdDiscount;
        private int _prdMinQtyPOTrig;
        private bool _prdAutoPO;
        private int _prdPOQty;
        private bool _prdIsActive;
        private bool _prdIsWeb;
        private string _prdComissionCode;
        private bool _isPOSMenu;
        private int _prdCategory;
        private int _prdSubcategory;
        private double _prdWebSalesPrice;
        private string _prdDiscountType;
        private string _prdHeight;
        private string _prdLength;
        private string _prdHeightPkg;
        private string _prdLengthPkg;
        private string _prdWidth;
        private string _prdWidthPkg;
        private int _prdExtendedCategory;
        private int _prdAccommodationType;
        // Add for Resturant Application
        private bool _prdIsGlutenFree;
        private bool _prdIsVegetarian;
        private bool _prdIsContainsNuts;
        private bool _prdIsCookedSushi;
        private int _prdSpicyLevel;

        private double _prdFOBPrice;
        private double _prdLandedPrice;
        private Int64 _Defective;
        private Int64 _Available;
        private bool _prdIsGiftCardProduct;

        public int PrdAccommodationType
        {
            get { return _prdAccommodationType; }
            set { _prdAccommodationType = value; }
        }

        public int ProductID
        {
            get { return _productID; }
            set { _productID = value; }
        }

        public Int64 DefectiveQty
        {
            get { return _Defective; }
            set { _Defective = value; }
        }

        public Int64 AvailableQty
        {
            get { return _Available; }
            set { _Available = value; }
        }
        public string PrdIntID
        {
            get { return _prdIntID; }
            set { _prdIntID = value; }
        }
        public string PrdExtID
        {
            get { return _prdExtID; }
            set { _prdExtID = value; }
        }

        public int PrdType
        {
            get { return _prdType; }
            set { _prdType = value; }
        }

        public string PrdUPCCode
        {
            get { return _prdUPCCode; }
            set { _prdUPCCode = value; }
        }

        public string PrdName
        {
            get { return _prdName; }
            set { _prdName = value; }
        }

        public bool PrdIsKit
        {
            get { return _prdIsKit; }
            set { _prdIsKit = value; }
        }

        public int PrdMinQtyPerSO
        {
            get { return _prdMinQtyPerSO; }
            set { _prdMinQtyPerSO = value; }
        }

        public string PrdWeight
        {
            get { return _prdWeight; }
            set { _prdWeight = value; }
        }

        public string PrdWeightPkg
        {
            get { return _prdWeightPkg; }
            set { _prdWeightPkg = value; }
        }

        public string PrdInoclTerms
        {
            get { return _prdInoclTerms; }
            set { _prdInoclTerms = value; }
        }

        public int PrdCreatedUserID
        {
            get { return _prdCreatedUserID; }
            set { _prdCreatedUserID = value; }
        }

        public DateTime PrdCreatedOn
        {
            get { return _prdCreatedOn; }
            set { _prdCreatedOn = value; }
        }

        public int PrdLastUpdatedByUserID
        {
            get { return _prdLastUpdatedByUserID; }
            set { _prdLastUpdatedByUserID = value; }
        }

        public DateTime PrdLastUpdatedOn
        {
            get { return _prdLastUpdatedOn; }
            set { _prdLastUpdatedOn = value; }
        }

        public double PrdSalePricePerMinQty
        {
            get { return _prdSalePricePerMinQty; }
            set { _prdSalePricePerMinQty = value; }
        }

        public double PrdEndUserSalesPrice
        {
            get { return _prdEndUserSalesPrice; }
            set { _prdEndUserSalesPrice = value; }
        }

        public bool PrdIsSpecial
        {
            get { return _prdIsSpecial; }
            set { _prdIsSpecial = value; }
        }

        public int PrdDiscount
        {
            get { return _prdDiscount; }
            set { _prdDiscount = value; }
        }

        public int PrdMinQtyPOTrig
        {
            get { return _prdMinQtyPOTrig; }
            set { _prdMinQtyPOTrig = value; }
        }

        public bool PrdAutoPO
        {
            get { return _prdAutoPO; }
            set { _prdAutoPO = value; }
        }

        public int PrdPOQty
        {
            get { return _prdPOQty; }
            set { _prdPOQty = value; }
        }

        public bool PrdIsActive
        {
            get { return _prdIsActive; }
            set { _prdIsActive = value; }
        }

        public bool PrdIsWeb
        {
            get { return _prdIsWeb; }
            set { _prdIsWeb = value; }
        }

        public string PrdComissionCode
        {
            get { return _prdComissionCode; }
            set { _prdComissionCode = value; }
        }

        public bool IsPOSMenu
        {
            get { return _isPOSMenu; }
            set { _isPOSMenu = value; }
        }

        public int PrdCategory
        {
            get { return _prdCategory; }
            set { _prdCategory = value; }
        }

        public int PrdSubcategory
        {
            get { return _prdSubcategory; }
            set { _prdSubcategory = value; }
        }

        public double PrdWebSalesPrice
        {
            get { return _prdWebSalesPrice; }
            set { _prdWebSalesPrice = value; }
        }

        public string PrdDiscountType
        {
            get { return _prdDiscountType; }
            set { _prdDiscountType = value; }
        }

        public string PrdHeight
        {
            get { return _prdHeight; }
            set { _prdHeight = value; }
        }

        public string PrdLength
        {
            get { return _prdLength; }
            set { _prdLength = value; }
        }

        public string PrdHeightPkg
        {
            get { return _prdHeightPkg; }
            set { _prdHeightPkg = value; }
        }

        public string PrdLengthPkg
        {
            get { return _prdLengthPkg; }
            set { _prdLengthPkg = value; }
        }

        public string PrdWidth
        {
            get { return _prdWidth; }
            set { _prdWidth = value; }
        }

        public string PrdWidthPkg
        {
            get { return _prdWidthPkg; }
            set { _prdWidthPkg = value; }
        }

        public int PrdExtendedCategory
        {
            get { return _prdExtendedCategory; }
            set { _prdExtendedCategory = value; }
        }

        // Property used for resturant app
        public bool PrdIsGlutenFree
        {
            get { return _prdIsGlutenFree; }
            set { _prdIsGlutenFree = value; }
        }

        public bool PrdIsVegetarian
        {
            get { return _prdIsVegetarian; }
            set { _prdIsVegetarian = value; }
        }

        public bool PrdIsContainsNuts
        {
            get { return _prdIsContainsNuts; }
            set { _prdIsContainsNuts = value; }
        }

        public bool PrdIsCookedSushi
        {
            get { return _prdIsCookedSushi; }
            set { _prdIsCookedSushi = value; }
        }

        public int PrdSpicyLevel
        {
            get { return _prdSpicyLevel; }
            set { _prdSpicyLevel = value; }
        }

        public double PrdFOBPrice
        {
            get { return _prdFOBPrice; }
            set { _prdFOBPrice = value; }
        }

        public double PrdLandedPrice
        {
            get { return _prdLandedPrice; }
            set { _prdLandedPrice = value; }
        }

        public bool PrdIsGiftCardProduct
        {
            get { return _prdIsGiftCardProduct; }
            set { _prdIsGiftCardProduct = value; }
        }


        public int MasterID { get; set; }
        public int CollectionID { get; set; }
        public int ColrID { get; set; }


        public bool Insert(DbHelper dbHelp, int userid)
        {
            string sqlExists = "SELECT COUNT(*) FROM products WHERE prdExtendedCategory=@prdExtendedCategory AND prdName=@prdName";

            string sqlInsert = "INSERT INTO products( prdIntID, prdExtID, prdType, prdUPCCode, prdName, prdIsKit, prdMinQtyPerSO,  prdWeight, prdWeightPkg, prdInoclTerms,";
            sqlInsert += " prdCreatedUserID, prdCreatedOn, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIsSpecial,";
            sqlInsert += " prdDiscount,prdMinQtyPOTrig, prdAutoPO, prdPOQty, prdIsActive, prdIsWeb, prdComissionCode,isPOSMenu,prdWebSalesPrice,prdDiscountType,";
            sqlInsert += "prdHeight, prdLength, prdHeightPkg, prdLengthPkg,prdWidth, prdWidthPkg,prdExtendedCategory,prdAccomodationType, prdISGlutenFree, prdIsVegetarian, prdIsContainsNuts, prdIsCookedSushi, prdSpicyLevel, prdFOBPrice, prdLandedPrice, prdIsGiftCardProduct)";
            sqlInsert += " VALUES ( @prdIntID, @prdExtID, @prdType, @prdUPCCode, @prdName, @prdIsKit, @prdMinQtyPerSO,  @prdWeight, @prdWeightPkg, @prdInoclTerms,";
            sqlInsert += " @prdCreatedUserID, @prdCreatedOn, @prdSalePricePerMinQty, @prdEndUserSalesPrice, @prdIsSpecial,";
            sqlInsert += " @prdDiscount,@prdMinQtyPOTrig, @prdAutoPO, @prdPOQty, @prdIsActive, @prdIsWeb, @prdComissionCode,@isPOSMenu,@prdWebSalesPrice,@prdDiscountType,";
            sqlInsert += "@prdHeight, @prdLength, @prdHeightPkg, @prdLengthPkg,@prdWidth, @prdWidthPkg,@prdExtendedCategory,@prdAccomodationType, @prdISGlutenFree, @prdIsVegetarian, @prdIsContainsNuts, @prdIsCookedSushi, @prdSpicyLevel, @prdFOBPrice, @prdLandedPrice, @prdIsGiftCardProduct)";

            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("prdIntID", this.PrdIntID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdExtID", this.PrdExtID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdType", this.PrdType, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdUPCCode", this.PrdUPCCode, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String));

            pList.Add(DbUtility.GetParameter("prdIsKit", this.PrdIsKit, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdMinQtyPerSO", this.PrdMinQtyPerSO, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdWeight", this.PrdWeight, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdWeightPkg", this.PrdWeightPkg, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdInoclTerms", this.PrdInoclTerms, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdSalePricePerMinQty", this.PrdSalePricePerMinQty, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdEndUserSalesPrice", this.PrdEndUserSalesPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdIsSpecial", this.PrdIsSpecial, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdDiscount", this.PrdDiscount, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdMinQtyPOTrig", this.PrdMinQtyPOTrig, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdAutoPO", this.PrdAutoPO, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdPOQty", this.PrdPOQty, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdIsActive", this.PrdIsActive, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsWeb", this.PrdIsWeb, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdComissionCode", this.PrdComissionCode, MyDbType.String));
            pList.Add(DbUtility.GetParameter("isPOSMenu", this.IsPOSMenu, MyDbType.Boolean));
            //pList.Add(DbUtility.GetParameter("prdCategory", this.PrdCategory, MyDbType.Int));
            //pList.Add(DbUtility.GetParameter("prdSubcategory", this.PrdSubcategory, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdWebSalesPrice", this.PrdWebSalesPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdDiscountType", this.PrdDiscountType, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdHeight", this.PrdHeight, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdLength", this.PrdLength, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdHeightPkg", this.PrdHeightPkg, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdLengthPkg", this.PrdLengthPkg, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdWidth", this.PrdWidth, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdWidthPkg", this.PrdWidthPkg, MyDbType.String));

            pList.Add(DbUtility.GetParameter("prdCreatedUserID", userid, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdCreatedOn", DateTime.Now, MyDbType.DateTime));

            int? accommodationType = this.PrdAccommodationType > 0 ? (int?)this.PrdAccommodationType : null;
            pList.Add(DbUtility.GetIntParameter("prdAccomodationType", accommodationType));

            int? extcateg = this.PrdExtendedCategory > 0 ? (int?)this.PrdExtendedCategory : null;
            pList.Add(DbUtility.GetIntParameter("prdExtendedCategory", extcateg));

            pList.Add(DbUtility.GetParameter("prdISGlutenFree", this.PrdIsGlutenFree, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsVegetarian", this.PrdIsVegetarian, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsContainsNuts", this.PrdIsContainsNuts, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsCookedSushi", this.PrdIsCookedSushi, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdSpicyLevel", this.PrdSpicyLevel, MyDbType.Boolean));

            pList.Add(DbUtility.GetParameter("prdFOBPrice", this.PrdFOBPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdLandedPrice", this.PrdLandedPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdIsGiftCardProduct", this.PrdIsGiftCardProduct, MyDbType.Boolean));

            try
            {
                object check = dbHelp.GetValue(sqlExists, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("prdExtendedCategory", extcateg),
                    DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String)
                });
                if (BusinessUtility.GetInt(check) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PRODUCT_ALREADY_EXISTS);
                }


                dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pList.ToArray());

                this.ProductID = dbHelp.GetLastInsertID();
                if (this.ProductID > 0)
                {
                    ProductDescription desc = new ProductDescription();
                    desc.Id = this.ProductID;
                    desc.PrdName = this.PrdName;
                    desc.PrdSmallDesc = this.PrdName;
                    desc.PrdLargeDesc = this.PrdName;

                    desc.DescLang = AppLanguageCode.EN;
                    desc.Insert(dbHelp);

                    desc.DescLang = AppLanguageCode.FR;
                    desc.Insert(dbHelp);
                }
                return this.ProductID > 0;
            }
            catch
            {
                throw;
            }
        }

        public void Insert(int userid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Insert(dbHelp, userid);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(int userid)
        {
            string sqlExists = "SELECT COUNT(*) FROM products WHERE prdExtendedCategory=@prdExtendedCategory AND prdName=@prdName AND productID<>@productID";
            string sqlUpdate = "UPDATE products set prdIntID=@prdIntID,prdExtID=@prdExtID, prdType=@prdType,prdUPCCode=@prdUPCCode,prdName=@prdName,prdIsKit=@prdIsKit,prdMinQtyPerSO=@prdMinQtyPerSO,prdWeight=@prdWeight,";
            sqlUpdate += "prdWeightPkg=@prdWeightPkg,prdInoclTerms=@prdInoclTerms,prdLastUpdatedByUserID=@prdLastUpdatedByUserID,prdLastUpdatedOn=@prdLastUpdatedOn,";
            sqlUpdate += "prdSalePricePerMinQty=@prdSalePricePerMinQty,prdEndUserSalesPrice=@prdEndUserSalesPrice,prdIsSpecial=@prdIsSpecial,";
            sqlUpdate += "prdDiscount=@prdDiscount,prdMinQtyPOTrig=@prdMinQtyPOTrig,prdAutoPO=@prdAutoPO,prdPOQty=@prdPOQty,prdIsActive=@prdIsActive,prdIsWeb=@prdIsWeb,";
            sqlUpdate += "prdComissionCode=@prdComissionCode,isPOSMenu=@isPOSMenu,prdWebSalesPrice=@prdWebSalesPrice,prdDiscountType=@prdDiscountType,prdHeight=@prdHeight,";
            sqlUpdate += "prdLength=@prdLength,prdHeightPkg=@prdHeightPkg,prdLengthPkg=@prdLengthPkg,prdWidth=@prdWidth,prdWidthPkg=@prdWidthPkg, ";
            sqlUpdate += "prdExtendedCategory=@prdExtendedCategory, prdAccomodationType=@prdAccomodationType, prdISGlutenFree=@prdISGlutenFree, prdIsVegetarian=@prdIsVegetarian, ";
            sqlUpdate += "prdIsContainsNuts=@prdIsContainsNuts, prdIsCookedSushi=@prdIsCookedSushi, prdSpicyLevel=@prdSpicyLevel, prdFOBPrice = @prdFOBPrice,  ";
            sqlUpdate += "prdLandedPrice = @prdLandedPrice, prdIsGiftCardProduct = @prdIsGiftCardProduct /*, CollectionID = @CollectionID, CoLID = @ColrID */ ";
            sqlUpdate += "WHERE productID=@productID";
            DbHelper dbHelp = new DbHelper(true);
            List<MySqlParameter> pList = new List<MySqlParameter>();

            pList.Add(DbUtility.GetParameter("prdIntID", this.PrdIntID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdExtID", this.PrdExtID, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdType", this.PrdType, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdUPCCode", this.PrdUPCCode, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String));

            pList.Add(DbUtility.GetParameter("prdIsKit", this.PrdIsKit, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdMinQtyPerSO", this.PrdMinQtyPerSO, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdWeight", this.PrdWeight, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdWeightPkg", this.PrdWeightPkg, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdInoclTerms", this.PrdInoclTerms, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdSalePricePerMinQty", this.PrdSalePricePerMinQty, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdEndUserSalesPrice", this.PrdEndUserSalesPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdIsSpecial", this.PrdIsSpecial, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdDiscount", this.PrdDiscount, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdMinQtyPOTrig", this.PrdMinQtyPOTrig, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdAutoPO", this.PrdAutoPO, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdPOQty", this.PrdPOQty, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdIsActive", this.PrdIsActive, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsWeb", this.PrdIsWeb, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdComissionCode", this.PrdComissionCode, MyDbType.String));
            pList.Add(DbUtility.GetParameter("isPOSMenu", this.IsPOSMenu, MyDbType.Boolean));
            //pList.Add(DbUtility.GetParameter("prdCategory", this.PrdCategory, MyDbType.Int));
            //pList.Add(DbUtility.GetParameter("prdSubcategory", this.PrdSubcategory, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("prdWebSalesPrice", this.PrdWebSalesPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdDiscountType", this.PrdDiscountType, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdHeight", this.PrdHeight, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdLength", this.PrdLength, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdHeightPkg", this.PrdHeightPkg, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdLengthPkg", this.PrdLengthPkg, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdWidth", this.PrdWidth, MyDbType.String));//
            pList.Add(DbUtility.GetParameter("prdWidthPkg", this.PrdWidthPkg, MyDbType.String));

            pList.Add(DbUtility.GetParameter("PrdLastUpdatedByUserID", userid, MyDbType.Int));//
            pList.Add(DbUtility.GetParameter("PrdLastUpdatedOn", DateTime.Now, MyDbType.DateTime));//
            pList.Add(DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.String));//            

            int? accommodationType = this.PrdAccommodationType > 0 ? (int?)this.PrdAccommodationType : null;
            pList.Add(DbUtility.GetIntParameter("prdAccomodationType", accommodationType));

            int? extcateg = this.PrdExtendedCategory > 0 ? (int?)this.PrdExtendedCategory : null;
            pList.Add(DbUtility.GetIntParameter("prdExtendedCategory", extcateg));

            pList.Add(DbUtility.GetParameter("prdISGlutenFree", this.PrdIsGlutenFree, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsVegetarian", this.PrdIsVegetarian, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsContainsNuts", this.PrdIsContainsNuts, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdIsCookedSushi", this.PrdIsCookedSushi, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdSpicyLevel", this.PrdSpicyLevel, MyDbType.Boolean));
            pList.Add(DbUtility.GetParameter("prdFOBPrice", this.PrdFOBPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdLandedPrice", this.PrdLandedPrice, MyDbType.Double));
            pList.Add(DbUtility.GetParameter("prdIsGiftCardProduct", this.PrdIsGiftCardProduct, MyDbType.Boolean));
            //pList.Add(DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int));//
            //pList.Add(DbUtility.GetParameter("ColrID", this.ColrID, MyDbType.Int));//

            try
            {
                object check = dbHelp.GetValue(sqlExists, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("prdExtendedCategory", extcateg),
                    DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String),
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.String)
                });
                if (BusinessUtility.GetInt(check) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PRODUCT_ALREADY_EXISTS);
                }

                dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, pList.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int productID)
        {
            string sql = "SELECT * FROM products WHERE productID=@productID";
            MySqlParameter[] p = { DbUtility.GetParameter("productID", productID, MyDbType.Int) };
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, p);
                if (dr.Read())
                {
                    this._isPOSMenu = BusinessUtility.GetBool(dr["isPOSMenu"]);
                    this._prdAutoPO = BusinessUtility.GetBool(dr["prdAutoPO"]);
                    this._prdCategory = BusinessUtility.GetInt(dr["prdAutoPO"]);
                    this._prdComissionCode = BusinessUtility.GetString(dr["prdComissionCode"]);
                    this._prdCreatedOn = BusinessUtility.GetDateTime(dr["prdCreatedOn"]);
                    this._prdCreatedUserID = BusinessUtility.GetInt(dr["prdCreatedUserID"]);
                    this._prdDiscount = BusinessUtility.GetInt(dr["prdDiscount"]);
                    this._prdDiscountType = BusinessUtility.GetString(dr["prdDiscountType"]);
                    this._prdEndUserSalesPrice = BusinessUtility.GetDouble(dr["prdEndUserSalesPrice"]);
                    this._prdExtID = BusinessUtility.GetString(dr["prdExtID"]);
                    this._prdHeight = BusinessUtility.GetString(dr["prdHeight"]);
                    this._prdHeightPkg = BusinessUtility.GetString(dr["prdHeightPkg"]);
                    this._prdInoclTerms = BusinessUtility.GetString(dr["prdInoclTerms"]);
                    this._prdIntID = BusinessUtility.GetString(dr["prdIntID"]);
                    this._prdIsActive = BusinessUtility.GetBool(dr["prdIsActive"]);
                    this._prdIsKit = BusinessUtility.GetBool(dr["prdIsKit"]);
                    this._prdIsSpecial = BusinessUtility.GetBool(dr["prdIsSpecial"]);
                    this._prdIsWeb = BusinessUtility.GetBool(dr["prdIsWeb"]);
                    this._prdLastUpdatedByUserID = BusinessUtility.GetInt(dr["prdLastUpdatedByUserID"]);
                    this._prdLastUpdatedOn = BusinessUtility.GetDateTime(dr["prdLastUpdatedOn"]);
                    this._prdLength = BusinessUtility.GetString(dr["prdLength"]);
                    this._prdLengthPkg = BusinessUtility.GetString(dr["prdLengthPkg"]);
                    this._prdMinQtyPerSO = BusinessUtility.GetInt(dr["prdMinQtyPerSO"]);
                    this._prdMinQtyPOTrig = BusinessUtility.GetInt(dr["prdMinQtyPOTrig"]);
                    this._prdName = BusinessUtility.GetString(dr["prdName"]);
                    this._prdPOQty = BusinessUtility.GetInt(dr["prdPOQty"]);
                    this._prdSalePricePerMinQty = BusinessUtility.GetDouble(dr["prdSalePricePerMinQty"]);
                    this._prdSubcategory = BusinessUtility.GetInt(dr["prdSubcategory"]);
                    this._prdUPCCode = BusinessUtility.GetString(dr["prdUPCCode"]);
                    this._prdWebSalesPrice = BusinessUtility.GetDouble(dr["prdWebSalesPrice"]);
                    this._prdWeight = BusinessUtility.GetString(dr["prdWeight"]);
                    this._prdWeightPkg = BusinessUtility.GetString(dr["prdWeightPkg"]);
                    this._prdWidth = BusinessUtility.GetString(dr["prdWidth"]);
                    this._prdWidthPkg = BusinessUtility.GetString(dr["prdWidthPkg"]);
                    this._productID = BusinessUtility.GetInt(dr["productID"]);
                    this._prdType = BusinessUtility.GetInt(dr["prdType"]);
                    this._prdExtendedCategory = BusinessUtility.GetInt(dr["prdExtendedCategory"]);
                    this._prdAccommodationType = BusinessUtility.GetInt(dr["prdAccomodationType"]);
                    this._prdIsGlutenFree = BusinessUtility.GetBool(dr["prdISGlutenFree"]);
                    this._prdIsVegetarian = BusinessUtility.GetBool(dr["prdIsVegetarian"]);
                    this._prdIsContainsNuts = BusinessUtility.GetBool(dr["prdIsContainsNuts"]);
                    this._prdIsCookedSushi = BusinessUtility.GetBool(dr["prdIsCookedSushi"]);
                    this._prdSpicyLevel = BusinessUtility.GetInt(dr["prdSpicyLevel"]);
                    this._prdFOBPrice = BusinessUtility.GetDouble(dr["prdFOBPrice"]);
                    this._prdLandedPrice = BusinessUtility.GetDouble(dr["prdLandedPrice"]);
                    this._prdIsGiftCardProduct = BusinessUtility.GetBool(dr["prdIsGiftCardProduct"]);
                    this.MasterID = BusinessUtility.GetInt(dr["MasterID"]);
                    this.CollectionID = BusinessUtility.GetInt(dr["CollectionID"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public void PopulateObject(int productID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, productID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string SearchProductsSql(ParameterCollection pCol, StatusProductType pType, string searchField, string searchText, int vendorID, string lang, string sCollectionID, string sSearchStyle, string sSearchCollection, string sSearchMaterial, string sSearchColor, string sSearchSizeGroup, string sSearchHistory, string sSearchProdCatg, string sSearchProdKeyWord)
        {
            pCol.Clear();

            string strSql = "select Products.prdname, ";
            strSql += " Products.productID, (SELECT SUM(prdQuantity.prdOhdQty) FROM prdQuantity WHERE PrdID=Products.ProductID ) AS  prdOhdQty,prdUPCCode,prdIntID,prdExtID,prdSmallDesc,prdLargeDesc,  ";
            strSql += " CASE prdIsActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as prdIsActive from Products";
            strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
            strSql += " LEFT OUTER JOIN productmaterial pmt on pmt.MatID = Products.MatID";
            strSql += " LEFT OUTER JOIN productcolor pcol on pcol.colorID = Products.ColID";
            strSql += " LEFT OUTER JOIN productsize psz on psz.sizeID = Products.SizeID";
            strSql += " LEFT OUTER JOIN prodCatgSelVal pWebCatg on pWebCatg.ProdID = Products.productID "; // and pWebCatg.ProdSyCatgHdrID IN(1,7,8)
            strSql += " LEFT OUTER JOIN prodSysCatgDtl pscd on pscd.sysCatgDtlID = pWebCatg.prodSysCatgDtlID";
            if (!sSearchSizeGroup.Contains("0") && sSearchSizeGroup != "")
            {
                string[] sGroup = sSearchSizeGroup.Split(',');
                string sSizeGroup = "";
                foreach (string sizegroup in sGroup)
                {
                    if (sSizeGroup == "")
                        sSizeGroup = "'" + sizegroup + "'";
                    else
                        sSizeGroup += "," + "'" + sizegroup + "'";
                }
                strSql += "  LEFT OUTER JOIN productsizecatg AS pszCatg ON pszCatg.SizeCatgName in(" + sSizeGroup + ") ";
            }

            strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID AND descLang = @descLang  ";
            strSql += " LEFT OUTER JOIN ProductClothDesc AS pCd on pCd.ProductID = Products.productID ";
            strSql += "  where  ";
            pCol.Add("@descLang", lang); //Add Lanagguage Parameter

            strSql += "  prdType = @prdType ";
            pCol.Add("@prdType", ((int)pType).ToString());

            if (!string.IsNullOrEmpty(searchText))
            {
                if (searchField != "")
                {
                    switch (searchField)
                    {
                        case ProductSearchFields.ProductName:
                            strSql += " And Pdes.prdName like CONCAT('%', @searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductInternalID:
                            strSql += " And prdIntID like CONCAT(@searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductExternalID:
                            strSql += " And prdExtID like CONCAT(@searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductBarCode:
                            strSql += " And prdUPCCode like CONCAT(@searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductTag:
                            strSql += " AND Products.productID IN (SELECT ProductID FROM prdtags WHERE Tag = @searchText) ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductDescription:
                            strSql += " And ";
                            strSql += " (";
                            strSql += " prdSmallDesc like CONCAT('%', @searchText, '%') or ";
                            strSql += " prdLargeDesc like CONCAT('%', @searchText, '%') ";
                            strSql += " )";
                            pCol.Add("@searchText", searchText);
                            break;

                        case ProductSearchFields.ProductMaterial:
                            strSql += " And pmt.Mat{0} like CONCAT('%', @searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductColor:
                            strSql += " And pcol.color{0} like CONCAT('%', @searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductSize:
                            strSql += " And psz.size{0} like CONCAT('%', @searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                        case ProductSearchFields.ProductKeywords:
                            strSql += " And pscd.sysCatgSelValue{0} like CONCAT('%', @searchText, '%') ";
                            pCol.Add("@searchText", searchText);
                            break;
                    }
                }
            }


            if (sSearchStyle != "")
            {
                strSql += " AND pCd.Style like '%" + sSearchStyle + "%' ";
            }

            if (sSearchCollection != "")
            {
                strSql += " AND pCd.Collection = @CollectionID ";
                pCol.Add("@CollectionID", sSearchCollection);
            }

            if (sSearchMaterial != "")
            {
                strSql += " AND pCd.Material = @MaterialID ";
                pCol.Add("@MaterialID", sSearchMaterial);
            }

            string sSearch = "," + sSearchColor + ",";
            if (!sSearch.Contains(",0,"))
            {
                if (sSearchColor != "")
                {
                    strSql += " AND pCd.Color IN( @ColorID )";
                    pCol.Add("@ColorID", sSearchColor);
                }
            }
            //if (sSearchColor != "")
            //{
            //    strSql += " AND pCd.Color = @ColorID ";
            //    pCol.Add("@ColorID", sSearchColor);
            //}

            //if (sSearchSizeGroup != "")
            if (!sSearchSizeGroup.Contains("0") && sSearchSizeGroup != "")
            {
                strSql += " AND pCd.Size = pszCatg.SizeID ";
            }


            if (sSearchProdCatg != "")
            {
                strSql += " AND pWebCatg.prodSysCatgDtlID IN (@CatgID) ";
                pCol.Add("@CatgID", sSearchProdCatg);
            }

            strSql += "  GROUP BY pWebCatg.ProdID , Products.productID  ORDER BY Products.productID";

            if (vendorID == 0 && sSearchProdCatg == "" && sSearchSizeGroup == "" && sSearchColor == "" && sSearchMaterial == "" && sSearchCollection == "" && sSearchStyle == "" && sSearchSizeGroup == "" && searchText == "") //&& searchField == ""
            {
                strSql += " LIMIT 0 ";
            }

            strSql = string.Format(strSql, lang);
            return strSql;
        }

        /*ParameterCollection pCol, */
        public DataTable SearchProductsQuerySql(DbHelper dbHelp, StatusProductType pType, string searchField, string searchText, int vendorID, string lang, string sCollectionID, string sSearchStyle, string sSearchCollection, string sSearchMaterial, string sSearchColor, string sSearchSizeGroup, string sSearchHistory, string sSearchProdCatg, string sSearchProdKeyWord, string slblDetails)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<MySqlParameter> pList = new List<MySqlParameter>();
                //pCol.Clear();
                string strSql = "";
                //string strSql = "select distinct Products.prdname,prdInoclTerms,Products.productID,prdIsKit as kit,sum(prdOhdQty) as prdOhdQty,Sum(prdQuoteRsv) as prdQuoteRsv,prdUPCCode,sum(prdSORsv) as prdSORsv,prdIntID,prdExtID,prdUPCCode,prdSmallDesc,prdLargeDesc,prdDefectiveQty,  ";
                //strSql += " (funGetProductQtyAvailable(Products.productID,NULL) - funGetProductQtyDefective(Products.productID,NULL)) AS AvailableQty,";
                //strSql += " CASE prdIsActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as prdIsActive,";
                //strSql += " prdType," + Globals.GetFieldName("r.RoomTitle", lang) + " AS Room," + Globals.GetFieldName("b.BuildingTitle", lang) + " AS Building, Products.prdEndUserSalesPrice, r.RoomType, prdSeq ";

                strSql += " SELECT distinct Products.productID, prdUPCCode AS 'SKU#' , Products.prdname AS Name, pCollection.ShortName AS Col, pCd.Style AS 'Style', pmt.MatEn AS 'Material', ";
                strSql += " pcol.ColorEn AS 'Color',  psz.SizeEn AS 'Size' ";

                strSql += " from Products ";

                if (vendorID > 0)
                {
                    strSql += " Inner join prdassociatevendor on prdassociatevendor.prdId=Products.ProductID ";
                }
                strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
                strSql += " LEFT OUTER JOIN z_accommodation_rooms r ON r.RoomID=Products.prdExtendedCategory";
                strSql += " LEFT OUTER JOIN z_accommodation_buildings b ON b.BuildingID=r.BuildingID";
                strSql += " LEFT OUTER JOIN productmaterial pmt on pmt.MatID = Products.MatID";

                strSql += " LEFT OUTER JOIN prodCatgSelVal pWebCatg on pWebCatg.ProdID = Products.productID "; // and pWebCatg.ProdSyCatgHdrID IN(1,7,8)
                //strSql += " LEFT OUTER JOIN prodCatgSelVal pKeyWord on pKeyWord.ProdID = Products.productID and pKeyWord.ProdSyCatgHdrID =7";
                //strSql += " LEFT OUTER JOIN prodCatgSelVal pcsv on pcsv.ProdID = Products.productID and pcsv.ProdSyCatgHdrID =8";
                //strSql += " LEFT OUTER JOIN prodSysCatgDtl pscd on pscd.sysCatgDtlID = pcsv.prodSysCatgDtlID";
                strSql += " LEFT OUTER JOIN prodSysCatgDtl pscd on pscd.sysCatgDtlID = pWebCatg.prodSysCatgDtlID";
                //if (sSearchSizeGroup != "")
                //if (!sSearchSizeGroup.Contains("0") && sSearchSizeGroup != "")
                //{
                //    //strSql += "  LEFT OUTER JOIN productsizecatg AS pszCatg ON pszCatg.SizeCatgName = '" + sSearchSizeGroup + "' ";
                //    string[] sGroup = sSearchSizeGroup.Split(',');
                //    string sSizeGroup = "";
                //    foreach (string sizegroup in sGroup)
                //    {
                //        if (sSizeGroup == "")
                //            sSizeGroup = "'" + sizegroup + "'";
                //        else
                //            sSizeGroup += "," + "'" + sizegroup + "'";
                //    }
                //    strSql += "  LEFT OUTER JOIN productsizecatg AS pszCatg ON pszCatg.SizeCatgName in(" + sSizeGroup + ") ";
                //}

                strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID AND descLang =@descLang ";
                strSql += " INNER JOIN ProductClothDesc AS pCd on pCd.ProductID = Products.productID ";
                strSql += " LEFT OUTER JOIN productcolor pcol on pcol.colorID = pCd.Color";
                strSql += " LEFT OUTER JOIN productsize psz on psz.sizeID = pCd.Size";
                strSql += " INNER JOIN productcycle as pCollection ON pCollection.CycleID = pCd.Collection ";
                strSql += "  where   ";
                //pCol.Add("@descLang", lang); //Add Lanagguage Parameter
                pList.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));


                strSql += "  prdType = @prdType ";
                //pCol.Add("@prdType", ((int)pType).ToString());
                pList.Add(DbUtility.GetParameter("@prdType", ((int)pType), MyDbType.Int));

                if (!string.IsNullOrEmpty(searchText))
                {
                    if (searchField != "")
                    {
                        switch (searchField)
                        {
                            case ProductSearchFields.ProductName:
                                strSql += " And Pdes.prdName like CONCAT('%', @searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductInternalID:
                                strSql += " And prdIntID like CONCAT(@searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductExternalID:
                                strSql += " And prdExtID like CONCAT(@searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductBarCode:
                                strSql += " And prdUPCCode like CONCAT(@searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductTag:
                                strSql += " AND Products.productID IN (SELECT ProductID FROM prdtags WHERE Tag = @searchText) ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductDescription:
                                strSql += " And ";
                                strSql += " (";
                                strSql += " prdSmallDesc like CONCAT('%', @searchText, '%') or ";
                                strSql += " prdLargeDesc like CONCAT('%', @searchText, '%') ";
                                strSql += " )";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;

                            case ProductSearchFields.ProductMaterial:
                                strSql += " And pmt.Mat{0} like CONCAT('%', @searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductColor:
                                strSql += " And pcol.color{0} like CONCAT('%', @searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductSize:
                                strSql += " And psz.size{0} like CONCAT('%', @searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                            case ProductSearchFields.ProductKeywords:
                                strSql += " And pscd.sysCatgSelValue{0} like CONCAT('%', @searchText, '%') ";
                                //pCol.Add("@searchText", searchText);
                                pList.Add(DbUtility.GetParameter("@searchText", searchText, MyDbType.String));
                                break;
                        }
                    }
                }


                if (sSearchStyle != "")
                {
                    strSql += " AND pCd.Style like '%" + sSearchStyle + "%' ";
                }

                if (sSearchCollection != "")
                {
                    strSql += " AND pCd.Collection = @CollectionID ";
                    pList.Add(DbUtility.GetParameter("@CollectionID", BusinessUtility.GetInt(sSearchCollection), MyDbType.Int));
                }

                if (sSearchMaterial != "")
                {
                    strSql += " AND pCd.Material = @MaterialID ";
                    pList.Add(DbUtility.GetParameter("@MaterialID", BusinessUtility.GetInt(sSearchMaterial), MyDbType.Int));
                }

                string sSearch = "," + sSearchColor + ",";
                if (!sSearch.Contains(",0,"))
                {
                    if (sSearchColor != "")
                    {
                        strSql += " AND pCd.Color IN( " + sSearchColor + " )";
                    }
                }

                if (!sSearchSizeGroup.Contains("0") && sSearchSizeGroup != "")
                {
                    strSql += " AND pCd.Size IN(" + sSearchSizeGroup + ") ";
                }


                if (sSearchProdCatg != "")
                {
                    strSql += " AND pWebCatg.prodSysCatgDtlID IN (" + sSearchProdCatg + ") ";
                }

                if (vendorID > 0)
                {
                    strSql += " And prdVendorID = @prdVendorID ";
                    pList.Add(DbUtility.GetParameter("@prdVendorID", BusinessUtility.GetInt(vendorID), MyDbType.Int));
                }
                strSql += "  GROUP BY pWebCatg.ProdID , Products.productID  ORDER BY Products.productID";

                if (vendorID == 0 && sSearchProdCatg == "" && sSearchSizeGroup == "" && sSearchColor == "" && sSearchMaterial == "" && sSearchCollection == "" && sSearchStyle == "" && sSearchSizeGroup == "" && searchText == "") //&& searchField == ""
                {
                    strSql += " LIMIT 0 ";
                }

                DataTable dtProdSearchQuery = dbHelp.GetDataTable(strSql, CommandType.Text, pList.ToArray());

                SysWarehouses objWareHouse = new SysWarehouses();
                DataTable dtWareHouse = objWareHouse.GetCompanyWhs();
                foreach (DataRow drWareHouseCode in dtWareHouse.Rows)
                {
                    DataColumn dCol = new DataColumn();
                    dCol.ColumnName = BusinessUtility.GetString(drWareHouseCode["WareHouseCode"]);
                    dtProdSearchQuery.Columns.Add(dCol);
                }
                DataColumn dColNew = new DataColumn();
                dColNew.ColumnName = BusinessUtility.GetString("Detail");
                dtProdSearchQuery.Columns.Add(dColNew);

                foreach (DataRow drProd in dtProdSearchQuery.Rows)
                {
                    foreach (DataRow drWareHouseCode in dtWareHouse.Rows)
                    {
                        ProductQuantity objProdQty = new ProductQuantity();
                        objProdQty.PopulateObject(null, BusinessUtility.GetInt(drProd["productID"]), Convert.ToString(drWareHouseCode["WarehouseCode"]));
                        this.GetDefectiveAvailableQty(null, BusinessUtility.GetInt(drProd["productID"]), Convert.ToString(drWareHouseCode["WarehouseCode"]));
                        //drProd[BusinessUtility.GetString(drWareHouseCode["WarehouseCode"])] = BusinessUtility.GetString(BusinessUtility.GetInt(objProdQty.PrdOhdQty)) + " (" + BusinessUtility.GetString(BusinessUtility.GetInt(objProdQty.PrdOhdQty) - this.AvailableQty) + ") (" + ProcessInventory.GetInTransitProductQuantity(BusinessUtility.GetInt(drProd["productID"]), Convert.ToString(drWareHouseCode["WarehouseCode"])).ToString() + ")";
                        drProd[BusinessUtility.GetString(drWareHouseCode["WarehouseCode"])] = BusinessUtility.GetString(Convert.ToInt64(objProdQty.PrdOhdQty)) + " (" + BusinessUtility.GetString(Convert.ToInt64(objProdQty.PrdOhdQty) - Convert.ToInt64(this.AvailableQty)) + ") (" + ProcessInventory.GetInTransitProductQuantity(BusinessUtility.GetInt(drProd["productID"]), Convert.ToString(drWareHouseCode["WarehouseCode"])).ToString() + ")";
                    }
                    string editUrl = string.Format("Product.aspx?PrdID={0}&Kit={1}&ptype={2}", BusinessUtility.GetInt(drProd["productID"]), false, 1);
                    string sUrl = string.Format(@"<a href=""{0}"">{1}</a>", editUrl, slblDetails);
                    drProd["Detail"] = sUrl;
                }
                //dtProdSearchQuery.Columns.Remove("productID1");
                return dtProdSearchQuery;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public bool IsOccupiedForFuture(int prdID)
        {
            string sql = "SELECT COUNT(*) FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID WHERE (ri.CheckInDate >= @Today";
            sql += " OR ri.CheckOutDate=@Today)  AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0";

            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Today",  DateTime.Today, MyDbType.DateTime),                        
                        DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
                        DbUtility.GetParameter("BedID", prdID, MyDbType.Int)
                    });
                return BusinessUtility.GetInt(o) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        //Cascate delete will remove data from all child tabls first then from product table
        public void Delete(int productID)
        {

            string sqlInActive = "UPDATE products SET prdIsActive=0 WHERE ProductID={0}";
            string[] arrSql = {
                                  "delete from prdavailablewhs where prdWhsID ={0};",
                                  "delete from prdassociatevendor where prdID={0}",
                                  "delete from prdassociations where id={0}",
                                  "delete from prddescriptions where id={0}",
                                  "delete from prdimages where prdID={0}",
                                  "delete from productsalesprice where prdSalesPriceID={0}",
                                  "delete from productkits where prdID={0}",
                                  "delete from prdcolor where prdID={0}",
                                  "delete from prdquantity where prdID={0}",
                                  "delete from Products where ProductID={0}"
                              };
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(string.Format(sqlInActive, productID), System.Data.CommandType.Text, null);
                //foreach (string sql in arrSql)
                //{
                //    dbHelp.ExecuteNonQuery(string.Format(sql, productID), System.Data.CommandType.Text, null);
                //}
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

        }

        public bool IsProductKit(int productID)
        {
            string sql = "SELECT prdIsKit from products where ProductID=@ProductID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productID, MyDbType.Int) });
                return BusinessUtility.GetBool(scalar);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool IsProductGift(int productID)
        {
            string sql = "SELECT prdIsGiftCardProduct from products where ProductID=@ProductID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productID, MyDbType.Int) });
                return BusinessUtility.GetBool(scalar);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetAdminFeeProductID()
        {
            string sql = string.Format("SELECT ProductID FROM products WHERE prdType={0} AND prdIsActive=1", (int)StatusProductType.AdminFee);
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, System.Data.CommandType.Text, null);
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateAdminFeeProduct(DbHelper dbHelp)
        {
            string sql = string.Format("SELECT ProductID FROM products WHERE prdType={0} AND prdIsActive=1", (int)StatusProductType.AdminFee);
            try
            {
                object val = dbHelp.GetValue(sql, System.Data.CommandType.Text, null);
                this.PopulateObject(dbHelp, BusinessUtility.GetInt(val));
            }
            catch
            {
                throw;
            }
        }

        public void UpdateProductSequence(int productID, int newSeq)
        {
            string sqlUpdate = "UPDATE products SET prdSeq=@Seq WHERE productID=@ProductID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("Seq", newSeq, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void FillProductsAccociations(ListControl lCtrl, int catID, int subCatID, ListItem lRootItem, string lang)
        {
            string sql1 = "SELECT prdName,ProductID, prdCategory, prdSubCategory  FROM products  WHERE prdISActive=1 AND prdIsWeb=1";
            sql1 += string.Format(" AND (IFNULL(prdCategory, 0)=0 OR prdCategory={0}) AND (IFNULL(prdSubCategory, 0)=0 OR prdSubCategory={1}) ORDER BY prdName", catID, subCatID);
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                lCtrl.Items.Clear();
                dr = dbHelp.GetDataReader(sql1, CommandType.Text, null);
                while (dr.Read())
                {
                    ListItem item = new ListItem();
                    item.Text = BusinessUtility.GetString(dr["prdName"]);
                    item.Value = BusinessUtility.GetString(dr["ProductID"]);
                    item.Selected = (BusinessUtility.GetInt(dr["prdCategory"]) == catID && BusinessUtility.GetInt(dr["prdSubCategory"]) == subCatID);

                    lCtrl.Items.Add(item);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void AssignCategoryToProducts(int catID, int subCatID, int[] products)
        {
            string sqlUnassign = "UPDATE products SET prdCategory=0,prdSubCategory=0 WHERE prdCategory=@CatID AND prdSubCategory=@SCatID";
            string sqlAssign = "UPDATE products SET prdCategory=@CatID, prdSubCategory=@SCatID WHERE ProductID IN({0})";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlUnassign, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CatID", catID, MyDbType.Int),
                    DbUtility.GetParameter("SCatID", subCatID, MyDbType.Int)
                });
                if (products != null && products.Length > 0)
                {
                    string[] arrID = Array.ConvertAll(products, p => p.ToString());
                    sqlAssign = string.Format(sqlAssign, string.Join(",", arrID));
                    dbHelp.ExecuteNonQuery(sqlAssign, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("CatID", catID, MyDbType.Int),
                        DbUtility.GetParameter("SCatID", subCatID, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool IsProductUPCCodeDuplicate()
        {
            List<MySqlParameter> lstP = new List<MySqlParameter>();

            string sql = " Select   COUNT(prdUPCCode) FROM Products WHERE prdUPCCode =  @PrdUPCCode"; // SELECT prdIsKit from products where ProductID=@ProductID
            lstP.Add(DbUtility.GetParameter("prdUPCCode", this.PrdUPCCode, typeof(string)));
            if (BusinessUtility.GetInt(this.ProductID) > 0)
            {
                sql += " AND ProductID <>@productId";
                lstP.Add(DbUtility.GetParameter("productId", this.ProductID, MyDbType.Int));
            }


            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, lstP.ToArray());

                if (BusinessUtility.GetInt(o) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void GetDefectiveAvailableQty(DbHelper dbHelp, int productID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "select sum(prdDefectiveQty) DefectiveQty,(funGetProductQtyAvailable(prdID,NULL) - funGetProductQtyDefective(prdID,NULL)) AS AvailableQty from prdQuantity WHERE prdID=@productID";
            MySqlParameter[] p = { DbUtility.GetParameter("productID", productID, MyDbType.Int) };
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, p);
                if (dr.Read())
                {
                    this._Defective = BusinessUtility.GetLong(dr["DefectiveQty"]);
                    this._Available = BusinessUtility.GetLong(dr["AvailableQty"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void GetDefectiveAvailableQty(DbHelper dbHelp, int productID, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "select sum(prdDefectiveQty) DefectiveQty,(funGetProductQtyAvailable(prdID, prdWhsCode) - IFNULL(prdDefectiveQty,0)) AS AvailableQty from prdQuantity WHERE prdID=@productID and prdWhsCode=@prdWhsCode ";
            MySqlParameter[] p = { DbUtility.GetParameter("productID", productID, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String) };
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, p);
                if (dr.Read())
                {
                    this._Defective = BusinessUtility.GetLong(dr["DefectiveQty"]);
                    this._Available = BusinessUtility.GetLong(dr["AvailableQty"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetProductID(DbHelper dbHelp, string sBarCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                string strSql = " SELECT productID from Products WHERE prdUPCCode = @barCode";
                MySqlParameter[] p = { DbUtility.GetParameter("barCode", sBarCode, MyDbType.String) };
                object rValue = dbHelp.GetValue(strSql, CommandType.Text, p);
                return BusinessUtility.GetInt(rValue);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Xml Post Relates Methods

        public void PopulateProduct(DbHelper dbHelp, string externalID, string prdName, double unitPrice, string whsCode, double prdQty, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT ProductID FROM products WHERE prdExtID=@prdExtID";
                ProductQuantity pQty = new ProductQuantity();
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdExtID", externalID, MyDbType.String)
                });
                int id = BusinessUtility.GetInt(v);
                if (id > 0)
                {
                    this.PopulateObject(dbHelp, id);
                    pQty.PopulateObject(dbHelp, id, whsCode);
                    if (pQty.Id <= 0) //Need to add product quantity for appropriate warehouse
                    {
                        pQty.PrdID = id;
                        pQty.PrdOhdQty = prdQty;
                        pQty.PrdWhsCode = whsCode;
                        pQty.Insert(dbHelp);
                    }
                }
                else
                {
                    throw new Exception("Invalid Prdouct");

                    //Insert Product first
                    /*this.IsPOSMenu = false;
                    this.PrdAutoPO = false;
                    this.PrdCreatedOn = DateTime.Now;
                    this.PrdCreatedUserID = userID;
                    this.PrdDiscount = 0;
                    this.PrdDiscountType = "P";
                    this.PrdEndUserSalesPrice = unitPrice;
                    this.PrdExtID = externalID;
                    this.PrdIsActive = true;
                    this.PrdIsKit = false;
                    this.PrdIsSpecial = false;
                    this.PrdIsWeb = false;
                    this.PrdLastUpdatedByUserID = userID;
                    this.PrdLastUpdatedOn = DateTime.Now;
                    this.PrdName = prdName;
                    this.PrdSalePricePerMinQty = unitPrice;
                    this.PrdType = (int)StatusProductType.Product;
                    this.PrdUPCCode = externalID;
                    this.PrdWebSalesPrice = unitPrice;
                    this.Insert(dbHelp, userID);

                    if (this.ProductID > 0) //If record inserted need to add product quantity
                    {
                        pQty.PrdID = this.ProductID;
                        pQty.PrdOhdQty = prdQty;
                        pQty.PrdWhsCode = whsCode;
                        pQty.Insert(dbHelp);
                    }*/
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion

        #region Order Level Methods
        public DataTable GetProductsForQuoation(DbHelper dbHelp, string srcField, string srcText, int compID, StatusProductType prdType, string lang, int iCollectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                var listParams = new List<MySqlParameter>();
                //We should show available quantity instead on head
                string sql = "SELECT * FROM  ( SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, sum(pq.prdOhdQty) as prdOhdQty, ";
                sql += " p.prdSalePricePerMinQty as Price,prdTaxCode,prdDiscount,p.prdDiscountType, pClothDesc.Style, pClothDesc.Collection AS CollectionID, pc.ShortName AS Collection ,pColor.Color{0} AS Color, pSize.Size{0} AS Size FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
                sql += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
                sql += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection ";
                sql += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
                sql += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";
                sql = string.Format(sql, lang);
                if (prdType == StatusProductType.ServiceProduct)
                {
                    sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                else
                {
                    //sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
                    sql += " LEFT OUTER join prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                listParams.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));
                sql += " p.prdType=@prdType AND p.prdIsActive=1 ";
                listParams.Add(DbUtility.GetParameter("@prdType", (int)prdType, MyDbType.Int));
                if (compID > 0)
                {
                    sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
                    listParams.Add(DbUtility.GetParameter("@WarehouseCompanyID", compID, MyDbType.Int));
                }

                if (iCollectionID > 0)
                {
                    //sql += " AND p.CollectionID = @CollectionID";
                    sql += " AND pClothDesc.Collection = @CollectionID";
                    listParams.Add(DbUtility.GetParameter("@CollectionID", iCollectionID, MyDbType.Int));
                }

                if (!string.IsNullOrEmpty(srcText))
                {
                    int intParam = 0;
                    switch (srcField)
                    {
                        case ProductSearchFields.ProductID:
                            sql += " AND p.productID = @productID";
                            int.TryParse(srcText, out intParam);
                            listParams.Add(DbUtility.GetParameter("@productID", intParam, MyDbType.Int));
                            break;
                        case ProductSearchFields.ProductName:
                            sql += " AND p.prdName LIKE CONCAT('%', @prdName, '%')";
                            listParams.Add(DbUtility.GetParameter("@prdName", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductInternalID:
                            sql += " AND p.prdIntID = @prdIntID";
                            listParams.Add(DbUtility.GetParameter("@prdIntID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductExternalID:
                            sql += " AND p.prdExtID = @prdExtID";
                            listParams.Add(DbUtility.GetParameter("@prdExtID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductBarCode:
                            sql += " AND p.prdUPCCode = @prdUPCCode";
                            listParams.Add(DbUtility.GetParameter("@prdUPCCode", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductDescription:
                            sql += " AND (prdSmallDesc LIKE CONCAT('%', @srcData, '%')  OR prdLargeDesc LIKE CONCAT('%', @srcData, '%')";
                            listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ALPHA:
                            if (srcText != "All")
                            {
                                sql += "  And p.prdName LIKE CONCAT(@srcData, '%')";
                                listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            }
                            break;
                        case ProductSearchFields.CUSTOMER_FAV:
                            sql += "  And p.productID IN (SELECT oi.ordProductID FROM orderitems oi INNER JOIN orders o ON o.ordID = oi.ordID AND o.ordCustID = @ordCustID)";
                            listParams.Add(DbUtility.GetParameter("@ordCustID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.MY_FAV:
                            sql += "   And p.prdIsSpecial = 1";
                            break;
                        case ProductSearchFields.ProductStyle:
                            sql += " AND pClothDesc.Style = @Style";
                            listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            //sql += " AND pClothDesc.Style LIKE CONCAT('%', @Style, '%')";
                            //listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            break;
                    }
                }
                sql += "  GROUP BY p.productID order by p.prdName ";
                sql += "  ) AS Desp";

                if (srcText == "")
                {
                    sql += " limit 0";
                }
                return dbHelp.GetDataTable(sql, CommandType.Text, listParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataRow GetProductDetailForQuoation(DbHelper dbHelp, int prdID, StatusProductType prdType, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                DataTable dt = this.GetProductsForQuoation(dbHelp, ProductSearchFields.ProductID, prdID.ToString(), 0, prdType, lang, 0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return dt.Rows[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public DataTable GetProductDetailStyle(DbHelper dbHelp, string pStyle, StatusProductType prdType, string lang, int iCollection, string whs)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                DataTable dt = this.GetStyleSearchProduct(dbHelp, ProductSearchFields.ProductStyle, pStyle, whs, prdType, lang, iCollection);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    return dt.Rows[0];
                //}
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetProductDetailGroupbyColor(DbHelper dbHelp, string pStyle, StatusProductType prdType, string lang, int iCollection, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                DataTable dt = this.GetStyleSearchGroupbyColor(dbHelp, ProductSearchFields.ProductStyle, pStyle, whsCode, prdType, lang, iCollection);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    return dt.Rows[0];
                //}
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public DataTable GetStyleSearchProduct(DbHelper dbHelp, string srcField, string srcText, string whsCode, StatusProductType prdType, string lang, int iCollectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                var listParams = new List<MySqlParameter>();
                //We should show available quantity instead on head
                string sql = "SELECT * FROM  ( SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, IFNULL(pq.prdOhdQty,0) as prdOhdQty, p.prdEndUserSalesPrice AS EndUserSalesPrice, ";
                sql += " p.prdSalePricePerMinQty as Price,prdTaxCode,prdDiscount,p.prdDiscountType, pClothDesc.Style, pClothDesc.Collection AS CollectionID, pc.ShortName AS Collection ,pColor.ColorID ,pColor.Color{0} AS Color, pSize.Size{0} AS Size, p.prdEndUserSalesPrice, p.prdWebSalesPrice, p.prdFOBPrice, p.prdLandedPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
                sql += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
                sql += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection ";
                sql += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
                sql += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";

                if (prdType == StatusProductType.ServiceProduct)
                {
                    sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID  ";
                }
                else
                {
                    //sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
                    sql += " LEFT OUTER join prdquantity pq on pq.prdID=p.productID  ";
                    if (!string.IsNullOrEmpty(whsCode))
                    {
                        sql += " AND pq.prdWhsCode = @whsCode ";
                        listParams.Add(DbUtility.GetParameter("@whsCode", whsCode, MyDbType.String));
                    }
                }

                sql += " WHERE 1=1 AND ";
                listParams.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));
                sql += " p.prdType=@prdType AND p.prdIsActive=1 ";
                listParams.Add(DbUtility.GetParameter("@prdType", (int)prdType, MyDbType.Int));
                //if (compID > 0)
                //{
                //    sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
                //    listParams.Add(DbUtility.GetParameter("@WarehouseCompanyID", compID, MyDbType.Int));
                //}


                if (iCollectionID > 0)
                {
                    // sql += " AND p.CollectionID = @CollectionID";
                    sql += " AND pClothDesc.Collection = @CollectionID";
                    listParams.Add(DbUtility.GetParameter("@CollectionID", iCollectionID, MyDbType.Int));
                }

                if (!string.IsNullOrEmpty(srcText))
                {
                    int intParam = 0;
                    switch (srcField)
                    {
                        case ProductSearchFields.ProductID:
                            sql += " AND p.productID = @productID";
                            int.TryParse(srcText, out intParam);
                            listParams.Add(DbUtility.GetParameter("@productID", intParam, MyDbType.Int));
                            break;
                        case ProductSearchFields.ProductName:
                            sql += " AND p.prdName LIKE CONCAT('%', @prdName, '%')";
                            listParams.Add(DbUtility.GetParameter("@prdName", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductInternalID:
                            sql += " AND p.prdIntID = @prdIntID";
                            listParams.Add(DbUtility.GetParameter("@prdIntID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductExternalID:
                            sql += " AND p.prdExtID = @prdExtID";
                            listParams.Add(DbUtility.GetParameter("@prdExtID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductBarCode:
                            sql += " AND p.prdUPCCode = @prdUPCCode";
                            listParams.Add(DbUtility.GetParameter("@prdUPCCode", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductDescription:
                            sql += " AND (prdSmallDesc LIKE CONCAT('%', @srcData, '%')  OR prdLargeDesc LIKE CONCAT('%', @srcData, '%')";
                            listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ALPHA:
                            if (srcText != "All")
                            {
                                sql += "  And p.prdName LIKE CONCAT(@srcData, '%')";
                                listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            }
                            break;
                        case ProductSearchFields.CUSTOMER_FAV:
                            sql += "  And p.productID IN (SELECT oi.ordProductID FROM orderitems oi INNER JOIN orders o ON o.ordID = oi.ordID AND o.ordCustID = @ordCustID)";
                            listParams.Add(DbUtility.GetParameter("@ordCustID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.MY_FAV:
                            sql += "   And p.prdIsSpecial = 1";
                            break;
                        case ProductSearchFields.ProductStyle:
                            sql += " AND pClothDesc.Style = @Style ";
                            listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            //sql += " AND pClothDesc.Style LIKE CONCAT('%', @Style, '%')";
                            //listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            break;
                    }
                }

                //sql += "  GROUP BY p.prdName, pColor.Color{0}, pSize.Size{0} order by p.prdName ";
                sql += "  GROUP BY pColor.Color{0}, pSize.Size{0} order by p.prdName ";
                sql += "  ) AS Desp";
                sql = string.Format(sql, lang);
                return dbHelp.GetDataTable(sql, CommandType.Text, listParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public DataTable GetStyleSearchGroupbyColor(DbHelper dbHelp, string srcField, string srcText, string whsCode, StatusProductType prdType, string lang, int iCollectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                var listParams = new List<MySqlParameter>();
                //We should show available quantity instead on head
                string sql = "SELECT * FROM  ( SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, IFNULL(pq.prdOhdQty,0) as prdOhdQty, ";
                sql += " p.prdSalePricePerMinQty as Price,prdTaxCode,prdDiscount,p.prdDiscountType, pClothDesc.Style, pClothDesc.Collection  AS CollectionID, pc.ShortName AS Collection ,pColor.ColorID ,pColor.Color{0} AS Color, pSize.Size{0} AS Size, p.prdEndUserSalesPrice, p.prdWebSalesPrice, p.prdFOBPrice, p.prdLandedPrice  FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
                sql += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
                sql += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection ";
                sql += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
                sql += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";

                if (prdType == StatusProductType.ServiceProduct)
                {
                    sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID  ";
                }
                else
                {
                    //sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
                    sql += " LEFT OUTER join prdquantity pq on pq.prdID=p.productID  ";
                    if (!string.IsNullOrEmpty(whsCode))
                    {
                        sql += " AND pq.prdWhsCode = @whsCode ";
                        listParams.Add(DbUtility.GetParameter("@whsCode", whsCode, MyDbType.String));
                    }
                }

                sql += " WHERE 1=1 AND ";

                listParams.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));
                sql += " p.prdType=@prdType AND p.prdIsActive=1 ";
                listParams.Add(DbUtility.GetParameter("@prdType", (int)prdType, MyDbType.Int));
                //if (compID > 0)
                //{
                //    sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
                //    listParams.Add(DbUtility.GetParameter("@WarehouseCompanyID", compID, MyDbType.Int));
                //}



                if (iCollectionID > 0)
                {
                    //sql += " AND p.CollectionID = @CollectionID";
                    sql += " AND pClothDesc.Collection = @CollectionID";

                    listParams.Add(DbUtility.GetParameter("@CollectionID", iCollectionID, MyDbType.Int));
                }

                if (!string.IsNullOrEmpty(srcText))
                {
                    int intParam = 0;
                    switch (srcField)
                    {
                        case ProductSearchFields.ProductID:
                            sql += " AND p.productID = @productID";
                            int.TryParse(srcText, out intParam);
                            listParams.Add(DbUtility.GetParameter("@productID", intParam, MyDbType.Int));
                            break;
                        case ProductSearchFields.ProductName:
                            sql += " AND p.prdName LIKE CONCAT('%', @prdName, '%')";
                            listParams.Add(DbUtility.GetParameter("@prdName", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductInternalID:
                            sql += " AND p.prdIntID = @prdIntID";
                            listParams.Add(DbUtility.GetParameter("@prdIntID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductExternalID:
                            sql += " AND p.prdExtID = @prdExtID";
                            listParams.Add(DbUtility.GetParameter("@prdExtID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductBarCode:
                            sql += " AND p.prdUPCCode = @prdUPCCode";
                            listParams.Add(DbUtility.GetParameter("@prdUPCCode", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ProductDescription:
                            sql += " AND (prdSmallDesc LIKE CONCAT('%', @srcData, '%')  OR prdLargeDesc LIKE CONCAT('%', @srcData, '%')";
                            listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.ALPHA:
                            if (srcText != "All")
                            {
                                sql += "  And p.prdName LIKE CONCAT(@srcData, '%')";
                                listParams.Add(DbUtility.GetParameter("@srcData", srcText, MyDbType.String));
                            }
                            break;
                        case ProductSearchFields.CUSTOMER_FAV:
                            sql += "  And p.productID IN (SELECT oi.ordProductID FROM orderitems oi INNER JOIN orders o ON o.ordID = oi.ordID AND o.ordCustID = @ordCustID)";
                            listParams.Add(DbUtility.GetParameter("@ordCustID", srcText, MyDbType.String));
                            break;
                        case ProductSearchFields.MY_FAV:
                            sql += "   And p.prdIsSpecial = 1";
                            break;
                        case ProductSearchFields.ProductStyle:
                            //sql += " AND p.Style LIKE CONCAT('%', @Style, '%')";
                            sql += " AND pClothDesc.Style = @Style";
                            listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            //sql += " AND pClothDesc.Style LIKE CONCAT('%', @Style, '%')";
                            //listParams.Add(DbUtility.GetParameter("@Style", srcText, MyDbType.String));
                            break;
                    }
                }

                //sql += "  GROUP BY p.prdName, pColor.Color{0}, pSize.Size{0} order by p.prdName ";
                sql += "  GROUP BY pColor.Color{0} order by p.prdName ";
                sql += "  ) AS Desp";
                sql = string.Format(sql, lang);
                return dbHelp.GetDataTable(sql, CommandType.Text, listParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public DataTable GetProductColorSize(DbHelper dbHelp, int iProductID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                string sql = " SELECT  pc.Color{0} AS Color, Size{0}  AS Size FROM  ";
                sql += "   Products AS p ";
                sql += " INNER JOIN productClothDesc AS Pcd ON Pcd.ProductID = p.ProductID ";
                sql += "  INNER JOIN ProductSize AS ps ON ps.SizeID = Pcd.Size ";
                sql += "  INNER JOIN ProductColor AS pc ON pc.ColorID = Pcd.Color ";
                sql += "  where p.PRoductID=  " + iProductID;
                sql = string.Format(sql, Globals.CurrentAppLanguageCode);
                return dbHelp.GetDataTable(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdatePrdOnHandQty(DbHelper dbHelp, int productID, int qty, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlExists = "SELECT COUNT(*) FROM prdquantity WHERE prdID=@prdID and prdWhsCode = @prdWhsCode ";
            string sqlInsert = "insert into prdquantity (prdOhdQty,prdID,prdWhsCode) values(@prdOhdQty,@prdID,@prdWhsCode) ";
            string sqlUpdate = "update prdquantity  set prdOhdQty = @prdOhdQty where prdID = @prdID and  prdWhsCode = @prdWhsCode ";
            try
            {
                object check = dbHelp.GetValue(sqlExists, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String)
                });

                if (BusinessUtility.GetInt(check) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdOhdQty", qty, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdOhdQty", qty, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String)
                    });
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void UpdatePrdPrice(DbHelper dbHelp, int collectionID, string styleID, double salePricePerMinQty, double endUserSalePrice, double wholeSalePrice, double fobPrice, double landedPrice)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            StringBuilder sbUpdateQuery = new StringBuilder();
            sbUpdateQuery.Append(" UPDATE  ");

            sbUpdateQuery.Append("  Products prd  ");
            sbUpdateQuery.Append(" JOIN ProductClothDesc pClothDesc ON pClothDesc.ProductID = prd.ProductID ");

            sbUpdateQuery.Append("  SET prdSalePricePerMinQty = @prdSalePricePerMinQty, prdEndUserSalesPrice = @prdEndUserSalesPrice, prdWebSalesPrice = @prdWebSalesPrice, ");
            sbUpdateQuery.Append(" prdFOBPrice = @prdFOBPrice, prdLandedPrice =  @prdLandedPrice ");

            sbUpdateQuery.Append(" WHERE pClothDesc.Collection = @collectionID AND pClothDesc.Style = @style ");

            try
            {
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbUpdateQuery), System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("collectionID", collectionID, MyDbType.Int),
                    DbUtility.GetParameter("style", styleID, MyDbType.String),
                    DbUtility.GetParameter("prdSalePricePerMinQty", salePricePerMinQty, MyDbType.Double),
                    DbUtility.GetParameter("prdEndUserSalesPrice", endUserSalePrice, MyDbType.Double),
                    DbUtility.GetParameter("prdWebSalesPrice", wholeSalePrice, MyDbType.Double),
                    DbUtility.GetParameter("prdFOBPrice", fobPrice, MyDbType.Double),
                    DbUtility.GetParameter("prdLandedPrice", landedPrice, MyDbType.Double),
                    });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetProductIDOnUPCCode()
        {
            List<MySqlParameter> lstP = new List<MySqlParameter>();
            string sql = " Select   ProductID FROM Products WHERE prdUPCCode =  @PrdUPCCode"; // SELECT prdIsKit from products where ProductID=@ProductID
            lstP.Add(DbUtility.GetParameter("prdUPCCode", this.PrdUPCCode, typeof(string)));
            DbHelper dbHelp = new DbHelper();
            try
            {
                DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, lstP.ToArray());
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        return BusinessUtility.GetInt(dt.Rows[0]["ProductID"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean MoveProductQtyToWhrHouse(int productID, int qtyToMove, string fromWhsCode, string toWhsCode, int userID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            string sqlExistsToWhs = "SELECT COUNT(*) FROM prdquantity WHERE prdID=@prdID and prdWhsCode = @prdWhsCode ";
            string sqlInsertToWhs = "insert into prdquantity (prdOhdQty,prdID,prdWhsCode) values(@prdOhdQty,@prdID,@prdWhsCode) ";
            string sqlUpdateToWhs = "update prdquantity  set prdOhdQty = IFNULL(prdOhdQty, 0) + @prdOhdQty where prdID = @prdID and  prdWhsCode = @prdWhsCode ";

            string sqlUpdateFromWhs = "update prdquantity  set prdOhdQty = IFNULL(prdOhdQty, 0) -  @prdOhdQty where prdID = @prdID and  prdWhsCode = @prdWhsCode ";
            try
            {
                object check = dbTransactionHelper.GetValue(sqlExistsToWhs, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", toWhsCode, MyDbType.String)
                });

                if (BusinessUtility.GetInt(check) > 0)
                {
                    dbTransactionHelper.ExecuteNonQuery(sqlUpdateToWhs, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdOhdQty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", toWhsCode, MyDbType.String)
                    });

                    StringBuilder sbInsert = new StringBuilder();
                    sbInsert.Append(" INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ");
                    sbInsert.Append(" VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsert), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", toWhsCode, MyDbType.String),
                    DbUtility.GetParameter("UserID", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.MV), MyDbType.String),
                    DbUtility.GetParameter("Qty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.INC), MyDbType.String)
                    });

                }
                else
                {
                    dbTransactionHelper.ExecuteNonQuery(sqlInsertToWhs, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdOhdQty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", toWhsCode, MyDbType.String)
                    });

                    StringBuilder sbInsert = new StringBuilder();
                    sbInsert.Append(" INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ");
                    sbInsert.Append(" VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsert), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", toWhsCode, MyDbType.String),
                    DbUtility.GetParameter("UserID", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.MV), MyDbType.String),
                    DbUtility.GetParameter("Qty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.ORWT), MyDbType.String)
                    });
                }

                dbTransactionHelper.ExecuteNonQuery(sqlUpdateFromWhs, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdOhdQty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("prdWhsCode", fromWhsCode, MyDbType.String)
                    });

                StringBuilder sbInsert1 = new StringBuilder();
                sbInsert1.Append(" INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ");
                sbInsert1.Append(" VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsert1), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", fromWhsCode, MyDbType.String),
                    DbUtility.GetParameter("UserID", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.MV), MyDbType.String),
                    DbUtility.GetParameter("Qty", qtyToMove, MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.DIC), MyDbType.String)
                    });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        //public bool UpdatePublishedProduct(int userid)
        //{
        //    DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        //    dbTransactionHelper.BeginTransaction();
        //    try
        //    {
        //        string sql = " SELECT * FROM Products WHERE MasterID = @MasterID";
        //        DataTable dtProdcuts = dbTransactionHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
        //        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int )});

        //        foreach (DataRow dRowPmaster in dtProdcuts.Rows)
        //        {
        //            int iProductID = BusinessUtility.GetInt(dRowPmaster["productID"]);
        //            string sqlUpdate = "UPDATE products set prdIsKit=@prdIsKit,prdMinQtyPerSO=@prdMinQtyPerSO,prdWeight=@prdWeight,";
        //            sqlUpdate += "prdWeightPkg=@prdWeightPkg,prdInoclTerms=@prdInoclTerms,prdLastUpdatedByUserID=@prdLastUpdatedByUserID,prdLastUpdatedOn=@prdLastUpdatedOn,";
        //            sqlUpdate += "prdSalePricePerMinQty=@prdSalePricePerMinQty,prdEndUserSalesPrice=@prdEndUserSalesPrice,prdIsSpecial=@prdIsSpecial,";
        //            sqlUpdate += "prdDiscount=@prdDiscount,prdMinQtyPOTrig=@prdMinQtyPOTrig,prdAutoPO=@prdAutoPO,prdPOQty=@prdPOQty,prdIsActive=@prdIsActive,prdIsWeb=@prdIsWeb,";
        //            sqlUpdate += "prdComissionCode=@prdComissionCode,isPOSMenu=@isPOSMenu,prdWebSalesPrice=@prdWebSalesPrice,prdDiscountType=@prdDiscountType,prdHeight=@prdHeight,";
        //            sqlUpdate += "prdLength=@prdLength,prdHeightPkg=@prdHeightPkg,prdLengthPkg=@prdLengthPkg,prdWidth=@prdWidth,prdWidthPkg=@prdWidthPkg, ";
        //            sqlUpdate += "prdExtendedCategory=@prdExtendedCategory, prdAccomodationType=@prdAccomodationType, prdISGlutenFree=@prdISGlutenFree, prdIsVegetarian=@prdIsVegetarian, prdIsContainsNuts=@prdIsContainsNuts, prdIsCookedSushi=@prdIsCookedSushi, prdSpicyLevel=@prdSpicyLevel, prdFOBPrice = @prdFOBPrice, prdLandedPrice = @prdLandedPrice WHERE productID=@productID";

        //            DbHelper dbHelp = new DbHelper(true);
        //            List<MySqlParameter> pList = new List<MySqlParameter>();

        //            //pList.Add(DbUtility.GetParameter("prdIntID", this.PrdIntID, MyDbType.String));
        //            //pList.Add(DbUtility.GetParameter("prdExtID", this.PrdExtID, MyDbType.String));
        //            //pList.Add(DbUtility.GetParameter("prdType", this.PrdType, MyDbType.Int));
        //            //pList.Add(DbUtility.GetParameter("prdUPCCode", this.PrdUPCCode, MyDbType.String));
        //            //pList.Add(DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String));

        //            pList.Add(DbUtility.GetParameter("prdIsKit", this.PrdIsKit, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdMinQtyPerSO", this.PrdMinQtyPerSO, MyDbType.Int));
        //            pList.Add(DbUtility.GetParameter("prdWeight", this.PrdWeight, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdWeightPkg", this.PrdWeightPkg, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdInoclTerms", this.PrdInoclTerms, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdSalePricePerMinQty", this.PrdSalePricePerMinQty, MyDbType.Double));
        //            pList.Add(DbUtility.GetParameter("prdEndUserSalesPrice", this.PrdEndUserSalesPrice, MyDbType.Double));
        //            pList.Add(DbUtility.GetParameter("prdIsSpecial", this.PrdIsSpecial, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdDiscount", this.PrdDiscount, MyDbType.Int));
        //            pList.Add(DbUtility.GetParameter("prdMinQtyPOTrig", this.PrdMinQtyPOTrig, MyDbType.Int));
        //            pList.Add(DbUtility.GetParameter("prdAutoPO", this.PrdAutoPO, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdPOQty", this.PrdPOQty, MyDbType.Int));
        //            pList.Add(DbUtility.GetParameter("prdIsActive", this.PrdIsActive, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdIsWeb", this.PrdIsWeb, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdComissionCode", this.PrdComissionCode, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("isPOSMenu", this.IsPOSMenu, MyDbType.Boolean));
        //            //pList.Add(DbUtility.GetParameter("prdCategory", this.PrdCategory, MyDbType.Int));
        //            //pList.Add(DbUtility.GetParameter("prdSubcategory", this.PrdSubcategory, MyDbType.Int));
        //            pList.Add(DbUtility.GetParameter("prdWebSalesPrice", this.PrdWebSalesPrice, MyDbType.Double));
        //            pList.Add(DbUtility.GetParameter("prdDiscountType", this.PrdDiscountType, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdHeight", this.PrdHeight, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdLength", this.PrdLength, MyDbType.String));
        //            pList.Add(DbUtility.GetParameter("prdHeightPkg", this.PrdHeightPkg, MyDbType.String));//
        //            pList.Add(DbUtility.GetParameter("prdLengthPkg", this.PrdLengthPkg, MyDbType.String));//
        //            pList.Add(DbUtility.GetParameter("prdWidth", this.PrdWidth, MyDbType.String));//
        //            pList.Add(DbUtility.GetParameter("prdWidthPkg", this.PrdWidthPkg, MyDbType.String));

        //            pList.Add(DbUtility.GetParameter("PrdLastUpdatedByUserID", userid, MyDbType.Int));//
        //            pList.Add(DbUtility.GetParameter("PrdLastUpdatedOn", DateTime.Now, MyDbType.DateTime));//
        //            pList.Add(DbUtility.GetParameter("ProductID", iProductID, MyDbType.String));//            

        //            int? accommodationType = this.PrdAccommodationType > 0 ? (int?)this.PrdAccommodationType : null;
        //            pList.Add(DbUtility.GetIntParameter("prdAccomodationType", accommodationType));

        //            int? extcateg = this.PrdExtendedCategory > 0 ? (int?)this.PrdExtendedCategory : null;
        //            pList.Add(DbUtility.GetIntParameter("prdExtendedCategory", extcateg));

        //            pList.Add(DbUtility.GetParameter("prdISGlutenFree", this.PrdIsGlutenFree, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdIsVegetarian", this.PrdIsVegetarian, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdIsContainsNuts", this.PrdIsContainsNuts, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdIsCookedSushi", this.PrdIsCookedSushi, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdSpicyLevel", this.PrdSpicyLevel, MyDbType.Boolean));
        //            pList.Add(DbUtility.GetParameter("prdFOBPrice", this.PrdFOBPrice, MyDbType.Double));
        //            pList.Add(DbUtility.GetParameter("prdLandedPrice", this.PrdLandedPrice, MyDbType.Double));
        //            dbTransactionHelper.ExecuteNonQuery(sqlUpdate, System.Data.CommandType.Text, pList.ToArray());

        //        }

        //        dbTransactionHelper.CommitTransaction();
        //        return true;
        //    }
        //    catch
        //    {
        //        dbTransactionHelper.RollBackTransaction();
        //        throw;
        //    }
        //    finally
        //    {
        //        dbTransactionHelper.CloseDatabaseConnection();

        //    }
        //}

        #endregion



        public class ProductClothDesc
        {
            public int ProductID { get; set; }
            public string Style { get; set; }

            public int Size { get; set; }
            public string JupesPantalons { get; set; }
            public string Gauge { get; set; }
            public string Texture { get; set; }
            public string LongueurLength { get; set; }
            public string EpauleShoulder { get; set; }
            public string BusteChest { get; set; }
            public string Bicep { get; set; }
            public int Material { get; set; }
            public int Micron { get; set; }
            public int Collection { get; set; }
            public int Color { get; set; }

            public string Neckline { get; set; }
            public string Sleeve { get; set; }
            public string Silhouette { get; set; }
            public string ExtraCatg { get; set; }
            public string Trend { get; set; }
            public string KeyWord { get; set; }


            public string WebSiteCatg { get; set; }
            public string WebSiteSubCatg { get; set; }
            public string WebAssociatedStyle { get; set; }

            public int MasterID { get; set; }
            public int CollectionID { get; set; }
            public bool isPublish { get; set; }


            public string PrdName { get; set; }
            public string PrdEnDesc { get; set; }
            public string PrdFrDesc { get; set; }


            public Boolean InsertUpdateClothDesc()
            {
                DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                dbTransactionHelper.BeginTransaction();
                try
                {
                    string sqlInsert;


                    if (isPublish == false)
                    {
                        sqlInsert = @"SELECT * FROM ProductClothDesc WHERE productID = @productID   ";
                        MySqlParameter[] pParam = {                                            
                                        DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                     };
                        DataTable dt = dbTransactionHelper.GetDataTable(sqlInsert, System.Data.CommandType.Text, pParam);

                        if (dt.Rows.Count == 0)
                        {
                            #region InsertEventStatus
                            sqlInsert = @"INSERT INTO ProductClothDesc (productID, style, size, jupesPantalons,longueurLength, epauleShoulder, busteChest, bicep, material, micron, collection, color)
                            VALUES (@productID, @style, @size,  @jupesPantalons, @longueurLength, @epauleShoulder, @busteChest, @bicep, @material, @micron, @Collection, @color)"; // , @neckline, @sleeve, @silhouette , @ExtraCatg  , neckline, sleeve, silhouette , ExtraCatg texture,  @texture, //  gauge,   @gauge,
                            MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                        DbUtility.GetParameter("style", this.Style, MyDbType.String),
                                        //DbUtility.GetParameter("neckline", this.Neckline, MyDbType.Int),
                                        //DbUtility.GetParameter("sleeve", this.Sleeve, MyDbType.Int),
                                        //DbUtility.GetParameter("silhouette", this.Silhouette, MyDbType.Int),
                                        DbUtility.GetParameter("size", this.Size, MyDbType.Int),
                                        DbUtility.GetParameter("jupesPantalons", this.JupesPantalons, MyDbType.String),
                                        //DbUtility.GetParameter("gauge", this.Gauge, MyDbType.Int),
                                        //DbUtility.GetParameter("texture", this.Texture, MyDbType.Int),
                                        DbUtility.GetParameter("longueurLength", this.LongueurLength, MyDbType.String),
                                        DbUtility.GetParameter("epauleShoulder", this.EpauleShoulder, MyDbType.String),
                                        DbUtility.GetParameter("busteChest", this.BusteChest, MyDbType.String),
                                        DbUtility.GetParameter("bicep", this.Bicep, MyDbType.String),
                                        DbUtility.GetParameter("material", this.Material, MyDbType.Int),
                                        DbUtility.GetParameter("micron", this.Micron, MyDbType.Int),
                                        DbUtility.GetParameter("Collection", this.CollectionID, MyDbType.Int),
                                        DbUtility.GetParameter("color", this.Color, MyDbType.Int),
                                        //DbUtility.GetParameter("ExtraCatg", this.ExtraCatg, MyDbType.Int),
                                     };
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pInsert);


                            string[] sNickLine = this.Neckline.Split(',');
                            foreach (string item in sNickLine)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            string[] sGauge = this.Gauge.Split(',');
                            foreach (string item in sGauge)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            string[] sTexture = this.Texture.Split(',');
                            foreach (string item in sTexture)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO productAssociateTexture (ProductID, TextureID) VALUES (@ProductID, @TextureID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("TextureID", BusinessUtility.GetInt( item), MyDbType.Int)
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            string[] sExtraCatg = this.ExtraCatg.Split(',');
                            foreach (string item in sExtraCatg)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            string[] sSleeve = this.Sleeve.Split(',');
                            foreach (string item in sSleeve)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            string[] sSilhouette = this.Silhouette.Split(',');
                            foreach (string item in sSilhouette)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            string[] sTrend = this.Trend.Split(',');
                            foreach (string item in sTrend)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            string[] skeyWord = this.KeyWord.Split(',');
                            foreach (var item in skeyWord)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            string[] sWebSiteCatg = this.WebSiteCatg.Split(',');
                            foreach (string item in sWebSiteCatg)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 1, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            string[] sWebSiteSubCatg = this.WebSiteSubCatg.Split(',');
                            foreach (string item in sWebSiteSubCatg)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 8, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            string[] sAssociatedStyle = this.WebAssociatedStyle.Split(',');
                            foreach (string item in sAssociatedStyle)
                            {
                                if (item != "")
                                {
                                    string sqlInsertProdCatg = "INSERT INTO productAssociatedStyle (ProductID, MasterID, Style, CollectionID ) VALUES (@ProductID, @MasterID, @Style, @CollectionID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                                                DbUtility.GetParameter("Style", item, MyDbType.String),
                                                DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int),
                                                };
                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            #endregion
                        }
                        else
                        {
                            #region UpdateEventStatus
                            sqlInsert = @"Update ProductClothDesc SET  style = @style,  size = @size, jupesPantalons = @jupesPantalons, longueurLength = @longueurLength, 
                        epauleShoulder = @epauleShoulder, busteChest = @busteChest, bicep = @bicep, material = @material, micron = @micron, collection = @Collection, color = @color 
                            WHERE productID = @productID"; // neckline = @neckline, sleeve = @sleeve, silhouette = @silhouette, ExtraCatg = @ExtraCatg texture = @texture, gauge = @gauge,  

                            MySqlParameter[] pUpdate = {                                            
                                        DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                        DbUtility.GetParameter("style", this.Style, MyDbType.String),
                                        //DbUtility.GetParameter("neckline", this.Neckline, MyDbType.Int),
                                        //DbUtility.GetParameter("sleeve", this.Sleeve, MyDbType.Int),
                                        //DbUtility.GetParameter("silhouette", this.Silhouette, MyDbType.Int),
                                        DbUtility.GetParameter("size", this.Size, MyDbType.Int),
                                        DbUtility.GetParameter("jupesPantalons", this.JupesPantalons, MyDbType.String),
                                        //DbUtility.GetParameter("gauge", this.Gauge, MyDbType.Int),
                                        //DbUtility.GetParameter("texture", this.Texture, MyDbType.Int),
                                        DbUtility.GetParameter("longueurLength", this.LongueurLength, MyDbType.String),
                                        DbUtility.GetParameter("epauleShoulder", this.EpauleShoulder, MyDbType.String),
                                        DbUtility.GetParameter("busteChest", this.BusteChest, MyDbType.String),
                                        DbUtility.GetParameter("bicep", this.Bicep, MyDbType.String),
                                        DbUtility.GetParameter("material", this.Material, MyDbType.Int),
                                        DbUtility.GetParameter("micron", this.Micron, MyDbType.Int),
                                        DbUtility.GetParameter("Collection", this.CollectionID, MyDbType.Int),
                                        DbUtility.GetParameter("color", this.Color, MyDbType.Int),
                                        //DbUtility.GetParameter("ExtraCatg", this.ExtraCatg, MyDbType.Int),
                                     };




                            string sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });
                            string[] sNickLine = this.Neckline.Split(',');
                            foreach (string item in sNickLine)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });
                            string[] sGauge = this.Gauge.Split(',');
                            foreach (string item in sGauge)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }



                            sqlDelete = " DELETE FROM productAssociateTexture WHERE ProductID = @ProductID  ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int)
                        });
                            string[] sTexture = this.Texture.Split(',');
                            foreach (string item in sTexture)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO productAssociateTexture (ProductID, TextureID) VALUES (@ProductID, @TextureID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("TextureID", BusinessUtility.GetInt( item), MyDbType.Int)
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });
                            string[] sExtraCatg = this.ExtraCatg.Split(',');
                            foreach (string item in sExtraCatg)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });
                            string[] sSleeve = this.Sleeve.Split(',');
                            foreach (string item in sSleeve)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });
                            string[] sSilhouette = this.Silhouette.Split(',');
                            foreach (string item in sSilhouette)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                        });
                            string[] sTrend = this.Trend.Split(',');
                            foreach (string item in sTrend)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });
                            string[] skeyWord = this.KeyWord.Split(',');
                            foreach (var item in skeyWord)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                                                };
                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }


                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 1, MyDbType.Int),
                        });
                            string[] sWebSiteCatg = this.WebSiteCatg.Split(',');
                            foreach (string item in sWebSiteCatg)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 1, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 8, MyDbType.Int),
                        });
                            string[] sWebSiteSubCatg = this.WebSiteSubCatg.Split(',');
                            foreach (string item in sWebSiteSubCatg)
                            {
                                if (item != "" && BusinessUtility.GetInt(item) != 0)
                                {
                                    string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                    MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", BusinessUtility.GetInt( item), MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", 8, MyDbType.Int),
                                                };

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                }
                            }

                            if (BusinessUtility.GetString(this.WebAssociatedStyle) != "")
                            {
                                sqlDelete = " DELETE FROM productAssociatedStyle WHERE ProductID = @ProdID AND  MasterID = @MasterID AND CollectionID = @CollectionID ";
                                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int),
                        });
                                string[] sAssociatedStyle = this.WebAssociatedStyle.Split(',');
                                foreach (string item in sAssociatedStyle)
                                {
                                    if (item != "" && BusinessUtility.GetInt(item) != 0)
                                    {
                                        string sqlInsertProdCatg = "INSERT INTO productAssociatedStyle (ProductID, MasterID, Style, CollectionID ) VALUES (@ProductID, @MasterID, @Style, @CollectionID) ";
                                        MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                                                DbUtility.GetParameter("Style", item, MyDbType.String),
                                                DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int),
                                                };
                                        dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                    }
                                }
                            }


                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pUpdate);
                            #endregion
                        }

                    }
                    dbTransactionHelper.CommitTransaction();
                    return true;
                }
                catch
                {
                    dbTransactionHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dbTransactionHelper.CloseDatabaseConnection();
                }
            }


            public Boolean UpdateProduceDesc()
            {
                DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
                dbTransactionHelper.BeginTransaction();
                try
                {
                    string sqlDelete;

                    sqlDelete = " DELETE FROM prddescriptions WHERE id = @productID";
                    dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                                                DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                                                });
                    string sqlDescEn = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                    MySqlParameter[] p = { 
                                     DbUtility.GetParameter("id", this.ProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.EN, MyDbType.String),
                                     DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", this.PrdEnDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", this.PrdEnDesc, MyDbType.String)
                                            };
                    dbTransactionHelper.ExecuteNonQuery(sqlDescEn, System.Data.CommandType.Text, p);


                    string sqlDescFr = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                    MySqlParameter[] p1 = { 
                                     DbUtility.GetParameter("id", this.ProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.FR, MyDbType.String),
                                     DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", this.PrdFrDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", this.PrdFrDesc, MyDbType.String)
                                            };
                    dbTransactionHelper.ExecuteNonQuery(sqlDescFr, System.Data.CommandType.Text, p1);
                    dbTransactionHelper.CommitTransaction();
                    return true;
                }
                catch
                {
                    dbTransactionHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dbTransactionHelper.CloseDatabaseConnection();
                }
            }


            public void getClothDesc(DbHelper dbHelp, int userID)
            {
                bool mustClose = false;
                if (dbHelp == null)
                {
                    mustClose = true;
                    dbHelp = new DbHelper(true);
                }

                string sql = "SELECT * FROM productclothdesc WHERE productID=@productID";
                MySqlParameter[] p = { DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int) };
                //MySqlDataReader dr = null;
                try
                {
                    //dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, p);
                    //if (dr.Read())
                    DataTable dtDesc = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, p);
                    foreach (DataRow dr in dtDesc.Rows)
                    {
                        this.ProductID = BusinessUtility.GetInt(dr["ProductID"]);
                        this.Style = BusinessUtility.GetString(dr["style"]);
                        this.Size = BusinessUtility.GetInt(dr["size"]);
                        this.JupesPantalons = BusinessUtility.GetString(dr["jupesPantalons"]);
                        //this.Gauge = BusinessUtility.GetInt(dr["gauge"]);
                        //this.Texture = BusinessUtility.GetInt(dr["texture"]);
                        this.LongueurLength = BusinessUtility.GetString(dr["longueurLength"]);
                        this.EpauleShoulder = BusinessUtility.GetString(dr["epauleShoulder"]);
                        this.BusteChest = BusinessUtility.GetString(dr["busteChest"]);
                        this.Bicep = BusinessUtility.GetString(dr["bicep"]);
                        this.Material = BusinessUtility.GetInt(dr["material"]);
                        this.Micron = BusinessUtility.GetInt(dr["micron"]);
                        this.Collection = BusinessUtility.GetInt(dr["Collection"]);
                        this.Color = BusinessUtility.GetInt(dr["color"]);
                        //this.Neckline = BusinessUtility.GetInt(dr["neckline"]);
                        //this.Sleeve = BusinessUtility.GetInt(dr["sleeve"]);
                        //this.Silhouette = BusinessUtility.GetInt(dr["silhouette"]);
                        //this.ExtraCatg = BusinessUtility.GetInt(dr["ExtraCatg"]);



                        string sqlNickline = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtNickLine = dbHelp.GetDataTable(sqlNickline, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });
                        string sNickLine = "";
                        foreach (DataRow dRow in dtNickLine.Rows)
                        {
                            if (sNickLine != "")
                            {
                                sNickLine += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sNickLine += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlTexture = " Select * FROM productAssociateTexture WHERE ProductID = @ProductID  ";
                        DataTable dtTexture = dbHelp.GetDataTable(sqlTexture, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int)
                        });
                        string sTexture = "";
                        foreach (DataRow dRow in dtTexture.Rows)
                        {
                            if (sTexture != "")
                            {
                                sTexture += "," + BusinessUtility.GetString(dRow["TextureID"]);
                            }
                            else
                            {
                                sTexture += BusinessUtility.GetString(dRow["TextureID"]);
                            }
                        }


                        string sqlGauge = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtGauge = dbHelp.GetDataTable(sqlGauge, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });
                        string sGauge = "";
                        foreach (DataRow dRow in dtGauge.Rows)
                        {
                            if (sGauge != "")
                            {
                                sGauge += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sGauge += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }



                        string sqlExtraCatg = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtExtraCatg = dbHelp.GetDataTable(sqlExtraCatg, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });
                        string sExtraCatg = "";
                        foreach (DataRow dRow in dtExtraCatg.Rows)
                        {
                            if (sExtraCatg != "")
                            {
                                sExtraCatg += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sExtraCatg += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }



                        string sqlSleeve = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtSleeve = dbHelp.GetDataTable(sqlSleeve, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });
                        string sSleeve = "";
                        foreach (DataRow dRow in dtSleeve.Rows)
                        {
                            if (sSleeve != "")
                            {
                                sSleeve += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sSleeve += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlSilhouette = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtSilhouette = dbHelp.GetDataTable(sqlSilhouette, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });
                        string sSilhouette = "";
                        foreach (DataRow dRow in dtSilhouette.Rows)
                        {
                            if (sSilhouette != "")
                            {
                                sSilhouette += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sSilhouette += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlTrend = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtTrend = dbHelp.GetDataTable(sqlTrend, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                        });
                        string sTrend = "";
                        foreach (DataRow dRow in dtTrend.Rows)
                        {
                            if (sTrend != "")
                            {
                                sTrend += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sTrend += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlKeyWord = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtKeyWord = dbHelp.GetDataTable(sqlKeyWord, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });
                        string sKeyWord = "";
                        foreach (DataRow dRow in dtKeyWord.Rows)
                        {
                            if (sKeyWord != "")
                            {
                                sKeyWord += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sKeyWord += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }



                        string sqlWebSiteCatg = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtWebSiteCatg = dbHelp.GetDataTable(sqlWebSiteCatg, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 1, MyDbType.Int),
                        });
                        string sWebSiteCatg = "";
                        foreach (DataRow dRow in dtWebSiteCatg.Rows)
                        {
                            if (sWebSiteCatg != "")
                            {
                                sWebSiteCatg += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sWebSiteCatg += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlWebSiteSubCatg = " Select prodSysCatgDtlID FROM prodCatgSelVal WHERE ProdID = @ProdID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        DataTable dtWebSiteSubCatg = dbHelp.GetDataTable(sqlWebSiteSubCatg, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProdID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 8, MyDbType.Int),
                        });
                        string sWebSiteSubCatg = "";
                        foreach (DataRow dRow in dtWebSiteSubCatg.Rows)
                        {
                            if (sWebSiteSubCatg != "")
                            {
                                sWebSiteSubCatg += "," + BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                            else
                            {
                                sWebSiteSubCatg += BusinessUtility.GetString(dRow["prodSysCatgDtlID"]);
                            }
                        }


                        string sqlAssociateStyle = " Select Style FROM productAssociatedStyle WHERE productID = @productID AND  MasterID = @MasterID AND CollectionID = @CollectionID ";
                        DataTable dtAssociateStyle = dbHelp.GetDataTable(sqlAssociateStyle, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("productID", this.ProductID, MyDbType.Int),
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int),
                        });
                        string sWebAssociateStyle = "";
                        foreach (DataRow dRow in dtAssociateStyle.Rows)
                        {
                            if (sWebAssociateStyle != "")
                            {
                                sWebAssociateStyle += "," + BusinessUtility.GetString(dRow["Style"]);
                            }
                            else
                            {
                                sWebAssociateStyle += BusinessUtility.GetString(dRow["Style"]);
                            }
                        }



                        this.Neckline = sNickLine;
                        this.Gauge = sGauge;
                        this.Texture = sTexture;
                        this.ExtraCatg = sExtraCatg;
                        this.Sleeve = sSleeve;
                        this.Silhouette = sSilhouette;
                        this.Trend = sTrend;
                        this.KeyWord = sKeyWord;
                        this.WebSiteCatg = sWebSiteCatg;
                        this.WebSiteSubCatg = sWebSiteSubCatg;
                        this.WebAssociatedStyle = sWebAssociateStyle;
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    //if (dr != null && !dr.IsClosed)
                    //{
                    //    dr.Close();
                    //}
                    if (mustClose) dbHelp.CloseDatabaseConnection();
                }
            }

        }
    }
}
