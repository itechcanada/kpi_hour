﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductSize
    {
        /* Used For Cava PRoduct Master */

        public string CategoryName { get; set; }
        public string Size { get; set; }
        public int SizeID { get; set; }
        public string SizeFr { get; set; }
        public string SizeEn { get; set; }
        public int IsActive { get; set; }
        public string SearchKey { get; set; }
        public string SizeName { get; set; }
        public string IsActiveUrl { get; set; }
        public int Seq { get; set; }



        public Boolean SaveSize(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.SizeID > 0)
                {
                    string sql = " UPDATE ProductSize SET SizeFr = @SizeFr, SizeEn = @SizeEn, SizeActive = @SizeActive, SizeLastUpdBy = @SizeLastUpdBy, SizeLastUpdOn = @SizeLastUpdOn, Seq = @Seq WHERE SizeID = @SizeID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeFr", this.SizeFr, MyDbType.String),
                    DbUtility.GetParameter("SizeEn", this.SizeEn, MyDbType.String),
                    DbUtility.GetParameter("SizeActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("SizeID", this.SizeID, MyDbType.Int),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int),
                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO ProductSize (SizeFr, SizeEn, SizeActive, SizeLastUpdBy, SizeLastUpdOn, Seq) VALUES (@SizeFr, @SizeEn, @SizeActive, @SizeLastUpdBy, @SizeLastUpdOn, @Seq) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeFr", this.SizeFr, MyDbType.String),
                    DbUtility.GetParameter("SizeEn", this.SizeEn, MyDbType.String),
                    DbUtility.GetParameter("SizeActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int),
                });

                    this.SizeID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.SizeID > 0)
                {
                    string sql = " UPDATE ProductSize SET SizeActive = @SizeActive, SizeLastUpdBy = @SizeLastUpdBy, SizeLastUpdOn = @SizeLastUpdOn WHERE SizeID = @SizeID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("SizeLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("SizeID", this.SizeID, MyDbType.Int)
                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductSize> GetProductCatSize(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSize> lResult = new List<ProductSize>();
                string sql = " SELECT PS.Size{0} AS Size, PSC.SizeCatgName AS CategoryName  FROM productsizeCatg  PSC ";
                sql += "INNER JOIN ProductSize PS ON PS.SizeID = PSC.SizeID AND PS.SizeActive = 1 ";
                sql += " GROUP BY PSC.SizeCatgName, PS.Size{0} ORDER BY PSC.SizeCatgName ";
                sql = string.Format(sql, lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSize { CategoryName = BusinessUtility.GetString(dr["CategoryName"]), Size = BusinessUtility.GetString(dr["Size"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductSize> GetAllSizeList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSize> lResult = new List<ProductSize>();
                string sql = string.Format("SELECT SizeID AS SizeID, SizeFr, SizeEn, Size{0} as SizeName, SizeActive AS IsActive, CASE SizeActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, Seq  FROM ProductSize WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND Size{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.SizeID > 0)
                {
                    sql += "AND SizeID = " + this.SizeID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSize
                        {
                            SizeName = BusinessUtility.GetString(dr["SizeName"]),
                            SizeID = BusinessUtility.GetInt(dr["SizeID"]),
                            SizeEn = BusinessUtility.GetString(dr["SizeEn"]),
                            SizeFr = BusinessUtility.GetString(dr["SizeFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"]),
                            Seq = BusinessUtility.GetInt(dr["Seq"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddSizeCategoryInStyle(int iMasterID, string[] iCategorySizeID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                //foreach (var item in iColorID)
                //{
                string sqlDelete = "DELETE FROM PrdMstSizeCatg WHERE MasterID = @MasterID ";//AND ColorID = @ColorID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    //DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                //}


                foreach (var item in iCategorySizeID)
                {
                    string sql = " INSERT INTO PrdMstSizeCatg (MasterID, SizeCatgID) VALUES (@MasterID, @SizeCatgID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("SizeCatgID",BusinessUtility.GetString( item), MyDbType.String)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteSizeCategoryInStyle(int iMasterID, string sSizeCategoryID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM PrdMstSizeCatg WHERE MasterID = @MasterID AND SizeCatgID = @SizeCatgID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("SizeCatgID", sSizeCategoryID, MyDbType.String),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductSize> GetStyleSizeCategory(DbHelper dbHelp, int iMasterID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSize> lResult = new List<ProductSize>();
                string sql = string.Format("SELECT SizeCatgID AS CategoryName FROM PrdMstSizeCatg  WHERE MasterID = @MasterID ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSize { CategoryName = BusinessUtility.GetString(dr["CategoryName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductSize> GetSizeCatg(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSize> lResult = new List<ProductSize>();
                string sql = " SELECT  PSC.SizeCatgName AS CategoryName  FROM productsizeCatg  PSC ";
                sql += " INNER JOIN ProductSize PS ON PS.SizeID = PSC.SizeID AND PS.SizeActive = 1 ";
                sql += " GROUP BY PSC.SizeCatgName ";

                sql = string.Format(sql, lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSize { CategoryName = BusinessUtility.GetString(dr["CategoryName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductSize> GetSize(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSize> lResult = new List<ProductSize>();
                string sql = " Select SizeID, Size{0} AS SizeName from productsize WHERE SizeActive = 1 ";

                sql = string.Format(sql, lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSize { SizeID = BusinessUtility.GetInt(dr["SizeID"]), Size = BusinessUtility.GetString(dr["SizeName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

    }
}
