﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Collection
    {
        public string CollectionEn { get; set; }
        public string CollectionFr { get; set; }
        public string CollectionName { get; set; }
        public string ShortName { get; set; }
        public int CollectionID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }



        public Boolean Save(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.CollectionID > 0)
                {
                    string sqlUpdate = " UPDATE productcycle SET cycleFr = @cycleFr, cycleEn = @cycleEn, cycleActive = @cycleActive, cycleLastUpdBy = @cycleLastUpdBy, cycleLastUpdOn = @cycleLastUpdOn, shortName = @shortName WHERE cycleID = @collectionID  ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("cycleFr", this.CollectionFr, MyDbType.String),
                    DbUtility.GetParameter("cycleEn", this.CollectionEn, MyDbType.String),
                    DbUtility.GetParameter("cycleActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("shortName", ShortName, MyDbType.String),
                    DbUtility.GetParameter("collectionID", this.CollectionID, MyDbType.Int),
                    });
                }
                else
                {
                    string sql = " INSERT INTO productcycle (cycleFr, cycleEn, cycleActive, cycleLastCreatedBy, cycleLastCreatedOn, shortName) VALUES (@cycleFr, @cycleEn, @cycleActive, @cycleLastCreatedBy, @cycleLastCreatedOn, @shortName) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("cycleFr", this.CollectionFr, MyDbType.String),
                    DbUtility.GetParameter("cycleEn", this.CollectionEn, MyDbType.String),
                    DbUtility.GetParameter("cycleActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastCreatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("shortName", ShortName, MyDbType.String),
                    });
                }
                this.CollectionID = dbHelp.GetLastInsertID();

                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.CollectionID > 0)
                {
                    string sqlUpdate = " UPDATE productcycle SET  cycleActive = @cycleActive, cycleLastUpdBy = @cycleLastUpdBy, cycleLastUpdOn = @cycleLastUpdOn WHERE cycleID = @collectionID  ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("cycleActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("cycleLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("collectionID", this.CollectionID, MyDbType.Int),
                    });
                }

                //this.CollectionID = dbHelp.GetLastInsertID();

                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<Collection> GetCollectionList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<Collection> lResult = new List<Collection>();
                string sql = string.Format("SELECT cycleID AS CollectionID, cycle{0} as CollectionName, ShortName FROM productcycle WHERE cycleActive = 1 ORDER BY ShortName, cycleLastCreatedOn DESC ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new Collection { CollectionName = BusinessUtility.GetString(dr["CollectionName"]), CollectionID = BusinessUtility.GetInt(dr["CollectionID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public List<Collection> GetAllCollectionList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<Collection> lResult = new List<Collection>();
                string sql = string.Format("SELECT cycleID AS CollectionID, cycleFr, cycleEn, cycle{0} as CollectionName,cycleActive AS IsActive, CASE cycleActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl,  ShortName  FROM productcycle WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND cycle{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.CollectionID > 0)
                {
                    sql += "AND cycleID = " + this.CollectionID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new Collection
                        {
                            CollectionName = BusinessUtility.GetString(dr["CollectionName"]),
                            CollectionID = BusinessUtility.GetInt(dr["CollectionID"]),
                            CollectionEn = BusinessUtility.GetString(dr["cycleEn"]),
                            CollectionFr = BusinessUtility.GetString(dr["cycleFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            ShortName = BusinessUtility.GetString(dr["ShortName"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public List<Collection> GetCollection(DbHelper dbHelp, int iMasterID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<Collection> lResult = new List<Collection>();
                string sql = string.Format("SELECT cycleID AS CollectionID, cycleFr, cycleEn, cycleEn as CollectionName,cycleActive AS IsActive, CASE cycleActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, shortName AS ShortName FROM ProdMstCol AS pmc INNER JOIN productcycle AS pc ON pmc.collectionID = pc.cycleID  WHERE 1 = 1  ", lang);
                if (iMasterID > 0)
                {
                    sql += "AND pmc.MasterID = " + iMasterID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new Collection
                        {
                            CollectionName = BusinessUtility.GetString(dr["CollectionName"]),
                            CollectionID = BusinessUtility.GetInt(dr["CollectionID"]),
                            CollectionEn = BusinessUtility.GetString(dr["cycleEn"]),
                            CollectionFr = BusinessUtility.GetString(dr["cycleFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            ShortName = BusinessUtility.GetString(dr["ShortName"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public List<Collection> GetStyleCollection(DbHelper dbHelp, string sStyle, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<Collection> lResult = new List<Collection>();
                //string sql = "SELECT cycleID AS CollectionID, cycleFr, cycleEn, cycle{0} as CollectionName,cycleActive AS IsActive, CASE cycleActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, shortName AS ShortName FROM ProdMstCol AS pmc INNER JOIN productcycle AS pc ON pmc.collectionID = pc.cycleID  WHERE 1 = 1  ";
                //if (sStyle != "")
                //{
                //    //sql += " cycle{0} like '%" + sCollection + "%'  ";

                //    sql += " AND cycle{0} LIKE CONCAT('%', " + sStyle + ", '%') ";
                //}

                //string sql = " Select cycleID AS CollectionID, cycleFr, cycleEn, cycle{0} as CollectionName,cycleActive AS IsActive, CASE cycleActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, shortName AS ShortName from PrdMstStyle AS pSty ";
                //sql += " INNER JOIN ProdMstCol AS pColl ON pColl.MasterID = pSty.MasterID  ";
                //sql += " INNER JOIN productcycle  AS pc ON pc.CycleID = pColl.CollectionID ";
                //sql += "  WHERE 1=1  ";
                //if (sStyle != "")
                //{
                //    sql += " AND  pSty.Style LIKE CONCAT('%', '" + sStyle + "', '%') ";
                //}
                //sql += " ORDER BY pc.CycleLastCreatedOn Desc ";


                string sql = " Select DISTINCT PsDsc.Collection AS CollectionID, cycleFr, cycleEn, cycle{0} as CollectionName,cycleActive AS IsActive, CASE cycleActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, shortName AS ShortName  ";
                //sql += " from PrdMstStyle AS pSty ";
                //sql += " INNER JOIN ProdMstCol AS pColl ON pColl.MasterID = pSty.MasterID  ";
                sql += " from ProductClothDesc AS PsDsc ";
                sql += " INNER JOIN productcycle  AS pc ON pc.CycleID = PsDsc.Collection ";
                sql += "  WHERE 1=1  ";
                if (sStyle != "")
                {
                    sql += " AND  PsDsc.Style LIKE CONCAT('%', '" + sStyle + "', '%') ";
                }
                sql += " ORDER BY pc.CycleLastCreatedOn Desc ";


                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new Collection
                        {
                            CollectionName = BusinessUtility.GetString(dr["CollectionName"]),
                            CollectionID = BusinessUtility.GetInt(dr["CollectionID"]),
                            CollectionEn = BusinessUtility.GetString(dr["cycleEn"]),
                            CollectionFr = BusinessUtility.GetString(dr["cycleFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            ShortName = BusinessUtility.GetString(dr["ShortName"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
