﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductPublish
    {
        public int MasterID { get; set; }

        public Boolean Save(int userID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                int iMasterID = this.MasterID;
                int iCollectionID;
                string sStyleName;
                int iMaterialID;
                int iTexture;
                int iColorID;
                int iSizeID;


                string sSqlCollection = " SELECT * FROM  ProdMstCol WHERE MasterID =  " + this.MasterID;
                DataTable dtCollection = dbTransactionHelper.GetDataTable(sSqlCollection, CommandType.Text, null);

                string sSqlStyle = " SELECT * FROM PrdMstStyle WHERE MasterID =  " + this.MasterID;
                DataTable dtStyle = dbTransactionHelper.GetDataTable(sSqlStyle, CommandType.Text, null);


                string sSqlMaterial = " SELECT * FROM PrdMstMaterial WHERE MasterID =  " + this.MasterID;
                DataTable dtMaterial = dbTransactionHelper.GetDataTable(sSqlMaterial, CommandType.Text, null);

                string sSqlTexture = " SELECT * FROM PrdMstTexture WHERE MasterID =  " + this.MasterID;
                DataTable dtTexture = dbTransactionHelper.GetDataTable(sSqlTexture, CommandType.Text, null);

                string sSqlColor = " SELECT * FROM PrdMstColor WHERE MasterID =  " + this.MasterID;
                DataTable dtColor = dbTransactionHelper.GetDataTable(sSqlColor, CommandType.Text, null);

                string sSqlSizeCatg = " SELECT pSize.SizeID, COALESCE(psz.Seq,0) AS Seq  FROM PrdMstSizeCatg AS MstSize ";
                sSqlSizeCatg += " INNER JOIN productsizecatg AS pSize ON pSize.SizeCatgName = MstSize.SizeCatgID  ";
                sSqlSizeCatg += " INNER JOIN ProductSize AS psz ON psz.SizeID = pSize.SizeID ";
                sSqlSizeCatg += " WHERE MstSize.MasterID =  " + this.MasterID;
                sSqlSizeCatg += " ORDER BY Seq ";


                DataTable dtSizeCatg = dbTransactionHelper.GetDataTable(sSqlSizeCatg, CommandType.Text, null);


                ProductCategory objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 1;
                var lstProductCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 2;
                var lstProductNickLine = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 9;
                var lstProductGauge = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 3;
                var lstProductExtraCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 4;
                var lstProductSleeve = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 5;
                var lstProductSilhouette = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 6;
                var lstProductTrend = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 7;
                var lstProductkeyWord = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);


                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 8;
                var lstProductWebSiteSubCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                ProductMasterDetail objPMDetail = new ProductMasterDetail();
                objPMDetail.MasterID = this.MasterID;
                objPMDetail.PopulateObject(null, BusinessUtility.GetInt(this.MasterID));
                int i = 0;
                foreach (DataRow drCollection in dtCollection.Rows)
                {
                    iCollectionID = BusinessUtility.GetInt(drCollection["CollectionID"]);

                    foreach (DataRow drStyle in dtStyle.Rows)
                    {
                        sStyleName = BusinessUtility.GetString(drStyle["Style"]);

                        foreach (DataRow drMaterial in dtMaterial.Rows)
                        {
                            iMaterialID = BusinessUtility.GetInt(drMaterial["MaterialID"]);

                            foreach (DataRow drColor in dtColor.Rows)
                            {
                                iColorID = BusinessUtility.GetInt(drColor["ColorID"]);
                                string sLength = "";
                                string sChestLength = "";
                                string sSoldLength = "";
                                string sBicepLength = "";
                                string sPrdWeight = "";


                                sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartLength));
                                sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartChest));
                                sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartShoulder));
                                sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartBicep));
                                sPrdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdWeight));


                                int iRowCnt = 0;
                                foreach (DataRow drSize in dtSizeCatg.Rows)
                                {
                                    iSizeID = BusinessUtility.GetInt(drSize["SizeID"]);

                                    //string sBarCode = iCollectionID.ToString("00") + this.MasterID.ToString("00") + iMaterialID.ToString("00") + iTexture.ToString("00") + iColorID.ToString("00") + iSizeID.ToString("00");// +BusinessUtility.GetString(BusinessUtility.GenerateRandomNumber(5)); 
                                    string sBarCode = iCollectionID.ToString("00") + this.MasterID.ToString("0000") + iMaterialID.ToString("00") + iColorID.ToString("0000") + iSizeID.ToString("00");

                                    sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementLength) * iRowCnt));
                                    sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sChestLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementChest) * iRowCnt));
                                    sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sSoldLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementShoulder) * iRowCnt));
                                    sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sBicepLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementBicep) * iRowCnt));
                                    sPrdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(sPrdWeight) + (BusinessUtility.GetDouble(objPMDetail.ProdWeightIncrement) * iRowCnt));
                                    iRowCnt = +1;
                                    string sqlInsertProduct = " INSERT INTO Products (MasterID, prdName, prdUPCCode, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIntID, prdWebSalesPrice, prdWeight, ";
                                    sqlInsertProduct += " prdLength, prdWeightPkg, prdLengthPkg, prdWidthPkg, prdHeightPkg,  "; //prdStartLength, prdIncrementLength, prdStartChest, prdIncrementChest, prdStartShoulder,
                                    sqlInsertProduct += "  PrdMinQtyPOTrig, prdPOQty, prdAutoPO, prdIsSpecial, prdIsKit, isPOSMenu, prdIsWeb, prdIsActive,  "; //prdIncrementShoulder, prdStartBicep, prdIncrementBicep,
                                    sqlInsertProduct += " prdInoclTerms, prdCreatedUserID, prdCreatedOn, prdType, CollectionID, Style, MatID, ColID, SizeID, prdFOBPrice, prdLandedPrice)   ";//TextureID, 
                                    sqlInsertProduct += " VALUES ( @MasterID, @prdName, @prdUPCCode, @prdSalePricePerMinQty, @prdEndUserSalesPrice, @prdIntID, @prdWebSalesPrice, @prdWeight, @prdLength,  ";
                                    sqlInsertProduct += " @prdWeightPkg, @prdLengthPkg, @prdWidthPkg, @prdHeightPkg,   "; //@prdStartLength, @prdIncrementLength, @prdStartChest, @prdIncrementChest, @prdStartShoulder,
                                    sqlInsertProduct += " @prdMinQtyPO, @prdQtyPO, @prdAutoPO, @prdIsSpecial, @prdIsKit, @isPOSMenu, @prdIsWeb,  "; // @prdIncrementShoulder, @prdStartBicep, @prdIncrementBicep,
                                    sqlInsertProduct += " @prdIsActive, @prdNotes, @prdCreatedUserID, @prdCreatedOn, 1, @CollectionID, @Style, @MatID, @ColID, @SizeID,  @prdFOBPrice, @prdLandedPrice )  ";//@TextureID,
                                    sqlInsertProduct += " ";

                                    dbTransactionHelper.ExecuteNonQuery(sqlInsertProduct, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdName", objPMDetail.PrdName, MyDbType.String ),
                                            DbUtility.GetParameter("PrdUPCCode", sBarCode, MyDbType.String ),
                                            DbUtility.GetParameter("PrdSalePricePerMinQty",  objPMDetail.PrdSalePricePerMinQty, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdEndUserSalesPrice",  objPMDetail.PrdEndUserSalesPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdIntID", sBarCode, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWebSalesPrice", objPMDetail.PrdWebSalesPrice, MyDbType.Double ),
                                            //DbUtility.GetParameter("PrdWeight", objPMDetail.PrdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdWeight", sPrdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdLength", objPMDetail.PrdLength, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWeightPkg", objPMDetail.PrdWeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdLengthPkg", objPMDetail.PrdLengthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWidthPkg", objPMDetail.PrdWidthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdHeightPkg", objPMDetail.PrdHeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdMinQtyPO", objPMDetail.PrdMinQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdQtyPO", objPMDetail.PrdQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdAutoPO", objPMDetail.PrdAutoPO, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsSpecial", objPMDetail.PrdIsSpecial, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsKit", objPMDetail.PrdIsKit, MyDbType.Boolean ),
                                            DbUtility.GetParameter("isPOSMenu", objPMDetail.IsPOSMenu, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsWeb", objPMDetail.PrdIsWeb, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsActive", objPMDetail.PrdIsActive, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdNotes", objPMDetail.PrdNotes, MyDbType.String ),
                                            DbUtility.GetParameter("PrdCreatedUserID", userID, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdCreatedOn", DateTime.Now, MyDbType.DateTime ),
                                            DbUtility.GetParameter("CollectionID", iCollectionID, MyDbType.Int ),
                                            DbUtility.GetParameter("Style", sStyleName, MyDbType.String ),
                                            DbUtility.GetParameter("MatID", iMaterialID, MyDbType.Int ),
                                            DbUtility.GetParameter("ColID", iColorID, MyDbType.Int ),
                                            DbUtility.GetParameter("SizeID", iSizeID, MyDbType.Int ),
                                            //DbUtility.GetParameter("TextureID", iTexture, MyDbType.Int ),
                                            DbUtility.GetParameter("prdFOBPrice",  objPMDetail.PrdFOBPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("prdLandedPrice",  objPMDetail.PrdLandedPrice, MyDbType.Double ),
                                        });


                                    int iProductID = dbTransactionHelper.GetLastInsertID();
                                    if (iProductID > 0)
                                    {
                                        foreach (var item in lstProductCatg)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        foreach (var item in lstProductNickLine)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }


                                        foreach (var item in lstProductGauge)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        foreach (var item in lstProductExtraCatg)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        foreach (var item in lstProductSleeve)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        foreach (var item in lstProductSilhouette)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }


                                        foreach (var item in lstProductTrend)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        foreach (var item in lstProductkeyWord)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }


                                        foreach (var item in lstProductWebSiteSubCatg)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }


                                        ProductStyle objProductStyle = new ProductStyle();
                                        var lstAssociatedMasterStyle = objProductStyle.GetProductMasterAssociatedStyle(null, this.MasterID, iCollectionID);
                                        foreach (var item in lstAssociatedMasterStyle)
                                        {
                                            string sqlInsertProdCatg = "INSERT INTO productAssociatedStyle (ProductID, MasterID, Style, CollectionID ) VALUES (@ProductID, @MasterID, @Style, @CollectionID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("MasterID", item.MasterID, MyDbType.Int),
                                                DbUtility.GetParameter("Style", item.StyleName, MyDbType.String),
                                                DbUtility.GetParameter("CollectionID", item.CollectionID, MyDbType.Int),
                                                };
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                        string sqlDesc = @"INSERT INTO ProductClothDesc (productID, style,  size, longueurLength, epauleShoulder, busteChest, bicep, material, collection, color) 
                                        VALUES (@productID, @style,  @size,  @longueurLength, @epauleShoulder, @busteChest, @bicep, @material, @Collection, @color)"; // @neckline, @sleeve, @silhouette, , @ExtraCatg  // neckline, sleeve, silhouette, , ExtraCatg texture, @texture, , Gauge , @Gauge
                                        MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                        DbUtility.GetParameter("style", sStyleName, MyDbType.String),
                                        DbUtility.GetParameter("size", iSizeID, MyDbType.Int),
                                        //DbUtility.GetParameter("texture", iTexture, MyDbType.Int),
                                        DbUtility.GetParameter("longueurLength", sLength, MyDbType.String),
                                        DbUtility.GetParameter("epauleShoulder", sSoldLength, MyDbType.String),
                                        DbUtility.GetParameter("busteChest", sChestLength, MyDbType.String),
                                        DbUtility.GetParameter("bicep", sBicepLength, MyDbType.String),
                                        DbUtility.GetParameter("material", iMaterialID, MyDbType.Int),
                                        DbUtility.GetParameter("Collection", iCollectionID, MyDbType.Int),
                                        DbUtility.GetParameter("color", iColorID, MyDbType.Int),
                                        //DbUtility.GetParameter("Gauge", objPMDetail.PrdGauge, MyDbType.Int),
                                        };
                                        dbTransactionHelper.ExecuteNonQuery(sqlDesc, System.Data.CommandType.Text, pInsert);

                                        string sqlDescEn = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                                        MySqlParameter[] p = { 
                                     DbUtility.GetParameter("id", iProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.EN, MyDbType.String),
                                     DbUtility.GetParameter("prdName", objPMDetail.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", objPMDetail.PrdEnDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", objPMDetail.PrdEnDesc, MyDbType.String)
                                            };

                                        dbTransactionHelper.ExecuteNonQuery(sqlDescEn, System.Data.CommandType.Text, p);


                                        string sqlDescFr = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                                        MySqlParameter[] p1 = { 
                                     DbUtility.GetParameter("id", iProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.FR, MyDbType.String),
                                     DbUtility.GetParameter("prdName", objPMDetail.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", objPMDetail.PrdFrDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", objPMDetail.PrdFrDesc, MyDbType.String)
                                            };

                                        dbTransactionHelper.ExecuteNonQuery(sqlDescFr, System.Data.CommandType.Text, p1);


                                        foreach (DataRow drTexture in dtTexture.Rows)
                                        {
                                            iTexture = BusinessUtility.GetInt(drTexture["TextureID"]);
                                            string sqlInsertProdCatg = "INSERT INTO productAssociateTexture (ProductID, TextureID) VALUES (@ProductID, @TextureID) ";
                                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("TextureID", iTexture, MyDbType.Int),
                                                };
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                                        }

                                    }
                                    i += 1;

                                }
                            }

                        }
                    }
                }

                string sqlupdate = " UPDATE productMasterHdr SET MasterPublished = 1 WHERE MasterID = @MasterID ";
                dbTransactionHelper.ExecuteNonQuery(sqlupdate, CommandType.Text, new MySqlParameter[]{
                DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int)});


                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

    }
}
