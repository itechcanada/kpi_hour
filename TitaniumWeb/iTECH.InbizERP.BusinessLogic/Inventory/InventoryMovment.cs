﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InventoryMovment
    {
        public Boolean AddMovment(string whsCode, int userID, int productID, string transactionSourceID, int qty, string updateType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                StringBuilder sbInsert = new StringBuilder();
                sbInsert.Append(" INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ");
                sbInsert.Append(" VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ");
                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsert), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", whsCode, MyDbType.String),
                    DbUtility.GetParameter("UserID", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", transactionSourceID, MyDbType.String),
                    DbUtility.GetParameter("Qty", qty, MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",updateType, MyDbType.String)
                    });
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }
    }
}
