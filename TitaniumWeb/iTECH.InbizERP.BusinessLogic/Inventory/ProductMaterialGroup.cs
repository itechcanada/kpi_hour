﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductMaterialGroup
    {
        /* Used For Cava PRoduct Master */

        public string MaterialEn { get; set; }
        public string MaterialFr { get; set; }
        public string MaterialName { get; set; }
        public int MaterialID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string ShortName { get; set; }
        public string MaterialGroupNameEn { get; set; }
        public string MaterialGroupNameFr { get; set; }
        public int MaterialGroupID { get; set; }
        public int MaterialGroupIsActive { get; set; }
        public string MaterialGroupName { get; set; }


        public Boolean SaveMaterialGroup(DbHelper dbHelp, int userID, int iMaterialGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iMaterialGroupID > 0)
                {
                    string sql = " UPDATE prdMaterialGroupHdr SET MaterialGroupFr = @MaterialGroupFr, MaterialGroupEn = @MaterialGroupEn, isActive = @MaterialGroupActive, lastUpdatedby = @MaterialLastUpdBy, lastUpdatedOn = @MaterialLastUpdOn  WHERE MaterialGroupHdrID = @MaterialGroupHdrID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupFr", this.MaterialGroupNameFr, MyDbType.String),
                    DbUtility.GetParameter("MaterialGroupEn", this.MaterialGroupNameEn, MyDbType.String),
                    DbUtility.GetParameter("MaterialGroupHdrID", iMaterialGroupID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("MaterialGroupActive", this.MaterialGroupIsActive, MyDbType.Int),
                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO prdMaterialGroupHdr (MaterialGroupFr, MaterialGroupEn, createdBy, CreatedOn, isActive) VALUES (@MaterialGroupFr, @MaterialGroupEn,  @createdBy, @CreatedOn, @MaterialGroupActive) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupFr", this.MaterialGroupNameFr, MyDbType.String),
                    DbUtility.GetParameter("MaterialGroupEn", this.MaterialGroupNameEn, MyDbType.String),
                    DbUtility.GetParameter("createdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("MaterialGroupActive", this.MaterialGroupIsActive, MyDbType.Int),
                });
                    this.MaterialGroupID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteMaterialGroup(DbHelper dbHelp, int userID, int iMaterialGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iMaterialGroupID > 0)
                {
                    string sql = " UPDATE prdMaterialGroupHdr SET isActive = @MaterialGroupActive , lastUpdatedby = @MaterialLastUpdBy, lastUpdatedOn = @MaterialLastUpdOn WHERE MaterialGroupHdrID = @MaterialGroupHdrID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("MaterialLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("MaterialGroupHdrID", iMaterialGroupID, MyDbType.Int)
                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public List<ProductMaterialGroup> GetMaterialGroupList(DbHelper dbHelp, string lang)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        List<ProductMaterialGroup> lResult = new List<ProductMaterialGroup>();
        //        string sql = string.Format("SELECT MaterialGroupHdrID AS MaterialGroupID, MaterialGroup{0} as MaterialGroupName FROM prdMaterialGroupHdr WHERE MaterialActive = 1 ", lang);

        //        using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
        //        {
        //            while (dr.Read())
        //            {
        //                lResult.Add(new ProductMaterialGroup { MaterialGroupName = BusinessUtility.GetString(dr["MaterialGroupName"]), MaterialGroupID = BusinessUtility.GetInt(dr["MaterialGroupID"]) });
        //            }
        //            return lResult;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public List<ProductMaterialGroup> GetAllMaterialGroupList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMaterialGroup> lResult = new List<ProductMaterialGroup>();
                string sql = string.Format("SELECT MaterialGroupHdrID AS MaterialGroupID, MaterialGroupFr, MaterialGroupEn, MaterialGroup{0} as MaterialGroupName, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdMaterialGroupHdr WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND MaterialGroup{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.MaterialGroupID > 0)
                {
                    sql += "AND MaterialGroupHdrID = " + this.MaterialGroupID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMaterialGroup
                        {
                            MaterialGroupName = BusinessUtility.GetString(dr["MaterialGroupName"]),
                            MaterialGroupID = BusinessUtility.GetInt(dr["MaterialGroupID"]),
                            MaterialGroupNameEn = BusinessUtility.GetString(dr["MaterialGroupEn"]),
                            MaterialGroupNameFr = BusinessUtility.GetString(dr["MaterialGroupFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            MaterialGroupIsActive = BusinessUtility.GetInt(dr["IsActive"]),
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddMaterialInGroup(int iMaterialGroupID, int[] iMaterialID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                //foreach (var item in iMaterialID)
                //{
                string sqlDelete = "DELETE FROM prdMaterialGroupDtl WHERE MaterialGroupHdrID = @MaterialGroupHdrID ";//AND MaterialID = @MaterialID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupHdrID", iMaterialGroupID, MyDbType.Int),
                    //DbUtility.GetParameter("MaterialID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                //}


                foreach (var item in iMaterialID)
                {
                    string sql = " INSERT INTO prdMaterialGroupDtl (MaterialGroupHdrID, MaterialID) VALUES (@MaterialGroupHdrID, @MaterialID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupHdrID", iMaterialGroupID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public Boolean DeleteMaterialInGroup(int iMaterialGroupID, int iMaterialID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM prdMaterialGroupDtl WHERE MaterialGroupHdrID = @MaterialGroupHdrID AND MaterialID = @MaterialID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupHdrID", iMaterialGroupID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialID", iMaterialID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductMaterialGroup> GetGroupMaterial(DbHelper dbHelp, int iMaterialGroupID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMaterialGroup> lResult = new List<ProductMaterialGroup>();
                string sql = string.Format("SELECT PM.MatID AS MaterialID, PM.Mat{0} as MaterialName, PM.ShortName FROM ProductMaterial PM INNER JOIN prdMaterialGroupDtl PMsT ON PMsT.MaterialID = PM.MatID WHERE PMsT.MaterialGroupHdrID = @MaterialGroupID AND PM.MatActive = 1 ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MaterialGroupID", iMaterialGroupID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMaterialGroup { MaterialName = BusinessUtility.GetString(dr["MaterialName"]), MaterialID = BusinessUtility.GetInt(dr["MaterialID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public string GetMaterialGroupSql(string lang)
        {
            string sql = string.Format("SELECT MaterialGroupHdrID AS MaterialGroupID, MaterialGroupFr, MaterialGroupEn, MaterialGroup{0} as MaterialGroupName, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdMaterialGroupHdr WHERE 1=1  ", lang);
            if (BusinessUtility.GetString(this.SearchKey) != "")
            {
                sql += string.Format(" AND MaterialGroup{0} like '%" + this.SearchKey + "%' ", lang);
            }
            if (this.MaterialGroupID > 0)
            {
                sql += "AND MaterialGroupID = " + this.MaterialGroupID;
            }
            return sql;
        }
    }
}
