﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductMaterial
    {
        public string MaterialEn { get; set; }
        public string MaterialFr { get; set; }
        public string MaterialName { get; set; }
        public int MaterialID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string MaterialShortName { get; set; }


        public Boolean Save(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.MaterialID > 0)
                {
                    string sql = " UPDATE productmaterial SET MatFr = @MatFr, MatEn = @MatEn, MatActive = @MatActive, MatLastUpdBy = @MatLastUpdBy, MatLastUpdOn = @MatLastUpdOn, ShortName = @ShortName WHERE MatID = @MatID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MatFr", this.MaterialFr, MyDbType.String),
                    DbUtility.GetParameter("MatEn", this.MaterialEn, MyDbType.String),
                    DbUtility.GetParameter("MatActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("MatID", this.MaterialID, MyDbType.Int),
                    DbUtility.GetParameter("ShortName", this.MaterialShortName, MyDbType.String),

                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO productmaterial (MatFr, MatEn, MatActive, MatLastUpdBy, MatLastUpdOn, ShortName) VALUES (@MatFr, @MatEn, @MatActive, @MatLastUpdBy, @MatLastUpdOn, @ShortName) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MatFr", this.MaterialFr, MyDbType.String),
                    DbUtility.GetParameter("MatEn", this.MaterialEn, MyDbType.String),
                    DbUtility.GetParameter("MatActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ShortName", this.MaterialShortName, MyDbType.String),
                });
                    
                }

                this.MaterialID = dbHelp.GetLastInsertID();

                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.MaterialID > 0)
                {
                    string sql = " UPDATE productmaterial SET MatActive = @MatActive, MatLastUpdBy = @MatLastUpdBy, MatLastUpdOn = @MatLastUpdOn WHERE MatID = @MatID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MatActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MatLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("MatID", this.MaterialID, MyDbType.Int)

                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductMaterial> GetMaterialList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMaterial> lResult = new List<ProductMaterial>();
                string sql = string.Format("SELECT MatID AS MaterialID, Mat{0} as MaterialName, ShortName FROM productmaterial WHERE MatActive = 1 ORDER BY Mat{0} ", lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMaterial { MaterialName = BusinessUtility.GetString(dr["MaterialName"]), MaterialID = BusinessUtility.GetInt(dr["MaterialID"]), MaterialShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductMaterial> GetAllMaterialList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMaterial> lResult = new List<ProductMaterial>();
                string sql = string.Format("SELECT MatID AS MaterialID, MatFr, MatEn, Mat{0} as MaterialName,MatActive AS IsActive, CASE MatActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, ShortName  FROM productmaterial WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND Mat{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.MaterialID > 0)
                {
                    sql += "AND MatID = " + this.MaterialID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMaterial
                        {
                            MaterialName = BusinessUtility.GetString(dr["MaterialName"]),
                            MaterialID = BusinessUtility.GetInt(dr["MaterialID"]),
                            MaterialEn = BusinessUtility.GetString(dr["MatEn"]),
                            MaterialFr = BusinessUtility.GetString(dr["MatFr"]),
                            MaterialShortName = BusinessUtility.GetString(dr["ShortName"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean AddMaterialInStyle(int iMasterID, int[] iMaterialID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                //foreach (var item in iMaterialID)
                //{
                string sqlDelete = "DELETE FROM PrdMstMaterial WHERE MasterID = @MasterID ";//AND MaterialID = @MaterialID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    //DbUtility.GetParameter("MaterialID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                //}


                foreach (var item in iMaterialID)
                {
                    string sql = " INSERT INTO PrdMstMaterial (MasterID, MaterialID) VALUES (@MasterID, @MaterialID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteMaterialInStyle(int iMasterID, int iMaterialID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM PrdMstMaterial WHERE MasterID = @MasterID AND MaterialID = @MaterialID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("MaterialID", iMaterialID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductMaterial> GetStyleMaterial(DbHelper dbHelp, int iStyleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMaterial> lResult = new List<ProductMaterial>();
                string sql = string.Format("SELECT PM.MatID AS MaterialID, PM.Mat{0} as MaterialName, PM.ShortName FROM productmaterial PM INNER JOIN PrdMstMaterial PMsT ON PMsT.MaterialID = PM.MatID WHERE PMsT.MasterID = @StyleID AND PM.MatActive = 1 ", lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("StyleID", iStyleID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMaterial { MaterialName = BusinessUtility.GetString(dr["MaterialName"]), MaterialID = BusinessUtility.GetInt(dr["MaterialID"]), MaterialShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }



    }
}
