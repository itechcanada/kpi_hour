﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProcessInventory
    {
        public static bool IsImageAdded(int productID)
        {
            string sql = string.Format("SELECT COUNT(*) FROM prdimages pim WHERE pim.prdID={0}", productID);
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, System.Data.CommandType.Text, null);
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {
                throw;
            }
            finally
            { 

            }
        }

        public static string GetVendorsCountByProductID(int productID) {
            string sql = string.Format("SELECT v.vendorName FROM Products p LEFT JOIN prdassociatevendor pv  ON pv.prdId = p.ProductID LEFT JOIN vendor v ON pv.prdVendorID = v.vendorID WHERE p.productID = {0} GROUP BY p.productID", productID);
            List<string> lResult = new List<string>();
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, null);
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetString(dr[0]));
                }
                return string.Join(", ", lResult.ToArray());
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public static int GetInTransitProductQuantity(int productID, string whsCode) {
            List<MySqlParameter> pList1 = new List<MySqlParameter>();
            string totalSql = "select sum(poQty) as poQty from purchaseorderitems as pi ";
            totalSql += " Inner join purchaseorders on purchaseorders.poID=pi.poID ";
            totalSql += " where poItemPrdId=@ProductID and poStatus='R'";  //To Do to replce this hard code value
            pList1.Add(DbUtility.GetParameter("ProductID", productID, MyDbType.Int));
            if (!string.IsNullOrEmpty(whsCode)) {
                totalSql += " and poItemShpToWhsCode=@Whs";
                pList1.Add(DbUtility.GetParameter("Whs", whsCode, MyDbType.String));
            }
            totalSql += " Group by pi.poItemPrdId";

            List<MySqlParameter> pList2 = new List<MySqlParameter>();
            string rcvdSql = "select sum(poRcvdQty) as poRcvdQty from purchaseorderitems as pi ";
            rcvdSql += " Inner join purchaseorders on purchaseorders.poID=pi.poID ";
            rcvdSql += " where poItemPrdId=@ProductID and poStatus='R'";  //To Do to replce this hard code value
            pList2.Add(DbUtility.GetParameter("ProductID", productID, MyDbType.Int));
            if (!string.IsNullOrEmpty(whsCode))
            {
                rcvdSql += " and poItemShpToWhsCode=@Whs";
                pList2.Add(DbUtility.GetParameter("Whs", whsCode, MyDbType.String));
            }
            rcvdSql += " Group by pi.poItemPrdId";


            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalarTotal = dbHelp.GetValue(totalSql, System.Data.CommandType.Text, pList1.ToArray());
                int total = BusinessUtility.GetInt(scalarTotal);

                object scalarRcvd = dbHelp.GetValue(rcvdSql, System.Data.CommandType.Text, pList2.ToArray());
                int rcvd = BusinessUtility.GetInt(scalarRcvd);

                return total - rcvd;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static List<OptionList> GetProductListToAssign(int productID, string lang) {
            List<OptionList> lResult = new List<OptionList>();
            string sql = "SELECT Pdes.prdName as ProductName,ProductID from products  ";
            sql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where  descLang=@descLang and prdISActive=1 AND prdType <> @prdType and ProductID <> @ProductID order by Pdes.prdName";
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelper.GetDataReader(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("descLang", lang, MyDbType.String), 
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdType", StatusProductType.Accommodation, MyDbType.Int)
                });
                while (dr.Read())
                {
                    OptionList opt = new OptionList();
                    opt.id = BusinessUtility.GetString(dr["ProductID"]);
                    opt.Name = BusinessUtility.GetString(dr["ProductName"]);
                    lResult.Add(opt);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection(dr);
            }
        }

        public static List<OptionList> GetProductListAssigned(int productID, string lang) {
            List<OptionList> lResult = new List<OptionList>();
            string sql = "select ap.assProdID ,ap.id,pd.prdName from prdassociations as ap ";
            sql += "  Inner Join prddescriptions as pd on pd.id=ap.AssprodID where ap.id=@ProductID AND  descLang=@descLang";
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelper.GetDataReader(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("descLang", lang, MyDbType.String), DbUtility.GetParameter("ProductID", productID, MyDbType.Int) });
                while (dr.Read())
                {
                    OptionList opt = new OptionList();
                    opt.id = BusinessUtility.GetString(dr["assProdID"]);
                    opt.Name = BusinessUtility.GetString(dr["prdName"]);
                    lResult.Add(opt);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection(dr);
            }
        }

        public static void AssociateProducts(int[] assoPrdIds, int targetProductID) {
            string sqlDelete = "DELETE FROM prdassociations WHERE id=@id";
            string sqlInsert = "INSERT INTO prdassociations (id, assProdID) VALUES (@id, @assProdID)";
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("id", targetProductID, MyDbType.Int) });
                foreach (int item in assoPrdIds)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("id", targetProductID, MyDbType.Int), DbUtility.GetParameter("assProdID", item, MyDbType.Int) });
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void AssignPOSCategories(string catList, int productID) {
            int[] categID = StringUtil.GetArrayFromJoindString("~", catList);
            string Delete = string.Format("DELETE FROM prdposcatgsel WHERE productID={0}", productID);
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(Delete, System.Data.CommandType.Text, null);
                string insert = "INSERT INTO prdposcatgsel (productID, prdPOSCatgID) VALUES ({0}, {1})";
                foreach (int item in categID)
                {
                    if (item > 0) {
                        dbHelp.ExecuteNonQuery(string.Format(insert, productID, item), System.Data.CommandType.Text, null);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }       

        public static string GetBedAmenities(int productID, string lang)
        {            
            DbHelper dbHelp = new DbHelper(true);            
            try
            {
                return GetBedAmenities(dbHelp, productID, lang);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static string GetBedAmenities(DbHelper dbHelp, int productID, string lang)
        {
            string strSql = "SELECT zpa.ProductID,";
            strSql += "  " + Globals.GetFieldName("za.AttributeDesc", lang);
            strSql += "   AS Amenity";
            strSql += "  FROM    z_product_attributes zpa";
            strSql += "       INNER JOIN";
            strSql += "          z_attributes za";
            strSql += "       ON (zpa.AttributeID = za.AttributeID)";
            strSql += " WHERE zpa.ProductID=@ProductID ORDER BY 2";            
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productID, MyDbType.Int) });
                List<string> lResult = new List<string>();
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetString(dr["Amenity"]));
                }

                return string.Join(",", lResult.ToArray()).Replace(",", ", ");
            }
            catch
            {

                throw;
            }
            finally
            {
                if (dr!= null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        #region GetPreDefinedFunctions

        public static Product GetCancelProduct(int userid)
        {
            string sqlCheckExist = "SELECT productID FROM products WHERE prdIntID=@prdIntID";
            DbHelper dbHelp = new DbHelper(true);
            Product prd = new Product();
            try
            {
                object id = dbHelp.GetValue(sqlCheckExist, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdIntID", PreDefinedProductsInternalID.ID_CANCEL, MyDbType.String)
                });
                if (BusinessUtility.GetInt(id) > 0)
                {
                    prd.PopulateObject(dbHelp, BusinessUtility.GetInt(id));
                }
                else
                {
                    prd.PrdName = "CANCEL";
                    prd.PrdIntID = PreDefinedProductsInternalID.ID_CANCEL;
                    prd.PrdExtID = PreDefinedProductsInternalID.ID_CANCEL;
                    prd.PrdUPCCode = prd.PrdIntID = PreDefinedProductsInternalID.ID_CANCEL;
                    prd.Insert(dbHelp, userid);
                }

                return prd;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static Product GetDiscountProduct(int userid)
        {
            string sqlCheckExist = "SELECT productID FROM products WHERE prdIntID=@prdIntID";
            DbHelper dbHelp = new DbHelper(true);
            Product prd = new Product();
            try
            {
                object id = dbHelp.GetValue(sqlCheckExist, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdIntID", PreDefinedProductsInternalID.ID_DISCOUNT, MyDbType.String)
                });
                if (BusinessUtility.GetInt(id) > 0)
                {
                    prd.PopulateObject(dbHelp, BusinessUtility.GetInt(id));
                }
                else
                {
                    prd.PrdName = "DISCOUNT";
                    prd.PrdIntID = PreDefinedProductsInternalID.ID_DISCOUNT;
                    prd.PrdExtID = PreDefinedProductsInternalID.ID_DISCOUNT;
                    prd.PrdUPCCode = prd.PrdIntID = PreDefinedProductsInternalID.ID_DISCOUNT;
                    prd.Insert(dbHelp, userid);
                }

                return prd;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}
