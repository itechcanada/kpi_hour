﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductMasterDetail
    {
        public int MasterID { get; set; }
        public string PrdName { get; set; }
        public string PrdUPCCode { get; set; }
        public double PrdSalePricePerMinQty { get; set; }
        public double PrdEndUserSalesPrice { get; set; }
        public string PrdIntID { get; set; }
        public double PrdWebSalesPrice { get; set; }
        public string PrdWeight { get; set; }
        public string PrdLength { get; set; }
        public string PrdWeightPkg { get; set; }
        public string PrdLengthPkg { get; set; }
        public string PrdWidthPkg { get; set; }
        public string PrdHeightPkg { get; set; }
        public string PrdStartLength { get; set; }
        public string PrdIncrementLength { get; set; }
        public string PrdStartChest { get; set; }
        public string PrdIncrementChest { get; set; }
        public string PrdStartShoulder { get; set; }
        public string PrdIncrementShoulder { get; set; }
        public string PrdStartBicep { get; set; }
        public string PrdIncrementBicep { get; set; }
        public int PrdMinQtyPO { get; set; }
        public int PrdQtyPO { get; set; }
        public bool PrdAutoPO { get; set; }
        public bool PrdIsSpecial { get; set; }
        public bool PrdIsKit { get; set; }
        public bool IsPOSMenu { get; set; }
        public bool PrdIsWeb { get; set; }
        public bool PrdIsActive { get; set; }
        public string PrdNotes { get; set; }
        public string PrdFrDesc { get; set; }
        public string PrdEnDesc { get; set; }
        public string PrdNickLine { get; set; }
        public string PrdExtraCategory { get; set; }
        public string PrdSleeve { get; set; }
        public string PrdSilhouette { get; set; }
        public string PrdTrend { get; set; }
        public string PrdKeyWord { get; set; }
        public double PrdFOBPrice { get; set; }
        public double PrdLandedPrice { get; set; }
        public string PrdGauge { get; set; }
        public string ProdWeightIncrement { get; set; }

        public Boolean Save(int userID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sql = "";

                sql = " SELECT * FROM ProdMasterDetail WHERE MasterID = @MasterID";
                DataTable dt = dbTransactionHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int )});

                if (dt.Rows.Count > 0)
                {
                    sql = "  UPDATE  ProdMasterDetail SET ";
                    sql += " prdName = @prdName, prdUPCCode = @prdUPCCode, prdSalePricePerMinQty = @prdSalePricePerMinQty, prdEndUserSalesPrice = @prdEndUserSalesPrice, prdIntID = @prdIntID, prdWebSalesPrice = @prdWebSalesPrice, prdWeight = @prdWeight, ";
                    sql += " prdLength = @prdLength, prdWeightPkg = @prdWeightPkg, prdLengthPkg = @prdLengthPkg, prdWidthPkg = @prdWidthPkg, prdHeightPkg = @prdHeightPkg, prdStartLength = @prdStartLength, prdIncrementLength = @prdIncrementLength, prdStartChest = @prdStartChest, prdIncrementChest = @prdIncrementChest, prdStartShoulder = @prdStartShoulder, ";
                    sql += " prdIncrementShoulder = @prdIncrementShoulder, prdStartBicep = @prdStartBicep, prdIncrementBicep = @PrdIncrementBicep, prdMinQtyPO = @prdMinQtyPO, prdQtyPO = @prdQtyPO, prdAutoPO = @prdAutoPO, prdIsSpecial = @prdIsSpecial, prdIsKit = @prdIsKit, isPOSMenu = @isPOSMenu, prdIsWeb = @prdIsWeb, prdIsActive = @prdIsActive,  ";
                    sql += " prdNotes = @prdNotes, prdFrDesc = @prdFrDesc, prdEnDesc = @prdEnDesc, prdLastUpdatedByUserID = @prdLastUpdatedByUserID, prdLastUpdatedOn = @prdLastUpdatedOn, prdFOBPrice = @prdFOBPrice, prdLandedPrice = @prdLandedPrice, ProdWeightIncrement = @ProdWeightIncrement "; // , prdGauge = @prdGauge
                    sql += "  WHERE MasterID = @MasterID";

                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int ),
                    DbUtility.GetParameter("PrdName", this.PrdName, MyDbType.String ),
                    DbUtility.GetParameter("PrdUPCCode", this.PrdUPCCode, MyDbType.String ),
                    DbUtility.GetParameter("PrdSalePricePerMinQty", this.PrdSalePricePerMinQty, MyDbType.Double ),
                    DbUtility.GetParameter("PrdEndUserSalesPrice", this.PrdEndUserSalesPrice, MyDbType.Double ),
                    DbUtility.GetParameter("PrdIntID", this.PrdIntID, MyDbType.String ),
                    DbUtility.GetParameter("PrdWebSalesPrice", this.PrdWebSalesPrice, MyDbType.Double ),
                    DbUtility.GetParameter("PrdWeight", this.PrdWeight, MyDbType.String),
                    DbUtility.GetParameter("PrdLength", this.PrdLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdWeightPkg", this.PrdWeightPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdLengthPkg", this.PrdLengthPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdWidthPkg", this.PrdWidthPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdHeightPkg", this.PrdHeightPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartLength", this.PrdStartLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementLength", this.PrdIncrementLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartChest", this.PrdStartChest, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementChest", this.PrdIncrementChest, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartShoulder", this.PrdStartShoulder, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementShoulder", this.PrdIncrementShoulder, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartBicep", this.PrdStartBicep, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementBicep", this.PrdIncrementBicep, MyDbType.String ),
                    DbUtility.GetParameter("PrdMinQtyPO", this.PrdMinQtyPO, MyDbType.Int ),
                    DbUtility.GetParameter("PrdQtyPO", this.PrdQtyPO, MyDbType.Int ),
                    DbUtility.GetParameter("PrdAutoPO", this.PrdAutoPO, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsSpecial", this.PrdIsSpecial, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsKit", this.PrdIsKit, MyDbType.Boolean ),
                    DbUtility.GetParameter("isPOSMenu", this.IsPOSMenu, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsWeb", this.PrdIsWeb, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsActive", this.PrdIsActive, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdNotes", this.PrdNotes, MyDbType.String ),
                    DbUtility.GetParameter("PrdFrDesc", this.PrdFrDesc, MyDbType.String ),
                    DbUtility.GetParameter("PrdEnDesc", this.PrdEnDesc, MyDbType.String ),
                    DbUtility.GetParameter("prdLastUpdatedByUserID", userID, MyDbType.Int ),
                    DbUtility.GetParameter("prdLastUpdatedOn", DateTime.Now, MyDbType.DateTime ),
                    DbUtility.GetParameter("prdFOBPrice", this.PrdFOBPrice, MyDbType.Double ),
                    DbUtility.GetParameter("prdLandedPrice", this.PrdLandedPrice, MyDbType.Double ),
                    //DbUtility.GetParameter("prdGauge", this.PrdGauge, MyDbType.Int )
                    DbUtility.GetParameter("ProdWeightIncrement", this.ProdWeightIncrement, MyDbType.String ),
                });

                    if (this.PrdNickLine != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });

                        string[] sNickline = this.PrdNickLine.Split(',');
                        foreach (string nickLine in sNickline)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(nickLine), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });
                        }
                    }


                    if (this.PrdGauge != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });

                        string[] sGauge = this.PrdGauge.Split(',');
                        foreach (string gauge in sGauge)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(gauge), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });
                        }
                    }


                    if (this.PrdExtraCategory != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });

                        string[] sExtraCategory = this.PrdExtraCategory.Split(',');
                        foreach (string extraCategory in sExtraCategory)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(extraCategory), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdSleeve != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });

                        string[] sSleeve = this.PrdSleeve.Split(',');
                        foreach (string sleeve in sSleeve)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(sleeve), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdSilhouette != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });

                        string[] sSilhouette = this.PrdSilhouette.Split(',');
                        foreach (string silhouette in sSilhouette)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(silhouette), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });
                        }
                    }



                    if (this.PrdTrend != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID",6, MyDbType.Int),
                        });

                        string[] sTrend = this.PrdTrend.Split(',');
                        foreach (string silhouette in sTrend)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(silhouette), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                        });
                        }
                    }


                    if (this.PrdKeyWord != "")
                    {
                        string sqlDelete = " DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });

                        string[] sKeyWord = this.PrdKeyWord.Split(',');
                        foreach (string KeyWord in sKeyWord)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(KeyWord), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });
                        }
                    }

                }
                else
                {
                    sql = " INSERT INTO ProdMasterDetail (MasterID, prdName, prdUPCCode, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIntID, prdWebSalesPrice, prdWeight, ";
                    sql += " prdLength, prdWeightPkg, prdLengthPkg, prdWidthPkg, prdHeightPkg, prdStartLength, prdIncrementLength, prdStartChest, prdIncrementChest, prdStartShoulder, ";
                    sql += " prdIncrementShoulder, prdStartBicep, prdIncrementBicep, prdMinQtyPO, prdQtyPO, prdAutoPO, prdIsSpecial, prdIsKit, isPOSMenu, prdIsWeb, prdIsActive,  ";
                    sql += " prdNotes, prdFrDesc, prdEnDesc, prdCreatedUserID, prdCreatedOn, prdFOBPrice, prdLandedPrice, ProdWeightIncrement )   "; //prdGauge
                    sql += " VALUES ( @MasterID, @prdName, @prdUPCCode, @prdSalePricePerMinQty, @prdEndUserSalesPrice, @prdIntID, @prdWebSalesPrice, @prdWeight, @prdLength,  ";
                    sql += " @prdWeightPkg, @prdLengthPkg, @prdWidthPkg, @prdHeightPkg, @prdStartLength, @prdIncrementLength, @prdStartChest, @prdIncrementChest, @prdStartShoulder,  ";
                    sql += " @prdIncrementShoulder, @prdStartBicep, @prdIncrementBicep, @prdMinQtyPO, @prdQtyPO, @prdAutoPO, @prdIsSpecial, @prdIsKit, @isPOSMenu, @prdIsWeb,  ";
                    sql += " @prdIsActive, @prdNotes, @prdFrDesc, @prdEnDesc, @prdCreatedUserID, @prdCreatedOn, @prdFOBPrice, @prdLandedPrice,  @ProdWeightIncrement  )  "; // @prdGauge
                    sql += " ";

                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int ),
                    DbUtility.GetParameter("PrdName", this.PrdName, MyDbType.String ),
                    DbUtility.GetParameter("PrdUPCCode", this.PrdUPCCode, MyDbType.String ),
                    DbUtility.GetParameter("PrdSalePricePerMinQty", this.PrdSalePricePerMinQty, MyDbType.Double ),
                    DbUtility.GetParameter("PrdEndUserSalesPrice", this.PrdEndUserSalesPrice, MyDbType.Double ),
                    DbUtility.GetParameter("PrdIntID", this.PrdIntID, MyDbType.String ),
                    DbUtility.GetParameter("PrdWebSalesPrice", this.PrdWebSalesPrice, MyDbType.Double ),
                    DbUtility.GetParameter("PrdWeight", this.PrdWeight, MyDbType.String),
                    DbUtility.GetParameter("PrdLength", this.PrdLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdWeightPkg", this.PrdWeightPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdLengthPkg", this.PrdLengthPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdWidthPkg", this.PrdWidthPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdHeightPkg", this.PrdHeightPkg, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartLength", this.PrdStartLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementLength", this.PrdIncrementLength, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartChest", this.PrdStartChest, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementChest", this.PrdIncrementChest, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartShoulder", this.PrdStartShoulder, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementShoulder", this.PrdIncrementShoulder, MyDbType.String ),
                    DbUtility.GetParameter("PrdStartBicep", this.PrdStartBicep, MyDbType.String ),
                    DbUtility.GetParameter("PrdIncrementBicep", this.PrdIncrementBicep, MyDbType.String ),
                    DbUtility.GetParameter("PrdMinQtyPO", this.PrdMinQtyPO, MyDbType.Int ),
                    DbUtility.GetParameter("PrdQtyPO", this.PrdQtyPO, MyDbType.Int ),
                    DbUtility.GetParameter("PrdAutoPO", this.PrdAutoPO, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsSpecial", this.PrdIsSpecial, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsKit", this.PrdIsKit, MyDbType.Boolean ),
                    DbUtility.GetParameter("isPOSMenu", this.IsPOSMenu, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsWeb", this.PrdIsWeb, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdIsActive", this.PrdIsActive, MyDbType.Boolean ),
                    DbUtility.GetParameter("PrdNotes", this.PrdNotes, MyDbType.String ),
                    DbUtility.GetParameter("PrdFrDesc", this.PrdFrDesc, MyDbType.String ),
                    DbUtility.GetParameter("PrdEnDesc", this.PrdEnDesc, MyDbType.String ),
                    DbUtility.GetParameter("PrdCreatedUserID", userID, MyDbType.Int ),
                    DbUtility.GetParameter("PrdCreatedOn", DateTime.Now, MyDbType.DateTime ),
                    DbUtility.GetParameter("prdFOBPrice", this.PrdFOBPrice, MyDbType.Double ),
                    DbUtility.GetParameter("prdLandedPrice", this.PrdLandedPrice, MyDbType.Double ),
                    //DbUtility.GetParameter("prdGauge", this.PrdGauge, MyDbType.Int )
                    DbUtility.GetParameter("ProdWeightIncrement", this.ProdWeightIncrement, MyDbType.String ),
                });

                    if (this.PrdNickLine != "")
                    {
                        string[] sNickline = this.PrdNickLine.Split(',');
                        foreach (string nickLine in sNickline)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(nickLine), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdGauge != "")
                    {
                        string[] sGauge = this.PrdGauge.Split(',');
                        foreach (string gauge in sGauge)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(gauge), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdExtraCategory != "")
                    {
                        string[] sExtraCategory = this.PrdExtraCategory.Split(',');
                        foreach (string extraCategory in sExtraCategory)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(extraCategory), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdSleeve != "")
                    {
                        string[] sSleeve = this.PrdSleeve.Split(',');
                        foreach (string sleeve in sSleeve)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(sleeve), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });
                        }
                    }

                    if (this.PrdSilhouette != "")
                    {
                        string[] sSilhouette = this.PrdSilhouette.Split(',');
                        foreach (string silhouette in sSilhouette)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(silhouette), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });
                        }
                    }


                    if (this.PrdTrend != "")
                    {
                        string[] sTrend = this.PrdTrend.Split(',');
                        foreach (string silhouette in sTrend)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(silhouette), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                        });
                        }
                    }


                    if (this.PrdKeyWord != "")
                    {
                        string[] sKeyWord = this.PrdKeyWord.Split(',');
                        foreach (string KeyWord in sKeyWord)
                        {
                            string sqlInsert = " INSERT INTO prodMasterCatgSelVal (MasterID, ProdSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @ProdSysCatgDtlID, @ProdSyCatgHdrID) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSysCatgDtlID", BusinessUtility.GetInt(KeyWord), MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });
                        }
                    }
                }

                string sqlUpdate = " UPDATE productMasterHdr SET MasterActive = @MasterActive, MasterLastUpdBy = @MasterLastUpdBy, MasterLastUpdOn = @MasterLastUpdOn WHERE MasterID = @MasterID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("MasterActive", this.PrdIsActive , MyDbType.Int),
                        DbUtility.GetParameter("MasterLastUpdBy", userID, MyDbType.Int),
                         DbUtility.GetParameter("MasterLastUpdOn", DateTime.Now, MyDbType.DateTime),
                 });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int MasterID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT * FROM ProdMasterDetail WHERE MasterID=@MasterID";
            MySqlParameter[] p = { DbUtility.GetParameter("MasterID", MasterID, MyDbType.Int) };
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, p);
                if (dr.Read())
                {
                    this.PrdName = BusinessUtility.GetString(dr["prdName"]);
                    this.PrdUPCCode = BusinessUtility.GetString(dr["prdUPCCode"]);
                    this.PrdSalePricePerMinQty = BusinessUtility.GetDouble(dr["prdSalePricePerMinQty"]);
                    this.PrdEndUserSalesPrice = BusinessUtility.GetDouble(dr["prdEndUserSalesPrice"]);
                    this.PrdIntID = BusinessUtility.GetString(dr["prdIntID"]);
                    this.PrdWebSalesPrice = BusinessUtility.GetDouble(dr["prdWebSalesPrice"]);
                    this.PrdWeight = BusinessUtility.GetString(dr["prdWeight"]);
                    this.PrdLength = BusinessUtility.GetString(dr["prdLength"]);
                    this.PrdWeightPkg = BusinessUtility.GetString(dr["prdWeightPkg"]);
                    this.PrdLengthPkg = BusinessUtility.GetString(dr["prdLengthPkg"]);
                    this.PrdWidthPkg = BusinessUtility.GetString(dr["prdWidthPkg"]);
                    this.PrdHeightPkg = BusinessUtility.GetString(dr["prdHeightPkg"]);
                    this.PrdStartLength = BusinessUtility.GetString(dr["prdStartLength"]);
                    this.PrdIncrementLength = BusinessUtility.GetString(dr["prdIncrementLength"]);
                    this.PrdStartChest = BusinessUtility.GetString(dr["prdStartChest"]);
                    this.PrdIncrementChest = BusinessUtility.GetString(dr["prdIncrementChest"]);
                    this.PrdStartShoulder = BusinessUtility.GetString(dr["prdStartShoulder"]);
                    this.PrdIncrementShoulder = BusinessUtility.GetString(dr["prdIncrementShoulder"]);
                    this.PrdStartBicep = BusinessUtility.GetString(dr["prdStartBicep"]);
                    this.PrdIncrementBicep = BusinessUtility.GetString(dr["prdIncrementBicep"]);
                    this.PrdMinQtyPO = BusinessUtility.GetInt(dr["prdMinQtyPO"]);
                    this.PrdQtyPO = BusinessUtility.GetInt(dr["prdQtyPO"]);
                    this.PrdAutoPO = BusinessUtility.GetBool(dr["prdAutoPO"]);
                    this.PrdIsSpecial = BusinessUtility.GetBool(dr["prdIsSpecial"]);
                    this.PrdIsKit = BusinessUtility.GetBool(dr["prdIsKit"]);
                    this.IsPOSMenu = BusinessUtility.GetBool(dr["isPOSMenu"]);
                    this.PrdIsWeb = BusinessUtility.GetBool(dr["prdIsWeb"]);
                    this.PrdIsActive = BusinessUtility.GetBool(dr["prdIsActive"]);
                    this.PrdNotes = BusinessUtility.GetString(dr["prdNotes"]);
                    this.PrdFrDesc = BusinessUtility.GetString(dr["prdFrDesc"]);
                    this.PrdEnDesc = BusinessUtility.GetString(dr["prdEnDesc"]);
                    this.PrdFOBPrice = BusinessUtility.GetDouble(dr["prdFOBPrice"]);
                    this.PrdLandedPrice = BusinessUtility.GetDouble(dr["prdLandedPrice"]);
                    //this.PrdGauge = BusinessUtility.GetInt(dr["prdGauge"]);
                    this.ProdWeightIncrement = BusinessUtility.GetString(dr["ProdWeightIncrement"]);
                }
                dr.Close();
                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                DataTable dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 2, MyDbType.Int),
                        });
                string sNickline = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sNickline != "")
                    {
                        sNickline += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sNickline = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdNickLine = sNickline;

                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 9, MyDbType.Int),
                        });
                string sGauge = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sGauge != "")
                    {
                        sGauge += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sGauge = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdGauge = sGauge;


                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 3, MyDbType.Int),
                        });
                string sExtraCategory = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sExtraCategory != "")
                    {
                        sExtraCategory += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sExtraCategory = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdExtraCategory = sExtraCategory;


                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 4, MyDbType.Int),
                        });
                string sSleeve = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sSleeve != "")
                    {
                        sSleeve += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sSleeve = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdSleeve = sSleeve;

                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 5, MyDbType.Int),
                        });
                string sSilhouette = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sSilhouette != "")
                    {
                        sSilhouette += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sSilhouette = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdSilhouette = sSilhouette;



                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 6, MyDbType.Int),
                        });
                string sTrend = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sTrend != "")
                    {
                        sTrend += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sTrend = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdTrend = sTrend;


                sql = " SELECT prodSysCatgDtlID FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dr = null;
                dtTbl = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int),
                        DbUtility.GetParameter("ProdSyCatgHdrID", 7, MyDbType.Int),
                        });
                string sKeyWord = "";
                foreach (DataRow drRow in dtTbl.Rows)
                {
                    if (sKeyWord != "")
                    {
                        sKeyWord += "," + BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                    else
                    {
                        sKeyWord = BusinessUtility.GetString(drRow["prodSysCatgDtlID"]);
                    }
                }
                this.PrdKeyWord = sKeyWord;


            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

    }
}
