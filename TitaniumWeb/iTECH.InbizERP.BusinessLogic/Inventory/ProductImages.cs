﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductImages
    {
        //public const string IMAGE_META_RECTANGLE_DROP_AREAS = "RECTANGLE_DROP_AREAS";
        //public const string IMAGE_META_CHARM_WIDTH = "CHARM_WIDTH";
        //public const string IMAGE_META_CHARM_OFFSET = "CHARM_OFFSET";
        
        public const string PRODUCT_IMAGE_TYPE_FOUNDATION = "foundation";
        public const string PRODUCT_IMAGE_TYPE_CHARM = "charm";
        public const string PRODUCT_IMAGE_TYPE_NORMAL = "normal";

        private int _prdImageID;

        public int PrdImageID
        {
            get { return _prdImageID; }
            set { _prdImageID = value; }
        }
        private int _prdID;

        public int PrdID
        {
            get { return _prdID; }
            set { _prdID = value; }
        }
        private string _prdSmallImagePath;

        public string PrdSmallImagePath
        {
            get { return _prdSmallImagePath; }
            set { _prdSmallImagePath = value; }
        }
        private string _prdLargeImagePath;

        public string PrdLargeImagePath
        {
            get { return _prdLargeImagePath; }
            set { _prdLargeImagePath = value; }
        }
        private bool _pimgIsDefault;

        public bool PimgIsDefault
        {
            get { return _pimgIsDefault; }
            set { _pimgIsDefault = value; }
        }

        public string ProductImageType { get; set; }
        public int CharmOffSet { get; set; }
        public int CharmWidth { get; set; }
        public string RectangleCoordinates
        {
            get;
            set;
        }

        public string Rotate90ImageUrl { get; set; }
        public string ImageGroup { get; set; }        

        public ProductImages() {
            
        }           

        public void Insert() {
            string sql = "INSERT INTO prdimages (prdID,prdSmallImagePath,prdLargeImagePath,pimgIsDefault,ProductImageType,CharmOffSet,CharmWidth,RectangleCoordinates,ImageGroup,Rotate90ImageUrl) VALUES(@prdID,@prdSmallImagePath,@prdLargeImagePath,@pimgIsDefault,@ProductImageType,@CharmOffSet,@CharmWidth,@RectangleCoordinates,@ImageGroup,@Rotate90ImageUrl)";
                
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                                     DbUtility.GetParameter("prdSmallImagePath", this.PrdSmallImagePath, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeImagePath", this.PrdLargeImagePath, MyDbType.String),
                                     DbUtility.GetParameter("pimgIsDefault", this.PimgIsDefault, MyDbType.Boolean),
                                     DbUtility.GetParameter("CharmOffSet", this.CharmOffSet, MyDbType.Int),
                                     DbUtility.GetParameter("CharmWidth", this.CharmWidth, MyDbType.Int),
                                     DbUtility.GetParameter("RectangleCoordinates", this.RectangleCoordinates, MyDbType.String),
                                     DbUtility.GetParameter("ProductImageType", this.ProductImageType, MyDbType.String),
                                     DbUtility.GetParameter("ImageGroup", this.ImageGroup, MyDbType.String),
                                     DbUtility.GetParameter("Rotate90ImageUrl", this.Rotate90ImageUrl, MyDbType.String)
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, System.Data.CommandType.Text, p);
                this.PrdImageID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(int imgID, string prdImageType, string smallImagePath, string largeImagePath, string img90DegPath, bool isDefault)
        {
            string sql = "UPDATE prdimages SET prdSmallImagePath=@prdSmallImagePath, prdLargeImagePath=@prdLargeImagePath,";
            sql += " pimgIsDefault=@pimgIsDefault,ProductImageType=@ProductImageType WHERE prdImageID=@prdImageID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("prdSmallImagePath", smallImagePath, MyDbType.String),
                     DbUtility.GetParameter("prdLargeImagePath", largeImagePath, MyDbType.String),
                     DbUtility.GetParameter("pimgIsDefault", isDefault, MyDbType.Boolean),
                     DbUtility.GetParameter("ProductImageType", prdImageType, MyDbType.String),
                     DbUtility.GetParameter("prdImageID", imgID, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(int imgID, int charmOffset, int charmWidth, string recCord)
        {
            string sql = "UPDATE prdimages SET CharmOffSet=@CharmOffSet,CharmWidth=@CharmWidth,RectangleCoordinates=@RectangleCoordinates WHERE prdImageID=@prdImageID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("CharmOffSet", charmOffset, MyDbType.Int),
                     DbUtility.GetParameter("CharmWidth", charmWidth, MyDbType.Int),
                     DbUtility.GetParameter("RectangleCoordinates", recCord, MyDbType.String),                     
                     DbUtility.GetParameter("prdImageID", imgID, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int prdImageID) {
            string sql = "DELETE FROM prdimages WHERE prdImageID=@prdImageID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdImageID", prdImageID, MyDbType.Int) });
                return true;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID)
        {
            pCol.Clear();
            pCol.Add("@prdID", productID.ToString());
            return "SELECT * FROM prdimages WHERE prdID=@prdID";
        }

        public void PopulatObject(int imgID)
        {
            string sql = "SELECT * FROM prdimages WHERE prdImageID=@ImageID";
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ImageID", imgID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.ProductImageType = BusinessUtility.GetString(dr["ProductImageType"]);
                    this.PimgIsDefault = BusinessUtility.GetBool(dr["pimgIsDefault"]);
                    this.PrdID = BusinessUtility.GetInt(dr["prdID"]);
                    this.PrdImageID = BusinessUtility.GetInt(dr["prdImageID"]);
                    this.PrdLargeImagePath = BusinessUtility.GetString(dr["prdLargeImagePath"]);
                    this.PrdSmallImagePath = BusinessUtility.GetString(dr["prdSmallImagePath"]);
                    this.CharmOffSet = BusinessUtility.GetInt(dr["CharmOffSet"]);
                    this.CharmWidth = BusinessUtility.GetInt(dr["CharmWidth"]);
                    this.RectangleCoordinates = BusinessUtility.GetString(dr["RectangleCoordinates"]);
                    this.ImageGroup = BusinessUtility.GetString(dr["ImageGroup"]);                   
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }   
}
