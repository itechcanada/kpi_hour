﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductCategory
    {
        public string CatgName { get; set; }
        public string CatgValue { get; set; }
        public string CatgNameFr { get; set; }
        public string CatgNameEn { get; set; }
        public int CatgID { get; set; }
        public int CatgHrdID { get; set; }
        public bool CatgIsActive { get; set; }
        public int CatgUpdatedBy { get; set; }
        public DateTime CatgUpdatedAt { get; set; }

        public string CatgHdrName { get; set; }
        public int CatgHdrSeq { get; set; }
        public int CatgHdrSelNo { get; set; }


        public List<ProductCategory> GetCategoryList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                string sql = " SELECT pCatgDtl.sysCatgDtlID AS CatgID, pCatgDtl.sysCatgSelValue{0} AS CatgName FROM prodSysCatgHdr AS pCatgHdr  ";
                sql += " INNER JOIN prodSysCatgDtl AS pCatgDtl ON pCatgHdr.sysCatgHdrID = pCatgDtl.sysCatgHdrID ";
                sql += " WHERE pCatgHdr.sysCatgHdrID = @CatgHdrID AND pCatgDtl.sysCatgSelActive = 1 ORDER BY pCatgDtl.sysCatgSelValue{0} /*pCatgDtl.sysCatgSelSeq*/  ";

                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int)}))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(dr["CatgID"]), CatgName = BusinessUtility.GetString(dr["CatgName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetSearchCategoryList(DbHelper dbHelp, string lang, string resourcevalueAll)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                //string sql = " SELECT pCatgDtl.sysCatgDtlID AS CatgID, pCatgDtl.sysCatgSelValue{0} AS CatgName FROM prodSysCatgHdr AS pCatgHdr  ";
                //sql += " INNER JOIN prodSysCatgDtl AS pCatgDtl ON pCatgHdr.sysCatgHdrID = pCatgDtl.sysCatgHdrID ";
                //sql += " WHERE pCatgHdr.sysCatgHdrID = @CatgHdrID AND pCatgDtl.sysCatgSelActive = 1 ORDER BY pCatgDtl.sysCatgSelValue{0} /*pCatgDtl.sysCatgSelSeq*/  ";


                string sql = " SELECT 1, sysCatgHdrID, sysCatgSelValue{0} AS CatgName, sysCatgDtlID  FROM prodsyscatgdtl ";
                sql += " UNION ";
                //sql += " Select  0, h.sysCatgHdrID, concat('ALL ', sysCatgNAmeFr), group_concat(sysCatgDtlID) FROM prodsyscatgdtl d join  prodsyscatghdr h  on h.sysCatgHdrID = d.sysCatgHdrID ";
                sql += " Select  0, h.sysCatgHdrID, concat('{1} ', sysCatgNAme{0}), group_concat(sysCatgDtlID) FROM prodsyscatgdtl d join  prodsyscatghdr h  on h.sysCatgHdrID = d.sysCatgHdrID ";
                sql += " group by h.sysCatgHdrID, sysCatgNAme{0} order by  sysCatgHdrID, 1 ";

                sql = string.Format(sql, lang, resourcevalueAll);
                

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int)}))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgValue = BusinessUtility.GetString(dr["sysCatgDtlID"]), CatgName = BusinessUtility.GetString(dr["CatgName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean AddCatgInStyle(int iMasterID, int[] iCatgID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND  ProdSyCatgHdrID = @CatgHdrID ";//AND MaterialID = @MaterialID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("CatgHdrID",BusinessUtility.GetInt( this.CatgHrdID), MyDbType.Int)
                });

                foreach (var item in iCatgID)
                {
                    string sql = " INSERT INTO prodMasterCatgSelVal (MasterID, prodSysCatgDtlID, ProdSyCatgHdrID) VALUES (@MasterID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("prodSysCatgDtlID",BusinessUtility.GetInt( item), MyDbType.Int),
                    DbUtility.GetParameter("ProdSyCatgHdrID",BusinessUtility.GetInt( this.CatgHrdID), MyDbType.Int)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteCatgInStyle(int iMasterID, int iCatgID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM prodMasterCatgSelVal WHERE MasterID = @MasterID AND prodSysCatgDtlID = @prodSysCatgDtlID AND ProdSyCatgHdrID = @ProdSyCatgHdrID ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("prodSysCatgDtlID", iCatgID, MyDbType.Int),
                     DbUtility.GetParameter("ProdSyCatgHdrID", this.CatgHrdID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetStyleCatg(DbHelper dbHelp, int iStyleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                //string sql = string.Format("SELECT PM.MatID AS MaterialID, PM.Mat{0} as MaterialName FROM productmaterial PM INNER JOIN PrdMstMaterial PMsT ON PMsT.MaterialID = PM.MatID WHERE PMsT.MasterID = @StyleID AND PM.MatActive = 1 ", lang);
                string sql = " SELECT pCatgDtl.sysCatgDtlID AS CatgID, pCatgDtl.sysCatgSelValue{0} AS CatgName, pMCSL.ProdSyCatgHdrID AS HdrID, sysCatgSelValueFr,sysCatgSelValueEn, pCatgDtl.sysCatgSelActive as sysCatgSelActive, pCatgDtl.sysLastUpdatedBy AS sysLastUpdatedBy, pCatgDtl.sysLasUpdatedAt AS sysLasUpdatedAt FROM prodSysCatgHdr AS pCatgHdr ";
                sql += " INNER JOIN prodSysCatgDtl AS pCatgDtl ON pCatgHdr.sysCatgHdrID = pCatgDtl.sysCatgHdrID ";
                sql += " INNER JOIN prodMasterCatgSelVal pMCSL ON pMCSL.prodSysCatgDtlID = pCatgDtl.sysCatgDtlID AND pMCSL.ProdSyCatgHdrID = @CatgHdrID AND pMCSL.MasterID = @StyleID ";
                sql += " WHERE pCatgHdr.sysCatgHdrID = @CatgHdrID AND pCatgDtl.sysCatgSelActive = 1 ORDER BY pCatgDtl.sysCatgSelSeq ";

                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("StyleID", iStyleID, MyDbType.Int),
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int),
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgName = BusinessUtility.GetString(dr["CatgName"]), CatgID = BusinessUtility.GetInt(dr["CatgID"]), CatgHrdID = BusinessUtility.GetInt(dr["HdrID"]), CatgIsActive = BusinessUtility.GetBool(dr["sysCatgSelActive"]), CatgUpdatedBy = BusinessUtility.GetInt(dr["sysLastUpdatedBy"]), CatgUpdatedAt = BusinessUtility.GetDateTime(dr["sysLasUpdatedAt"]), CatgNameFr = BusinessUtility.GetString(dr["sysCatgSelValueFr"]), CatgNameEn = BusinessUtility.GetString(dr["sysCatgSelValueEn"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetDetailStyleCatg(DbHelper dbHelp, int iStyleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                //string sql = string.Format("SELECT PM.MatID AS MaterialID, PM.Mat{0} as MaterialName FROM productmaterial PM INNER JOIN PrdMstMaterial PMsT ON PMsT.MaterialID = PM.MatID WHERE PMsT.MasterID = @StyleID AND PM.MatActive = 1 ", lang);
                string sql = " SELECT pCatgDtl.sysCatgDtlID AS CatgID, pCatgDtl.sysCatgSelValue{0} AS CatgName, sysCatgSelValueFr, sysCatgSelValueEn, pCatgDtl.sysCatgSelActive as sysCatgSelActive, pCatgDtl.sysLastUpdatedBy AS sysLastUpdatedBy, pCatgDtl.sysLasUpdatedAt AS sysLasUpdatedAt FROM prodSysCatgDtl AS pCatgDtl ";
                sql += " WHERE pCatgDtl.sysCatgHdrID = @CatgHdrID and sysCatgDtlID= @StyleID  ORDER BY pCatgDtl.sysCatgSelSeq ";

                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("StyleID", iStyleID, MyDbType.Int),
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int),
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgName = BusinessUtility.GetString(dr["CatgName"]), CatgID = BusinessUtility.GetInt(dr["CatgID"]), CatgIsActive = BusinessUtility.GetBool(dr["sysCatgSelActive"]), CatgUpdatedBy = BusinessUtility.GetInt(dr["sysLastUpdatedBy"]), CatgUpdatedAt = BusinessUtility.GetDateTime(dr["sysLasUpdatedAt"]), CatgNameFr = BusinessUtility.GetString(dr["sysCatgSelValueFr"]), CatgNameEn = BusinessUtility.GetString(dr["sysCatgSelValueEn"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetCategoryHeader(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                string sql = " SELECT sysCatgName{0} AS CatgHdrName, sysCatgSeq AS HdrSeq, sysCatgNoOfSel AS HdrCagSelNo FROM prodSysCatgHdr WHERE sysCatgActive = 1 AND sysCatgHdrID = @CatgHdrID  ";
                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int)}))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgHdrSeq = BusinessUtility.GetInt(dr["HdrSeq"]), CatgHdrName = BusinessUtility.GetString(dr["CatgHdrName"]), CatgHdrSelNo = BusinessUtility.GetInt(dr["HdrCagSelNo"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetALLCategoryHeader(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                string sql = " SELECT sysCatgHdrID, sysCatgName{0} AS CatgHdrName, sysCatgSeq AS HdrSeq, sysCatgNoOfSel AS HdrCagSelNo FROM prodSysCatgHdr WHERE sysCatgActive = 1 ";
                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgHdrSeq = BusinessUtility.GetInt(dr["HdrSeq"]), CatgHdrName = BusinessUtility.GetString(dr["CatgHdrName"]), CatgHdrSelNo = BusinessUtility.GetInt(dr["HdrCagSelNo"]), CatgHrdID = BusinessUtility.GetInt(dr["sysCatgHdrID"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductCategory> GetAllCategoryList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductCategory> lResult = new List<ProductCategory>();
                string sql = " SELECT pCatgDtl.sysCatgDtlID AS CatgID, pCatgDtl.sysCatgSelValue{0} AS CatgName, pCatgDtl.sysCatgSelActive as sysCatgSelActive,pCatgDtl.sysCatgHdrID as CatgHdrID, pCatgHdr.sysCatgName{0} AS CatgHdrName  FROM prodSysCatgHdr AS pCatgHdr  ";
                sql += " INNER JOIN prodSysCatgDtl AS pCatgDtl ON pCatgHdr.sysCatgHdrID = pCatgDtl.sysCatgHdrID ";
                sql += " WHERE pCatgHdr.sysCatgHdrID = @CatgHdrID ORDER BY pCatgDtl.sysCatgSelSeq  ";

                sql = string.Format(sql, lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int)}))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(dr["CatgID"]), CatgName = BusinessUtility.GetString(dr["CatgName"]), CatgIsActive = BusinessUtility.GetBool(dr["sysCatgSelActive"]), CatgHrdID = BusinessUtility.GetInt(dr["CatgHdrID"]), CatgHdrName = BusinessUtility.GetString(dr["CatgHdrName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateCategory(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = " UPDATE prodSysCatgDtl set sysCatgSelValueFr = @sysCatgSelValueFr, sysCatgSelValueEn = @sysCatgSelValueEn, sysCatgSelActive = @sysCatgSelActive, sysLastUpdatedBy = @sysLastUpdatedBy,sysLasUpdatedAt=@sysLasUpdatedAt";
                sql += " WHERE sysCatgHdrID = @CatgHdrID and sysCatgDtlID = @sysCatgDtlID";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysCatgSelValueFr", this.CatgNameFr, MyDbType.String),
                    DbUtility.GetParameter("sysCatgSelValueEn", this.CatgNameEn, MyDbType.String),
                    DbUtility.GetParameter("sysCatgSelActive", this.CatgIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("sysLastUpdatedBy", this.CatgUpdatedBy, MyDbType.Int),
                    DbUtility.GetParameter("sysLasUpdatedAt", DateTime.Now, MyDbType.DateTime),
                     DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int),
                      DbUtility.GetParameter("sysCatgDtlID", this.CatgID, MyDbType.Int)});

            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool UpdateCategoryStatus(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = " UPDATE prodSysCatgDtl set sysCatgSelActive = @sysCatgSelActive, sysLastUpdatedBy = @sysLastUpdatedBy,sysLasUpdatedAt=@sysLasUpdatedAt";
                sql += " WHERE sysCatgHdrID = @CatgHdrID and sysCatgDtlID = @sysCatgDtlID";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysCatgSelActive", this.CatgIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("sysLastUpdatedBy", this.CatgUpdatedBy, MyDbType.Int),
                    DbUtility.GetParameter("sysLasUpdatedAt", DateTime.Now, MyDbType.DateTime),
                     DbUtility.GetParameter("CatgHdrID", this.CatgHrdID, MyDbType.Int),
                      DbUtility.GetParameter("sysCatgDtlID", this.CatgID, MyDbType.Int)});
                return true;

            }
            catch
            {
                throw;

            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean AddCategory(DbHelper dbHelp, int categoryHdrID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                string sqlCountCategory = " Select Count(*) as categoryCount from prodSysCatgDtl Where sysCatgHdrID = @sysCatgHdrID ";
                int countCategory = 0;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlCountCategory, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysCatgHdrID",categoryHdrID, MyDbType.Int)}))
                {
                    dr.Read();
                    countCategory = BusinessUtility.GetInt(dr["categoryCount"]);
                }

                string sql = " INSERT INTO prodSysCatgDtl (sysCatgHdrID, sysCatgSelValueFr, sysCatgSelValueEn, sysCatgSelSeq, sysCatgSelActive, sysLasUpdatedAt, sysLastUpdatedBy) VALUES (@sysCatgHdrID, @sysCatgSelValueFr, @sysCatgSelValueEn, @sysCatgSelSeq, @sysCatgSelActive, @sysLasUpdatedAt, @sysLastUpdatedBy) ";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysCatgHdrID", categoryHdrID, MyDbType.Int),
                    DbUtility.GetParameter("sysCatgSelValueFr", this.CatgNameFr, MyDbType.String),
                    DbUtility.GetParameter("sysCatgSelValueEn",this.CatgNameEn, MyDbType.String),
                    DbUtility.GetParameter("sysCatgSelSeq", countCategory +1, MyDbType.Int),
                    DbUtility.GetParameter("sysCatgSelActive",this.CatgIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("sysLasUpdatedAt",DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("sysLastUpdatedBy",this.CatgUpdatedBy, MyDbType.Int)
                });

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean AddAssociatedStyleInStyle(int iMasterID, int iCollectionID, string[] sAssociatedStyle)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM productMstAssociateStyle WHERE MasterID = @MasterID AND CollectionID = @CollectionID ";//AND MaterialID = @MaterialID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("CollectionID", iCollectionID, MyDbType.Int),
                });

                foreach (var item in sAssociatedStyle)
                {
                    string sql = " INSERT INTO productMstAssociateStyle (MasterID, CollectionID, Style) VALUES (@MasterID, @CollectionID, @Style) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("CollectionID", iCollectionID, MyDbType.Int),
                    DbUtility.GetParameter("Style", BusinessUtility.GetString( item), MyDbType.String),
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


    }
}
