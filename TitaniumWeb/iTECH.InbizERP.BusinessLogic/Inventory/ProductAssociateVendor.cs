﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductAssociateVendor
    {
        public int PrdID { get; set; }
        public int PrdVendorID { get; set; }
        public double PrdCostPrice { get; set; }

        //below propertied added on 2013-01-09 
        public double NoOfUnits { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int PurchaseOrderNo { get; set; }
        public string VendorRefNo { get; set; }

        public void Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlCheck = "SELECT count(*) FROM prdassociatevendor where prdID=@prdID AND prdVendorID=@prdVendorID";
            string sqlInsert = "INSERT INTO prdassociatevendor(prdID, prdVendorID, prdCostPrice, NoOfUnits,VendorRefNo) VALUES(@prdID, @prdVendorID, @prdCostPrice,@NoOfUnits,@VendorRefNo)";
            string sqlUpdate = "UPDATE prdassociatevendor SET prdCostPrice=@prdCostPrice, NoOfUnits=IFNULL(NoOfUnits, 0) + @NoOfUnits,VendorRefNo=@VendorRefNo WHERE prdID=@prdID AND prdVendorID=@prdVendorID";
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                    DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    //throw new Exception(CustomExceptionCodes.VENDOR_ALREADY_ASSIGN_TO_PRODUCT);
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                        DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int),
                        DbUtility.GetParameter("prdCostPrice", this.PrdCostPrice, MyDbType.Double),
                        DbUtility.GetParameter("NoOfUnits", this.NoOfUnits, MyDbType.Double),
                        DbUtility.GetParameter("VendorRefNo", this.VendorRefNo, MyDbType.String)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                        DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int),
                        DbUtility.GetParameter("prdCostPrice", this.PrdCostPrice, MyDbType.Double),
                        DbUtility.GetParameter("NoOfUnits", this.NoOfUnits, MyDbType.Double),
                        DbUtility.GetParameter("VendorRefNo", this.VendorRefNo, MyDbType.String)
                    });
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateUnitsOnReceiving(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlCheck = "SELECT count(*) FROM prdassociatevendor where prdID=@prdID AND prdVendorID=@prdVendorID";
            string sqlInsert = "INSERT INTO prdassociatevendor(prdID, prdVendorID, prdCostPrice, NoOfUnits, PurchaseDate, PurchaseOrderNo) VALUES(@prdID, @prdVendorID, @prdCostPrice,@NoOfUnits,@PurchaseDate, @PurchaseOrderNo)";
            string sqlUpdate = "UPDATE prdassociatevendor SET prdCostPrice=@prdCostPrice, NoOfUnits=IFNULL(NoOfUnits, 0) + @NoOfUnits,PurchaseDate=@PurchaseDate, PurchaseOrderNo=@PurchaseOrderNo WHERE prdID=@prdID AND prdVendorID=@prdVendorID";
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                    DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    //throw new Exception(CustomExceptionCodes.VENDOR_ALREADY_ASSIGN_TO_PRODUCT);
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                        DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int),
                        DbUtility.GetParameter("prdCostPrice", this.PrdCostPrice, MyDbType.Double),
                        DbUtility.GetParameter("NoOfUnits", this.NoOfUnits, MyDbType.Double),
                        DbUtility.GetParameter("PurchaseDate", this.PurchaseDate, MyDbType.DateTime),
                        DbUtility.GetParameter("PurchaseOrderNo", this.PurchaseOrderNo, MyDbType.Int)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                        DbUtility.GetParameter("prdVendorID", this.PrdVendorID, MyDbType.Int),
                        DbUtility.GetParameter("prdCostPrice", this.PrdCostPrice, MyDbType.Double),
                        DbUtility.GetParameter("NoOfUnits", this.NoOfUnits, MyDbType.Double),
                        DbUtility.GetParameter("PurchaseDate", this.PurchaseDate, MyDbType.DateTime),
                        DbUtility.GetParameter("PurchaseOrderNo", this.PurchaseOrderNo, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID)
        {
            pCol.Clear();
            string sql = "select prdVendorID,prdID,vendorName,prdCostPrice,NoOfUnits,PurchaseDate,PurchaseOrderNo,VendorRefNo from prdassociatevendor as pv ";
            sql += "  Inner Join vendor as v on v.vendorID=pv.prdVendorID where pv.prdID =@prdID";
            pCol.Add("@prdID", productID.ToString());
            return sql;
        }

        public void Delete(int productID, int vendorID)
        {
            string sql = "delete from prdassociatevendor  where prdID=@prdID AND prdVendorID=@prdVendorID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int),
                    DbUtility.GetParameter("prdVendorID", vendorID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
