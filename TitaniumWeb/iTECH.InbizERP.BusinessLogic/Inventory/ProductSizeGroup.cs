﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;


namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductSizeGroup
    {
        /* Used For Cava PRoduct Master */

        public string ColorEn { get; set; }
        public string ColorFr { get; set; }
        public string ColorName { get; set; }
        public int ColorID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string ShortName { get; set; }
        public string ColorGroupNameEn { get; set; }
        public string ColorGroupNameFr { get; set; }
        public int ColorGroupID { get; set; }
        public int ColorGroupIsActive { get; set; }
        public string ColorGroupName { get; set; }

        public string SizeGroupName { get; set; }
        public int SizeGroupID { get; set; }
        public int SizeGroupIsActive { get; set; }
        public string SizeName { get; set; }
        public int SizeID { get; set; }


        public Boolean SaveSizeGroup(DbHelper dbHelp, int userID, int iSizeGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iSizeGroupID > 0)
                {
                    string sql = " UPDATE prdSizeGroup SET SizeGroup = @SizeGroup, isActive = @SizeGroupIsActive, lastUpdatedby = @lastUpdatedby, lastUpdatedOn = @lastUpdatedOn  WHERE SizeGroupID = @SizeGroupID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeGroup", this.SizeGroupName, MyDbType.String),
                    DbUtility.GetParameter("SizeGroupID", iSizeGroupID, MyDbType.Int),
                    DbUtility.GetParameter("lastUpdatedby", userID, MyDbType.Int),
                    DbUtility.GetParameter("lastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("SizeGroupIsActive", this.SizeGroupIsActive, MyDbType.Int),
                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO prdSizeGroup (SizeGroup, createdBy, CreatedOn, isActive) VALUES (@SizeGroup, @createdBy, @CreatedOn, @SizeGroupIsActive) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeGroup", this.SizeGroupName, MyDbType.String),
                    //DbUtility.GetParameter("SizeGroupID", iSizeGroupID, MyDbType.Int),
                    DbUtility.GetParameter("createdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("SizeGroupIsActive", this.SizeGroupIsActive, MyDbType.Int),
                });
                    this.SizeGroupID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteSizeGroup(DbHelper dbHelp, int userID, int iSizeGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iSizeGroupID > 0)
                {
                    string sql = " UPDATE prdSizeGroup SET isActive = @SizeGroupIsActive , lastUpdatedby = @lastUpdatedby, lastUpdatedOn = @lastUpdatedOn WHERE SizeGroupID = @SizeGroupID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeGroupIsActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("lastUpdatedby", userID, MyDbType.Int),
                    DbUtility.GetParameter("lastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("SizeGroupID", iSizeGroupID, MyDbType.Int)
                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public List<ProductColorGroup> GetColorGroupList(DbHelper dbHelp, string lang)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        List<ProductColorGroup> lResult = new List<ProductColorGroup>();
        //        string sql = string.Format("SELECT ColorGroupHdrID AS ColorGroupID, ColorGroup{0} as ColorGroupName FROM prdColorGroupHdr WHERE ColorActive = 1 ", lang);

        //        using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
        //        {
        //            while (dr.Read())
        //            {
        //                lResult.Add(new ProductColorGroup { ColorGroupName = BusinessUtility.GetString(dr["ColorGroupName"]), ColorGroupID = BusinessUtility.GetInt(dr["ColorGroupID"]) });
        //            }
        //            return lResult;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public List<ProductSizeGroup> GetAllSizeGroupList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSizeGroup> lResult = new List<ProductSizeGroup>();
                string sql = string.Format("SELECT SizeGroupID AS SizeGroupID,  SizeGroup as SizeGroup, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdSizeGroup WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND SizeGroup like '%" + this.SearchKey + "%' ", lang);
                }
                if (this.ColorGroupID > 0)
                {
                    sql += "AND SizeGroupID = " + this.ColorGroupID;
                }


                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSizeGroup
                        {
                            SizeGroupName = BusinessUtility.GetString(dr["SizeGroup"]),
                            SizeGroupID = BusinessUtility.GetInt(dr["SizeGroupID"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            SizeGroupIsActive = BusinessUtility.GetInt(dr["IsActive"]),
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddSizeInSizeGroup(string sSizeGroup, int[] iSizeID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM productsizeCatg WHERE SizeCatgName = @SizeCatgName ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeCatgName", sSizeGroup, MyDbType.String),
                });

                foreach (var item in iSizeID)
                {
                    string sql = " INSERT INTO productsizeCatg (SizeCatgName, SizeID) VALUES (@SizeCatgName, @SizeID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeCatgName", sSizeGroup, MyDbType.String),
                    DbUtility.GetParameter("SizeID",BusinessUtility.GetInt( item), MyDbType.Int),
                    });
                }

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public Boolean DeleteSizeInGroupSize(string sSizeGroup, int iSizeID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM productsizeCatg WHERE SizeCatgName = @SizeCatgName AND SizeID = @SizeID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeCatgName", sSizeGroup, MyDbType.String),
                    DbUtility.GetParameter("SizeID", iSizeID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductSizeGroup> GetGroupSize(DbHelper dbHelp, string sSizeGroup, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductSizeGroup> lResult = new List<ProductSizeGroup>();


                string sql = " SELECT  PSC.SizeCatgName AS SizeGroupName, PS.SizeID, PS.Size{0} AS SizeName   FROM productsizeCatg  PSC  ";
                sql += " INNER JOIN ProductSize PS ON PS.SizeID = PSC.SizeID AND PS.SizeActive = 1 ";
                sql += " WHERE  PSC.SizeCatgName = @SizeCatgName  ";
                sql += " GROUP BY PSC.SizeCatgName, PS.SizeID ";
                sql = string.Format(sql, Globals.CurrentAppLanguageCode);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SizeCatgName", sSizeGroup, MyDbType.String)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductSizeGroup { SizeGroupName = BusinessUtility.GetString(dr["SizeGroupName"]), SizeID = BusinessUtility.GetInt(dr["SizeID"]), SizeName = BusinessUtility.GetString(dr["SizeName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public string GetSizeGroupSql(string lang)
        {
            string sql = string.Format("SELECT SizeGroupID AS SizeGroupID,  SizeGroup as SizeGroup, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdSizeGroup WHERE 1=1  ", lang);
            if (BusinessUtility.GetString(this.SearchKey) != "")
            {
                sql += string.Format(" AND SizeGroup like '%" + this.SearchKey + "%' ", lang);
            }
            if (this.ColorGroupID > 0)
            {
                sql += "AND SizeGroupID = " + this.ColorGroupID;
            }
            return sql;
        }
    }
}
