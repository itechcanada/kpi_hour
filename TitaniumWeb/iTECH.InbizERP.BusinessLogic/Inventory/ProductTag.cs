﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductTag
    {
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private int _ProductID;

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        private int _ReceivingID;

        public int ReceivingID
        {
            get { return _ReceivingID; }
            set { _ReceivingID = value; }
        }
        private int _SOID;

        public int SOID
        {
            get { return _SOID; }
            set { _SOID = value; }
        }
        private int _POID;

        public int POID
        {
            get { return _POID; }
            set { _POID = value; }
        }
        private string _WareHouseCode;

        public string WareHouseCode
        {
            get { return _WareHouseCode; }
            set { _WareHouseCode = value; }
        }
        private string _Tag;

        public string Tag
        {
            get { return _Tag; }
            set { _Tag = value; }
        }
        private int _Quantity;

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        private DateTime _TagCreatedDate;

        public DateTime TagCreatedDate
        {
            get { return _TagCreatedDate; }
            set { _TagCreatedDate = value; }
        }


        public bool Insert(DbHelper dbHelp) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sqlInert = "INSERT INTO prdtags (ProductID, ReceivingID, SOID, POID, WareHouseCode, Tag, Quantity, TagCreatedDate) VALUES";
            sqlInert += "(@ProductID, @ReceivingID, @SOID, @POID, @WareHouseCode, @Tag, @Quantity, @TagCreatedDate)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                                     DbUtility.GetParameter("ReceivingID", this.ReceivingID, MyDbType.Int),
                                     DbUtility.GetParameter("SOID", this.SOID, MyDbType.Int),
                                     DbUtility.GetParameter("POID", this.POID, MyDbType.Int),
                                     DbUtility.GetParameter("WareHouseCode", this.WareHouseCode, MyDbType.String),
                                     DbUtility.GetParameter("Tag", this.Tag, MyDbType.String),
                                     DbUtility.GetParameter("Quantity", this.Quantity, MyDbType.Int),
                                     DbUtility.GetParameter("TagCreatedDate", DateTime.Now, MyDbType.DateTime)
                                 };
            
            try
            {
                dbHelp.ExecuteNonQuery(sqlInert, CommandType.Text, p);
                return true;
            }
            catch
            {

                throw;
            }
            finally {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Returns the sequence PONO-SONO-WHS
        /// </summary>
        /// <param name="poid"></param>
        /// <returns></returns>
        public string GetUniqueTagWithoutProductID(DbHelper dbHelp, int poid) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT p.poID, p.poWhsCode, p.poForSoNo FROM purchaseorders p WHERE p.poID = @poID";            
            
            try
            {
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poID", poid, MyDbType.Int) }))
                {
                    string poID = "", whs = "", soID = "";
                    if (dr.Read())
                    {
                        poID = BusinessUtility.GetString(dr[0]);
                        whs = BusinessUtility.GetString(dr[1]);
                        soID = BusinessUtility.GetString(dr[2]);
                    }
                    return string.Format("{0}-{1}-{2}", poid, !string.IsNullOrEmpty(soID) ? soID : "NA", !string.IsNullOrEmpty(whs) ? whs : "NA");
                }                
            }
            catch
            {

                throw;
            }
            finally {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetTagsByProductID(int productid) {
            string sql = "SELECT Tag FROM prdtags WHERE ProductID = @ProductID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productid, MyDbType.Int) });
                while (dr.Read()) {
                    sb.Append(BusinessUtility.GetString(dr[0])).Append("^");                    
                }
                return sb.ToString();
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetTags(int productID, string whsCode) {
            string sql = "SELECT ID, ProductID,ReceivingID, SOID, POID, WareHouseCode, Tag, Quantity, TagCreatedDate FROM prdtags WHERE ProductID=@ProductID AND WareHouseCode=@WareHouseCode";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productID, MyDbType.Int), DbUtility.GetParameter("WareHouseCode", whsCode, MyDbType.String) });
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int prdID, string whsCode) {
            pCol.Clear();
            string sql = "SELECT ID, ProductID,ReceivingID, SOID, POID, WareHouseCode, Tag, Quantity, TagCreatedDate FROM prdtags WHERE ProductID=@ProductID";
            pCol.Add("@ProductID", prdID.ToString());
            if (!string.IsNullOrEmpty(whsCode)) {
                sql += " AND WareHouseCode=@WareHouseCode";
                pCol.Add("@WareHouseCode", whsCode);
            }

            return sql;
        }
    }

    public class PrintSchema
    {
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; } 
    }
}
