﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductColorGroup
    {

        /* Used For Cava PRoduct Master */

        public string ColorEn { get; set; }
        public string ColorFr { get; set; }
        public string ColorName { get; set; }
        public int ColorID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string ShortName { get; set; }
        public string ColorGroupNameEn { get; set; }
        public string ColorGroupNameFr { get; set; }
        public int ColorGroupID { get; set; }
        public int ColorGroupIsActive { get; set; }
        public string ColorGroupName { get; set; }


        public Boolean SaveColorGroup(DbHelper dbHelp, int userID, int iColorGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iColorGroupID > 0)
                {
                    string sql = " UPDATE prdColorGroupHdr SET ColorGroupFr = @ColorGroupFr, ColorGroupEn = @ColorGroupEn, isActive = @ColorGroupActive, lastUpdatedby = @ColorLastUpdBy, lastUpdatedOn = @ColorLastUpdOn  WHERE ColorGroupHdrID = @ColorGroupHdrID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupFr", this.ColorGroupNameFr, MyDbType.String),
                    DbUtility.GetParameter("ColorGroupEn", this.ColorGroupNameEn, MyDbType.String),
                    DbUtility.GetParameter("ColorGroupHdrID", iColorGroupID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColorGroupActive", this.ColorGroupIsActive, MyDbType.Int),
                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO prdColorGroupHdr (ColorGroupFr, ColorGroupEn, createdBy, CreatedOn, isActive) VALUES (@ColorGroupFr, @ColorGroupEn,  @createdBy, @CreatedOn, @ColorGroupActive) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupFr", this.ColorGroupNameFr, MyDbType.String),
                    DbUtility.GetParameter("ColorGroupEn", this.ColorGroupNameEn, MyDbType.String),
                    DbUtility.GetParameter("createdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColorGroupActive", this.ColorGroupIsActive, MyDbType.Int),
                });
                    this.ColorGroupID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean DeleteColorGroup(DbHelper dbHelp, int userID, int iColorGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (iColorGroupID > 0)
                {
                    string sql = " UPDATE prdColorGroupHdr SET isActive = @ColorGroupActive , lastUpdatedby = @ColorLastUpdBy, lastUpdatedOn = @ColorLastUpdOn WHERE ColorGroupHdrID = @ColorGroupHdrID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColorGroupHdrID", iColorGroupID, MyDbType.Int)
                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public List<ProductColorGroup> GetColorGroupList(DbHelper dbHelp, string lang)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        List<ProductColorGroup> lResult = new List<ProductColorGroup>();
        //        string sql = string.Format("SELECT ColorGroupHdrID AS ColorGroupID, ColorGroup{0} as ColorGroupName FROM prdColorGroupHdr WHERE ColorActive = 1 ", lang);

        //        using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
        //        {
        //            while (dr.Read())
        //            {
        //                lResult.Add(new ProductColorGroup { ColorGroupName = BusinessUtility.GetString(dr["ColorGroupName"]), ColorGroupID = BusinessUtility.GetInt(dr["ColorGroupID"]) });
        //            }
        //            return lResult;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public List<ProductColorGroup> GetAllColorGroupList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductColorGroup> lResult = new List<ProductColorGroup>();
                string sql = string.Format("SELECT ColorGroupHdrID AS ColorGroupID, ColorGroupFr, ColorGroupEn, ColorGroup{0} as ColorGroupName, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdColorGroupHdr WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND ColorGroup{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.ColorGroupID > 0)
                {
                    sql += "AND ColorGroupHdrID = " + this.ColorGroupID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductColorGroup
                        {
                            ColorGroupName = BusinessUtility.GetString(dr["ColorGroupName"]),
                            ColorGroupID = BusinessUtility.GetInt(dr["ColorGroupID"]),
                            ColorGroupNameEn = BusinessUtility.GetString(dr["ColorGroupEn"]),
                            ColorGroupNameFr = BusinessUtility.GetString(dr["ColorGroupFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            ColorGroupIsActive = BusinessUtility.GetInt(dr["IsActive"]),
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddColorInGroup(int iColorGroupID, int[] iColorID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                //foreach (var item in iColorID)
                //{
                string sqlDelete = "DELETE FROM prdColorGroupDtl WHERE ColorGroupHdrID = @ColorGroupHdrID ";//AND ColorID = @ColorID 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupHdrID", iColorGroupID, MyDbType.Int),
                    //DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                //}


                foreach (var item in iColorID)
                {
                    string sql = " INSERT INTO prdColorGroupDtl (ColorGroupHdrID, ColorID) VALUES (@ColorGroupHdrID, @ColorID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupHdrID", iColorGroupID, MyDbType.Int),
                    DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public Boolean DeleteColorInGroup(int iColorGroupID, int iColorID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM prdColorGroupDtl WHERE ColorGroupHdrID = @ColorGroupHdrID AND ColorID = @ColorID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupHdrID", iColorGroupID, MyDbType.Int),
                    DbUtility.GetParameter("ColorID", iColorID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductColorGroup> GetGroupColor(DbHelper dbHelp, int iColorGroupID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductColorGroup> lResult = new List<ProductColorGroup>();
                string sql = string.Format("SELECT PM.ColorID AS ColorID, PM.Color{0} as ColorName, PM.ShortName FROM ProductColor PM INNER JOIN prdColorGroupDtl PMsT ON PMsT.ColorID = PM.ColorID WHERE PMsT.ColorGroupHdrID = @ColorGroupID AND PM.ColorActive = 1 ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorGroupID", iColorGroupID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductColorGroup { ColorName = BusinessUtility.GetString(dr["ColorName"]), ColorID = BusinessUtility.GetInt(dr["ColorID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public string GetColorGroupSql(string lang)
        {
            string sql = string.Format("SELECT ColorGroupHdrID AS ColorGroupID, ColorGroupFr, ColorGroupEn, ColorGroup{0} as ColorGroupName, isActive AS IsActive, CASE isActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl  FROM prdColorGroupHdr WHERE 1=1  ", lang);
            if (BusinessUtility.GetString(this.SearchKey) != "")
            {
                sql += string.Format(" AND ColorGroup{0} like '%" + this.SearchKey + "%' ", lang);
            }
            if (this.ColorGroupID > 0)
            {
                sql += "AND ColorGroupID = " + this.ColorGroupID;
            }
            return sql;
        }

    }
}
