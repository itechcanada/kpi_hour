﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InvoiceItems
    {
        public int InvItemID { get; set; }
        public int Invoices_invID { get; set; }
        public int InvProductID { get; set; }
        public double InvProductQty { get; set; }
        public double InvProductUnitPrice { get; set; }
        public int InvProductTaxGrp { get; set; }
        public int InvProductDiscount { get; set; }
        public string InvProdIDDesc { get; set; }
        public string InvProductDiscountType { get; set; }
        public int InvReservationItemID { get; set; }
        public int InvGuestID { get; set; }

        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }

        //public void Insert(DbHelper dbHelp)
        //{
        //    try
        //    {
        //        string sql = "INSERT INTO invoiceitems (invoices_invID,invProductID,invProductQty,invProductUnitPrice,";
        //        sql += " invProductTaxGrp,invProductDiscount,invProdIDDesc,invProductDiscountType,invReservationItemID,invGuestID,";
        //        sql += "Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5)";
        //        sql += " VALUES (@invoices_invID,@invProductID,@invProductQty,@invProductUnitPrice,";
        //        sql += " @invProductTaxGrp,@invProductDiscount,@invProdIDDesc,@invProductDiscountType,@invReservationItemID,@invGuestID,";
        //        sql += " @Tax1,@TaxDesc1,@Tax2,@TaxDesc2,@Tax3,@TaxDesc3,@Tax4,@TaxDesc4,@Tax5,@TaxDesc5)";

        //        int? ritmID = this.InvReservationItemID > 0 ? (int?)this.InvReservationItemID : null;
        //        int? gsid = this.InvGuestID > 0 ? (int?)this.InvGuestID : null;
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("invoices_invID", this.Invoices_invID, MyDbType.Int),
        //            DbUtility.GetParameter("invProductID", this.InvProductID, MyDbType.Int),
        //            DbUtility.GetParameter("invProductQty", this.InvProductQty, MyDbType.Double),
        //            DbUtility.GetParameter("invProductUnitPrice", this.InvProductUnitPrice, MyDbType.Double),
        //            DbUtility.GetParameter("invProductTaxGrp", this.InvProductTaxGrp, MyDbType.Double),
        //            DbUtility.GetParameter("invProductDiscount", this.InvProductDiscount, MyDbType.Int),
        //            DbUtility.GetParameter("invProdIDDesc", this.InvProdIDDesc, MyDbType.String),
        //            DbUtility.GetParameter("invProductDiscountType", this.InvProductDiscountType, MyDbType.String),
        //            DbUtility.GetIntParameter("invReservationItemID", ritmID),
        //            DbUtility.GetIntParameter("invGuestID", gsid),
        //            DbUtility.GetParameter("Tax1", this.Tax1, MyDbType.Double),
        //            DbUtility.GetParameter("Tax2", this.Tax2, MyDbType.Double),
        //            DbUtility.GetParameter("Tax3", this.Tax3, MyDbType.Double),
        //            DbUtility.GetParameter("Tax4", this.Tax4, MyDbType.Double),
        //            DbUtility.GetParameter("Tax5", this.Tax5, MyDbType.Double),
        //            DbUtility.GetParameter("TaxDesc1", this.TaxDesc1, MyDbType.String),
        //            DbUtility.GetParameter("TaxDesc2", this.TaxDesc2, MyDbType.String),
        //            DbUtility.GetParameter("TaxDesc3", this.TaxDesc3, MyDbType.String),
        //            DbUtility.GetParameter("TaxDesc4", this.TaxDesc4, MyDbType.String),
        //            DbUtility.GetParameter("TaxDesc5", this.TaxDesc5, MyDbType.String)
        //        });
        //    }
        //    catch 
        //    {

        //        throw;
        //    }
        //}

        //public void Insert()
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        this.Insert(dbHelp);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void AddInvoiceItemsFromOrder(DbHelper dbHelp, int orderID, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                OrderReturn ortn = new OrderReturn();
                var lstOrderItems = new OrderItems().GetOrderItemList(dbHelp, orderID);

                string sqlInsertII = "INSERT INTO invoiceitems(invoices_invID,invProductID,invProductQty,invProductUnitPrice,invProductTaxGrp,invProductDiscount,";
                sqlInsertII += "invProdIDDesc,invProductDiscountType,invReservationItemID,invGuestID,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,";
                sqlInsertII += "Tax5,TaxDesc5,TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5) ";
                sqlInsertII += " VALUES(@invoices_invID,@invProductID,@invProductQty,@invProductUnitPrice,@invProductTaxGrp,@invProductDiscount,";
                sqlInsertII += "@invProdIDDesc,@invProductDiscountType,@invReservationItemID,@invGuestID,@Tax1,@TaxDesc1,@Tax2,@TaxDesc2,@Tax3,@TaxDesc3,@Tax4,@TaxDesc4,";
                sqlInsertII += "@Tax5,@TaxDesc5,@TaxCalculated1,@TaxCalculated2,@TaxCalculated3,@TaxCalculated4,@TaxCalculated5) ";
                //sqlInsertII += "SELECT @InvoiceID AS invoices_invID, ordProductID AS invProductID,ordProductQty AS invProductQty,ordProductUnitPrice AS invProductUnitPrice,";
                //sqlInsertII += "ordProductTaxGrp AS invProductTaxGrp,ordProductDiscount AS invProductDiscount,orderItemDesc AS invProdIDDesc,ordProductDiscountType AS invProductDiscountType,";
                //sqlInsertII += "ordReservationItemID AS invReservationItemID,ordGuestID AS invGuestID,Tax1,TaxDesc1,";
                //sqlInsertII += "Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5 FROM orderitems WHERE ordID=@OrderID";

                int invoiceItemID = 0;
                List<InvoiceReturn> lstReturns;

                foreach (var item in lstOrderItems)
                {
                    //Inser order item equivelent to order item
                    dbHelp.ExecuteNonQuery(sqlInsertII, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("invoices_invID", invoiceID, MyDbType.Int),
                        DbUtility.GetParameter("invProductID", item.OrdProductID, MyDbType.Int),
                        DbUtility.GetParameter("invProductQty", item.OrdProductQty, MyDbType.Double),
                        DbUtility.GetParameter("invProductUnitPrice", item.OrdProductUnitPrice, MyDbType.Double),
                        DbUtility.GetParameter("invProductTaxGrp", item.OrdProductTaxGrp, MyDbType.Int),
                        DbUtility.GetParameter("invProductDiscount", item.OrdProductDiscount, MyDbType.Int),
                        DbUtility.GetParameter("invProdIDDesc", item.OrderItemDesc, MyDbType.String),
                        DbUtility.GetParameter("invProductDiscountType", item.OrdProductDiscountType, MyDbType.String),
                        DbUtility.GetParameter("invReservationItemID", item.OrdReservationItemID, MyDbType.Int),
                        DbUtility.GetParameter("invGuestID", item.OrdGuestID, MyDbType.Int),
                        DbUtility.GetParameter("Tax1", item.Tax1, MyDbType.Double),
                        DbUtility.GetParameter("Tax2", item.Tax2, MyDbType.Double),
                        DbUtility.GetParameter("Tax3", item.Tax3, MyDbType.Double),
                        DbUtility.GetParameter("Tax4", item.Tax4, MyDbType.Double),
                        DbUtility.GetParameter("Tax5", item.Tax5, MyDbType.Double),
                        DbUtility.GetParameter("TaxDesc1", item.TaxDesc1, MyDbType.String),
                        DbUtility.GetParameter("TaxDesc2", item.TaxDesc2, MyDbType.String),
                        DbUtility.GetParameter("TaxDesc3", item.TaxDesc3, MyDbType.String),
                        DbUtility.GetParameter("TaxDesc4", item.TaxDesc4, MyDbType.String),
                        DbUtility.GetParameter("TaxDesc5", item.TaxDesc5, MyDbType.String),
                        DbUtility.GetParameter("TaxCalculated1", item.TaxCalculated1, MyDbType.Double),
                        DbUtility.GetParameter("TaxCalculated2", item.TaxCalculated2, MyDbType.Double),
                        DbUtility.GetParameter("TaxCalculated3", item.TaxCalculated3, MyDbType.Double),
                        DbUtility.GetParameter("TaxCalculated4", item.TaxCalculated4, MyDbType.Double),
                        DbUtility.GetParameter("TaxCalculated5", item.TaxCalculated5, MyDbType.Double),
                    });
                    invoiceItemID = dbHelp.GetLastInsertID();
                    lstReturns = ortn.GetInvoiceReturnsList(dbHelp, item.OrderItemID, invoiceItemID, invoiceID);
                    //Insert all returns that were found for orderites
                    foreach (var r in lstReturns)
                    {
                        r.ReturnAndGetWriteOff(dbHelp, invoiceItemID, r.ReturnProductQty, r.ReturnBy);
                    }
                }

                //Check for returns & need to associate appropriate invoiceitems id for returned item                                
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateTax(DbHelper dbHelp, int invItemID, int taxGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE invoiceitems SET invProductTaxGrp=@TaxGroupID, Tax1=@Tax1, TaxDesc1=@TaxDesc1,";
                sql += "Tax2=@Tax2, TaxDesc2=@TaxDesc2,Tax3=@Tax3, TaxDesc3=@TaxDesc3,Tax4=@Tax4, TaxDesc4=@TaxDesc4,";
                sql += "Tax5=@Tax5,TaxDesc5=@TaxDesc5,TaxCalculated1=@TaxCalculated1,TaxCalculated2=@TaxCalculated2,TaxCalculated3=@TaxCalculated3,TaxCalculated4=@TaxCalculated4,TaxCalculated5=@TaxCalculated5 WHERE invItemID=@invItemID";

                string sqlAmount = "SELECT TaxGroupID, Amount FROM  vw_invoice_item_amount WHERE InvoiceItemID=@InvoiceItemID";
                Tax tx = new Tax();
                double amount = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceItemID", invItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        amount = BusinessUtility.GetDouble(dr["Amount"]);
                        if (taxGroupID <= 0)
                        {
                            taxGroupID = BusinessUtility.GetInt(dr["TaxGroupID"]);
                        }
                    }
                }
                if (taxGroupID > 0)
                {
                    tx.PopulateObject(dbHelp, taxGroupID, amount);
                }
                int? txGrp = taxGroupID > 0 ? (int?)taxGroupID : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("TaxGroupID", txGrp),
                    DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                    DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                    DbUtility.GetParameter("Tax3", tx.Tax3, MyDbType.Double),
                    DbUtility.GetParameter("Tax4", tx.Tax4, MyDbType.Double),
                    DbUtility.GetParameter("Tax5", tx.Tax5, MyDbType.Double),
                    DbUtility.GetParameter("TaxDesc1", tx.TaxDesc1, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc2", tx.TaxDesc2, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc3", tx.TaxDesc3, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc4", tx.TaxDesc4, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc5", tx.TaxDesc5, MyDbType.String),
                    DbUtility.GetParameter("invItemID", invItemID, MyDbType.Int),
                    DbUtility.GetParameter("TaxCalculated1", tx.TaxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", tx.TaxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", tx.TaxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", tx.TaxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", tx.TaxCalculated5, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public DataTable GetInvoiceItems(int invID)
        //{
        //    string strSQL = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID,";
        //    strSQL += " p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, ";
        //    strSQL += " invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, ";
        //    strSQL += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else ";
        //    strSQL += "  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE ";
        //    strSQL += " (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, ";
        //    strSQL += " invProductUnitPrice AS IninvProductUnitPrice,";
        //    strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, i.invReservationItemID, i.invGuestID ";
        //    strSQL += " FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID ";
        //    strSQL += " inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp ";
        //    strSQL += " where i.invoices_invID=@invoices_invID";
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public IDataReader GetInvoiceItemsReader(DbHelper dbHelp, int invID)
        {
            string strSQL = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID,";
            strSQL += " p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, ";
            strSQL += " invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, ";
            strSQL += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else ";
            strSQL += "  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE ";
            strSQL += " (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, ";
            strSQL += " invProductUnitPrice AS IninvProductUnitPrice,";
            strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, i.invReservationItemID, i.invGuestID, ri.ReservationID, prt.PartnerLongName, o.invDiscount, o.invDiscountType, pc.Color{0} AS Color, Size{0} AS Size  ";
            strSQL += " FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID ";
            //strSQL += " inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp ";
            strSQL += " inner join products p on i.invProductID=p.productID  ";
            strSQL += " INNER JOIN ProductClothDesc AS pcd ON pcd.ProductId = p.productID  ";
            strSQL += " left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp ";
            strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.invReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.invGuestID";
            strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = pcd.Size INNER JOIN ProductColor AS pc ON pc.ColorID = pcd.Color ";
            //strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.invReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.invGuestID";
            //strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = p.SizeID INNER JOIN ProductColor AS pc ON pc.ColorID = p.ColID ";
            strSQL += " where i.invoices_invID=@invoices_invID";

            //string strSQL = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID,";
            //strSQL += " p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, ";
            //strSQL += " invProductQty, (invProductUnitPrice ) as invProductUnitPrice, invCurrencyCode, ";
            //strSQL += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice ) - i.invProductDiscount else ";
            //strSQL += "  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice ) ELSE ";
            //strSQL += " (invProductQty * invProductUnitPrice ) - ((invProductQty * invProductUnitPrice ) * i.invProductDiscount/100) END End  as amount, ";
            //strSQL += " invProductUnitPrice AS IninvProductUnitPrice,";
            //strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, i.invReservationItemID, i.invGuestID, ri.ReservationID, prt.PartnerLongName, o.invDiscount, o.invDiscountType, pc.Color{0} AS Color, Size{0} AS Size  ";
            //strSQL += " FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID ";
            //strSQL += " inner join products p on i.invProductID=p.productID  ";
            //strSQL += " INNER JOIN ProductClothDesc AS pcd ON pcd.ProductId = p.productID  ";
            //strSQL += " left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp ";
            //strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.invReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.invGuestID";
            //strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = pcd.Size INNER JOIN ProductColor AS pc ON pc.ColorID = pcd.Color ";
            //strSQL += " where i.invoices_invID=@invoices_invID";

            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            try
            {
                return dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
        }


        public DataTable GetInvoiceItemsDataTable(DbHelper dbHelp, int invID)
        {
            string strSQL = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID,";
            strSQL += " p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, ";
            strSQL += " invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, ";
            strSQL += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else ";
            strSQL += "  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE ";
            strSQL += " (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, ";
            strSQL += " invProductUnitPrice AS IninvProductUnitPrice,";
            strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, i.invReservationItemID, i.invGuestID, ri.ReservationID, prt.PartnerLongName, o.invDiscount, o.invDiscountType, pc.Color{0} AS Color, Size{0} AS Size, i.TaxDesc1, i.TaxDesc2, i.TaxDesc3, i.TaxDesc4, i.TaxDesc5  ";
            strSQL += " FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID ";
            strSQL += " inner join products p on i.invProductID=p.productID ";
            strSQL += " INNER JOIN productclothdesc AS pcd ON pcd.productID = p.productID  ";
            strSQL += " left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp  ";
            strSQL += " LEFT OUTER JOIN z_reservation_items ri ON ri.ReservationItemID=i.invReservationItemID LEFT OUTER JOIN partners prt ON prt.PartnerID=i.invGuestID";
            strSQL += "  INNER JOIN ProductSize AS ps ON ps.SizeID = pcd.Size INNER JOIN ProductColor AS pc ON pc.ColorID = pcd.Color ";
            strSQL += " where i.invoices_invID=@invoices_invID";

            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
        }


        public List<InvoiceItems> GetInvoiceItemList(DbHelper dbHelp, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<InvoiceItems> lResult = new List<InvoiceItems>();
            string sql = "SELECT * FROM invoiceitems WHERE invoices_invID=@InvoiceID";
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        InvoiceItems it = new InvoiceItems();
                        it.InvProdIDDesc = BusinessUtility.GetString(dr["invProdIDDesc"]);
                        it.InvItemID = BusinessUtility.GetInt(dr["invItemID"]);
                        it.InvGuestID = BusinessUtility.GetInt(dr["invGuestID"]);
                        it.Invoices_invID = BusinessUtility.GetInt(dr["invoices_invID"]);
                        it.InvProductDiscount = BusinessUtility.GetInt(dr["invProductDiscount"]);
                        it.InvProductDiscountType = BusinessUtility.GetString(dr["invProductDiscountType"]);
                        it.InvProductID = BusinessUtility.GetInt(dr["invProductID"]);
                        it.InvProductQty = BusinessUtility.GetDouble(dr["invProductQty"]);
                        it.InvProductTaxGrp = BusinessUtility.GetInt(dr["invProductTaxGrp"]);
                        it.InvProductUnitPrice = BusinessUtility.GetDouble(dr["invProductUnitPrice"]);
                        it.InvReservationItemID = BusinessUtility.GetInt(dr["invReservationItemID"]);

                        it.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        it.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        it.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        it.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        it.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);

                        it.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        it.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        it.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        it.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        it.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                        lResult.Add(it);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateReservationInvoiceItem(DbHelper dbHelp, int reservationItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM invoiceitems WHERE invReservationItemID=@invReservationItemID";

            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("invReservationItemID", reservationItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.InvItemID = BusinessUtility.GetInt(dr["invItemID"]);
                        this.Invoices_invID = BusinessUtility.GetInt(dr["invoices_invID"]);
                        this.InvProductID = BusinessUtility.GetInt(dr["invProductID"]);
                        this.InvProductQty = BusinessUtility.GetDouble(dr["invProductQty"]);
                        this.InvProductUnitPrice = BusinessUtility.GetDouble(dr["invProductUnitPrice"]);
                        this.InvProductTaxGrp = BusinessUtility.GetInt(dr["invProductTaxGrp"]);
                        this.InvProductDiscount = BusinessUtility.GetInt(dr["invProductDiscount"]);
                        this.InvProdIDDesc = BusinessUtility.GetString(dr["invProdIDDesc"]);
                        this.InvProductDiscountType = BusinessUtility.GetString(dr["invProductDiscountType"]);
                        this.InvReservationItemID = BusinessUtility.GetInt(dr["invReservationItemID"]);
                        this.InvGuestID = BusinessUtility.GetInt(dr["invGuestID"]);

                        this.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        this.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        this.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        this.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        this.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);

                        this.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        this.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        this.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        this.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        this.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public void MoveReservation(DbHelper dbHelp)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    string sqlUpdate = "UPDATE invoiceitems SET invProductID=@invProductID, invProdIDDesc=@invProdIDDesc,invProductUnitPrice=@invProductUnitPrice WHERE invItemID=@invItemID";            
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("invProductID", this.InvProductID, MyDbType.Int),
        //            DbUtility.GetParameter("invProdIDDesc", this.InvProdIDDesc, MyDbType.String),
        //            DbUtility.GetParameter("invItemID", this.InvItemID, MyDbType.Int),
        //            DbUtility.GetParameter("invProductUnitPrice", this.InvProductUnitPrice, MyDbType.Double)
        //        });
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if(mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void ChangeGuest(DbHelper dbHelp, int guestId, int reservationItmID)
        {
            string sql = "UPDATE invoiceitems SET invGuestID=@invGuestID WHERE invReservationItemID=@invReservationItemID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invGuestID", guestId, MyDbType.Int),
                    DbUtility.GetParameter("invReservationItemID", reservationItmID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
        }
    }
}
