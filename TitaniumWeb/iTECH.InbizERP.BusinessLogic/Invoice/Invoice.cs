﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Invoice
    {
        public int InvID { get; set; }
        public string InvCustType { get; set; }
        public int InvCustID { get; set; }
        public DateTime InvCreatedOn { get; set; }
        public string InvStatus { get; set; }
        public string InvComment { get; set; }
        public DateTime InvLastUpdatedOn { get; set; }
        public int InvLastUpdateBy { get; set; }
        public double InvShpCost { get; set; }
        public string InvCurrencyCode { get; set; }
        public double InvCurrencyExRate { get; set; }
        public string InvCustPO { get; set; }
        public string InvShpWhsCode { get; set; }
        public int InvForOrderNo { get; set; }
        public int InvCompanyID { get; set; }
        public int InvRefNo { get; set; }
        public string InvRefType { get; set; }
        public int InvTypeCommission { get; set; }
        public double InvDiscount { get; set; }
        public string InvDiscountType { get; set; }
        public string InvRegCode { get; set; }

        public string InvCustName { get; set; }
        public string InvSaleRep { get; set; }


        //public void Insert(int userID)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        this.Insert(dbHelp, userID);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}        

        public void Insert(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "INSERT INTO invoices (invCustType,invCustID,invCreatedOn,invStatus,invComment,invLastUpdatedOn,invLastUpdateBy,";
            sql += " invShpCost,invCurrencyCode,invCurrencyExRate,invCustPO,invShpWhsCode,invForOrderNo,invCompanyID,InvRefNo,invRefType,invTypeCommission,invDiscount,invDiscountType,invRegCode)";
            sql += " VALUES(@invCustType,@invCustID,@invCreatedOn,@invStatus,@invComment,@invLastUpdatedOn,@invLastUpdateBy,";
            sql += " @invShpCost,@invCurrencyCode,@invCurrencyExRate,@invCustPO,@invShpWhsCode,@invForOrderNo,@invCompanyID,@InvRefNo,@invRefType,@invTypeCommission,@invDiscount,@invDiscountType,@invRegCode)";
            try
            {
                if (IsInvoiceExist(this.InvForOrderNo, this.InvRefType))
                {
                    throw new Exception(CustomExceptionCodes.INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER);
                }
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invCustType", this.InvCustType, MyDbType.String),
                    DbUtility.GetParameter("invCustID", this.InvCustID, MyDbType.Int),
                    DbUtility.GetParameter("invCreatedOn", this.InvCreatedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("invStatus", this.InvStatus, MyDbType.String),
                    DbUtility.GetParameter("invComment", this.InvComment, MyDbType.String),
                    DbUtility.GetParameter("invLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("invLastUpdateBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("invShpCost", this.InvShpCost, MyDbType.Double),
                    DbUtility.GetParameter("invCurrencyCode", this.InvCurrencyCode, MyDbType.String),
                    DbUtility.GetParameter("invCurrencyExRate", this.InvCurrencyExRate, MyDbType.Double),
                    DbUtility.GetParameter("invCustPO", this.InvCustPO, MyDbType.String),
                    DbUtility.GetParameter("invShpWhsCode", this.InvShpWhsCode, MyDbType.String),
                    DbUtility.GetParameter("invForOrderNo", this.InvForOrderNo, MyDbType.Int),
                    DbUtility.GetParameter("invCompanyID", this.InvCompanyID, MyDbType.Int),
                    DbUtility.GetParameter("InvRefNo", this.InvRefNo, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", this.InvRefType, MyDbType.String),
                    DbUtility.GetParameter("invTypeCommission", this.InvTypeCommission, MyDbType.Int),
                    DbUtility.GetParameter("invDiscount", this.InvDiscount, MyDbType.Double),
                    DbUtility.GetParameter("invDiscountType", this.InvDiscountType, MyDbType.String),
                    DbUtility.GetParameter("invRegCode", this.InvRegCode, MyDbType.String)
                });
                this.InvID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Update(int userID)
        {
            string sql = "UPDATE invoices SET invCreatedOn=@invCreatedOn, invStatus=@invStatus,invComment=@invComment,invLastUpdatedOn=@invLastUpdatedOn,";
            sql += " invLastUpdateBy=@invLastUpdateBy,invShpCost=@invShpCost,";
            sql += " invCustPO=@invCustPO,invShpWhsCode=@invShpWhsCode,";
            sql += " invCompanyID=@invCompanyID,InvRefNo=@InvRefNo,invRefType=@invRefType,invTypeCommission=@invTypeCommission  WHERE invID=@invID";

            DbHelper dBhelp = new DbHelper();
            try
            {
                dBhelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invCreatedOn", this.InvCreatedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("invStatus", this.InvStatus, MyDbType.String),
                    DbUtility.GetParameter("invComment", this.InvComment, MyDbType.String),
                    DbUtility.GetParameter("invLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("invLastUpdateBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("invShpCost", this.InvShpCost, MyDbType.Double),
                    DbUtility.GetParameter("invCustPO", this.InvCustPO, MyDbType.String),
                    DbUtility.GetParameter("invShpWhsCode", this.InvShpWhsCode, MyDbType.String),
                    DbUtility.GetParameter("invCompanyID", this.InvCompanyID, MyDbType.Int),
                    DbUtility.GetParameter("InvRefNo", this.InvRefNo, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", this.InvRefType, MyDbType.String),
                    DbUtility.GetParameter("invTypeCommission", this.InvTypeCommission, MyDbType.Int),
                    //DbUtility.GetParameter("invDiscount", this.InvDiscount, MyDbType.Double),
                    //DbUtility.GetParameter("invDiscountType", this.InvDiscountType, MyDbType.String),
                    DbUtility.GetParameter("invID", this.InvID, MyDbType.Int)
                });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dBhelp.CloseDatabaseConnection();
            }
        }

        public void UpdateStatus(DbHelper dbHelp, int invID, string status, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE invoices SET invStatus=@invStatus,invLastUpdatedOn=@invLastUpdatedOn,invLastUpdateBy=@invLastUpdateBy WHERE invID=@invID";

            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invStatus", status, MyDbType.String),
                    DbUtility.GetParameter("invLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("invLastUpdateBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int invID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, invID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int invID)
        {
            string sql = "SELECT * FROM invoices WHERE invID=@invID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.InvID = BusinessUtility.GetInt(dr["invID"]);
                    this.InvCustType = BusinessUtility.GetString(dr["invCustType"]);
                    this.InvCustID = BusinessUtility.GetInt(dr["invCustID"]);
                    this.InvCreatedOn = BusinessUtility.GetDateTime(dr["invCreatedOn"]);
                    this.InvStatus = BusinessUtility.GetString(dr["invStatus"]);
                    this.InvComment = BusinessUtility.GetString(dr["invComment"]);
                    this.InvLastUpdatedOn = BusinessUtility.GetDateTime(dr["invLastUpdatedOn"]);
                    this.InvLastUpdateBy = BusinessUtility.GetInt(dr["invLastUpdateBy"]);
                    this.InvShpCost = BusinessUtility.GetInt(dr["invShpCost"]);
                    this.InvCurrencyCode = BusinessUtility.GetString(dr["invCurrencyCode"]);
                    this.InvCurrencyExRate = BusinessUtility.GetInt(dr["invCurrencyExRate"]);
                    this.InvCustPO = BusinessUtility.GetString(dr["invCustPO"]);
                    this.InvShpWhsCode = BusinessUtility.GetString(dr["invShpWhsCode"]);
                    this.InvForOrderNo = BusinessUtility.GetInt(dr["invForOrderNo"]);
                    this.InvCompanyID = BusinessUtility.GetInt(dr["invCompanyID"]);
                    this.InvRefNo = BusinessUtility.GetInt(dr["InvRefNo"]);
                    this.InvRefType = BusinessUtility.GetString(dr["invRefType"]);
                    this.InvTypeCommission = BusinessUtility.GetInt(dr["invTypeCommission"]);
                    this.InvDiscount = BusinessUtility.GetDouble(dr["invDiscount"]);
                    this.InvDiscountType = BusinessUtility.GetString(dr["invDiscountType"]);
                    this.InvRegCode = BusinessUtility.GetString(dr["invRegCode"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        public void PopulateObjectByOrder(DbHelper dbHelp, int orderID, string invoiceStatus, string refType, int userid)
        {
            bool mustClose = true;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, orderID);

                this.InvComment = ord.OrdComment;
                this.InvCompanyID = ord.OrdCompanyID;
                if (ord.InvRefNo > 0)
                {
                    this.InvRefNo = ord.InvRefNo;
                    this.InvCreatedOn = ord.InvDate;
                }
                else
                {
                    this.InvRefNo = 0;
                    this.InvCreatedOn = DateTime.Now;
                }
                this.InvCurrencyCode = ord.OrdCurrencyCode;
                this.InvCurrencyExRate = ord.OrdCurrencyExRate;
                this.InvCustID = ord.OrdCustID;
                this.InvCustPO = ord.OrdCustPO;
                this.InvCustType = ord.OrdCustType;
                this.InvForOrderNo = ord.OrdID;
                this.InvLastUpdateBy = userid;
                this.InvLastUpdatedOn = DateTime.Now;
                this.InvRefType = refType;
                this.InvShpCost = ord.OrdShpCost;
                this.InvShpWhsCode = ord.OrdShpWhsCode;
                this.InvStatus = invoiceStatus;
                this.InvTypeCommission = ord.OrderTypeCommission;
                this.InvDiscount = ord.OrdDiscount;
                this.InvDiscountType = ord.OrdDiscountType;
                this.InvRegCode = ord.OrdRegCode;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool UpdateInvoiceRefNo(DbHelper dbHelp, int invID, int companyID, string refType, bool distinctInvID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            string sql = "SELECT case isnull(max(InvRefNo)+1) when 0 then max(InvRefNo)+1 when 1 then 1 end  FROM invoices where invRefType=@invRefType";
            if (distinctInvID)
            {
                sql += " and invCompanyID=@invCompanyID";
                lParams.Add(DbUtility.GetParameter("invCompanyID", companyID, MyDbType.Int));
            }
            lParams.Add(DbUtility.GetParameter("invRefType", refType, MyDbType.String));
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, lParams.ToArray());
                string sqlUpdae = "UPDATE invoices set InvRefNo=@InvRefNo WHERE invID=@invID";

                if (refType.ToUpper() == InvoiceReferenceType.CREDIT_NOTE)
                {
                    val = 0;
                }
                this.InvRefNo = BusinessUtility.GetInt(val);
                dbHelp.ExecuteNonQuery(sqlUpdae, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invID", invID, MyDbType.Int),
                    DbUtility.GetParameter("InvRefNo", val, MyDbType.Int)
                });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool IsInvoiceExist(DbHelper dbHelp, int orderID, string invRefType)
        {
            string sql = "SELECT count(*) FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invForOrderNo", orderID, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", invRefType, MyDbType.String)
                });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {

                throw;
            }
        }

        public bool IsInvoiceExist(int orderID, string invRefType)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.IsInvoiceExist(dbHelp, orderID, invRefType);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetInvoiceSql(ParameterCollection pCol, string searchField, string searchStatus, string searchText, int partnerID, int userid, bool isSalesRestricted)
        {
            pCol.Clear();
            string strSQL = "";
            if (isSalesRestricted)
            {
                strSQL += "SELECT distinct  o.invID,PartnerAcronyme, (case When ifnull(o.invRegCode,'') <> '' then concat(o.invShpWhsCode,'-',o.invRegCode) else o.invShpWhsCode end) AS invShpWhsCode,  w.WarehouseCode, o.InvRefNo,invCreatedOn, PartnerLongName as CustomerName,";
                strSQL += " orderTypeDesc, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate,";
                strSQL += " invStatus, invCustPO, invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate,IFNULL(InvoicePrintCounter,0) AS InvoicePrintCounter FROM invoices o";
                strSQL += " left join syswarehouses w on w.WarehouseCompanyID=o.invcompanyid Inner join salesrepcustomer s ";
                strSQL += " on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID=@UserID";
                strSQL += " inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID";

                pCol.Add("@UserID", userid.ToString());
            }
            else
            {
                strSQL += " SELECT distinct o.invID,PartnerAcronyme,(case When ifnull(o.invRegCode,'') <> '' then concat(o.invShpWhsCode,'-',o.invRegCode) else o.invShpWhsCode end) AS invShpWhsCode, w.WarehouseCode, o.InvRefNo,invCreatedOn, PartnerLongName  as CustomerName,orderTypeDesc, ";
                strSQL += " sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, invStatus, invCustPO, ";
                strSQL += " invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate, IFNULL(InvoicePrintCounter,0) AS InvoicePrintCounter FROM invoices o left join syswarehouses w";
                strSQL += " on w.WarehouseCompanyID=o.invcompanyid inner join invoiceitems i on i.invoices_invID=o.invID  left join partners";
                strSQL += " on partners.PartnerID =o.invCustID";
            }
            strSQL += " left join ordertype on ordertype.orderTypeID=o.InvTypeCommission";
            strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.InvTypeCommission";
            strSQL += " WHERE PartnerActive=1"; //Retreive only if partner is active

            if (searchField == OrderSearchFields.InvoiceNo && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.InvRefNo like CONCAT('%', @SearchText ,'%') AND  o.invStatus=@SearchStatus";
            }
            else if (searchField == OrderSearchFields.OrderNo && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.invForOrderNo=@SearchText AND o.invStatus=@SearchStatus";
            }
            else if (searchField == CustomerSearchFields.ContactName && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND PartnerLongName like CONCAT('%', @SearchText ,'%') AND  o.invStatus=@SearchStatus";
            }
            else if (searchField == OrderSearchFields.CustomerPO && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.invCustPO =@SearchText AND o.invStatus=@SearchStatus";
            }
            else if (searchField == OrderSearchFields.InvoiceNo && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.InvRefNo=@SearchText ";
            }
            else if (searchField == OrderSearchFields.OrderNo && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.invForOrderNo=@SearchText AND o.invStatus=@SearchStatus";
            }
            else if (searchField == CustomerSearchFields.ContactName && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND PartnerLongName like CONCAT('%', @SearchText ,'%') ";
            }
            else if (searchField == OrderSearchFields.CustomerPO && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
            {
                strSQL += " AND o.invCustPO =@SearchText";
            }
            else if (!string.IsNullOrEmpty(searchStatus))
            {
                strSQL += "  AND o.invStatus=@SearchStatus";
            }

            //Following line of code for future to search on Invoice Ref Type
            if (false)
            {
                strSQL += " AND o.invRefType=@invRefType";
                pCol.Add("@invRefType", "");//Need to update parameter 
            }

            if (partnerID > 0)
            {
                strSQL += " AND (o.invCustID=@invCustID OR i.invGuestID=@invCustID)";
                pCol.Add("@invCustID", partnerID.ToString());
            }

            strSQL += "  GROUP BY o.invID ORDER BY o.invID DESC";

            if (strSQL.Contains("@SearchText"))
            {
                pCol.Add("@SearchText", searchText);
            }
            if (strSQL.Contains("@SearchStatus"))
            {
                pCol.Add("@SearchStatus", searchStatus);
            }

            return strSQL;
        }

        public DataTable GetInvoice(string searchField, string searchStatus, string searchText, int partnerID, int userid, bool isSalesRestricted)
        {
            List<MySqlParameter> pList = new List<MySqlParameter>();
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = true;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {

                string strSQL = "";
                if (isSalesRestricted)
                {
                    strSQL += "SELECT distinct  o.invID,PartnerAcronyme, (case When ifnull(o.invRegCode,'') <> '' then concat(o.invShpWhsCode,'-',o.invRegCode) else o.invShpWhsCode end) AS invShpWhsCode,  w.WarehouseCode, o.InvRefNo,invCreatedOn, CASE partners.PartnerType When 2 Then CONCAT_WS(' ',partners.PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',partners.PartnerLongName,'(V)') ELSE partners.PartnerLongName END as CustomerName,";
                    strSQL += " orderTypeDesc, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate,";
                    strSQL += " invStatus, invCustPO, invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate,IFNULL(InvoicePrintCounter,0) AS InvoicePrintCounter FROM invoices o";
                    strSQL += " left join syswarehouses w on w.WarehouseCompanyID=o.invcompanyid Inner join salesrepcustomer s ";
                    strSQL += " on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID=@UserID";
                    strSQL += " inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID";

                    pList.Add(DbUtility.GetParameter("@UserID", userid, MyDbType.String));
                    //pCol.Add("@UserID", userid.ToString());

                }
                else
                {
                    strSQL += " SELECT distinct o.invID,PartnerAcronyme,(case When ifnull(o.invRegCode,'') <> '' then concat(o.invShpWhsCode,'-',o.invRegCode) else o.invShpWhsCode end) AS invShpWhsCode, w.WarehouseCode, o.InvRefNo,invCreatedOn, CASE partners.PartnerType When 2 Then CONCAT_WS(' ',partners.PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',partners.PartnerLongName,'(V)') ELSE partners.PartnerLongName END  as CustomerName,orderTypeDesc, ";
                    strSQL += " sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, invStatus, invCustPO, ";
                    strSQL += " invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate, IFNULL(InvoicePrintCounter,0) AS InvoicePrintCounter FROM invoices o left join syswarehouses w";
                    strSQL += " on w.WarehouseCompanyID=o.invcompanyid inner join invoiceitems i on i.invoices_invID=o.invID  left join partners";
                    strSQL += " on partners.PartnerID =o.invCustID";
                }
                strSQL += " left join ordertype on ordertype.orderTypeID=o.InvTypeCommission";
                strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.InvTypeCommission";
                strSQL += " WHERE PartnerActive=1"; //Retreive only if partner is active

                if (searchField == OrderSearchFields.InvoiceNo && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.InvRefNo like CONCAT('%', @SearchText ,'%') AND  o.invStatus=@SearchStatus";
                }
                else if (searchField == OrderSearchFields.OrderNo && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.invForOrderNo=@SearchText AND o.invStatus=@SearchStatus";
                }
                else if (searchField == CustomerSearchFields.ContactName && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND PartnerLongName like CONCAT('%', @SearchText ,'%') AND  o.invStatus=@SearchStatus";
                }
                else if (searchField == OrderSearchFields.CustomerPO && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.invCustPO =@SearchText AND o.invStatus=@SearchStatus";
                }
                else if (searchField == OrderSearchFields.InvoiceNo && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.InvRefNo=@SearchText ";
                }
                else if (searchField == OrderSearchFields.OrderNo && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.invForOrderNo=@SearchText AND o.invStatus=@SearchStatus";
                }
                else if (searchField == CustomerSearchFields.ContactName && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND PartnerLongName like CONCAT('%', @SearchText ,'%') ";
                }
                else if (searchField == OrderSearchFields.CustomerPO && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.invCustPO =@SearchText";
                }
                else if (searchField == OrderSearchFields.Register_Code && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += " AND o.invRegCode like CONCAT('%', @SearchText ,'%') ";
                }
                else if (!string.IsNullOrEmpty(searchStatus))
                {
                    strSQL += "  AND o.invStatus=@SearchStatus";
                }

                //Following line of code for future to search on Invoice Ref Type
                if (false)
                {
                    strSQL += " AND o.invRefType=@invRefType";
                    pList.Add(DbUtility.GetParameter("@invRefType", "", MyDbType.String));
                    //pCol.Add("@invRefType", "");//Need to update parameter 
                }

                if (partnerID > 0)
                {
                    strSQL += " AND (o.invCustID=@invCustID OR i.invGuestID=@invCustID)";
                    pList.Add(DbUtility.GetParameter("@invCustID", partnerID, MyDbType.String));
                    //pCol.Add("@invCustID", partnerID.ToString());
                }

                strSQL += "  GROUP BY o.invID ORDER BY o.invID DESC";

                if (strSQL.Contains("@SearchText"))
                {
                    pList.Add(DbUtility.GetParameter("@SearchText", searchText, MyDbType.String));
                    //pCol.Add("@SearchText", searchText);
                }
                if (strSQL.Contains("@SearchStatus"))
                {
                    pList.Add(DbUtility.GetParameter("@SearchStatus", searchStatus, MyDbType.String));
                    //pCol.Add("@SearchStatus", searchStatus);
                }

                return dbHelp.GetDataTable(strSQL, CommandType.Text, pList.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                //if (mustClose) 
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetInvoiceIDByRefID(DbHelper dbHelp, int invRefID, int companyID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = string.Empty;
            List<MySqlParameter> lParmas = new List<MySqlParameter>();
            if (companyID > 0)
            {
                sql += "SELECT InvID FROM invoices where InvRefNo=@InvRefNo AND invCompanyID=@CompanyID";
                lParmas.Add(DbUtility.GetParameter("InvRefNo", invRefID, MyDbType.Int));
                lParmas.Add(DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int));
            }
            else
            {
                sql += "SELECT InvID FROM invoices where InvRefNo=@InvRefNo";
                lParmas.Add(DbUtility.GetParameter("InvRefNo", invRefID, MyDbType.Int));
            }

            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, lParmas.ToArray());
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }

        }

        public bool IsInovicePaid(DbHelper dbHelp, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(*) FROM invoices WHERE invStatus=@Status AND invID=@invID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Status", InvoicesStatus.PAYMENT_RECEIVED, MyDbType.String),
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val) > 0;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetInvoiceID(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT invID FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invForOrderNo", ordID, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", "IV", MyDbType.String)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetInvoiceID(DbHelper dbHelp, int ordID, string invRefType)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT invID FROM invoices WHERE invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invForOrderNo", ordID, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", invRefType, MyDbType.String)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Statement Print Section
        public List<int> GetBatchStatementInvoiceList(DbHelper dbHelp, int comID, int custID, int daysRange, bool showPaidOnly, bool isSalesRistricted, int currentUserID)
        {
            List<int> lResult = new List<int>();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<MySqlParameter> lParams = new List<MySqlParameter>();
            string sql = string.Empty;
            if (isSalesRistricted)
            {
                sql += "SELECT distinct o.invID FROM invoices o inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID=@UserID inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo ";
                lParams.Add(DbUtility.GetParameter("UserID", currentUserID, MyDbType.Int));
            }
            else
            {
                sql += "SELECT distinct o.invID FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo ";
            }
            if (showPaidOnly)
            {
                sql += " WHERE o.invStatus<> '" + InvoicesStatus.PAYMENT_RECEIVED + "' ";
            }
            else
            {
                sql += " WHERE 1=1";
            }
            if (comID > 0)
            {
                sql += " AND invCompanyID = @ComID";
                lParams.Add(DbUtility.GetParameter("ComID", comID, MyDbType.Int));
            }
            if (custID > 0)
            {
                sql += " AND invCustID=@CustID";
                lParams.Add(DbUtility.GetParameter("CustID", custID, MyDbType.Int));
            }
            switch (daysRange)
            {
                case 10:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 10)  and o.invCreatedOn < now())";
                    break;
                case 15:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 15)  and o.invCreatedOn < SUBDATE(now(), 10))";
                    break;
                case 30:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 30)  and o.invCreatedOn < SUBDATE(now(), 15))";
                    break;
                case 60:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 60)  and o.invCreatedOn < SUBDATE(now(), 30))";
                    break;
                case 90:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 90)  and o.invCreatedOn < SUBDATE(now(), 60))";
                    break;
                case 180:
                    sql += " AND (o.invCreatedOn >= SUBDATE(now(), 180)  and o.invCreatedOn < SUBDATE(now(), 90))";
                    break;
                default:
                    sql += " AND (o.invCreatedOn < SUBDATE(now(), 180))";
                    break;
            }
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, lParams.ToArray());
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr["invID"]));
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose)
                {
                    dbHelp.CloseDatabaseConnection(dr);
                }
                else
                {
                    if (dr != null && !dr.IsClosed)
                    {
                        dr.Close();
                    }
                }
            }
        }

        public DataTable GetStatements(DbHelper dbHelp, int comID, string daysRange, int customerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<MySqlParameter> lp = new List<MySqlParameter>();
            string strSQL = "SELECT DISTINCT o.invID, CONCAT(o.invRefType, CAST(o.InvRefNo AS CHAR)) AS InvoiceNo,";
            strSQL += "o.invCreatedOn AS InvDate, o.invCreatedOn AS DueDate,";
            //strSQL += "a.ARAmtRcvdDateTime AS PaymentReceivedDate,";
            strSQL += "0 AS CalculateBalance FROM invoices o ";
            strSQL += " left join accountreceivable a  ON a.ARInvoiceNo=o.invID";
            strSQL += " left join collection c on c.ColInvID=o.invID ";
            strSQL += " left join users u on u.userID=c.ColUserAssignedTo ";
            strSQL += " WHERE o.invStatus<>@invStatus";

            lp.Add(DbUtility.GetParameter("invStatus", InvoicesStatus.PAYMENT_RECEIVED, MyDbType.String));

            if (comID > 0)
            {
                strSQL += " AND invCompanyID =@invCompanyID";
                lp.Add(DbUtility.GetParameter("invCompanyID", comID, MyDbType.Int));
            }
            if (customerID > 0)
            {
                strSQL += " AND o.InvCustID=@CustomerID";
                lp.Add(DbUtility.GetParameter("CustomerID", customerID, MyDbType.Int));
            }

            DateTime fDate = DateTime.Now, tDate = DateTime.Now;
            switch (daysRange)
            {
                case "10":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-10);
                    tDate = DateTime.Now;
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "15":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-15);
                    tDate = DateTime.Now.AddDays(-10);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "30":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-30);
                    tDate = DateTime.Now.AddDays(-15);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "60":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-60);
                    tDate = DateTime.Now.AddDays(-30);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "90":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-90);
                    tDate = DateTime.Now.AddDays(-60);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "180":
                    strSQL += " AND o.invCreatedOn BETWEEN @FDate AND @TDate";
                    fDate = DateTime.Now.AddDays(-180);
                    tDate = DateTime.Now.AddDays(-90);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    lp.Add(DbUtility.GetParameter("TDate", tDate, MyDbType.DateTime));
                    break;
                case "180p":
                    strSQL += " AND o.invCreatedOn < @FDate";
                    fDate = DateTime.Now.AddDays(-180);
                    lp.Add(DbUtility.GetParameter("FDate", fDate, MyDbType.DateTime));
                    break;
            }

            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, lp.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        public bool IsInvoiceExists(DbHelper dbHelp, string refType, int comID, int invID, int soid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = string.Empty;
            List<MySqlParameter> p = new List<MySqlParameter>();
            if (soid > 0)
            {
                sql = "SELECT count(*) FROM invoices where invRefType=@invRefType AND invCompanyID=@invCompanyID AND InvRefNo=@InvRefNo AND invForOrderNo <> @invForOrderNo";
                p.Add(DbUtility.GetParameter("invRefType", refType, MyDbType.String));
                p.Add(DbUtility.GetParameter("invCompanyID", comID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("InvRefNo", invID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("invForOrderNo", soid, MyDbType.Int));
            }
            else
            {
                sql = "SELECT count(*) FROM invoices where invRefType=@invRefType AND invCompanyID=@invCompanyID AND InvRefNo=@InvRefNo";
                p.Add(DbUtility.GetParameter("invRefType", refType, MyDbType.String));
                p.Add(DbUtility.GetParameter("invCompanyID", comID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("InvRefNo", invID, MyDbType.Int));
            }
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, p.ToArray());
                return BusinessUtility.GetInt(o) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateInvoicePrintCounter(DbHelper dbHelp, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE invoices SET InvoicePrintCounter = IFNULL(InvoicePrintCounter, 0) + 1 WHERE invID=@invID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invID", invoiceID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Gross Profit Section
        //Function to move in business Logic
        public double GetGrossProfitByInvoice(DbHelper dbHelp, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT (t1.Amount/t1.RCount) AS Amount FROM(SELECT ";
                sql += " SUM(an.slsTotalUnits) AS Qty,SUM(100* (an.slsTotalUnits * (an.slsUnitPrice - an.slsUnitCostPriceAvg)) / (an.slsTotalPrice)) AS Amount,";
                sql += " COUNT(an.slsProductID) AS RCount";
                sql += "  FROM z_sls_analysis an WHERE an.slsInvoiceNo=@slsInvoiceNo GROUP BY an.slsInvoiceNo) AS t1";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { 
                DbUtility.GetParameter("slsInvoiceNo", invoiceID, MyDbType.Int)
            });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region
        //Calculate Amt without taxes and including discount
        public double GetPerItemAmtWithoutTax(DbHelper dbHelp, int inItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "select (sum(invProductQty * invProductUnitPrice) -  sum(invProductQty * invProductUnitPrice * invProductDiscount)/100)/invProductQty as returnAmt from invoiceitems where invItemID = @invItemID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { 
                DbUtility.GetParameter("invItemID", inItemID, MyDbType.Int)
            });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        public void GetInvoiceCustSaleRepName(int invID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "";
            sql += " SELECT ptr.PartnerLongName AS CustName, usr.UserFirstName AS SaleRep FROM invoices AS inv ";
            sql += " INNER JOIN orders AS o ON o.ordID = inv.invForOrderNo ";
            sql += " Inner JOIN Partners AS ptr ON ptr.PartnerID = o.ordCustID ";
            sql += " INNER JOIN users AS usr ON usr.userID = o.ordSalesRepID ";
            sql += " WHERE inv.invID = @invID ";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.InvCustName = BusinessUtility.GetString(dr["CustName"]);
                    this.InvSaleRep = BusinessUtility.GetString(dr["SaleRep"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void InvoiceDetail(int invID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT * FROM invoices WHERE invID=@invID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.InvID = BusinessUtility.GetInt(dr["invID"]);
                    this.InvCustType = BusinessUtility.GetString(dr["invCustType"]);
                    this.InvCustID = BusinessUtility.GetInt(dr["invCustID"]);
                    this.InvCreatedOn = BusinessUtility.GetDateTime(dr["invCreatedOn"]);
                    this.InvStatus = BusinessUtility.GetString(dr["invStatus"]);
                    this.InvComment = BusinessUtility.GetString(dr["invComment"]);
                    this.InvLastUpdatedOn = BusinessUtility.GetDateTime(dr["invLastUpdatedOn"]);
                    this.InvLastUpdateBy = BusinessUtility.GetInt(dr["invLastUpdateBy"]);
                    this.InvShpCost = BusinessUtility.GetInt(dr["invShpCost"]);
                    this.InvCurrencyCode = BusinessUtility.GetString(dr["invCurrencyCode"]);
                    this.InvCurrencyExRate = BusinessUtility.GetInt(dr["invCurrencyExRate"]);
                    this.InvCustPO = BusinessUtility.GetString(dr["invCustPO"]);
                    this.InvShpWhsCode = BusinessUtility.GetString(dr["invShpWhsCode"]);
                    this.InvForOrderNo = BusinessUtility.GetInt(dr["invForOrderNo"]);
                    this.InvCompanyID = BusinessUtility.GetInt(dr["invCompanyID"]);
                    this.InvRefNo = BusinessUtility.GetInt(dr["InvRefNo"]);
                    this.InvRefType = BusinessUtility.GetString(dr["invRefType"]);
                    this.InvTypeCommission = BusinessUtility.GetInt(dr["invTypeCommission"]);
                    this.InvDiscount = BusinessUtility.GetDouble(dr["invDiscount"]);
                    this.InvDiscountType = BusinessUtility.GetString(dr["invDiscountType"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetPOSReceiptDetail(int iInvID)
        {
            List<MySqlParameter> pList = new List<MySqlParameter>();
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = true;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" SELECT Distinct i.invItemID, i.invProductID,i.invProductDiscount AS posPercentDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode, ");
                sb.Append(" CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as ProductName, invProductQty AS posPrdQty, (invProductUnitPrice * invCurrencyExRate) as posPrdUnitPrice, invCurrencyCode, ");
                sb.Append(" (invProductQty * invProductUnitPrice) AS posPrdPrice,(select sum(invProductQty * invProductUnitPrice)  from invoiceitems invi where invi.invoices_invID =@INVID) AS posSubTotal, ");
                sb.Append(" i.Tax1 AS posTax1, i.Tax2 AS posTax2, i.Tax3 AS posTax3, i.Tax4 AS posTax4,i.TaxDesc1 AS posTax1Desc, i.TaxDesc2 AS posTax2Desc,i.TaxDesc3 AS posTax3Desc,i.TaxDesc4 AS posTax4Desc,o.invLastUpdatedOn AS posTransDateTime, ");
                sb.Append(" funGetReceivedAmountByInvoiceID(i.invoices_invID) AS posCasReceived,case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as posTotalValue, ");
                sb.Append(" /*(case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End - IFNULL(acc.ARAmtRcvd,0)) AS posCashReturned,acc.ARAmtRcvdVia AS postransType, */ ");
                sb.Append(" invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, reg.sysRegMessage, pColor.Color{0} AS Color, pSize.Size{0} AS Size  FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID ");
                sb.Append(" inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp ");
                sb.Append(" INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ");
                sb.Append(" INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ");
                sb.Append(" INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ");
                sb.Append(" left join accountreceivable acc on  acc.ARInvoiceNo = i.invoices_invID ");
                sb.Append(" Inner join sysregister AS reg on reg.sysRegcode= o.invRegCode ");
                sb.Append(" where  i.invoices_invID=@INVID ");
                pList.Add(DbUtility.GetParameter("@INVID", iInvID, MyDbType.String));
                return dbHelp.GetDataTable(BusinessUtility.GetString(string.Format(BusinessUtility.GetString(sb), "en")), CommandType.Text, pList.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                //if (mustClose) 
                dbHelp.CloseDatabaseConnection();
            }
        }


        public double GetCustTotalSales(int custID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT SUM( (ifnull((invProductQty*invProductUnitPrice),0) - ");
            sbQuery.Append(" (ifnull((invProductQty*invProductUnitPrice)*invProductDiscount*0.01,0)))+ ifnull((it.TaxCalculated1+it.TaxCalculated2+it.TaxCalculated3+it.TaxCalculated4+it.TaxCalculated5),0) ) - Sum(ifnull(ir.ReturnAmount,0)) ");
            sbQuery.Append(" FROM Invoices   ");
            sbQuery.Append(" INNER JOIN InvoiceItems  it ON it.invoices_invID=  invID left outer join  z_invoice_return ir on ir.ReturnInvoiceID = invID Where invCustID = @CustID ");
            MySqlDataReader dr = null;
            try
            {
                object totalSaleValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("CustID", custID, MyDbType.Int)
                });

                return BusinessUtility.GetDouble(totalSaleValue);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetCustTotalSales(int custID, DateTime dtFromDate, DateTime dtTodate)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT SUM( (ifnull((invProductQty*invProductUnitPrice),0) - ");
            sbQuery.Append(" (ifnull((invProductQty*invProductUnitPrice)*invProductDiscount*0.01,0)))+ ifnull((it.TaxCalculated1+it.TaxCalculated2+it.TaxCalculated3+it.TaxCalculated4+it.TaxCalculated5),0) ) - Sum(ifnull(ir.ReturnAmount,0)) ");
            sbQuery.Append(" FROM Invoices   ");
            sbQuery.Append(" INNER JOIN InvoiceItems  it ON it.invoices_invID=  invID left outer join  z_invoice_return ir on ir.ReturnInvoiceID = invID Where invCustID = @CustID  AND invCreatedOn BETWEEN @Fdate AND @Tdate  ");
            MySqlDataReader dr = null;
            try
            {
                object totalSaleValue = dbHelp.GetValue(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("CustID", custID, MyDbType.Int),
                    DbUtility.GetParameter("Fdate", dtFromDate, MyDbType.DateTime),
                    DbUtility.GetParameter("Tdate", dtTodate, MyDbType.DateTime)

                });

                return BusinessUtility.GetDouble(totalSaleValue);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


    }
}
