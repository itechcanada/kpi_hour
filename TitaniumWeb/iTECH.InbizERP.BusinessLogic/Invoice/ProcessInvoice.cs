﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProcessInvoice
    {
        public static bool IsInvoiceExists(string refType, int comID, int invID, int soid)
        {
            string sql = string.Empty;
            List<MySqlParameter> p = new List<MySqlParameter>();
            if (soid > 0)
            {
                sql = "SELECT count(*) FROM invoices where invRefType=@invRefType AND invCompanyID=@invCompanyID AND InvRefNo=@InvRefNo AND invForOrderNo <> @invForOrderNo";
                p.Add(DbUtility.GetParameter("invRefType", refType, MyDbType.String));
                p.Add(DbUtility.GetParameter("invCompanyID", comID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("InvRefNo", invID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("invForOrderNo", soid, MyDbType.Int));                
            }
            else
            {
                sql = "SELECT count(*) FROM invoices where invRefType=@invRefType AND invCompanyID=@invCompanyID AND InvRefNo=@InvRefNo";
                p.Add(DbUtility.GetParameter("invRefType", refType, MyDbType.String));
                p.Add(DbUtility.GetParameter("invCompanyID", comID, MyDbType.Int));
                p.Add(DbUtility.GetParameter("InvRefNo", invID, MyDbType.Int));
            }
            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, p.ToArray());
                return BusinessUtility.GetInt(o) > 0;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static int GetInvoiceID(string invRefType, int soid)
        {
            string sql = "SELECT invID FROM invoices  where invForOrderNo=@invForOrderNo AND invRefType=@invRefType";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invForOrderNo", soid, MyDbType.Int),
                    DbUtility.GetParameter("invRefType", invRefType, MyDbType.String)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static int GetOrderID(int invid)
        {
            string sql = "SELECT invForOrderNo FROM invoices  where invID=@invID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invID", invid, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static double GetInvoiceReceivedAmout(int invID)
        {
            string sql = "SELECT if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo group by ARInvoiceNo ";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static double GetInvoiceWriteOffAmout(int invID)
        {
            string sql = "SELECT if(sum(ARWriteOff) Is Null,0, sum(ARWriteOff)) as WriteOffAmount FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo group by ARInvoiceNo ";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static double GetCustomerUnpaidAmount(int customerID)
        {
            string sql = "SELECT DISTINCT o.ordID, i.invID FROM orders o LEFT OUTER JOIN invoices i ON i.invForOrderNo=o.ordID WHERE o.ordCustID=@CustID";
            //string sql = "SELECT SUM(itm.invProductQty * invProductUnitPrice) FROM invoices i INNER JOIN  invoiceitems itm ON itm.invoices_invID = i.invID WHERE i.invStatus <> 'P' AND i.invCustID = @invCustID GROUP BY i.invCustID";          
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustID", customerID, MyDbType.Int)
                });

                double unpaidAmount = 0.0D;
                TotalSummary ts;
                foreach (DataRow item in dt.Rows)
                {
                    if (BusinessUtility.GetInt(item["invID"]) > 0)
                    {
                        ts = CalculationHelper.GetInvoiceTotal(dbHelp, BusinessUtility.GetInt(item["invID"]));
                    }
                    else
                    {
                        ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(item["ordID"]));
                    }
                    unpaidAmount += ts.OutstandingAmount;
                }
                return unpaidAmount;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public static string AmountReceivedBy(int invNo)
        {
            string sql = string.Format("SELECT ARAmtRcvdVia FROM accountreceivable WHERE ARInvoiceNo={0} LIMIT 1", invNo);
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, null);
                int idVal = BusinessUtility.GetInt(val);
                switch (idVal)
                {
                    case (int)StatusAmountReceivedVia.Cash:
                        return "Cash";
                    case (int)StatusAmountReceivedVia.Cheque:
                        return "Cheque";
                    case (int)StatusAmountReceivedVia.CreditCard:
                        return "Credit Card";
                    case (int)StatusAmountReceivedVia.MoneyOrder:
                        return "Money Order";
                    case (int)StatusAmountReceivedVia.DirectWireTransfer:
                        return "Direct Wire Transfer";
                    case (int)StatusAmountReceivedVia.DebitCard:
                        return "Debit Card";
                    case (int)StatusAmountReceivedVia.WriteOff:
                        return "Write Off";
                    case (int)StatusAmountReceivedVia.SwipeCard:
                        return "Swipe Card";
                    case (int)StatusAmountReceivedVia.Paypal:
                        return "Paypal";
                    default:
                        return string.Empty;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }                
    }
}
