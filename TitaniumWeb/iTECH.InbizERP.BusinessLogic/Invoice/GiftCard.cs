﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class GiftCard
    {
        public string GiftCardNo { get; set; }
        public string TransactionType { get; set; }
        public double Amount { get; set; }
        public int POSUser { get; set; }
        public int PartnerID { get; set; }
        public int InvoiceID { get; set; }

        public Boolean Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(" INSERT INTO GiftCardTransactionDtl (GiftCardNo, TransactionType, Amount, POSUser, PartnerID, InvoiceID, TransactionDateTime, GiftCardExpiryDate) ");
                sql.Append(" VALUES (@GiftCardNo, @TransactionType, @Amount, @POSUser, @PartnerID, @InvoiceID, @TransactionDateTime, @GiftCardExpiryDate) ");
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sql), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("GiftCardNo", this.GiftCardNo, MyDbType.String),
                    DbUtility.GetParameter("TransactionType", this.TransactionType, MyDbType.String),
                    DbUtility.GetParameter("Amount", this.Amount, MyDbType.Double),
                    DbUtility.GetParameter("POSUser", this.POSUser, MyDbType.Int),
                    DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    DbUtility.GetParameter("InvoiceID", this.InvoiceID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("GiftCardExpiryDate", DateTime.Now.AddDays(15), MyDbType.DateTime),
                });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<GiftCard> GetValidateGiftCardNo(DbHelper dbHelp, string GiftCardNo)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<GiftCard> lResult = new List<GiftCard>();
                string sql = "SELECT GiftCardNo FROM GiftCardTransactionDtl WHERE TransactionType = '" + GiftCardTransType.Buy + "' AND DATE(GiftCardExpiryDate) >= DATE(NOW())  AND GiftCardNo ='" + GiftCardNo + "'";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new GiftCard { GiftCardNo = BusinessUtility.GetString(dr["GiftCardNo"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }

        }

        public double AvailableGiftCardValue(DbHelper dbHelp, string GiftCardNo)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "(SELECT SUM( Amount) FROM GiftCardTransactionDtl WHERE TransactionType = '" + GiftCardTransType.Buy + "' AND GiftCardNo ='" + GiftCardNo + "')";
                object CardValue = BusinessUtility.GetDouble(dbHelp.GetValue(sql, CommandType.Text, null));

                sql = " (SELECT SUM( Amount) FROM GiftCardTransactionDtl WHERE TransactionType = '" + GiftCardTransType.Sale + "' AND GiftCardNo ='" + GiftCardNo + "')";
                object CardUsedValue = BusinessUtility.GetDouble(dbHelp.GetValue(sql, CommandType.Text, null));

                return BusinessUtility.GetDouble(CardValue) - BusinessUtility.GetDouble(CardUsedValue);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }

        }

        public bool IsExistsGiftCardNo(DbHelper dbHelp, string GiftCardNo)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = " SELECT COUNT(*) FROM GiftCardTransactionDtl WHERE TransactionType = '" + GiftCardTransType.Buy + "' AND GiftCardNo ='" + GiftCardNo + "' ";
                object CardValue = BusinessUtility.GetDouble(dbHelp.GetValue(sql, CommandType.Text, null));
                if (BusinessUtility.GetInt(CardValue) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }

        }


    }
}
