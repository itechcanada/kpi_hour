﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SlsAnalysis
    {
        public int RecordID { get; set; }
        public DateTime slsRecordDateTime { get; set; }
        public int slsDay { get { return this.slsRecordDateTime.Day; } }
        public int slsMonth { get { return this.slsRecordDateTime.Month; } }
        public int slsYear { get { return this.slsRecordDateTime.Year; } }
        public int slsCustID { get; set; }
        public int slsOrdNo { get; set; }
        public int slsInvoiceNo { get; set; }
        public string slsShippedWhs { get; set; }
        public int slsOrderCreatedBy { get; set; }
        public DateTime slsOrderCreatedOn { get; set; }
        public DateTime slsShippedOn { get; set; }
        public DateTime slsInvoicedOn { get; set; }
        public DateTime slsLastPmtReceivedOn { get; set; }
        public double slsLastPmtAmtReceived { get; set; }
        public int slsOrderSalesRepID1 { get; set; }
        public int slsOrderSalesRepID2 { get; set; }
        public int slsOrderSalesRepID3 { get; set; }
        public int slsOrderSalesRepID4 { get; set; }
        public int slsProductID { get; set; }
        public string slsProductName { get; set; }
        public string slsPrdDesc { get; set; }
        public string slsPrdUPCCode { get; set; }
        public string slsPrdIntID{ get; set; }
        public string slsPrdExtID { get; set; }
        public double slsTotalUnits { get; set; }
        public double slsUnitPrice { get; set; }
        public double slsTotalPrice { get; set; }
        public double slsDiscount { get; set; }
        public double slsUnitCostPriceAvg { get; set; }
        public double slsUnitCostPriceFIFO { get; set; }
        public bool slsWriteOff { get; set; }
        public double slsWriteOffAmount { get; set; }
        public DateTime slsWriteOffDateTime { get; set; }
        public int slsWriteOffByUserID { get; set; }

        public void RecordOnInvoiceGeneration(DbHelper dbHelp, int invID)
        {
            bool mustClose = false;          
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlInsert = "INSERT INTO z_sls_analysis (slsRecordDateTime,slsDay,slsMonth,slsYear,slsCustID,slsOrdNo,slsInvoiceNo,";
                sqlInsert += "slsShippedWhs,slsOrderCreatedBy,slsOrderCreatedOn,slsShippedOn,slsInvoicedOn,slsLastPmtReceivedOn,slsLastPmtAmtReceived,";
                sqlInsert += "slsOrderSalesRepID1,slsOrderSalesRepID2,slsOrderSalesRepID3,slsOrderSalesRepID4,slsProductID,slsProductName,";
                sqlInsert += "slsPrdDesc,slsPrdUPCCode,slsPrdIntID,slsPrdExtID,slsTotalUnits,slsUnitPrice,slsTotalPrice,slsDiscount,";
                sqlInsert += "slsUnitCostPriceAvg,slsUnitCostPriceFIFO,slsWriteOff,slsWriteOffAmount,slsWriteOffDateTime,slsWriteOffByUserID)";
                sqlInsert += " VALUES(@slsRecordDateTime,@slsDay,@slsMonth,@slsYear,@slsCustID,@slsOrdNo,@slsInvoiceNo,";
                sqlInsert += "@slsShippedWhs,@slsOrderCreatedBy,@slsOrderCreatedOn,@slsShippedOn,@slsInvoicedOn,@slsLastPmtReceivedOn,@slsLastPmtAmtReceived,";
                sqlInsert += "@slsOrderSalesRepID1,@slsOrderSalesRepID2,@slsOrderSalesRepID3,@slsOrderSalesRepID4,@slsProductID,@slsProductName,";
                sqlInsert += "@slsPrdDesc,@slsPrdUPCCode,@slsPrdIntID,@slsPrdExtID,@slsTotalUnits,@slsUnitPrice,@slsTotalPrice,@slsDiscount,";
                sqlInsert += "@slsUnitCostPriceAvg,@slsUnitCostPriceFIFO,@slsWriteOff,@slsWriteOffAmount,@slsWriteOffDateTime,@slsWriteOffByUserID)";

                Invoice inv = new Invoice();
                inv.PopulateObject(dbHelp, invID);
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, inv.InvForOrderNo);

                this.slsRecordDateTime = DateTime.Now;

                //From Invoice
                this.slsCustID = inv.InvCustID;
                this.slsInvoicedOn = inv.InvCreatedOn;
                this.slsInvoiceNo = inv.InvID;                
                this.slsShippedWhs = inv.InvShpWhsCode;


                //From Order
                this.slsOrderCreatedBy = ord.OrdCreatedBy;
                this.slsOrderCreatedOn = ord.OrdCreatedOn;

                string sqlGetSaleRep = "SELECT SalesRepID FROM z_order_sales_rep WHERE OrderID=@OrderID";
                List<int> lstSalesRep = new List<int>();
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGetSaleRep, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderID", ord.OrdID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lstSalesRep.Add(BusinessUtility.GetInt(dr["SalesRepID"]));
                    }
                }
                if (lstSalesRep.Count == 1 || lstSalesRep.Count > 1)
                {
                    this.slsOrderSalesRepID1 = lstSalesRep[0];
                }
                if (lstSalesRep.Count == 2 || lstSalesRep.Count > 2)
                {
                    this.slsOrderSalesRepID2 = lstSalesRep[1];
                }
                if (lstSalesRep.Count == 3 || lstSalesRep.Count > 3)
                {
                    this.slsOrderSalesRepID3 = lstSalesRep[2];
                }
                if (lstSalesRep.Count == 4 || lstSalesRep.Count > 4)
                {
                    this.slsOrderSalesRepID4 = lstSalesRep[3];
                }
                                               
                this.slsOrdNo = ord.OrdID;
                this.slsShippedOn = ord.OrdShpDate;

                // WriteOff = 6,Credit = 9, Refund = 10
                string sqlGetAr = "SELECT ARAmtRcvd, ARRcvdBy, ARAmtRcvdDateTime FROM accountreceivable WHERE ARInvoiceNo=@InvoiceID AND ARAmtRcvdVia NOT IN(6,9,10) ORDER BY ARAmtRcvdDateTime DESC LIMIT 1";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGetAr, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.slsLastPmtAmtReceived = BusinessUtility.GetDouble(dr["ARAmtRcvd"]);
                        this.slsLastPmtReceivedOn = BusinessUtility.GetDateTime(dr["ARAmtRcvdDateTime"]);
                    }
                }

                this.slsWriteOff = false; //Set by default false

                // WriteOff = 6
                string sqlGetWriteOff = "SELECT ARWriteOff, ARRcvdBy, ARAmtRcvdDateTime FROM accountreceivable WHERE ARInvoiceNo=@InvoiceID AND ARAmtRcvdVia=6 ORDER BY ARAmtRcvdDateTime DESC LIMIT 1";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGetWriteOff, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.slsWriteOff = true;
                        this.slsWriteOffAmount = BusinessUtility.GetDouble(dr["ARWriteOff"]);
                        this.slsWriteOffByUserID = BusinessUtility.GetInt(dr["ARRcvdBy"]);
                        this.slsWriteOffDateTime = BusinessUtility.GetDateTime(dr["ARAmtRcvdDateTime"]);
                    }
                }


                string sqlInvItems = "SELECT ii.invProductID,ii.invProductDiscount,ii.invProductDiscountType,ii.invProductUnitPrice,ii.invProductQty,";
                sqlInvItems += "p.prdIntID,p.prdExtID,p.prdUPCCode,p.prdName, ii.invProdIDDesc, p.prdLandedPrice ";
                sqlInvItems += " FROM invoiceitems ii INNER JOIN products p ON p.productID=ii.invProductID WHERE ii.invoices_invID=@InvoiceID";
                var dtItms = dbHelp.GetDataTable(sqlInvItems, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceID", inv.InvID, MyDbType.Int)
                });

                //ProcessInventory chkPrdVendorExist = new ProcessInventory();
                foreach (DataRow item in dtItms.Rows)
                {
                    this.slsPrdDesc = BusinessUtility.GetString(item["invProdIDDesc"]);
                    this.slsPrdExtID = BusinessUtility.GetString(item["prdExtID"]);
                    this.slsPrdIntID = BusinessUtility.GetString(item["prdIntID"]);
                    this.slsPrdUPCCode = BusinessUtility.GetString(item["prdUPCCode"]);
                    this.slsProductID = BusinessUtility.GetInt(item["invProductID"]);
                    this.slsProductName = BusinessUtility.GetString(item["prdName"]);                    
                    this.slsUnitPrice = BusinessUtility.GetDouble(item["invProductUnitPrice"]);
                    this.slsTotalUnits = BusinessUtility.GetDouble(item["invProductQty"]);
                    //this.slsTotalPrice = Math.Round(this.slsUnitPrice * this.slsTotalUnits * inv.InvCurrencyExRate, 2);
                    this.slsTotalPrice = Math.Round(this.slsUnitPrice * this.slsTotalUnits, 2);
                    switch (BusinessUtility.GetString(item["invProductDiscountType"]))
                    {
                        case "A":
                            this.slsDiscount = BusinessUtility.GetDouble(item["invProductDiscount"]);
                            break;
                        default:
                            this.slsDiscount = ((this.slsTotalPrice * BusinessUtility.GetDouble(item["invProductDiscount"])) / 100.00D);
                            break;
                    }
                    int a = ProcessInventory.GetVendorsCountByProductID(BusinessUtility.GetInt(item["invProductID"])).Count();
                    if (a > 0)
                    {
                        this.slsUnitCostPriceAvg = this.GetUnitCostPriceAvg(dbHelp, this.slsProductID, this.slsTotalUnits);
                    }
                    else
                    {
                        this.slsUnitCostPriceAvg = BusinessUtility.GetDouble(item["prdLandedPrice"]);
                    }
                    this.slsUnitCostPriceFIFO = this.GetUnitCostPriceFIFO(dbHelp, this.slsProductID, this.slsTotalUnits);

                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("slsRecordDateTime", this.slsRecordDateTime, MyDbType.DateTime),
                        DbUtility.GetParameter("slsDay", this.slsDay, MyDbType.Int),
                        DbUtility.GetParameter("slsMonth", this.slsMonth, MyDbType.Int),
                        DbUtility.GetParameter("slsYear", this.slsYear, MyDbType.Int),
                        DbUtility.GetParameter("slsCustID", this.slsCustID, MyDbType.Int),
                        DbUtility.GetParameter("slsOrdNo", this.slsOrdNo, MyDbType.Int),
                        DbUtility.GetParameter("slsInvoiceNo", this.slsInvoiceNo, MyDbType.Int),
                        DbUtility.GetParameter("slsShippedWhs", this.slsShippedWhs, MyDbType.String),
                        DbUtility.GetParameter("slsOrderCreatedBy", this.slsOrderCreatedBy, MyDbType.Int),
                        DbUtility.GetParameter("slsOrderCreatedOn", this.slsOrderCreatedOn, MyDbType.DateTime),
                        DbUtility.GetParameter("slsShippedOn", this.slsShippedOn, MyDbType.DateTime),
                        DbUtility.GetParameter("slsInvoicedOn", this.slsInvoicedOn, MyDbType.DateTime),
                        DbUtility.GetParameter("slsLastPmtReceivedOn", this.slsLastPmtReceivedOn, MyDbType.DateTime),
                        DbUtility.GetParameter("slsLastPmtAmtReceived", this.slsLastPmtAmtReceived, MyDbType.Double),
                        DbUtility.GetParameter("slsOrderSalesRepID1", this.slsOrderSalesRepID1, MyDbType.Int),
                        DbUtility.GetParameter("slsOrderSalesRepID2", this.slsOrderSalesRepID2, MyDbType.Int),
                        DbUtility.GetParameter("slsOrderSalesRepID3", this.slsOrderSalesRepID3, MyDbType.Int),
                        DbUtility.GetParameter("slsOrderSalesRepID4", this.slsOrderSalesRepID4, MyDbType.Int),
                        DbUtility.GetParameter("slsProductID", this.slsProductID, MyDbType.Int),
                        DbUtility.GetParameter("slsProductName", this.slsProductName, MyDbType.String),
                        DbUtility.GetParameter("slsPrdDesc", this.slsPrdDesc, MyDbType.String),
                        DbUtility.GetParameter("slsPrdUPCCode", this.slsPrdUPCCode, MyDbType.String),
                        DbUtility.GetParameter("slsPrdIntID", this.slsPrdIntID, MyDbType.String),
                        DbUtility.GetParameter("slsPrdExtID", this.slsPrdExtID, MyDbType.String),
                        DbUtility.GetParameter("slsTotalUnits", this.slsTotalUnits, MyDbType.Double),
                        DbUtility.GetParameter("slsUnitPrice", this.slsUnitPrice, MyDbType.Double),
                        DbUtility.GetParameter("slsTotalPrice", this.slsTotalPrice, MyDbType.Double),
                        DbUtility.GetParameter("slsDiscount", this.slsDiscount, MyDbType.Double),
                        DbUtility.GetParameter("slsUnitCostPriceAvg", this.slsUnitCostPriceAvg, MyDbType.Double),
                        DbUtility.GetParameter("slsUnitCostPriceFIFO", this.slsUnitCostPriceFIFO, MyDbType.Double),
                        DbUtility.GetParameter("slsWriteOff", this.slsWriteOff, MyDbType.Boolean),
                        DbUtility.GetParameter("slsWriteOffAmount", this.slsWriteOffAmount, MyDbType.Double),
                        DbUtility.GetParameter("slsWriteOffDateTime", this.slsWriteOffDateTime, MyDbType.DateTime),
                        DbUtility.GetParameter("slsWriteOffByUserID", this.slsWriteOffByUserID, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateOnPaymentReceived(DbHelper dbHelp, int invID, double amountReceived, DateTime receivedOn)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE z_sls_analysis SET slsLastPmtAmtReceived=@slsLastPmtAmtReceived,slsLastPmtReceivedOn=@slsLastPmtReceivedOn WHERE slsInvoiceNo=@slsInvoiceNo";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("slsLastPmtAmtReceived", amountReceived, MyDbType.Double),
                    DbUtility.GetParameter("slsLastPmtReceivedOn", receivedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("slsInvoiceNo", invID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose)
                {
                    dbHelp.CloseDatabaseConnection();
                }
            }
        }

        public void UpdateOnPaymentWriteOff(DbHelper dbHelp, int invID, double writeOffAmount, DateTime writeOffOn, int writeOffBy)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE z_sls_analysis SET slsWriteOff=1, slsWriteOffAmount=@slsWriteOffAmount,slsWriteOffDateTime=@slsWriteOffDateTime,slsWriteOffByUserID=@slsWriteOffByUserID WHERE slsInvoiceNo=@slsInvoiceNo";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("slsWriteOffAmount", writeOffAmount, MyDbType.Double),
                    DbUtility.GetParameter("slsWriteOffDateTime", writeOffOn, MyDbType.DateTime),
                    DbUtility.GetParameter("slsInvoiceNo", invID, MyDbType.Int),
                    DbUtility.GetParameter("slsWriteOffByUserID", writeOffBy, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose)
                {
                    dbHelp.CloseDatabaseConnection();
                }
            }
        }

        private double GetUnitCostPriceAvg(DbHelper dbHelp, int productID, double totalQty)
        {
            if (totalQty <= 0)
            {
                return 0.00D;
            }
            string sql = "SELECT * FROM prdassociatevendor WHERE prdID=@ProductID ORDER BY PurchaseDate DESC";
            try
            {
                double sum = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                }))
                {
                    double remainingQty = totalQty;
                    
                    double qtyToApply, availUnits;
                    while (dr.Read() && remainingQty > 0)
                    {
                        qtyToApply = 0;
                        availUnits = BusinessUtility.GetDouble(dr["NoOfUnits"]);
                        if (availUnits >= remainingQty)
                        {
                            qtyToApply = remainingQty;
                        }
                        else if(availUnits > 0)
                        {
                            qtyToApply = availUnits;
                        }
                        sum += qtyToApply * BusinessUtility.GetDouble(dr["prdCostPrice"]);
                        remainingQty = remainingQty - qtyToApply;
                    }
                }

                return Math.Round(sum / totalQty, 2);
            }
            catch 
            {                
                throw;
            }
        }

        private double GetUnitCostPriceFIFO(DbHelper dbHelp, int productID, double totalQty)
        {
            if (totalQty <= 0)
            {
                return 0.00D;
            }
            string sql = "SELECT * FROM prdassociatevendor WHERE prdID=@ProductID ORDER BY NoOfUnits";
            try
            {
                double sum = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                }))
                {
                    double remainingQty = totalQty;

                    double qtyToApply, availUnits;
                    while (dr.Read() && remainingQty > 0)
                    {
                        qtyToApply = 0;
                        availUnits = BusinessUtility.GetDouble(dr["NoOfUnits"]);
                        if (availUnits >= remainingQty)
                        {
                            qtyToApply = remainingQty;
                        }
                        else if (availUnits > 0)
                        {
                            qtyToApply = availUnits;
                        }
                        sum += qtyToApply * BusinessUtility.GetDouble(dr["prdCostPrice"]);
                        remainingQty = remainingQty - qtyToApply;
                    }
                }

                return Math.Round(sum / totalQty, 2);
            }
            catch
            {
                throw;
            }
        }

        //Added by mukesh 20130607
        public double TotalCostPriceAvg(DbHelper dbHelp, int productID)
        {

            string sql = "select sum(prdCostPrice*NoOfUnits) /sum(NoOfUnits) as UnitCostPrice from prdassociatevendor WHERE prdID=@ProductID";
            try
            {
                double sum = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        sum = BusinessUtility.GetDouble(dr["UnitCostPrice"]);
                    }
                }

                return Math.Round(sum, 2);
            }
            catch
            {
                throw;
            }
        }
        //Added end
    }
}
