﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InvoiceItemProcess
    {
        public int InvItemProcID{ get; set; }
        public int Invoices_invID{ get; set; }
        public string InvItemProcCode{ get; set; }
        public double InvItemProcFixedPrice{ get; set; }
        public double InvItemProcInternalCost { get; set; } //Added by mukesh 20130524
        public double InvItemProcPricePerHour{ get; set; }
        public int InvItemProcHours{ get; set; }
        public double InvItemProcPricePerUnit{ get; set; }
        public int InvItemProcUnits{ get; set; }
        public int SysTaxCodeDescID{ get; set; }

        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            try
            {
                string sql = "INSERT INTO invitemprocess (invoices_invID,invItemProcCode,invItemProcFixedPrice,invItemProcPricePerHour,invItemProcHours,";
                sql += " invItemProcPricePerUnit,invItemProcUnits,sysTaxCodeDescID,invItemProcInternalCost) ";
                sql += " VALUES(@invoices_invID,@invItemProcCode,@invItemProcFixedPrice,@invItemProcPricePerHour,@invItemProcHours,";
                sql += " @invItemProcPricePerUnit,@invItemProcUnits,@sysTaxCodeDescID,@invItemProcInternalCost)"; //Edited by mukesh 20130524
                int? txGrp = this.SysTaxCodeDescID > 0 ? (int?)this.SysTaxCodeDescID : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invoices_invID", this.Invoices_invID, MyDbType.Int),
                    DbUtility.GetParameter("invItemProcCode", this.InvItemProcCode, MyDbType.String),
                    DbUtility.GetParameter("invItemProcFixedPrice", this.InvItemProcFixedPrice, MyDbType.Double),
                    DbUtility.GetParameter("invItemProcInternalCost", this.InvItemProcInternalCost, MyDbType.Double), //Added by mukesh 20130524
                    DbUtility.GetParameter("invItemProcPricePerHour", this.InvItemProcPricePerHour, MyDbType.Double),
                    DbUtility.GetParameter("invItemProcHours", this.InvItemProcHours, MyDbType.Int),
                    DbUtility.GetParameter("invItemProcPricePerUnit", this.InvItemProcPricePerUnit, MyDbType.Double),
                    DbUtility.GetParameter("invItemProcUnits", this.InvItemProcUnits, MyDbType.Int),
                    DbUtility.GetIntParameter("sysTaxCodeDescID", txGrp)
                });

                this.InvItemProcID = dbHelp.GetLastInsertID();
                if (this.InvItemProcID > 0)
                {
                    this.UpdateTax(dbHelp, this.InvItemProcID, this.SysTaxCodeDescID);
                }
            }
            catch 
            {                
                throw;
            }
        }

        public void UpdateTax(DbHelper dbHelp, int invProcessItemID, int taxGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE invitemprocess SET sysTaxCodeDescID=@TaxGroupID, Tax1=@Tax1, TaxDesc1=@TaxDesc1,";
                sql += "Tax2=@Tax2, TaxDesc2=@TaxDesc2,Tax3=@Tax3, TaxDesc3=@TaxDesc3,Tax4=@Tax4, TaxDesc4=@TaxDesc4,";
                sql += "Tax5=@Tax5,TaxDesc5=@TaxDesc5,TaxCalculated1=@TaxCalculated1,TaxCalculated2=@TaxCalculated2,TaxCalculated3=@TaxCalculated3,TaxCalculated4=@TaxCalculated4,TaxCalculated5=@TaxCalculated5 WHERE invItemProcID=@invItemProcID";
                string sqlAmount = "SELECT TaxGroupID, Amount FROM  vw_invoice_process_item_amount WHERE InvoiceProcessItemID=@InvoiceProcessItemID";
                Tax tx = new Tax();
                double amount = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceProcessItemID", invProcessItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        amount = BusinessUtility.GetDouble(dr["Amount"]);
                        if (taxGroupID <= 0)
                        {
                            taxGroupID = BusinessUtility.GetInt(dr["TaxGroupID"]);
                        }
                    }
                }
                if (taxGroupID > 0)
                {
                    tx.PopulateObject(dbHelp, taxGroupID, amount);
                }
                int? txGrp = taxGroupID > 0 ? (int?)taxGroupID : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("TaxGroupID", txGrp),
                    DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                    DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                    DbUtility.GetParameter("Tax3", tx.Tax3, MyDbType.Double),
                    DbUtility.GetParameter("Tax4", tx.Tax4, MyDbType.Double),
                    DbUtility.GetParameter("Tax5", tx.Tax5, MyDbType.Double),
                    DbUtility.GetParameter("TaxDesc1", tx.TaxDesc1, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc2", tx.TaxDesc2, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc3", tx.TaxDesc3, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc4", tx.TaxDesc4, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc5", tx.TaxDesc5, MyDbType.String),
                    DbUtility.GetParameter("invItemProcID", invProcessItemID, MyDbType.Int),
                    DbUtility.GetParameter("TaxCalculated1", tx.TaxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", tx.TaxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", tx.TaxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", tx.TaxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", tx.TaxCalculated5, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddProcessInvoiceItemsForOrder(DbHelper dbHelp, int orderID, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlProcIds = "SELECT DISTINCT orderItemProcID FROM orderitemprocess WHERE ordID=@OrderID";

                string sql = "INSERT INTO invitemprocess(invoices_invID,invItemProcCode,invItemProcFixedPrice,invItemProcInternalCost,invItemProcPricePerHour,invItemProcHours,"; //Edited by mukesh 20130524
                sql += "invItemProcPricePerUnit,invItemProcUnits,sysTaxCodeDescID,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,";
                sql += "TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5)";
                sql += " SELECT @InvoiceID AS invoices_invID, ordItemProcCode AS invItemProcCode, ordItemProcFixedPrice AS invItemProcFixedPrice, ordItemProcInternalCost AS invItemProcInternalCost, "; //Edited by mukesh 20130524
                sql += "ordItemProcPricePerHour AS invItemProcPricePerHour, ordItemProcHours AS invItemProcHours, ";
                sql += " ordItemProcPricePerUnit AS invItemProcPricePerUnit, ordItemProcUnits AS invItemProcUnits,";
                sql += " sysTaxCodeDescID AS sysTaxCodeDescID,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,";
                sql += " TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5";
                sql += " FROM orderitemprocess WHERE orderItemProcID=@OrderProcessItemID";

                List<int> lstProcessItemId = new List<int>();
                using (var dr = dbHelp.GetDataReader(sqlProcIds, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lstProcessItemId.Add(BusinessUtility.GetInt(dr[0]));
                    }
                }

                //To Copy Returns data from Order Returns.
                int invProcessItemID = 0;
                List<InvoiceProcessReturn> lstProcessReturn;
                OrderReturn rtn = new OrderReturn();
                foreach (var item in lstProcessItemId)
                {
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("InvoiceID", invoiceID, MyDbType.Int),
                        DbUtility.GetParameter("OrderProcessItemID", item, MyDbType.Int)
                    });
                    invProcessItemID = dbHelp.GetLastInsertID();

                    lstProcessReturn = rtn.GetInvoiceProcessReturnsList(dbHelp, item, invProcessItemID, invoiceID);
                    foreach (var pRtn in lstProcessReturn)
                    {
                        pRtn.ReturnAndGetWriteOff(dbHelp, invProcessItemID, pRtn.ReturnAmount, pRtn.ReturnBy);
                    }
                }                               
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public DataTable GetProcessItems(int invID)
        //{
        //    string strSQL = "SELECT invItemProcID, invoices_invID, invItemProcCode, ProcessDescription, (invItemProcFixedPrice*invCurrencyExRate) as invItemProcFixedPrice,";
        //    strSQL += " (invItemProcPricePerHour*invCurrencyExRate) as invItemProcPricePerHour,(invItemProcPricePerUnit*invCurrencyExRate) as invItemProcPricePerUnit,";
        //    strSQL += " invItemProcHours, invItemProcUnits, (invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost, ";
        //    strSQL += " IFNULL(systaxcodedesc.sysTaxCodeDescID,0) as sysTaxCodeDescID, invItemProcFixedPrice AS IninvItemProcFixedPrice, invItemProcPricePerHour AS IninvItemProcPricePerHour,";
        //    strSQL += " invItemProcPricePerUnit AS IninvItemProcPricePerUnit,  IFNULL(systaxcodedesc.sysTaxCodeDescText, 'Warehous Tax') as sysTaxCodeDescText ";
        //    strSQL += " FROM invitemprocess i inner join invoices on invID=invoices_invID inner join sysprocessgroup on ProcessCode=invItemProcCode ";
        //    strSQL += " Left outer  join systaxcodedesc on i.sysTaxCodeDescID = systaxcodedesc.sysTaxCodeDescID where invoices_invID=@invoices_invID ";
        //    strSQL += " order by ProcessDescription ";
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    { 

        //    }
        //}

        public IDataReader GetProcessItemsReader(DbHelper dbHelp, int invID)
        {
            string strSQL = "SELECT invItemProcID, invoices_invID, invItemProcCode, ProcessDescription, (invItemProcFixedPrice*invCurrencyExRate) as invItemProcFixedPrice, invItemProcInternalCost, "; //Edited by mukesh 20130524
            strSQL += " (invItemProcPricePerHour*invCurrencyExRate) as invItemProcPricePerHour,(invItemProcPricePerUnit*invCurrencyExRate) as invItemProcPricePerUnit,";
            strSQL += " invItemProcHours, invItemProcUnits, (invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost, ";
            strSQL += " IFNULL(systaxcodedesc.sysTaxCodeDescID,0) as sysTaxCodeDescID, invItemProcFixedPrice AS IninvItemProcFixedPrice, invItemProcPricePerHour AS IninvItemProcPricePerHour,";
            strSQL += " invItemProcPricePerUnit AS IninvItemProcPricePerUnit,  IFNULL(systaxcodedesc.sysTaxCodeDescText, 'Warehous Tax') as sysTaxCodeDescText ";
            strSQL += " FROM invitemprocess i inner join invoices on invID=invoices_invID inner join sysprocessgroup on ProcessCode=invItemProcCode ";
            strSQL += " Left outer  join systaxcodedesc on i.sysTaxCodeDescID = systaxcodedesc.sysTaxCodeDescID where invoices_invID=@invoices_invID ";
            strSQL += " order by ProcessDescription ";            
            try
            {
                return dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }            
        }

        public void DeleteProcessItems(int invID)
        {
            string sql = "DELETE FROM invitemprocess WHERE invoices_invID=@invoices_invID";
            DbHelper dBhelp = new DbHelper();
            try
            {
                dBhelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invoices_invID", invID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dBhelp.CloseDatabaseConnection();
            }
        }

        public int GetInvoiceProcessItemID(DbHelper dbHelp, string procCode, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                object val = dbHelp.GetValue("SELECT invItemProcID FROM invitemprocess WHERE invItemProcCode=@ProcCode AND invoices_invID=@InvoiceID", CommandType.Text,
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("ProcCode", procCode, MyDbType.String),
                        DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int)
                    });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public void UpdateProcessFixedCost(DbHelper dbHelp, double fixedCost, int invProcessItemID)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        string sqlUpdate = "UPDATE invitemprocess SET invItemProcFixedPrice=@FixedProice WHERE invItemProcID=@invItemProcID";
        //        dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("FixedProice", fixedCost, MyDbType.Double),
        //            DbUtility.GetParameter("invItemProcID", invProcessItemID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //Added by mukesh 20130606
        public void UpdateInvoiceProcessInternalCost(DbHelper dbHelp, string procCode, int invProcessItemID, double internalcost)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlUpdate = "UPDATE invitemprocess SET invItemProcInternalCost=@invItemProcInternalCost WHERE invItemProcID=@invItemProcID and invItemProcCode=@invItemProcCode";
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invItemProcInternalCost", internalcost, MyDbType.Double),
                     DbUtility.GetParameter("invItemProcCode", procCode, MyDbType.String),
                    DbUtility.GetParameter("invItemProcID", invProcessItemID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        //Added end

        public double GetShippingPrice(DbHelper dbHelp, string processCode, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT SUM(v.Amount) FROM vw_invoice_process_item_amount v INNER JOIN invitemprocess oi ON oi.invItemProcID=v.InvoiceProcessItemID";
                sql += " WHERE oi.invItemProcCode=@ProcCode AND v.InvoiceID=@InvoiceID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ProcCode", processCode, MyDbType.String),
                    DbUtility.GetParameter("InvoiceID", invID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }        
    } 
}
