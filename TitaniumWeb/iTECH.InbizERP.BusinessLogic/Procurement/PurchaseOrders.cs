﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PurchaseOrders
    {

        #region Properties
        public int PoID { get; set; }
        public int PoVendorID { get; set; }
        public int PoCreatedByUserID { get; set; }
        public int PoAuthroisedByUserID { get; set; }
        public int PoCompanyID { get; set; }
        public int PoForSoNo { get; set; }
        public DateTime PoDate { get; set; }
        public DateTime PoFaxedEmailDateTime { get; set; }
        public double PoCurrencyExRate { get; set; }
        public string PoStatus { get; set; }
        public string PoShipVia { get; set; }
        public string PoFOBLoc { get; set; }
        public string PoShpTerms { get; set; }
        public string PoNotes { get; set; }
        public string PoCurrencyCode { get; set; }
        public string PoWhsCode { get; set; }
        public string PoRef { get; set; }

        #endregion
        #region Functions
        /// <summary>
        /// Get Sql for grid view 
        /// </summary>
        /// <param name="pCol"></param>
        /// <param name="SearchData"></param>
        /// <param name="VendorId"></param>
        /// <returns></returns>
        public string GetSQL(ParameterCollection pCol, string SearchData, string VendorId)
        {
            pCol.Clear();
            string strSQL = "SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus,v.vendorID FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID  where 1=1 ";

            if (!string.IsNullOrEmpty(SearchData))
            {
                strSQL += " And   vendorName LIKE CONCAT('%', @SearchData, '%') ";
                pCol.Add("@SearchData", SearchData);
            }
            if (!string.IsNullOrEmpty(VendorId))
            {
                strSQL += " And v.vendorID = @VendorId  ";
                pCol.Add("@VendorId", DbType.Int16, VendorId);
            }
            strSQL += "  group by p.poID order by p.poID desc ";
            return strSQL;
        }

        /// <summary>
        /// Get sql for Purchase order..
        /// </summary>
        /// <param name="pCol"></param>
        /// <param name="SearchData"></param>
        /// <param name="StatusData"></param>
        /// <param name="PoStatus"></param>
        /// <returns></returns>
        public string GetSQLForGeneratedPOList(ParameterCollection pCol, string StatusData, string SearchData, string PoStatus)
        {
            pCol.Clear();
            string strSQL = " SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID ";
            strSQL += " inner join products as pro on i.poItemPrdId = pro.productID where 1=1 ";

            if (StatusData == "PN" & !string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
            {
                strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
                pCol.Add("@SearchData", SearchData);
                pCol.Add("@PoStatus", PoStatus);
            }
            else
                if (StatusData == ProductSearchFields.ProductName & !string.IsNullOrEmpty(SearchData) & string.IsNullOrEmpty(PoStatus))
                {
                    strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%') ";
                    pCol.Add("@SearchData", SearchData);
                }
                else
                    if (StatusData == ProductSearchFields.ProductName & string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                    {
                        strSQL += " And poStatus = @PoStatus ";
                        pCol.Add("@PoStatus", PoStatus);
                    }
                    else
                        if (StatusData == ProductSearchFields.ProductBarCode & !string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                        {
                            strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
                            pCol.Add("@SearchData", SearchData);
                            pCol.Add("@PoStatus", PoStatus);
                        }
                        else
                            if (StatusData == ProductSearchFields.ProductBarCode & !string.IsNullOrEmpty(SearchData) & string.IsNullOrEmpty(PoStatus))
                            {
                                strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%') ";
                                pCol.Add("@SearchData", SearchData);
                            }
                            else
                                if (StatusData == ProductSearchFields.ProductBarCode & string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                                {
                                    strSQL += " And poStatus = @PoStatus";
                                    pCol.Add("@PoStatus", PoStatus);
                                }
                                else
                                    if (StatusData == "PO" & !string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                                    {
                                        strSQL += " And p.poID = SearchData  and poStatus = @PoStatus";
                                        pCol.Add("@SearchData", DbType.Int16, SearchData);
                                        pCol.Add("@PoStatus", PoStatus);
                                    }
                                    else
                                        if (StatusData == "VN" & !string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                                        {
                                            strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
                                            pCol.Add("@SearchData", SearchData);
                                            pCol.Add("@PoStatus", PoStatus);
                                        }
                                        else
                                            if (StatusData == "PO" & !string.IsNullOrEmpty(SearchData) & string.IsNullOrEmpty(PoStatus))
                                            {
                                                strSQL += " And p.poID =  SearchData  ";
                                                pCol.Add("@SearchData", DbType.Int16, SearchData);
                                            }
                                            else
                                                if (StatusData == "VN" & !string.IsNullOrEmpty(SearchData) & string.IsNullOrEmpty(PoStatus))
                                                {
                                                    strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')  ";
                                                    pCol.Add("@SearchData", SearchData);
                                                }
                                                else
                                                    if (StatusData == "PO" & string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                                                    {
                                                        strSQL += " And poStatus = @PoStatus";
                                                        pCol.Add("@PoStatus", PoStatus);
                                                    }
                                                    else
                                                        if (StatusData == "VN" & string.IsNullOrEmpty(SearchData) & !string.IsNullOrEmpty(PoStatus))
                                                        {
                                                            strSQL += " And poStatus = @PoStatus";
                                                            pCol.Add("@PoStatus", PoStatus);
                                                        }
            strSQL += " group by p.poID order by p.poID desc, vendorName ";
            return strSQL;
        }

        //public string GetSearchSql(ParameterCollection pCol, string searchField, string searchText, string searchStatus)
        //{
        //    pCol.Clear();
        //    string strSQL = " SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, ";
        //    strSQL += " poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID ";
        //    strSQL += " inner join products as pro on i.poItemPrdId = pro.productID where 1=1 ";

        //    if (searchField == POSearchField.PRODUCT_NAME && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
        //        pCol.Add("@SearchData", searchText);
        //        pCol.Add("@PoStatus", searchStatus);
        //    }
        //    else if (searchField == POSearchField.PRODUCT_NAME && !string.IsNullOrEmpty(searchStatus) && string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And poStatus = @PoStatus";               
        //        pCol.Add("@PoStatus", searchStatus);
        //    }
        //    else if (searchField == POSearchField.PRODUCT_NAME && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%')";
        //        pCol.Add("@SearchData", searchText);
        //    }
        //    else if (searchField == POSearchField.PRODUCT_BAR_CODE && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%')  And poStatus = @PoStatus";
        //        pCol.Add("@SearchData", searchText);
        //        pCol.Add("@PoStatus", searchStatus);
        //    }            
        //    else if (searchField == POSearchField.PRODUCT_BAR_CODE && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%') ";
        //        pCol.Add("@SearchData", searchText);
        //    }
        //    else if(searchField == POSearchField.PO_ID && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And p.poID = @SearchData  and poStatus = @PoStatus";
        //        pCol.Add("@SearchData", DbType.Int16, searchText);
        //        pCol.Add("@PoStatus", searchStatus);
        //    }
        //    else if (searchField == POSearchField.PO_ID && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And p.poID = @SearchData";
        //        pCol.Add("@SearchData", DbType.Int16, searchText);                
        //    }
        //    else if(searchField == POSearchField.VENDOR_NAME && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
        //        pCol.Add("@SearchData", searchText);
        //        pCol.Add("@PoStatus", searchStatus);
        //    }
        //    else if (searchField == POSearchField.VENDOR_NAME && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')";
        //        pCol.Add("@SearchData", searchText);                
        //    }

        //    strSQL += " group by p.poID order by p.poID desc, vendorName ";
        //    return strSQL;
        //}

        public DataTable GetSearchResult(DbHelper dbHelp, string searchField, string searchText, string searchStatus, int vendorID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            var pList = new List<MySqlParameter>();
            string strSQL = " SELECT p.poID, v.vendorName, (sum(poQty * poUnitPrice)*p.poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, ";
            strSQL += " poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID ";
            strSQL += " inner join products as pro on i.poItemPrdId = pro.productID where 1=1 ";
            if (searchField == POSearchField.PRODUCT_NAME && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
                pList.Add(DbUtility.GetParameter("@PoStatus", searchStatus, MyDbType.String));
            }
            else if (searchField == POSearchField.PRODUCT_NAME && !string.IsNullOrEmpty(searchStatus) && string.IsNullOrEmpty(searchText))
            {
                strSQL += " And poStatus = @PoStatus";
                pList.Add(DbUtility.GetParameter("@PoStatus", searchStatus, MyDbType.String));
            }
            else if (searchField == POSearchField.PRODUCT_NAME && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And pro.prdName LIKE CONCAT('%', @SearchData, '%')";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
            }
            else if (searchField == POSearchField.PRODUCT_BAR_CODE && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%')  And poStatus = @PoStatus";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
                pList.Add(DbUtility.GetParameter("@PoStatus", searchStatus, MyDbType.String));
            }
            else if (searchField == POSearchField.PRODUCT_BAR_CODE && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And pro.prdUPCCode LIKE CONCAT('%', @SearchData, '%') ";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
            }
            else if (searchField == POSearchField.PO_ID && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And p.poID = @SearchData  and poStatus = @PoStatus";
                int poid = 0;
                int.TryParse(searchText, out poid);
                pList.Add(DbUtility.GetParameter("@SearchData", poid, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("@PoStatus", searchStatus, MyDbType.String));
            }
            else if (searchField == POSearchField.PO_ID && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And p.poID = @SearchData";
                int poid = 0;
                int.TryParse(searchText, out poid);
                pList.Add(DbUtility.GetParameter("@SearchData", poid, MyDbType.Int));
            }
            else if (searchField == POSearchField.VENDOR_NAME && !string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')  and poStatus = @PoStatus";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
                pList.Add(DbUtility.GetParameter("@PoStatus", searchStatus, MyDbType.String));
            }
            else if (searchField == POSearchField.VENDOR_NAME && string.IsNullOrEmpty(searchStatus) && !string.IsNullOrEmpty(searchText))
            {
                strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%')";
                pList.Add(DbUtility.GetParameter("@SearchData", searchText, MyDbType.String));
            }

            if (vendorID > 0)
            {
                strSQL += " AND p.poVendorID=@VendorID";
                pList.Add(DbUtility.GetParameter("VendorID", vendorID, MyDbType.Int));
            }

            strSQL += " group by p.poID order by p.poID desc, vendorName ";

            try
            {
                return dbHelp.GetDataTable(strSQL, CommandType.Text, pList.ToArray());
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get Purchase Order Info.
        /// </summary>
        public void PopulateObject(DbHelper dbHelp, int poid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT  poDate, poStatus, poVendorID, poShipVia, poFOBLoc, poShpTerms, poNotes, poCreatedByUserID, poAuthroisedByUserID, poFaxedEmailDateTime, poCurrencyCode, poCurrencyExRate, poCompanyID, poWhsCode, poForSoNo, poRef from purchaseorders where poID= @poID";
            MySqlDataReader dr = null;
            try
            {
                using (dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("@poID", poid, typeof(int))
                }))
                {
                    if (dr.Read())
                    {
                        this.PoID = poid;
                        this.PoAuthroisedByUserID = BusinessUtility.GetInt(dr["poAuthroisedByUserID"]);
                        this.PoCompanyID = BusinessUtility.GetInt(dr["poCompanyID"]);
                        this.PoCreatedByUserID = BusinessUtility.GetInt(dr["poCreatedByUserID"]);
                        this.PoCurrencyCode = BusinessUtility.GetString(dr["poCurrencyCode"]);
                        //this.PoCurrencyExRate = BusinessUtility.GetInt(dr["poCurrencyExRate"]);
                        this.PoCurrencyExRate = BusinessUtility.GetDouble(dr["poCurrencyExRate"]);
                        this.PoDate = BusinessUtility.GetDateTime(dr["poDate"]);
                        this.PoFaxedEmailDateTime = BusinessUtility.GetDateTime(dr["poFaxedEmailDateTime"]);
                        this.PoFOBLoc = BusinessUtility.GetString(dr["poFOBLoc"]);
                        this.PoForSoNo = BusinessUtility.GetInt(dr["poForSoNo"]);
                        this.PoNotes = BusinessUtility.GetString(dr["poNotes"]);
                        this.PoShipVia = BusinessUtility.GetString(dr["poShipVia"]);
                        this.PoShpTerms = BusinessUtility.GetString(dr["poShpTerms"]);
                        this.PoStatus = BusinessUtility.GetString(dr["poStatus"]);
                        this.PoVendorID = BusinessUtility.GetInt(dr["poVendorID"]);
                        this.PoWhsCode = BusinessUtility.GetString(dr["poWhsCode"]);
                        this.PoRef = BusinessUtility.GetString(dr["poRef"]);

                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetPurchaseOrderItems(int poID)
        {
            string strSQL = " SELECT p.productID, CASE poi.prodIDDesc='' WHEN False THEN poi.prodIDDesc Else p.prdName  END as prdName, p.prdUPCCode, poi.poQty, poi.poUnitPrice, poi.poItemShpToWhsCode, v.vendorID, v.vendorName ";
            strSQL += " FROM products as p inner join purchaseorderitems as poi on poi.poItemPrdId = p.productID inner join purchaseorders as po on po.poID = poi.poID inner join vendor as v on v.vendorID = po.poVendorID ";
            strSQL += " where po.poID = @poID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("@poID", poID, typeof(int)) });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "Insert INTO purchaseorders(poDate, poStatus, poVendorID, poShipVia, poFOBLoc, poShpTerms, poNotes, poCreatedByUserID, poAuthroisedByUserID, poFaxedEmailDateTime, poCurrencyCode, poCurrencyExRate, poCompanyID, poWhsCode, poForSoNo, poRef ) VALUES( ";
            sql += "  @poDate, @poStatus, @poVendorID, @poShipVia, @poFOBLoc, @poShpTerms, @poNotes, @poCreatedByUserID, @poAuthroisedByUserID, @poFaxedEmailDateTime, @poCurrencyCode, @poCurrencyExRate, @poCompanyID, @poWhsCode, @poForSoNo, @poRef )";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poDate", this.PoDate,typeof(DateTime)),
                                     DbUtility.GetParameter("poStatus", this.PoStatus,typeof(string)),
                                     DbUtility.GetParameter("poVendorID", this.PoVendorID,typeof(int)),
                                     DbUtility.GetParameter("poShipVia", this.PoShipVia,typeof(string)),
                                     DbUtility.GetParameter("poFOBLoc", this.PoFOBLoc,typeof(string)),
                                     DbUtility.GetParameter("poShpTerms", this.PoShpTerms,typeof(string)),
                                     DbUtility.GetParameter("poNotes", this.PoNotes,typeof(string)),
                                     DbUtility.GetParameter("poCreatedByUserID", this.PoCreatedByUserID,typeof(int)),
                                     DbUtility.GetParameter("poAuthroisedByUserID", this.PoAuthroisedByUserID,typeof(int)),
                                     DbUtility.GetParameter("poFaxedEmailDateTime", this.PoFaxedEmailDateTime,typeof(DateTime)),
                                     DbUtility.GetParameter("poCurrencyCode", this.PoCurrencyCode,typeof(string)),
                                     DbUtility.GetParameter("poCurrencyExRate", this.PoCurrencyExRate,typeof(double)),
                                     DbUtility.GetParameter("poCompanyID", this.PoCompanyID,typeof(int)),
                                     DbUtility.GetParameter("poWhsCode", this.PoWhsCode,typeof(string)),
                                     DbUtility.GetParameter("poForSoNo", this.PoForSoNo,typeof(int)),
                                     DbUtility.GetParameter("poRef", this.PoRef,typeof(string)),
                                     
                                 };
            try
            {
                if (dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {
                    this.PoID = dbHelp.GetLastInsertID();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Update Purachas order On Approve Condition
        /// </summary>
        /// <returns></returns>
        public bool UpdateOnApprove(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            //SQL to Insert              
            string sql = "Update purchaseorders Set poStatus=@poStatus, poShipVia=@poShipVia, poFOBLoc=@poFOBLoc, poNotes=@poNotes, poAuthroisedByUserID=@poAuthroisedByUserID, poRef = @poRef where poID= @poID";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poStatus", this.PoStatus,typeof(string)),                                     
                                     DbUtility.GetParameter("poShipVia", this.PoShipVia,typeof(string)),
                                     DbUtility.GetParameter("poFOBLoc", this.PoFOBLoc,typeof(string)),
                                     DbUtility.GetParameter("poNotes", this.PoNotes,typeof(string)),
                                     DbUtility.GetParameter("poAuthroisedByUserID", this.PoAuthroisedByUserID,typeof(int)),
                                     DbUtility.GetParameter("poID", this.PoID,typeof(int)),
                                     DbUtility.GetParameter("poRef", this.PoRef,typeof(string)),
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool UpdateForReceiving(DbHelper dbHelp, int poID, DateTime receivingDate)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string strSQL = null;
            strSQL = "SELECT poItemShpToWhsCode FROM purchaseorderitems WHERE poID= @poID ORDER BY poID";
            string sqlUpdate = "UPDATE purchaseorders SET poStatus=@poStatus, poRef = @poRef WHERE poID=@poID";
            Receiving rcv = new Receiving();
            try
            {
                //Update 
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("@poID", poID, MyDbType.Int),
                     //DbUtility.GetParameter("@poStatus", POStatus.READY_FOR_RECEIVING, MyDbType.String)
                     DbUtility.GetParameter("@poStatus",PoStatus, MyDbType.String),
                     DbUtility.GetParameter("poRef", this.PoRef,typeof(string)),
                });

                object val = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("@poID", poID, MyDbType.Int),
                });
                if (val != DBNull.Value && val != null)
                {
                    rcv.PopulateObjectByPoID(dbHelp, poID);
                    rcv.RecDateTime = receivingDate;
                    rcv.RecPOID = poID;
                    rcv.RecAtWhsCode = val.ToString();
                    if (rcv.RecID <= 0)//RecId is zero then Insert else Update 
                        rcv.Insert(dbHelp);
                    else
                        rcv.Update(dbHelp);
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSQLProduct(ParameterCollection pCol, string searchField, string searchTxt, string lang, int iCollectionID)
        {
            pCol.Clear();
            string strSQL = "SELECT * FROM (  SELECT p.productID, p.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty,   ";
            strSQL += " pv.prdCostPrice, pClothDesc.Style, pClothDesc.Collection AS CollectionID, pc.ShortName AS Collection ,pColor.Color{0} AS Color, pSize.Size{0} AS Size  ";
            strSQL += " FROM products as p  ";
            strSQL += " INNER JOIN prddescriptions as pd on pd.id=p.productID and pd.descLang='{0}'  ";
            strSQL += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
            strSQL += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection  ";
            strSQL += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color  ";
            strSQL += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size    ";
            strSQL += " LEFT JOIN prdassociatevendor as pv on pv.prdID=p.productID  ";
            strSQL += " LEFT JOIN vendor as v on v.vendorID=pv.prdVendorID  ";
            strSQL += "  Where 1=1 ";
            strSQL += "  ";
            strSQL += "  ";
            //pCol.Add("@strLang", lang);

            //string strSQL = " SELECT p.productID, p.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty, pv.prdCostPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='{0}'  left join prdassociatevendor as pv on pv.prdID=p.productID left Join vendor as v on v.vendorID=pv.prdVendorID Where 1=1  ";
            //pCol.Add("@strLang", lang);
            strSQL = string.Format(strSQL, lang);


            if (iCollectionID > 0)
            {
                strSQL += " AND pClothDesc.Collection = @CollectionID";
                pCol.Add("@CollectionID", BusinessUtility.GetString(iCollectionID));
            }


            if (searchField == ProductSearchFields.ProductID & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.productID = @SearchData  ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == ProductSearchFields.ProductName & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.prdName  LIKE CONCAT('%', @SearchData, '%') ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == ProductSearchFields.ProductInternalID & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.prdIntID = @SearchData  ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == ProductSearchFields.ProductExternalID & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.prdExtID = @SearchData  ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == ProductSearchFields.ProductBarCode & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.prdUPCCode = @SearchData ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == ProductSearchFields.ProductDescription & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And  ";
                strSQL += " (";
                strSQL += " prdSmallDesc  LIKE CONCAT('%', @SearchData, '%') or ";
                strSQL += " prdLargeDesc  LIKE CONCAT('%', @SearchData, '%') ";
                strSQL += " )";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == "VN" & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And vendorName  LIKE CONCAT('%', @SearchData, '%') ";
                pCol.Add("@SearchData", searchTxt);
            }
            else if (searchField == "ST" & !string.IsNullOrEmpty(searchTxt))
            {
                strSQL += " And p.Style = @SearchData  ";
                pCol.Add("@SearchData", searchTxt);

                //strSQL += " And p.Style  LIKE CONCAT('%', @SearchData, '%') ";
                //pCol.Add("@SearchData", searchTxt);
            }
            //strSQL += " group by p.productID order by p.prdName desc, vendorName ) AS pDesc";
            strSQL += "  GROUP BY p.productID order by p.prdName) AS pDesc ";


            return strSQL;
        }
        public string GetSQLProductDetailSql(ParameterCollection pCol, string productId, string strLang)
        {
            pCol.Clear();
            string strSQL = "SELECT p.productID, p.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty, pv.prdCostPrice, p.prdFOBPrice, p.prdLandedPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@strLang left join prdassociatevendor as pv on pv.prdID=p.productID left Join vendor as v on v.vendorID=pv.prdVendorID ";
            strSQL += " where p.productID = @productId ";
            pCol.Add("@strLang", strLang);
            pCol.Add("@productId", DbType.Int16, productId);
            return strSQL;
        }


        public void UpdatePO(DbHelper dbHelp, int poid, string status, string poShpTerms)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "Update purchaseorders set poStatus=@Status,poShpTerms=@poShpTerms, poRef = @poRef WHERE poID=@POID ";
            try
            {

                dbHelp.ExecuteNonQuery(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("@POID", poid, MyDbType.Int),
                    DbUtility.GetParameter("@Status", status, MyDbType.String),
                    DbUtility.GetParameter("@poShpTerms", poShpTerms, MyDbType.String),
                    DbUtility.GetParameter("poRef", this.PoRef,typeof(string)),
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void UpdatePOExchangeRate(DbHelper dbHelp, int poid, double poCurrencyExRate)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "Update purchaseorders set poCurrencyExRate=@poCurrencyExRate WHERE poID=@POID ";
            try
            {

                dbHelp.ExecuteNonQuery(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("@POID", poid, MyDbType.Int),
                    DbUtility.GetParameter("@poCurrencyExRate", poCurrencyExRate, MyDbType.Double),
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        //Returns new Po ID
        public int CreateDuplicateOrder(DbHelper dbHelp, int poID, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlPo = "INSERT INTO purchaseorders(poDate,poStatus,poVendorID,poShipVia,poFOBLoc,poShpTerms,poNotes,poCreatedByUserID,";
                sqlPo += " poAuthroisedByUserID,poFaxedEmailDateTime,poCurrencyCode,poCurrencyExRate,poCompanyID,poWhsCode)";
                sqlPo += " SELECT @poDate AS poDate, 'N' AS poStatus, po.poVendorID, po.poShipVia, po.poFOBLoc,po.poShpTerms,po.poNotes,@CreatedUserID AS poCreatedByUserID,";
                sqlPo += " 0 AS poAuthroisedByUserID, po.poFaxedEmailDateTime, po.poCurrencyCode, po.poCurrencyExRate, po.poCompanyID, po.poWhsCode";
                sqlPo += " FROM purchaseorders po WHERE po.poID=@poID";

                string sqlPoItems = "INSERT INTO purchaseorderitems(poID,poItemPrdId,poQty,poUnitPrice,poItemShpToWhsCode,prodIDDesc,poRcvdQty, poLandedPrice)";
                sqlPoItems += " SELECT @NewPoID AS poID, poi.poItemPrdId, poi.poQty,poi.poUnitPrice,poi.poItemShpToWhsCode, poi.prodIDDesc, 0 AS poRcvdQty, poi.poLandedPrice";
                sqlPoItems += " FROM purchaseorderitems poi WHERE poi.poID=@OldPoID";

                if (dbHelp.ExecuteNonQuery(sqlPo, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("poID", poID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedUserID", userID, MyDbType.Int),
                    DbUtility.GetParameter("poDate", DateTime.Now, MyDbType.DateTime)
                }) > 0)
                {
                    int newPoId = dbHelp.GetLastInsertID();
                    if (newPoId > 0)
                    {
                        dbHelp.ExecuteNonQuery(sqlPoItems, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("NewPoID", newPoId, MyDbType.Int),
                            DbUtility.GetParameter("OldPoID", poID, MyDbType.Int)
                        });
                    }

                    return newPoId;
                }

                return 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        //public DataTable GetProductDetailStyle(DbHelper dbHelp, string pStyle, StatusProductType prdType, string lang, int iCollection)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    try
        //    {
        //        DataTable dt = this.GetStyleSearchProduct(dbHelp, ProductSearchFields.ProductStyle, pStyle, 0, prdType, lang, iCollection);
        //        //if (dt != null && dt.Rows.Count > 0)
        //        //{
        //        //    return dt.Rows[0];
        //        //}
        //        return dt;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //public DataTable GetProductByStyleCollection(DbHelper dbHelp, string srcField, string srcText, int compID, StatusProductType prdType, string lang, int iCollectionID)
        public DataTable GetProductByStyleCollection(DbHelper dbHelp, string sStyle, int compID, StatusProductType prdType, string lang, int iCollectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                var listParams = new List<MySqlParameter>();
                //We should show available quantity instead on head
                string sql = "SELECT * FROM  ( SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, pq.prdOhdQty as prdOhdQty, ";
                sql += " p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType, pClothDesc.Style, pClothDesc.Collection AS CollectionID, pc.ShortName AS Collection ,pColor.ColorID,pColor.Color{0} AS Color, pSize.Size{0} AS Size, p.PrdPOQty FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
                sql += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
                sql += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection ";
                sql += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
                sql += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";

                if (prdType == StatusProductType.ServiceProduct)
                {
                    sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                else
                {
                    //sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
                    sql += " LEFT OUTER join prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                listParams.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));
                sql += " p.prdType=@prdType AND p.prdIsActive=1 ";
                listParams.Add(DbUtility.GetParameter("@prdType", (int)prdType, MyDbType.Int));
                if (compID > 0)
                {
                    sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
                    listParams.Add(DbUtility.GetParameter("@WarehouseCompanyID", compID, MyDbType.Int));
                }


                if (iCollectionID > 0)
                {
                    sql += " AND pClothDesc.Collection = @CollectionID";

                    listParams.Add(DbUtility.GetParameter("@CollectionID", iCollectionID, MyDbType.Int));
                }


                if (sStyle != "")
                {
                    //sql += " AND p.Style LIKE CONCAT('%', @Style, '%')";
                    //listParams.Add(DbUtility.GetParameter("@Style", sStyle, MyDbType.String));
                    sql += " AND p.Style = @Style";
                    listParams.Add(DbUtility.GetParameter("@Style", sStyle, MyDbType.String));
                }

                sql += "  GROUP BY pColor.Color{0}, pSize.Size{0} order by p.prdName ";
                sql += "  ) AS Desp";
                sql = string.Format(sql, lang);
                return dbHelp.GetDataTable(sql, CommandType.Text, listParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public DataTable GetProductGroupBYColor(DbHelper dbHelp, string sStyle, int compID, StatusProductType prdType, string lang, int iCollectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                var listParams = new List<MySqlParameter>();
                //We should show available quantity instead on head
                string sql = "SELECT * FROM  ( SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, pq.prdOhdQty as prdOhdQty, ";
                sql += " p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType, pClothDesc.Style, pClothDesc.Collection AS CollectionID, pc.ShortName AS Collection ,pColor.ColorID,pColor.Color{0} AS Color, pSize.Size{0} AS Size, p.prdFOBPrice, p.prdLandedPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang=@descLang ";
                sql += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID ";
                sql += " INNER JOIN ProductCycle as pc on pc.CycleID = pClothDesc.Collection ";
                sql += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
                sql += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";

                if (prdType == StatusProductType.ServiceProduct)
                {
                    sql += " LEFT JOIN prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                else
                {
                    //sql += " inner join prdquantity pq on pq.prdID=p.productID WHERE ";
                    sql += " LEFT OUTER join prdquantity pq on pq.prdID=p.productID WHERE ";
                }
                listParams.Add(DbUtility.GetParameter("@descLang", lang, MyDbType.String));
                sql += " p.prdType=@prdType AND p.prdIsActive=1 ";
                listParams.Add(DbUtility.GetParameter("@prdType", (int)prdType, MyDbType.Int));
                if (compID > 0)
                {
                    sql += " AND pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= @WarehouseCompanyID) and pq.prdOhdQty <> 0";
                    listParams.Add(DbUtility.GetParameter("@WarehouseCompanyID", compID, MyDbType.Int));
                }


                if (iCollectionID > 0)
                {
                    sql += " AND pClothDesc.Collection = @CollectionID";

                    listParams.Add(DbUtility.GetParameter("@CollectionID", iCollectionID, MyDbType.Int));
                }


                if (sStyle != "")
                {
                    //sql += " AND p.Style LIKE CONCAT('%', @Style, '%')";
                    //listParams.Add(DbUtility.GetParameter("@Style", sStyle, MyDbType.String));

                    sql += " AND p.Style = @Style";
                    listParams.Add(DbUtility.GetParameter("@Style", sStyle, MyDbType.String));
                }

                sql += "  GROUP BY pColor.Color{0} order by p.prdName ";
                sql += "  ) AS Desp";
                sql = string.Format(sql, lang);

                DataTable dt = dbHelp.GetDataTable("select * from Products", CommandType.Text, null);


                return dbHelp.GetDataTable(sql, CommandType.Text, listParams.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion
    }
}
