﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PurchaseOrderItems
    {
        #region property
        public int PoItems { get; set; }
        public int PoID { get; set; }
        public int PoItemPrdId { get; set; }
        public double PoQty { get; set; }
        public double PoUnitPrice { get; set; }
        public double PoRcvdQty { get; set; }
        public string PoItemShpToWhsCode { get; set; }
        public string ProdIDDesc { get; set; }
        public DateTime PoItemRcvDateTime { get; set; }
        public double LandedPrice { get; set; }
        public string ProductColor { get; set; }
        public string ProductSize { get; set; }
        #endregion
        #region Functions
        /// <summary>
        /// Inser the Purchase Order Item
        /// </summary>
        /// <returns></returns>
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            //SQL to Insert              
            string sql = "Insert purchaseorderitems (poID, poItemPrdId, poQty, poUnitPrice, poItemShpToWhsCode, poItemRcvDateTime, prodIDDesc, poRcvdQty, poLandedPrice ) Value ( ";
            sql += "  @poID, @poItemPrdId, @poQty, @poUnitPrice, @poItemShpToWhsCode, @poItemRcvDateTime, @prodIDDesc, @poRcvdQty, @poLandedPrice )";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poID", this.PoID,typeof(int)),
                                     DbUtility.GetParameter("poItemPrdId", this.PoItemPrdId,typeof(int)),
                                     DbUtility.GetParameter("poQty", this.PoQty,typeof(double)),
                                     DbUtility.GetParameter("poUnitPrice", this.PoUnitPrice,typeof(double)),
                                     DbUtility.GetParameter("poItemShpToWhsCode", this.PoItemShpToWhsCode,typeof(string)),
                                     DbUtility.GetParameter("poItemRcvDateTime", this.PoItemRcvDateTime,typeof(DateTime)),
                                     DbUtility.GetParameter("prodIDDesc", this.ProdIDDesc,typeof(string)),
                                     DbUtility.GetParameter("poRcvdQty", this.PoRcvdQty,typeof(double)),
                                     DbUtility.GetParameter("poLandedPrice", this.LandedPrice,typeof(double)),
                                 };
            try
            {
                if (dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {
                    this.PoItems = dbHelp.GetLastInsertID();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            //SQL to Update PoItem              
            string sql = "UPDATE purchaseorderitems SET poQty = @poQty, poUnitPrice = @poUnitPrice, poItemShpToWhsCode = @poItemShpToWhsCode, poItemRcvDateTime = @poItemRcvDateTime, prodIDDesc = @prodIDDesc, ";
            sql += " poRcvdQty =  @poRcvdQty, poLandedPrice = @poLandedPrice  ";
            sql += " WHERE poID = @poID AND poItemPrdId = @poItemPrdId "; 
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poID", this.PoID,typeof(int)),
                                     DbUtility.GetParameter("poItemPrdId", this.PoItemPrdId,typeof(int)),
                                     DbUtility.GetParameter("poQty", this.PoQty,typeof(double)),
                                     DbUtility.GetParameter("poUnitPrice", this.PoUnitPrice,typeof(double)),
                                     DbUtility.GetParameter("poItemShpToWhsCode", this.PoItemShpToWhsCode,typeof(string)),
                                     DbUtility.GetParameter("poItemRcvDateTime", this.PoItemRcvDateTime,typeof(DateTime)),
                                     DbUtility.GetParameter("prodIDDesc", this.ProdIDDesc,typeof(string)),
                                     DbUtility.GetParameter("poRcvdQty", this.PoRcvdQty,typeof(double)),
                                     DbUtility.GetParameter("poLandedPrice", this.LandedPrice,typeof(double)),
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean POItemExists(DbHelper dbHelp, int poID, int itemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " SELECT * FROM purchaseorderitems WHERE PoID = @poID AND poItemPrdID = @poItemPrdId ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poID", this.PoID,typeof(int)),
                                     DbUtility.GetParameter("poItemPrdId", this.PoItemPrdId,typeof(int)),
                                 };
            try
            {
                DataTable dt = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, p);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetPOItemValue(DbHelper dbHelp, int poID, int itemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " SELECT POItems FROM purchaseorderitems WHERE PoID = @poID AND poItemPrdID = @poItemPrdId ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poID", this.PoID,typeof(int)),
                                     DbUtility.GetParameter("poItemPrdId", this.PoItemPrdId,typeof(int)),
                                 };
            try
            {
                //DataTable dt = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, p);
                object ob = dbHelp.GetValue(sql, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(ob);
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void Update(DbHelper dbHelp, int poitmID, double qty, double unitPrice, string whsCode, string prdDesc, double prdlandedPrice)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlUpdate = "UPDATE purchaseorderitems SET poQty=@poQty, poUnitPrice=@poUnitPrice, poItemShpToWhsCode=@poItemShpToWhsCode, prodIDDesc=@prodIDDesc, poLandedPrice = @prdlandedPrice WHERE poItems=@poItems";
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("poQty", qty, MyDbType.Double),
                    DbUtility.GetParameter("poUnitPrice", unitPrice, MyDbType.Double),
                    DbUtility.GetParameter("poItemShpToWhsCode", whsCode, MyDbType.String),
                    DbUtility.GetParameter("prodIDDesc", prdDesc, MyDbType.String),
                    DbUtility.GetParameter("poItems", poitmID, MyDbType.Int),
                    DbUtility.GetParameter("prdlandedPrice", prdlandedPrice, MyDbType.Double),
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int poItemID)
        {
            //string sql = "SELECT  * FROM purchaseorderitems where poItems=@poItems";

            string sql = " SELECT pi.*, pc.Color{0} AS Color, Size{0}  AS Size FROM purchaseorderitems AS pi ";
            sql += "  INNER JOIN Products AS p ON pi.poItemPrdId = p.ProductID ";
            sql += "  INNER JOIN ProductSize AS ps ON ps.SizeID = p.SizeID ";
            sql += "  INNER JOIN ProductColor AS pc ON pc.ColorID = p.ColID ";
            sql += "  where poItems=@poItems ";

            sql = string.Format(sql, Globals.CurrentAppLanguageCode);
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poItems", poItemID, MyDbType.Int) });
                while (dr.Read())
                {
                    this.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                    this.PoID = BusinessUtility.GetInt(dr["poID"]);
                    this.PoItemPrdId = BusinessUtility.GetInt(dr["poItemPrdId"]);
                    this.PoItemRcvDateTime = BusinessUtility.GetDateTime(dr["PoItemRcvDateTime"]);
                    this.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                    this.PoItemShpToWhsCode = BusinessUtility.GetString(dr["poItemShpToWhsCode"]);
                    this.PoQty = BusinessUtility.GetInt(dr["poQty"]);
                    this.PoRcvdQty = BusinessUtility.GetInt(dr["poRcvdQty"]);
                    this.PoUnitPrice = BusinessUtility.GetDouble(dr["poUnitPrice"]);
                    this.ProdIDDesc = BusinessUtility.GetString(dr["prodIDDesc"]);
                    this.LandedPrice = BusinessUtility.GetDouble(dr["poLandedPrice"]);
                    this.ProductColor = BusinessUtility.GetString(dr["Color"]);
                    this.ProductSize = BusinessUtility.GetString(dr["Size"]);

                }
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void PopulateObject(DbHelper dbHelp, int poItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            //string sql = "SELECT  * FROM purchaseorderitems where poItems=@poItems";  


            string sql = " SELECT pi.*, pc.Color{0} AS Color, Size{0}  AS Size FROM purchaseorderitems AS pi ";
            sql += "  INNER JOIN Products AS p ON pi.poItemPrdId = p.ProductID ";
            sql += "  join productclothdesc pcd on pcd.ProductID = p.productID ";
            sql += "  INNER JOIN ProductSize AS ps ON ps.SizeID = pcd.size  ";
            sql += "  INNER JOIN ProductColor AS pc ON pc.ColorID = pcd.Color ";
            sql += "  where poItems=@poItems ";

            sql = string.Format(sql, Globals.CurrentAppLanguageCode);
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poItems", poItemID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        this.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                        this.PoID = BusinessUtility.GetInt(dr["poID"]);
                        this.PoItemPrdId = BusinessUtility.GetInt(dr["poItemPrdId"]);
                        this.PoItemRcvDateTime = BusinessUtility.GetDateTime(dr["PoItemRcvDateTime"]);
                        this.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                        this.PoItemShpToWhsCode = BusinessUtility.GetString(dr["poItemShpToWhsCode"]);
                        this.PoQty = BusinessUtility.GetInt(dr["poQty"]);
                        this.PoRcvdQty = BusinessUtility.GetInt(dr["poRcvdQty"]);
                        this.PoUnitPrice = BusinessUtility.GetDouble(dr["poUnitPrice"]);
                        this.ProdIDDesc = BusinessUtility.GetString(dr["prodIDDesc"]);
                        this.LandedPrice = BusinessUtility.GetDouble(dr["poLandedPrice"]);
                        this.ProductColor = BusinessUtility.GetString(dr["Color"]);
                        this.ProductSize = BusinessUtility.GetString(dr["Size"]);
                    }
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public bool UpdatePurchaseOrderItemQuantity(int itemId, double rcvdQty, DateTime rcvdDateTime) {
        //    string sql = "UPDATE purchaseorderitems set poRcvdQty=@poRcvdQty,poItemRcvDateTime=@poItemRcvDateTime WHERE poItems=@poItems";
        //    MySqlParameter[] p = { 
        //                             DbUtility.GetParameter("poRcvdQty", rcvdQty, MyDbType.Double),
        //                             DbUtility.GetParameter("poItemRcvDateTime", rcvdDateTime, MyDbType.DateTime),
        //                             DbUtility.GetParameter("poItems", itemId, MyDbType.Int)
        //                         };
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
        //        return true;
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally { 
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public bool UpdatePurchaseOrderItemQuantity(DbHelper dbHelp, int itemId, double rcvdQty, DateTime rcvdDateTime)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE purchaseorderitems set poRcvdQty=@poRcvdQty,poItemRcvDateTime=@poItemRcvDateTime WHERE poItems=@poItems";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("poRcvdQty", rcvdQty, MyDbType.Double),
                                     DbUtility.GetParameter("poItemRcvDateTime", rcvdDateTime, MyDbType.DateTime),
                                     DbUtility.GetParameter("poItems", itemId, MyDbType.Int)
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public bool UpdateReceivedProductQuantity(int productid, string whsCode, double quantityToAdjust)
        //{
        //    ProductQuantity pQty = new ProductQuantity();
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        if (pQty.IsProductQuantityExists(dbHelp, productid, whsCode))
        //        {
        //            pQty.PopulateObject(dbHelp, productid, whsCode);
        //            pQty.UpdateProductQuantity(dbHelp, pQty.Id, quantityToAdjust + pQty.PrdOhdQty);
        //        }
        //        else
        //        {
        //            pQty.PrdID = productid;
        //            pQty.PrdWhsCode = whsCode;
        //            pQty.PrdOhdQty = quantityToAdjust;
        //            pQty.PrdQuoteRsv = 0;
        //            pQty.PrdSORsv = 0;
        //            pQty.PrdDefectiveQty = 0;
        //            pQty.PrdTaxCode = 0;
        //            pQty.Insert(dbHelp);
        //        }

        //        return true;
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public bool UpdateReceivedProductQuantity(DbHelper dbHelp, int productid, string whsCode, double quantityToAdjust)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            ProductQuantity pQty = new ProductQuantity();
            InventoryMovment objIM = new InventoryMovment();
            try
            {
                if (pQty.IsProductQuantityExists(dbHelp, productid, whsCode))
                {
                    pQty.PopulateObject(dbHelp, productid, whsCode);
                    if (pQty.UpdateProductQuantity(dbHelp, pQty.Id, quantityToAdjust + pQty.PrdOhdQty))
                    {
                        objIM.AddMovment(whsCode, CurrentUser.UserID, productid, BusinessUtility.GetString(InvMovmentSrc.PO), BusinessUtility.GetInt(quantityToAdjust),BusinessUtility.GetString(InvMovmentUpdateType.INC));
                    }
                }
                else
                {
                    pQty.PrdID = productid;
                    pQty.PrdWhsCode = whsCode;
                    pQty.PrdOhdQty = quantityToAdjust;
                    pQty.PrdQuoteRsv = 0;
                    pQty.PrdSORsv = 0;
                    pQty.PrdDefectiveQty = 0;
                    pQty.PrdTaxCode = 0;
                    pQty.Insert(dbHelp);
                }

                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetRemainingPOItemsQty(DbHelper dbHelp, int itemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT * FROM purchaseorderitems Where poItems=@poItems";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poItems", itemID, MyDbType.Int) });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetRemainingItemQtyToReceive(DbHelper dbHelp, int poID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT (SUM(poQty) - SUM(poRcvdQty)) AS RemainingQty FROM purchaseorderitems WHERE poID=@poID  GROUP BY poID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poID", poID, MyDbType.Int) });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<PurchaseOrderItems> GetItems(DbHelper dbHelp, int poid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT  * FROM purchaseorderitems WHERE poID=@poID";
            MySqlDataReader dr = null;
            List<PurchaseOrderItems> lResult = new List<PurchaseOrderItems>();
            try
            {
                using (dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("poID", poid, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        PurchaseOrderItems pi = new PurchaseOrderItems();
                        pi.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                        pi.PoID = BusinessUtility.GetInt(dr["poID"]);
                        pi.PoItemPrdId = BusinessUtility.GetInt(dr["poItemPrdId"]);
                        pi.PoItemRcvDateTime = BusinessUtility.GetDateTime(dr["PoItemRcvDateTime"]);
                        pi.PoItems = BusinessUtility.GetInt(dr["poItems"]);
                        pi.PoItemShpToWhsCode = BusinessUtility.GetString(dr["poItemShpToWhsCode"]);
                        pi.PoQty = BusinessUtility.GetInt(dr["poQty"]);
                        pi.PoRcvdQty = BusinessUtility.GetInt(dr["poRcvdQty"]);
                        pi.PoUnitPrice = BusinessUtility.GetDouble(dr["poUnitPrice"]);
                        pi.ProdIDDesc = BusinessUtility.GetString(dr["prodIDDesc"]);
                        pi.LandedPrice = BusinessUtility.GetDouble(dr["poLandedPrice"]);
                        lResult.Add(pi);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public DataTable GetPurchaseOrderItems(DbHelper dbHelp, int poID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = " SELECT poi.poItems AS PurchaseOrderItemID, p.productID, CASE poi.prodIDDesc='' WHEN False THEN poi.prodIDDesc Else p.prdName  END as prdName, p.prdUPCCode, poi.poQty, poi.poUnitPrice, poi.poLandedPrice,  poi.poItemShpToWhsCode, v.vendorID, v.vendorName, pc.Color{0} AS Color, Size{0} AS Size ";
            strSQL += " FROM products as p inner join purchaseorderitems as poi on poi.poItemPrdId = p.productID inner join purchaseorders as po on po.poID = poi.poID inner join vendor as v on v.vendorID = po.poVendorID INNER JOIN ProductClothDesc AS PsDsc ON PsDsc.ProductID = p.productID INNER JOIN ProductSize AS ps ON ps.SizeID = PsDsc.Size INNER JOIN ProductColor AS pc ON pc.ColorID = PsDsc.Color ";
            strSQL += " where po.poID = @poID";

            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            try
            {
                return dbHelp.GetDataTable(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("@poID", poID, typeof(int)) });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion
    }
}