﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class RecievedJournal
    {        
        #region property
        public int Tagid { get; set; }
        public int PoId { get; set; }
        public DateTime WhenRecieved { get; set; }
        #endregion
        #region Functions
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSqlMaxtag = "SELECT MAX(tagid) FROM recievedjournal";

            object objMaxtag = dbHelp.GetValue(strSqlMaxtag, System.Data.CommandType.Text, null);
            if (objMaxtag != null)
                this.Tagid = 1 + BusinessUtility.GetInt(objMaxtag);
            else
                this.Tagid = 1;

            //SQL to Insert              
            string sql = "INSERT INTO recievedjournal(tagid, whenRecieved, poid )";
            sql += "  VALUES(@tagid, @whenRecieved, @poid)";
            MySqlParameter[] p = { 
                                    DbUtility.GetParameter("tagid", this.Tagid,typeof(int)),
                                    DbUtility.GetParameter("whenRecieved", this.WhenRecieved,typeof(DateTime)),
                                    DbUtility.GetParameter("poid", this.PoId,typeof(int))
                                 };
            
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);                
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}
