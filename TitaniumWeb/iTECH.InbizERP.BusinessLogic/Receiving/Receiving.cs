﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Receiving
    {
        #region Properties
        public int RecID { get; set; }
        public int RecPOID { get; set; }
        public int RecByUserID { get; set; }
        public string RecAtWhsCode { get; set; }
        public DateTime RecDateTime { get; set; }
        #endregion
        #region Functions

        public void PopulateObjectByPoID(DbHelper dbHelp, int poid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT recID, recDateTime, recPOID, recByUserID, recAtWhsCode FROM  receiving  WHERE recPOID = @recPOID";
            MySqlDataReader dr = null;
            try
            {

                using (dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("recPOID", poid, typeof(int)) }))
                {
                    if (dr.Read())
                    {
                        this.RecPOID = BusinessUtility.GetInt(dr["recPOID"]);
                        this.RecID = BusinessUtility.GetInt(dr["recID"]);
                        this.RecDateTime = BusinessUtility.GetDateTime(dr["recDateTime"]);
                        this.RecByUserID = BusinessUtility.GetInt(dr["recByUserID"]);
                        this.RecAtWhsCode = BusinessUtility.GetString(dr["recAtWhsCode"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void PopulateObject(DbHelper dbHelp, int rcvID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }

            string strSQL = "SELECT recID, recDateTime, recPOID, recByUserID, recAtWhsCode FROM receiving where recID=@recID";
            MySqlDataReader dr = null;
            try
            {

                using (dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("recID", rcvID, typeof(int)) }))
                {
                    if (dr.Read())
                    {
                        this.RecID = BusinessUtility.GetInt(dr["recID"]);
                        this.RecPOID = BusinessUtility.GetInt(dr["recPOID"]);
                        this.RecDateTime = BusinessUtility.GetDateTime(dr["recDateTime"]);
                        this.RecByUserID = BusinessUtility.GetInt(dr["recByUserID"]);
                        this.RecAtWhsCode = BusinessUtility.GetString(dr["recAtWhsCode"]);
                    }
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection(dr);
            }
        }

        /// <summary>
        /// Insert Into Receving 
        /// </summary>
        /// <returns></returns>
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }

            //SQL to Insert              
            string sql = "Insert receiving (recDateTime, recPOID, recByUserID, recAtWhsCode) Value ( ";
            sql += "  @recDateTime, @recPOID, @recByUserID, @recAtWhsCode )";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("recPOID", this.RecPOID,typeof(int)),
                                     DbUtility.GetParameter("recByUserID", this.RecByUserID,typeof(int)),
                                     DbUtility.GetParameter("recAtWhsCode", this.RecAtWhsCode,typeof(string)),
                                     DbUtility.GetParameter("recDateTime", this.RecDateTime,typeof(DateTime))
                                 };
            try
            {
                if (dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {
                    this.RecID = dbHelp.GetLastInsertID();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Update the Receving tag
        /// </summary>
        /// <returns></returns>
        public bool Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }

            //SQL to Update
            string sql = "UPDATE receiving set recDateTime=@recDateTime, recPOID=@recPOID, recByUserID=@recByUserID, recAtWhsCode=@recAtWhsCode ";
            sql += "  Where recID=@recID";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("recPOID", this.RecPOID,typeof(int)),
                                     DbUtility.GetParameter("recID", this.RecID,typeof(int)),
                                     DbUtility.GetParameter("recByUserID", this.RecByUserID,typeof(int)),
                                     DbUtility.GetParameter("recAtWhsCode", this.RecAtWhsCode,typeof(string)),
                                     DbUtility.GetParameter("recDateTime", this.RecDateTime,typeof(DateTime))
                                 };
            try
            {
                if (dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetRecevingIDByPO(DbHelper dbHelp, int poId)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = false;
                dbHelp = new DbHelper(true);
            }

            //SQL to Update
            string sql = "SELECT recID FROM receiving where recPOID=@recPOID ";
            try
            {
                object obj = dbHelp.GetValue(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("recPOID", poId, typeof(int)) });
                return BusinessUtility.GetInt(obj);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string FillGrid(string StatusData, string WhsCode)
        {
            string strSQL = null;
            strSQL = "SELECT distinct recID, recPOID, v.vendorID, v.vendorName, poStatus, DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join vendor v on po.poVendorID=v.vendorID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus='R' ";
            if (WhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode like '" + WhsCode + "%' ";
            }
            //
            Int16 nDOW = default(Int16);
            nDOW = Convert.ToInt16(DateTime.Now.DayOfWeek);
            Int16 nFirst = default(Int16);
            Int16 nLast = default(Int16);

            nFirst = Convert.ToInt16(-nDOW);
            nLast = Convert.ToInt16(nFirst + 6);
            if (StatusData == "T")
            {
                strSQL += " and recDateTime like '" + DateTime.Now.ToString("yyyy-MM-dd") + "%' ";
            }
            else if (StatusData == "C")
            {
                strSQL += " and (recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59') ";
            }
            else if (StatusData == "N")
            {
                if (nDOW == 0)
                {
                    nFirst = Convert.ToInt16(nFirst + 7);
                }
                else
                {
                    nFirst = Convert.ToInt16(nFirst + 7);
                }
                nLast = Convert.ToInt16(nFirst + 6);
                strSQL += " and (recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59') ";
            }
            strSQL += " order by recID desc ";
            return strSQL;
        }


        //   Parametrised function for view receiveing Form 
        public string FillGrid(ParameterCollection pCol, string StatusData, string SearchData, string WhsCode)
        {
            pCol.Clear();
            string strSQL = null;

            strSQL = "SELECT distinct recID, recPOID, v.vendorID, v.vendorName, poStatus, DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime  FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join vendor v on po.poVendorID=v.vendorID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus IN ('R','Y') ";
            if (WhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode  LIKE CONCAT('%', @WhsCode, '%') ";
                pCol.Add("@WhsCode", WhsCode);
            }
            //
            int nDOW = (int)DateTime.Now.DayOfWeek;
            int nFirst = 0;
            int nLast = 0;

            nFirst = -nDOW;
            nLast = nFirst + 6;
            if (StatusData == "PO" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and po.poID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "RN" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and recID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "VI" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and v.vendorID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "UC" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and prdUPCCode = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            if (StatusData == "T")
            {
                strSQL += " and recDateTime like '" + DateTime.Now.ToString("yyyy-MM-dd") + "%' ";
            }
            else if (StatusData == "C")
            {
                strSQL += " and recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59' ";
            }
            else if (StatusData == "N")
            {
                if (nDOW == 0)
                {
                    nFirst = nFirst + 7;
                }
                else
                {
                    nFirst = nFirst + 6;
                }
                nLast = nFirst + 6;
                strSQL += " and recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59' ";
            }

            strSQL += " order by recDateTime asc ";
            return strSQL;
        }

        public string GetSql(ParameterCollection pCol, string whsCode, int poID, int recID, string lang, string StatusData, string SearchData)
        {
            pCol.Clear();
            string strSQL = null;
            strSQL = "SELECT i.poItems, i.poID, i.poItemPrdId, i.poQty, i.poRcvdQty,(i.poQty-i.poRcvdQty) as Quantity, i.poUnitPrice, i.poItemShpToWhsCode, i.poItemRcvDateTime, i.prodIDDesc,  p.prdIntID, p.prdExtID, p.prdUPCCode, prdName, text.Texture{0} as textureName, colr.color{0} AS colorName, size.size{0} AS sizeName  ";
            strSQL += " FROM purchaseorderitems i inner join purchaseorders po on po.poID=i.poID INNER JOIN vendor v ON v.VendorID = po.poVendorID inner join products p on i.poItemPrdId=p.productID inner join receiving r on r.recPOID=po.poID and r.recAtWhsCode=i.poItemShpToWhsCode ";
            strSQL += "LEFT OUTER JOIN producttexture text on text.TextureID = p.TextureID INNER JOIN ProductClothDesc AS PsDsc ON PsDsc.ProductID = p.productID  join productcolor colr on colr.colorID = PsDsc.Color  join productsize size on size.sizeID = PsDsc.Size where i.poItemShpToWhsCode=@recAtWhsCode and r.recPOID=@recPOID and r.recid=@recID ";
            strSQL = string.Format(strSQL, lang);
            pCol.Add("@recAtWhsCode", DbType.String, whsCode);
            pCol.Add("@recPOID", DbType.Int16, Convert.ToString(poID));
            pCol.Add("@recID", DbType.Int16, Convert.ToString(recID));


            int nDOW = (int)DateTime.Now.DayOfWeek;
            int nFirst = 0;
            int nLast = 0;

            nFirst = -nDOW;
            nLast = nFirst + 6;
            if (StatusData == "PO" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and po.poID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "RN" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and recID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "VI" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and v.vendorID = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            else if (StatusData == "UC" & !string.IsNullOrEmpty(SearchData))
            {
                strSQL += " and prdUPCCode = @SearchData ";
                pCol.Add("@SearchData", SearchData);
            }
            if (StatusData == "T")
            {
                strSQL += " and recDateTime like '" + DateTime.Now.ToString("yyyy-MM-dd") + "%' ";
            }
            else if (StatusData == "C")
            {
                strSQL += " and recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59' ";
            }
            else if (StatusData == "N")
            {
                if (nDOW == 0)
                {
                    nFirst = nFirst + 7;
                }
                else
                {
                    nFirst = nFirst + 6;
                }
                nLast = nFirst + 6;
                strSQL += " and recDateTime >= '" + DateTime.Now.AddDays(nFirst).ToString("yyyy-MM-dd") + " 00:00:00' ";
                strSQL += " and recDateTime <= '" + DateTime.Now.AddDays(nLast).ToString("yyyy-MM-dd") + " 23:59:59' ";
            }


            strSQL += " order by r.recID desc ";

            return strSQL;
        }

        public bool CheckRecevingForPO(string recAtWhsCode, int recPOID)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where recPOID=@recPOID and poStatus='R' ";
            if (recAtWhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode=@WhsCode";
            }
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("@recPOID", recPOID,typeof(int)),
                                     DbUtility.GetParameter("@WhsCode", recAtWhsCode,typeof(string))
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object obj = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool CheckRecevingForReceiptNo(string recAtWhsCode, int recID)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where recID=@recPOID and poStatus='R'";
            if (recAtWhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode=@WhsCode";
            }
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("@recPOID", recID,typeof(int)),
                                     DbUtility.GetParameter("@WhsCode", recAtWhsCode,typeof(string))
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object obj = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool CheckRecevingForVendor(string SearchData, string recAtWhsCode)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where p.poVendorID=@SearchData and poStatus='R' ";
            if (recAtWhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode=@WhsCode";
            }
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("@SearchData", SearchData,typeof(string)),
                                     DbUtility.GetParameter("@WhsCode", recAtWhsCode,typeof(string))
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object obj = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool CheckRecevingForUPCCode(string SearchData, string recAtWhsCode)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where p.prdUPCCode=@SearchData and poStatus='R' ";
            if (recAtWhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode=@WhsCode";
            }
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("@SearchData", SearchData,typeof(string)),
                                     DbUtility.GetParameter("@WhsCode", recAtWhsCode,typeof(string))
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object obj = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool CheckReceving(string SearchData, string recAtWhsCode)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus IN ('R','Y') ";

            List<MySqlParameter> lstP = new List<MySqlParameter>();

            if (recAtWhsCode != "ALL")
            {
                strSQL += " and recAtWhsCode=@WhsCode";
                lstP.Add(DbUtility.GetParameter("@WhsCode", recAtWhsCode, typeof(string)));
            }
            int nDOW = (int)DateTime.Now.DayOfWeek;
            int nFirst = 0;
            int nLast = 0;

            nFirst = -nDOW;
            nLast = nFirst + 6;
            if (SearchData == "T")
            {
                strSQL += " and recDateTime=@recDateTime ";
                lstP.Add(DbUtility.GetParameter("@recDateTime", DateTime.Now, typeof(DateTime)));
            }
            else if (SearchData == "C")
            {
                strSQL += " and (recDateTime >= @nFirst ";
                strSQL += " and recDateTime <= @nLast) ";

                lstP.Add(DbUtility.GetParameter("@nFirst", DateTime.Now.AddDays(nFirst), typeof(DateTime)));
                lstP.Add(DbUtility.GetParameter("@nLast", DateTime.Now.AddDays(nLast), typeof(DateTime)));
            }
            else if (SearchData == "N")
            {
                if (nDOW == 0)
                {
                    nFirst = nFirst + 7;
                }
                else
                {
                    nFirst = nFirst + 6;
                }
                nLast = nFirst + 6;
                strSQL += " and (recDateTime >= @nFirst";
                strSQL += " and recDateTime <= @nLast)";

                lstP.Add(DbUtility.GetParameter("@nFirst", DateTime.Now.AddDays(nFirst), typeof(DateTime)));
                lstP.Add(DbUtility.GetParameter("@nLast", DateTime.Now.AddDays(nLast), typeof(DateTime)));
            }

            DbHelper dbHelp = new DbHelper();
            try
            {
                object obj = dbHelp.GetValue(strSQL, System.Data.CommandType.Text, lstP.ToArray());
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID, string whsCode)
        {
            pCol.Clear();
            string strSQL = "select pi.poID,max(poQty) as poQty,poUnitPrice,DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime from purchaseorderitems as pi";
            strSQL += " Inner join purchaseorders on purchaseorders.poID=pi.poID ";
            strSQL += " Inner join receiving on receiving.recPOID=pi.poID ";
            strSQL += " where poItemPrdId=@poItemPrdId and poStatus='R'  ";
            pCol.Add("@poItemPrdId", productID.ToString());
            if (!string.IsNullOrEmpty(whsCode))
            {
                strSQL += " and poItemShpToWhsCode=@poItemShpToWhsCode";
                pCol.Add("@poItemShpToWhsCode", whsCode);
            }
            strSQL += " Group by pi.poID";

            return strSQL;
        }

        public DataTable CheckPOForSKU(string recAtWhsCode, string sBarCode)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT p.poID FROM  purchaseorders p ");
            sbQuery.Append(" INNER JOIN purchaseorderitems i on p.poID = i.poID ");
            sbQuery.Append(" INNER JOIN Products prd on prd.ProductID = i.poItemPrdID ");
            sbQuery.Append(" where  poStatus='R'  AND prd.prdUPCCode= @SKU AND i.poItemShpToWhsCode=@WhsCode ");
            sbQuery.Append(" GROUP BY p.poID ");
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("@SKU", sBarCode,typeof(string)),
                                     DbUtility.GetParameter("@WhsCode", recAtWhsCode,typeof(string))
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                return  dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), System.Data.CommandType.Text, p);
                
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion
    }
}
