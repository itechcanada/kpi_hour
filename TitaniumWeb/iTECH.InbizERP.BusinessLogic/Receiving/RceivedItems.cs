﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;


namespace iTECH.InbizERP.BusinessLogic
{
    public class RceivedItems
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private int _poItems;

        public int PoItems
        {
            get { return _poItems; }
            set { _poItems = value; }
        }
        private int _rcvdQty;

        public int RcvdQty
        {
            get { return _rcvdQty; }
            set { _rcvdQty = value; }
        }
        private DateTime _poItemRcvDateTime;

        public DateTime PoItemRcvDateTime
        {
            get { return _poItemRcvDateTime; }
            set { _poItemRcvDateTime = value; }
        }
        private int _recId;

        public int RecId
        {
            get { return _recId; }
            set { _recId = value; }
        }

        public bool Insert() {
            string sql = "INSERT INTO receivedItems(poitems, rcvdQty, recId, poItemRcvDateTime) VALUES(@poitems, @rcvdQty, @recId, @poItemRcvDateTime)";
            DbHelper dbHelp = new DbHelper();
            try
            {
                MySqlParameter[] p = {
                                         DbUtility.GetParameter("poitems", this.PoItems, MyDbType.Int),
                                         DbUtility.GetParameter("rcvdQty", this.RcvdQty, MyDbType.Int),
                                         DbUtility.GetParameter("recId", this.RecId, MyDbType.Int),
                                         DbUtility.GetParameter("poItemRcvDateTime", this.PoItemRcvDateTime, MyDbType.DateTime)
                                     };
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO receivedItems(poitems, rcvdQty, recId, poItemRcvDateTime) VALUES(@poitems, @rcvdQty, @recId, @poItemRcvDateTime)";            
            try
            {
                MySqlParameter[] p = {
                                         DbUtility.GetParameter("poitems", this.PoItems, MyDbType.Int),
                                         DbUtility.GetParameter("rcvdQty", this.RcvdQty, MyDbType.Int),
                                         DbUtility.GetParameter("recId", this.RecId, MyDbType.Int),
                                         DbUtility.GetParameter("poItemRcvDateTime", this.PoItemRcvDateTime, MyDbType.DateTime)
                                     };
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool IsReceivingExists(int recId)
        {
            string sql = "SELECT count(*) FROM receivedItems where recID=@recID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("recID", recId, MyDbType.Int) });
                return BusinessUtility.GetInt(scalar) > 0;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
