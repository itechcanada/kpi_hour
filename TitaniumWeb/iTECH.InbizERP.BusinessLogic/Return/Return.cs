﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;


using System.Linq;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderReturn
    {
        public int ReturnID { get; set; }
        public DateTime ReturnDate { get; set; }
        public int ReturnOrderID { get; set; }
        public int ReturnOrderItemID { get; set; }
        public int ReturnProductID { get; set; }
        public double ReturnProductQty { get; set; }
        public double ProductInOrderQty { get; set; }
        public double ProductUnitPrice { get; set; }
        public int ReturnBy { get; set; }

        //There is no need to track taxcode after using Writeoff value to calculate tax returned on 2013-03-20 03:00 PM we are keeping this just as working snipt of code.
        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }
        

        public double ReturnAndGetWriteOff(DbHelper dbHelp, int orderItemID, double qtyToReturn, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {                
                string sql = "INSERT INTO z_order_return (ReturnDate,ReturnOrderID,ReturnOrderItemID,ReturnProductID,";
                sql += " ReturnProductQty,ProductInOrderQty,ProductUnitPrice,ReturnAmount,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,";
                sql += " TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5,ReturnBy)";
                sql += " SELECT @ReturnDate AS ReturnDate, ordID AS ReturnOrderID, orderItemID AS ReturnOrderItemID,ordProductID AS ReturnProductID,";
                sql += " @ReturnProductQty AS ReturnProductQty, ordProductQty AS ProductInOrderQty, ordProductUnitPrice AS ProductUnitPrice,";
                sql += " @ReturnAmount AS ReturnAmount, Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3, TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,@TaxCalculated1,@TaxCalculated2,@TaxCalculated3,@TaxCalculated4,@TaxCalculated5, @ReturnBy AS ReturnBy";
                sql += " FROM orderitems WHERE orderItemID=@OrderItemID";

                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;

                string sqlAmount = "SELECT * FROM vw_order_item_amount WHERE OrderItemID=@OrderItemID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderItemID", orderItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }
                    }
                }

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ReturnDate",  DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ReturnProductQty", qtyToReturn, MyDbType.Double),
                    DbUtility.GetParameter("OrderItemID", orderItemID, MyDbType.Int),
                    DbUtility.GetParameter("ReturnAmount", returnAmount, MyDbType.Double),
                    DbUtility.GetParameter("ReturnBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("TaxCalculated1", taxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", taxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", taxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", taxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", taxCalculated5, MyDbType.Double)
                });

                int returnID = dbHelp.GetLastInsertID();
                this.PopulateObject(dbHelp, returnID);

                return returnAmount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetOrderReturnWriteOff(DbHelper dbHelp, int orderItemID, double qtyToReturn)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;

                string sqlAmount = "SELECT * FROM vw_order_item_amount WHERE OrderItemID=@OrderItemID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderItemID", orderItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }
                    }
                }
                return returnAmount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int returnID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_order_return WHERE ReturnID=@ReturnID";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ReturnID", returnID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.ProductInOrderQty = BusinessUtility.GetDouble(dr["ProductInOrderQty"]);
                        this.ProductUnitPrice = BusinessUtility.GetDouble(dr["ProductUnitPrice"]);
                        this.ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]);
                        this.ReturnID = BusinessUtility.GetInt(dr["ReturnID"]);
                        this.ReturnOrderID = BusinessUtility.GetInt(dr["ReturnOrderID"]);
                        this.ReturnOrderItemID = BusinessUtility.GetInt(dr["ReturnOrderItemID"]);
                        this.ReturnProductID = BusinessUtility.GetInt(dr["ReturnProductID"]);
                        this.ReturnProductQty = BusinessUtility.GetDouble(dr["ReturnProductQty"]);
                        this.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        this.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        this.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        this.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        this.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        this.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        this.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        this.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        this.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                        this.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                        this.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        this.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        this.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        this.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        this.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        this.ReturnBy = BusinessUtility.GetInt(dr["ReturnBy"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<InvoiceReturn> GetInvoiceReturnsList(DbHelper dbHelp, int orderItemID, int invoiceItemID, int invoiceID)
        {
            List<InvoiceReturn> lResult = new List<InvoiceReturn>();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_order_return WHERE ReturnOrderItemID=@ReturnOrderItemID";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ReturnOrderItemID", orderItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        InvoiceReturn item = new InvoiceReturn();
                        item.ProductInOrderQty = BusinessUtility.GetDouble(dr["ProductInOrderQty"]);
                        item.ProductUnitPrice = BusinessUtility.GetDouble(dr["ProductUnitPrice"]);
                        item.ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]);
                        item.ReturnID = BusinessUtility.GetInt(dr["ReturnID"]);
                        item.ReturnInvoiceID = invoiceID;
                        item.ReturnInvoiceItemID = invoiceItemID;
                        item.ReturnProductID = BusinessUtility.GetInt(dr["ReturnProductID"]);
                        item.ReturnProductQty = BusinessUtility.GetDouble(dr["ReturnProductQty"]);
                        item.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        item.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        item.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        item.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        item.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        item.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        item.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        item.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        item.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                        item.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                        item.ReturnBy = BusinessUtility.GetInt(dr["ReturnBy"]);
                        item.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        item.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        item.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        item.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        item.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        lResult.Add(item);
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<InvoiceProcessReturn> GetInvoiceProcessReturnsList(DbHelper dbHelp, int orderProcessItemID, int invoiceProcessItemID, int invoiceID)
        {
            List<InvoiceProcessReturn> lResult = new List<InvoiceProcessReturn>();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_order_return_process WHERE OrderProcessItemID=@OrderProcessItemID";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        InvoiceProcessReturn item = new InvoiceProcessReturn();                      
                        item.ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]);
                        item.ReturnID = BusinessUtility.GetInt(dr["ReturnID"]);
                        item.ReturnInvoiceID = invoiceID;
                        item.InvoiceProcessItemID = invoiceProcessItemID;                        
                        item.ReturnBy = BusinessUtility.GetInt(dr["ReturnBy"]);
                        item.ReturnAmount = BusinessUtility.GetDouble(dr["ReturnAmount"]);
                        item.InvoiceItemProcessCode = BusinessUtility.GetString(dr["OrderItemProcessCode"]);                                              
                        lResult.Add(item);
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetReturnHistorySql(ParameterCollection pCol, int ordID)
        {
            pCol.Clear();
            string sql = "SELECT ReturnID,ReturnProductID,prdUPCCode,prdName,ReturnProductQty, TextedAmount AS AdjustedAmount,ReturnDate,UserName FROM vw_return_history_ord WHERE OrderID=@OrderID";
            pCol.Add("@OrderID", ordID.ToString());
            return sql; 
        }

        public double GetPreviousReturnedQty(DbHelper dbHelp, int orderItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {                
                string sql = "SELECT SUM(ReturnProductQty) AS QtySum FROM z_order_return WHERE ReturnOrderItemID=@ReturnOrderItemID GROUP BY ReturnOrderItemID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReturnOrderItemID", orderItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //Common function for order return & invoice return as well
        public void ReturnAndUpdateInventory(DbHelper dbHelp, int ordID, int userID, List<ReturnCart> lstToReturn, List<ReturnProcessCart> lstProcess, bool isTransfer, string Reason)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, ordID);

                SysWarehouses whs = new SysWarehouses();
                whs.PopulateObject(ord.OrdShpWhsCode, dbHelp);

                Invoice inv = new Invoice();                
                int invID = inv.GetInvoiceID(dbHelp, ordID);
                inv.PopulateObject(dbHelp, invID);
                Product objprd = new Product();
                ProductQuantity pQty = new ProductQuantity();
                double returnAmtWithoutTax = 0.0;
                //if invoice generated return need to perform on invoice
                if (invID > 0)
                {
                    InvoiceReturn invReturn = new InvoiceReturn();
                    InvoiceProcessReturn invProcessRtn = new InvoiceProcessReturn();
                    InventoryMovment objIM = new InventoryMovment();
                    double amountToWriteOff = 0.00D;
                    double amt = 0.00D;
                    foreach (var item in lstToReturn)
                    {
                        if (item.ProductToReturnQty > 0)
                        {
                            //Calculate writeoff
                            amt = invReturn.ReturnAndGetWriteOff(dbHelp, item.InvoiceItemID, item.ProductToReturnQty, userID);
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + amt);
                            if(!objprd.IsProductGift(item.ProductID))
                            {
                            returnAmtWithoutTax += inv.GetPerItemAmtWithoutTax(null, item.InvoiceItemID)*item.ProductToReturnQty;
                            }
                            //Adjust returned quantity in inventory
                            if (isTransfer)
                            {
                                if (!string.IsNullOrEmpty(ord.OrdShpToWhsCode))
                                {
                                    pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpToWhsCode);
                                    objIM.AddMovment(ord.OrdShpToWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.TRN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                }
                                else
                                {
                                    pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, inv.InvShpWhsCode);
                                    objIM.AddMovment(inv.InvShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.TRN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Reason))
                                {
                                    if (Reason == "1" || Reason == "2")
                                    {
                                        if (!string.IsNullOrEmpty(whs.DamagedWarehouse))
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.DamagedWarehouse);
                                            objIM.AddMovment(whs.DamagedWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                        else
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, inv.InvShpWhsCode);
                                            objIM.AddMovment(inv.InvShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(whs.ReservedWarehouse))
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.ReservedWarehouse);
                                            objIM.AddMovment(whs.ReservedWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                        else
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, inv.InvShpWhsCode);
                                            objIM.AddMovment(inv.InvShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(whs.ShippingWarehouse))
                                    {
                                        pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.ShippingWarehouse);
                                        objIM.AddMovment(whs.ShippingWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                    }
                                    else
                                    {
                                        pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, inv.InvShpWhsCode);
                                        objIM.AddMovment(inv.InvShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                    }
                                }
                            }
                        }
                    }

                    foreach (var item in lstProcess)
                    {
                        if (item.AmountToReturn > 0)
                        {
                            invProcessRtn.ReturnAndGetWriteOff(dbHelp, item.InvoiceProcessItemID, item.AmountToReturn, userID);
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + item.AmountToReturn);
                        }
                    }

                    //if (amountToWriteOff > 0 && inv.InvTypeCommission != (int)OrderCommission.POS) //Add Writeoff record in account receivable
                    //{
                    //    //Add Account receivable Entry
                    //    AccountReceivable arc = new AccountReceivable();
                    //    arc.ARAmtRcvd = 0.00D;
                    //    arc.ARAmtRcvdDateTime = DateTime.Now;
                    //    arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.WriteOff;
                    //    arc.ARChequeNo = string.Empty;
                    //    arc.ARColAsigned = false;
                    //    arc.ARContactEmail = string.Empty;
                    //    arc.ARContactNm = string.Empty;
                    //    arc.ARContactPhone = string.Empty;
                    //    arc.ARCreditCardCVVCode = string.Empty;
                    //    arc.ARCreditCardExp = string.Empty;
                    //    arc.ARCreditCardFirstName = string.Empty;
                    //    arc.ARCreditCardLastName = string.Empty;
                    //    arc.ARCreditCardNo = string.Empty;
                    //    arc.ARInvoiceNo = invID;
                    //    if (isTransfer)
                    //    {
                    //        arc.ARNote = "Transfer";
                    //    }
                    //    else
                    //    {
                    //        arc.ARNote = "Returns";
                    //    }
                    //    arc.ARRcvdBy = CurrentUser.UserID;
                    //    arc.ARWriteOff = amountToWriteOff;
                    //    arc.ARReceiptNo = string.Empty;
                    //    arc.Insert(dbHelp, CurrentUser.UserID);
                    //}
                }
                else //Return perform on orderitem
                {
                    OrderReturn ordReturn = new OrderReturn();
                    OrderProcessReturn ordProcRtn = new OrderProcessReturn();
                    InventoryMovment objIM = new InventoryMovment();
                    double amountToWriteOff = 0.00D;
                    foreach (var item in lstToReturn)
                    {
                        if (item.ProductToReturnQty > 0)
                        {
                            //Calculate writeoff
                            amountToWriteOff += ordReturn.ReturnAndGetWriteOff(dbHelp, item.OrderItemID, item.ProductToReturnQty, userID);                                   

                            //Adjust returned quantity in inventory
                            if (isTransfer)
                            {

                                if (!string.IsNullOrEmpty(ord.OrdShpToWhsCode))
                                {
                                    pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpToWhsCode);
                                    objIM.AddMovment(ord.OrdShpToWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.TRN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                }
                                else
                                {
                                    pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpWhsCode);
                                    objIM.AddMovment(ord.OrdShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.TRN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Reason))
                                {
                                    if (Reason == "1" || Reason == "2")
                                    {
                                        if (!string.IsNullOrEmpty(whs.DamagedWarehouse))
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.DamagedWarehouse);
                                            objIM.AddMovment(whs.DamagedWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                        else
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpWhsCode);
                                            objIM.AddMovment(ord.OrdShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(whs.ReservedWarehouse))
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.ReservedWarehouse);
                                            objIM.AddMovment(whs.ReservedWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                        else
                                        {
                                            pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpWhsCode);
                                            objIM.AddMovment(ord.OrdShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(whs.ShippingWarehouse))
                                    {
                                        pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, whs.ShippingWarehouse);
                                        objIM.AddMovment(whs.ShippingWarehouse, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                    }
                                    else
                                    {
                                        pQty.UpdateQuantityOnReturn(dbHelp, item.ProductID, item.ProductToReturnQty, ord.OrdShpWhsCode);
                                        objIM.AddMovment(ord.OrdShpWhsCode, userID, item.ProductID, BusinessUtility.GetString(InvMovmentSrc.RTN), BusinessUtility.GetInt(item.ProductToReturnQty), BusinessUtility.GetString(InvMovmentUpdateType.INC));
                                    }
                                }
                            }
                        }
                    }

                    foreach (var item in lstProcess)
                    {
                        if (item.AmountToReturn > 0)
                        {
                            ordProcRtn.ReturnAndGetWriteOff(dbHelp, item.OrderProcessItemID, item.AmountToReturn, userID);
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + item.AmountToReturn);
                        }
                    }

                    //if (amountToWriteOff > 0 && ord.OrderTypeCommission != (int)OrderCommission.POS) //Add Writeoff record in account receivable
                    //{
                    //    //Add Account receivable Entry
                    //    PreAccountRcv arc = new PreAccountRcv();
                    //    arc.AmountDeposit = 0.00D;
                    //    arc.DateReceived = DateTime.Now;
                    //    arc.PaymentMethod = (int)StatusAmountReceivedVia.WriteOff;
                    //    arc.CustomerID = ord.OrdCustID;
                    //    if (isTransfer)
                    //    {
                    //        arc.Notes = "Transfer";
                    //    }
                    //    else
                    //    {
                    //        arc.Notes = "Returns";
                    //    }
                    //    arc.OrderID = ord.OrdID;
                    //    arc.ReceiptNo = string.Empty;
                    //    arc.WriteOffAmount = amountToWriteOff;
                    //    arc.Insert(dbHelp);
                    //}
                }

                #region LoyaltyPointEntry
                //Entry of LoyalPoint
                Partners part = new Partners();
                double amtPerPoint = whs.GetAmtPerPoint(ord.OrdShpWhsCode);
                int calculatedPoint = 0;
                if (amtPerPoint > 0)
                {
                    calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(returnAmtWithoutTax) / amtPerPoint);
                }
                part.PopulateObject(ord.OrdCustID);
                LoyalPartnerHistory lph = new LoyalPartnerHistory();
                lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                lph.LoyalPartnerID = part.PartnerID;
                lph.PointAuto = false;
                lph.Points = (-1)*calculatedPoint;
                lph.PointsAddedBy = userID;
                lph.PointsAddedOn = DateTime.Now;
                lph.PosTransactionID = invID;
                lph.InvoiceNo = invID;
                lph.OrderNo = ord.OrdID;
                lph.Insert(null);
                #endregion

                // Set Transfer Order Status to be Closed if All Product is Transffered
                if (isTransfer)
                {
                    var result = lstToReturn.OfType<ReturnCart>().Where(s => (s.ProductInOrderQty - (s.PreviousReturnedQty + s.ProductToReturnQty)) > 0);
                    var itemlist = result.Cast<ReturnCart>().ToList();
                    if (itemlist.Count == 0)
                    {
                        ord.UpdateOrderStatus(dbHelp, ordID, SOStatus.CLOSED);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool HasReturn(DbHelper dbHelp, int ordID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT COUNT(*) FROM z_order_return WHERE ReturnOrderID=@OrderID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", ordID, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetReturnHistoryItems(DbHelper dbHelp, int orderID, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = string.Empty;
                //var vResult = new List<object>();
                if (invoiceID <= 0)
                {
                    Invoice inv = new Invoice();
                    invoiceID = inv.GetInvoiceID(dbHelp, orderID);
                }
                if (invoiceID > 0)
                {                    
                    sql = "SELECT ReturnID, ReturnProductID, ProductUPCCode, ProductName, ReturnProductQty, ReturnAmount, ReturnDate, ReturnBy FROM vw_return_history_inv WHERE ReturnInvoiceID=@InvoiceID ORDER BY ReturnDate DESC";
                    return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("InvoiceID", invoiceID, MyDbType.Int)
                    });
                    //using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    //    DbUtility.GetParameter("InvoiceID", invoiceID, MyDbType.Int)
                    //}))
                    //{
                    //    while (dr.Read())
                    //    {
                    //        vResult.Add(new
                    //        {
                    //            ReturnID = BusinessUtility.GetInt(dr["ReturnID"]),
                    //            ReturnProductID = BusinessUtility.GetInt(dr["ReturnProductID"]),
                    //            ProductUPCCode = BusinessUtility.GetString(dr["ProductUPCCode"]),
                    //            ProductName = BusinessUtility.GetString(dr["ProductName"]),
                    //            ReturnProductQty = BusinessUtility.GetDouble(dr["ReturnProductQty"]),
                    //            ReturnAmount = BusinessUtility.GetDouble(dr["ReturnAmount"]),
                    //            ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]),
                    //            ReturnBy = BusinessUtility.GetString(dr["ReturnBy"])
                    //        });
                    //    }
                    //}
                }
                else
                {
                    sql = "SELECT ReturnID, ReturnProductID, ProductUPCCode, ProductName, ReturnProductQty, ReturnAmount, ReturnDate, ReturnBy FROM vw_return_history_ord WHERE ReturnOrderID=@OrderID ORDER BY ReturnDate DESC";
                    return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    });
                    //using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    //    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    //}))
                    //{
                    //    while (dr.Read())
                    //    {
                    //        vResult.Add(new
                    //        {
                    //            ReturnID = BusinessUtility.GetInt(dr["ReturnID"]),
                    //            ReturnProductID = BusinessUtility.GetInt(dr["ReturnProductID"]),
                    //            ProductUPCCode = BusinessUtility.GetString(dr["ProductUPCCode"]),
                    //            ProductName = BusinessUtility.GetString(dr["ProductName"]),
                    //            ReturnProductQty = BusinessUtility.GetDouble(dr["ReturnProductQty"]),
                    //            ReturnAmount = BusinessUtility.GetDouble(dr["ReturnAmount"]),
                    //            ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]),
                    //            ReturnBy = BusinessUtility.GetString(dr["ReturnBy"])
                    //        });
                    //    }
                    //}
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetReturnHistoryProcessItems(DbHelper dbHelp, int orderID, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = string.Empty;
                
                if (invoiceID <= 0)
                {
                    Invoice inv = new Invoice();
                    invoiceID = inv.GetInvoiceID(dbHelp, orderID);
                }
                if (invoiceID > 0)
                {
                    sql = "SELECT ReturnID,ReturnDate,InvoiceItemProcessCode AS ProcessCode,ReturnAmount,funGetUserName(ReturnBy) AS ReturnBy FROM z_invoice_return_process WHERE ReturnInvoiceID=@InvoiceID";
                    return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("InvoiceID", invoiceID , MyDbType.Int)
                    });
                }
                else
                {
                    sql = "SELECT ReturnID,ReturnDate,OrderItemProcessCode AS ProcessCode,ReturnAmount,funGetUserName(ReturnBy) AS ReturnBy FROM z_order_return_process WHERE ReturnOrderID=@OrderID";
                    return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    });                    
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }


    //
    public class InvoiceReturn
    {
        public int ReturnID { get; set; }
        public DateTime ReturnDate { get; set; }
        public int ReturnInvoiceID { get; set; }
        public int ReturnInvoiceItemID { get; set; }
        public int ReturnProductID { get; set; }
        public double ReturnProductQty { get; set; }
        public double ProductInOrderQty { get; set; }
        public double ProductUnitPrice { get; set; }
        public int ReturnBy { get; set; }

        //There is no need to track taxcode after using Writeoff value to calculate tax returned on 2013-03-20 03:00 PM we are keeping this just as working snipt of code.
        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }
        
        public double ReturnAndGetWriteOff(DbHelper dbHelp, int invItemID, double qtyToReturn, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {                
                string sql = "INSERT INTO z_invoice_return (ReturnDate,ReturnInvoiceID,ReturnInvoiceItemID,ReturnProductID,";
                sql += " ReturnProductQty,ProductInOrderQty,ProductUnitPrice,ReturnAmount,Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,";
                sql += " TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,TaxCalculated1,TaxCalculated2,TaxCalculated3,TaxCalculated4,TaxCalculated5,ReturnBy)";
                sql += " SELECT @ReturnDate AS ReturnDate, invoices_invID AS ReturnInvoiceID, invItemID AS ReturnInvoiceItemID,invProductID AS ReturnProductID,";
                sql += " @ReturnProductQty AS ReturnProductQty, invProductQty AS ProductInOrderQty, invProductUnitPrice AS ProductUnitPrice,@ReturnAmount AS ReturnAmount,";
                sql += " Tax1,TaxDesc1,Tax2,TaxDesc2,Tax3,TaxDesc3,Tax4,TaxDesc4,Tax5,TaxDesc5,@TaxCalculated1,@TaxCalculated2,@TaxCalculated3,@TaxCalculated4,@TaxCalculated5,@ReturnBy AS ReturnBy";
                sql += " FROM invoiceitems WHERE invItemID=@InvoiceItemID";
                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;

                string sqlAmount = "SELECT * FROM vw_invoice_item_amount WHERE InvoiceItemID=@InvoiceItemID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("InvoiceItemID", invItemID, MyDbType.Int)
                }))
                {                    
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }                        
                    }
                }
                
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ReturnDate",  DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ReturnProductQty", qtyToReturn, MyDbType.Double),
                    DbUtility.GetParameter("InvoiceItemID", invItemID, MyDbType.Int),
                    DbUtility.GetParameter("ReturnBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ReturnAmount", returnAmount, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated1", taxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", taxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", taxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", taxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", taxCalculated5, MyDbType.Double)
                });

                int returnID = dbHelp.GetLastInsertID();                
                this.PopulateObject(dbHelp, returnID);
                
                return returnAmount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetInvReturnWriteOff(DbHelper dbHelp, int invItemID, double qtyToReturn)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;
                string sqlAmount = "SELECT * FROM vw_invoice_item_amount WHERE InvoiceItemID=@InvoiceItemID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("InvoiceItemID", invItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }
                    }
                }
                return returnAmount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int returnID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_invoice_return WHERE ReturnID=@ReturnID";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ReturnID", returnID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.ProductInOrderQty = BusinessUtility.GetDouble(dr["ProductInOrderQty"]);
                        this.ProductUnitPrice = BusinessUtility.GetDouble(dr["ProductUnitPrice"]);
                        this.ReturnDate = BusinessUtility.GetDateTime(dr["ReturnDate"]);
                        this.ReturnID = BusinessUtility.GetInt(dr["ReturnID"]);
                        this.ReturnInvoiceID = BusinessUtility.GetInt(dr["ReturnInvoiceID"]);
                        this.ReturnInvoiceItemID = BusinessUtility.GetInt(dr["ReturnInvoiceItemID"]);
                        this.ReturnProductID = BusinessUtility.GetInt(dr["ReturnProductID"]);
                        this.ReturnProductQty = BusinessUtility.GetDouble(dr["ReturnProductQty"]);
                        this.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        this.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        this.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        this.Tax4 = BusinessUtility.GetDouble(dr["Tax4"]);
                        this.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        this.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        this.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        this.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        this.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                        this.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                        this.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        this.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        this.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        this.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        this.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        this.ReturnBy = BusinessUtility.GetInt(dr["ReturnBy"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetReturnHistorySql(ParameterCollection pCol, int invoiceID)
        {
            pCol.Clear();
            string sql = "SELECT ReturnID,ReturnProductID,prdUPCCode,prdName,ReturnProductQty, TextedAmount AS AdjustedAmount,ReturnDate,UserName FROM vw_return_history_inv WHERE InvoiceID=@Invoice";
            pCol.Add("@Invoice", invoiceID.ToString());
            return sql;
        }

        public double GetPreviousReturnedQty(DbHelper dbHelp, int invItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT SUM(ReturnProductQty) AS QtySum FROM z_invoice_return WHERE ReturnInvoiceItemID=@ReturnInvoiceItemID GROUP BY ReturnInvoiceItemID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReturnInvoiceItemID", invItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool HasReturn(DbHelper dbHelp, int invoiceID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT COUNT(*) FROM z_invoice_return WHERE ReturnInvoiceID=@InvoiceID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceID", invoiceID, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }


    public class OrderProcessReturn
    {
        public int ReturnID { get; set; }
        public DateTime ReturnDate { get; set; }
        public int ReturnOrderID { get; set; }
        public int OrderProcessItemID { get; set; }
        public string OrderItemProcessCode { get; set; }
        public double ReturnAmount { get; set; }        
        public int ReturnBy { get; set; }

        public void ReturnAndGetWriteOff(DbHelper dbHelp, int orderProcessItemID, double amountToReturn, int userID) //As we already know how much amount wee need to get to be writeoff so no ned to return write-off amount
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlReturn = "INSERT INTO z_order_return_process(ReturnDate,ReturnOrderID,OrderProcessItemID,OrderItemProcessCode,ReturnAmount,ReturnBy) ";
                sqlReturn += "SELECT @ReturnDate AS ReturnDate,ordID AS ReturnOrderID, orderItemProcID AS OrderProcessItemID,ordItemProcCode AS OrderItemProcessCode,@ReturnAmount,";
                sqlReturn += "@ReturnBy AS ReturnBy FROM orderitemprocess WHERE orderItemProcID=@OrderProcessItemID";
                
                //Do not calculate amount to return just pass value from interface
                /*string sqlAmount = "SELECT * FROM vw_order_process_item_amount WHERE OrderProcessItemID=@OrderProcessItemID";
                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;                
                
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }
                    }
                }*/


                dbHelp.ExecuteNonQuery(sqlReturn, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("ReturnDate", DateTime.Now, MyDbType.DateTime),                            
                            DbUtility.GetParameter("ReturnAmount", amountToReturn, MyDbType.Double),
                            DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int),
                            DbUtility.GetParameter("ReturnBy", userID, MyDbType.Int)
                        });
                int returnID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetPreviousReturnAmount(DbHelper dbHelp, int orderProcessItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT SUM(ReturnAmount) FROM z_order_return_process WHERE OrderProcessItemID=@OrderProcessItemID";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetProcessAmount(DbHelper dbHelp, int orderProcessItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT TextedAmount FROM vw_order_process_item_amount WHERE OrderProcessItemID=@OrderProcessItemID";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //private bool IsArrayContains(string[] arr, string valToCheck)
        //{
        //    foreach (var item in arr)
        //    {
        //        if (valToCheck.Equals(item))
        //        {
        //            return true;                    
        //        }
        //    }
        //    return false;
        //}
    }

    public class InvoiceProcessReturn
    {
        public int ReturnID { get; set; }
        public DateTime ReturnDate { get; set; }
        public int ReturnInvoiceID { get; set; }
        public int InvoiceProcessItemID { get; set; }
        public string InvoiceItemProcessCode { get; set; }
        public double ReturnAmount { get; set; }        
        public int ReturnBy { get; set; }

        public void ReturnAndGetWriteOff(DbHelper dbHelp, int invProcessItemID, double amountToReturn, int userID) //As we already know how much amount wee need to get to be writeoff so no ned to return write-off amount
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlReturn = "INSERT INTO z_invoice_return_process(ReturnDate,ReturnInvoiceID,InvoiceProcessItemID,InvoiceItemProcessCode,ReturnAmount,ReturnBy) ";
                sqlReturn += "SELECT @ReturnDate AS ReturnDate,invoices_invID AS ReturnInvoiceID, invItemProcID AS InvoiceProcessItemID,invItemProcCode AS InvoiceItemProcessCode,@ReturnAmount,";
                sqlReturn += "@ReturnBy AS ReturnBy FROM invitemprocess WHERE invItemProcID=@InvoiceProcessItemID";

                //Do not calculate amount to return just pass value from interface
                /*string sqlAmount = "SELECT * FROM vw_order_process_item_amount WHERE OrderProcessItemID=@OrderProcessItemID";
                double returnAmount = 0, taxCalculated1 = 0, taxCalculated2 = 0, taxCalculated3 = 0, taxCalculated4 = 0, taxCalculated5 = 0;                
                
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        double units = BusinessUtility.GetDouble(dr["Units"]);
                        if (units > 0 && qtyToReturn > 0)
                        {
                            returnAmount = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["Amount"]) / units) * qtyToReturn);
                            taxCalculated1 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated1"]) / units) * qtyToReturn);
                            taxCalculated2 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated2"]) / units) * qtyToReturn);
                            taxCalculated3 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated3"]) / units) * qtyToReturn);
                            taxCalculated4 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated4"]) / units) * qtyToReturn);
                            taxCalculated5 = CalculationHelper.GetAmount((BusinessUtility.GetDouble(dr["TaxCalculated5"]) / units) * qtyToReturn);
                            returnAmount = CalculationHelper.GetAmount(returnAmount + taxCalculated1 + taxCalculated2 + taxCalculated3 + taxCalculated4 + taxCalculated5);
                        }
                    }
                }*/


                dbHelp.ExecuteNonQuery(sqlReturn, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("ReturnDate", DateTime.Now, MyDbType.DateTime),                            
                            DbUtility.GetParameter("ReturnAmount", amountToReturn, MyDbType.Double),
                            DbUtility.GetParameter("InvoiceProcessItemID", invProcessItemID, MyDbType.Int),
                            DbUtility.GetParameter("ReturnBy", userID, MyDbType.Int)
                        });
                int returnID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetPreviousReturnAmount(DbHelper dbHelp, int invProcessItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT SUM(ReturnAmount) FROM z_invoice_return_process WHERE InvoiceProcessItemID=@InvoiceProcessItemID";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceProcessItemID", invProcessItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetProcessAmount(DbHelper dbHelp, int invProcessItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT TextedAmount FROM vw_invoice_process_item_amount WHERE InvoiceProcessItemID=@InvoiceProcessItemID";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("InvoiceProcessItemID", invProcessItemID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    } 
}
