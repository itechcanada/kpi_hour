﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class LoyalPartnerHistory
    {
        public int LoyalHistoryID { get; set; }
        public int LoyalPartnerID { get; set; }
        public int LoyalCategoryID { get; set; }
        public double Points { get; set; }
        public DateTime PointsAddedOn { get; set; }
        public int PointsAddedBy { get; set; }
        public bool PointAuto { get; set; }
        public int ProductID { get; set; }
        public int InvoiceNo { get; set; }
        public int OrderNo { get; set; }
        public int PosTransactionID { get; set; }
        public string Notes { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "INSERT INTO z_loyal_partner_history(LoyalPartnerID,LoyalCategoryID,Points,PointsAddedOn,PointsAddedBy,";
                sql += "PointAuto, ProductID,InvoiceNo,OrderNo,PosTransactionID,Notes) VALUES(@LoyalPartnerID,@LoyalCategoryID,@Points,@PointsAddedOn,@PointsAddedBy,";
                sql += "@PointAuto,@ProductID,@InvoiceNo,@OrderNo,@PosTransactionID,@Notes)";

                int? invid = this.InvoiceNo <= 0 ? null : (int?)this.InvoiceNo;
                int? ordid = this.OrderNo <= 0 ? null : (int?)this.OrderNo;
                int? posTransID = this.PosTransactionID <= 0 ? null : (int?)this.PosTransactionID;
                int? prdid = this.ProductID <= 0 ? null : (int?)this.ProductID;
               
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("LoyalPartnerID", this.LoyalPartnerID, MyDbType.Int),
                    DbUtility.GetParameter("LoyalCategoryID", this.LoyalCategoryID, MyDbType.Int),
                    DbUtility.GetParameter("Points", this.Points, MyDbType.Double),
                    DbUtility.GetParameter("PointsAddedOn", this.PointsAddedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("PointsAddedBy", this.PointsAddedBy, MyDbType.Int),
                    DbUtility.GetParameter("PointAuto", this.PointAuto, MyDbType.Boolean),
                    DbUtility.GetIntParameter("InvoiceNo", invid),
                    DbUtility.GetIntParameter("OrderNo", ordid),
                    DbUtility.GetIntParameter("PosTransactionID", posTransID),
                    DbUtility.GetParameter("Notes", this.Notes, MyDbType.Int),
                    DbUtility.GetIntParameter("ProductID", prdid)
                });

                this.LoyalHistoryID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetPartnerLoyaltyHistory(DbHelper dbHelp, int partnerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                dbHelp.OpenDatabaseConnection();
                string sql = "SELECT LoyalHistoryID,LoyalPartnerID,LoyalCategoryID,Points,PointsAddedOn,";
                sql += " funGetUserName(PointsAddedBy) AS PointsAddedBy, PointAuto,ProductID,InvoiceNo,";
                sql += " OrderNo,PosTransactionID,Notes";
                sql += "  FROM z_loyal_partner_history WHERE LoyalPartnerID=@LoyalPartnerID";

                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalPartnerID", partnerID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double GetPartnerLoyaltyPoints(DbHelper dbHelp, int partnerID)
        {
            bool mustclose = false;
            if (dbHelp == null)
            {
                mustclose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECt SUM(Points) FROM z_loyal_partner_history WHERE LoyalPartnerID=@LoyalPartnerID ";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalPartnerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustclose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
