﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Partners
    {
        public int PartnerID { get; set; }
        public string PartnerLongName { get; set; }
        public string PartnerPhone { get; set; }
        public string PartnerFax { get; set; }
        public string PartnerEmail { get; set; }
        public string PartnerWebsite { get; set; }
        public bool PartnerActive { get; set; }
        public bool PartnerLocked { get; set; }
        public DateTime PartnerCreatedOn { get; set; }
        public int PartnerCreatedBy { get; set; }
        public DateTime PartnerLastUpdatedOn { get; set; }
        public int PartnerLastUpdatedBy { get; set; }
        public int PartnerType { get; set; }
        public string PartnerAcronyme { get; set; }
        public string PartnerNote { get; set; }
        public bool PartnerValidated { get; set; }
        public int PartnerTaxCode { get; set; }
        public string PartnerCommissionCode { get; set; }
        public int PartnerDiscount { get; set; }
        public string PartnerCurrencyCode { get; set; }
        public string PartnerInvoiceNetTerms { get; set; }
        public bool PartnerShipBlankPref { get; set; }
        public string PartnerCourierCode { get; set; }
        public string PartnerLang { get; set; }
        public string PartnerStatus { get; set; }
        public string PartnerValue { get; set; }
        public string PartnerPhone2 { get; set; }
        public string PartnerDesc1 { get; set; }
        public string PartnerDesc2 { get; set; }
        public double PartnerCreditAvailable { get; set; }
        public string InActiveReason { get; set; }
        public string PartnerAdditionPhone { get; set; }
        public string PartnerInvoicePreference { get; set; }
        public int PartnerLoyalCategoryID { get; set; }
        public string PartnerPassword { get; set; }

        public string PartnerEmail2 { get; set; }
        public string PartnerGender { get; set; }
        public string FaceBook { get; set; }
        public string Tiwtter { get; set; }
        public string Length { get; set; }
        public string Shoulder { get; set; }
        public string Chest { get; set; }
        public string Bicep { get; set; }
        public string Alert { get; set; }
        public DateTime PartnerSince { get; set; }
        public string Warehousecode { get; set; }
        public string legacyOpenAR { get; set; }
        public string legacyTotalSales { get; set; }


        public CustomerExtended ExtendedProperties { get; set; }

        public Partners()
        {
            this.PartnerPassword = string.Empty;
            this.ExtendedProperties = new CustomerExtended();
            this.InActiveReason = string.Empty;
            this.PartnerAcronyme = string.Empty;
            this.PartnerActive = false;
            this.PartnerAdditionPhone = string.Empty;
            this.PartnerCommissionCode = string.Empty;
            this.PartnerCourierCode = string.Empty;
            this.PartnerCreatedBy = 0;
            this.PartnerCreatedOn = DateTime.Now;
            this.PartnerCreditAvailable = 0.0d;
            this.PartnerCurrencyCode = string.Empty;
            this.PartnerDesc1 = string.Empty;
            this.PartnerDesc2 = string.Empty;
            this.PartnerDiscount = 0;
            this.PartnerEmail = string.Empty;
            this.PartnerFax = string.Empty;
            this.PartnerID = 0;
            this.PartnerInvoiceNetTerms = string.Empty;
            this.PartnerInvoicePreference = string.Empty;
            this.PartnerLang = string.Empty;
            this.PartnerLastUpdatedBy = 0;
            this.PartnerLastUpdatedOn = DateTime.Now;
            this.PartnerLocked = false;
            this.PartnerLongName = string.Empty;
            this.PartnerNote = string.Empty;
            this.PartnerPhone = string.Empty;
            this.PartnerPhone2 = string.Empty;
            this.PartnerShipBlankPref = false;
            this.PartnerStatus = string.Empty;
            this.PartnerTaxCode = 0;
            this.PartnerType = 0;
            this.PartnerValidated = true;
            this.PartnerValue = string.Empty;
            this.PartnerWebsite = string.Empty;
            this.PartnerEmail2 = string.Empty;
            this.PartnerGender = string.Empty;
            this.FaceBook = string.Empty;
            this.Tiwtter = string.Empty;
            this.Length = string.Empty;
            this.Shoulder = string.Empty;
            this.Chest = string.Empty;
            this.Bicep = string.Empty;
            this.Alert = string.Empty;
            this.PartnerSince = DateTime.Now;
            this.Warehousecode = string.Empty;
            this.legacyOpenAR = string.Empty;
            this.legacyTotalSales = string.Empty;
        }

        public void Insert(DbHelper dbHelp, int userid, List<Addresses> lstAddress)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = @"INSERT INTO partners(PartnerLongName, PartnerPhone, PartnerAdditionPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive,PartnerCreatedOn, PartnerCreatedBy, 
                PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote,PartnerInvoicePreference,
                PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, 
                PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue,PartnerPhone2, PartnerDesc1, 
                PartnerDesc2,PartnerCreditAvailable,InActiveReason,PartnerPassword, PartnerEmail2, PartnerGender, FaceBook, Tiwtter, Length, Shoulder, Chest, Bicep, Alert, PartnerSince, CreatedWhs, legacyOpenAR, legacyTotalSales) VALUES(";
            sql += "@PartnerLongName, @PartnerPhone, @PartnerAdditionPhone, @PartnerFax, @PartnerEmail, @PartnerWebsite, @PartnerActive,@PartnerCreatedOn, ";
            sql += "@PartnerCreatedBy, @PartnerLastUpdatedOn, @PartnerLastUpdatedBy, @PartnerType, @PartnerAcronyme, @PartnerNote,@PartnerInvoicePreference,";
            sql += "@PartnerValidated, @PartnerTaxCode, @PartnerCommissionCode, @PartnerDiscount, @PartnerCurrencyCode, @PartnerInvoiceNetTerms, @PartnerShipBlankPref, ";
            sql += "@PartnerCourierCode, @PartnerLang, @PartnerStatus, @PartnerValue,@PartnerPhone2, @PartnerDesc1, @PartnerDesc2,@PartnerCreditAvailable,@InActiveReason,PASSWORD(@PartnerPassword), ";
            sql += " @PartnerEmail2, @PartnerGender, @FaceBook, @Tiwtter, @Length, @Shoulder, @Chest, @Bicep, @Alert, @PartnerSince, @CreatedWhs, @legacyOpenAR, @legacyTotalSales) ";
            try
            {
                int? partnerTextCode = this.PartnerTaxCode > 0 ? (int?)this.PartnerTaxCode : null;

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerLongName", this.PartnerLongName, MyDbType.String),
                    DbUtility.GetParameter("PartnerPhone", this.PartnerPhone, MyDbType.String),
                    DbUtility.GetParameter("PartnerAdditionPhone", this.PartnerAdditionPhone, MyDbType.String),
                    DbUtility.GetParameter("PartnerFax", this.PartnerFax, MyDbType.String),
                    DbUtility.GetParameter("PartnerEmail", this.PartnerEmail, MyDbType.String),
                    DbUtility.GetParameter("PartnerWebsite", this.PartnerWebsite, MyDbType.String),
                    DbUtility.GetParameter("PartnerActive", this.PartnerActive, MyDbType.Boolean),
                    DbUtility.GetParameter("PartnerCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("PartnerCreatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("PartnerLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("PartnerLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("PartnerType", this.PartnerType, MyDbType.Int),
                    DbUtility.GetParameter("PartnerAcronyme", this.PartnerAcronyme, MyDbType.String),
                    DbUtility.GetParameter("PartnerNote", this.PartnerNote, MyDbType.String),
                    DbUtility.GetParameter("PartnerInvoicePreference", this.PartnerInvoicePreference, MyDbType.String),
                    DbUtility.GetParameter("PartnerValidated", this.PartnerValidated, MyDbType.Boolean),
                    DbUtility.GetIntParameter("PartnerTaxCode", partnerTextCode),
                    DbUtility.GetParameter("PartnerCommissionCode", this.PartnerCommissionCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerDiscount", this.PartnerDiscount, MyDbType.Int),
                    DbUtility.GetParameter("PartnerCurrencyCode", this.PartnerCurrencyCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerInvoiceNetTerms", this.PartnerInvoiceNetTerms, MyDbType.String),
                    DbUtility.GetParameter("PartnerShipBlankPref", this.PartnerShipBlankPref, MyDbType.Boolean),
                    DbUtility.GetParameter("PartnerCourierCode", this.PartnerCourierCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerLang", this.PartnerLang, MyDbType.String),
                    DbUtility.GetParameter("PartnerStatus", this.PartnerStatus, MyDbType.String),
                    DbUtility.GetParameter("PartnerValue", this.PartnerValue, MyDbType.String),
                    DbUtility.GetParameter("PartnerPhone2", this.PartnerPhone2, MyDbType.String),
                    DbUtility.GetParameter("PartnerDesc1", this.PartnerDesc1, MyDbType.String),
                    DbUtility.GetParameter("PartnerDesc2", this.PartnerDesc2, MyDbType.String),
                    DbUtility.GetParameter("PartnerCreditAvailable", this.PartnerCreditAvailable, MyDbType.Double),
                    DbUtility.GetParameter("InActiveReason", this.InActiveReason, MyDbType.String),
                    DbUtility.GetParameter("PartnerPassword", this.PartnerPassword, MyDbType.String),
                    DbUtility.GetParameter("PartnerEmail2", this.PartnerEmail2, MyDbType.String),
                    DbUtility.GetParameter("PartnerGender", this.PartnerGender, MyDbType.String),
                    DbUtility.GetParameter("FaceBook", this.FaceBook, MyDbType.String),
                    DbUtility.GetParameter("Tiwtter", this.Tiwtter, MyDbType.String),
                    DbUtility.GetParameter("Length", this.Length, MyDbType.String),
                    DbUtility.GetParameter("Shoulder", this.Shoulder, MyDbType.String),
                    DbUtility.GetParameter("Chest", this.Chest, MyDbType.String),
                    DbUtility.GetParameter("Bicep", this.Bicep, MyDbType.String),
                    DbUtility.GetParameter("Alert", this.Alert, MyDbType.String),
                    DbUtility.GetParameter("PartnerSince", this.PartnerSince, MyDbType.DateTime),
                    DbUtility.GetParameter("CreatedWhs", this.Warehousecode, MyDbType.String),
                    DbUtility.GetParameter("legacyOpenAR", this.legacyOpenAR, MyDbType.String),
                    DbUtility.GetParameter("legacyTotalSales", this.legacyTotalSales, MyDbType.String),
                    
                });

                this.PartnerID = dbHelp.GetLastInsertID();

                if (this.PartnerID > 0)
                {
                    //Obsolated following table imlemented only for backward businesslogic competibility
                    //Partner Table now itself has a field PartnerType & solve the purpose
                    string sqlParterType = "INSERT INTO partnerseltype(PartnerSelPartID, PartnerSelTypeID) VALUES(@PartnerSelPartID, @PartnerSelTypeID)";
                    dbHelp.ExecuteNonQuery(sqlParterType, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerSelPartID", this.PartnerID, MyDbType.Int),
                        DbUtility.GetParameter("PartnerSelTypeID", this.PartnerType, MyDbType.Int)
                    });

                    this.ExtendedProperties.PartnerID = this.PartnerID;
                    this.ExtendedProperties.Save(dbHelp);

                    foreach (Addresses adr in lstAddress)
                    {
                        adr.AddressSourceID = this.PartnerID;
                        adr.Save(dbHelp);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry"))
                    throw new Exception(CustomExceptionCodes.DUPLICATE_KEY);
                else
                    throw ex;
            }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public void Update(DbHelper dbHelp, int userid, List<Addresses> lstAddress)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlUpate = "UPDATE partners ";
            sqlUpate += "SET PartnerLongName = @PartnerLongName , PartnerPhone = @PartnerPhone, PartnerFax = @PartnerFax, PartnerEmail = @PartnerEmail, ";
            sqlUpate += "PartnerWebsite = @PartnerWebsite, PartnerActive = @PartnerActive, PartnerLocked = @PartnerLocked, ";
            sqlUpate += "PartnerLastUpdatedOn = @PartnerLastUpdatedOn, PartnerLastUpdatedBy = @PartnerLastUpdatedBy, ";
            sqlUpate += "PartnerType = @PartnerType, PartnerAcronyme = @PartnerAcronyme, PartnerNote = @PartnerNote, PartnerValidated = @PartnerValidated,";
            sqlUpate += "PartnerTaxCode = @PartnerTaxCode, PartnerCommissionCode = @PartnerCommissionCode, PartnerDiscount = @PartnerDiscount, ";
            sqlUpate += "PartnerCurrencyCode = @PartnerCurrencyCode, PartnerInvoiceNetTerms = @PartnerInvoiceNetTerms, PartnerShipBlankPref = @PartnerShipBlankPref, ";
            sqlUpate += "PartnerCourierCode = @PartnerCourierCode, PartnerLang = @PartnerLang, PartnerStatus = @PartnerStatus, PartnerValue = @PartnerValue,";
            sqlUpate += "PartnerPhone2 = @PartnerPhone2, PartnerDesc1 = @PartnerDesc1, PartnerDesc2 = @PartnerDesc2, PartnerCreditAvailable = @PartnerCreditAvailable, ";
            sqlUpate += "InActiveReason = @InActiveReason, PartnerAdditionPhone = @PartnerAdditionPhone, PartnerInvoicePreference = @PartnerInvoicePreference, ";
            sqlUpate += " PartnerEmail2 = @PartnerEmail2, PartnerGender = @PartnerGender, FaceBook = @FaceBook, Tiwtter = @Tiwtter, Length = @Length, Shoulder = @Shoulder, ";
            sqlUpate += " Chest = @Chest, Bicep = @Bicep, Alert = @Alert, PartnerSince = @PartnerSince, CreatedWhs = @CreatedWhs, legacyOpenAR = @legacyOpenAR, legacyTotalSales = @legacyTotalSales ";
            sqlUpate += "WHERE PartnerID=@PartnerID";

            try
            {
                int? partnerTextCode = this.PartnerTaxCode > 0 ? (int?)this.PartnerTaxCode : null;

                dbHelp.ExecuteNonQuery(sqlUpate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerLongName", this.PartnerLongName, MyDbType.String),
                    DbUtility.GetParameter("PartnerPhone", this.PartnerPhone, MyDbType.String),
                    DbUtility.GetParameter("PartnerAdditionPhone", this.PartnerAdditionPhone, MyDbType.String),
                    DbUtility.GetParameter("PartnerFax", this.PartnerFax, MyDbType.String),
                    DbUtility.GetParameter("PartnerEmail", this.PartnerEmail, MyDbType.String),
                    DbUtility.GetParameter("PartnerWebsite", this.PartnerWebsite, MyDbType.String),
                    DbUtility.GetParameter("PartnerActive", this.PartnerActive, MyDbType.Boolean),                    
                    DbUtility.GetParameter("PartnerLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("PartnerLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("PartnerType", this.PartnerType, MyDbType.Int),
                    DbUtility.GetParameter("PartnerAcronyme", this.PartnerAcronyme, MyDbType.String),
                    DbUtility.GetParameter("PartnerNote", this.PartnerNote, MyDbType.String),
                    DbUtility.GetParameter("PartnerInvoicePreference", this.PartnerInvoicePreference, MyDbType.String),
                    DbUtility.GetParameter("PartnerValidated", this.PartnerValidated, MyDbType.Boolean),
                    DbUtility.GetIntParameter("PartnerTaxCode", partnerTextCode),
                    DbUtility.GetParameter("PartnerCommissionCode", this.PartnerCommissionCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerDiscount", this.PartnerDiscount, MyDbType.Int),
                    DbUtility.GetParameter("PartnerCurrencyCode", this.PartnerCurrencyCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerInvoiceNetTerms", this.PartnerInvoiceNetTerms, MyDbType.String),
                    DbUtility.GetParameter("PartnerShipBlankPref", this.PartnerShipBlankPref, MyDbType.Boolean),
                    DbUtility.GetParameter("PartnerCourierCode", this.PartnerCourierCode, MyDbType.String),
                    DbUtility.GetParameter("PartnerLang", this.PartnerLang, MyDbType.String),
                    DbUtility.GetParameter("PartnerStatus", this.PartnerStatus, MyDbType.String),
                    DbUtility.GetParameter("PartnerValue", this.PartnerValue, MyDbType.String),
                    DbUtility.GetParameter("PartnerPhone2", this.PartnerPhone2, MyDbType.String),
                    DbUtility.GetParameter("PartnerDesc1", this.PartnerDesc1, MyDbType.String),
                    DbUtility.GetParameter("PartnerDesc2", this.PartnerDesc2, MyDbType.String),
                    DbUtility.GetParameter("PartnerCreditAvailable", this.PartnerCreditAvailable, MyDbType.Double),
                    DbUtility.GetParameter("InActiveReason", this.InActiveReason, MyDbType.String),
                    DbUtility.GetParameter("PartnerLocked", this.PartnerLocked, MyDbType.Boolean),
                    DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    DbUtility.GetParameter("PartnerEmail2", this.PartnerEmail2, MyDbType.String),
                    DbUtility.GetParameter("PartnerGender", this.PartnerGender, MyDbType.String),
                    DbUtility.GetParameter("FaceBook", this.FaceBook, MyDbType.String),
                    DbUtility.GetParameter("Tiwtter", this.Tiwtter, MyDbType.String),
                    DbUtility.GetParameter("Length", this.Length, MyDbType.String),
                    DbUtility.GetParameter("Shoulder", this.Shoulder, MyDbType.String),
                    DbUtility.GetParameter("Chest", this.Chest, MyDbType.String),
                    DbUtility.GetParameter("Bicep", this.Bicep, MyDbType.String),
                    DbUtility.GetParameter("Alert", this.Alert, MyDbType.String),
                    DbUtility.GetParameter("PartnerSince", this.PartnerSince, MyDbType.DateTime),
                    DbUtility.GetParameter("CreatedWhs", this.Warehousecode, MyDbType.String),
                    DbUtility.GetParameter("legacyOpenAR", this.legacyOpenAR, MyDbType.String),
                    DbUtility.GetParameter("legacyTotalSales", this.legacyTotalSales, MyDbType.String),
                });

                string sqlpTypeUpdate = "UPDATE partnerseltype SET PartnerSelTypeID=@PartnerSelTypeID WHERE PartnerSelPartID=@PartnerSelPartID";
                dbHelp.ExecuteNonQuery(sqlpTypeUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerSelTypeID", this.PartnerType, MyDbType.Int),
                    DbUtility.GetParameter("PartnerSelPartID", this.PartnerID, MyDbType.Int)
                });

                this.ExtendedProperties.Save(dbHelp);

                foreach (Addresses adr in lstAddress)
                {
                    adr.AddressID = adr.AddressID;
                    adr.AddressSourceID = this.PartnerID;
                    adr.Save(dbHelp);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry"))
                {
                    throw new Exception(CustomExceptionCodes.DUPLICATE_KEY);
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AssignSalesRepresentatives(DbHelper dbHelp, int customerID, string custType, List<int> lstRep)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlDelete = "DELETE FROM salesrepcustomer WHERE CustomerID=@CustomerID";
            string sqlInsert = "INSERT INTO salesrepcustomer (UserID,CustomerID,CustomerType) VALUES(@UserID,@CustomerID,@CustomerType)";
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", customerID, MyDbType.Int)
                });
                foreach (var rep in lstRep)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("UserID", rep, MyDbType.Int),
                        DbUtility.GetParameter("CustomerID", customerID, MyDbType.Int),
                        DbUtility.GetParameter("CustomerType", custType, MyDbType.String)
                    });
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<int> GetCustomerSalesRep(int customerID)
        {
            List<int> lResult = new List<int>();
            string sql = "SELECT * FROM salesrepcustomer WHERE CustomerID=@CustomerID";
            DbHelper dbHelp = new DbHelper();
            IDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", customerID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr["UserID"]));
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void PopulateObject(int partnerid)
        {
            string strSQL = "SELECT PartnerID,  PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn,  ";
            strSQL += " PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, ";
            strSQL += " PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue, PartnerPhone2, PartnerDesc1, PartnerDesc2, PartnerCreditAvailable, InActiveReason, PartnerAdditionPhone,  ";
            strSQL += " PartnerInvoicePreference, PartnerLoyalCategoryID, PartnerEmail2, PartnerGender, FaceBook, Tiwtter, Length, Shoulder, Chest, Bicep, Alert, PartnerSince, CreatedWhs, legacyOpenAR, legacyTotalSales from partners  ";
            strSQL += " WHERE PartnerID=@PartnerID  ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("PartnerID", partnerid,typeof(int) )
                                 };
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = default(MySqlDataReader);
            try
            {

                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (dr.Read())
                {
                    this.PartnerID = BusinessUtility.GetInt(dr["PartnerID"]);
                    this.PartnerLongName = BusinessUtility.GetString(dr["PartnerLongName"]);
                    this.PartnerPhone = BusinessUtility.GetString(dr["PartnerPhone"]);
                    this.PartnerFax = BusinessUtility.GetString(dr["PartnerFax"]);
                    this.PartnerEmail = BusinessUtility.GetString(dr["PartnerEmail"]);
                    this.PartnerWebsite = BusinessUtility.GetString(dr["PartnerWebsite"]);
                    this.PartnerActive = BusinessUtility.GetBool(dr["PartnerActive"]);
                    this.PartnerLocked = BusinessUtility.GetBool(dr["PartnerLocked"]);
                    this.PartnerCreatedOn = BusinessUtility.GetDateTime(dr["PartnerCreatedOn"]);
                    this.PartnerCreatedBy = BusinessUtility.GetInt(dr["PartnerCreatedBy"]);
                    this.PartnerLastUpdatedOn = BusinessUtility.GetDateTime(dr["PartnerLastUpdatedOn"]);
                    this.PartnerLastUpdatedBy = BusinessUtility.GetInt(dr["PartnerLastUpdatedBy"]);
                    this.PartnerType = BusinessUtility.GetInt(dr["PartnerType"]);
                    this.PartnerAcronyme = BusinessUtility.GetString(dr["PartnerAcronyme"]);
                    this.PartnerNote = BusinessUtility.GetString(dr["PartnerNote"]);
                    this.PartnerValidated = BusinessUtility.GetBool(dr["PartnerValidated"]);
                    this.PartnerTaxCode = BusinessUtility.GetInt(dr["PartnerTaxCode"]);
                    this.PartnerCommissionCode = BusinessUtility.GetString(dr["PartnerCommissionCode"]);
                    this.PartnerDiscount = BusinessUtility.GetInt(dr["PartnerDiscount"]);
                    this.PartnerCurrencyCode = BusinessUtility.GetString(dr["PartnerCurrencyCode"]);
                    this.PartnerInvoiceNetTerms = BusinessUtility.GetString(dr["PartnerInvoiceNetTerms"]);
                    this.PartnerShipBlankPref = BusinessUtility.GetBool(dr["PartnerShipBlankPref"]);
                    this.PartnerCourierCode = BusinessUtility.GetString(dr["PartnerCourierCode"]);
                    this.PartnerLang = BusinessUtility.GetString(dr["PartnerLang"]);
                    this.PartnerStatus = BusinessUtility.GetString(dr["PartnerStatus"]);
                    this.PartnerValue = BusinessUtility.GetString(dr["PartnerValue"]);
                    this.PartnerPhone2 = BusinessUtility.GetString(dr["PartnerPhone2"]);
                    this.PartnerDesc1 = BusinessUtility.GetString(dr["PartnerDesc1"]);
                    this.PartnerDesc2 = BusinessUtility.GetString(dr["PartnerDesc2"]);
                    this.PartnerCreditAvailable = BusinessUtility.GetDouble(dr["PartnerCreditAvailable"]);
                    this.InActiveReason = BusinessUtility.GetString(dr["InActiveReason"]);
                    this.PartnerAdditionPhone = BusinessUtility.GetString(dr["PartnerAdditionPhone"]);
                    this.PartnerInvoicePreference = BusinessUtility.GetString(dr["PartnerInvoicePreference"]);
                    this.PartnerLoyalCategoryID = BusinessUtility.GetInt(dr["PartnerLoyalCategoryID"]);
                    this.PartnerEmail2 = BusinessUtility.GetString(dr["PartnerEmail2"]);
                    this.PartnerGender = BusinessUtility.GetString(dr["PartnerGender"]);
                    this.FaceBook = BusinessUtility.GetString(dr["FaceBook"]);
                    this.Tiwtter = BusinessUtility.GetString(dr["Tiwtter"]);
                    this.Length = BusinessUtility.GetString(dr["Length"]);
                    this.Shoulder = BusinessUtility.GetString(dr["Shoulder"]);
                    this.Chest = BusinessUtility.GetString(dr["Chest"]);
                    this.Bicep = BusinessUtility.GetString(dr["Bicep"]);
                    this.Alert = BusinessUtility.GetString(dr["Alert"]);
                    this.PartnerSince = BusinessUtility.GetDateTime(dr["PartnerSince"]);
                    this.Warehousecode = BusinessUtility.GetString(dr["CreatedWhs"]);
                    this.legacyOpenAR = BusinessUtility.GetString(dr["legacyOpenAR"]);
                    this.legacyTotalSales = BusinessUtility.GetString(dr["legacyTotalSales"]);

                }
                dr.Close();
                this.ExtendedProperties.PopulateObject(dbHelp, this.PartnerID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);

            }

        }

        public bool CheckEmailIDExists(DbHelper dbHelp, string emailID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail";
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", emailID, MyDbType.String)          });
                return BusinessUtility.GetInt(sclr) > 0;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public void PopulateObject(DbHelper dbHelp, int partnerid)
        {
            string strSQL = "SELECT PartnerID, PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, ";
            strSQL += " PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, ";
            strSQL += " PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue, PartnerPhone2, PartnerDesc1, PartnerDesc2, PartnerCreditAvailable, InActiveReason, ";
            strSQL += " PartnerAdditionPhone, PartnerInvoicePreference, PartnerLoyalCategoryID, PartnerEmail2, PartnerGender, FaceBook, Tiwtter, Length, Shoulder, Chest, Bicep, Alert, PartnerSince, ";
            strSQL += " CreatedWhs, legacyOpenAR, legacyTotalSales from partners WHERE PartnerID=@PartnerID  ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("PartnerID", partnerid,typeof(int) )
                                 };
            MySqlDataReader dr = null;
            try
            {

                dr = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (dr.Read())
                {
                    this.PartnerID = BusinessUtility.GetInt(dr["PartnerID"]);
                    this.PartnerLongName = BusinessUtility.GetString(dr["PartnerLongName"]);
                    this.PartnerPhone = BusinessUtility.GetString(dr["PartnerPhone"]);
                    this.PartnerFax = BusinessUtility.GetString(dr["PartnerFax"]);
                    this.PartnerEmail = BusinessUtility.GetString(dr["PartnerEmail"]);
                    this.PartnerWebsite = BusinessUtility.GetString(dr["PartnerWebsite"]);
                    this.PartnerActive = BusinessUtility.GetBool(dr["PartnerActive"]);
                    this.PartnerLocked = BusinessUtility.GetBool(dr["PartnerLocked"]);
                    this.PartnerCreatedOn = BusinessUtility.GetDateTime(dr["PartnerCreatedOn"]);
                    this.PartnerCreatedBy = BusinessUtility.GetInt(dr["PartnerCreatedBy"]);
                    this.PartnerLastUpdatedOn = BusinessUtility.GetDateTime(dr["PartnerLastUpdatedOn"]);
                    this.PartnerLastUpdatedBy = BusinessUtility.GetInt(dr["PartnerLastUpdatedBy"]);
                    this.PartnerType = BusinessUtility.GetInt(dr["PartnerType"]);
                    this.PartnerAcronyme = BusinessUtility.GetString(dr["PartnerAcronyme"]);
                    this.PartnerNote = BusinessUtility.GetString(dr["PartnerNote"]);
                    this.PartnerValidated = BusinessUtility.GetBool(dr["PartnerValidated"]);
                    this.PartnerTaxCode = BusinessUtility.GetInt(dr["PartnerTaxCode"]);
                    this.PartnerCommissionCode = BusinessUtility.GetString(dr["PartnerCommissionCode"]);
                    this.PartnerDiscount = BusinessUtility.GetInt(dr["PartnerDiscount"]);
                    this.PartnerCurrencyCode = BusinessUtility.GetString(dr["PartnerCurrencyCode"]);
                    this.PartnerInvoiceNetTerms = BusinessUtility.GetString(dr["PartnerInvoiceNetTerms"]);
                    this.PartnerShipBlankPref = BusinessUtility.GetBool(dr["PartnerShipBlankPref"]);
                    this.PartnerCourierCode = BusinessUtility.GetString(dr["PartnerCourierCode"]);
                    this.PartnerLang = BusinessUtility.GetString(dr["PartnerLang"]);
                    this.PartnerStatus = BusinessUtility.GetString(dr["PartnerStatus"]);
                    this.PartnerValue = BusinessUtility.GetString(dr["PartnerValue"]);
                    this.PartnerPhone2 = BusinessUtility.GetString(dr["PartnerPhone2"]);
                    this.PartnerDesc1 = BusinessUtility.GetString(dr["PartnerDesc1"]);
                    this.PartnerDesc2 = BusinessUtility.GetString(dr["PartnerDesc2"]);
                    this.PartnerCreditAvailable = BusinessUtility.GetDouble(dr["PartnerCreditAvailable"]);
                    this.InActiveReason = BusinessUtility.GetString(dr["InActiveReason"]);
                    this.PartnerAdditionPhone = BusinessUtility.GetString(dr["PartnerAdditionPhone"]);
                    this.PartnerInvoicePreference = BusinessUtility.GetString(dr["PartnerInvoicePreference"]);
                    this.PartnerLoyalCategoryID = BusinessUtility.GetInt(dr["PartnerLoyalCategoryID"]);
                    this.PartnerEmail2 = BusinessUtility.GetString(dr["PartnerEmail2"]);
                    this.PartnerGender = BusinessUtility.GetString(dr["PartnerGender"]);
                    this.FaceBook = BusinessUtility.GetString(dr["FaceBook"]);
                    this.Tiwtter = BusinessUtility.GetString(dr["Tiwtter"]);
                    this.Length = BusinessUtility.GetString(dr["Length"]);
                    this.Shoulder = BusinessUtility.GetString(dr["Shoulder"]);
                    this.Chest = BusinessUtility.GetString(dr["Chest"]);
                    this.Bicep = BusinessUtility.GetString(dr["Bicep"]);
                    this.Alert = BusinessUtility.GetString(dr["Alert"]);
                    this.PartnerSince = BusinessUtility.GetDateTime(dr["PartnerSince"]);
                    this.Warehousecode = BusinessUtility.GetString(dr["CreatedWhs"]);
                    this.legacyOpenAR = BusinessUtility.GetString(dr["legacyOpenAR"]);
                    this.legacyTotalSales = BusinessUtility.GetString(dr["legacyTotalSales"]);
                }
                dr.Close();
                this.ExtendedProperties.PopulateObject(dbHelp, this.PartnerID);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public void PopulateObject(DbHelper dbHelp, string emailID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail AND PartnerActive=1";
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", emailID, MyDbType.String)
                });
                if (BusinessUtility.GetInt(sclr) > 0)
                {
                    this.PopulateObject(dbHelp, BusinessUtility.GetInt(sclr));
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSearchSql(ParameterCollection pCol, string searchText, int userid, bool isSalesRestricted, int partnerType)
        {
            pCol.Clear();
            string strSQL = "SELECT PartnerID, PartnerAcronyme, CASE PartnerType When 2 Then CONCAT_WS(' ',PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',PartnerLongName,'(V)') ELSE PartnerLongName END  AS PartnerLongName ,(SELECT Count(*) FROM orders o where ordCustID =PartnerID) as TotalOrders, ";
            strSQL += " (SELECT Count(*) FROM invoices i where invCustID=PartnerID) as TotalInvoices, ";
            strSQL += " (SELECT Sum(ARAmtRcvd) FROM accountreceivable a inner join invoices on (invID = ARInvoiceNo) where invCustId =PartnerID ) as TotalAmtRcvd, ";
            strSQL += " (SELECT  Max(ARAmtRcvdDateTime) FROM accountreceivable a inner join invoices on (invID = ARInvoiceNo) where invCustId =PartnerID ) as  LastAmtRcvd ";
            strSQL += " ,( SELECT COUNT(*) FROM  z_order_return AS zrtn ";
            strSQL += " INNER JOIN Orders AS ord ON ord.OrdID = zrtn.ReturnOrderID WHERE ord.OrdCustID = partners.PartnerID ) +  ";

            strSQL += " (SELECT COUNT(*) FROM  z_invoice_return AS zrtn ";
            strSQL += " INNER JOIN Invoices AS inv ON inv.invID = zrtn.ReturnInvoiceID WHERE inv.InvCustID = partners.PartnerID) AS TotalReturn ";

            strSQL += " ,(SELECT invCreatedON FROM Invoices WHERE invCustID =partners.PartnerID ORDER BY invID Desc LIMIT 1) AS LstTransactionDate ";
            strSQL += " ,(SELECT invShpWhsCode FROM Invoices WHERE invCustID =partners.PartnerID ORDER BY invID Desc LIMIT 1) AS LstWhsCode ";
            strSQL += " ,(SELECT invRegCode FROM Invoices WHERE invCustID =partners.PartnerID ORDER BY invID Desc LIMIT 1) AS LstRegCode ";
            strSQL += " ,(SELECT invID FROM Invoices WHERE invCustID =partners.PartnerID ORDER BY invID Desc LIMIT 1) AS LstInvoice, ps.PartnerSelTypeID ";



            if (isSalesRestricted)
            {
                strSQL += " FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID Inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID=@UserID ";
                pCol.Add("@UserID", userid.ToString());
            }
            else
            {
                strSQL += " FROM partners LEFT Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID ";
            }
            strSQL += " where PartnerActive=1 ";

            if (BusinessUtility.GetInt(partnerType) > 0)
            {
                strSQL += " AND PartnerType = @pType ";
                pCol.Add("@pType", BusinessUtility.GetString(partnerType));
            }

            //strSQL += " where PartnerActive=1 AND ps.PartnerSelTypeID=@PartnerType";
            //pCol.Add("@PartnerType", Globals.GetPartnerTypeID(StatusCustomerTypes.DISTRIBUTER).ToString());
            if (!string.IsNullOrEmpty(searchText))
            {
                //strSQL += " and  (PartnerLongName like CONCAT('%', @searchData, '%')  or PartnerAcronyme like CONCAT('%', @searchData, '%'))";
                //pCol.Add("@searchData", searchText);
                strSQL += " and  (PartnerLongName like CONCAT('%', @searchData, '%')  or PartnerAcronyme like CONCAT('%', @searchData, '%') or PartnerEmail like CONCAT('%', @searchData, '%') or PartnerPhone like CONCAT('%', @searchData, '%'))";
                pCol.Add("@searchData", searchText);
            }

            if (searchText == "")
            {
                strSQL += " LIMIT 0 ";
            }

            return strSQL;
        }

        /// <summary>
        /// Search Partners
        /// </summary>
        /// <param name="pCol">Search Parameter Collection Object</param>
        /// <param name="searchField">Search Field</param>
        /// <param name="searchText">Search Text</param>
        /// <param name="userid">User ID</param>
        /// <param name="isSalesRestricted">Is Sales Restricted</param>
        /// <param name="partType">Partner Type</param>
        /// <param name="gustType">Guest Type</param>
        /// <param name="showActive">Show Active (if 1 = Show Active List, if 0 Show Inactives, if null Show All)</param>
        /// <param name="includeStaff">Need to include Staff</param>
        /// <returns></returns>
        public string GetSearchSql(ParameterCollection pCol, string searchField, string searchText, int userid, bool isSalesRestricted, PartnerTypeIDs partType, StatusGuestType gustType, string showActive, bool includeStaff)
        {
            pCol.Clear();
            string strSQL = "";
            if (!isSalesRestricted)
            {
                strSQL = "SELECT distinct p.PartnerID,PartnerAcronyme, CASE PartnerType When 2 Then CONCAT_WS(' ',PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',PartnerLongName,'(V)') ELSE PartnerLongName END  AS PartnerLongName , PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote, funGetCustomerCreditAvailable(p.PartnerID) AS AvailableCredit, funGetCustomerAmountDue(p.PartnerID) AS AmountDue FROM partners p LEFT Join partnerseltype as ps on ps.PartnerSelPartID=p.PartnerID ";
            }
            else
            {
                strSQL = "SELECT distinct p.PartnerID,PartnerAcronyme, CASE PartnerType When 2 Then CONCAT_WS(' ',PartnerLongName, '(D)') When 1 Then CONCAT_WS(' ',PartnerLongName,'(V)') ELSE PartnerLongName END  AS PartnerLongName , PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote, funGetCustomerCreditAvailable(p.PartnerID) AS AvailableCredit, funGetCustomerAmountDue(p.PartnerID) AS AmountDue FROM partners p LEFT Join partnerseltype as ps on ps.PartnerSelPartID=p.PartnerID inner join salesrepcustomer s on s.CustomerID=p.PartnerID  and s.UserID=@UserID";
                pCol.Add("@UserID", userid.ToString());
            }

            if (searchField == CustomerSearchFields.INVOICE_NO)
            {
                strSQL += " INNER JOIN invoices i on  i.invCustID = p.PartnerID ";
            }

            if (!includeStaff)
            {
                if (gustType != StatusGuestType.None && gustType != StatusGuestType.Staff)
                {
                    strSQL += string.Format(" LEFT JOIN z_customer_extended z on  p.PartnerID = z.PartnerID AND (z.GuestType IN ({0},{1},{2}))", (int)StatusGuestType.Guest, (int)StatusGuestType.SpecialGuest, (int)StatusGuestType.CourseParticipants);
                    //pCol.Add("@GuestType1", ((int)StatusGuestType.Guest).ToString());
                    //pCol.Add("@GuestType2", ((int)StatusGuestType.SpecialGuest).ToString());
                }
                else
                {
                    strSQL += " LEFT JOIN z_customer_extended z on  p.PartnerID = z.PartnerID AND z.GuestType=@GuestType";
                    pCol.Add("@GuestType", ((int)gustType).ToString());
                }
            }
            else
            {
                strSQL += string.Format(" LEFT JOIN z_customer_extended z on  p.PartnerID = z.PartnerID AND (z.GuestType IN ({0},{1},{2},{3}))", (int)StatusGuestType.Guest, (int)StatusGuestType.SpecialGuest, (int)StatusGuestType.CourseParticipants, (int)StatusGuestType.Staff);
            }

            strSQL += " WHERE PartnerType=@PartnerType ";
            pCol.Add("@PartnerType", ((int)partType).ToString());

            if (showActive == "1")
            {
                strSQL += " AND PartnerActive=1";
            }
            if (showActive == "0")
            {
                strSQL += " AND PartnerActive=0";
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                if (searchField == CustomerSearchFields.CUSTOMER_NAME)
                {
                    strSQL += " AND  (PartnerLongName like CONCAT('%', @searchData, '%')  OR PartnerAcronyme like CONCAT('%', @searchData, '%') OR z.SpirtualName like CONCAT('%', @searchData, '%') OR p.PartnerID=@searchData )";
                }
                else if (searchField == CustomerSearchFields.PartnerPhone)
                {
                    strSQL += " AND PartnerPhone=@searchData";
                }
                else if (searchField == CustomerSearchFields.INVOICE_NO)
                {
                    strSQL += " AND invID=@searchData";
                }
                else if (searchField == CustomerSearchFields.EMAIL_ADDRESS)
                {
                    strSQL += " AND PartnerEmail LIKE CONCAT('%', @searchData, '%')";
                }
                else
                {
                    strSQL += " AND  (PartnerLongName like CONCAT('%', @searchData, '%')  OR PartnerAcronyme like CONCAT('%', @searchData, '%') OR PartnerEmail LIKE CONCAT('%', @searchData, '%'))";
                }

                pCol.Add("@searchData", searchText);
            }

            strSQL += "  ORDER BY PartnerLongName ";

            if (BusinessUtility.GetString( searchText )== "")
            {
                strSQL += "  LIMIT 0 ";
            }
            return strSQL;
        }

        //public string GetSearchGuestSql(ParameterCollection pCol, string searchText, int userid, bool isSalesRestricted, StatusGuestType gustType)
        //{
        //    pCol.Clear();
        //    string strSQL = "";
        //    if (!isSalesRestricted)
        //    {
        //        strSQL = "SELECT distinct partners.PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID ";
        //    }
        //    else
        //    {
        //        strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID=@UserID";
        //        pCol.Add("@UserID", userid.ToString());
        //    }


        //    if (gustType != StatusGuestType.None && gustType != StatusGuestType.Staff)
        //    {
        //        strSQL += string.Format(" INNER JOIN z_customer_extended z on  partners.PartnerID = z.PartnerID AND (z.GuestType IN ({0},{1},{2}))", (int)StatusGuestType.Guest, (int)StatusGuestType.SpecialGuest, (int)StatusGuestType.CourseParticipants);                
        //    }
        //    else
        //    {
        //        strSQL += " INNER JOIN z_customer_extended z on  partners.PartnerID = z.PartnerID AND z.GuestType=@GuestType";
        //        pCol.Add("@GuestType", ((int)gustType).ToString());
        //    }

        //    strSQL += " where 1=1 ";
        //    strSQL += " AND PartnerActive=1";


        //    if (!string.IsNullOrEmpty(searchText))
        //    {
        //        strSQL += " and  (PartnerLongName like CONCAT('%', @searchData, '%')  or PartnerEmail like CONCAT('%', @searchData, '%') or PartnerPhone like CONCAT('%', @searchData, '%'))";                       

        //        pCol.Add("@searchData", searchText);
        //    }

        //    strSQL += "  order by PartnerLongName ";
        //    return strSQL;
        //}

        public Addresses GetBillToAddress(DbHelper dbHelp, int pid, int partnerTypeID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            Addresses addr = new Addresses();
            try
            {
                addr.PopulateObject(dbHelp, pid, Globals.GetPartnerType(partnerTypeID), AddressType.BILL_TO_ADDRESS);
                return addr;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Addresses GetShipToAddress(DbHelper dbHelp, int pid, int partnerTypeID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            Addresses addr = new Addresses();
            try
            {
                addr.PopulateObject(dbHelp, pid, Globals.GetPartnerType(partnerTypeID), AddressType.SHIP_TO_ADDRESS);
                return addr;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Addresses GetHQAddress(DbHelper dbHelp, int pid, int partnerTypeID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            Addresses addr = new Addresses();
            try
            {
                addr.PopulateObject(dbHelp, pid, Globals.GetPartnerType(partnerTypeID), AddressType.HEAD_OFFICE);
                return addr;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Addresses GetBillToAddress(int pid, int partnerTypeID)
        {

            try
            {
                return this.GetBillToAddress(null, pid, partnerTypeID);
            }
            catch
            {
                throw;
            }
        }

        public Addresses GetShipToAddress(int pid, int partnerTypeID)
        {
            try
            {
                return this.GetShipToAddress(null, pid, partnerTypeID);
            }
            catch
            {
                throw;
            }
        }

        public Addresses GetHQAddress(int pid, int partnerTypeID)
        {
            try
            {
                return this.GetHQAddress(null, pid, partnerTypeID);
            }
            catch
            {
                throw;
            }
        }

        public int GetPartnerTypeID(DbHelper dbHelp, int pid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                object val = dbHelp.GetValue("SELECT PartnerSelTypeID FROM partnerseltype where PartnerSelPartID=@PartnerSelPartID", CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("PartnerSelPartID", pid, MyDbType.Int) });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool UpdatePartnerEmailID(DbHelper dbHelp, int pid, string sEmailID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                dbHelp.ExecuteNonQuery("Update Partners SET PartnerEmail = @PartnerEmail where PartnerID=@PartnerID", CommandType.Text,
                new MySqlParameter[] { 
                DbUtility.GetParameter("PartnerID", pid, MyDbType.Int), 
                DbUtility.GetParameter("PartnerEmail", sEmailID, MyDbType.String) });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int partnerID, bool allowDelete)
        {
            //string sqlDelete = "DELETE FROM partneractivitylog WHERE PartnerId=@PartnerID;";
            //sqlDelete += "DELETE FROM partnercontacts WHERE ContactPartnerID=@PartnerID;";
            //sqlDelete += "DELETE FROM partnereventlogs WHERE PartnerID=@PartnerID;";
            //sqlDelete += "DELETE FROM partnerselspecialization WHERE PartnerSelPartID=@PartnerID;";
            //sqlDelete += "DELETE FROM partnerseltype WHERE PartnerSelPartID=@PartnerID;" ;
            //sqlDelete += "DELETE FROM partners WHERE PartnerID=@PartnerID;";

            //string sqlPartnerInactive = "UPDATE partners SET PartnerActive=0 WHERE PartnerID=@PartnerID;";
            //string sqlPartnerInactive = "UPDATE z_reservation r, orders o, partners p";
            //sqlPartnerInactive += string.Format(" SET r.ReservationStatus={0},", (int)StatusReservation.Closed);
            //sqlPartnerInactive += " p.PartnerActive=0 WHERE r.SoID = o.ordID AND p.PartnerID=@PartnerID";

            List<string> sqlToExecute = new List<string>();
            //sqlToExecute.Add(string.Format("UPDATE z_reservation r, orders o SET r.ReservationStatus={0} WHERE r.SoID = o.ordID AND o.ordCustID=@PartnerID ", (int)StatusReservation.Closed));
            sqlToExecute.Add("UPDATE partners SET PartnerActive=0 WHERE PartnerID=@PartnerID");

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //dbHelp.ExecuteNonQuery(sqlPartnerInactive, CommandType.Text, new MySqlParameter[] { 
                //    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                //});
                foreach (var item in sqlToExecute)
                {
                    dbHelp.ExecuteNonQuery(item, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }



        public int GetRecentOrderID(DbHelper dbHelp, int partnerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT o.ordID FROM orders o INNER JOIN orderitems oi ON oi.ordID=o.ordID ";
            sql += " WHERE (o.ordCustID=@PartnerID OR oi.ordGuestID=@PartnerID) ORDER BY o.ordCreatedOn DESC LIMIT 1";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetMaxPartnerID(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT MAX(PartnerID) FROM partners";

            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, null);
                return BusinessUtility.GetInt(val) + 1;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double CustomerCreditAvailable(DbHelper dbHelp, int partnerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT funGetCustomerCreditAvailable(@PartnerID);";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public double CustomerDueAmount(DbHelper dbHelp, int partnerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT funGetCustomerAmountDue(@PartnerID);";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(v);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Sivananda Specific Functions

        /// <summary>
        /// Returns PartnerID
        /// </summary>
        /// <param name="emailID"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int ValidatePartner(string emailID, string password)
        {
            string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail AND PartnerPassword=PASSWORD(@PartnerPassword) AND PartnerActive=1";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", emailID, MyDbType.String),
                    DbUtility.GetParameter("PartnerPassword", password, MyDbType.String)
                });
                return BusinessUtility.GetInt(sclr);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        public void RegisterGuest(string password)
        {
            string sql = "SELECT COUNT(*) FROM partners WHERE PartnerEmail=@PartnerEmail AND PartnerActive=1";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", this.PartnerEmail, MyDbType.String)
                });
                if (BusinessUtility.GetInt(sclr) > 0)
                {
                    throw new Exception("PARTNER_ALREADY_EXISTS");
                }
                this.Insert(dbHelp, 0, new List<Addresses>());
                if (this.PartnerID > 0)
                {
                    string sqlSetPassword = "UPDATE partners SET PartnerPassword=PASSWORD(@PartnerPassword) WHERE PartnerID=@PartnerID";
                    dbHelp.ExecuteNonQuery(sqlSetPassword, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerPassword", password, MyDbType.String),
                        DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string RestPassword(string partnerEmailID)
        {
            string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail AND PartnerActive=1";
            string sqlUpdatePass = "UPDATE partners SET PartnerPassword=PASSWORD(@PartnerPassword) WHERE PartnerID=@PartnerID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object id = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", partnerEmailID, MyDbType.String)
                });
                if (BusinessUtility.GetInt(id) <= 0)
                {
                    throw new Exception(CustomExceptionCodes.INVALID_EMAIL_ID);
                }
                string randomPass = BusinessUtility.GenerateRandomString(6, true);
                dbHelp.ExecuteNonQuery(sqlUpdatePass, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerPassword", randomPass, MyDbType.String),
                    DbUtility.GetParameter("PartnerID", id, MyDbType.Int)
                });
                return randomPass;
            }
            catch { throw; }
            finally { dbHelp.CloseDatabaseConnection(); }
        }

        #endregion

        #region Caya Specific Functions

        public bool ResetPassowrd(string emailID, string RestPassword)
        {
            bool result = false;
            DbHelper dbHelp = new DbHelper(true);
            string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail";
            //string sql = "SELECT PartnerID FROM partners WHERE PartnerEmail=@PartnerEmail";
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", emailID, MyDbType.String)
                });
                if (BusinessUtility.GetInt(sclr) > 0)
                {
                    this.PopulateObject(dbHelp, BusinessUtility.GetInt(sclr));
                    if (this.PartnerID > 0)
                    {
                        string sqlSetPassword = "UPDATE partners SET PartnerPassword=PASSWORD(@PartnerPassword) WHERE PartnerID=@PartnerID";
                        object obj = dbHelp.ExecuteNonQuery(sqlSetPassword, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerPassword", RestPassword, MyDbType.String),
                        DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    });
                        return obj != null ? true : false;
                    }
                }
                else
                { throw new Exception("EMAILID-NOT-EXISTS"); }
                return result;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ChangePassowrd(string OldPassword, string newPassword)
        {
            bool result = false;
            DbHelper dbHelp = new DbHelper(true);
            string sql = "SELECT PartnerID FROM partners WHERE PartnerID=@PartnerID AND PartnerPassword=PASSWORD(@PartnerPassword)";
            try
            {
                object sclr = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    DbUtility.GetParameter("PartnerPassword", OldPassword, MyDbType.String)
                });
                if (BusinessUtility.GetInt(sclr) > 0)
                {
                    if (this.PartnerID > 0)
                    {
                        string sqlSetPassword = "UPDATE partners SET PartnerPassword=PASSWORD(@PartnerPassword) WHERE PartnerID=@PartnerID";
                        object obj = dbHelp.ExecuteNonQuery(sqlSetPassword, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerPassword", newPassword, MyDbType.String),
                        DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                    });
                        return obj != null ? true : false;
                    }
                }
                else
                { throw new Exception("PASSWORD-NOT-MATCH"); }
                return result;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion

        #region Post Xml Related Functions
        public void PopulateCustomer(DbHelper dbHelp, string email, string longName, string phone, string curCode, int userID, string lang, Addresses billToAddress, Addresses shipToAddress)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT PartnerID FROM partners WHERE PartnerLongName=@PartnerLongName AND PartnerEmail=@PartnerEmail AND PartnerPhone=@PartnerPhone";
                var v = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("PartnerLongName",  longName, MyDbType.String),
                    DbUtility.GetParameter("PartnerEmail", email, MyDbType.String),
                    DbUtility.GetParameter("PartnerPhone", phone, MyDbType.String)
                });
                int pId = BusinessUtility.GetInt(v);
                if (pId > 0)
                {
                    this.PopulateObject(dbHelp, pId);
                }
                else
                {
                    //Need to insert
                    this.PartnerActive = true;
                    this.PartnerAdditionPhone = string.Empty;
                    this.PartnerCommissionCode = string.Empty;
                    //_cust.PartnerAcronyme = txtCustActonym.Text;
                    this.PartnerCourierCode = string.Empty;
                    this.PartnerCreatedBy = userID;
                    this.PartnerCreditAvailable = 0D;
                    this.PartnerCurrencyCode = curCode;
                    this.PartnerDesc1 = string.Empty;
                    this.PartnerDesc2 = string.Empty;
                    this.PartnerDiscount = 0;
                    this.PartnerEmail = email;
                    this.PartnerFax = string.Empty;
                    this.PartnerInvoiceNetTerms = string.Empty;
                    this.PartnerInvoicePreference = string.Empty;
                    this.PartnerLang = lang;
                    this.PartnerLastUpdatedBy = userID;
                    this.PartnerLocked = false;
                    this.PartnerLongName = longName;
                    this.PartnerNote = string.Empty;
                    this.PartnerPhone = phone;
                    this.PartnerPhone2 = string.Empty;
                    this.PartnerShipBlankPref = false;
                    this.PartnerStatus = string.Empty;

                    //Retreive Ship to state tax
                    CountryStateTaxGroup tx = new CountryStateTaxGroup();
                    tx.PopulateObject(dbHelp, shipToAddress.AddressCountry, shipToAddress.AddressState);

                    this.PartnerTaxCode = tx.StateGroupTaxCode;
                    this.PartnerType = (int)PartnerTypeIDs.Distributer;
                    this.PartnerValidated = true;
                    this.PartnerValue = string.Empty;
                    this.PartnerWebsite = string.Empty;

                    this.ExtendedProperties.Age = 0;
                    this.ExtendedProperties.DOB = DateTime.MinValue;
                    this.ExtendedProperties.EmergencyAltTelephone = string.Empty;
                    this.ExtendedProperties.EmergencyContactName = string.Empty;
                    this.ExtendedProperties.EmergencyRelationship = string.Empty;
                    this.ExtendedProperties.EmergencyTelephone = string.Empty;
                    this.ExtendedProperties.FirstName = string.Empty;

                    this.ExtendedProperties.GuestType = (int)StatusGuestType.None;

                    this.ExtendedProperties.Illnesses = string.Empty;
                    this.ExtendedProperties.IsMember = string.Empty;
                    this.ExtendedProperties.IsPartOfStaffInPast = false;
                    this.ExtendedProperties.LastName = string.Empty;
                    this.ExtendedProperties.Medication = string.Empty;
                    this.ExtendedProperties.MembershipID = string.Empty;
                    this.ExtendedProperties.Nationality = string.Empty;

                    this.ExtendedProperties.ReasonToJoinStaff = string.Empty;
                    this.ExtendedProperties.ReceiveNewsLetters = false;
                    this.ExtendedProperties.Sex = string.Empty;
                    this.ExtendedProperties.SpirtualName = string.Empty;
                    this.ExtendedProperties.WorkExperience = string.Empty;
                    this.ExtendedProperties.EnableSocialPlugins = false;

                    this.Insert(dbHelp, userID, new List<Addresses> { billToAddress, shipToAddress });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Dropdown Helper Functions
        public void FillCustomers(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT PartnerID, PartnerLongName FROM partners WHERE PartnerActive=1 ORDER BY PartnerLongName";
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    lCtrl.DataSource = dr;
                    lCtrl.DataTextField = "PartnerLongName";
                    lCtrl.DataValueField = "PartnerID";
                    lCtrl.DataBind();
                }
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        public DateTime GetRecentPOSTransactionDate(DbHelper dbHelp, int partnerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT posTransDateTime from postransaction Where posTransUserID = @PartnerID order by posTransDateTime desc limit 1 ";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetDateTime(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Addresses GetShipToAddress(DbHelper dbHelp, int pid, string addressRef)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            Addresses addr = new Addresses();
            try
            {
                addr.PopulateObject(dbHelp, pid, addressRef, AddressType.SHIP_TO_ADDRESS);
                return addr;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
