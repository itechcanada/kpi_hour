﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Customer
    {
        public int ID { get; set; }
        public string CustomerName{ get; set; }
        public string Email{ get; set; }
        public string NetTerms{ get; set; }
        public int CustType{ get; set; }
        public string Currency{ get; set; }
        public string Fax{ get; set; }
        public string Phone{ get; set; }
        public int PartnerTaxCode{ get; set; }
        public string Acronyme{ get; set; }

        public void PopulateObject(int id)
        {
            string sql = " SELECT PartnerID as ID, PartnerLongName as CustomerName, PartnerEmail as Email, PartnerInvoiceNetTerms as NetTerms, ";
            sql += " PartnerSelTypeID as CustType,  PartnerCurrencyCode as Currency, PartnerFax as Fax,PartnerPhone as Phone,PartnerTaxCode, ";
            sql += " partnerAcronyme As Acronyme FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID";
            sql += "  where PartnerID=@PartnerID";
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID",   id, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.ID = BusinessUtility.GetInt(dr["ID"]);
                    this.Acronyme = BusinessUtility.GetString(dr["Acronyme"]);
                    this.Currency = BusinessUtility.GetString(dr["Currency"]);
                    this.CustomerName = BusinessUtility.GetString(dr["CustomerName"]);
                    this.CustType = BusinessUtility.GetInt(dr["CustType"]);
                    this.Email = BusinessUtility.GetString(dr["Email"]);
                    this.Fax = BusinessUtility.GetString(dr["Fax"]);
                    this.NetTerms = BusinessUtility.GetString(dr["NetTerms"]);
                    this.PartnerTaxCode = BusinessUtility.GetInt(dr["PartnerTaxCode"]);
                    this.Phone = BusinessUtility.GetString(dr["Phone"]);                    
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}
