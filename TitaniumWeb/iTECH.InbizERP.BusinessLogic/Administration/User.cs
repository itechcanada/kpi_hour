﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InbizUser
    {
        #region Property
        public int UserID { get; set; }
        public int UserRoleID { get; set; }
        public bool UserActive { get; set; }
        public bool UserValidated { get; set; }
        public DateTime UserLastUpdatedAt { get; set; }
        public string UserLoginId { get; set; }
        public string UserPassword { get; set; }
        public string UserSalutation { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string UserDesignation { get; set; }
        public string UserDepartment { get; set; }
        public string UserPhone { get; set; }
        public string UserPhoneExt { get; set; }
        public string UserFax { get; set; }
        public string UserCellPhone { get; set; }
        public string UserType { get; set; }
        public string UserModules { get; set; }
        public string UserStatus { get; set; }
        public string UserLang { get; set; }
        public string UserDefaultWhs { get; set; }
        public int UserGridHeight { get; set; }
        public bool ReceiveNotificationViaEmail { get; set; }
        #endregion

        /// <summary>
        /// to Get the filter query for Grid 
        /// </summary>
        /// <returns>as string </returns>
        public string GetSql(ParameterCollection pCol, string searchStatus, string searchText)
        {
            pCol.Clear();
            string sql = "SELECT UserId, userLoginId, userEmail, userModules, userType, concat(userFirstName,' ',userLastName) as userName, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as userActive, ReceiveNotificationViaEmail FROM users WHERE 1=1";
            if (!string.IsNullOrEmpty(searchStatus))
            {
                sql += " AND userStatus LIKE CONCAT('%', @StatusData, '%') ";
                pCol.Add("@StatusData", searchStatus);
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " And  (userEmail LIKE CONCAT('%', @SearchData, '%')  or ";
                sql += " concat(userFirstName,' ',userLastName)  LIKE CONCAT('%', @SearchData, '%') or ";
                sql += " userPhone LIKE CONCAT('%', @SearchData, '%') or ";
                sql += " userLoginId LIKE CONCAT('%', @SearchData, '%') )";
                pCol.Add("@SearchData", searchText);
            }
            sql += " order by userLoginId ";
            return sql;
        }


        public void Insert()
        {
            DbHelper dbHelper = new DbHelper(true);
            // Check Login id Exists or not
            string sqlLogin = "Select Count(userLoginId) From users WHERE userLoginId = @userLoginId";

            //SQL to Insert              
            string sql = "INSERT INTO users(userLoginId, userPassword, userSalutation, userFirstName, userLastName, userEmail, userDesignation, userDepartment, userPhone, userPhoneExt, userFax, userCellPhone, userRoleID, userType, userStatus, userLastUpdatedAt, userActive, userValidated, userLang, userDefaultWhs, ReceiveNotificationViaEmail, UserGridHeight) VALUES( "; //userModules, 
            sql += "@userLoginId, PASSWORD(@userPassword), @userSalutation, @userFirstName, @userLastName, @userEmail, @userDesignation, @userDepartment, @userPhone, @userPhoneExt, @userFax, @userCellPhone, @userRoleID, @userType,  @userStatus, @userLastUpdatedAt, @userActive, @userValidated, @userLang, @userDefaultWhs,@ReceiveNotificationViaEmail, @UserGridHeight ) ";//@userModules,
            MySqlParameter[] p = { 
                                     new MySqlParameter("@userLoginId", this.UserLoginId),
                                     new MySqlParameter("@userPassword", this.UserPassword),
                                     new MySqlParameter("@userSalutation", this.UserSalutation),
                                     new MySqlParameter("@userFirstName", this.UserFirstName),
                                     new MySqlParameter("@userLastName", this.UserLastName),
                                     new MySqlParameter("@userEmail", this.UserEmail),
                                     new MySqlParameter("@userDesignation", this.UserDesignation),
                                     new MySqlParameter("@userDepartment", this.UserDepartment),
                                     new MySqlParameter("@userPhone", this.UserPhone),
                                     new MySqlParameter("@userPhoneExt", this.UserPhoneExt),
                                     new MySqlParameter("@userFax", this.UserFax),
                                     new MySqlParameter("@userCellPhone", this.UserCellPhone),
                                     new MySqlParameter("@userRoleID", this.UserRoleID),
                                     new MySqlParameter("@userType", this.UserType),
                                     //new MySqlParameter("@userModules", this.UserModules),
                                     new MySqlParameter("@userStatus", this.UserStatus),
                                     new MySqlParameter("@userLastUpdatedAt", this.UserLastUpdatedAt),
                                     new MySqlParameter("@userActive", this.UserActive),
                                     new MySqlParameter("@userValidated", this.UserValidated),
                                     new MySqlParameter("@userLang", this.UserLang),
                                     new MySqlParameter("@userDefaultWhs", this.UserDefaultWhs),
                                     DbUtility.GetParameter("ReceiveNotificationViaEmail", this.ReceiveNotificationViaEmail, MyDbType.Boolean),
                                     DbUtility.GetParameter("UserGridHeight", this.UserGridHeight, MyDbType.Int)
                                     };
            try
            {
                object userCount = dbHelper.GetValue(sqlLogin, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("userLoginId", this.UserLoginId, MyDbType.String) });
                if (BusinessUtility.GetInt(userCount) > 0)
                {
                    throw new Exception("USER_ALREADY_EXISTS");
                }

                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                this.UserID = dbHelper.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public void ChangePassword(int userid, string password)
        {
            string sql = "UPDATE users SET userPassword=PASSWORD(@userPassword) WHERE userID=@userID";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("userPassword", password, MyDbType.String),
                                     DbUtility.GetParameter("userID", userid, MyDbType.Int)
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ChangePassword(int userid, string currentPassword, string newPassword)
        {
            string sqlCheck = "SELECT COUNT(*) FROM users WHERE userID=@userID AND userPassword=PASSWORD(@userPassword)";
            string sql = "UPDATE users SET userPassword=PASSWORD(@newPassword) WHERE userID=@userID";
            MySqlParameter[] pCheck = {
                                     DbUtility.GetParameter("userPassword", currentPassword, MyDbType.String),
                                     DbUtility.GetParameter("userID", userid, MyDbType.Int)
                                 };
            MySqlParameter[] pNew = {
                                     DbUtility.GetParameter("newPassword", newPassword, MyDbType.String),
                                     DbUtility.GetParameter("userID", userid, MyDbType.Int)
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                object scalar = dbHelp.GetValue(sqlCheck, CommandType.Text, pCheck);
                if (BusinessUtility.GetInt(scalar) != 1)
                {
                    return false;
                }
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, pNew);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        /// <summary>
        /// Update Users  Details Into users table
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            //SQL to Update              
            string sql = "UPDATE users SET  userSalutation=@userSalutation, userFirstName=@userFirstName, userLastName=@userLastName, userEmail=@userEmail, userDesignation=@userDesignation, userDepartment=@userDepartment, userPhone=@userPhone, userPhoneExt=@userPhoneExt, userFax=@userFax, userCellPhone=@userCellPhone, userRoleID=@userRoleID, userType=@userType, userStatus=@userStatus, userLastUpdatedAt=@userLastUpdatedAt, userActive=@userActive, userValidated=@userValidated, userLang=@userLang, userDefaultWhs=@userDefaultWhs ";//userModules=@userModules, 
            sql += " ,ReceiveNotificationViaEmail=@ReceiveNotificationViaEmail, UserGridHeight = @UserGridHeight ";
            sql += " WHERE userID = @userID";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@userID", this.UserID),                                 
                                     new MySqlParameter("@userSalutation", this.UserSalutation),
                                     new MySqlParameter("@userFirstName", this.UserFirstName),
                                     new MySqlParameter("@userLastName", this.UserLastName),
                                     new MySqlParameter("@userEmail", this.UserEmail),
                                     new MySqlParameter("@userDesignation", this.UserDesignation),
                                     new MySqlParameter("@userDepartment", this.UserDepartment),
                                     new MySqlParameter("@userPhone", this.UserPhone),
                                     new MySqlParameter("@userPhoneExt", this.UserPhoneExt),
                                     new MySqlParameter("@userFax", this.UserFax),
                                     new MySqlParameter("@userCellPhone", this.UserCellPhone),
                                     new MySqlParameter("@userRoleID", this.UserRoleID),
                                     new MySqlParameter("@userType", this.UserType),
                                     //new MySqlParameter("@userModules", this.UserModules),
                                     new MySqlParameter("@userStatus", this.UserStatus),
                                     new MySqlParameter("@userLastUpdatedAt", this.UserLastUpdatedAt),
                                     new MySqlParameter("@userActive", this.UserActive),
                                     new MySqlParameter("@userValidated", this.UserValidated),
                                     new MySqlParameter("@userLang", this.UserLang),
                                     new MySqlParameter("@userDefaultWhs", this.UserDefaultWhs),
                                     DbUtility.GetParameter("ReceiveNotificationViaEmail", this.ReceiveNotificationViaEmail, MyDbType.Boolean),
                                     DbUtility.GetParameter("UserGridHeight", this.UserGridHeight, MyDbType.Int)
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int userid)
        {

            DbHelper dbHelper = new DbHelper(true);
            try
            {
                this.PopulateObject(userid, dbHelper);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int userid, DbHelper dbHelper)
        {
            string strSQL = "SELECT * FROM users where userID= @userID";
            MySqlParameter[] p = { new MySqlParameter("@userID", userid) };

            MySqlDataReader objDr = null;

            try
            {
                objDr = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this.UserID = BusinessUtility.GetInt(objDr["userID"]);
                    this.UserLoginId = BusinessUtility.GetString(objDr["userLoginId"]);
                    this.UserSalutation = BusinessUtility.GetString(objDr["userSalutation"]);
                    this.UserFirstName = BusinessUtility.GetString(objDr["userFirstName"]);

                    this.UserLastName = BusinessUtility.GetString(objDr["userLastName"]);
                    this.UserEmail = BusinessUtility.GetString(objDr["userEmail"]);
                    this.UserDesignation = BusinessUtility.GetString(objDr["userDesignation"]);
                    this.UserDepartment = BusinessUtility.GetString(objDr["userDepartment"]);
                    this.UserPhone = BusinessUtility.GetString(objDr["userPhone"]);
                    this.UserRoleID = BusinessUtility.GetInt(objDr["userRoleID"]);
                    this.UserType = BusinessUtility.GetString(objDr["userType"]);
                    this.UserPhoneExt = BusinessUtility.GetString(objDr["userPhoneExt"]);
                    this.UserFax = BusinessUtility.GetString(objDr["userFax"]);
                    this.UserCellPhone = BusinessUtility.GetString(objDr["userCellPhone"]);
                    this.UserModules = BusinessUtility.GetString(objDr["userModules"]);
                    this.UserStatus = BusinessUtility.GetString(objDr["userStatus"]);
                    this.UserLastUpdatedAt = BusinessUtility.GetDateTime(objDr["userLastUpdatedAt"]);
                    this.UserActive = BusinessUtility.GetBool(objDr["userActive"]);
                    this.UserValidated = BusinessUtility.GetBool(objDr["userValidated"]);
                    this.UserLang = BusinessUtility.GetString(objDr["userLang"]);
                    this.UserDefaultWhs = BusinessUtility.GetString(objDr["userDefaultWhs"]);
                    this.ReceiveNotificationViaEmail = BusinessUtility.GetBool(objDr["ReceiveNotificationViaEmail"]);
                    this.UserGridHeight = BusinessUtility.GetInt(objDr["UserGridHeight"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
            }
        }

        public bool ResetPassword(DbHelper dbHelp, string loginId, string emailID, out string newPassword)
        {
            newPassword = string.Empty;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT userID FROM users WHERE userLoginId=@LoginId AND userEmail=@EmailID AND userActive=1";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoginID", loginId, MyDbType.String),
                    DbUtility.GetParameter("EmailID", emailID, MyDbType.String)
                });
                int id = BusinessUtility.GetInt(val);
                if (id > 0)
                {
                    newPassword = BusinessUtility.GenerateRandomString(6, true).ToLower();
                    string sqlUpdate = "UPDATE users SET userPassword=PASSWORD(@NewPassword) WHERE userID=@UserID";
                    if (dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("NewPassword", newPassword, MyDbType.String),
                        DbUtility.GetParameter("UserID", id, MyDbType.Int)
                    }) > 0)
                    {
                        return true;
                    }
                    else
                    {
                        newPassword = string.Empty;
                        return false;
                    }
                }
                return false;

            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int userid)
        {
            string sql = "DELETE FROM users WHERE UserId=@UserId";
            string sqlDelRoles = "DELETE FROM z_security_user_roles WHERE UserID=@UserID";


            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelRoles, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) });
                return dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) }) > 0;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        private void AddRoleToUser(DbHelper dbHelp, int roleid, int userid)
        {
            if (roleid > 0)
            {
                string sqlAssignNew = "INSERT INTO z_security_user_roles (UserID, RoleID) VALUES(@UserID, @RoleID)";
                string sqlCheckExists = "SELECT COUNT(*) FROM z_security_user_roles WHERE UserID=@UserID AND RoleID=@RoleID";

                object scalar = dbHelp.GetValue(sqlCheckExists, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int), DbUtility.GetParameter("RoleID", roleid, MyDbType.Int) });
                if (BusinessUtility.GetInt(scalar) <= 0)
                {
                    dbHelp.ExecuteNonQuery(sqlAssignNew, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int), DbUtility.GetParameter("RoleID", roleid, MyDbType.Int) });
                }
            }
        }


        public void AssignRoles(DbHelper dbHelp, List<int> roles, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                dbHelp = new DbHelper(true);
                mustClose = true;
            }
            try
            {
                string sqlDeleteAll = "DELETE FROM z_security_user_roles WHERE UserID=@UserID";
                string sqlOldKeys = "SELECT DISTINCT r.OldSecurityKey FROM z_security_roles r INNER JOIN z_security_user_roles ur ON ur.RoleID = r.RoleID";
                sqlOldKeys += " WHERE (ur.UserID = @UserID)";

                //Delete all past roles from user
                dbHelp.ExecuteNonQuery(sqlDeleteAll, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) });

                List<int> distinctRoles = new List<int>();
                foreach (var item in roles)
                {
                    if (distinctRoles.Contains(item)) continue;
                    distinctRoles.Add(item);
                }

                foreach (var item in distinctRoles)
                {
                    this.AddRoleToUser(dbHelp, item, userid);
                }

                //Update old modules with user so that old code work proper need to remove later
                List<string> lstOldRoles = new List<string>();
                string joinedOldRoles = string.Empty;
                string[] splitetedOldRoles;
                List<string> distinctOldRoles = new List<string>();
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlOldKeys, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        lstOldRoles.Add(BusinessUtility.GetString(dr[0]));
                    }
                    joinedOldRoles = string.Join(",", lstOldRoles.ToArray());
                    splitetedOldRoles = joinedOldRoles.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var dOldrole in splitetedOldRoles)
                    {
                        if (distinctOldRoles.Contains(dOldrole)) continue;
                        distinctOldRoles.Add(dOldrole);
                    }
                }

                string rolesToUpdate = string.Join(",", distinctOldRoles.ToArray());
                dbHelp.ExecuteNonQuery("UPDATE users SET userModules=@Modules WHERE UserID=@UserID", CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Modules", rolesToUpdate, MyDbType.String),
                    DbUtility.GetParameter("UserID", userid, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //Getusers by role
        public int[] GetUserIDsByRole(RoleID roleID)
        {
            string sql = "SELECT DISTINCT usr.userID FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            DbHelper dbHelp = new DbHelper();
            List<int> lResult = new List<int>();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("RoleID", (int)roleID, MyDbType.Int) });
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr[0]));
                }

                return lResult.ToArray();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetUserName(int userid)
        {
            string sql = "SELECT concat(userFirstName,' ',userLastName) as UserName FROM users where UserId=@UserId";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("UserId", userid, MyDbType.Int)
                });
                return BusinessUtility.GetString(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetUserName(DbHelper dbHelp, int userid)
        {
            bool mustClost = false;
            if (mustClost)
            {
                mustClost = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT CONCAT(userFirstName,' ',IFNULL(userLastName, '')) AS UserName FROM users WHERE UserId=@UserId";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("UserId", userid, MyDbType.Int)
                });
                return BusinessUtility.GetString(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClost) dbHelp.CloseDatabaseConnection();
            }
        }

        #region DropdownList Helper Functions
        public void FillSalesRepresentatives(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem, string sCurrentUserWhsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT usr.userID, concat(usr.userFirstName, ' ', IFNULL(usr.userLastName, '')) as Name FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            if (sCurrentUserWhsCode != "")
            {
                sql += " AND  usr.userDefaultWhs = '"+  sCurrentUserWhsCode +"'";
            }
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RoleID", (int)RoleID.SALES_REPRESENTATIVE, MyDbType.Int)
                });
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void FillSalesRepresentatives(ListControl lCtrl, string sCurrentUserWhsCode)
        {
            ListItem rootItem = null;
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT usr.userID, (usr.userFirstName) as Name FROM users usr ";
            sql += " INNER JOIN z_security_user_roles z ON z.UserID=usr.userID INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID";
            sql += " WHERE usr.userActive = 1 AND zr.RoleID=@RoleID";
            if (sCurrentUserWhsCode != "")
            {
                sql += " AND  usr.userDefaultWhs = '" + sCurrentUserWhsCode + "'";
            }
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RoleID", (int)RoleID.SALES_REPRESENTATIVE, MyDbType.Int)
                });
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void FillAllUsers(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT usr.userID, concat(usr.userFirstName, ' ', IFNULL(usr.userLastName, '')) as Name FROM users usr ";
            sql += " WHERE usr.userActive = 1";
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "Name";
                lCtrl.DataValueField = "userID";
                lCtrl.DataBind();

                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

    }
}