﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysRegister
    {
        #region Private Member
        private string _sysRegCode;
        private string _sysRegDesc;
        private string _sysRegAddress;
        private string _sysRegCity;
        private string _sysRegState;
        private string _sysRegPostalCode;
        private string _sysRegEmailID;
        private string _sysRegPhone;
        private string _sysRegFax;
        private string _sysRegWhsCode;
        private string _sysRegMerchantId;
        private string _sysRegHost;
        private string _sysRegHostPort;
        private string _sysRegTerminalID;
        private string _sysRegLogFilePath;
        private string _sysRegMessage;
        private bool _sysRegActive;
        private bool _sysRegPrintMerchantCopy;
        private int _sysRegTaxCode;
        private int _sysDefaultCustomerID;
        private string _sysMangerCode;
        #endregion
        #region Property
        public int SysRegTaxCode
        {
            get { return _sysRegTaxCode; }
            set { _sysRegTaxCode = value; }
        }
        public bool SysRegPrintMerchantCopy
        {
            get { return _sysRegPrintMerchantCopy; }
            set { _sysRegPrintMerchantCopy = value; }
        }
        public bool SysRegActive
        {
            get { return _sysRegActive; }
            set { _sysRegActive = value; }
        }
        public string SysRegMessage
        {
            get { return _sysRegMessage; }
            set { _sysRegMessage = value; }
        }
        public string SysRegLogFilePath
        {
            get { return _sysRegLogFilePath; }
            set { _sysRegLogFilePath = value; }
        }
        public string SysRegTerminalID
        {
            get { return _sysRegTerminalID; }
            set { _sysRegTerminalID = value; }
        }
        public string SysRegHostPort
        {
            get { return _sysRegHostPort; }
            set { _sysRegHostPort = value; }
        }
        public string SysRegHost
        {
            get { return _sysRegHost; }
            set { _sysRegHost = value; }
        }
        public string SysRegMerchantId
        {
            get { return _sysRegMerchantId; }
            set { _sysRegMerchantId = value; }
        }
        public string SysRegWhsCode
        {
            get { return _sysRegWhsCode; }
            set { _sysRegWhsCode = value; }
        }
        public string SysRegCode
        {
            get { return _sysRegCode; }
            set { _sysRegCode = value; }
        }
        public string SysRegDesc
        {
            get { return _sysRegDesc; }
            set { _sysRegDesc = value; }
        }
        public string SysRegAddress
        {
            get { return _sysRegAddress; }
            set { _sysRegAddress = value; }
        }
        public string SysRegCity
        {
            get { return _sysRegCity; }
            set { _sysRegCity = value; }
        }
        public string SysRegState
        {
            get { return _sysRegState; }
            set { _sysRegState = value; }
        }
        public string SysRegPostalCode
        {
            get { return _sysRegPostalCode; }
            set { _sysRegPostalCode = value; }
        }
        public string SysRegEmailID
        {
            get { return _sysRegEmailID; }
            set { _sysRegEmailID = value; }
        }
        public string SysRegPhone
        {
            get { return _sysRegPhone; }
            set { _sysRegPhone = value; }
        }
        public string SysRegFax
        {
            get { return _sysRegFax; }
            set { _sysRegFax = value; }
        }
        public int SysDefaultCustomerID
        {
            get { return _sysDefaultCustomerID; }
            set { _sysDefaultCustomerID = value; }
        }

        public string SysManagerCode
        {
            get { return _sysMangerCode; }
            set { _sysMangerCode = value; }
        }

        #endregion

        #region Functions

        public string GetSQL(ParameterCollection pCol, string SearchData)
        {
            pCol.Clear();
            string strSQL = "SELECT sysRegCode, sysRegDesc FROM sysregister Where 1=1 ";
            if (!string.IsNullOrEmpty(SearchData))
            {
                strSQL += " And  sysRegDesc LIKE CONCAT('%', @SearchData, '%')  ";
                pCol.Add("@SearchData", SearchData);
            }
            strSQL += "order by sysRegCode ";
            return strSQL;
        }

        public void PopulateObject(string regCode)
        {
            try
            {
                this.PopulateObject(null, regCode);
            }
            catch
            {

                throw;
            }
        }

        public void PopulateObject(DbHelper dbHelp, string regCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT sysRegCode, sysRegDesc, sysRegAddress, sysRegCity, sysRegState, sysRegPostalCode, sysRegEmailID, sysRegPhone, sysRegFax, sysRegActive, sysRegTaxCode, sysRegWhsCode, sysRegMerchantId, sysRegHost, sysRegHostPort, sysRegTerminalID, sysRegLogFilePath, sysRegMessage, sysRegPrintMerchantCopy,sysDefaultCustomerID, sysManagerCode FROM sysRegister where sysRegCode= @sysRegCode";
            MySqlParameter[] p = { 
                                   DbUtility.GetParameter("@sysRegCode", regCode,  typeof(string))
                                 };
            try
            {
                using (var drObj = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    if (drObj.Read())
                    {
                        _sysRegCode = BusinessUtility.GetString(drObj["sysRegCode"]);
                        _sysRegDesc = BusinessUtility.GetString(drObj["sysRegDesc"]);
                        _sysRegAddress = BusinessUtility.GetString(drObj["sysRegAddress"]);
                        _sysRegCity = BusinessUtility.GetString(drObj["sysRegCity"]);
                        _sysRegState = BusinessUtility.GetString(drObj["sysRegState"]);
                        _sysRegPostalCode = BusinessUtility.GetString(drObj["sysRegPostalCode"]);
                        _sysRegEmailID = BusinessUtility.GetString(drObj["sysRegEmailID"]);
                        _sysRegPhone = BusinessUtility.GetString(drObj["sysRegPhone"]);
                        _sysRegFax = BusinessUtility.GetString(drObj["sysRegFax"]);
                        _sysRegWhsCode = BusinessUtility.GetString(drObj["sysRegWhsCode"]);
                        _sysRegMerchantId = BusinessUtility.GetString(drObj["sysRegMerchantId"]);
                        _sysRegHost = BusinessUtility.GetString(drObj["sysRegHost"]);
                        _sysRegHostPort = BusinessUtility.GetString(drObj["sysRegHostPort"]);
                        _sysRegTerminalID = BusinessUtility.GetString(drObj["sysRegTerminalID"]);
                        _sysRegLogFilePath = BusinessUtility.GetString(drObj["sysRegLogFilePath"]);
                        _sysRegMessage = BusinessUtility.GetString(drObj["sysRegMessage"]);
                        _sysRegPrintMerchantCopy = BusinessUtility.GetBool(drObj["sysRegPrintMerchantCopy"]);
                        _sysRegActive = BusinessUtility.GetBool(drObj["sysRegActive"]);
                        _sysRegTaxCode = BusinessUtility.GetInt(drObj["sysRegTaxCode"]);
                        _sysDefaultCustomerID = BusinessUtility.GetInt(drObj["sysDefaultCustomerID"]);
                        _sysMangerCode = BusinessUtility.GetString(drObj["sysManagerCode"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Insert the Register  Information 
        /// </summary>
        /// <returns>if -1 then Register Code Exists in Data base  </returns>
        public OperationResult Insert()
        {
            DbHelper dbHelper = new DbHelper(true);
            // Check Register Code Exists or not
            string sqlRegisterCode = "SELECT count(*) FROM sysregister where sysRegCode= @sysRegCode";
            MySqlParameter[] pRegisterCode = { 
                                     DbUtility.GetParameter("sysRegCode", this._sysRegCode,typeof(string))
                                     };

            string sql = "INSERT INTO sysRegister(sysRegCode, sysRegDesc, sysRegAddress, sysRegCity, sysRegState, sysRegPostalCode, sysRegEmailID, sysRegPhone, sysRegFax, sysRegActive, sysRegTaxCode, sysRegWhsCode, sysRegMerchantId, sysRegHost,sysRegHostPort, sysRegTerminalID, sysRegLogFilePath, sysRegMessage, sysRegPrintMerchantCopy,sysDefaultCustomerID, sysManagerCode) VALUES( ";
            sql += "@sysRegCode, @sysRegDesc, @sysRegAddress, @sysRegCity, @sysRegState, @sysRegPostalCode, @sysRegEmailID, @sysRegPhone, @sysRegFax, @sysRegActive, @sysRegTaxCode, @sysRegWhsCode, @sysRegMerchantId,@sysRegHost, @sysRegHostPort, @sysRegTerminalID, @sysRegLogFilePath, @sysRegMessage, @sysRegPrintMerchantCopy, @sysDefaultCustomerID, @sysManagerCode)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("sysRegCode", this._sysRegCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegDesc", this._sysRegDesc,typeof(string)),
                                     DbUtility.GetParameter("sysRegAddress", this._sysRegAddress,typeof(string)),
                                     DbUtility.GetParameter("sysRegCity", this._sysRegCity,typeof(string)),
                                     DbUtility.GetParameter("sysRegState", this._sysRegState,typeof(string)),
                                     DbUtility.GetParameter("sysRegPostalCode", this._sysRegPostalCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegEmailID", this._sysRegEmailID,typeof(string)),
                                     DbUtility.GetParameter("sysRegPhone", this._sysRegPhone,typeof(string)),
                                     DbUtility.GetParameter("sysRegFax", this._sysRegFax,typeof(string)),
                                     DbUtility.GetParameter("sysRegActive", this._sysRegActive,typeof(bool)),
                                     DbUtility.GetParameter("sysRegTaxCode", this._sysRegTaxCode,typeof(int)),
                                     DbUtility.GetParameter("sysRegWhsCode", this._sysRegWhsCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegMerchantId", this._sysRegMerchantId,typeof(string)),
                                     DbUtility.GetParameter("sysRegHost", this._sysRegHost,typeof(string)),
                                     DbUtility.GetParameter("sysRegHostPort", this._sysRegHostPort,typeof(string)),
                                     DbUtility.GetParameter("sysRegTerminalID", this._sysRegTerminalID,typeof(string)),
                                     DbUtility.GetParameter("sysRegLogFilePath", this._sysRegLogFilePath,typeof(string)),
                                     DbUtility.GetParameter("sysRegMessage", this._sysRegMessage,typeof(string)),
                                     DbUtility.GetParameter("sysRegPrintMerchantCopy", this._sysRegPrintMerchantCopy,typeof(bool)),
                                     DbUtility.GetParameter("sysDefaultCustomerID", this._sysDefaultCustomerID,typeof(int)),
                                     DbUtility.GetParameter("sysManagerCode", this._sysMangerCode,typeof(string))
                                 };
            try
            {

                object objScalr1 = dbHelper.GetValue(sqlRegisterCode, System.Data.CommandType.Text, pRegisterCode);
                if (Convert.ToInt32(objScalr1) > 0)
                    return OperationResult.AlreadyExists;

                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return OperationResult.Success;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Update the Process Group Information 
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string sql = "UPDATE sysRegister set sysRegDesc=@sysRegDesc, sysRegAddress=@sysRegAddress, sysRegCity=@sysRegCity, sysRegState=@sysRegState, sysRegPostalCode=@sysRegPostalCode, sysRegEmailID=@sysRegEmailID, sysRegPhone=@sysRegPhone, sysRegFax=@sysRegFax, sysRegActive=@sysRegActive, sysRegTaxCode=@sysRegTaxCode, ";
            sql += " sysRegWhsCode=@sysRegWhsCode, sysRegMerchantId=@sysRegMerchantId, sysRegHost=@sysRegHost, sysRegHostPort=@sysRegHostPort, sysRegTerminalID=@sysRegTerminalID, sysRegLogFilePath=@sysRegLogFilePath, sysRegMessage=@sysRegMessage, sysRegPrintMerchantCopy=@sysRegPrintMerchantCopy, sysDefaultCustomerID=@sysDefaultCustomerID, sysManagerCode = @sysManagerCode ";
            sql += " Where sysRegCode=@sysRegCode";

            MySqlParameter[] p = { 
                                    DbUtility.GetParameter("sysRegCode", this._sysRegCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegDesc", this._sysRegDesc,typeof(string)),
                                     DbUtility.GetParameter("sysRegAddress", this._sysRegAddress,typeof(string)),
                                     DbUtility.GetParameter("sysRegCity", this._sysRegCity,typeof(string)),
                                     DbUtility.GetParameter("sysRegState", this._sysRegState,typeof(string)),
                                     DbUtility.GetParameter("sysRegPostalCode", this._sysRegPostalCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegEmailID", this._sysRegEmailID,typeof(string)),
                                     DbUtility.GetParameter("sysRegPhone", this._sysRegPhone,typeof(string)),
                                     DbUtility.GetParameter("sysRegFax", this._sysRegFax,typeof(string)),
                                     DbUtility.GetParameter("sysRegActive", this._sysRegActive,typeof(bool)),
                                     DbUtility.GetParameter("sysRegTaxCode", this._sysRegTaxCode,typeof(int)),
                                     DbUtility.GetParameter("sysRegWhsCode", this._sysRegWhsCode,typeof(string)),
                                     DbUtility.GetParameter("sysRegMerchantId", this._sysRegMerchantId,typeof(string)),
                                     DbUtility.GetParameter("sysRegHost", this._sysRegHost,typeof(string)),
                                     DbUtility.GetParameter("sysRegHostPort", this._sysRegHostPort,typeof(string)),
                                     DbUtility.GetParameter("sysRegTerminalID", this._sysRegTerminalID,typeof(string)),
                                     DbUtility.GetParameter("sysRegLogFilePath", this._sysRegLogFilePath,typeof(string)),
                                     DbUtility.GetParameter("sysRegMessage", this._sysRegMessage,typeof(string)),
                                     DbUtility.GetParameter("sysRegPrintMerchantCopy", this._sysRegPrintMerchantCopy,typeof(bool)),
                                     DbUtility.GetParameter("sysDefaultCustomerID", this._sysDefaultCustomerID,typeof(int)),
                                     DbUtility.GetParameter("sysManagerCode", this._sysMangerCode,typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public bool Delete(string regCode)
        {
            string sqlDelete = "DELETE FROM sysregister WHERE sysRegCode=@sysRegCode";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("sysRegCode", regCode, MyDbType.String) });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ValidateMangerCode(string regCode, string managerCode)
        {
            DbHelper dbHelper = new DbHelper(true);
            // Check Register Code And Manager Code Exists or not
            string sqlRegisterCode = "SELECT count(*) FROM sysregister where sysRegCode= @sysRegCode AND sysManagerCode = @sysManagerCode ";
            MySqlParameter[] pRegisterCode = { 
                                     DbUtility.GetParameter("sysRegCode", regCode,typeof(string)),
                                     DbUtility.GetParameter("sysManagerCode", managerCode,typeof(string)),
                                     };

            try
            {
                object objScalr1 = dbHelper.GetValue(sqlRegisterCode, System.Data.CommandType.Text, pRegisterCode);
                if (Convert.ToInt32(objScalr1) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        #endregion
    }
}