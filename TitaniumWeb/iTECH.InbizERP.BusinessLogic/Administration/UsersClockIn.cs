﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class UsersClockIn
    {
        public int idUsersClockIn { get; set; }
        public int UserID { get; set; }
        public string WhsCode { get; set; }
        public DateTime InDateTime { get; set; }
        public DateTime OutDateTime { get; set; }
        public int UserIDInOut { get; set; }


        public Boolean Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.idUsersClockIn > 0)
                {
                    string sqlUpdate = " UPDATE UsersClockIn SET OutDateTime = @OutDateTime, InDateTime = @InDateTime, UpdatedBy = @UpdatedBy, UpdatedOn = @UpdatedOn WHERE IdUsersClockIn = @IdUsersClockIn "; //InDateTime = @InDateTime,  UserID = @UserID, WhsCode = @WhsCode, 
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    //DbUtility.GetParameter("UserID", this.UserIDInOut, MyDbType.Int),
                    //DbUtility.GetParameter("WhsCode", this.WhsCode, MyDbType.String),
                    DbUtility.GetParameter("InDateTime", this.InDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("OutDateTime", this.OutDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("IdUsersClockIn", this.idUsersClockIn, MyDbType.Int),
                    DbUtility.GetParameter("UpdatedBy", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    });
                }
                else
                {
                    string sql = " INSERT INTO UsersClockIn (UserID, WhsCode, InDateTime, OutDateTime, CreatedBy, CreatedOn) VALUES (@UserID, @WhsCode, @InDateTime, @OutDateTime, @CreatedBy, @CreatedOn) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("UserID", this.UserIDInOut, MyDbType.Int),
                    DbUtility.GetParameter("WhsCode", this.WhsCode, MyDbType.String),
                    DbUtility.GetParameter("InDateTime", this.InDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("OutDateTime", this.OutDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("CreatedBy", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    });
                }
                this.idUsersClockIn = dbHelp.GetLastInsertID();
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Delete(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.idUsersClockIn > 0)
                {
                    string sqlUpdate = " UPDATE UsersClockIn SET  IsActive = @IsActive, UpdatedBy = @UpdatedBy, CreatedOn = @CreatedOn WHERE IdUsersClockIn = @IdUsersClockIn  ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("IsActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("IdUsersClockIn", this.idUsersClockIn, MyDbType.Int),
                    DbUtility.GetParameter("UpdatedBy", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetClockInList(DbHelper dbHelp, string sUserID, string sDate)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" SELECT uclkIn.idUsersClockIn, usr.UserLoginID AS UserID, CONCAT(usr.UserFirstName, ' ',usr.UserLastName ) AS UserName, whs.WarehouseCode AS Location, ");
                sbQuery.Append(" uclkIn.InDateTime AS 'In', uclkIn.OutDateTime AS 'Out', uclkIn.UserID as UID   FROM UsersClockIn AS uclkIn ");
                sbQuery.Append(" INNER JOIN Users usr ON usr.UserID = uclkIn.UserID ");
                sbQuery.Append(" INNER JOIN SysWarehouses whs ON whs.WarehouseCode = uclkIn.whsCode ");
                sbQuery.Append(" WHERE 1 = 1 ");

                if (this.idUsersClockIn > 0)
                {
                    sbQuery.Append(" AND uclkIn.idUsersClockIn= " + this.idUsersClockIn);
                }

                if (BusinessUtility.GetInt(sUserID) > 0)
                {
                    sbQuery.Append(" AND uclkIn.UserID= " + sUserID);
                }

                if (sDate!="")
                {
                    DateTime dt1 = BusinessUtility.GetDateTime(sDate, "MM/dd/yyyy");
                    sbQuery.Append(" AND uclkIn.InDateTime >= '" + dt1.ToString("yyyy/MM/dd") + "'");
                }

                sbQuery.Append(" ORDER BY uclkIn.InDateTime DESC ");
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetClockInSql(DbHelper dbHelp, string sUserID, string sDate)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" SELECT uclkIn.idUsersClockIn, usr.UserLoginID AS UserID, CONCAT(usr.UserFirstName, ' ',usr.UserLastName ) AS UserName, whs.WarehouseCode AS Location, ");
                sbQuery.Append(" uclkIn.InDateTime AS 'In', uclkIn.OutDateTime AS 'Out', uclkIn.UserID as UID   FROM UsersClockIn AS uclkIn ");
                sbQuery.Append(" INNER JOIN Users usr ON usr.UserID = uclkIn.UserID ");
                sbQuery.Append(" INNER JOIN SysWarehouses whs ON whs.WarehouseCode = uclkIn.whsCode ");
                sbQuery.Append(" WHERE 1 = 1 ");

                if (this.idUsersClockIn > 0)
                {
                    sbQuery.Append(" AND uclkIn.idUsersClockIn= " + this.idUsersClockIn);
                }

                if (BusinessUtility.GetInt(sUserID) > 0)
                {
                    sbQuery.Append(" AND uclkIn.UserID= " + sUserID);
                }

                if (sDate != "")
                {
                    DateTime dt1 = BusinessUtility.GetDateTime(sDate, "MM/dd/yyyy");
                    sbQuery.Append(" AND uclkIn.InDateTime >= '" + dt1.ToString("yyyy/MM/dd") + "'");
                }

                sbQuery.Append(" ORDER BY In DESC ");

                return  BusinessUtility.GetString(sbQuery);
                //DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery), CommandType.Text, null);
                //return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
