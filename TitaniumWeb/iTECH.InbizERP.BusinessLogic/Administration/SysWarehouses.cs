﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysWarehouses
    {       
        #region Property

        public string WarehouseCode { get; set; }
        public string WarehouseDescription { get; set; }
        public string WarehouseAddressLine1 { get; set; }
        public string WarehouseAddressLine2 { get; set; }
        public string WarehouseCity { get; set; }
        public string WarehouseState { get; set; }
        public string WarehouseCountry { get; set; }
        public string WarehousePostalCode { get; set; }
        public string WarehousePrinterID { get; set; }
        public string WarehouseEmailID { get; set; }
        public string WarehouseFax { get; set; }
        public string WarehousePhone { get; set; }
        public bool WarehouseActive { get; set; }
        public int WarehouseRegTaxCode { get; set; }
        public int WarehouseCompanyID { get; set; }
        public string ShippingWarehouse { get; set; } //Added on 2012-12-21
        public int AddPoints { get; set; }
        public double AddAmtForPoints { get; set; }
        public int RedeemPoints { get; set; }
        public double RedeemAmtForPoints { get; set; }
        public int MinPointsForRedemption { get; set; }
        public string ActiveWarehouse { get; set; }
        public string ReservedWarehouse { get; set; }
        public string DamagedWarehouse { get; set; }
        public bool ShowInProductQuery { get; set; }

        #endregion
        
        //public void PopulateWarehouse(DropDownList ddl, string userWhs, int companyID)
        //{
        //    string strSQL = null;
        //    List<MySqlParameter> p = new List<MySqlParameter>();
        //    strSQL = "SELECT WarehouseCode, WarehouseDescription, concat(upper(WarehouseCode),'::',substring(WarehouseDescription,1,15)) as WD FROM syswarehouses WHERE WarehouseActive='1'  ";
        //    //if (HostSetting.GetHostSetting(HostSettingKey.MultipleCompanies).ToLower() == "false")
        //    //{
        //    //    strSQL += " and WarehouseCompanyID ='" + HttpContext.Current.Session["DefaultCompanyID"].ToString() + "'";
        //    //}
        //    if (companyID > 0)
        //    {
        //        strSQL += " and WarehouseCompanyID =@WarehouseCompanyID";
        //        p.Add(DbUtility.GetParameter("WarehouseCompanyID", companyID, MyDbType.Int));
        //    }
        //    strSQL += " order by WarehouseDescription";
        //    DbHelper dbHelper = new DbHelper();
        //    try
        //    {
        //        ddl.Items.Clear();
        //        ddl.DataSource = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p.ToArray());
        //        ddl.DataTextField = "WD";
        //        ddl.DataValueField = "WarehouseCode";
        //        ddl.DataBind();
        //        ddl.Items.Insert(0, new ListItem("--Select--", "0"));
        //        if (!string.IsNullOrEmpty(userWhs)) {
        //            ddl.SelectedValue = userWhs;
        //        }
        //        //if (!string.IsNullOrEmpty(HttpContext.Current.Session["UserWarehouse"].ToString()))
        //        //{
        //        //    ddl.SelectedValue = HttpContext.Current.Session["UserWarehouse"].ToString();
        //        //}
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelper.CloseDatabaseConnection();
        //    }
        //}

        public string GetSQL(ParameterCollection pCol, string searchData)
        {
            pCol.Clear();
            string strSQL = "SELECT WarehouseCode, WarehouseDescription,WarehouseCompanyID,CompanyName,ShippingWarehouse  ";
            strSQL += "  FROM syswarehouses Inner join syscompanyinfo on syscompanyinfo.CompanyID= syswarehouses.WarehouseCompanyID";

            if (!string.IsNullOrEmpty(searchData))
            {
                strSQL += " And  WarehouseDescription LIKE CONCAT('%', @SearchData, '%')  ";
                pCol.Add("@SearchData", searchData);
            }
            strSQL += " order by WarehouseDescription ";
            return strSQL;
        }

        /// <summary>
        /// Get the Ware House Informaions 
        /// </summary>
        public void PopulateObject(string whsCode)
        {
            try
            {
                this.PopulateObject(whsCode, null);
            }
            catch
            {
                throw;
            }          
        }

        public void PopulateObject(string whsCode, DbHelper dbHelper)
        {
            bool mustClose = false;
            if (dbHelper == null)
            {
                mustClose = true;
                dbHelper = new DbHelper(true);
            }

            string strSQL = null;
            strSQL = "SELECT * FROM syswarehouses where WarehouseCode= @WarehouseCode";
            MySqlParameter[] p = { 
                                  DbUtility.GetParameter("@WarehouseCode", whsCode, typeof(string))
                                 };                       
            try
            {
                using (MySqlDataReader drObj = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    while (drObj.Read())
                    {
                        this.WarehouseActive = BusinessUtility.GetBool(drObj["WarehouseActive"]);
                        this.WarehouseAddressLine1 = BusinessUtility.GetString(drObj["WarehouseAddressLine1"]);
                        this.WarehouseAddressLine2 = BusinessUtility.GetString(drObj["WarehouseAddressLine2"]);
                        this.WarehouseCity = BusinessUtility.GetString(drObj["WarehouseCity"]);
                        this.WarehouseCompanyID = BusinessUtility.GetInt(drObj["WarehouseCompanyID"]);
                        this.WarehouseCountry = BusinessUtility.GetString(drObj["WarehouseCountry"]);
                        this.WarehouseDescription = BusinessUtility.GetString(drObj["WarehouseDescription"]);
                        this.WarehouseEmailID = BusinessUtility.GetString(drObj["WarehouseEmailID"]);
                        this.WarehouseFax = BusinessUtility.GetString(drObj["WarehouseFax"]);
                        this.WarehousePhone = BusinessUtility.GetString(drObj["WarehousePhone"]);
                        this.WarehousePostalCode = BusinessUtility.GetString(drObj["WarehousePostalCode"]);
                        this.WarehousePrinterID = BusinessUtility.GetString(drObj["WarehousePrinterID"]);
                        this.WarehouseRegTaxCode = BusinessUtility.GetInt(drObj["WarehouseRegTaxCode"]);
                        this.WarehouseState = BusinessUtility.GetString(drObj["WarehouseState"]);
                        this.ShippingWarehouse = BusinessUtility.GetString(drObj["ShippingWarehouse"]);
                        this.AddPoints = BusinessUtility.GetInt(drObj["AddPoints"]);
                        this.AddAmtForPoints = BusinessUtility.GetDouble(drObj["AddAmtForPoints"]);
                        this.RedeemPoints = BusinessUtility.GetInt(drObj["RedeemPoints"]);
                        this.RedeemAmtForPoints = BusinessUtility.GetDouble(drObj["RedeemAmtForPoints"]);
                        this.MinPointsForRedemption = BusinessUtility.GetInt(drObj["MinPointsForRedemption"]);
                        this.ActiveWarehouse = BusinessUtility.GetString(drObj["ActiveWarehouse"]);
                        this.ReservedWarehouse = BusinessUtility.GetString(drObj["ReservedWarehouse"]);
                        this.DamagedWarehouse = BusinessUtility.GetString(drObj["DamagedWarehouse"]);
                        this.ShowInProductQuery = BusinessUtility.GetBool(drObj["ShowInProductQuery"]);
                    }
                }               
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelper.CloseDatabaseConnection();
            }
        }
       
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            // Check Warehouse Code Exists or not
            string sqlWarehouseCode = "SELECT count(*) FROM syswarehouses where WarehouseCode=@WarehouseCode";
            
            // Check Ware house Discription Exists or not
            string sqlWarehouseDis = "SELECT count(*) FROM syswarehouses where WarehouseDescription=@WarehouseDescription";

            string sql = "INSERT INTO syswarehouses ( WarehouseCode, WarehouseDescription, WarehouseAddressLine1, WarehouseAddressLine2, WarehouseCity, WarehouseState, WarehouseCountry, WarehousePostalCode, WarehousePrinterID, WarehouseEmailID, WarehouseFax, WarehousePhone, WarehouseActive, WarehouseRegTaxCode,WarehouseCompanyID,ShippingWarehouse, AddPoints, AddAmtForPoints, RedeemPoints, RedeemAmtForPoints, MinPointsForRedemption,ActiveWarehouse,ReservedWarehouse,DamagedWarehouse,ShowInProductQuery) VALUES( ";
            sql += "@WarehouseCode, @WarehouseDescription, @WarehouseAddressLine1, @WarehouseAddressLine2, @WarehouseCity, @WarehouseState, @WarehouseCountry, @WarehousePostalCode, @WarehousePrinterID, @WarehouseEmailID, @WarehouseFax, @WarehousePhone, @WarehouseActive, @WarehouseRegTaxCode,@WarehouseCompanyID,@ShippingWarehouse,@AddPoints,@AddAmtForPoints,@RedeemPoints,@RedeemAmtForPoints,@MinPointsForRedemption,@ActiveWarehouse,@ReservedWarehouse,@DamagedWarehouse,@ShowInProductQuery)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("WarehouseCode", this.WarehouseCode,typeof(string)),
                                     DbUtility.GetParameter("WarehouseDescription", this.WarehouseDescription,typeof(string)),
                                     DbUtility.GetParameter("WarehouseCity", this.WarehouseCity,typeof(string)),
                                     DbUtility.GetParameter("WarehouseAddressLine1", this.WarehouseAddressLine1,typeof(string)),
                                     DbUtility.GetParameter("WarehouseAddressLine2", this.WarehouseAddressLine2,typeof(string)),
                                     DbUtility.GetParameter("WarehouseState", this.WarehouseState,typeof(string)),
                                     DbUtility.GetParameter("WarehouseCountry", this.WarehouseCountry,typeof(string)),
                                     DbUtility.GetParameter("WarehousePostalCode", this.WarehousePostalCode,typeof(string)),
                                     DbUtility.GetParameter("WarehousePrinterID", this.WarehousePrinterID,typeof(string)),
                                     DbUtility.GetParameter("WarehouseEmailID", this.WarehouseEmailID,typeof(string)),
                                     DbUtility.GetParameter("WarehouseFax", this.WarehouseFax,typeof(string)),
                                     DbUtility.GetParameter("WarehousePhone", this.WarehousePhone,typeof(string)),
                                     DbUtility.GetParameter("WarehouseActive", this.WarehouseActive,typeof(bool)),
                                     DbUtility.GetParameter("WarehouseRegTaxCode", this.WarehouseRegTaxCode,typeof(Int32)),
                                     DbUtility.GetParameter("WarehouseCompanyID", this.WarehouseCompanyID,typeof(Int32)),
                                     DbUtility.GetParameter("ShippingWarehouse", this.ShippingWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("AddPoints", this.AddPoints, MyDbType.Int),
                                     DbUtility.GetParameter("AddAmtForPoints", this.AddAmtForPoints, MyDbType.Double),
                                     DbUtility.GetParameter("RedeemPoints", this.RedeemPoints, MyDbType.Int),
                                     DbUtility.GetParameter("RedeemAmtForPoints", this.RedeemAmtForPoints, MyDbType.Double),
                                     DbUtility.GetParameter("MinPointsForRedemption", this.MinPointsForRedemption, MyDbType.Int),
                                     DbUtility.GetParameter("ActiveWarehouse", this.ActiveWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("ReservedWarehouse", this.ReservedWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("DamagedWarehouse", this.DamagedWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("ShowInProductQuery", this.ShowInProductQuery,typeof(bool))
                                 };
            try
            {
                object objScalr1 = dbHelp.GetValue(sqlWarehouseCode, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("WarehouseCode", this.WarehouseCode, typeof(string)) });
                if (BusinessUtility.GetInt(objScalr1) > 0) {
                    throw new Exception(CustomExceptionCodes.WAREHOUSE_CODE_ALREADY_EXISTS);
                }


                object objScalr2 = dbHelp.GetValue(sqlWarehouseDis, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("WarehouseDescription", this.WarehouseDescription, typeof(string)) });
                if (BusinessUtility.GetInt(objScalr2) > 0)
                {
                    throw new Exception(CustomExceptionCodes.WAREHOUSE_DESCRIPTION_ALREADY_EXISTS);
                }

                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);                
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        /// <summary>
        /// Update the Process Group Information 
        /// </summary>
        /// <returns></returns>
        public bool Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE syswarehouses set WarehouseDescription=@WarehouseDescription, WarehouseAddressLine1=@WarehouseAddressLine1, WarehouseAddressLine2=@WarehouseAddressLine2, WarehouseCity=@WarehouseCity, WarehouseState=@WarehouseState, WarehouseCountry=@WarehouseCountry, WarehousePostalCode=@WarehousePostalCode, ";
            sql += " WarehousePrinterID=@WarehousePrinterID, WarehouseEmailID=@WarehouseEmailID, WarehouseFax=@WarehouseFax, WarehousePhone=@WarehousePhone, WarehouseActive=@WarehouseActive, WarehouseRegTaxCode=@WarehouseRegTaxCode, WarehouseCompanyID=@WarehouseCompanyID,ShippingWarehouse=@ShippingWarehouse, ";
            sql += " AddPoints=@AddPoints, AddAmtForPoints=@AddAmtForPoints, RedeemPoints=@RedeemPoints, RedeemAmtForPoints=@RedeemAmtForPoints, MinPointsForRedemption=@MinPointsForRedemption,";
            sql += " ActiveWarehouse=@ActiveWarehouse,ReservedWarehouse=@ReservedWarehouse,DamagedWarehouse=@DamagedWarehouse,ShowInProductQuery=@ShowInProductQuery Where WarehouseCode=@WarehouseCode";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("WarehouseCode", this.WarehouseCode,typeof(string)),
                                     DbUtility.GetParameter("WarehouseCity", this.WarehouseCity,typeof(string)),
                                     DbUtility.GetParameter("WarehouseDescription", this.WarehouseDescription,typeof(string)),
                                     DbUtility.GetParameter("WarehouseAddressLine1", this.WarehouseAddressLine1,typeof(string)),
                                     DbUtility.GetParameter("WarehouseAddressLine2", this.WarehouseAddressLine2,typeof(string)),
                                     DbUtility.GetParameter("WarehouseState", this.WarehouseState,typeof(string)),
                                     DbUtility.GetParameter("WarehouseCountry", this.WarehouseCountry,typeof(string)),
                                     DbUtility.GetParameter("WarehousePostalCode", this.WarehousePostalCode,typeof(string)),
                                     DbUtility.GetParameter("WarehousePrinterID", this.WarehousePrinterID,typeof(string)),
                                     DbUtility.GetParameter("WarehouseEmailID", this.WarehouseEmailID,typeof(string)),
                                     DbUtility.GetParameter("WarehouseFax", this.WarehouseFax,typeof(string)),
                                     DbUtility.GetParameter("WarehousePhone", this.WarehousePhone,typeof(string)),
                                     DbUtility.GetParameter("WarehouseActive", this.WarehouseActive,typeof(bool)),
                                     DbUtility.GetParameter("WarehouseRegTaxCode", this.WarehouseRegTaxCode,typeof(Int32)),
                                     DbUtility.GetParameter("WarehouseCompanyID", this.WarehouseCompanyID,typeof(Int32)),
                                     DbUtility.GetParameter("ShippingWarehouse", this.ShippingWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("AddPoints", this.AddPoints, MyDbType.Int),
                                     DbUtility.GetParameter("AddAmtForPoints", this.AddAmtForPoints, MyDbType.Double),
                                     DbUtility.GetParameter("RedeemPoints", this.RedeemPoints, MyDbType.Int),
                                     DbUtility.GetParameter("RedeemAmtForPoints", this.RedeemAmtForPoints, MyDbType.Double),
                                     DbUtility.GetParameter("MinPointsForRedemption", this.MinPointsForRedemption, MyDbType.Int),
                                     DbUtility.GetParameter("ActiveWarehouse", this.ActiveWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("ReservedWarehouse", this.ReservedWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("DamagedWarehouse", this.DamagedWarehouse, MyDbType.String),
                                     DbUtility.GetParameter("ShowInProductQuery", this.ShowInProductQuery,typeof(bool))
                                 };            
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get Company iD BY Warehouse Code
        /// </summary>
        /// <param name="WarehouseCode"></param>
        /// <returns></returns>
        public int GetCompanyIdByWhsCode(string WarehouseCode)
        {
            //SQL to Update              
            string sql = "SELECT WarehouseCompanyID FROM syswarehouses Where WarehouseCode=@WarehouseCode ";
            MySqlParameter[] p = {                                    
                                     DbUtility.GetParameter("WarehouseCode", WarehouseCode,typeof(string)),
                                  };
            DbHelper dbHelper = new DbHelper();
            try
            {
                object obj = dbHelper.GetValue(sql, System.Data.CommandType.Text, p);
                if (obj != null)
                {
                    return BusinessUtility.GetInt(obj);
                }
                return 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public bool Delete(string whsCode) {
            string sql = "DELETE FROM syswarehouses WHERE WarehouseCode=@WarehouseCode";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("WarehouseCode", whsCode, MyDbType.String) });
                return true;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        #region Dropdown Helper functions
        public void FillWharehouse(DbHelper dbHelp, ListControl lCtrl, string userWhs, int companyID, ListItem defaultRootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            List<MySqlParameter> p = new List<MySqlParameter>();
            string strSQL = "SELECT WarehouseCode, WarehouseDescription, CONCAT(UPPER(WarehouseCode),'::',SUBSTRING(WarehouseDescription,1,25)) as WD FROM syswarehouses WHERE WarehouseActive='1'  ";
            if (companyID > 0)
            {
                strSQL += " and WarehouseCompanyID =@WarehouseCompanyID";
                p.Add(DbUtility.GetParameter("WarehouseCompanyID", companyID, MyDbType.Int));
            }
            strSQL += " order by WarehouseDescription";            
            try
            {
                lCtrl.Items.Clear();
                lCtrl.DataSource = dbHelp.GetDataTable(strSQL, System.Data.CommandType.Text, p.ToArray());
                lCtrl.DataTextField = "WD";
                lCtrl.DataValueField = "WarehouseCode";
                lCtrl.DataBind();
                if (defaultRootItem != null)
                {
                    lCtrl.Items.Insert(0, defaultRootItem);
                }
                if (!string.IsNullOrEmpty(userWhs))
                {
                    lCtrl.SelectedValue = userWhs;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion
        //Added by mukesh 20130829
        public double GetAmtPerPoint(string whsCode)
        {
            double amtPerPoint = 0.0;
            DbHelper  dbHelper = new DbHelper(true);
            string strSQL = null;
            strSQL = "SELECT (AddAmtForPoints/AddPoints) AS AmtPerPoint, MinPointsForRedemption from syswarehouses where WarehouseCode= @WarehouseCode";
            MySqlParameter[] p = { 
                                  DbUtility.GetParameter("@WarehouseCode", whsCode, typeof(string))
                                 };
            try
            {
                using (MySqlDataReader drObj = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    while (drObj.Read())
                    {
                        amtPerPoint = BusinessUtility.GetDouble(drObj["AmtPerPoint"]);
                        this.MinPointsForRedemption = BusinessUtility.GetInt(drObj["MinPointsForRedemption"]);
                    }
                }
                return amtPerPoint;
            }
            catch
            {
                throw;
            }
            finally
            {
               dbHelper.CloseDatabaseConnection();
            }
        }

        public double GetLoyalAmtPerPoint(string whsCode)
        {
            double amtPerPoint = 0.0;
            DbHelper dbHelper = new DbHelper(true);
            string strSQL = null;
            strSQL = "SELECT (RedeemAmtForPoints/RedeemPoints) AS AmtPerPoint, MinPointsForRedemption from syswarehouses where WarehouseCode= @WarehouseCode";
            MySqlParameter[] p = { 
                                  DbUtility.GetParameter("@WarehouseCode", whsCode, typeof(string))
                                 };
            try
            {
                using (MySqlDataReader drObj = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    while (drObj.Read())
                    {
                        amtPerPoint = BusinessUtility.GetDouble(drObj["AmtPerPoint"]);
                        this.MinPointsForRedemption = BusinessUtility.GetInt(drObj["MinPointsForRedemption"]);
                    }
                }
                return amtPerPoint;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
        //Added end


        public DataTable GetCompanyWhs()
        {
            //SQL to Update              
            string sql = "SELECT WarehouseCode AS WarehouseCode FROM syswarehouses Where WareHouseActive =1 and ShowInProductQuery = 1";
            //MySqlParameter[] p = {                                    
            //                         DbUtility.GetParameter("WarehouseCode", WarehouseCode,typeof(string)),
            //                      };
            DbHelper dbHelper = new DbHelper();
            try
            {
                return dbHelper.GetDataTable(sql, System.Data.CommandType.Text, null);
                //object obj = dbHelper.GetValue(sql, System.Data.CommandType.Text, p);
                //if (obj != null)
                //{
                //    return BusinessUtility.GetInt(obj);
                //}
                //return 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
    }
}
