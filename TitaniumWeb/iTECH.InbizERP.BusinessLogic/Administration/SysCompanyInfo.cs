﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysCompanyInfo
    {
        #region Property
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddressLine1 { get; set; }
        public string CompanyAddressLine2 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyBillToAddr1 { get; set; }
        public string CompanyBillToAddr2 { get; set; }
        public string CompanyBillToCity { get; set; }
        public string CompanyBillToState { get; set; }
        public string CompanyBillToCountry { get; set; }
        public string CompanyBillToPostalCode { get; set; }
        public string CompanyShpToTerms { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyBasCur { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyInternetFax { get; set; }
        public string CompanyPOTerms { get; set; }
        public string CompanyPOName { get; set; }
        public string CompanyPOPhone { get; set; }
        public string CompanyPOEmail { get; set; }
        public string CompanyLogoPath { get; set; }
        public string CompanyCourierCode { get; set; }
        public string CompanyGenQuotationRemarks { get; set; }
        public string CompanyGenInvoiceRemarks { get; set; }
        public string CompanyPhoneno { get; set; }
        public bool CompanySalesRepRestricted { get; set; }
        public CompanyUnitType CompanyUnitType { get; set; }
        public bool RestrictedPhoneFormat { get; set; }
        #endregion
        
        /// <summary>
        /// Get the Filter Query for JQ Grid to fill 
        /// </summary>
        /// <param name="pCol">as ParameterCollection</param>
        /// <param name="searchText">as string</param>
        /// <returns></returns>
        public string GetFilterQueryBySearchCriteria(ParameterCollection pCol, string searchText)
        {
            pCol.Clear();
            string strSQL = "SELECT CompanyID,CompanyName, CompanyBasCur, CompanyEmail FROM syscompanyinfo WHERE 1=1 ";

            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL += " And CompanyName LIKE CONCAT('%', @SearchData, '%') or CompanyEmail LIKE CONCAT('%', @SearchData, '%') ";
                pCol.Add("@SearchData", searchText);
            }

            strSQL += " order by CompanyName ";
            return strSQL;
        }
        /// <summary>
        /// Get the Company Information By its id
        /// </summary>
        public void PopulateObject(int companyID)
        {
            DbHelper dbHelper = new DbHelper();
            bool mustClose = false;
            if (dbHelper == null)
            {
                mustClose = true;
                dbHelper = new DbHelper(true);
            }

            try
            {
                this.PopulateObject(companyID, dbHelper);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelper.CloseDatabaseConnection(); //dbHelper.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int companyID, DbHelper dbHelper)
        {
            string strSQL = null;
            strSQL = "SELECT  * FROM syscompanyinfo WHERE CompanyID= @CompanyID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    this.CompanyID = BusinessUtility.GetInt(dr["CompanyID"]);
                    this.CompanyName = BusinessUtility.GetString(dr["CompanyName"]);
                    this.CompanyAddressLine1 = BusinessUtility.GetString(dr["CompanyAddressLine1"]);
                    this.CompanyAddressLine2 = BusinessUtility.GetString(dr["CompanyAddressLine2"]);
                    this.CompanyCity = BusinessUtility.GetString(dr["CompanyCity"]);
                    this.CompanyState = BusinessUtility.GetString(dr["CompanyState"]);
                    this.CompanyCountry = BusinessUtility.GetString(dr["CompanyCountry"]);
                    this.CompanyPostalCode = BusinessUtility.GetString(dr["CompanyPostalCode"]);
                    this.CompanyBillToAddr1 = BusinessUtility.GetString(dr["CompanyBillToAddr1"]);
                    this.CompanyBillToAddr2 = BusinessUtility.GetString(dr["CompanyBillToAddr2"]);
                    this.CompanyBillToCity = BusinessUtility.GetString(dr["CompanyBillToCity"]);
                    this.CompanyBillToState = BusinessUtility.GetString(dr["CompanyBillToState"]);
                    this.CompanyBillToCountry = BusinessUtility.GetString(dr["CompanyBillToCountry"]);
                    this.CompanyBillToPostalCode = BusinessUtility.GetString(dr["CompanyBillToPostalCode"]);
                    this.CompanyShpToTerms = BusinessUtility.GetString(dr["CompanyShpToTerms"]);
                    this.CompanyFax = BusinessUtility.GetString(dr["CompanyFax"]);
                    this.CompanyBasCur = BusinessUtility.GetString(dr["CompanyBasCur"]);
                    this.CompanyEmail = BusinessUtility.GetString(dr["CompanyEmail"]);
                    this.CompanyInternetFax = BusinessUtility.GetString(dr["CompanyInternetFax"]);
                    this.CompanyPOTerms = BusinessUtility.GetString(dr["CompanyPOTerms"]);
                    this.CompanyPOName = BusinessUtility.GetString(dr["CompanyPOName"]);
                    this.CompanyPOPhone = BusinessUtility.GetString(dr["CompanyPOPhone"]);
                    this.CompanyPOEmail = BusinessUtility.GetString(dr["CompanyPOEmail"]);
                    this.CompanySalesRepRestricted = BusinessUtility.GetBool(dr["CompanySalesRepRestricted"]);
                    this.CompanyLogoPath = BusinessUtility.GetString(dr["CompanyLogoPath"]);
                    this.CompanyCourierCode = BusinessUtility.GetString(dr["CompanyCourierCode"]);
                    this.CompanyGenQuotationRemarks = BusinessUtility.GetString(dr["CompanyGenQuotationRemarks"]);
                    this.CompanyGenInvoiceRemarks = BusinessUtility.GetString(dr["CompanyGenInvoiceRemarks"]);
                    this.CompanyPhoneno = BusinessUtility.GetString(dr["CompanyPhoneno"]);
                    this.CompanyUnitType = (CompanyUnitType)BusinessUtility.GetInt(dr["CompanyUnitType"]);
                    this.RestrictedPhoneFormat = BusinessUtility.GetBool(dr["RestrictedPhoneFormat"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {                
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public bool Update()
        {
            //SQL to Update              
            string sql = "UPDATE syscompanyinfo set  CompanyName=@CompanyName, CompanyAddressLine1=@CompanyAddressLine1, CompanyAddressLine2=@CompanyAddressLine2, CompanyCity=@CompanyCity, CompanyState=@CompanyState, CompanyCountry=@CompanyCountry, CompanyPostalCode=@CompanyPostalCode, CompanyBillToAddr1=@CompanyBillToAddr1,";
            sql += " CompanyBillToAddr2=@CompanyBillToAddr2, CompanyBillToCity=@CompanyBillToCity, CompanyBillToState=@CompanyBillToState, CompanyBillToCountry=@CompanyBillToCountry, CompanyBillToPostalCode=@CompanyBillToPostalCode, CompanyShpToTerms=@CompanyShpToTerms, CompanyFax=@CompanyFax, CompanyBasCur=@CompanyBasCur, ";
            sql += " CompanyEmail=@CompanyEmail, CompanyInternetFax=@CompanyInternetFax, CompanyPOTerms=@CompanyPOTerms, CompanyPOName=@CompanyPOName, CompanyPOPhone=@CompanyPOPhone, CompanyPOEmail=@CompanyPOEmail, CompanySalesRepRestricted=@CompanySalesRepRestricted, CompanyLogoPath=@CompanyLogoPath,  CompanyCourierCode=@CompanyCourierCode, ";
            sql += " CompanyGenQuotationRemarks=@CompanyGenQuotationRemarks, CompanyGenInvoiceRemarks=@CompanyGenInvoiceRemarks, CompanyPhoneno=@CompanyPhoneno,CompanyUnitType=@CompanyUnitType,";
            sql += " RestrictedPhoneFormat=@RestrictedPhoneFormat";
            sql += "  WHERE CompanyID = @CompanyID";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("CompanyID", this.CompanyID,typeof(Int32)), 
                                     DbUtility.GetParameter("CompanyName", this.CompanyName,typeof(string)),
                                     DbUtility.GetParameter("CompanyCity", this.CompanyCity,typeof(string)),
                                     DbUtility.GetParameter("CompanyAddressLine1", this.CompanyAddressLine1,typeof(string)),
                                     DbUtility.GetParameter("CompanyAddressLine2", this.CompanyAddressLine2,typeof(string)),
                                     DbUtility.GetParameter("CompanyState", this.CompanyState,typeof(string)),
                                     DbUtility.GetParameter("CompanyCountry", this.CompanyCountry,typeof(string)),
                                     DbUtility.GetParameter("CompanyPostalCode", this.CompanyPostalCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToAddr1", this.CompanyBillToAddr1,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToAddr2", this.CompanyBillToAddr2,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToCity", this.CompanyBillToCity,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToState", this.CompanyBillToState,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToCountry", this.CompanyBillToCountry,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToPostalCode", this.CompanyBillToPostalCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyShpToTerms", this.CompanyShpToTerms,typeof(string)),
                                     DbUtility.GetParameter("CompanyFax", this.CompanyFax,typeof(string)),
                                     DbUtility.GetParameter("CompanyBasCur", this.CompanyBasCur,typeof(string)),
                                     DbUtility.GetParameter("CompanyEmail", this.CompanyEmail,typeof(string)),
                                     DbUtility.GetParameter("CompanyInternetFax", this.CompanyInternetFax,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOTerms", this.CompanyPOTerms,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOName", this.CompanyPOName,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOPhone", this.CompanyPOPhone,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOEmail", this.CompanyPOEmail,typeof(string)),
                                     DbUtility.GetParameter("CompanySalesRepRestricted", this.CompanySalesRepRestricted,typeof(bool)),
                                     DbUtility.GetParameter("CompanyLogoPath", this.CompanyLogoPath,typeof(string)),
                                     DbUtility.GetParameter("CompanyCourierCode", this.CompanyCourierCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyGenQuotationRemarks", this.CompanyGenQuotationRemarks,typeof(string)),
                                     DbUtility.GetParameter("CompanyGenInvoiceRemarks", this.CompanyGenInvoiceRemarks,typeof(string)),
                                     DbUtility.GetParameter("CompanyPhoneno", this.CompanyPhoneno,typeof(string)),
                                     DbUtility.GetParameter("CompanyUnitType", (int)this.CompanyUnitType, MyDbType.Int),
                                     DbUtility.GetParameter("RestrictedPhoneFormat", this.RestrictedPhoneFormat, MyDbType.Boolean)
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Insert the Company Information 
        /// </summary>
        /// <returns></returns>
        public bool Insert()
        {
            //SQL to Update              
            string sql = "Insert syscompanyinfo (CompanyName, CompanyAddressLine1, CompanyAddressLine2, CompanyCity, CompanyState, CompanyCountry, CompanyPostalCode, CompanyBillToAddr1, ";
            sql+=" CompanyBillToAddr2, CompanyBillToCity, CompanyBillToState, CompanyBillToCountry, CompanyBillToPostalCode, CompanyShpToTerms, CompanyFax, CompanyBasCur, CompanyEmail, ";
            sql += " CompanyInternetFax, CompanyPOTerms, CompanyPOName, CompanyPOPhone, CompanyPOEmail, CompanySalesRepRestricted, CompanyLogoPath, CompanyCourierCode, CompanyGenQuotationRemarks, CompanyGenInvoiceRemarks, CompanyPhoneno, CompanyUnitType, RestrictedPhoneFormat) Value ( ";
            sql+=" @CompanyName, @CompanyAddressLine1, @CompanyAddressLine2, @CompanyCity, @CompanyState, @CompanyCountry, @CompanyPostalCode, @CompanyBillToAddr1, ";
            sql+=" @CompanyBillToAddr2, @CompanyBillToCity, @CompanyBillToState, @CompanyBillToCountry, @CompanyBillToPostalCode, @CompanyShpToTerms, @CompanyFax, @CompanyBasCur, @CompanyEmail, ";
            sql += " @CompanyInternetFax, @CompanyPOTerms, @CompanyPOName, @CompanyPOPhone, @CompanyPOEmail, @CompanySalesRepRestricted, @CompanyLogoPath, @CompanyCourierCode, @CompanyGenQuotationRemarks, @CompanyGenInvoiceRemarks, @CompanyPhoneno,@CompanyUnitType,@RestrictedPhoneFormat)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("CompanyName", this.CompanyName,typeof(string)),
                                     DbUtility.GetParameter("CompanyAddressLine1", this.CompanyAddressLine1,typeof(string)),
                                     DbUtility.GetParameter("CompanyAddressLine2", this.CompanyAddressLine2,typeof(string)),
                                     DbUtility.GetParameter("CompanyState", this.CompanyState,typeof(string)),
                                     DbUtility.GetParameter("CompanyCity", this.CompanyCity,typeof(string)),
                                     DbUtility.GetParameter("CompanyCountry", this.CompanyCountry,typeof(string)),
                                     DbUtility.GetParameter("CompanyPostalCode", this.CompanyPostalCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToAddr1", this.CompanyBillToAddr1,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToAddr2", this.CompanyBillToAddr2,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToCity", this.CompanyBillToCity,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToState", this.CompanyBillToState,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToCountry", this.CompanyBillToCountry,typeof(string)),
                                     DbUtility.GetParameter("CompanyBillToPostalCode", this.CompanyBillToPostalCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyShpToTerms", this.CompanyShpToTerms,typeof(string)),
                                     DbUtility.GetParameter("CompanyFax", this.CompanyFax,typeof(string)),
                                     DbUtility.GetParameter("CompanyBasCur", this.CompanyBasCur,typeof(string)),
                                     DbUtility.GetParameter("CompanyEmail", this.CompanyEmail,typeof(string)),
                                     DbUtility.GetParameter("CompanyInternetFax", this.CompanyInternetFax,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOTerms", this.CompanyPOTerms,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOName", this.CompanyPOName,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOPhone", this.CompanyPOPhone,typeof(string)),
                                     DbUtility.GetParameter("CompanyPOEmail", this.CompanyPOEmail,typeof(string)),
                                     DbUtility.GetParameter("CompanySalesRepRestricted", this.CompanySalesRepRestricted,typeof(bool)),
                                     DbUtility.GetParameter("CompanyLogoPath", this.CompanyLogoPath,typeof(string)),
                                     DbUtility.GetParameter("CompanyCourierCode", this.CompanyCourierCode,typeof(string)),
                                     DbUtility.GetParameter("CompanyGenQuotationRemarks", this.CompanyGenQuotationRemarks,typeof(string)),
                                     DbUtility.GetParameter("CompanyGenInvoiceRemarks", this.CompanyGenInvoiceRemarks,typeof(string)),
                                     DbUtility.GetParameter("CompanyPhoneno", this.CompanyPhoneno,typeof(string)),
                                     DbUtility.GetParameter("CompanyUnitType", (int)this.CompanyUnitType, MyDbType.Int),
                                     DbUtility.GetParameter("RestrictedPhoneFormat", this.RestrictedPhoneFormat, MyDbType.Boolean)
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public string GetBaseCurrency() {
            string sql = "SELECT CompanyBasCur FROM syscompanyinfo";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return BusinessUtility.GetString(dbHelp.GetValue(sql, CommandType.Text, null));
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool UpdateBaseCurrency(string baseCurrency) {
            string sql = "UPDATE syscompanyinfo SET CompanyBasCur=@CompanyBasCur";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CompanyBasCur", baseCurrency, MyDbType.String) });
                return true;
            }
            catch
            {
                throw;
            }
            finally { 

            }
        }

        public bool UpdateCompanyLogo(int companyID, string imageName) {
            string sql = "UPDATE sysCompanyInfo SET CompanyLogoPath=@CompanyLogoPath where companyID=@companyID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("companyID", companyID, MyDbType.Int), DbUtility.GetParameter("CompanyLogoPath", imageName, MyDbType.String) });
                return true;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int companyID) {
            string sql = "DELETE FROM syscompanyinfo WHERE CompanyID=@CompanyID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("companyID", companyID, MyDbType.Int) });
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetCompanyFaxInfo(int companyID)
        {
            string sql = "SELECT CompanyInternetFax FROM syscompanyinfo where companyID=@companyID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                       DbUtility.GetParameter("companyID",  companyID, MyDbType.Int)
                });
                return BusinessUtility.GetString(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetFaxNumbers(string strInput)
        {
            string strOutput = null;
            strOutput = strInput.ToLower();
            strOutput = strOutput.Replace(" ", "");
            strOutput = strOutput.Replace(Microsoft.VisualBasic.Constants.vbCrLf, "");
            strOutput = strOutput.Replace(Microsoft.VisualBasic.Constants.vbLf, "");
            strOutput = strOutput.Replace(",", "");
            strOutput = strOutput.Replace("$", "");
            strOutput = strOutput.Replace("(", "");
            strOutput = strOutput.Replace(")", "");
            strOutput = strOutput.Replace("-", "");
            int intCount = 0;
            string strTemp = "";
            //Step through each character
            for (intCount = 1; intCount <= Microsoft.VisualBasic.Strings.Len(strOutput); intCount++)
            {
                //if a number
                if (Microsoft.VisualBasic.Information.IsNumeric(Microsoft.VisualBasic.Strings.Mid(strOutput, intCount, 1)))
                {
                    strTemp = strTemp + Microsoft.VisualBasic.Strings.Mid(strOutput, intCount, 1);
                    //add to temp
                }
            }
            strOutput = strTemp;
            return strOutput;
        }

        public int GetCompanyID()
        {
            string sql = "SELECT CompanyID FROM syscompanyinfo LIMIT 1";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, null);
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int WarehouseCompanyID(string whs)
        {
            string sql = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode=@whs";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                       DbUtility.GetParameter("whs",  whs, MyDbType.String)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public CompanyUnitType GetUnitType(DbHelper dbHelp, int comID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT CompanyUnitType FROM syscompanyinfo WHERE CompanyID=@CompanyID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CompanyID", comID, MyDbType.Int)
                });
                int id = BusinessUtility.GetInt(val);
                if (id > 0)
                {
                    return (BusinessLogic.CompanyUnitType)id;
                }
                else
                {
                    return BusinessLogic.CompanyUnitType.Metric;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void SetOrderStatusToAllowInvoiceGeneration(DbHelper dbHelp, int companyID, string[] status)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_company_ord_status_to_invoiced (CompanyID,OrderStatus,AllowInvoiceGeneration) VALUES(@CompanyID,@OrderStatus,@AllowInvoiceGeneration)";
            string sqlDeleteAll = "DELETE FROM z_company_ord_status_to_invoiced WHERE CompanyID=@CompanyID";
            try
            {
                dbHelp.ExecuteNonQuery(sqlDeleteAll, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int)
                });
                foreach (var item in status)
                {
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int),
                        DbUtility.GetParameter("OrderStatus", item, MyDbType.String),
                        DbUtility.GetParameter("AllowInvoiceGeneration", true, MyDbType.Boolean)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ValidateOrderStatusBeforeInvoiced(DbHelper dbHelp, int companyID, string status)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(*) FROM z_company_ord_status_to_invoiced WHERE CompanyID=@CompanyID AND OrderStatus=@OrderStatus AND AllowInvoiceGeneration=1";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int),
                    DbUtility.GetParameter("OrderStatus", status, MyDbType.String)
                });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {
                
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<string> GetValidOrderStatusToAllowInvoiceGeneration(DbHelper dbHelp, int companyID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT DISTINCT OrderStatus FROM z_company_ord_status_to_invoiced WHERE CompanyID=@CompanyID AND AllowInvoiceGeneration=1";
            try
            {
                List<string> lResult = new List<string>();
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CompanyID", companyID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(BusinessUtility.GetString(dr["OrderStatus"]));    
                    }                    
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Dropdownlist Helper functions
        public void FillCompany(DbHelper dbHelp,ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT companyID,companyName FROM sysCompanyInfo order by companyName asc ";
            DbHelper dbHelper = new DbHelper();
            try
            {
                using (var dr = dbHelper.GetDataReader(sql, System.Data.CommandType.Text, null))
                {
                    lCtrl.DataSource = dr;
                    lCtrl.DataTextField = "companyName";
                    lCtrl.DataValueField = "companyID";
                    lCtrl.DataBind();
                }
                
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelper.CloseDatabaseConnection();
            }
        }
        #endregion
    }

    public enum CompanyUnitType
    {
        Metric = 1,
        English = 2
    }
}
