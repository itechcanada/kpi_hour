﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;



namespace iTECH.InbizERP.BusinessLogic
{
    public class SysTaxCodeDesc
    {        
        #region Property
        public int SysTaxCodeDescID { get; set; }
        public string SysTaxCodeDescText { get; set; }
        #endregion
        #region Public Functions

        public DataTable GetAllTaxGroups(DbHelper dbHelp, string searchText)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                var lstParameters = new List<MySqlParameter>();
                string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc WHERE 1=1 ";
                if (!string.IsNullOrEmpty(searchText))
                {
                    sql += " AND sysTaxCodeDescText LIKE CONCAT('%', @SearchData, '%')";
                    lstParameters.Add(DbUtility.GetParameter("SearchData", searchText, MyDbType.String));
                }
                sql += " ORDER BY sysTaxCodeDescText ";
                return dbHelp.GetDataTable(sql, CommandType.Text, lstParameters.ToArray());
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
       
        /// <summary>
        /// Get all Details about Group Tax
        /// </summary>
        public void PopulateObject(DbHelper dbHelp, int taxGrpId)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc where sysTaxCodeDescID= @sysTaxCodeDescID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysTaxCodeDescID", taxGrpId, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.SysTaxCodeDescID = BusinessUtility.GetInt(dr["sysTaxCodeDescID"]);
                        this.SysTaxCodeDescText = BusinessUtility.GetString(dr["sysTaxCodeDescText"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }                        
        }

        public void PopulateObject(int taxGrpId)
        {
            DbHelper dbHelp =new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc where sysTaxCodeDescID= @sysTaxCodeDescID";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("sysTaxCodeDescID", taxGrpId, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        this.SysTaxCodeDescID = BusinessUtility.GetInt(dr["sysTaxCodeDescID"]);
                        this.SysTaxCodeDescText = BusinessUtility.GetString(dr["sysTaxCodeDescText"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        
        /// <summary>
        /// Update the Grop Tax Description 
        /// </summary>
        /// <returns></returns>
        public void Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                // Check sysTaxCodeDescText Exists or not
                string sqlChkDuplication = "Select Count(sysTaxCodeDescText) From systaxcodedesc WHERE sysTaxCodeDescText = @sysTaxCodeDescText AND sysTaxCodeDescID<>@sysTaxCodeDescID";
                object userCount = dbHelp.GetValue(sqlChkDuplication, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCodeDescText", this.SysTaxCodeDescText, MyDbType.String),
                    DbUtility.GetParameter("sysTaxCodeDescID", this.SysTaxCodeDescID, MyDbType.Int)
                });
                if (Convert.ToInt32(userCount) > 0)
                {
                    throw new Exception("Tax Group Already Exists!");
                }
                string sql = "UPDATE systaxcodedesc set  sysTaxCodeDescText=@sysTaxCodeDescText WHERE sysTaxCodeDescID = @sysTaxCodeDescID";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCodeDescText", this.SysTaxCodeDescText, MyDbType.String),
                    DbUtility.GetParameter("sysTaxCodeDescID", this.SysTaxCodeDescID, MyDbType.Int)
                });
                this.SysTaxCodeDescID = dbHelp.GetLastInsertID();
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        
        /// <summary>
        /// Insert The Group Tax 
        /// </summary>
        /// <returns></returns>
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                // Check sysTaxCodeDescText Exists or not
                string sqlChkDuplication = "Select Count(sysTaxCodeDescText) From systaxcodedesc WHERE sysTaxCodeDescText=@sysTaxCodeDescText";
                object userCount = dbHelp.GetValue(sqlChkDuplication, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCodeDescText", this.SysTaxCodeDescText, MyDbType.String)
                });
                if (Convert.ToInt32(userCount) > 0) {
                    throw new Exception("Tax Group Already Exists!");
                }
                string sql = "Insert systaxcodedesc (sysTaxCodeDescText) Value (@sysTaxCodeDescText)";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCodeDescText", this.SysTaxCodeDescText, MyDbType.String)
                });
                this.SysTaxCodeDescID = dbHelp.GetLastInsertID();
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }      

        public void Delete(int grpID)
        {
            string sql = "DELETE FROM systaxcodedesc WHERE sysTaxCodeDescID=@ID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", grpID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
       
        #endregion

        #region Dropdown helper functions
        public void FillListControl(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc ORDER BY sysTaxCodeDescText";
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "sysTaxCodeDescText";
                lCtrl.DataValueField = "sysTaxCodeDescID";
                lCtrl.DataBind();                
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}
