﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CountryStateTaxGroup
    {
        public string Country { get; set; }
        public string StateCode { get; set; }
        public int StateGroupTaxCode { get; set; }

        public void PopulateListControlWithCountry(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT DISTINCT Country FROM z_country_state_tax_group  WHERE Country NOT IN ('USA', 'CANADA')";
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "Country";
                lCtrl.DataValueField = "Country";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateListControlWithStateCodes(DbHelper dbHelp, string country, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                //lCtrl.Items.Clear();
                //lCtrl.ClearSelection();
                lCtrl.SelectedIndex = -1;
                string sql = "SELECT * FROM z_country_state_tax_group WHERE Country=@Country";
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Country", country, MyDbType.String)
                });
                lCtrl.DataTextField = "StateCode";
                lCtrl.DataValueField = "StateCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<CountryStateTaxGroup> GetListCountry(DbHelper dbHelp, string country)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<CountryStateTaxGroup> lResult = new List<CountryStateTaxGroup>();
            try
            {
                string sql = "SELECT * FROM z_country_state_tax_group WHERE Country=@Country";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("Country", country, MyDbType.String) }))
                {
                    while (dr.Read())
                    {
                        CountryStateTaxGroup stg = new CountryStateTaxGroup();
                        stg.Country = BusinessUtility.GetString(dr["Country"]);
                        stg.StateCode = BusinessUtility.GetString(dr["StateCode"]);
                        stg.StateGroupTaxCode = BusinessUtility.GetInt(dr["StateGroupTaxCode"]);

                        lResult.Add(stg);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<CountryStateTaxGroup> GetAllCountryStateList(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<CountryStateTaxGroup> lResult = new List<CountryStateTaxGroup>();
            try
            {
                string sql = "SELECT * FROM z_country_state_tax_group";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        CountryStateTaxGroup stg = new CountryStateTaxGroup();
                        stg.Country = BusinessUtility.GetString(dr["Country"]);
                        stg.StateCode = BusinessUtility.GetString(dr["StateCode"]);
                        stg.StateGroupTaxCode = BusinessUtility.GetInt(dr["StateGroupTaxCode"]);

                        lResult.Add(stg);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, string country, string state)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }            
            try
            {
                string sql = "SELECT * FROM z_country_state_tax_group WHERE Country=@Country AND StateCode=@StateCode";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Country", country, MyDbType.String),
                    DbUtility.GetParameter("StateCode", state, MyDbType.String)
                }))
                {
                    if (dr.Read())
                    {                       
                        this.Country = BusinessUtility.GetString(dr["Country"]);
                        this.StateCode = BusinessUtility.GetString(dr["StateCode"]);
                        this.StateGroupTaxCode = BusinessUtility.GetInt(dr["StateGroupTaxCode"]);                        
                    }
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
