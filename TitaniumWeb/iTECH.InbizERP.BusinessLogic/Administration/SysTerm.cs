﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class NetTerm
    {
        public int SysTermID { get; set; }
        public string SysTerm { get; set; }
        public int SysDays { get; set; }
        public double Discount { get; set; }
        public bool IsActive { get; set; }

        public void PopulateListControl(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_sys_term WHERE IsActive=1";
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "SysTerm";
                lCtrl.DataValueField = "SysTermID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
