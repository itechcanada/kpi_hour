﻿#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
#endregion
#region "Modify Version "
/**********************************
 * Modified By   :: Ankit Choure
 * Modified Date :: 4-July-2011
 * ********************************/
#endregion

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysUserModule
    {
        #region Private Member
        private int _userID;
        private int _userTypeID;
        private int _moduleID;
        private char _accessType;

        #endregion

        #region Property
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        public int UserTypeID
        {
            get { return _userTypeID; }
            set { _userTypeID = value; }
        }

        public int ModuleID
        {
            get { return _moduleID; }
            set { _moduleID = value; }
        }
        public char AccessType
        {
            get { return _accessType; }
            set { _accessType = value; }
        }
        #endregion
        /// <summary>
        /// Delete the User Module in sysusermodules table By User id
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool DeleteUserModules(int userID)
        {
            //SQL to Delete              
            string sql = "DELETE FROM sysusermodules  WHERE UserID = @UserID;";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@UserID", userID)
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
        public int InsertUserModules()
        {
            DbHelper dbHelper = new DbHelper(true);
            //SQL to Insert              
            string sql = "INSERT INTO sysusermodules (UserID, UserTypeID, ModuleID, AccessType) VALUES(@UserID, @UserTypeID, @ModuleID, @AccessType);";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@UserID", this._userID),
                                     new MySqlParameter("@UserTypeID", this._userTypeID),
                                     new MySqlParameter("@ModuleID", this._moduleID),
                                     new MySqlParameter("@AccessType", this._accessType)
                                     };
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return dbHelper.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
    }
}
