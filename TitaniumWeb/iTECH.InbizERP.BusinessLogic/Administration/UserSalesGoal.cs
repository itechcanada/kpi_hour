﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class UserSalesGoal
    {
        public int SalesGoalUserID { get; set; }
        public int idUserSalesGoal { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public double SalesAmountPerDay { get; set; }
        public Boolean Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.idUserSalesGoal > 0 && this.SalesGoalUserID > 0)
                {
                    string sqlUpdate = " UPDATE usersalesgoal SET fromDate = @fromDate, toDate = @toDate, salesAmountPerDay = @salesAmountPerDay WHERE userID = @userID and idUserSalesGoal=@idUserSalesGoal ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("userID", this.SalesGoalUserID, MyDbType.Int),
                    DbUtility.GetParameter("idUserSalesGoal", this.idUserSalesGoal, MyDbType.Int),
                    DbUtility.GetParameter("fromDate", this.FromDate, MyDbType.DateTime),
                    DbUtility.GetParameter("toDate", this.ToDate, MyDbType.DateTime),
                    DbUtility.GetParameter("salesAmountPerDay", this.SalesAmountPerDay, MyDbType.Double),
                    });
                    SaveUsersSalesGoalPerDay(this.SalesGoalUserID, this.FromDate, this.ToDate, this.SalesAmountPerDay);
                }
                else
                {
                    string sql = " INSERT INTO usersalesgoal (fromDate,toDate,salesAmountPerDay,userID) VALUES (@fromDate,@toDate,@salesAmountPerDay,@userID) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("fromDate", this.FromDate, MyDbType.DateTime),
                    DbUtility.GetParameter("toDate", this.ToDate, MyDbType.DateTime),
                    DbUtility.GetParameter("salesAmountPerDay", this.SalesAmountPerDay, MyDbType.Double),
                    DbUtility.GetParameter("userID", this.SalesGoalUserID, MyDbType.Int)
                    });
                    this.idUserSalesGoal = dbHelp.GetLastInsertID();

                    SaveUsersSalesGoalPerDay(this.SalesGoalUserID, this.FromDate, this.ToDate, this.SalesAmountPerDay);
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetSalesGoalList(DbHelper dbHelp, int sUserID, int sSalesGoalID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = " select * from usersalesgoal where userID =  " + sUserID;
                if (sSalesGoalID > 0)
                {
                    sql += " and idUserSalesGoal = " + sSalesGoalID;
                }
                sql += " ORDER BY fromDate DESC";
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sql), CommandType.Text, null);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        private Boolean SaveUsersSalesGoalPerDay(int iUserID, DateTime dtGoalStart, DateTime dtGoalEnd, Double dblSalesPerDay)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlInsert = "";
                string sqlUpdate = "";
                int iDaysCountToAdd = 0;
                int i = 0;
                DateTime dtLastUserSavedToDate = DateTime.MinValue;
                DateTime dtToAdd;
                #region GetUserLastSetGoalDate
                string sql = " select * from userssalesgoalperday where userID =  " + iUserID;
                sql += " order by SalesDate Desc Limit 1 ";
                DataTable dt = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sql), CommandType.Text, null);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        dtLastUserSavedToDate = Convert.ToDateTime(dt.Rows[0]["SalesDate"]);
                    }
                }

                if (dtLastUserSavedToDate != DateTime.MinValue)
                {
                    if ((dtLastUserSavedToDate <= dtGoalEnd) && (dtGoalStart != dtGoalEnd))
                    {
                        sql = " SELECT * FROM userssalesgoalperday WHERE userID =  " + iUserID;
                        //sql += " AND SalesDate BETWEEN @FromDate AND @ToDate ";
                        sql += " AND SalesDate >= @ToDate ";
                        dt = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sql), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                                DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                                });

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                sqlUpdate = " UPDATE userssalesgoalperday SET  SalesPerDay = CASE  WHEN  SalesPerDay >= " + dblSalesPerDay + " THEN  SalesPerDay ELSE " + dblSalesPerDay + " END   WHERE  SalesDate BETWEEN @FromDate AND @ToDate AND UserID = @UserID ";
                                dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                                DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                                DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double),
                                });
                            }
                            else
                            {
                                if ((dtGoalStart <= dtLastUserSavedToDate) && (dtGoalEnd >= dtLastUserSavedToDate))
                                {
                                    sqlUpdate = " UPDATE userssalesgoalperday SET  SalesPerDay = CASE  WHEN  SalesPerDay >= " + dblSalesPerDay + " THEN  SalesPerDay ELSE " + dblSalesPerDay + " END  WHERE  SalesDate BETWEEN @FromDate AND @ToDate AND UserID = @UserID ";
                                    dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                    DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                                    DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                                    DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double),
                                    });

                                    TimeSpan span = dtGoalEnd.Subtract(dtLastUserSavedToDate);
                                    iDaysCountToAdd = (int)span.TotalDays;
                                    if (iDaysCountToAdd > 0)
                                    {
                                        i = 1;
                                        while (i <= iDaysCountToAdd)
                                        {
                                            dtToAdd = dtLastUserSavedToDate.AddDays(i);
                                            sqlInsert = " INSERT INTO userssalesgoalperday (UserID, SalesDate, SalesPerDay ) VALUES (@UserID, @SalesDate, @SalesPerDay) ";
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                            DbUtility.GetParameter("SalesDate", dtToAdd, MyDbType.DateTime),
                                            DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double)
                                            });
                                            i += 1;
                                        }
                                    }
                                }
                                else
                                {
                                    TimeSpan span = dtGoalEnd.Subtract(dtGoalStart);
                                    iDaysCountToAdd = (int)span.TotalDays;
                                    if (iDaysCountToAdd > 0)
                                    {
                                        i = 1;
                                        dtLastUserSavedToDate = dtGoalStart.AddDays(-1);
                                        while (i <= iDaysCountToAdd + 1)
                                        {
                                            dtToAdd = dtLastUserSavedToDate.AddDays(i);
                                            sqlInsert = " INSERT INTO userssalesgoalperday (UserID, SalesDate, SalesPerDay ) VALUES (@UserID, @SalesDate, @SalesPerDay) ";
                                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                        DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                        DbUtility.GetParameter("SalesDate", dtToAdd, MyDbType.DateTime),
                                        DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double)
                                        });
                                            i += 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if ((dtGoalStart <= dtLastUserSavedToDate) && (dtGoalEnd <= dtLastUserSavedToDate))
                    {
                        sqlUpdate = " UPDATE userssalesgoalperday SET SalesPerDay = CASE  WHEN  SalesPerDay >= " + dblSalesPerDay + " THEN  SalesPerDay ELSE " + dblSalesPerDay + " END  WHERE  SalesDate BETWEEN @FromDate AND @ToDate AND UserID = @UserID ";
                        dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                            DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                            DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                            DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double),
                            });
                    }
                }
                else if (dtGoalStart == dtGoalEnd)
                {
                    sql = " SELECT * FROM userssalesgoalperday WHERE userID =  " + iUserID;
                    //sql += " AND SalesDate BETWEEN @FromDate AND @ToDate ";
                    sql += " AND SalesDate >= @ToDate ";
                    dt = dbTransactionHelper.GetDataTable(BusinessUtility.GetString(sql), CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                                DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                                });

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            sqlUpdate = " UPDATE userssalesgoalperday SET  SalesPerDay = CASE  WHEN  SalesPerDay >= " + dblSalesPerDay + " THEN  SalesPerDay ELSE " + dblSalesPerDay + " END   WHERE  SalesDate BETWEEN @FromDate AND @ToDate AND UserID = @UserID ";
                            dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                DbUtility.GetParameter("FromDate", dtGoalStart, MyDbType.DateTime),
                                DbUtility.GetParameter("ToDate", dtGoalEnd, MyDbType.DateTime),
                                DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double),
                                });
                        }
                        else
                        {
                            TimeSpan span = dtGoalEnd.Subtract(dtGoalStart);
                            iDaysCountToAdd = (int)span.TotalDays;
                            //if (iDaysCountToAdd > 0)
                            {
                                i = 1;
                                dtLastUserSavedToDate = dtGoalStart.AddDays(-1);
                                while (i <= iDaysCountToAdd + 1)
                                {
                                    dtToAdd = dtLastUserSavedToDate.AddDays(i);
                                    sqlInsert = " INSERT INTO userssalesgoalperday (UserID, SalesDate, SalesPerDay ) VALUES (@UserID, @SalesDate, @SalesPerDay) ";
                                    dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                        DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                        DbUtility.GetParameter("SalesDate", dtToAdd, MyDbType.DateTime),
                                        DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double)
                                        });
                                    i += 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    TimeSpan span = dtGoalEnd.Subtract(dtGoalStart);
                    iDaysCountToAdd = (int)span.TotalDays;
                    if (iDaysCountToAdd > 0)
                    {
                        i = 1;
                        dtLastUserSavedToDate = dtGoalStart.AddDays(-1);
                        while (i <= iDaysCountToAdd + 1)
                        {
                            dtToAdd = dtLastUserSavedToDate.AddDays(i);
                            sqlInsert = " INSERT INTO userssalesgoalperday (UserID, SalesDate, SalesPerDay ) VALUES (@UserID, @SalesDate, @SalesPerDay) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                DbUtility.GetParameter("UserID", iUserID, MyDbType.Int),
                                DbUtility.GetParameter("SalesDate", dtToAdd, MyDbType.DateTime),
                                DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double)
                                });

                            i += 1;
                        }
                    }
                }
                #endregion
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

    }
}
