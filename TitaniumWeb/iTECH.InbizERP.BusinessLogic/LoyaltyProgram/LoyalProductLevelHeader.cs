﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class LoyalProductLevelHeader
    {
        public int LoyalProductLevelID { get; set; }
        public int LoyalProductID { get; set; }
        public int LoyalCategoryID { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public int LastUpdatedBy { get; set; }
        /// <summary>
        /// (Q= Product Quantity , M= monetary Value)
        /// </summary>
        public string PointsPerUnitType { get; set; }
        public double RedeemPoints { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "INSERT INTO z_loyal_product_level_hdr ";
                sql += " (LoyalProductID,LoyalCategoryID,IsActive,LastUpdatedOn,LastUpdatedBy,PointsPerUnitType,RedeemPoints)";
                sql += " VALUES(@LoyalProductID,@LoyalCategoryID,@IsActive,@LastUpdatedOn,@LastUpdatedBy,@PointsPerUnitType,@RedeemPoints)";

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalProductID", this.LoyalProductID, MyDbType.Int),
                    DbUtility.GetParameter("LoyalCategoryID", this.LoyalCategoryID, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("LastUpdatedOn", this.LastUpdatedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("LastUpdatedBy", this.LastUpdatedBy, MyDbType.Int),
                    DbUtility.GetParameter("PointsPerUnitType", this.PointsPerUnitType, MyDbType.String),
                    DbUtility.GetParameter("RedeemPoints", this.RedeemPoints, MyDbType.Double)
                });

                this.LoyalProductLevelID = dbHelp.GetLastInsertID();
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = @"UPDATE z_loyal_product_level_hdr SET 
                                   LoyalProductID=@LoyalProductID,LoyalCategoryID=@LoyalCategoryID,IsActive=@IsActive,LastUpdatedOn=@LastUpdatedOn,
                                   LastUpdatedBy=@LastUpdatedBy,PointsPerUnitType=@PointsPerUnitType,RedeemPoints=@RedeemPoints 
                                WHERE LoyalProductLevelID=@LoyalProductLevelID ";

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalProductLevelID", this.LoyalProductLevelID, MyDbType.Int),
                    DbUtility.GetParameter("LoyalProductID", this.LoyalProductID, MyDbType.Int),
                    DbUtility.GetParameter("LoyalCategoryID", this.LoyalCategoryID, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("LastUpdatedOn", this.LastUpdatedOn, MyDbType.DateTime),
                    DbUtility.GetParameter("LastUpdatedBy", this.LastUpdatedBy, MyDbType.Int),
                    DbUtility.GetParameter("PointsPerUnitType", this.PointsPerUnitType, MyDbType.String),
                    DbUtility.GetParameter("RedeemPoints", this.RedeemPoints, MyDbType.Double)
                });
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public DataTable GetLoyalProducts(DbHelper dbHelp, string opt)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = string.Empty;
                if (opt == "add")
                {
                    sql = "SELECT t1.* FROM (SELECT ph.LoyalProductLevelID, p.prdUPCCode,p.prdName,";
                    sql += " p.prdEndUserSalesPrice,funGetProductQtyOnHead(p.ProductID, NULL) AS OnHeadQty,";
                    sql += " IFNULL((SELECT SUM(pdtl.PointsPerUnit) FROM z_loyal_product_level_dtl pdtl WHERE pdtl.LoyalProductLevelHeaderID = ph.LoyalProductLevelID), 0)  AS Points,";
                    sql += " p.ProductID FROM products p LEFT JOIN z_loyal_product_level_hdr ph";
                    sql += " ON ph.LoyalProductID = p.ProductID) AS t1 WHERE t1.Points > 0";
                }
                else
                {
                    sql = "SELECT ph.LoyalProductLevelID, p.prdUPCCode,p.prdName,p.prdEndUserSalesPrice, funGetProductQtyOnHead(p.ProductID, NULL) AS OnHeadQty,";
                    sql += " IFNULL(ph.RedeemPoints, 0) AS Points, p.ProductID";
                    sql += " FROM products p LEFT JOIN z_loyal_product_level_hdr ph ON ph.LoyalProductID=p.ProductID";
                    sql += " WHERE IFNULL(ph.RedeemPoints, 0) > 0";
                }

                return dbHelp.GetDataTable(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        public DataTable GetLoyalLvlAndHdr(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = @"SELECT DISTINCT * FROM z_loyal_product_level_dtl lpld
                               INNER JOIN z_loyal_product_level_hdr lplh ON lplh.LoyalProductLevelID=lpld.LoyalProductLevelHeaderID
                               WHERE lplh.LoyalProductID=@LoyalProductID";
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("LoyalProductID", this.LoyalProductID, MyDbType.Int) });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
