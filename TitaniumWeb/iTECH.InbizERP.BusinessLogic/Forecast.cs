﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using iTECH.Library.DataAccess.MsSql;
using System.Data.SqlClient;
using iTECH.Library.Utilities;
namespace iTECH.InbizERP.BusinessLogic
{
    public class ForecastResult
    {
        public string Database
        {
            get;
            set;
        }
        public string Branch
        {
            get;
            set;
        }
        public double Budget
        {
            get;
            set;
        }
        public double Forecast
        {
            get;
            set;
        }
        public double GPBudget
        {
            get;
            set;
        }
        public double GPForecast
        {
            get;
            set;
        }
        public double GPForecastPct
        {
            get;
            set;
        }
        public double LBSShippedPlan
        {
            get;
            set;
        }
        public DataTable GetForecastValue() //string yyyyMM
        {
            DataTable dt = new DataTable();
            DbHelper dbhelp = new DbHelper();
            
            try
            {

                string sql = "Select Databases,Branch,Budget,Forecast,GPBudget,ISNULL(GPForecast,0) as GPForecast,ISNULL(GPForecastPct,0.0) as GPForecastPct,LBSShippedPlan from tbl_itech_Forecast_AM where YearMonth = convert(varchar(7),GetDate(), 126) order by Databases desc";
                dt = dbhelp.GetDataTable(sql, CommandType.Text, null);
                    //new SqlParameter[]{
                    //DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))});
                if (dt.Rows.Count == 0)
                {
                    string sqlinsert = "INSERT INTO [tbl_itech_Forecast_AM] ([Budget],[Forecast],[GPBudget],[GPForecast],[GPForecastPct],[LBSShippedPlan],[WarehseFees],[Branch],[Databases],[YearMonth],[Date],[InvtValBrh]) ";
                    sqlinsert += " select 0,0,0,0,0.0,0,0,[Branch],[Databases],convert(varchar(7),GetDate(), 126),convert(varchar(10),GetDate(), 126),InvtValBrh from [tbl_itech_Forecast_AM] where YearMonth = convert(varchar(7),dateadd(month, datediff(month, 0, getdate()) - 1, 0), 126) ";
                    dbhelp.ExecuteNonQuery(sqlinsert, CommandType.Text, null);
                    dt = dbhelp.GetDataTable(sql, CommandType.Text, null);
                    //    new SqlParameter[]{
                    //DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))});
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
            return dt;

        }

        public void UpdateForecast()    //string yyyyMM
        {
            DbHelper dbhelp = new DbHelper();
            try
            {
                string sql = " UPDATE tbl_itech_Forecast_AM set Budget = @Budget, Forecast = @Forecast, GPBudget = @GPBudget, GPForecast = @GPForecast,GPForecastPct = @GPForecastPct, LBSShippedPlan = @LBSShippedPlan, YearMonth = convert(varchar(7),GetDate(), 126), Date=@Date";
                sql += " WHERE Branch=@Branch and Databases=@Databases and YearMonth = convert(varchar(7),GetDate(), 126)";
                dbhelp.ExecuteNonQuery(sql, CommandType.Text, new SqlParameter[]{
                    DbUtility.GetParameter("Budget", this.Budget, typeof(double)),
                    DbUtility.GetParameter("Forecast", this.Forecast, typeof(double)),
                    DbUtility.GetParameter("GPBudget", this.GPBudget, typeof(double)),
                    DbUtility.GetParameter("GPForecast", this.GPForecast, typeof(double)),
                    DbUtility.GetParameter("GPForecastPct", this.GPForecastPct, typeof(double)),
                    DbUtility.GetParameter("LBSShippedPlan", this.LBSShippedPlan, typeof(double)),
                    DbUtility.GetParameter("Branch", this.Branch, typeof(string)),
                    DbUtility.GetParameter("Databases", this.Database, typeof(string)),
                    DbUtility.GetParameter("Date", DateTime.Now, typeof(DateTime))  //,    DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
        }
        public List<ForecastResult> GetForecastDetails(string dbName, string branch) //, string yyyyMM
        {
            DbHelper dbhelp = new DbHelper();
            SqlDataReader dr = null;
            List<ForecastResult> lResult = new List<ForecastResult>();
            SqlParameter[] p = { DbUtility.GetParameter("Databases", dbName, typeof(string)),
                               DbUtility.GetParameter("Branch", branch, typeof(string)) //, DbUtility.GetParameter("YearMonth", yyyyMM, typeof(string))
                               };
            try
            {
                string sql = " SELECT Budget,Forecast,GPBudget,GPForecast,GPForecastPct,LBSShippedPlan from tbl_itech_Forecast_AM where Databases= @Databases and Branch=@Branch AND YearMonth = convert(varchar(7),GetDate(), 126) "; //@YearMonth

                dr = dbhelp.GetDataReader(sql, System.Data.CommandType.Text, p);

                if (dr.Read())
                {
                    lResult.Add(new ForecastResult { Budget = BusinessUtility.GetDouble(dr["Budget"]), Forecast = BusinessUtility.GetDouble(dr["Forecast"]), GPBudget = BusinessUtility.GetDouble(dr["GPBudget"]), GPForecast = BusinessUtility.GetDouble(dr["GPForecast"]), GPForecastPct = BusinessUtility.GetDouble(dr["GPForecastPct"]), LBSShippedPlan = BusinessUtility.GetDouble(dr["LBSShippedPlan"]) });
                }
                return lResult;

            }
            catch
            {
                throw;
            }
            finally
            {
                dbhelp.CloseDatabaseConnection();
            }
        }
    }
}
