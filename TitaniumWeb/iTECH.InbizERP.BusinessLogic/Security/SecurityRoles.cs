﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class SecurityRoles
    {
        public DataTable GetRolesByModuleID(DbHelper dbHelp, int moduleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlSelect = "SELECT mr.ModuleID, mr.RoleID,r.RoleName,r.RoleDesc" + lang + " AS RoleDesc,r.OldSecurityKey FROM z_security_modules_roles mr";
                sqlSelect += " INNER JOIN z_security_roles r ON (mr.RoleID = r.RoleID) WHERE mr.ModuleID=@ModuleID";
                return dbHelp.GetDataTable(sqlSelect, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ModuleID", moduleID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //private int _roleID;

        //public int RoleID
        //{
        //    get { return _roleID; }
        //    set { _roleID = value; }
        //}
        //private int _moduleID;

        //public int ModuleID
        //{
        //    get { return _moduleID; }
        //    set { _moduleID = value; }
        //}

        //private int _roleDescID;

        //public int RoleDescID
        //{
        //    get { return _roleDescID; }
        //    set { _roleDescID = value; }
        //}

        //public SecurityRoles() { }
        //public SecurityRoles(int roleID, int moduleID, int roleDescID) {
        //    this._roleID = roleID;
        //    this._moduleID = moduleID;
        //    this._roleDescID = roleDescID;
        //}

        //public OperationResult Insert() {
        //    string sqlExist = "SELECT COUNT(*) FROM z_security_roles WHERE ModuleID=@ModuleID AND RoleDescID=@RoleDescID";
        //    string sqlInsert = "INSERT INTO z_security_roles (ModuleID,RoleDescID) VALUES (@ModuleID,@RoleDescID)";
            
        //    DbHelper dbHelp = new DbHelper();
            
        //    List<MySqlParameter> pExist = new List<MySqlParameter>();
        //    pExist.Add(new MySqlParameter("@ModuleID", this.ModuleID > 0 ? (object)this.ModuleID : DBNull.Value));
        //    pExist.Add(DbUtility.GetParameter("RoleDescID", this.RoleDescID, MyDbType.Int));

        //    List<MySqlParameter> p = new List<MySqlParameter>();
        //    p.Add(DbUtility.GetParameter("RoleName", this.RoleDescID, MyDbType.Int));           

        //    try
        //    {
        //        object scalar = dbHelp.GetValue(sqlExist, CommandType.Text, pExist.ToArray());
        //        if (BusinessUtility.GetInt(scalar) > 0) {
        //            return OperationResult.AlreadyExists;
        //        }

        //        if (dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, p.ToArray()) > 0) {
        //            return OperationResult.Success;
        //        }

        //        return OperationResult.None;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}
        

        //public void PopulateObject(int roleID)
        //{
        //    string sql = "SELECT RoleID,ModuleID,RoleDescID FROM z_security_roles WHERE RoleID=@RoleID";

        //    DbHelper dbHelp = new DbHelper();
        //    List<MySqlParameter> p = new List<MySqlParameter>();
        //    p.Add(DbUtility.GetParameter("RoleID", roleID, MyDbType.Int));
        //    MySqlDataReader dr = null;
        //    try
        //    {
        //        dr = dbHelp.GetDataReader(sql, CommandType.Text, p.ToArray());
        //        if (dr.Read())
        //        {
        //            this.RoleID = BusinessUtility.GetInt(dr["RoleID"]);                   
        //            this.ModuleID = BusinessUtility.GetInt(dr["ModuleID"]);
        //            this.RoleDescID = BusinessUtility.GetInt(dr["RoleName"]);
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection(dr);
        //    }
        //}

        //public List<SecurityRoleDescription> GetRolDescriptions(int[] roleDescIds, int moduleID, string lang) {
        //    string ids = StringUtil.GetJoinedString(",", roleDescIds);
        //    List<SecurityRoleDescription> lResult = new List<SecurityRoleDescription>();
        //    DbHelper dbHelp = new DbHelper(true);
        //    MySqlDataReader dr = null;
        //    string sql = string.Format("SELECT zr.RoleID, zrd.RoleDescID,zrd.RoleName,zrd.RoleDesc{0} AS RoleDesc, zrd.OldSecurityKey, zrd.AttachModules FROM z_security_role_description zrd INNER JOIN z_security_roles zr ON zr.RoleDescID=zrd.RoleDescID  WHERE zr.ModuleID={1} AND zrd.RoleDescID IN ({2}) ORDER BY RoleName", lang, moduleID, ids);
        //    try
        //    {
        //        dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
        //        while (dr.Read()) {
        //            SecurityRoleDescription desc = new SecurityRoleDescription();
        //            desc.OldSecurityKey = BusinessUtility.GetString(dr["OldSecurityKey"]);
        //            desc.RoleDesc = BusinessUtility.GetString(dr["RoleDesc"]);
        //            desc.RoleDescID = BusinessUtility.GetInt(dr["RoleDescID"]);
        //            desc.AttachModules = BusinessUtility.GetString(dr["AttachModules"]);
        //            desc.RoleID = BusinessUtility.GetInt(dr["RoleID"]);
        //            lResult.Add(desc);
        //        }
        //    }
        //    catch{}
        //    finally {
        //        dbHelp.CloseDatabaseConnection(dr);
        //    }
        //    return lResult;
        //}
    }

    //public class SecurityRoleDescription
    //{
    //    public int RoleID { get; set; }
    //    public int RoleDescID { get; set; }
    //    public string RoleDesc { get; set; }
    //    public string OldSecurityKey { get; set; }
    //    public string AttachModules { get; set; }
    //}
}
