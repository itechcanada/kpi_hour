﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProcessUsers
    {
        public static InbizUser GetUser(string loginid, string password) {
            string sqlvalidate = "SELECT userID FROM users WHERE userLoginId=@userLoginId AND userPassword=PASSWORD(@userPassword) AND userActive=1";
            DbHelper dbHelp = new DbHelper(true);                        
            try
            {
                object scalar = dbHelp.GetValue(sqlvalidate, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("userLoginId", loginid, MyDbType.String), DbUtility.GetParameter("userPassword", password, MyDbType.String) });
                int userid = BusinessUtility.GetInt(scalar);

                if (userid > 0) {
                    InbizUser usr = new InbizUser();
                    usr.PopulateObject(userid, dbHelp);
                    SysWarehouses whs = new SysWarehouses();
                    whs.PopulateObject(usr.UserDefaultWhs, dbHelp);
                    SysCompanyInfo comp = new SysCompanyInfo();
                    comp.PopulateObject(whs.WarehouseCompanyID, dbHelp);
                    //To keep alive Old site features
                    CurrentUser.SetCurrentUserSession(usr.UserID, usr.UserLoginId, usr.UserEmail, usr.UserDefaultWhs, comp.CompanyID, comp.CompanyName, usr.UserSalutation + " " + usr.UserFirstName + " " + usr.UserLastName, usr.UserModules);

                    //Set New Modules & Permissions
                    //SetUserModulesAndPermissions(usr.UserID, dbHelp);

                    //Set sales represtricted status
                    SetSalesRepRestrictedStatus(dbHelp);

                    //Get UserRoles
                    string roles = string.Join(",", GetUserRoles(usr.UserID, dbHelp).ToArray());

                    //Get all modules to assigned to logged user
                    UserProfile.SignIn(usr.UserID, usr.UserLoginId, usr.UserFirstName, roles);
                    return usr;
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        private static List<string> GetUserRoles(int userid, DbHelper dbHelp) {
            string sql = "SELECT DISTINCT zr.RoleID FROM z_security_user_roles z INNER JOIN z_security_roles zr ON zr.RoleID=z.RoleID WHERE z.UserID=@UserID";
            List<string> lResult = new List<string>();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) });
                while (dr.Read()) { 
                    lResult.Add(BusinessUtility.GetString(dr[0]));
                }
            }
            catch
            { }
            finally {
                if (dr != null && !dr.IsClosed) {
                    dr.Close();
                }
            }
            return lResult;
        }

        //private static void SetUserModulesAndPermissions(int userid, DbHelper dbHelp) {            
        //    string sqlModules = "SELECT DISTINCT zr.ModuleID FROM z_security_user_roles zur INNER JOIN z_security_roles zr ON zur.RoleID=zr.RoleID  WHERE zur.UserID=@UserID";
        //    string sqlPermissions = "SELECT DISTINCT zp.PermissionID FROM z_security_user_roles zu INNER JOIN z_security_role_permissions zp ON zp.RoleID=zu.RoleID WHERE zu.UserID=@UserID";
        //    MySqlDataReader dr = null;
        //    try
        //    {
        //        dr = dbHelp.GetDataReader(sqlModules, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) });
        //        List<int> lstResult1 = new List<int>();
        //        while (dr.Read()) {
        //            lstResult1.Add(BusinessUtility.GetInt(dr[0]));
        //        }

        //        dr.Close();
        //        CurrentUser.SetUserModules(lstResult1);

        //        dr = dbHelp.GetDataReader(sqlPermissions, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int) });
        //        List<int> lstResult2 = new List<int>();
        //        while (dr.Read())
        //        {
        //            lstResult2.Add(BusinessUtility.GetInt(dr[0]));
        //        }
        //        CurrentUser.SetUserPermissions(lstResult2);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally {
        //        dr.Close();
        //    }
        //}

        public static bool HasUserRole(int userid, int roleid) {
            string sql = "SELECT COUNT(*) FROM z_security_user_roles WHERE UserID=@UserID AND RoleID=@roleID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", userid, MyDbType.Int), DbUtility.GetParameter("RoleID", roleid, MyDbType.Int) });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void SetSalesRepRestrictedStatus(DbHelper dbHelp) {
            string sql = "SELECT CompanySalesRepRestricted FROM syscompanyinfo limit 1";
            object scalar = dbHelp.GetValue(sql, CommandType.Text, null);
            CurrentUser.SetSalesRepRestrictedStatus(BusinessUtility.GetString(scalar));
        }

        public static List<InbizUser> GetUsersByRole(DbHelper dbHelp, params RoleID[] roleID)
        {
            string sql = "SELECT DISTINCT ur.UserID FROM z_security_user_roles ur INNER JOIN z_security_roles sr ON sr.RoleID=ur.RoleID WHERE 1=1";
            List<InbizUser> lResult = new List<InbizUser>();           
            MySqlDataReader dr = null;
            try
            {
                List<int> lUsr = new List<int>();
                List<string> lstRoles = new List<string>();
                foreach (var item in roleID)
                {
                    if (item > 0)
                    {
                        lstRoles.Add(((int)item).ToString());
                    }
                }
                if (lstRoles.Count > 0)
                {
                    sql += string.Format(" AND sr.RoleID IN ({0})", string.Join(",", lstRoles.ToArray()));
                    dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
                    while (dr.Read())
                    {
                        lUsr.Add(BusinessUtility.GetInt(dr["UserID"]));
                    }
                    dr.Close(); 
                    dr.Dispose();

                    foreach (var item in lUsr)
                    {
                        InbizUser usr = new InbizUser();
                        usr.PopulateObject(item, dbHelp);
                        lResult.Add(usr);
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr!= null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }
    }

    public class CurrentUser
    {
        public static int UserID
        {
            get
            {
                return BusinessUtility.GetInt(HttpContext.Current.Session["UserID"]);
            }
        }

        public static string EmailID
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["EmailID"]);
            }
        }

        public static string UserDefaultWarehouse
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["UserWarehouse"]);
            }
        }

        public static int DefaultCompanyID
        {
            get
            {
                return BusinessUtility.GetInt(HttpContext.Current.Session["DefaultCompanyID"]);

            }
        }

        public static string CompanyName
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["CompanyName"]);
            }
        }

        public static int LoginID
        {
            get
            {
                return BusinessUtility.GetInt(HttpContext.Current.Session["LoginID"]);
            }
        }

        public static bool IsAutheticated
        {
            get
            {
                return CurrentUser.UserID > 0;
            }
        }

        public static bool IsSalesRistricted
        {
            get
            {
                return CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(HttpContext.Current.Session["SalesRepRestricted"]);
            }
        }

        public static string UserName
        {
            get
            {
                return BusinessUtility.GetString(HttpContext.Current.Session["UserName"]);
            }
        }

        public static void SetCurrentUserSession(int userID, string loginID, string emailID, string defWharehouse, int defCompanyId, string compName, string uName, string oldModules)
        {
            HttpContext.Current.Session["UserID"] = userID;
            HttpContext.Current.Session["LoginID"] = loginID;
            HttpContext.Current.Session["EmailID"] = emailID;
            HttpContext.Current.Session["UserWarehouse"] = defWharehouse;
            HttpContext.Current.Session["DefaultCompanyID"] = defCompanyId;
            HttpContext.Current.Session["CompanyName"] = compName;
            HttpContext.Current.Session["UserName"] = uName;
            HttpContext.Current.Session["UserModules"] = oldModules;
        }

        public static void RemoveUserFromSession()
        {
            HttpContext.Current.Session.Remove("UserID");
            HttpContext.Current.Session.Remove("LoginID");
            HttpContext.Current.Session.Remove("EmailID");
            HttpContext.Current.Session.Remove("UserWarehouse");
            HttpContext.Current.Session.Remove("DefaultCompanyID");
            HttpContext.Current.Session.Remove("CompanyName");
            HttpContext.Current.Session.Remove("UserName");
            HttpContext.Current.Session.Remove("UserModules");

            HttpContext.Current.Session.Remove("__CURRENT_USER_MODULES");
            HttpContext.Current.Session.Remove("__CURRENT_USER_PERMISSIONS");
        }

        //public static void SetUserModules(List<int> modules)
        //{
        //    HttpContext.Current.Session["__CURRENT_USER_MODULES"] = modules;
        //}

        //public static void SetUserPermissions(List<int> permissions)
        //{
        //    HttpContext.Current.Session["__CURRENT_USER_PERMISSIONS"] = permissions;
        //}

        //public static bool HasModule(Modules module)
        //{
        //    if (HttpContext.Current.Session["__CURRENT_USER_MODULES"] != null)
        //    {
        //        List<int> modules = (List<int>)HttpContext.Current.Session["__CURRENT_USER_MODULES"];

        //        return modules.Contains((int)module);
        //    }
        //    return false;
        //}

        //public static bool HasPermission(Permissions permission)
        //{
        //    if (HttpContext.Current.Session["__CURRENT_USER_PERMISSIONS"] != null)
        //    {
        //        List<int> permissions = (List<int>)HttpContext.Current.Session["__CURRENT_USER_PERMISSIONS"];

        //        return permissions.Contains((int)permission);
        //    }
        //    return false;
        //}

        public static bool IsInRole(RoleID role)
        {
            return UserProfile.IsInRole((int)role);
        }

        public static void SetSalesRepRestrictedStatus(string status)
        { //function to keep old logic live
            HttpContext.Current.Session["SalesRepRestricted"] = status;
        }

        public static bool ValidateAjaxRequest(string sessionID)
        {
            return HttpContext.Current.Session.SessionID == sessionID;
        }
    }
}
