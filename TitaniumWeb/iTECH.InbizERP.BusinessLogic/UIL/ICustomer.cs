﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public interface ICustomer
    {
        int PartnerID { get; }
        void Initialize();
    }
}
