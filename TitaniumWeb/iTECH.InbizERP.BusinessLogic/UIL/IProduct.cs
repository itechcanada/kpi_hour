﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public interface IProduct
    {
        int ProductID { get; }
        void Initialize();
    } 
}