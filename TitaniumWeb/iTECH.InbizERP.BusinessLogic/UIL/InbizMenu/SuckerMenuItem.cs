﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace iTECH.InbizERP.BusinessLogic
{
    /// <summary>
    /// http://www.aspcode.net/Suckerfish-menu-with-ASPNET-and-JQuery.aspx
    /// </summary>
    public class SuckerMenuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SuckerMenuItem"/> class.
        /// </summary>
        /// <param name="sLink">The s link.</param>
        /// <param name="sText">The s text.</param>
        /// <param name="oRoot">The o root.</param>
        public SuckerMenuItem(string sLink, string sText, MenuHelperRoot oRoot, string cssClass, int iItemSeq, string sRoleId)
            : this(sLink, sText, oRoot, cssClass, string.Empty, iItemSeq, sRoleId)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SuckerMenuItem"/> class.
        /// </summary>
        /// <param name="sLink">The s link.</param>
        /// <param name="sText">The s text.</param>
        /// <param name="oRoot">The o root.</param>
        /// <param name="cssClass">The CSS class.</param>
        public SuckerMenuItem(string sLink, string sText, MenuHelperRoot oRoot, string cssClass, string sItemGroup, int iItemSeq,string sRoleId)
        {
            Link = sLink.StartsWith("~") ? VirtualPathUtility.ToAbsolute(sLink) : sLink;
            Text = sText;
            m_Root = oRoot;
            CssClass = cssClass;
            ItemGroup = sItemGroup;
            ItemSeq = iItemSeq;
            RoleID = sRoleId;
        }
        private MenuHelperRoot m_Root;
        public string Link = "";
        public string Text = "";
        public string CssClass = "";
        public string ItemGroup = "";
        public int ItemSeq ;
        public string RoleID = "";



        public System.Collections.Generic.List<SuckerMenuItem> Items = new System.Collections.Generic.List<SuckerMenuItem>();


        /// <summary>
        /// Gets a value indicating whether [recursive is current].
        /// </summary>
        /// <value><c>true</c> if [recursive is current]; otherwise, <c>false</c>.</value>
        public bool RecursiveIsCurrent
        {
            get
            {
                if (Link != "#" && m_Root.IsCurrent(this))
                    return true;
                return Items.Any(oItem => oItem.RecursiveIsCurrent);
            }
        }

        /// <summary>
        /// Gets the HTML.
        /// </summary>
        /// <returns></returns>
        public string GetHtml()
        {
            //Text = Text.Replace("»", "<span class=\"sf-sub-indicator\"> »</span>");
            var oBuilder = new System.Text.StringBuilder();
            oBuilder.AppendFormat("<li {0}>", !string.IsNullOrEmpty(CssClass) ? string.Format(@"class=""{0}""", CssClass) : string.Empty);
            if (Items.Count > 0)
            {
                oBuilder.AppendFormat(@"<a href=""{0}"">{1}</a>", Link, Text);
                oBuilder.Append("<ul>");
                foreach (var oItem in Items)
                    oBuilder.Append(oItem.GetHtml());
                oBuilder.Append("</ul>");
            }
            else
                oBuilder.AppendFormat(@"<a href=""{0}"">{1}</a>", Link, Text);
            oBuilder.Append("</li>");
            return oBuilder.ToString();
        }
    }
}

