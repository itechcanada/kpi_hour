﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

namespace iTECH.InbizERP.ECommerce
{
    public class ProcessSearchProduct
    {
        public DataTable SearchProduct(int catID, int subCatID, string lang, string whsCode, string txtSearch, bool externalPrdRef, bool exactMatch, int rowNo, int pageSize, out int totalRecords) {
            string sql = "Select SQL_CALC_FOUND_ROWS distinct p.*,i.pimgProdImagePath as prodImagePath FROM product p left join productmoreimages i on i.pimgProdID=p.prodID and i.pimgIsDefault=1 inner join category on catid=prodcategoryid and catstatus=1 inner join subcategory on subcatid=prodsubcategoryid and subcatstatus=1 WHERE prodIsActive=1 AND descLang = @Lang AND  prdWhsCode = @prdWhsCode";
            totalRecords = 0;
            List<MySqlParameter> pList = new List<MySqlParameter>();
            pList.Add(DbUtility.GetParameter("Lang", lang, MyDbType.String));
            pList.Add(DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String));
            if (catID <= 0 && subCatID <= 0 && !string.IsNullOrEmpty(txtSearch)) {
                if (exactMatch) {
                    sql += " AND (prodName = @txtSearch) ";
                }
                else 
                {
                    if (externalPrdRef) {
                        sql += " AND (externalProductRef  like CONCAT('%', @txtSearch, '%') OR prodName like CONCAT('%', @txtSearch, '%')) ";
                    }
                    else
                    {
                        sql += " AND (prodName LIKE CONCAT('%', @txtSearch, '%')) ";
                    }
                }

                pList.Add(DbUtility.GetParameter("txtSearch", txtSearch, MyDbType.String));
            }
            else if (catID > 0 && subCatID > 0) {
                sql += " AND prodCategoryId = @CatID AND prodSubCategoryId = @SubCatID ";
                pList.Add(DbUtility.GetParameter("CatID", catID, MyDbType.Int));
                pList.Add(DbUtility.GetParameter("SubCatID", subCatID, MyDbType.Int));
            }
            else if (catID > 0) {
                sql += " AND prodCategoryId = @CatID ";
                pList.Add(DbUtility.GetParameter("CatID", catID, MyDbType.Int));
            }
            else if (subCatID > 0) {
                sql += " AND prodSubCategoryId = @SubCatID ";
                pList.Add(DbUtility.GetParameter("SubCatID", subCatID, MyDbType.Int));
            }
            else
            {
                sql += " AND (prodName LIKE CONCAT(IFNULL(@txtSearch, ''), '%') OR prodDescription LIKE CONCAT(IFNULL(@txtSearch, ''), '%')) ";
                pList.Add(DbUtility.GetParameter("txtSearch", txtSearch, MyDbType.String));
            }

            sql += " ORDER BY prodName ";
            sql += " LIMIT @StartRow, @PageSize;";
            pList.Add(DbUtility.GetParameter("StartRow", rowNo, MyDbType.Int));
            pList.Add(DbUtility.GetParameter("PageSize", pageSize, MyDbType.Int));
            sql += " select found_rows();";

            DbHelper dbHelp = new DbHelper();
            try
            {
                DataSet ds = dbHelp.GetDataSet(sql, CommandType.Text, pList.ToArray());
                totalRecords = BusinessUtility.GetInt(ds.Tables[1].Rows[0][0]);

                return ds.Tables[0];
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
