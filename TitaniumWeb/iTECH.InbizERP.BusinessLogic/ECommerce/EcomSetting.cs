﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

namespace iTECH.InbizERP.ECommerce
{
    public class EcomSetting
    {
        private int _settingID;

        public int SettingID
        {
            get { return _settingID; }
            set { _settingID = value; }
        }
        private string _key;

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public static string GetValue(SettingKey key) {
            string sqlSelect = "SELECT e.Value FROM ecom_settings e WHERE e.Key = @Key";
            DbHelper dbHelp = new DbHelper();
            MySqlParameter[] p = { DbUtility.GetParameter("Key", key.ToString(), MyDbType.String) };
            try
            {
                object scalar = dbHelp.GetValue(sqlSelect, CommandType.Text, p);
                return BusinessUtility.GetString(scalar);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
