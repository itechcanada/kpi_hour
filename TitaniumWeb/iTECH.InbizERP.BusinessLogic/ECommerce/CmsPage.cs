﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.ECommerce
{
    public class CmsPage
    {
        private int _pageID;

        public int PageID
        {
            get { return _pageID; }
            set { _pageID = value; }
        }
        private int _templateID;

        public int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }
        private string _pageName;

        public string PageName
        {
            get { return _pageName; }
            set { _pageName = value; }
        }
        private string _pageTitleEn;

        public string PageTitleEn
        {
            get { return _pageTitleEn; }
            set { _pageTitleEn = value; }
        }
        private string _pageTitleFr;

        public string PageTitleFr
        {
            get { return _pageTitleFr; }
            set { _pageTitleFr = value; }
        }
        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        private bool _showInMenu;

        public bool ShowInMenu
        {
            get { return _showInMenu; }
            set { _showInMenu = value; }
        }
        private string _menuTextEn;

        public string MenuTextEn
        {
            get { return _menuTextEn; }
            set { _menuTextEn = value; }
        }
        private string _menuTextFr;

        public string MenuTextFr
        {
            get { return _menuTextFr; }
            set { _menuTextFr = value; }
        }
        private int _parentID;

        public int ParentID
        {
            get { return _parentID; }
            set { _parentID = value; }
        }
        private int _sortOrder;

        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }
        private DateTime _lastModifiedDate;

        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }
        private int _lastModifiedBy;

        public int LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }

        private string _templateName;

        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }
        private string _templatePath;

        public string TemplatePath
        {
            get { return _templatePath; }
            set { _templatePath = value; }
        }

        private PageLayout _layout;

        public PageLayout Layout
        {
            get { return _layout; }
            set { _layout = value; }
        }

        private CmsSidebar _leftSidebar;
        public CmsSidebar LeftSidebar {
            get {
                return _leftSidebar;
            }
        }

        private CmsSidebar _rightSidebar;
        public CmsSidebar RightSidebar
        {
            get
            {
                return _rightSidebar;
            }
        }

        public void PopulateObject(int pageid) {
            //At present only one cms site having ID = 1
            string sql = "SELECT p.PageID, p.TemplateID, p.PageName, p.PageTitleEn, p.PageTitleFr, p.IsActive, p.ShowInMenu, p.MenuTextEn, p.MenuTextFr, p.ParentID, p.SortOrder, p.LastModifiedDate, p.LastModifiedBy, t.TemplateName, t.TemplatePath, p.LayoutID FROM cms_pages p INNER JOIN cms_templates t ON t.TemplateID = p.TemplateID INNER JOIN cms_website w ON w.WebSiteID=p.WebSiteID  WHERE p.PageID = @PageID AND p.WebSiteID = 1";
            MySqlParameter[] p = { DbUtility.GetParameter("PageID", pageid, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, p);
                if (dr.Read())
                {
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.LastModifiedBy = BusinessUtility.GetInt(dr["LastModifiedBy"]);
                    this.LastModifiedDate = BusinessUtility.GetDateTime(dr["LastModifiedDate"]);
                    this.MenuTextEn = BusinessUtility.GetString(dr["MenuTextEn"]);
                    this.MenuTextFr = BusinessUtility.GetString(dr["MenuTextFr"]);
                    this.PageID = BusinessUtility.GetInt(dr["PageID"]);
                    this.PageName = BusinessUtility.GetString(dr["PageName"]);
                    this.PageTitleEn = BusinessUtility.GetString(dr["PageTitleEn"]);
                    this.PageTitleFr = BusinessUtility.GetString(dr["PageTitleFr"]);
                    this.ParentID = BusinessUtility.GetInt(dr["ParentID"]);
                    this.ShowInMenu = BusinessUtility.GetBool(dr["ShowInMenu"]);
                    this.SortOrder = BusinessUtility.GetInt(dr["SortOrder"]);
                    this.TemplateID = BusinessUtility.GetInt(dr["TemplateID"]);
                    this.TemplateName = BusinessUtility.GetString(dr["TemplateName"]);
                    this.TemplatePath = BusinessUtility.GetString(dr["TemplatePath"]);
                    this.Layout = (PageLayout)Enum.Parse(typeof(PageLayout), BusinessUtility.GetString(dr["LayoutID"]));
                }

                dr.Close();
                if (this.PageID > 0) {
                    _leftSidebar = new CmsSidebar();
                    _leftSidebar.SetPageSidebar(this.PageID, SideBarPosition.Left, dbHelp);
                    if (_leftSidebar.SidebarID <= 0) {
                        _leftSidebar.SetPageSidebar(0, SideBarPosition.Left, dbHelp);
                    }

                    _rightSidebar = new CmsSidebar();
                    _rightSidebar.SetPageSidebar(this.PageID, SideBarPosition.Right, dbHelp);
                    if (_rightSidebar.SidebarID <= 0)
                    {
                        _rightSidebar.SetPageSidebar(0, SideBarPosition.Right, dbHelp);
                    }
                }
                else
                {
                    _leftSidebar = new CmsSidebar();
                    _leftSidebar.SetPageSidebar(0, SideBarPosition.Left, dbHelp);

                    _rightSidebar = new CmsSidebar();
                    _rightSidebar.SetPageSidebar(0, SideBarPosition.Right, dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public DataTable GetPages() {
            string strSql = "SELECT * FROM cms_pages";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(strSql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
