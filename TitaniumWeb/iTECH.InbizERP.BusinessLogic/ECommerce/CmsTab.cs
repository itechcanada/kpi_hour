﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.ECommerce
{
    public class CmsTab
    {
        private int _tabID;

        public int TabID
        {
            get { return _tabID; }
            set { _tabID = value; }
        }
        private int _pageID;

        public int PageID
        {
            get { return _pageID; }
            set { _pageID = value; }
        }
        private string _tabContainerID;

        public string TabContainerID
        {
            get { return _tabContainerID; }
            set { _tabContainerID = value; }
        }
        private string _tabTextEn;

        public string TabTextEn
        {
            get { return _tabTextEn; }
            set { _tabTextEn = value; }
        }
        private string _tabTextFr;

        public string TabTextFr
        {
            get { return _tabTextFr; }
            set { _tabTextFr = value; }
        }
        private int _tabIndex;

        public int TabIndex
        {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }
        private DateTime _createdOn;

        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }
        private DateTime _modifiedOn;

        public DateTime ModifiedOn
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }
        private int _createdBy;

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        private int _modifiedBy;

        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        public int Insert() {            
            string sql = "INSERT INTO cms_page_tabs (PageID,TabContainerID,TabTextEn,TabTextFr,TabIndex,CreatedOn,CreatedBy,ModifiedOn,ModifiedBy) VALUES";
            sql += " (@PageID,@TabContainerID,@TabTextEn,@TabTextFr,@TabIndex,@CreatedOn,@CreatedBy,@ModifiedOn,@ModifiedBy)";
            DbHelper dbHelp = new DbHelper(true);
            int tabIndex = GetLastTabIndex(this.PageID, dbHelp) + 1;
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("ModifiedBy", this.ModifiedBy, MyDbType.Int),
                                     DbUtility.GetParameter("ModifiedOn", DateTime.Now, MyDbType.DateTime),
                                     DbUtility.GetParameter("PageID", this.PageID, MyDbType.Int),
                                     DbUtility.GetParameter("TabContainerID", this.TabContainerID, MyDbType.String),
                                     DbUtility.GetParameter("TabIndex", tabIndex, MyDbType.Int),
                                     DbUtility.GetParameter("TabTextEn", this.TabTextEn, MyDbType.String),
                                     DbUtility.GetParameter("TabTextFr", this.TabTextFr, MyDbType.String),
                                     DbUtility.GetParameter("CreatedBy", this.CreatedBy, MyDbType.Int),
                                     DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime)
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
                return dbHelp.GetLastInsertID();
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int tabID, int pageID) {
            string sqlDelete = "DELETE FROM cms_page_tabs WHERE TabID=@TabID;";
            string sqlUpdate = "SET @i := -1; UPDATE cms_page_tabs SET TabIndex  = (@i := @i + 1)  WHERE  PageID=@PageID ORDER BY  TabIndex;";

            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("TabID", tabID, MyDbType.Int)
                                 };
            MySqlParameter[] pUpdate = {
                                           DbUtility.GetParameter("PageID", pageID, MyDbType.Int)
                                       };

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, p) > 0) {
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, pUpdate);
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        private int GetLastTabIndex(int pageid, DbHelper dbHelp) {
            string sql = "SELECT MAX(TabIndex) FROM cms_page_tabs WHERE PageID = @PageID";
            MySqlParameter[] p = {
                                           DbUtility.GetParameter("PageID", pageid, MyDbType.Int)
                                       };
            object i = dbHelp.GetValue(sql, CommandType.Text, p);
            return BusinessUtility.GetInt(i);
        }
    }
}
