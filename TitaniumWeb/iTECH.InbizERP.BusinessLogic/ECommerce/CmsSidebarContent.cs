﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.ECommerce
{
    public class CmsSidebarContent
    {
        private int _contentID;

        public int ContentID
        {
            get { return _contentID; }
            set { _contentID = value; }
        }
        private int _sidebarID;

        public int SidebarID
        {
            get { return _sidebarID; }
            set { _sidebarID = value; }
        }
        private int _contentPlaceHolderID;

        public int ContentPlaceHolderID
        {
            get { return _contentPlaceHolderID; }
            set { _contentPlaceHolderID = value; }
        }

        private string _contentTitleEn;

        public string ContentTitleEn
        {
            get { return _contentTitleEn; }
            set { _contentTitleEn = value; }
        }

        private string _contentTitleFr;

        public string ContentTitleFr
        {
            get { return _contentTitleFr; }
            set { _contentTitleFr = value; }
        }

        private string _contentDescEn;

        public string ContentDescEn
        {
            get { return _contentDescEn; }
            set { _contentDescEn = value; }
        }
        private string _contentDescFr;

        public string ContentDescFr
        {
            get { return _contentDescFr; }
            set { _contentDescFr = value; }
        }

        private DateTime _lastModifiedDate;

        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }
        private int _lastModifiedBy;

        public int LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }

        public void Insert() {
            string sqlInsert = "INSERT INTO cms_sidebar_content (SidebarID,ContentPlaceHolderID, ContentTitleEn, ContentTitleFr,ContentDescEn,ContentDescFr,LastModifiedDate,LastModifiedBy) VALUES (@SidebarID,@ContentPlaceHolderID, @ContentTitleEn, @ContentTitleFr,@ContentDescEn,@ContentDescFr,@LastModifiedDate,@LastModifiedBy)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MySqlParameter[] pInsert = { 
                                           DbUtility.GetParameter("SidebarID", this.SidebarID, MyDbType.Int),
                                           DbUtility.GetParameter("ContentTitleEn", this.ContentTitleEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentTitleFr", this.ContentTitleFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescEn", this.ContentDescEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescFr", this.ContentDescFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentPlaceHolderID", this.ContentPlaceHolderID, MyDbType.Int),                                           
                                           DbUtility.GetParameter("LastModifiedBy", this.LastModifiedBy, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedDate", DateTime.Now, MyDbType.DateTime)
                                       };
                if (dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, pInsert) > 0)
                {
                    this._contentID = dbHelp.GetLastInsertID();
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update() {
            string sqlUpdate = "UPDATE cms_sidebar_content SET SidebarID=@SidebarID,ContentPlaceHolderID=@ContentPlaceHolderID, ContentTitleEn=@ContentTitleEn, ContentTitleFr=@ContentTitleFr,ContentDescEn=@ContentDescEn,ContentDescFr=@ContentDescFr,LastModifiedDate=@LastModifiedDate,LastModifiedBy=@LastModifiedBy WHERE ContentID = @ContentID";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MySqlParameter[] pUpdate = { 
                                           DbUtility.GetParameter("ContentID", this.ContentID, MyDbType.Int),
                                           DbUtility.GetParameter("SidebarID", this.SidebarID, MyDbType.Int),
                                           DbUtility.GetParameter("ContentTitleEn", this.ContentTitleEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentTitleFr", this.ContentTitleFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescEn", this.ContentDescEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescFr", this.ContentDescFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentPlaceHolderID", this.ContentPlaceHolderID, MyDbType.Int),                                           
                                           DbUtility.GetParameter("LastModifiedBy", this.LastModifiedBy, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedDate", DateTime.Now, MyDbType.DateTime)
                                       };
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, pUpdate);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public void PopulateObject(int contentid) {
            string sqlSelect = "SELECT p.ContentID, p.SidebarID, p.ContentPlaceHolderID, p.ContentTitleEn, p.ContentTitleFr, p.ContentDescEn, p.ContentDescFr, p.LastModifiedDate, p.LastModifiedBy FROM cms_sidebar_content p WHERE p.ContentID = @ContentID";

            MySqlParameter[] p = {  
                                     DbUtility.GetParameter("ContentID", contentid, MyDbType.Int)
                                 };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlSelect, CommandType.Text, p);
                if (dr.Read()) {
                    this.ContentTitleEn = BusinessUtility.GetString(dr["ContentTitleEn"]);
                    this.ContentTitleFr = BusinessUtility.GetString(dr["ContentTitleFr"]);
                    this.ContentDescEn = BusinessUtility.GetString(dr["ContentDescEn"]);
                    this.ContentDescFr = BusinessUtility.GetString(dr["ContentDescFr"]);
                    this.ContentID = BusinessUtility.GetInt(dr["ContentID"]);
                    this.ContentPlaceHolderID = BusinessUtility.GetInt(dr["ContentPlaceHolderID"]);                    
                    this.LastModifiedBy = BusinessUtility.GetInt(dr["LastModifiedBy"]);
                    this.LastModifiedDate = BusinessUtility.GetDateTime(dr["LastModifiedDate"]);
                    this.SidebarID = BusinessUtility.GetInt(dr["SidebarID"]);                    
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public int GetSidebarID(int pageid, SideBarPosition position) {
            string sql = "SELECT c.SidebarID FROM cms_page_sidebar c WHERE PageID=@PageID AND Position=@Position;";
            MySqlParameter[] p = { DbUtility.GetParameter("PageID", pageid, MyDbType.Int), DbUtility.GetParameter("Position", position.ToString(), MyDbType.String) };
            DbHelper dbHelp = new DbHelper();
            try
            {
                int sidebarID = 0;
                if (pageid > 0) {
                    sidebarID = BusinessUtility.GetInt(dbHelp.GetValue(sql, CommandType.Text, p));
                    if (sidebarID <= 0) {
                       sidebarID = GetSidebarID(0, position);
                    }
                }
                else
                {
                    sidebarID = BusinessUtility.GetInt(dbHelp.GetValue(sql, CommandType.Text, p));
                }

                return sidebarID;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InitializeContentTemplate(int sidebarid, Control userControl, string placeholderControlIDFormat, string contentTitleFormat) {
            string sqlContent = "SELECT p.ContentID, p.SidebarID, p.ContentPlaceHolderID, p.ContentTitleEn, p.ContentTitleFr, p.ContentDescEn, p.ContentDescFr, p.LastModifiedDate, p.LastModifiedBy FROM cms_sidebar_content p  WHERE p.SidebarID = @SidebarID";
            MySqlParameter[] p = { DbUtility.GetParameter("SidebarID", sidebarid, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlContent, CommandType.Text, p);
                while (dr.Read())
                {
                    HtmlGenericControl cph = (HtmlGenericControl)userControl.FindControl(string.Format(placeholderControlIDFormat, BusinessUtility.GetInt(dr["ContentPlaceHolderID"])));                    
                    if (cph != null)
                    {                        
                        cph.Attributes.Add("contentid", BusinessUtility.GetString(dr["ContentID"]));
                        if (!string.IsNullOrEmpty(contentTitleFormat) && !string.IsNullOrEmpty(BusinessUtility.GetString(dr["ContentTitleEn"])))
                        {
                            cph.InnerHtml += string.Format(contentTitleFormat, dr["ContentTitleEn"]);
                        }
                        cph.InnerHtml += System.Web.HttpContext.Current.Server.HtmlDecode(BusinessUtility.GetString(dr["ContentDescEn"]));
                    }                    
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }        

        public void Delete(int contentID) {
            string sql = "DELETE FROM cms_sidebar_content WHERE ContentID=@ContentID";
            MySqlParameter[] p = { DbUtility.GetParameter("ContentID", contentID, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
