﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.ECommerce
{
    public class CmsPageContent
    {
        private int _contentID;

        public int ContentID
        {
            get { return _contentID; }
            set { _contentID = value; }
        }
        private int _pageID;

        public int PageID
        {
            get { return _pageID; }
            set { _pageID = value; }
        }
        private int _contentPlaceHolderID;

        public int ContentPlaceHolderID
        {
            get { return _contentPlaceHolderID; }
            set { _contentPlaceHolderID = value; }
        }
        private string _contentDescEn;

        public string ContentDescEn
        {
            get { return _contentDescEn; }
            set { _contentDescEn = value; }
        }
        private string _contentDescFr;

        public string ContentDescFr
        {
            get { return _contentDescFr; }
            set { _contentDescFr = value; }
        }
        private int _displayOrder;

        public int DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }
        private DateTime _lastModifiedDate;

        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }
        private int _lastModifiedBy;

        public int LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }
        private string _image;

        public string Image
        {
            get { return _image; }
            set { _image = value; }
        }

        private string _contentTitleEn;
        public string ContentTitleEn
        {
            get { return _contentTitleEn; }
            set { _contentTitleEn = value; }
        }
        private string _contentTitleFr;
        public string ContentTitleFr
        {
            get { return _contentTitleFr; }
            set { _contentTitleFr = value; }
        }

        public void Insert()
        {
            string sqlInsert = "INSERT INTO cms_page_content (PageID,ContentTitleEn, ContentTitleFr,ContentPlaceHolderID,ContentDescEn,ContentDescFr,DisplayOrder,LastModifiedDate,LastModifiedBy,Image) VALUES (@PageID,@ContentTitleEn, @ContentTitleFr,@ContentPlaceHolderID,@ContentDescEn,@ContentDescFr,@DisplayOrder,@LastModifiedDate,@LastModifiedBy,@Image)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MySqlParameter[] pInsert = { 
                                           DbUtility.GetParameter("PageID", this.PageID, MyDbType.Int),
                                           DbUtility.GetParameter("ContentDescEn", this.ContentDescEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescFr", this.ContentDescFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentPlaceHolderID", this.ContentPlaceHolderID, MyDbType.Int),
                                           DbUtility.GetParameter("DisplayOrder", this.DisplayOrder, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedBy", this.LastModifiedBy, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedDate", DateTime.Now, MyDbType.DateTime),
                                           DbUtility.GetParameter("ContentTitleEn", this.ContentTitleEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentTitleFr", this.ContentTitleFr, MyDbType.String),
                                           DbUtility.GetParameter("Image", this.Image, MyDbType.String)
                                           
                                       };
                if (dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, pInsert) > 0)
                {
                    this._contentID = dbHelp.GetLastInsertID();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update()
        {
            string sqlUpdate = "UPDATE cms_page_content SET PageID=@PageID,ContentTitleEn=@ContentTitleEn, ContentTitleFr=@ContentTitleFr,ContentPlaceHolderID=@ContentPlaceHolderID,ContentDescEn=@ContentDescEn,ContentDescFr=@ContentDescFr,DisplayOrder=@DisplayOrder,LastModifiedDate=@LastModifiedDate,LastModifiedBy=@LastModifiedBy, Image=@Image WHERE ContentID = @ContentID";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MySqlParameter[] pUpdate = { 
                                           DbUtility.GetParameter("ContentID", this.ContentID, MyDbType.Int),
                                           DbUtility.GetParameter("PageID", this.PageID, MyDbType.Int),
                                           DbUtility.GetParameter("ContentDescEn", this.ContentDescEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentDescFr", this.ContentDescFr, MyDbType.String),
                                           DbUtility.GetParameter("ContentPlaceHolderID", this.ContentPlaceHolderID, MyDbType.Int),
                                           DbUtility.GetParameter("DisplayOrder", this.DisplayOrder, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedBy", this.LastModifiedBy, MyDbType.Int),
                                           DbUtility.GetParameter("LastModifiedDate", DateTime.Now, MyDbType.DateTime),
                                              DbUtility.GetParameter("ContentTitleEn", this.ContentTitleEn, MyDbType.String),
                                           DbUtility.GetParameter("ContentTitleFr", this.ContentTitleFr, MyDbType.String),
                                           DbUtility.GetParameter("Image", this.Image, MyDbType.String)
                                       };
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, pUpdate);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int contentid)
        {
            string sqlSelect = "SELECT p.ContentID, p.PageID,ContentTitleEn, ContentTitleFr, p.ContentPlaceHolderID, p.ContentDescEn, p.ContentDescFr, p.DisplayOrder, p.LastModifiedDate, p.LastModifiedBy, p.Image FROM cms_page_content p WHERE p.ContentID = @ContentID";

            MySqlParameter[] p = {  
                                     DbUtility.GetParameter("ContentID", contentid, MyDbType.Int)
                                 };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlSelect, CommandType.Text, p);
                if (dr.Read())
                {
                    this.ContentDescEn = BusinessUtility.GetString(dr["ContentDescEn"]);
                    this.ContentDescFr = BusinessUtility.GetString(dr["ContentDescFr"]);
                    this.ContentID = BusinessUtility.GetInt(dr["ContentID"]);
                    this.ContentPlaceHolderID = BusinessUtility.GetInt(dr["ContentPlaceHolderID"]);
                    this.DisplayOrder = BusinessUtility.GetInt(dr["DisplayOrder"]);
                    this.LastModifiedBy = BusinessUtility.GetInt(dr["LastModifiedBy"]);
                    this.LastModifiedDate = BusinessUtility.GetDateTime(dr["LastModifiedDate"]);
                    this.PageID = BusinessUtility.GetInt(dr["PageID"]);
                    this.Image = BusinessUtility.GetString(dr["Image"]);
                    this.ContentTitleEn = BusinessUtility.GetString(dr["ContentTitleEn"]);
                    this.ContentTitleFr = BusinessUtility.GetString(dr["ContentTitleFr"]);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public DataTable GetRepeatContentList(int pageid, int placeholderid)
        {
            string sqlSelect = "SELECT p.ContentID, p.PageID,ContentTitleEn, ContentTitleFr, p.ContentPlaceHolderID, p.ContentDescEn, p.ContentDescFr, p.DisplayOrder, p.LastModifiedDate, p.LastModifiedBy,Image FROM cms_page_content p WHERE p.PageID = @PageID AND p.ContentPlaceHolderID =@ContentPlaceHolderID";

            MySqlParameter[] p = {  
                                     DbUtility.GetParameter("PageID", pageid, MyDbType.Int),
                                     DbUtility.GetParameter("ContentPlaceHolderID", placeholderid, MyDbType.Int)
                                 };
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(sqlSelect, CommandType.Text, p);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InitializeContentTemplate(int pageid, Control userControl, string placeholderControlIDFormat)
        {
            string sqlContent = "SELECT p.ContentID,ContentTitleEn, ContentTitleFr, p.PageID, p.ContentPlaceHolderID, p.ContentDescEn, p.ContentDescFr, p.DisplayOrder, p.LastModifiedDate, p.LastModifiedBy,p.Image FROM cms_page_content p WHERE p.PageID = @PageID";
            MySqlParameter[] p = { DbUtility.GetParameter("PageID", pageid, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlContent, CommandType.Text, p);
                while (dr.Read())
                {
                    HtmlGenericControl cph = (HtmlGenericControl)userControl.FindControl(string.Format(placeholderControlIDFormat, BusinessUtility.GetInt(dr["ContentPlaceHolderID"])));
                    if (cph != null)
                    {
                        cph.Attributes.Add("contentid", BusinessUtility.GetString(dr["ContentID"]));
                        cph.InnerHtml += System.Web.HttpContext.Current.Server.HtmlDecode(BusinessUtility.GetString(dr["ContentDescEn"]));
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void InitializeContentTemplate(int pageid, Control userControl, string placeholderControlIDFormat, string lang)
        {
            string sqlContent = "";
            if (lang == "en")
                sqlContent += "SELECT p.ContentID, p.PageID, p.ContentPlaceHolderID, p.ContentDescEn AS ContentDesc, p.ContentDescFr, p.DisplayOrder, p.LastModifiedDate, p.LastModifiedBy,p.Image FROM cms_page_content p WHERE p.PageID = @PageID";
            else
                sqlContent += "SELECT p.ContentID, p.PageID, p.ContentPlaceHolderID, p.ContentDescEn, p.ContentDescFr  AS ContentDesc, p.DisplayOrder, p.LastModifiedDate, p.LastModifiedBy,p.Image FROM cms_page_content p WHERE p.PageID = @PageID";

            MySqlParameter[] p = { DbUtility.GetParameter("PageID", pageid, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlContent, CommandType.Text, p);
                while (dr.Read())
                {
                    HtmlGenericControl cph = (HtmlGenericControl)userControl.FindControl(string.Format(placeholderControlIDFormat, BusinessUtility.GetInt(dr["ContentPlaceHolderID"])));
                    if (cph != null)
                    {
                        cph.Attributes.Add("contentid", BusinessUtility.GetString(dr["ContentID"]));
                        cph.InnerHtml += System.Web.HttpContext.Current.Server.HtmlDecode(BusinessUtility.GetString(dr["ContentDesc"]));
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
        public void InitializePlaceHoderContent(int pageid, int placeHoderID, HtmlGenericControl placeHoder)
        {
            string sqlContent = "SELECT p.ContentID, p.ContentDescEn, p.ContentDescFr FROM cms_page_content p WHERE p.PageID = @PageID AND p.ContentPlaceHolderID =@ContentPlaceHolderID";
            MySqlParameter[] p = { DbUtility.GetParameter("PageID", pageid, MyDbType.Int), DbUtility.GetParameter("ContentPlaceHolderID", placeHoderID, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sqlContent, CommandType.Text, p);
                if (dr.Read())
                {
                    placeHoder.Attributes.Add("contentid", BusinessUtility.GetString(dr["ContentID"]));
                    placeHoder.InnerHtml += System.Web.HttpContext.Current.Server.HtmlDecode(BusinessUtility.GetString(dr["ContentDescEn"]));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void Delete(int contentID)
        {
            string sql = "DELETE FROM cms_page_content WHERE ContentID=@ContentID";
            MySqlParameter[] p = { DbUtility.GetParameter("ContentID", contentID, MyDbType.Int) };
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, p);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
