﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Web.UI.WebControls;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class EcomCategory
    {
        public int CatId { get; set; }
        public string CatDescLang { get; set; }
        public string CatName { get; set; }
        public bool CatIsActive { get; set; }
        public int CatUpdatedBy { get; set; }
        public DateTime CatUpdatedOn { get; set; }
        public string CatImagePath { get; set; }
        public string CatOffImagePath { get; set; }
        public string CatLandingPageBannerPath { get; set; }
        public int CatWebSeq { get; set; }
        public bool CatStatus { get; set; }
        public bool CatIsFreeText { get; set; }
        public string CatFreeText { get; set; }
        public string eBayCatgNo { get; set; }
        public bool IsCustom { get; set; }
        public bool IsStarterCategory { get; set; }

        public void Insert(int userID)
        {
            //string sqlGetMaxID = "SELECT MAX(catID) FROM category";
            string sql1 = "INSERT INTO category (catDescLang, catName, catIsActive, catUpdatedBy, catUpdatedOn, ";
            sql1 += " catWebSeq, catStatus, catIsFreeText, catFreeText, eBayCatgNo,IsCustom,IsStarterCategory) VALUES";
            sql1 += " (@catDescLang, @catName, @catIsActive, @catUpdatedBy, @catUpdatedOn, ";
            sql1 += " @catWebSeq, @catStatus, @catIsFreeText, @catFreeText, @eBayCatgNo,@IsCustom,@IsStarterCategory)";
            string sql2 = "INSERT INTO category (catID, catDescLang, catName, catIsActive, catUpdatedBy, catUpdatedOn, ";
            sql2 += " catWebSeq, catStatus, catIsFreeText, catFreeText, eBayCatgNo,IsCustom,IsStarterCategory) VALUES";
            sql2 += " (@catID, @catDescLang, @catName, @catIsActive, @catUpdatedBy, @catUpdatedOn, ";
            sql2 += " @catWebSeq, @catStatus, @catIsFreeText, @catFreeText, @eBayCatgNo,@IsCustom,@IsStarterCategory)";

            string sqlMaxSeq = "SELECT IFNULL(MAX(catWebSeq), 0) + 1 FROM category";

            DbHelper dbHelp = new DbHelper(true);

            try
            {
                dbHelp.ExecuteNonQuery(sql1, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catDescLang", "en", MyDbType.String),
                    DbUtility.GetParameter("catName", this.CatName, MyDbType.String),
                    DbUtility.GetParameter("catIsActive", this.CatIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("catUpdatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("catUpdatedOn", DateTime.Now, MyDbType.DateTime),                    
                    DbUtility.GetParameter("catWebSeq", this.CatWebSeq, MyDbType.Int),
                    DbUtility.GetParameter("catStatus", this.CatStatus, MyDbType.Boolean),
                    DbUtility.GetParameter("catIsFreeText", this.CatIsFreeText, MyDbType.Boolean),
                    DbUtility.GetParameter("catFreeText", this.CatFreeText, MyDbType.String),
                    DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String),
                    DbUtility.GetParameter("IsCustom", this.IsCustom, MyDbType.Boolean),
                    DbUtility.GetParameter("IsStarterCategory", this.IsStarterCategory, MyDbType.Boolean)
                });
                //object val = dbHelp.GetValue(sqlGetMaxID, CommandType.Text, null);
                this.CatId = dbHelp.GetLastInsertID();
                if (this.CatId > 0)
                {
                    dbHelp.ExecuteNonQuery(sql2, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("catID", this.CatId, MyDbType.Int),
                        DbUtility.GetParameter("catDescLang", "fr", MyDbType.String),
                        DbUtility.GetParameter("catName", this.CatName, MyDbType.String),
                        DbUtility.GetParameter("catIsActive", this.CatIsActive, MyDbType.Boolean),
                        DbUtility.GetParameter("catUpdatedBy", userID, MyDbType.Int),
                        DbUtility.GetParameter("catUpdatedOn", DateTime.Now, MyDbType.DateTime),                        
                        DbUtility.GetParameter("catWebSeq", this.CatWebSeq, MyDbType.Int),
                        DbUtility.GetParameter("catStatus", this.CatStatus, MyDbType.Boolean),
                        DbUtility.GetParameter("catIsFreeText", this.CatIsFreeText, MyDbType.Boolean),
                        DbUtility.GetParameter("catFreeText", this.CatFreeText, MyDbType.String),
                        DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String),
                        DbUtility.GetParameter("IsCustom", this.IsCustom, MyDbType.Boolean),
                        DbUtility.GetParameter("IsStarterCategory", this.IsStarterCategory, MyDbType.Boolean)
                    });

                    dbHelp.ExecuteNonQuery(sql2, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("catID", this.CatId, MyDbType.Int),
                        DbUtility.GetParameter("catDescLang", "sp", MyDbType.String),
                        DbUtility.GetParameter("catName", this.CatName, MyDbType.String),
                        DbUtility.GetParameter("catIsActive", this.CatIsActive, MyDbType.Boolean),
                        DbUtility.GetParameter("catUpdatedBy", userID, MyDbType.Int),
                        DbUtility.GetParameter("catUpdatedOn", DateTime.Now, MyDbType.DateTime),                        
                        DbUtility.GetParameter("catWebSeq", this.CatWebSeq, MyDbType.Int),
                        DbUtility.GetParameter("catStatus", this.CatStatus, MyDbType.Boolean),
                        DbUtility.GetParameter("catIsFreeText", this.CatIsFreeText, MyDbType.Boolean),
                        DbUtility.GetParameter("catFreeText", this.CatFreeText, MyDbType.String),
                        DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String),
                        DbUtility.GetParameter("IsCustom", this.IsCustom, MyDbType.Boolean),
                        DbUtility.GetParameter("IsStarterCategory", this.IsStarterCategory, MyDbType.Boolean)
                    });

                    object maxSeq = dbHelp.GetValue(sqlMaxSeq, CommandType.Text, null);
                    this.UpdateSeq(dbHelp, this.CatId, BusinessUtility.GetInt(maxSeq));
                }
                //this.CatId = dbHelp.GetLastInsertID();               
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InsertImage(int catID, string imgPath)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.InsertImage(dbHelp, catID, imgPath, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InsertImage(DbHelper dbHelp, int catID, string imgPath, string lang)
        {
            string sql = "UPDATE category SET catImagePath=@catImagePath WHERE catID=@catID  ";
            if (!string.IsNullOrEmpty(lang))
            {
                sql += " AND catDescLang=@Lang";
            }
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catImagePath", imgPath, MyDbType.String),
                    DbUtility.GetParameter("catID", catID, MyDbType.Int),
                    DbUtility.GetParameter("Lang", lang, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void RemoveImage(int catID, string lang)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.InsertImage(dbHelp, catID, null, lang);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(int userID)
        {
            string sqlLangSpecific = "UPDATE category SET catName = @catName, catFreeText =@catFreeText WHERE catID=@catID AND catDescLang=@catDescLang";
            string sqlLangIndependent = "UPDATE category ";
            sqlLangIndependent += " SET catIsActive =@catIsActive, catUpdatedBy =@catUpdatedBy, ";
            sqlLangIndependent += " catUpdatedOn =@catUpdatedOn, ";
            sqlLangIndependent += " catStatus =@catStatus, catIsFreeText=@catIsFreeText, eBayCatgNo=@eBayCatgNo, IsCustom=@IsCustom,IsStarterCategory=@IsStarterCategory";
            if (!string.IsNullOrEmpty(this.CatImagePath))
            {
                sqlLangIndependent += ",catImagePath=@catImagePath";
            }
            sqlLangIndependent += " WHERE catID=@catID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlLangSpecific, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catName", this.CatName, MyDbType.String),
                    DbUtility.GetParameter("catFreeText", this.CatFreeText, MyDbType.String),
                    DbUtility.GetParameter("catID", this.CatId, MyDbType.Int),
                    DbUtility.GetParameter("catDescLang",  this.CatDescLang, MyDbType.String)
                });

                dbHelp.ExecuteNonQuery(sqlLangIndependent, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catID", this.CatId, MyDbType.Int),                                        
                    DbUtility.GetParameter("catIsActive", this.CatIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("catUpdatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("catUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("catImagePath", this.CatImagePath, MyDbType.String),                    
                    DbUtility.GetParameter("catStatus", this.CatStatus, MyDbType.Boolean),
                    DbUtility.GetParameter("catIsFreeText", this.CatIsFreeText, MyDbType.Boolean),                    
                    DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String),
                    DbUtility.GetParameter("IsCustom", this.IsCustom, MyDbType.Boolean),
                    DbUtility.GetParameter("IsStarterCategory", this.IsStarterCategory, MyDbType.Boolean)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSeq(int catID, int seq)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.UpdateSeq(dbHelp, catID, seq);
            }
            catch
            {                
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSeq(DbHelper dbHelp, int catID, int seq)
        {
            string sql = string.Format("UPDATE category SET catWebSeq={0} WHERE catID={1}", seq, catID);
            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            
        }

        public void Delete(int catID)
        {
            string sql = "UPDATE category SET catStatus=0, catIsActive=0 WHERE catId=@catID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catID", catID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int id, string lang)
        {
            string sql = "SELECT * FROM category WHERE catID=@catID AND catDescLang=@catDescLang";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catID", id, MyDbType.Int),
                    DbUtility.GetParameter("catDescLang", lang, MyDbType.String)
                });
                if (dr.Read())
                {
                    this.CatDescLang = BusinessUtility.GetString(dr["catDescLang"]);
                    this.CatFreeText = BusinessUtility.GetString(dr["catFreeText"]);
                    this.CatId = BusinessUtility.GetInt(dr["catId"]);
                    this.CatImagePath = BusinessUtility.GetString(dr["catImagePath"]);
                    this.CatIsActive = BusinessUtility.GetBool(dr["catIsActive"]);
                    this.CatIsFreeText = BusinessUtility.GetBool(dr["catIsFreeText"]);
                    this.CatLandingPageBannerPath = BusinessUtility.GetString(dr["catLandingPageBannerPath"]);
                    this.CatName = BusinessUtility.GetString(dr["catName"]);
                    this.CatOffImagePath = BusinessUtility.GetString(dr["catOffImagePath"]);
                    this.CatStatus = BusinessUtility.GetBool(dr["catStatus"]);
                    this.CatUpdatedBy = BusinessUtility.GetInt(dr["catUpdatedBy"]);
                    this.CatUpdatedOn = BusinessUtility.GetDateTime(dr["catUpdatedOn"]);
                    this.CatWebSeq = BusinessUtility.GetInt(dr["catWebSeq"]);
                    this.eBayCatgNo = BusinessUtility.GetString(dr["eBayCatgNo"]);
                    this.IsCustom = BusinessUtility.GetBool(dr["IsCustom"]);
                    this.IsStarterCategory = BusinessUtility.GetBool(dr["IsStarterCategory"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(ParameterCollection pCol, string searchKey, string lang)
        {
            pCol.Clear();
            string sql = "Select catID,catName,concat(userFirstName,' ',UserLastName) as UserName, CASE catIsActive WHEN 1 THEN 'green.gif' WHEN 2 THEN 'orange.gif' ELSE 'red.gif' END as catIsActive ,catStatus,catUpdatedOn,";
            sql += " case catImagePath when '' then '../images/nopic.jpg' else catImagePath end as catImagePath, ";
            sql += " catWebSeq, eBayCatgNo from category  inner join  users on  users.userid=category.catUpdatedBy where catStatus='1' and catDescLang =@Lang";
            if (!string.IsNullOrEmpty(searchKey))
            {
                sql += " AND catName LIKE CONCAT('%', @Search ,'%')";
                pCol.Add("@Search", DbType.String, searchKey);
            }
            sql += "  ORDER BY catWebSeq";
            pCol.Add("@Lang", lang);
            return sql;
        }

        public void FillDropDown(DropDownList ddl, ListItem rootItem, string lang)
        {
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader("SELECT * FROM category WHERE catIsActive=1 AND catDescLang=@catDescLang", CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("catDescLang", lang, MyDbType.String)
                });
                ddl.DataSource = dr;
                ddl.DataTextField = "catName";
                ddl.DataValueField = "catId";
                ddl.DataBind();

                if (rootItem != null)
                {
                    ddl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}
