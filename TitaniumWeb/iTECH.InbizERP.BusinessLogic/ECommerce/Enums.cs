﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.ECommerce
{    
    public enum SettingKey {
        WebSiteTitle = 1
    }

    public enum SideBarPosition { 
        Left = 1,
        Right =2
    }

    public enum PageLayout { 
        Default = 1,
        TwoColumn = 2,
        FullWidth = 3
    }

    public struct DefaultConstatns
    {
        public const int DefaultPageID = 1;
    }
}
