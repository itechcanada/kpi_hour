﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.ECommerce
{
    public class CmsSidebar
    {
        private int _sidebarID;

        public int SidebarID
        {
            get { return _sidebarID; }
            set { _sidebarID = value; }
        }
        private int _templateID;

        public int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }
        private string _templatePath;

        public string TemplatePath
        {
            get { return _templatePath; }            
        }

        private string _sidebarName;

        public string SidebarName
        {
            get { return _sidebarName; }
            set { _sidebarName = value; }
        }
        private DateTime _lastModifiedDate;

        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }
        private int _lastModifiedBy;

        public int LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }

        public void SetPageSidebar(int pageid, SideBarPosition position, DbHelper dbHelp) {
            string sql = "SELECT c.`SidebarID`, c.`TemplateID`, c.`SidebarName`, ct.TemplatePath FROM cms_sidebar c INNER JOIN cms_page_sidebar cp ON cp.SidebarID=c.SidebarID ";
            sql += " INNER JOIN cms_templates ct ON ct.TemplateID=c.TemplateID WHERE cp.PageID=@PageID AND cp.Position = @Position";
            MySqlParameter[] p ={
                                    DbUtility.GetParameter("PageID", pageid, MyDbType.Int),
                                    DbUtility.GetParameter("Position", position.ToString(), MyDbType.String)
                              };
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, p);
                if (dr.Read()) {
                    this._sidebarID = BusinessUtility.GetInt(dr["SidebarID"]);
                    this._templateID = BusinessUtility.GetInt(dr["TemplateID"]);
                    this._templatePath = BusinessUtility.GetString(dr["TemplatePath"]);
                }
                dr.Close();
            }
            catch 
            {
                
                throw;
            }
        }
    }
}
