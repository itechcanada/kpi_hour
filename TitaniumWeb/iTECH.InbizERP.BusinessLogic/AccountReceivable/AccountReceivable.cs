﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class AccountReceivable
    {
        public int AccountReceivableID { get; set; }
        public int ARInvoiceNo { get; set; }
        public double ARAmtRcvd { get; set; }
        public DateTime ARAmtRcvdDateTime { get; set; }
        public int ARAmtRcvdVia { get; set; }
        public int ARRcvdBy { get; set; }
        public string ARContactNm { get; set; }
        public string ARContactPhone { get; set; }
        public string ARContactEmail { get; set; }
        public string ARCreditCardNo { get; set; }
        public string ARCreditCardExp { get; set; }
        public string ARCreditCardCVVCode { get; set; }
        public string ARCreditCardLastName { get; set; }
        public string ARCreditCardFirstName { get; set; }
        public string ARChequeNo { get; set; }
        public string ARNote { get; set; }
        public bool ARColAsigned { get; set; }
        public double ARWriteOff { get; set; }
        public string ARReceiptNo { get; set; }
        public string Reason { get; set; }

        public void Insert(DbHelper dbHelp, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO accountreceivable (ARInvoiceNo, ARAmtRcvd,ARAmtRcvdDateTime,ARAmtRcvdVia,ARRcvdBy,ARContactNm,ARContactPhone,";
            sql += " ARContactEmail,ARCreditCardNo,ARCreditCardExp,ARCreditCardCVVCode,ARCreditCardLastName,ARCreditCardFirstName,ARChequeNo,ARNote,";
            sql += " ARColAsigned, ARWriteOff,ARReceiptNo, Reason) VALUES(@ARInvoiceNo, @ARAmtRcvd,@ARAmtRcvdDateTime,@ARAmtRcvdVia,@ARRcvdBy,@ARContactNm,@ARContactPhone,";
            sql += " @ARContactEmail,@ARCreditCardNo,@ARCreditCardExp,@ARCreditCardCVVCode,@ARCreditCardLastName,@ARCreditCardFirstName,@ARChequeNo,@ARNote,";
            sql += " @ARColAsigned, @ARWriteOff,@ARReceiptNo, @Reason)";

            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ARInvoiceNo", this.ARInvoiceNo, MyDbType.Int),
                    DbUtility.GetParameter("ARAmtRcvd", this.ARAmtRcvd, MyDbType.Double),
                    DbUtility.GetParameter("ARAmtRcvdDateTime", this.ARAmtRcvdDateTime, MyDbType.DateTime),
                    DbUtility.GetParameter("ARAmtRcvdVia", this.ARAmtRcvdVia, MyDbType.Int),
                    DbUtility.GetParameter("ARRcvdBy", this.ARRcvdBy, MyDbType.Int),
                    DbUtility.GetParameter("ARContactNm", this.ARContactNm, MyDbType.String),
                    DbUtility.GetParameter("ARContactPhone", this.ARContactPhone, MyDbType.String),
                    DbUtility.GetParameter("ARContactEmail", this.ARContactEmail, MyDbType.String),
                    DbUtility.GetParameter("ARCreditCardNo", this.ARCreditCardNo, MyDbType.String),
                    DbUtility.GetParameter("ARCreditCardExp", this.ARCreditCardExp, MyDbType.String),
                    DbUtility.GetParameter("ARCreditCardCVVCode", this.ARCreditCardCVVCode, MyDbType.String),
                    DbUtility.GetParameter("ARCreditCardLastName", this.ARCreditCardLastName, MyDbType.String),
                    DbUtility.GetParameter("ARCreditCardFirstName", this.ARCreditCardFirstName, MyDbType.String),
                    DbUtility.GetParameter("ARChequeNo", this.ARChequeNo, MyDbType.String),
                    DbUtility.GetParameter("ARNote", this.ARNote, MyDbType.String),
                    DbUtility.GetParameter("ARColAsigned", this.ARColAsigned, MyDbType.Boolean),
                    DbUtility.GetParameter("ARWriteOff", this.ARWriteOff, MyDbType.Double),
                    DbUtility.GetParameter("ARReceiptNo", this.ARReceiptNo, MyDbType.String),
                    DbUtility.GetParameter("Reason", this.Reason, MyDbType.String)
                });

                this.AccountReceivableID = dbHelp.GetLastInsertID();

                //Update slsAnalysis record
                if (this.ARAmtRcvdVia == (int)StatusAmountReceivedVia.WriteOff && this.ARWriteOff > 0 && this.ARAmtRcvd <= 0) //Update Writeoff entry
                {
                    new SlsAnalysis().UpdateOnPaymentWriteOff(dbHelp, this.ARInvoiceNo, this.ARWriteOff, this.ARAmtRcvdDateTime, this.ARRcvdBy);
                }
                else //Normal case of receiving or refund
                {
                    new SlsAnalysis().UpdateOnPaymentReceived(dbHelp, this.ARInvoiceNo, this.ARAmtRcvd, this.ARAmtRcvdDateTime);
                }

                //Need to update invoice status chck outstanding amount
                Invoice inv = new Invoice();
                TotalSummary ts = CalculationHelper.GetInvoiceTotal(dbHelp, this.ARInvoiceNo);
                if (ts.OutstandingAmount <= 0)
                {
                    inv.UpdateStatus(dbHelp, this.ARInvoiceNo, InvoicesStatus.PAYMENT_RECEIVED, userid);
                }
                else
                {
                    inv.UpdateStatus(dbHelp, this.ARInvoiceNo, InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, userid);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<int> GetInvoiceCustomersByCompany(int compayID)
        {
            //string sql = "SELECT DISTINCT  invCustID FROM invoices WHERE invStatus = @invStatus  AND  invCompanyID = @invCompanyID";
            string sql = "SELECT DISTINCT  invCustID FROM invoices WHERE  invCompanyID = @invCompanyID";
            List<int> lstCust = new List<int>();
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            MySqlParameter[] p = { 
                                     //DbUtility.GetParameter("invStatus", InvoicesStatus.P.ToString(), MyDbType.String),
                                     DbUtility.GetParameter("invCompanyID", compayID, MyDbType.Int)
                                 };
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, p);
                while (dr.Read())
                {
                    lstCust.Add(BusinessUtility.GetInt(dr["invCustID"]));
                }

                return lstCust;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public double GetWriteOffAmount(int invID)
        {
            string sql = "SELECT if(sum(ARWriteOff) Is Null,0, sum(ARWriteOff)) as WriteOffAmount FROM accountreceivable where ARInvoiceNo=@ARInvoiceNo  group by ARInvoiceNo ";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ARInvoiceNo", invID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(o);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string AssignTo(int invID)
        {
            string sql = "SELECT concat(u.userFirstName,' ',u.userLastName) as AssignedTo FROM invoices o inner join invoiceitems i on ";
            sql += " i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on ";
            sql += " a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo ";
            sql += " where o.invStatus<>'P' and o.invID=@invID  group by o.invID, a.ARInvoiceNo";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                });
                return BusinessUtility.GetString(o);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int invID)
        {
            pCol.Clear();
            string sql = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, ";
            sql += "  PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, ";
            sql += " funGetInvoiceTotal(o.invID) AS InvoiceTotal, funGetReceivedAmountByInvoiceID(o.invID) AS ReceivedAmount, (funGetInvoiceTotal(o.invID) - funGetReceivedAmountByInvoiceID(o.invID)) AS BalanceAmount,";
            sql += " a.*, pMode.sysAppDesc AS Mode FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID  ";
            sql += " inner join accountreceivable a on a.ARInvoiceNo=o.invID  and o.invID=@invID ";

            sql += " INNER JOIN  (  ";
            sql += " SELECT sysAppDesc,sysAppLogicCode  ";
            sql += " FROM sysstatus where  sysAppCodeLang =@lang and sysAppCodeActive=1 and sysAppPfx='AR' and sysAppCode='dlRcv'  ";
            sql += " order by sysAppCodeSeq) AS pMode On pMode.sysAppLogicCode =  a.ARAmtRcvdVia ";

            pCol.Add("invID", invID.ToString());
            pCol.Add("lang", Globals.CurrentAppLanguageCode);
            return sql;
        }


        public DataTable GetInvoicePartialPaymentList(int invID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            
            string sql = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, ";
            sql += "  PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, ";
            sql += " funGetInvoiceTotal(o.invID) AS InvoiceTotal, funGetReceivedAmountByInvoiceID(o.invID) AS ReceivedAmount, (funGetInvoiceTotal(o.invID) - funGetReceivedAmountByInvoiceID(o.invID)) AS BalanceAmount,";
            sql += " a.*, pMode.sysAppDesc AS Mode FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID  ";
            sql += " inner join accountreceivable a on a.ARInvoiceNo=o.invID  and o.invID=@invID ";
            sql += " INNER JOIN  (  ";
            sql += " SELECT sysAppDesc,sysAppLogicCode  ";
            sql += " FROM sysstatus where  sysAppCodeLang =@lang and sysAppCodeActive=1 and sysAppPfx='AR' and sysAppCode='dlRcv'  ";
            sql += " order by sysAppCodeSeq) AS pMode On pMode.sysAppLogicCode =  a.ARAmtRcvdVia ";

            try
            {
                DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("invID", invID, MyDbType.Int),
                    DbUtility.GetParameter("lang", Globals.CurrentAppLanguageCode, MyDbType.String)
                });


                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetInvoicePartialPaymentList(int invID, string langID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, ";
            sql += "  PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, ";
            sql += " funGetInvoiceTotal(o.invID) AS InvoiceTotal, funGetReceivedAmountByInvoiceID(o.invID) AS ReceivedAmount, (funGetInvoiceTotal(o.invID) - funGetReceivedAmountByInvoiceID(o.invID)) AS BalanceAmount,";
            sql += " a.*, pMode.sysAppDesc AS Mode FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID  ";
            sql += " inner join accountreceivable a on a.ARInvoiceNo=o.invID  and o.invID=@invID ";
            sql += " INNER JOIN  (  ";
            sql += " SELECT sysAppDesc,sysAppLogicCode  ";
            sql += " FROM sysstatus where  sysAppCodeLang =@lang and sysAppCodeActive=1 and sysAppPfx='AR' and sysAppCode='dlRcv'  ";
            sql += " order by sysAppCodeSeq) AS pMode On pMode.sysAppLogicCode =  a.ARAmtRcvdVia ";

            try
            {
                DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("invID", invID, MyDbType.Int),
                    DbUtility.GetParameter("lang", langID, MyDbType.String)
                });


                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, string searchField, string searchText, string days, int invID, int salesRepID, bool isSalesRestricted)
        {
            pCol.Clear();
            string sql = "";
            if (isSalesRestricted)
            {
                sql += "SELECT DISTINCT o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName  AS CustomerName, ";
                sql += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') AS invDate, PartnerInvoiceNetTerms AS NetTerms, ";
                sql += " sum(invProductQty * invProductUnitPrice*invCurrencyExRate) AS InvAmount, ";
                sql += " invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, ";
                sql += " CONCAT(u.userFirstName,' ',u.userLastName) AS AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType,o.invCustPO, ";
                sql += " funGetInvoiceTotal(o.invID) AS InvoiceTotal, funGetReceivedAmountByInvoiceID(o.invID) AS ReceivedAmount, CASE o.invTypeCommission WHEN 5 THEN 0 ELSE (funGetInvoiceTotal(o.invID) - funGetReceivedAmountByInvoiceID(o.invID)) END AS BalanceAmount,";
                sql += string.Format("(SELECT t.TransactionID FROM z_order_transaction t WHERE t.OrderID=@OrderID AND IFNULL(t.TransactionID, '')<>'' AND t.TransactionTypeID={0} AND t.TransactionStatusID={1})", (int)TransactionType.Sale, (int)TransactionStatus.Success);
                sql += " AS GatewayTransactionID";
                sql += " FROM invoices o INNER JOIN salesrepcustomer s ON s.CustomerID=o.invCustID AND s.CustomerType=o.invCustType AND s.UserID=@UserID ";
                sql += " INNER JOIN invoiceitems i ON i.invoices_invID=o.invID LEFT JOIN partners  ON partners.PartnerID =o.invCustID LEFT JOIN ";
                sql += " accountreceivable a ON a.ARInvoiceNo=o.invID LEFT JOIN collection c ON c.ColInvID=o.invID LEFT JOIN users u ON u.userID=c.ColUserAssignedTo ";
                pCol.Add("@UserID", salesRepID.ToString());
            }
            else
            {
                sql += "SELECT DISTINCT o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName  AS CustomerName, ";
                sql += " DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, ";
                sql += " sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, ";
                sql += " invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, ";
                sql += " o.invCurrencyExRate, o.invRefType,o.invCustPO,";
                sql += " funGetInvoiceTotal(o.invID) AS InvoiceTotal, funGetReceivedAmountByInvoiceID(o.invID) AS ReceivedAmount, CASE o.invTypeCommission WHEN 5 THEN 0 ELSE (funGetInvoiceTotal(o.invID) - funGetReceivedAmountByInvoiceID(o.invID)) END AS BalanceAmount,";
                sql += string.Format("(SELECT t.TransactionID FROM z_order_transaction t WHERE t.OrderID=@OrderID AND IFNULL(t.TransactionID, '')<>'' AND t.TransactionTypeID={0} AND t.TransactionStatusID={1})", (int)TransactionType.Sale, (int)TransactionStatus.Success);
                sql += " AS GatewayTransactionID";
                sql += " FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  ";
                sql += " ON partners.PartnerID =o.invCustID LEFT JOIN accountreceivable a ON a.ARInvoiceNo=o.invID LEFT JOIN collection c ON c.ColInvID=o.invID ";
                sql += " LEFT JOIN users u ON u.userID=c.ColUserAssignedTo ";
            }
            sql += " where 1=1 ";
            if (invID > 0)
            {
                sql += " AND o.invID=@InvID";
                pCol.Add("@InvID", invID.ToString());
            }
            if (searchField == SearchFieldAccountReceivable.INVOICE_NO && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(days))
            {
                sql += " AND  o.invID=@Search";
                pCol.Add("@Search", searchText);

                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }
            else if (searchField == SearchFieldAccountReceivable.ORDER_NO && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(days))
            {
                sql += " AND  o.invForOrderNo=@Search";
                pCol.Add("@Search", searchText);

                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }
            else if ((searchField == SearchFieldAccountReceivable.CUSTOMER_NAME || searchField == SearchFieldAccountReceivable.CUSTOMER_ACRONYM) && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(days))
            {
                sql += " AND  (PartnerLongName LIKE CONCAT('%',@Search ,'%') OR PartnerAcronyme LIKE CONCAT('%',@Search ,'%'))";
                pCol.Add("@Search", searchText);

                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }
            else if (searchField == SearchFieldAccountReceivable.CUSTOMER_PO && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(days))
            {
                sql += " AND  (o.invCustPO LIKE CONCAT('%',@Search ,'%'))";
                pCol.Add("@Search", searchText);

                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }//
            else if (searchField == SearchFieldAccountReceivable.INVOICE_NO && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(days))
            {
                sql += " AND  o.invID=@Search";
                pCol.Add("@Search", searchText);
            }
            else if (searchField == SearchFieldAccountReceivable.ORDER_NO && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(days))
            {
                sql += " AND  o.invForOrderNo=@Search";
                pCol.Add("@Search", searchText);
            }
            else if ((searchField == SearchFieldAccountReceivable.CUSTOMER_NAME || searchField == SearchFieldAccountReceivable.CUSTOMER_ACRONYM) && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(days))
            {
                sql += " AND  (PartnerLongName LIKE CONCAT('%',@Search ,'%') OR PartnerAcronyme LIKE CONCAT('%',@Search ,'%'))";
                pCol.Add("@Search", searchText);
            }
            else if (searchField == SearchFieldAccountReceivable.CUSTOMER_PO && !string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(days))
            {
                sql += " AND  (o.invCustPO LIKE CONCAT('%',@Search ,'%'))";
                pCol.Add("@Search", searchText);
            }//
            else if (string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(days))
            {
                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }
            else if (!string.IsNullOrEmpty(days))
            {
                sql += " AND " + this.CreatedDateRageSqlSnipt(days);
            }
            sql += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn,o.invID desc,  CustomerName ";
            return sql;
        }

        private string CreatedDateRageSqlSnipt(string rangeKey)
        {
            string strSql = string.Empty;
            if (rangeKey == "10")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 10)  and o.invCreatedOn < now())";
            }
            else if (rangeKey == "15")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 15)  and o.invCreatedOn < SUBDATE(now(), 10))";
            }
            else if (rangeKey == "30")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 30)  and o.invCreatedOn < SUBDATE(now(), 15))";
            }
            else if (rangeKey == "60")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 60)  and o.invCreatedOn < SUBDATE(now(), 30))";
            }
            else if (rangeKey == "90")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 90)  and o.invCreatedOn < SUBDATE(now(), 60))";
            }
            else if (rangeKey == "180")
            {
                strSql += "(o.invCreatedOn >= SUBDATE(now(), 180)  and o.invCreatedOn < SUBDATE(now(), 90))";
            }
            else
            {
                strSql += "(o.invCreatedOn < SUBDATE(now(), 180))";
            }
            return strSql;
        }


        public void Delete(int arcID)
        {
            string sql = "DELETE FROM accountreceivable WHERE AccountReceivableID=@AccountReceivableID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("AccountReceivableID", arcID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
