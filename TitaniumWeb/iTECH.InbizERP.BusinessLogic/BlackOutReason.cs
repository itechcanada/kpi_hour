﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class BlackOutReason
    {
        public int ID { get; set; }
        public string ReasonEn { get; set; }
        public string ReasonFr { get; set; }
        public bool IsActive { get; set; }

        public void Insert()
        {
            string sqlInsert = "INSERT INTO z_blackout_reasons (ReasonEn,ReasonFr,IsActive) VALUES(@ReasonEn,@ReasonFr,@IsActive)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReasonEn", this.ReasonEn, MyDbType.String),
                    DbUtility.GetParameter("ReasonFr", this.ReasonFr, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean)
                });
                this.ID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update()
        {
            string sqlUpdate = "UPDATE z_blackout_reasons SET ReasonEn=@ReasonEn,ReasonFr=@ReasonFr,IsActive=@IsActive WHERE ID=@ID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReasonEn", this.ReasonEn, MyDbType.String),
                    DbUtility.GetParameter("ReasonFr", this.ReasonFr, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("ID", this.ID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int id)
        {
            string sqlUpdate = "DELETE FROM  z_blackout_reasons WHERE ID=@ID ";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] {                    
                    DbUtility.GetParameter("ID", id, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql()
        {
            return "SELECT * FROM z_blackout_reasons";
        }
    }
}
