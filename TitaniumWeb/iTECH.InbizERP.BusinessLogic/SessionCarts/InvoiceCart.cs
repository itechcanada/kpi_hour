﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using System.Data;

using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    public class InvoiceCartHelper
    {
        #region Helper Members

        public static void InitCart(DbHelper dbHelp, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                InvoiceCart cart = new InvoiceCart();
                cart.BindCart(dbHelp, invID);
                HttpContext.Current.Session["CURRENT_INVOICE_CART"] = cart;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }            
        }

        public static InvoiceCart CurrentCart
        {
            get
            {
                if (HttpContext.Current.Session["CURRENT_INVOICE_CART"] != null)
                {
                    return (InvoiceCart)HttpContext.Current.Session["CURRENT_INVOICE_CART"];
                }
                return new InvoiceCart();
            }
            private set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("CURRENT_INVOICE_CART");
                }
                else
                {
                    HttpContext.Current.Session["CURRENT_INVOICE_CART"] = value;
                }
            }
        }                       
        #endregion

        public static TotalSummary GetOrderTotal()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return CalculationHelper.GetInvoiceTotal(dbHelp, CurrentCart.InvoiceID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void Dispose()
        {
            CurrentCart = null;
        }        
    }

    public class InvoiceCart
    {
        public List<InvoiceCartEntity> Items { get; private set; }
        public List<InvoiceProcessEntity> ProcessItems { get; private set; }

        public InvoiceCart()
        {
            this.Items = new List<InvoiceCartEntity>();
            this.ProcessItems = new List<InvoiceProcessEntity>();
        }

        public int OrderID { get; set; }
        public int InvoiceID { get; set; }
        public int ReservationID { get; set; }
        public double InvDiscount { get; set; }
        public string InvDiscountType { get; set; }
        public double ExchangeRate { get; set; }
        public int InvCustomerID { get; set; }
        public int InvCompanyID { get; set; }
        public string InvCustomerType { get; set; }
        public int InvGuestType { get; set; }
        public string InvCurrencyCode { get; set; }
        public string InvWarehouseCode { get; set; }
        public string InvCustomerName { get; set; }        

        public void BindCart(DbHelper dbHelp, int invID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            
            try
            {
                this.BindItems(dbHelp, invID);
                this.BindProcessCart(dbHelp, invID);
                string sqlExchangeRate = "SELECT o.invCurrencyExRate, o.invCustID, o.invCustType, o.invCompanyID, o.invCurrencyCode, o.invShpWhsCode, p.PartnerLongName, ce.GuestType, o.invDiscount, o.invDiscountType, o.invForOrderNo  FROM invoices o";
                sqlExchangeRate += " INNER JOIN partners p ON p.PartnerID=o.invCustID LEFT OUTER JOIN z_customer_extended ce ON ce.PartnerID=p.PartnerID";
                sqlExchangeRate += " where o.invID=@invID";
                using (IDataReader r = dbHelp.GetDataReader(sqlExchangeRate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("invID", invID, MyDbType.Int)
                }))
                {
                    if (r.Read())
                    {
                        this.OrderID = BusinessUtility.GetInt(r["invForOrderNo"]);
                        this.ExchangeRate = BusinessUtility.GetDouble(r["invCurrencyExRate"]);
                        this.InvCustomerID = BusinessUtility.GetInt(r["invCustID"]);
                        this.InvCustomerType = BusinessUtility.GetString(r["invCustType"]);
                        this.InvGuestType = BusinessUtility.GetInt(r["GuestType"]);
                        this.InvCompanyID = BusinessUtility.GetInt(r["invCompanyID"]);
                        this.InvCurrencyCode = BusinessUtility.GetString(r["invCurrencyCode"]);
                        this.InvWarehouseCode = BusinessUtility.GetString(r["invShpWhsCode"]);
                        this.InvCustomerName = BusinessUtility.GetString(r["PartnerLongName"]);
                        this.InvDiscount = BusinessUtility.GetDouble(r["invDiscount"]);
                        this.InvDiscountType = BusinessUtility.GetString(r["invDiscountType"]);
                    }
                }               

                if (this.ExchangeRate <= 0)
                {
                    this.ExchangeRate = 1.00D;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        private void BindItems(DbHelper dbHelp, int invID)
        {
            InvoiceItems invItems = new InvoiceItems();
            IDataReader r = null;
            try
            {
                r = invItems.GetInvoiceItemsReader(dbHelp, invID);
                int iCount = 1;
                while (r.Read())
                {
                    InvoiceCartEntity en = new InvoiceCartEntity();
                    en.ID = iCount;
                    en.ProductID = BusinessUtility.GetInt(r["invProductID"]);
                    en.UPCCode = BusinessUtility.GetString(r["prdUPCCode"]);
                    en.ProductName = BusinessUtility.GetString(r["prdName"]);
                    en.Quantity = BusinessUtility.GetDouble(r["invProductQty"]);
                    en.Price = BusinessUtility.GetDouble(r["IninvProductUnitPrice"]);
                    en.RangePrice = 0;
                    en.WarehouseCode = BusinessUtility.GetString(r["invShpWhsCode"]);
                    en.TaxGrp = BusinessUtility.GetInt(r["invProductTaxGrp"]);
                    en.TaxGrpDesc = BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                    en.Discount = BusinessUtility.GetInt(r["invProductDiscount"]);
                    en.DisType = BusinessUtility.GetString(r["invProductDiscountType"]);
                    en.ItemTotal = BusinessUtility.GetDouble(r["amount"]);
                    en.InvoiceItemID = BusinessUtility.GetInt(r["invItemID"]);
                    en.GuestID = BusinessUtility.GetInt(r["invGuestID"]);
                    en.ReservationID = BusinessUtility.GetInt(r["ReservationID"]);
                    en.ReservationItemID = BusinessUtility.GetInt(r["invReservationItemID"]);
                    en.InvoiceID = BusinessUtility.GetInt(invID);
                    //en.OrderID = BusinessUtility.GetInt(r["ordID"]);
                    //en.IsCanceled = BusinessUtility.GetBool(r["ordIsItemCanceled"]);
                    //en.OriginalPrdName = BusinessUtility.GetString(r["OriginalPrdName"]);
                    //en.ProductType = BusinessUtility.GetInt(r["ProductType"]);
                    if (en.GuestID > 0)
                    {
                        en.ItemGuestName = BusinessUtility.GetString(r["PartnerLongName"]);
                    }
                    if (en.ReservationID > 0 && this.ReservationID <= 0)
                    {
                        this.ReservationID = en.ReservationID;
                    }
                    
                    this.Items.Add(en);
                    iCount++;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (r != null && !r.IsClosed)
                {
                    r.Close();
                    r.Dispose();
                }
            }
        }

        private void BindProcessCart(DbHelper dbHelp, int invID)
        {
            InvoiceItemProcess invProcessItem = new InvoiceItemProcess();
            IDataReader r = null;
            try
            {
                r = invProcessItem.GetProcessItemsReader(dbHelp, invID);
                int iCount = 1;
                while (r.Read())
                {
                    InvoiceProcessEntity en = new InvoiceProcessEntity();
                    en.ProcessCode = BusinessUtility.GetString(r["invItemProcCode"]);
                    en.TaxGrpID = BusinessUtility.GetInt(r["sysTaxCodeDescID"]);
                    en.TaxGrpName = BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                    en.ProcessDescription = BusinessUtility.GetString(r["ProcessDescription"]);
                    en.ProcessFixedCost = BusinessUtility.GetDouble(r["IninvItemProcFixedPrice"]);
                    en.ProcessCostPerHour = BusinessUtility.GetDouble(r["IninvItemProcPricePerHour"]);
                    en.ProcessCostPerUnit = BusinessUtility.GetDouble(r["IninvItemProcPricePerUnit"]);
                    en.TotalHour = BusinessUtility.GetInt(r["invItemProcHours"]);
                    en.TotalUnit = BusinessUtility.GetInt(r["invItemProcUnits"]);
                    en.ProcessTotal = BusinessUtility.GetDouble(r["ProcessCost"]);
                    en.ProcessItemID = BusinessUtility.GetInt(r["invItemProcID"]);
                    en.InvoiceID = BusinessUtility.GetInt(r["invoices_invID"]);
                    en.ProcessID = iCount;

                    this.ProcessItems.Add(en);
                    iCount++;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (r != null && !r.IsClosed)
                {
                    r.Close();
                    r.Dispose();
                }
            }
        }
    }

    public class InvoiceCartEntity
    {
        public int ID { get; set; }
        public string UPCCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double RangePrice { get; set; }
        public string WarehouseCode { get; set; }
        public int TaxGrp { get; set; }
        public string TaxGrpDesc { get; set; }
        public int Discount { get; set; }
        public string DisType { get; set; }
        public double ItemTotal { get; set; }
        public int ProductID { get; set; }
        public int InvoiceID { get; set; }
        public int InvoiceItemID { get; set; }
        public int GuestID { get; set; }
        public int ReservationID { get; set; }
        public int ReservationItemID { get; set; }
        public string ItemGuestName { get; set; }
        //public int OrderItemID { get; set; }
        //public int OrderID { get; set; }        
        //public bool IsCanceled { get; set; }
        //public string OriginalPrdName { get; set; }
        //public int ProductType { get; set; }
    }

    public class InvoiceProcessEntity
    {
        public int ProcessID { get; set; }
        public string ProcessCode { get; set; }
        public int TaxGrpID { get; set; }
        public string TaxGrpName { get; set; }
        public string ProcessDescription { get; set; }
        public double ProcessFixedCost { get; set; }
        public double ProcessCostPerHour { get; set; }
        public double ProcessCostPerUnit { get; set; }
        public int TotalHour { get; set; }
        public int TotalUnit { get; set; }
        public double ProcessTotal { get; set; }
        public int ProcessItemID { get; set; }
        public int InvoiceID { get; set; }
    }
}
