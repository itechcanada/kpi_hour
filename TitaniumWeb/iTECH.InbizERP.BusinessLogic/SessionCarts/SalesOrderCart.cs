﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using System.Data;

using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;


namespace iTECH.InbizERP.BusinessLogic
{
    public class SalesCartHelper
    {
        #region Helper Members

        public static void InitCart(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                SalesOrderCart cart = new SalesOrderCart();
                cart.BindCart(dbHelp, orderID);
                HttpContext.Current.Session["CURRENT_SALES_CART"] = cart;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
            
        }

        public static void InitLocalCart(DbHelper dbHelp, string whsCode, int customerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                SalesOrderCart cart = new SalesOrderCart();
                cart.BindLocalCart(dbHelp, whsCode, customerID);
                HttpContext.Current.Session["CURRENT_SALES_CART"] = cart;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }            
        }

        public static SalesOrderCart CurrentCart
        {
            get
            {
                if (HttpContext.Current.Session["CURRENT_SALES_CART"] != null)
                {
                    return (SalesOrderCart)HttpContext.Current.Session["CURRENT_SALES_CART"];
                }
                return new SalesOrderCart();
            }
            private set {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("CURRENT_SALES_CART");
                }
                else
                {
                    HttpContext.Current.Session["CURRENT_SALES_CART"] = value;
                }                
            }
        }

        public static void SetOrderDiscount(double discount, string discountType)
        {
            SalesOrderCart cart = CurrentCart;
            cart.OrdDiscount = discount;
            cart.OrdDiscountType = discountType;

            CurrentCart = cart;
        }

        #region Items
        public static void AddToCart(SalesCartEntity item, bool findRow)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ProductID == item.ProductID && findRow == true select fi;

            DbHelper dbHelp = new DbHelper(true);
            Partners objPartnr = new Partners();
            objPartnr.PopulateObject(dbHelp, BusinessUtility.GetInt(CurrentCart.OrderCustomerID));
            string ordCurrencyCode = objPartnr.PartnerCurrencyCode;

            SysCurrencies cur = new SysCurrencies();
            cur.PopulateObject(ordCurrencyCode);
            double customerCurrencyRelativePrice = BusinessUtility.GetDouble(cur.CurrencyRelativePrice);



            if (foundItemCol.Count() == 1)
            {
                SalesCartEntity foundItem = foundItemCol.First();
                foundItem.TaxGrp = item.TaxGrp;
                foundItem.TaxGrpDesc = item.TaxGrpDesc;
                foundItem.Quantity = foundItem.Quantity + item.Quantity;
                foundItem.Price = item.Price;
                foundItem.OrderItemID = item.OrderItemID;                

                int discount = item.Discount;
                //double total = CalculationHelper.GetAmount(cart.ExchangeRate * (foundItem.Price * (double)foundItem.Quantity));
                double total = CalculationHelper.GetAmount(customerCurrencyRelativePrice * (foundItem.Price * (double)foundItem.Quantity));
                if (discount > 0)
                {
                    if (item.DisType.ToUpper().Trim() == "A")
                    {
                        total = CalculationHelper.GetAmount(total - discount);
                    }
                    else
                    {
                        //total = total - Math.Round((total * ((double)discount / 100)), 2);
                        total = CalculationHelper.GetAmount(total - (total * ((double)discount / 100)));
                    }
                }



                foundItem.ItemTotal = (total  );
                //foundItem.ItemTotal = total;

                //Update OrderItem In Database 
                if (item.OrderItemID > 0)
                {
                    OrderItems oi = new OrderItems();
                    oi.OrderItemID = foundItem.OrderItemID;
                    oi.OrderItemDesc = foundItem.ProductName;
                    oi.OrdProductDiscount = foundItem.Discount;
                    oi.OrdProductDiscountType = foundItem.DisType;
                    oi.OrdProductQty = foundItem.Quantity;
                    oi.OrdProductTaxGrp = foundItem.TaxGrp;
                    oi.OrdProductUnitPrice = foundItem.Price;

                    oi.Update(null);

                    foundItem.TaxGrp = oi.OrdProductTaxGrp;
                    foundItem.ItemTaxes = new Tax();
                    foundItem.ItemTaxes.Tax1 = oi.Tax1;
                    foundItem.ItemTaxes.Tax2 = oi.Tax1;
                    foundItem.ItemTaxes.Tax3 = oi.Tax1;
                    foundItem.ItemTaxes.Tax4 = oi.Tax1;
                    foundItem.ItemTaxes.Tax5 = oi.Tax1;
                    foundItem.ItemTaxes.TaxDesc1 = oi.TaxDesc1;
                    foundItem.ItemTaxes.TaxDesc2 = oi.TaxDesc2;
                    foundItem.ItemTaxes.TaxDesc3 = oi.TaxDesc3;
                    foundItem.ItemTaxes.TaxDesc4 = oi.TaxDesc4;
                    foundItem.ItemTaxes.TaxDesc5 = oi.TaxDesc5;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated1;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated2;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated3;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated4;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated5;
                    foundItem.ItemTaxes.TaxGroupID = oi.OrdProductTaxGrp;
                }
                else
                { 
                    //Update taxes in cart
                    foundItem.ItemTaxes = new Tax();
                    if (foundItem.TaxGrp <= 0)
                    {
                        foundItem.TaxGrp = TaxCalculationHelper.GetTaxGroup(null, foundItem.ProductID, cart.OrderCustomerID, cart.OrderWarehouseCode);
                    }
                    foundItem.ItemTaxes.PopulateObject(null, foundItem.TaxGrp, foundItem.ItemTotal);
                }
            }
            else
            {
                int discount = item.Discount;
                //double total = CalculationHelper.GetAmount(cart.ExchangeRate * (item.Price * item.Quantity));
                double total = CalculationHelper.GetAmount(customerCurrencyRelativePrice * (item.Price * item.Quantity));
                if (discount > 0)
                {
                    if (item.DisType.ToUpper().Trim() == "A")
                    {
                        total = CalculationHelper.GetAmount(total - discount);
                    }
                    else
                    {
                        //total = total - Math.Round((total * ((double)discount / 100)), 2);
                        total = CalculationHelper.GetAmount(total - (total * ((double)discount / 100)));
                    }
                }
                item.ItemTotal = total;
                if (cart.Items.Count > 0)
                {
                    item.ID = cart.Items.Max(maxid => maxid.ID) + 1;   
                }
                else
                {
                    item.ID = 1;
                }

                Product prd = new Product();
                prd.PopulateObject(item.ProductID);

                item.GuestID = CurrentCart.OrderCustomerID;
                item.ItemGuestName = CurrentCart.OrderCustomerName;
                item.ProductType = prd.PrdType;
                item.OriginalPrdName = prd.PrdName;
                cart.Items.Add(item);

                //Add Order Item To Database                
                if (cart.OrderID > 0)
                {
                    OrderItems oi = new OrderItems();
                    oi.OrderItemDesc = item.ProductName;
                    oi.OrdProductDiscount = item.Discount;
                    oi.OrdProductDiscountType = item.DisType;
                    oi.OrdProductQty = item.Quantity;
                    oi.OrdProductTaxGrp = item.TaxGrp;
                    oi.OrdProductUnitPrice = item.Price;
                    oi.OrdID = cart.OrderID;
                    oi.OrdIsItemCanceled = false;
                    oi.OrdProductID = item.ProductID;
                    oi.OrdGuestID = item.GuestID;

                    oi.Insert(null);

                    item.TaxGrp = oi.OrdProductTaxGrp;
                    item.ItemTaxes = new Tax();
                    item.ItemTaxes.Tax1 = oi.Tax1;
                    item.ItemTaxes.Tax2 = oi.Tax1;
                    item.ItemTaxes.Tax3 = oi.Tax1;
                    item.ItemTaxes.Tax4 = oi.Tax1;
                    item.ItemTaxes.Tax5 = oi.Tax1;
                    item.ItemTaxes.TaxDesc1 = oi.TaxDesc1;
                    item.ItemTaxes.TaxDesc2 = oi.TaxDesc2;
                    item.ItemTaxes.TaxDesc3 = oi.TaxDesc3;
                    item.ItemTaxes.TaxDesc4 = oi.TaxDesc4;
                    item.ItemTaxes.TaxDesc5 = oi.TaxDesc5;
                    item.ItemTaxes.TaxCalculated1 = oi.TaxCalculated1;
                    item.ItemTaxes.TaxCalculated1 = oi.TaxCalculated2;
                    item.ItemTaxes.TaxCalculated1 = oi.TaxCalculated3;
                    item.ItemTaxes.TaxCalculated1 = oi.TaxCalculated4;
                    item.ItemTaxes.TaxCalculated1 = oi.TaxCalculated5;
                    item.ItemTaxes.TaxGroupID = oi.OrdProductTaxGrp;
                }
                else
                {
                    //Update taxes in cart
                    item.ItemTaxes = new Tax();
                    if (item.TaxGrp <= 0)
                    {
                        item.TaxGrp = TaxCalculationHelper.GetTaxGroup(null, item.ProductID, cart.OrderCustomerID, cart.OrderWarehouseCode);
                    }                    
                    item.ItemTaxes.PopulateObject(null, item.TaxGrp, item.ItemTotal);
                }
            }

            CurrentCart = cart;
        }

        public static void EditCartItem(SalesCartEntity item)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ProductID == item.ProductID select fi;
            if (foundItemCol.Count() > 0)
            {
                SalesCartEntity foundItem = foundItemCol.First();
                foundItem.ProductID = item.ProductID;
                foundItem.Discount = item.Discount;
                foundItem.Price = item.Price;
                foundItem.ProductName = item.ProductName;
                foundItem.Quantity = item.Quantity;
                foundItem.UPCCode = item.UPCCode;
                foundItem.TaxGrp = item.TaxGrp;
                foundItem.TaxGrpDesc = item.TaxGrpDesc;
                foundItem.WarehouseCode = item.WarehouseCode;
                foundItem.DisType = item.DisType;
                foundItem.OrderItemID = item.OrderItemID;
                int discount = item.Discount;
                double total = CalculationHelper.GetAmount(cart.ExchangeRate * (foundItem.Price * foundItem.Quantity));
                if (discount > 0)
                {
                    if (item.DisType.ToUpper().Trim() == "A")
                    {
                        total = CalculationHelper.GetAmount(total - discount);
                    }
                    else
                    {
                        //total = total - Math.Round((total * ((double)discount / 100)), 2);
                        total = CalculationHelper.GetAmount(total - (total * ((double)discount / 100)));
                    }
                }
                foundItem.ItemTotal = total;

                //Update OrderItem In Database                 
                if (item.OrderItemID > 0)
                {
                    OrderItems oi = new OrderItems();
                    oi.OrderItemID = foundItem.OrderItemID;
                    oi.OrderItemDesc = foundItem.ProductName;
                    oi.OrdProductDiscount = foundItem.Discount;
                    oi.OrdProductDiscountType = foundItem.DisType;
                    oi.OrdProductQty = foundItem.Quantity;
                    oi.OrdProductTaxGrp = foundItem.TaxGrp;
                    oi.OrdProductUnitPrice = foundItem.Price;

                    oi.Update(null);
                    foundItem.TaxGrp = oi.OrdProductTaxGrp;
                    foundItem.ItemTaxes = new Tax();
                    foundItem.ItemTaxes.Tax1 = oi.Tax1;
                    foundItem.ItemTaxes.Tax2 = oi.Tax1;
                    foundItem.ItemTaxes.Tax3 = oi.Tax1;
                    foundItem.ItemTaxes.Tax4 = oi.Tax1;
                    foundItem.ItemTaxes.Tax5 = oi.Tax1;
                    foundItem.ItemTaxes.TaxDesc1 = oi.TaxDesc1;
                    foundItem.ItemTaxes.TaxDesc2 = oi.TaxDesc2;
                    foundItem.ItemTaxes.TaxDesc3 = oi.TaxDesc3;
                    foundItem.ItemTaxes.TaxDesc4 = oi.TaxDesc4;
                    foundItem.ItemTaxes.TaxDesc5 = oi.TaxDesc5;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated1;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated2;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated3;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated4;
                    foundItem.ItemTaxes.TaxCalculated1 = oi.TaxCalculated5;
                    foundItem.ItemTaxes.TaxGroupID = oi.OrdProductTaxGrp;
                }
                else
                {
                    //Update taxes in cart
                    foundItem.ItemTaxes = new Tax();
                    if (foundItem.TaxGrp <= 0)                    
                    {
                        foundItem.TaxGrp = TaxCalculationHelper.GetTaxGroup(null, foundItem.ProductID, cart.OrderCustomerID, cart.OrderWarehouseCode);                        
                    }
                    foundItem.ItemTaxes.PopulateObject(null, foundItem.TaxGrp, foundItem.ItemTotal);
                }

                

                CurrentCart = cart;
            }
        }

        public static void DeleteCartItem(int id)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ID == id select fi;
            if (foundItemCol.Count() > 0)
            {
                var fI = foundItemCol.First();
                if (fI.OrderItemID > 0)
                {
                    new OrderItems().Delete(fI.OrderItemID);
                }
                cart.Items.Remove(fI);
            }
            CurrentCart = cart;
        }

        public static SalesCartEntity GetItem(int id)
        {
            var foundItemCol = from fi in CurrentCart.Items where fi.ID == id select fi;
            if (foundItemCol.Count() > 0)
            {
                return foundItemCol.First();
            }
            return null;
        }

        public static void SetItemAsCanceled(int orderItemID)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.OrderItemID == orderItemID select fi;
            if (foundItemCol.Count() == 1)
            {
                SalesCartEntity first = foundItemCol.First();
                first.Price = 0.0D;
                first.ItemTotal = 0.0D;
                first.IsCanceled = true;
            }
            CurrentCart = cart;
        }

        #endregion

        #region Process Items
        public static void EditToProcessCart(SalesProcessEntity item)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.ProcessItems where fi.ProcessID == item.ProcessID select fi;
            if (foundItemCol.Count() > 0)
            {
                SalesProcessEntity foundItem = foundItemCol.First();
                foundItem.ProcessCode = item.ProcessCode;
                foundItem.TaxGrpID = item.TaxGrpID;
                foundItem.TaxGrpName = item.TaxGrpName;
                foundItem.ProcessDescription = item.ProcessDescription;
                foundItem.ProcessFixedCost = item.ProcessFixedCost;
                foundItem.ProcessCostPerHour = item.ProcessCostPerHour;
                foundItem.ProcessCostPerUnit = item.ProcessCostPerUnit;
                foundItem.TotalHour = item.TotalHour;
                foundItem.TotalUnit = item.TotalUnit;
                foundItem.ProcessInternalCost = item.ProcessInternalCost; //Added by mukesh 20130523
                foundItem.ProcessTotal = CalculationHelper.GetAmount(cart.ExchangeRate * (foundItem.ProcessFixedCost + (foundItem.ProcessCostPerHour * foundItem.TotalHour) + (foundItem.ProcessCostPerUnit * foundItem.TotalUnit)));

                //To Do to save in db
                if (item.ProcessItemID > 0)
                {
                    OrderItemProcess proce = new OrderItemProcess();
                    proce.OrdID = item.OrderID;
                    proce.OrdItemProcCode = item.ProcessCode;
                    proce.OrdItemProcFixedPrice = item.ProcessFixedCost;
                    proce.OrdItemProcHours = item.TotalHour;
                    proce.OrdItemProcPricePerHour = item.ProcessCostPerHour;
                    proce.OrdItemProcPricePerUnit = item.ProcessCostPerUnit;
                    proce.OrdItemProcUnits = item.TotalUnit;
                    proce.OrdItemProcInternalCost = item.ProcessInternalCost; //Added by mukesh 20130523
                    proce.SysTaxCodeDescID = item.TaxGrpID;
                    proce.OrderItemProcID = item.ProcessItemID;
                    proce.Update(null, item.ProcessItemID);
                    foundItem.TaxGrpID = proce.SysTaxCodeDescID; //Re populate tax group

                    foundItem.ItemTaxes = new Tax();
                    foundItem.ItemTaxes.Tax1 = proce.Tax1;
                    foundItem.ItemTaxes.Tax2 = proce.Tax1;
                    foundItem.ItemTaxes.Tax3 = proce.Tax1;
                    foundItem.ItemTaxes.Tax4 = proce.Tax1;
                    foundItem.ItemTaxes.Tax5 = proce.Tax1;
                    foundItem.ItemTaxes.TaxDesc1 = proce.TaxDesc1;
                    foundItem.ItemTaxes.TaxDesc2 = proce.TaxDesc2;
                    foundItem.ItemTaxes.TaxDesc3 = proce.TaxDesc3;
                    foundItem.ItemTaxes.TaxDesc4 = proce.TaxDesc4;
                    foundItem.ItemTaxes.TaxDesc5 = proce.TaxDesc5;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated1;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated2;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated3;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated4;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated5;
                    foundItem.ItemTaxes.TaxGroupID = proce.SysTaxCodeDescID;
                }
                else
                {
                    //Update taxes in cart
                    foundItem.ItemTaxes = new Tax();
                    if (foundItem.TaxGrpID <= 0)
                    {
                        foundItem.TaxGrpID = TaxCalculationHelper.GetTaxGroup(null, -1, -1, cart.OrderWarehouseCode);
                    }
                    foundItem.ItemTaxes.PopulateObject(null, foundItem.TaxGrpID, foundItem.ProcessTotal);
                }               
            }

            CurrentCart = cart;
        }

        public static void AddToProcessCart(SalesProcessEntity item)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.ProcessItems where fi.ProcessCode == item.ProcessCode select fi;
            
            if (foundItemCol.Count() > 0)
            {
                SalesProcessEntity foundItem = foundItemCol.First();

                foundItem.ProcessCode = item.ProcessCode;
                foundItem.TaxGrpID = item.TaxGrpID;
                foundItem.TaxGrpName = item.TaxGrpName;
                foundItem.ProcessDescription = item.ProcessDescription;
                foundItem.ProcessFixedCost = item.ProcessFixedCost;
                foundItem.ProcessCostPerHour = item.ProcessCostPerHour;
                foundItem.ProcessCostPerUnit = item.ProcessCostPerUnit;
                foundItem.TotalHour = item.TotalHour;
                foundItem.TotalUnit = item.TotalUnit;
                foundItem.ProcessInternalCost = item.ProcessInternalCost; //Added by mukesh 20130523
                foundItem.ProcessTotal = CalculationHelper.GetAmount(cart.ExchangeRate * (foundItem.ProcessFixedCost + (foundItem.ProcessCostPerHour * foundItem.TotalHour) + (foundItem.ProcessCostPerUnit * foundItem.TotalUnit))); 

                //To Do to save in db
                if (cart.OrderID > 0 && item.ProcessItemID > 0) //Ensure that record was exists in db.
                {
                    OrderItemProcess proce = new OrderItemProcess();
                    proce.OrdID = item.OrderID;
                    proce.OrdItemProcCode = item.ProcessCode;
                    proce.OrdItemProcFixedPrice = item.ProcessFixedCost;
                    proce.OrdItemProcHours = item.TotalHour;
                    proce.OrdItemProcPricePerHour = item.ProcessCostPerHour;
                    proce.OrdItemProcPricePerUnit = item.ProcessCostPerUnit;
                    proce.OrdItemProcUnits = item.TotalUnit;
                    proce.OrdItemProcInternalCost = item.ProcessInternalCost; //Added by mukesh 20130523
                    proce.SysTaxCodeDescID = item.TaxGrpID;
                    proce.OrderItemProcID = item.ProcessItemID;
                    proce.Update(null, item.ProcessItemID);
                    foundItem.TaxGrpID = proce.SysTaxCodeDescID; //Re populate tax group

                    foundItem.ItemTaxes = new Tax();
                    foundItem.ItemTaxes.Tax1 = proce.Tax1;
                    foundItem.ItemTaxes.Tax2 = proce.Tax1;
                    foundItem.ItemTaxes.Tax3 = proce.Tax1;
                    foundItem.ItemTaxes.Tax4 = proce.Tax1;
                    foundItem.ItemTaxes.Tax5 = proce.Tax1;
                    foundItem.ItemTaxes.TaxDesc1 = proce.TaxDesc1;
                    foundItem.ItemTaxes.TaxDesc2 = proce.TaxDesc2;
                    foundItem.ItemTaxes.TaxDesc3 = proce.TaxDesc3;
                    foundItem.ItemTaxes.TaxDesc4 = proce.TaxDesc4;
                    foundItem.ItemTaxes.TaxDesc5 = proce.TaxDesc5;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated1;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated2;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated3;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated4;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated5;
                    foundItem.ItemTaxes.TaxGroupID = proce.SysTaxCodeDescID;
                }
                else
                {
                    //Update taxes in cart
                    foundItem.ItemTaxes = new Tax();
                    if (foundItem.TaxGrpID <= 0)
                    {
                        foundItem.TaxGrpID = TaxCalculationHelper.GetTaxGroup(null, -1, -1, cart.OrderWarehouseCode);
                    }
                    foundItem.ItemTaxes.PopulateObject(null, foundItem.TaxGrpID, foundItem.ProcessTotal);
                }
            }
            else
            {
                SalesProcessEntity foundItem = new SalesProcessEntity();
                foundItem.ProcessCode = item.ProcessCode;
                foundItem.TaxGrpID = item.TaxGrpID;
                foundItem.TaxGrpName = item.TaxGrpName;
                foundItem.ProcessDescription = item.ProcessDescription;
                foundItem.ProcessFixedCost = item.ProcessFixedCost;
                foundItem.ProcessCostPerHour = item.ProcessCostPerHour;
                foundItem.ProcessCostPerUnit = item.ProcessCostPerUnit;
                foundItem.TotalHour = item.TotalHour;
                foundItem.TotalUnit = item.TotalUnit;
                foundItem.ProcessInternalCost = item.ProcessInternalCost; //Added by mukesh 20130523
                foundItem.ProcessTotal = CalculationHelper.GetAmount(cart.ExchangeRate * (foundItem.ProcessFixedCost + (foundItem.ProcessCostPerHour * foundItem.TotalHour) + (foundItem.ProcessCostPerUnit * foundItem.TotalUnit))); 
                foundItem.OrderID = item.OrderID;
                if (cart.ProcessItems.Count > 0)
                {
                    foundItem.ProcessID = cart.ProcessItems.Max(maxid => maxid.ProcessID) + 1;   
                }
                else
                {
                    foundItem.ProcessID = 1;
                }

                //To Do to save in db
                if (cart.OrderID > 0)
                {
                    OrderItemProcess proce = new OrderItemProcess();
                    proce.OrdID = cart.OrderID;
                    proce.OrdItemProcCode = foundItem.ProcessCode;
                    proce.OrdItemProcFixedPrice = foundItem.ProcessFixedCost;
                    proce.OrdItemProcHours = foundItem.TotalHour;
                    proce.OrdItemProcPricePerHour = foundItem.ProcessCostPerHour;
                    proce.OrdItemProcPricePerUnit = foundItem.ProcessCostPerUnit;
                    proce.OrdItemProcUnits = foundItem.TotalUnit;
                    proce.OrdItemProcInternalCost = foundItem.ProcessInternalCost; //Added by mukesh 20130523
                    proce.SysTaxCodeDescID = foundItem.TaxGrpID;
                    proce.Insert();
                    foundItem.ProcessItemID = proce.OrderItemProcID;
                    foundItem.TaxGrpID = proce.SysTaxCodeDescID; //Re populate tax group

                    foundItem.ItemTaxes = new Tax();
                    foundItem.ItemTaxes.Tax1 = proce.Tax1;
                    foundItem.ItemTaxes.Tax2 = proce.Tax1;
                    foundItem.ItemTaxes.Tax3 = proce.Tax1;
                    foundItem.ItemTaxes.Tax4 = proce.Tax1;
                    foundItem.ItemTaxes.Tax5 = proce.Tax1;
                    foundItem.ItemTaxes.TaxDesc1 = proce.TaxDesc1;
                    foundItem.ItemTaxes.TaxDesc2 = proce.TaxDesc2;
                    foundItem.ItemTaxes.TaxDesc3 = proce.TaxDesc3;
                    foundItem.ItemTaxes.TaxDesc4 = proce.TaxDesc4;
                    foundItem.ItemTaxes.TaxDesc5 = proce.TaxDesc5;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated1;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated2;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated3;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated4;
                    foundItem.ItemTaxes.TaxCalculated1 = proce.TaxCalculated5;
                    foundItem.ItemTaxes.TaxGroupID = proce.SysTaxCodeDescID;
                }
                else
                {
                    //Update taxes in cart
                    foundItem.ItemTaxes = new Tax();
                    if (foundItem.TaxGrpID <= 0)
                    {
                        foundItem.TaxGrpID = TaxCalculationHelper.GetTaxGroup(null, -1, -1, cart.OrderWarehouseCode);
                    }
                    foundItem.ItemTaxes.PopulateObject(null, foundItem.TaxGrpID, foundItem.ProcessTotal);
                }

                cart.ProcessItems.Add(foundItem);
            }

            CurrentCart = cart;
        }

        public static void DeleteProcessCartItem(int id)
        {
            SalesOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.ProcessItems where fi.ProcessID == id select fi;

            if (foundItemCol.Count() > 0)
            {
                var foundItem = foundItemCol.First();
                if (foundItem.ProcessItemID > 0)
                {
                    new OrderItemProcess().Delete(foundItem.ProcessItemID);
                }
                cart.ProcessItems.Remove(foundItem);
            }
            CurrentCart = cart;
        }

        public static SalesProcessEntity GetProcessItem(int processID)
        {
            var foundItemCol = from fi in CurrentCart.ProcessItems where fi.ProcessID == processID select fi;
            if (foundItemCol.Count() > 0)
            {
                return foundItemCol.First();
            }
            return null;
        }

        #endregion
        #endregion

        public static TotalSummary GetOrderTotal()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return CalculationHelper.GetOrderTotal(dbHelp, CurrentCart);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static void Dispose() {
            CurrentCart = null;
        }

        public static List<OrderItems> GetOrderItemsFromCart(int orderIdToSet)
        {
            List<OrderItems> ordItems = new List<OrderItems>();
            foreach (var dr in CurrentCart.Items)
            {
                OrderItems oi = new OrderItems();
                oi.OrderItemDesc = dr.ProductName;
                oi.OrdID = orderIdToSet;
                oi.OrdProductDiscount = dr.Discount;
                oi.OrdProductDiscountType = dr.DisType;
                oi.OrdProductID = dr.ProductID;
                oi.OrdProductQty = dr.Quantity;
                oi.OrdProductTaxGrp = dr.TaxGrp;
                oi.OrdProductUnitPrice = dr.Price;
                oi.OrdGuestID = dr.GuestID;
                
                ordItems.Add(oi);
            }
            return ordItems;
        }

        public static List<OrderItemProcess> GetProcessItemsFromCart(int orderIdToSet)
        {
            List<OrderItemProcess> ordITemProcess = new List<OrderItemProcess>();
            foreach (var dr in CurrentCart.ProcessItems)
            {
                OrderItemProcess oip = new OrderItemProcess();
                oip.OrdID = orderIdToSet;
                oip.OrdItemProcCode = dr.ProcessCode;
                oip.OrdItemProcFixedPrice = dr.ProcessFixedCost;
                oip.OrdItemProcHours = dr.TotalHour;
                oip.OrdItemProcPricePerHour = dr.ProcessCostPerHour;
                oip.OrdItemProcPricePerUnit = dr.ProcessCostPerUnit;
                oip.OrdItemProcUnits = dr.TotalUnit;
                oip.OrdItemProcInternalCost = dr.ProcessInternalCost; //Added by mukesh 20130523
                oip.SysTaxCodeDescID = dr.TaxGrpID;

                ordITemProcess.Add(oip);
            }
            return ordITemProcess;
        }
    }

    public class SalesOrderCart
    {
        public List<SalesCartEntity> Items { get; private set; }
        public List<SalesProcessEntity> ProcessItems { get; private set; }

        public SalesOrderCart() {
            this.Items = new List<SalesCartEntity>();
            this.ProcessItems = new List<SalesProcessEntity>();
        }

        public int OrderID { get; set; }
        public int ReservationID { get; set; }
        public double OrdDiscount { get; set; }
        public string OrdDiscountType { get; set; }
        public double ExchangeRate { get; set; }
        public int OrderCustomerID { get; set; }
        public int OrderCompanyID { get; set; }
        public string OrderCustomerType { get; set; }
        public int OrderGuestType { get; set; }
        public string OrderCurrencyCode { get; set; }
        public string OrderWarehouseCode { get; set; }
        public string OrderCustomerName { get; set; }

        public void BindLocalCart(DbHelper dbHelp, string whsCode, int customerID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT w.WarehouseCompanyID, c.CompanyBasCur FROM syswarehouses w INNER JOIN syscompanyinfo c ON c.CompanyID=w.WarehouseCompanyID WHERE WarehouseCode=@WarehouseCode";
            string sqlExRate = "SELECT c.CurrencyRelativePrice FROM syscurrencies c WHERE c.CurrencyCode=@CurrencyCode";                        
            try
            {
                Partners p = new Partners();
                p.PopulateObject(dbHelp, customerID); 
                this.OrderWarehouseCode = whsCode;
                this.OrderCustomerID = customerID;
                this.OrderCustomerType = Globals.GetPartnerType(p.PartnerType);
                this.OrderGuestType = p.ExtendedProperties.GuestType;
                this.OrderCurrencyCode = p.PartnerCurrencyCode;
                this.OrderCustomerName = p.PartnerLongName;

                using (IDataReader r = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("WarehouseCode", whsCode, MyDbType.String)
                    }))
                {
                    if (r.Read())
                    {
                        this.OrderCompanyID = BusinessUtility.GetInt(r["WarehouseCompanyID"]);
                        if (!string.IsNullOrEmpty(this.OrderCurrencyCode))
                        {
                            this.OrderCurrencyCode = BusinessUtility.GetString(r["CompanyBasCur"]);
                        }
                    }
                }               
                
                object val = dbHelp.GetValue(sqlExRate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CurrencyCode", this.OrderCurrencyCode, MyDbType.String)
                });
                this.ExchangeRate = BusinessUtility.GetDouble(val);
                if (this.ExchangeRate <= 0)
                {
                    this.ExchangeRate = 1.00D;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void BindCart(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }            
            try
            {
                this.BindItems(dbHelp, orderID);
                this.BindProcessCart(dbHelp, orderID);
                string sqlExchangeRate = "SELECT o.ordID, o.ordCurrencyExRate, o.ordCustID, o.ordCustType, ce.GuestType, o.ordCompanyID, o.ordCurrencyCode, o.ordShpWhsCode, p.PartnerLongName, o.ordDiscount, o.ordDiscountType, r.ReservationID FROM orders o";
                sqlExchangeRate += " INNER JOIN partners p ON p.PartnerID=o.ordCustID LEFT OUTER JOIN z_customer_extended ce ON ce.PartnerID=p.PartnerID";
                sqlExchangeRate += " LEFT OUTER JOIN z_reservation r ON r.SoID=o.ordID";
                sqlExchangeRate += " where o.ordID=@OrderID";
                using (IDataReader r = dbHelp.GetDataReader(sqlExchangeRate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    }))
                {
                    if (r.Read())
                    {
                        this.ExchangeRate = BusinessUtility.GetDouble(r["ordCurrencyExRate"]);
                        this.OrderCustomerID = BusinessUtility.GetInt(r["ordCustID"]);
                        this.OrderCustomerType = BusinessUtility.GetString(r["ordCustType"]);
                        this.OrderGuestType = BusinessUtility.GetInt(r["GuestType"]);
                        this.OrderCompanyID = BusinessUtility.GetInt(r["ordCompanyID"]);
                        this.OrderCurrencyCode = BusinessUtility.GetString(r["ordCurrencyCode"]);
                        this.OrderWarehouseCode = BusinessUtility.GetString(r["ordShpWhsCode"]);
                        this.OrderCustomerName = BusinessUtility.GetString(r["PartnerLongName"]);
                        this.OrdDiscount = BusinessUtility.GetDouble(r["ordDiscount"]);
                        this.OrdDiscountType = BusinessUtility.GetString(r["ordDiscountType"]);
                        this.OrderID = BusinessUtility.GetInt(r["ordID"]);
                        this.ReservationID = BusinessUtility.GetInt(r["ReservationID"]);
                    }
                }                
               
                if (this.ExchangeRate <= 0)
                {
                    this.ExchangeRate = 1.00D;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        private void BindItems(DbHelper dbHelp, int orderID)
        {            
            OrderItems ordItems = new OrderItems();            
            //IDataReader r = null;
            try
            {                
                //r = ordItems.GetOrderItemsReader(dbHelp, orderID);
                //int iCount = 1;
                //while (r.Read())
                DataTable dt = ordItems.GetOrderItemsTable(dbHelp, orderID);
                int iCount = 1;
                foreach (DataRow r in dt.Rows)
                {
                    SalesCartEntity en = new SalesCartEntity();
                    en.ID = iCount;
                    en.ProductID = BusinessUtility.GetInt(r["ordProductID"]);
                    en.UPCCode = BusinessUtility.GetString(r["prdUPCCode"]);
                    en.ProductName = BusinessUtility.GetString(r["prdName"]);
                    en.Quantity = BusinessUtility.GetDouble(r["ordProductQty"]);
                    en.Price = BusinessUtility.GetDouble(r["InOrdProductUnitPrice"]);
                    en.RangePrice = 0;
                    en.WarehouseCode = BusinessUtility.GetString(r["ordShpWhsCode"]);
                    en.TaxGrp = BusinessUtility.GetInt(r["ordProductTaxGrp"]);
                    en.TaxGrpDesc = BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                    en.Discount = BusinessUtility.GetInt(r["ordProductDiscount"]);
                    en.DisType = BusinessUtility.GetString(r["ordProductDiscountType"]);
                    en.ItemTotal = BusinessUtility.GetDouble(r["amount"]);
                    en.OrderItemID = BusinessUtility.GetInt(r["orderItemID"]);
                    en.GuestID = BusinessUtility.GetInt(r["ordGuestID"]);
                    en.ReservationID = BusinessUtility.GetInt(r["ReservationID"]);
                    en.ReservationItemID = BusinessUtility.GetInt(r["ReservationItemID"]);
                    en.OrderID = BusinessUtility.GetInt(r["ordID"]);
                    en.IsCanceled = BusinessUtility.GetBool(r["ordIsItemCanceled"]);
                    en.OriginalPrdName = BusinessUtility.GetString(r["OriginalPrdName"]);
                    en.ProductType = BusinessUtility.GetInt(r["ProductType"]);
                    en.ItemTaxes = new Tax();
                    en.ItemTaxes.Tax1 = BusinessUtility.GetDouble(r["Tax1"]);
                    en.ItemTaxes.Tax2 = BusinessUtility.GetDouble(r["Tax2"]);
                    en.ItemTaxes.Tax3 = BusinessUtility.GetDouble(r["Tax3"]);
                    en.ItemTaxes.Tax4 = BusinessUtility.GetDouble(r["Tax4"]);
                    en.ItemTaxes.Tax5 = BusinessUtility.GetDouble(r["Tax5"]);
                    en.ItemTaxes.TaxCalculated1 = BusinessUtility.GetDouble(r["TaxCalculated1"]);
                    en.ItemTaxes.TaxCalculated2 = BusinessUtility.GetDouble(r["TaxCalculated2"]);
                    en.ItemTaxes.TaxCalculated3 = BusinessUtility.GetDouble(r["TaxCalculated3"]);
                    en.ItemTaxes.TaxCalculated4 = BusinessUtility.GetDouble(r["TaxCalculated4"]);
                    en.ItemTaxes.TaxCalculated5 = BusinessUtility.GetDouble(r["TaxCalculated5"]);
                    en.ItemTaxes.TaxDesc1 = BusinessUtility.GetString(r["TaxDesc1"]);
                    en.ItemTaxes.TaxDesc2 = BusinessUtility.GetString(r["TaxDesc2"]);
                    en.ItemTaxes.TaxDesc3 = BusinessUtility.GetString(r["TaxDesc3"]);
                    en.ItemTaxes.TaxDesc4 = BusinessUtility.GetString(r["TaxDesc4"]);
                    en.ItemTaxes.TaxDesc5 = BusinessUtility.GetString(r["TaxDesc5"]);
                    en.PreviousReturnedQty = BusinessUtility.GetInt(r["ordProductQty"]) - BusinessUtility.GetInt(r["BalanceQty"]);
                    if (en.GuestID > 0)
                    {
                        //Partners gst = new Partners();
                        //gst.PopulateObject(en.GuestID);
                        en.ItemGuestName = BusinessUtility.GetString(r["PartnerLongName"]);
                    }
                    en.PrdColor = BusinessUtility.GetString(r["Color"]);
                    en.PrdSize = BusinessUtility.GetString(r["Size"]);
                    this.Items.Add(en);
                    iCount++;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                //if (r != null && !r.IsClosed)
                //{
                //    r.Close();
                //    r.Dispose();
                //}                
            }
        }

        private void BindProcessCart(DbHelper dbHelp, int orderID)
        {
            OrderItemProcess ordProcessItem = new OrderItemProcess();            
            IDataReader r = null;            
            try
            {
                r = ordProcessItem.GetOrderProcessItemsReader(dbHelp, orderID);
                int iCount = 1;
                while (r.Read())
                {
                    SalesProcessEntity en = new SalesProcessEntity();
                    en.ProcessCode = BusinessUtility.GetString(r["ordItemProcCode"]);
                    en.TaxGrpID = BusinessUtility.GetInt(r["sysTaxCodeDescID"]);
                    en.TaxGrpName = BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                    en.ProcessDescription = BusinessUtility.GetString(r["ProcessDescription"]);
                    en.ProcessFixedCost = BusinessUtility.GetDouble(r["InOrdItemProcFixedPrice"]);
                    en.ProcessCostPerHour = BusinessUtility.GetDouble(r["InOrdItemProcPricePerHour"]);
                    en.ProcessCostPerUnit = BusinessUtility.GetDouble(r["InOrdItemProcPricePerUnit"]);
                    en.TotalHour = BusinessUtility.GetInt(r["ordItemProcHours"]);
                    en.TotalUnit = BusinessUtility.GetInt(r["ordItemProcUnits"]);
                    en.ProcessInternalCost = BusinessUtility.GetDouble(r["ordItemProcInternalCost"]); //Added by mukesh 20130523
                    en.ProcessTotal = BusinessUtility.GetDouble(r["ProcessCost"]);
                    en.ProcessItemID = BusinessUtility.GetInt(r["orderItemProcID"]);
                    en.OrderID = BusinessUtility.GetInt(r["ordID"]);
                    en.ItemTaxes = new Tax();
                    en.ItemTaxes.Tax1 = BusinessUtility.GetDouble(r["Tax1"]);
                    en.ItemTaxes.Tax2 = BusinessUtility.GetDouble(r["Tax2"]);
                    en.ItemTaxes.Tax3 = BusinessUtility.GetDouble(r["Tax3"]);
                    en.ItemTaxes.Tax4 = BusinessUtility.GetDouble(r["Tax4"]);
                    en.ItemTaxes.Tax5 = BusinessUtility.GetDouble(r["Tax5"]);
                    en.ItemTaxes.TaxCalculated1 = BusinessUtility.GetDouble(r["TaxCalculated1"]);
                    en.ItemTaxes.TaxCalculated2 = BusinessUtility.GetDouble(r["TaxCalculated2"]);
                    en.ItemTaxes.TaxCalculated3 = BusinessUtility.GetDouble(r["TaxCalculated3"]);
                    en.ItemTaxes.TaxCalculated4 = BusinessUtility.GetDouble(r["TaxCalculated4"]);
                    en.ItemTaxes.TaxCalculated5 = BusinessUtility.GetDouble(r["TaxCalculated5"]);
                    en.ItemTaxes.TaxDesc1 = BusinessUtility.GetString(r["TaxDesc1"]);
                    en.ItemTaxes.TaxDesc2 = BusinessUtility.GetString(r["TaxDesc2"]);
                    en.ItemTaxes.TaxDesc3 = BusinessUtility.GetString(r["TaxDesc3"]);
                    en.ItemTaxes.TaxDesc4 = BusinessUtility.GetString(r["TaxDesc4"]);
                    en.ItemTaxes.TaxDesc5 = BusinessUtility.GetString(r["TaxDesc5"]);

                    en.ProcessID = iCount;
                    
                    this.ProcessItems.Add(en);
                    iCount++;
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (r != null && !r.IsClosed)
                {
                    r.Close();
                    r.Dispose();
                }
            }            
        }
    }

    public class SalesCartEntity
    {
        public int ID { get; set; }
        public string UPCCode { get; set; }
        public string ProductName { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double RangePrice { get; set; }
        public string WarehouseCode { get; set; }
        public int TaxGrp { get; set; }
        public string TaxGrpDesc { get; set; }
        public int Discount { get; set; }
        public string DisType { get; set; }
        public double ItemTotal { get; set; }
        public int ProductID { get; set; }
        public int GuestID { get; set; }
        public int ReservationID { get; set; }
        public int ReservationItemID { get; set; }
        public int OrderItemID { get; set; }
        public int OrderID { get; set; }
        public string ItemGuestName { get; set; }
        public bool IsCanceled { get; set; }
        public string OriginalPrdName { get; set; }
        public int ProductType { get; set; }
        public int ToShipQty { get; set; }
        public int PreviousReturnedQty { get; set; }
        public Tax ItemTaxes { get; set; }
        public string PrdColor { get; set; }
        public string PrdSize { get; set; }
    }

    public class SalesProcessEntity
    {
        public int ProcessID { get; set; }
        public string ProcessCode { get; set; }
        public int TaxGrpID { get; set; }
        public string TaxGrpName { get; set; }
        public string ProcessDescription { get; set; }
        public double ProcessFixedCost { get; set; }
        public double ProcessCostPerHour { get; set; }
        public double ProcessCostPerUnit { get; set; }
        public int TotalHour { get; set; }
        public double ProcessInternalCost { get; set; } //Added by mukesh 20130523
        public int TotalUnit { get; set; }
        public double ProcessTotal { get; set; }
        public int ProcessItemID { get; set; }
        public int OrderID { get; set; }
        public Tax ItemTaxes { get; set; }
    }

}
