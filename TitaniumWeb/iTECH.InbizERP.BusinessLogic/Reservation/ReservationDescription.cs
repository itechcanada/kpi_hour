﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ReservationDescription
    {
        public int ReservationItemID { get; set; }
        public int ReserveFor { get; set; }
        public int SOID { get; set; }
        public int TotalNights { get; set; }
        public int GuestID { get; set; }
        public bool IsCouple { get; set; }
        public string Sex { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpirtualName { get; set; }
        public bool IsInvoiceCreated { get; set; }
    }
}
