﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Rooms
    {
        public int RoomID { get; set; }
        public int BuildingID { get; set; }
        public int RoomType { get; set; }
        public string RoomTitleEn { get; set; }
        public string RoomTitleFr { get; set; }
        public double ProritySingle { get; set; }
        public double PriorityCouple { get; set; }
        public int Seq { get; set; }
        public bool IsActive { get; set; }

        public void Insert()
        {
            string sql = "INSERT INTO z_accommodation_rooms (BuildingID,RoomType,RoomTitleEn,RoomTitleFr,ProritySingle,PriorityCouple,Seq,IsActive)";
            sql += " VALUES(@BuildingID,@RoomType,@RoomTitleEn,@RoomTitleFr,@ProritySingle,@PriorityCouple,@Seq,@IsActive)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("PriorityCouple", this.PriorityCouple, MyDbType.Double),
                    DbUtility.GetParameter("ProritySingle", this.ProritySingle, MyDbType.Double),
                    DbUtility.GetParameter("RoomTitleEn", this.RoomTitleEn, MyDbType.String),
                    DbUtility.GetParameter("RoomTitleFr", this.RoomTitleFr, MyDbType.String),
                    DbUtility.GetParameter("RoomType", this.RoomType, MyDbType.Int),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int)                    
                });

                this.RoomType = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update() {
            string sql = "UPDATE z_accommodation_rooms SET BuildingID=@BuildingID,RoomType=@RoomType,RoomTitleEn=@RoomTitleEn,RoomTitleFr=@RoomTitleFr,";
            sql += "IsActive=@IsActive WHERE RoomID=@RoomID";

            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),                    
                    DbUtility.GetParameter("RoomTitleEn", this.RoomTitleEn, MyDbType.String),
                    DbUtility.GetParameter("RoomTitleFr", this.RoomTitleFr, MyDbType.String),
                    DbUtility.GetParameter("RoomType", this.RoomType, MyDbType.Int),                    
                    DbUtility.GetParameter("RoomID", this.RoomID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int rid)
        {
            //Inactivate instead of deleting
            //string sql = "DELETE FROM z_accommodation_rooms WHERE RoomID=@RoomID";
            //string sql = "UPDATE z_accommodation_rooms SET IsActive=0 WHERE RoomID=@RoomID";
            //string sql = "UPDATE z_accommodation_rooms r, products p";
            //sql += " SET r.IsActive=0, prdIsActive = 0";
            //sql += " WHERE p.prdExtendedCategory=r.RoomID";
            //sql += " AND r.RoomID=@RoomID";
            List<string> sqlToExecute = new List<string>();
            sqlToExecute.Add("UPDATE products SET prdIsActive=0 WHERE prdExtendedCategory=@RoomID;");
            sqlToExecute.Add("UPDATE z_accommodation_rooms SET IsActive=0 WHERE RoomID=@RoomID;");           
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                foreach (var item in sqlToExecute)
                {
                    dbHelp.ExecuteNonQuery(item, CommandType.Text, new MySqlParameter[] { 
                         DbUtility.GetParameter("RoomID", rid, MyDbType.Int)
                    });
                }   
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSequence(int roomId, int seq, double prioritySingle, double priorityCouple)
        {
            string sql = "UPDATE z_accommodation_rooms SET ProritySingle=@ProritySingle,PriorityCouple=@PriorityCouple,Seq=@Seq WHERE RoomID=@RoomID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                       DbUtility.GetParameter("RoomID", roomId, MyDbType.Int),
                       DbUtility.GetParameter("Seq", seq, MyDbType.Int),
                       DbUtility.GetParameter("ProritySingle", prioritySingle, MyDbType.Double),
                       DbUtility.GetParameter("PriorityCouple", priorityCouple, MyDbType.Double)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int rid)
        {
            string sql = "SELECT * FROM z_accommodation_rooms WHERE RoomID=@RoomID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("RoomID", rid, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.BuildingID = BusinessUtility.GetInt(dr["BuildingID"]);
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.PriorityCouple = BusinessUtility.GetDouble(dr["PriorityCouple"]);
                    this.ProritySingle = BusinessUtility.GetDouble(dr["ProritySingle"]);
                    this.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                    this.RoomTitleEn = BusinessUtility.GetString(dr["RoomTitleEn"]);
                    this.RoomTitleFr = BusinessUtility.GetString(dr["RoomTitleFr"]);
                    this.RoomType = BusinessUtility.GetInt(dr["RoomType"]);
                    this.Seq = BusinessUtility.GetInt(dr["Seq"]);                    
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(ParameterCollection pCol, string searchText, string lang)
        {
            pCol.Clear();
            string sql = "SELECT r.RoomID,r.BuildingID,r.RoomType," + Globals.GetFieldName("r.RoomTitle", lang) + " AS RoomTitle,r.ProritySingle,r.PriorityCouple,r.Seq AS SeqRoom,r.IsActive, r.ProritySingle,r.PriorityCouple,r.Seq,";
            sql += " " + Globals.GetFieldName("b.BuildingTitle", lang) + " AS BuildingTitle,b.Seq AS SeqBuilding FROM  z_accommodation_rooms r INNER JOIN z_accommodation_buildings b ON (r.BuildingID = b.BuildingID)";
            sql += " WHERE r.IsActive=1";
            
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " AND (" + Globals.GetFieldName("r.RoomTitle", lang) + " LIKE CONCAT(@SearchText, '%') OR " + Globals.GetFieldName("b.BuildingTitle", lang) + "  LIKE CONCAT(@SearchText, '%'))";
                pCol.Add("@SearchText", searchText);
            }
            sql += " ORDER BY b.Seq, r.Seq";
            return sql;
        }

        public List<OptionList> GetRooms(int buildingid, string lang)
        {
            string sql = "SELECT RoomID, " + Globals.GetFieldName("RoomTitle", lang) + " AS Room, RoomType FROM z_accommodation_rooms ";
            sql += " WHERE BuildingID=@BuildingID AND IsActive=1 ORDER BY Seq";
            DbHelper dbHelper = new DbHelper();
            List<OptionList> lResult = new List<OptionList>();
            try
            {
                DataTable dt = dbHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("BuildingID", buildingid, MyDbType.Int) });
                foreach (DataRow dr in dt.Rows)
                {
                    OptionList ol = new OptionList();
                    ol.id = BusinessUtility.GetString(dr["RoomID"]);
                    string roomType = this.GetCategorySubGroup(dbHelper, BusinessUtility.GetInt(dr["RoomType"]), lang);
                    ol.Name = string.Format("{0}{1}{2}", roomType.Trim(), !string.IsNullOrEmpty(roomType) ? " - " : "", dr["Room"]);
                    lResult.Add(ol);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public string GetCategorySubGroup(DbHelper dbHelp, int idVal, string lang)
        {
            string sql = "SELECT sysAppDesc FROM sysstatus WHERE sysApppfx='CT' AND sysAppCode='dlSGp' AND sysAppCodeLang=@sysAppCodeLang AND sysAppLogicCode=@sysAppLogicCode";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysAppCodeLang", lang, MyDbType.String),
                    DbUtility.GetParameter("sysAppLogicCode", idVal, MyDbType.String)
                });
                return BusinessUtility.GetString(val);
            }
            catch
            {

                throw;
            }
        }

        public double GetPrice(int roomID)
        {
            string sql = "SELECT Price FROM vw_beds WHERE RoomID=@RoomID LIMIT 1";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdatePrice(int roomID, double price)
        {
            string sql = "UPDATE products SET prdEndUserSalesPrice=@Price WHERE prdExtendedCategory=@RoomID AND IFNULL(prdExtendedCategory, 0) <> 0";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int),
                    DbUtility.GetParameter("Price", price, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetAllRoom(string lang)
        {
            string sql = string.Format("SELECT r.RoomID, CONCAT(b.BuildingTitle{0}, ' - ', r.RoomTitle{0}) AS DescText FROM z_accommodation_rooms r ", lang);
            sql += " INNER JOIN z_accommodation_buildings b ON (r.BuildingID = b.BuildingID) WHERE r.IsActive=1 ORDER BY b.Seq, r.Seq";

            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetSexAssociatedToRoom(DbHelper dbHelp, int roomID)
        {
            string sql = "SELECT ps.Sex FROM z_product_sex ps INNER JOIN products p ON p.ProductID=ps.ProductID";
            sql += " INNER JOIN z_accommodation_rooms r ON r.RoomID=p.prdExtendedCategory ";
            sql += " WHERE RoomID=@RoomID LIMIT 1";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val);
            }
            catch 
            {                
                throw;
            }
        }

        public int GetSexAssociatedToRoom(int roomID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.GetSexAssociatedToRoom(dbHelp, roomID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void AssignSexToRoom(int[] roomID, int sex)        
        {
            string sqlDelete = "DELETE FROM z_product_sex WHERE ProductID IN(SELECT ProductID FROM products WHERE prdExtendedCategory={0})";
            string sqlInsert = "INSERT INTO z_product_sex(ProductID, Sex) ";
            sqlInsert += " SELECT ProductID, {0} AS Sex FROM products WHERE prdExtendedCategory={1}";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                foreach (var item in roomID)
                {
                    dbHelp.ExecuteNonQuery(string.Format(sqlDelete, item), CommandType.Text, null);
                    dbHelp.ExecuteNonQuery(string.Format(sqlInsert, sex, item), CommandType.Text, null);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
