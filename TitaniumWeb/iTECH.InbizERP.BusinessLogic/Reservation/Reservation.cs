﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Reservation
    {
        public int ReservationID { get; set; }
        public int PrimaryPartnerID { get; set; }
        public int NoOfGuestOlderMale { get;set; }
        public int NoOfGuestOlderFeMale { get; set; }
        public int NoOfGuestYoungerMale { get; set; }
        public int NoOfGuestYoungerFeMale { get; set; }
        public int ReservationStatus { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ReserveFor { get; set; }
        public int SoID { get; set; }
        public string ReservationNote { get; set; }


        public void Insert(DbHelper dbHelp, int userid) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_reservation (PrimaryPartnerID, NoOfGuestOlderMale, NoOfGuestOlderFeMale, NoOfGuestYoungerMale, NoOfGuestYoungerFeMale, ReservationStatus,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,ReserveFor,ReservationNote) ";
            sql += " VALUES (@PrimaryPartnerID, @NoOfGuestOlderMale, @NoOfGuestOlderFeMale, @NoOfGuestYoungerMale, @NoOfGuestYoungerFeMale,";
            sql += "@ReservationStatus,@CreatedBy,@CreatedOn,@ModifiedBy,@ModifiedOn,@ReserveFor,@ReservationNote)";            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PrimaryPartnerID", this.PrimaryPartnerID, MyDbType.Int),
                        DbUtility.GetParameter("NoOfGuestOlderFeMale", this.NoOfGuestOlderFeMale, MyDbType.Int),
                        DbUtility.GetParameter("NoOfGuestOlderMale", this.NoOfGuestOlderMale, MyDbType.Int),
                        DbUtility.GetParameter("NoOfGuestYoungerFeMale", this.NoOfGuestYoungerFeMale, MyDbType.Int),
                        DbUtility.GetParameter("NoOfGuestYoungerMale", this.NoOfGuestYoungerMale, MyDbType.Int),
                        DbUtility.GetParameter("ReservationStatus", this.ReservationStatus, MyDbType.Int),
                        DbUtility.GetParameter("CreatedBy", userid, MyDbType.Int),
                        DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("ModifiedBy", userid, MyDbType.Int),
                        DbUtility.GetParameter("ModifiedOn", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("ReserveFor", this.ReserveFor, MyDbType.Int),
                        DbUtility.GetParameter("ReservationNote", this.ReservationNote, MyDbType.String)
                });

                this.ReservationID = dbHelp.GetLastInsertID();
                //if (this.ReservationID > 0 && firstItem != null)
                //{
                //    firstItem.ReservationID = this.ReservationID;
                //    firstItem.Insert(dbHelp);
                //}
            }
            catch
            {
                throw;
            }
            finally {
               if(mustClose)  dbHelp.CloseDatabaseConnection();
            }
        }                

        public void PopulateObject(DbHelper dbHelp, int reservationID) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM z_reservation WHERE ReservationID=@ReservationID";                       
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int) }))
                {
                    if (dr.Read())
                    {
                        this.CreatedBy = BusinessUtility.GetInt(dr["CreatedBy"]);
                        this.CreatedOn = BusinessUtility.GetDateTime(dr["CreatedOn"]);
                        this.ModifiedBy = BusinessUtility.GetInt(dr["ModifiedBy"]);
                        this.ModifiedOn = BusinessUtility.GetDateTime(dr["ModifiedOn"]);
                        this.NoOfGuestOlderFeMale = BusinessUtility.GetInt(dr["NoOfGuestOlderFeMale"]);
                        this.NoOfGuestOlderMale = BusinessUtility.GetInt(dr["NoOfGuestOlderMale"]);
                        this.NoOfGuestYoungerFeMale = BusinessUtility.GetInt(dr["NoOfGuestYoungerFeMale"]);
                        this.NoOfGuestYoungerMale = BusinessUtility.GetInt(dr["NoOfGuestYoungerMale"]);
                        this.PrimaryPartnerID = BusinessUtility.GetInt(dr["PrimaryPartnerID"]);
                        this.ReservationID = BusinessUtility.GetInt(dr["ReservationID"]);
                        this.ReservationStatus = BusinessUtility.GetInt(dr["ReservationStatus"]);
                        this.ReserveFor = BusinessUtility.GetInt(dr["ReserveFor"]);
                        this.ReservationNote = BusinessUtility.GetString(dr["ReservationNote"]);
                        this.SoID = BusinessUtility.GetInt(dr["SoID"]);
                    }
                }
            }
            catch
            {

                throw;
            }
            finally {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public void Update(int userid) {
        //    string sqlUpdate = "UPDATE z_reservation SET NoOfGuestOlderMale=@NoOfGuestOlderMale, NoOfGuestOlderFeMale=@NoOfGuestOlderFeMale, ";
        //    sqlUpdate += "NoOfGuestYoungerMale=@NoOfGuestYoungerMale, NoOfGuestYoungerFeMale=@NoOfGuestYoungerFeMale, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn WHERE ReservationID=@ReservationID";
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] {                         
        //                DbUtility.GetParameter("NoOfGuestOlderFeMale", this.NoOfGuestOlderFeMale, MyDbType.Int),
        //                DbUtility.GetParameter("NoOfGuestOlderMale", this.NoOfGuestOlderMale, MyDbType.Int),
        //                DbUtility.GetParameter("NoOfGuestYoungerFeMale", this.NoOfGuestYoungerFeMale, MyDbType.Int),
        //                DbUtility.GetParameter("NoOfGuestYoungerMale", this.NoOfGuestYoungerMale, MyDbType.Int),                       
        //                DbUtility.GetParameter("ModifiedBy", userid, MyDbType.Int),
        //                DbUtility.GetParameter("ModifiedOn", DateTime.Now, MyDbType.DateTime),
        //                DbUtility.GetParameter("ReservationID", this.ReservationID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}        

        public void CancelReservation(int soid) {
            string sql = "UPDATE z_reservation SET ReservationStatus=@ReservationStatus  WHERE SoID=@SoID";            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Closed, MyDbType.Int),
                    DbUtility.GetParameter("SoID", soid, MyDbType.Int)
                });

                TotalSummary lTotal = CalculationHelper.GetOrderTotal(dbHelp, soid);
                if (lTotal.TotalAmountReceived > 0.0D)
                {
                    Orders ord = new Orders();
                    ord.PopulateObject(dbHelp, soid);

                    //Credit Amount TO Order Customer
                    CustomerCredit cc = new CustomerCredit();
                    cc.CreditAmount = lTotal.TotalAmountReceived;
                    cc.CreditedBy = CurrentUser.UserID;
                    cc.CreditVia = (int)StatusAmountReceivedVia.Credit;
                    cc.CustomerID = ord.OrdCustID;
                    cc.ReservationItemID = 0;
                    cc.Insert(CurrentUser.UserID); //Inser Record to Track Customer Credits

                    //Update Order Items
                    //OrderItems oi = new OrderItems();
                    //oi.CancelAllOrderItem(dbHelp, soid);

                    //Update Reservation Ites
                    //int rsvID = this.GetReservationID(dbHelp, soid);
                    //ReservationItems ri = new ReservationItems();
                    //ri.CancelAllReservationItem(dbHelp, rsvID);
                }
            }
            catch 
            {
                
            }
        }

        public void CancelReservation(DbHelper dbHelp, int soid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE z_reservation SET ReservationStatus=@ReservationStatus  WHERE SoID=@SoID";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Closed, MyDbType.Int),
                    DbUtility.GetParameter("SoID", soid, MyDbType.Int)
                });

                TotalSummary lTotal = CalculationHelper.GetOrderTotal(dbHelp, soid);
                if (lTotal.TotalAmountReceived > 0.0D)
                {
                    Orders ord = new Orders();
                    ord.PopulateObject(dbHelp, soid);

                    //Credit Amount TO Order Customer
                    CustomerCredit cc = new CustomerCredit();
                    cc.CreditAmount = lTotal.TotalAmountReceived;
                    cc.CreditedBy = CurrentUser.UserID;
                    cc.CreditVia = (int)StatusAmountReceivedVia.Credit;
                    cc.CustomerID = ord.OrdCustID;
                    cc.ReservationItemID = 0;
                    cc.Insert(dbHelp, CurrentUser.UserID); //Inser Record to Track Customer Credits

                    //Update Order Items
                    //OrderItems oi = new OrderItems();
                    //oi.CancelAllOrderItem(dbHelp, soid);

                    //Update Reservation Ites
                    //int rsvID = this.GetReservationID(dbHelp, soid);
                    //ReservationItems ri = new ReservationItems();
                    //ri.CancelAllReservationItem(dbHelp, rsvID);
                }
            }
            catch
            {
                throw;
            }
            finally {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetReservationID(int orderID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.GetReservationID(dbHelp, orderID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetReservationID(DbHelper dbHelp, int orderID)
        {
            string sql = "SELECT ReservationID FROM z_reservation WHERE SoID=@SoID";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("SoID", orderID, MyDbType.Int)
                });

                return BusinessUtility.GetInt(val);
            }
            catch 
            {
                
                throw;
            }
        }

        public ReservationItems GetFirstReservationItem(int orderID)
        {
            DbHelper dbHelp = new DbHelper(true);            
            try
            {
                return this.GetFirstReservationItem(dbHelp, orderID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public ReservationItems GetFirstReservationItem(DbHelper dbHelp, int orderID)
        {            
            MySqlDataReader dr = null;
            ReservationItems ri = new ReservationItems();
            try
            {
                int rsvID = this.GetReservationID(dbHelp, orderID);
                if (rsvID > 0)
                {
                    string sql = "SELECT * FROM z_reservation_items WHERE ReservationID=@ReservationID";
                    dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", rsvID, MyDbType.Int)
                    });
                    if (dr.Read())
                    {
                        ri.BedID = BusinessUtility.GetInt(dr["BedID"]);
                        ri.CheckInDate = BusinessUtility.GetDateTime(dr["CheckInDate"]);
                        ri.CheckOutDate = BusinessUtility.GetDateTime(dr["CheckOutDate"]);
                        ri.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                        ri.IsCanceled = BusinessUtility.GetBool(dr["IsCanceled"]);
                        ri.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                        ri.PricePerDay = BusinessUtility.GetDouble(dr["PricePerDay"]);
                        ri.RelationshipToPrimaryGuest = BusinessUtility.GetString(dr["RelationshipToPrimaryGuest"]);
                        ri.ReservationID = BusinessUtility.GetInt(dr["ReservationID"]);
                        ri.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                    }

                    return ri;
                }
                return ri;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        public bool UpdateReservationStatus(DbHelper dbHelp, int reservationID, int orderid, StatusReservation status)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                //Update Reservation Status 
                dbHelp.ExecuteNonQuery("UPDATE z_reservation SET ReservationStatus=@ReservationStatus, SoID=@SoID WHERE ReservationID=@ReservationID", CommandType.Text,
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int),
                        DbUtility.GetParameter("SoID", orderid, MyDbType.Int),
                        DbUtility.GetParameter("ReservationStatus", (int)status, MyDbType.Int)
                    });

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        } 
    }
}
