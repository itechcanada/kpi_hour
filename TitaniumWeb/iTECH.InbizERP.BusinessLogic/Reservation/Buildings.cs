﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Buildings
    {
        public int BuildingID { get; set; }
        public string BuildingTitleEn { get; set;}
        public string BuildingTitleFr { get; set; }
        public bool IsActive { get; set; }
        public int Seq { get; set; }

        public void Insert() {
            string sql = "INSERT INTO z_accommodation_buildings(BuildingTitleEn,BuildingTitleFr,IsActive,Seq)";
            sql += " VALUES(@BuildingTitleEn,@BuildingTitleFr,@IsActive,@Seq)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BuildingTitleEn", this.BuildingTitleEn, MyDbType.String),
                    DbUtility.GetParameter("BuildingTitleFr", this.BuildingTitleFr, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int)   
                });
                this.BuildingID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update() {
            string sql = "UPDATE z_accommodation_buildings SET BuildingTitleEn=@BuildingTitleEn, BuildingTitleFr=@BuildingTitleFr, IsActive=@IsActive";
            sql += " WHERE BuildingID=@BuildingID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BuildingTitleEn", this.BuildingTitleEn, MyDbType.String),
                    DbUtility.GetParameter("BuildingTitleFr", this.BuildingTitleFr, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),                    
                    DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int bid)
        {
            //string sql = "DELETE FROM z_accommodation_buildings WHERE BuildingID=@BuildingID";
            //Make inactive all rooms in building & beds in each room inside building

            List<string> sqlToExecute = new List<string>();
            sqlToExecute.Add("UPDATE products SET prdIsActive=0 WHERE prdExtendedCategory IN(SELECT RoomID FROM z_accommodation_rooms WHERE BuildingID=@BuildingID);");
            sqlToExecute.Add("UPDATE z_accommodation_rooms SET IsActive=0 WHERE BuildingID=@BuildingID;");
            sqlToExecute.Add("UPDATE z_accommodation_buildings SET IsActive=0 WHERE  BuildingID=@BuildingID;");
            //string sql = "UPDATE z_accommodation_rooms r, z_accommodation_buildings b, products p";
            //sql += " SET r.IsActive=0, b.IsActive=0, p.prdIsActive = 0";
            //sql += " WHERE b.BuildingID = r.BuildingID AND p.prdExtendedCategory=r.RoomID";
            //sql += " AND b.BuildingID=@BuildingID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                foreach (var item in sqlToExecute)
                {
                    dbHelp.ExecuteNonQuery(item, CommandType.Text, new MySqlParameter[] { 
                         DbUtility.GetParameter("BuildingID", bid, MyDbType.Int)
                    });
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int bid)
        {
            string sql = "SELECT * FROM z_accommodation_buildings WHERE BuildingID=@BuildingID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BuildingID", bid, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.BuildingID = BusinessUtility.GetInt(dr["BuildingID"]);
                    this.BuildingTitleEn = BusinessUtility.GetString(dr["BuildingTitleEn"]);
                    this.BuildingTitleFr = BusinessUtility.GetString(dr["BuildingTitleFr"]);
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.Seq = BusinessUtility.GetInt(dr["Seq"]);                    
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(ParameterCollection pCol, string searchText, string lang)
        {
            pCol.Clear();
            string sql = "SELECT z.BuildingID, " + Globals.GetFieldName("z.BuildingTitle", lang) + " AS BuildingTitle FROM z_accommodation_buildings z WHERE z.IsActive=1";            
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " AND (" + Globals.GetFieldName("z.BuildingTitle", lang) + " LIKE CONCAT(@SearchText, '%'))";
                pCol.Add("@SearchText", searchText);
            }
            sql += " ORDER BY Seq";
            return sql;
        }

        public DataTable GetAllBuildings(string lang)
        {
            string sql = "SELECT BuildingID, BuildingTitle" + lang + " AS BuildingTitle,IsActive FROM z_accommodation_buildings WHERE IsActive=1 ORDER BY Seq";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

