﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{    
    public class Bed
    {
        //Do not modify/delete/change sequece/change name of any following Property becauese it is used in same sequence in OrderInfo.aspx form during reservation        
        //ReservationItemID
        //BedID
        //BedTitle,
        //BuildingTitle,
        //Roomtitle
        //RoomType
        //FromDate
        //ToDate,
        //TotalDays,
        //BedPrice,
        //TotalBedPrice
        //GuestID
        //IsCouple
        //CartItemID
        //RelationshipToPrimaryGuest

        public int ReservationItemID { get; set; }

        public int BedID
        {
            get;
            set;
        }

        public string BedTitle { get; set; }

        public string BuildingTitle { get; set; }

        public string RoomTitle { get; set; }

        public int RoomType { get; set; }

        public DateTime FromDate
        {
            get;
            set;
        }

        public DateTime ToDate
        {
            get;
            set;
        }

        public int TotalDays {
            get
            {
                //return (ToDate.DayOfYear - FromDate.DayOfYear) + 1;
                //return (ToDate.DayOfYear - FromDate.DayOfYear);
                return Convert.ToInt32((this.ToDate.Date - this.FromDate.Date).TotalDays);
            }
        }        

        public double BedPrice { get; set; }

        public double TotalBedPrice
        {
            get
            {
                //return Math.Round((double)TotalDays * BedPrice);
                return (double)TotalDays * BedPrice;
            }
            //Follwoin will not work because accommodation has different price on each day
            //{                
            //    return (double)TotalDays * BedPrice;
            //}
        }

        public int GuestID { get; set; }

        public bool IsCouple { get; set; }

        public int CartItemID { get; set; }

        public string RelationshipToPrimaryGuest { get; set; }

        public string ErrorData { get; set; }

        public void PopulateBedInfo(DbHelper dbHelp, Bed bed, string lang)
        {
            string sql = "SELECT * FROM vw_beds WHERE BedID=@BedID";            
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("BedID", bed.BedID, MyDbType.Int) });
                if (dr.Read())
                {
                    bed.BedTitle = BusinessUtility.GetString(dr["BedTitle"]);
                    bed.BuildingTitle = BusinessUtility.GetString(dr["BuildingEn"]);
                    bed.RoomTitle = BusinessUtility.GetString(dr["RoomEn"]);
                    bed.BedPrice = BusinessUtility.GetDouble(dr["Price"]);
                    bed.RoomType = BusinessUtility.GetInt(dr["RoomType"]);                    
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr!=null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        //public void PopulateBedInfo(Bed bed, string lang)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        this.PopulateBedInfo(dbHelp, bed, lang);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public double GetBedPrice(DbHelper dbHelp, int bedID)
        {
            string sql = "SELECT Price FROM vw_beds WHERE BedID=@BedID";            
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("BedID", bedID, MyDbType.Int) });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }            
        }

        public List<Bed> GetListByReservationID(DbHelper dbHelp, int reservationID) {
            string sql = "SELECT ri.BedID, ri.PricePerDay AS Price, ri.CheckInDate, ri.CheckOutDate, ri.GuestID, ri.IsCouple, ri.RelationshipToPrimaryGuest, b.BedTitle, b.BuildingEn, b.RoomEn, b.RoomType, ri.ReservationItemID  FROM z_reservation_items ri INNER JOIN vw_beds b ON b.BedID=ri.BedID  WHERE ReservationID = @ReservationID";
            MySqlDataReader dr = null;
            List<Bed> lREsult = new List<Bed>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int)
                });
                while (dr.Read()) {
                    Bed b = new Bed();
                    b.BedID = BusinessUtility.GetInt(dr["BedID"]);
                    b.BedPrice = BusinessUtility.GetDouble(dr["Price"]);
                    b.FromDate = BusinessUtility.GetDateTime(dr["CheckInDate"]);
                    b.ToDate = BusinessUtility.GetDateTime(dr["CheckOutDate"]);

                    b.BedTitle = BusinessUtility.GetString(dr["BedTitle"]);
                    b.BuildingTitle = BusinessUtility.GetString(dr["BuildingEn"]);
                    b.RoomTitle = BusinessUtility.GetString(dr["RoomEn"]);
                    b.RoomType = BusinessUtility.GetInt(dr["RoomType"]);
                    b.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                    b.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                    b.RelationshipToPrimaryGuest = BusinessUtility.GetString(dr["RelationshipToPrimaryGuest"]);
                    b.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                    lREsult.Add(b);           
                }
                return lREsult;
            }
            catch
            {

                throw;
            }
            finally {
                if (dr != null && !dr.IsClosed) {
                    dr.Close();
                }
            }
        }

        public List<Bed> GetListByReservationID(int reservationID)
        {           
            DbHelper dbHelp = new DbHelper(true);
           
            try
            {
                return this.GetListByReservationID(dbHelp, reservationID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            //sb.Append("Building: ").Append(this.BuildingTitle).Append(Environment.NewLine)
            //    .Append("Room: ").Append(this.RoomTitle).Append(Environment.NewLine)
            //    .Append("Bed: ").Append(this.BedTitle).Append(Environment.NewLine)
            //    .Append("Check-In Date: ").AppendFormat("{0:MMM dd, yyyy}", this.FromDate).Append(Environment.NewLine)
            //    .Append("Check-Out Date: ").AppendFormat("{0:MMM dd, yyyy}", this.ToDate).Append(Environment.NewLine)
            //    .Append("Total Days: ").Append(this.TotalDays);
            sb.Append("Room Type: ").Append(Globals.GetRoomType(this.RoomType)).Append(Environment.NewLine)                
                .Append("Check-In Date: ").AppendFormat("{0:MMM dd, yyyy}", this.FromDate).Append(Environment.NewLine)
                .Append("Check-Out Date: ").AppendFormat("{0:MMM dd, yyyy}", this.ToDate).Append(Environment.NewLine)
                .Append("Total Days: ").Append(this.TotalDays);

            return sb.ToString();
        }

        public List<OptionList> GetBeds(int roomID, string lang)
        {
            string sql = "SELECT BedID, Bedtitle FROM vw_beds WHERE RoomID=@RoomID ORDER BY SeqBed";
            List<MySqlParameter> p = new List<MySqlParameter>();
            List<OptionList> lResult = new List<OptionList>();
            
            p.Add(DbUtility.GetParameter("RoomID", roomID, MyDbType.Int));
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, p.ToArray());
                while (dr.Read())
                {
                    OptionList ol = new OptionList();
                    ol.id = BusinessUtility.GetString(dr["BedID"]);
                    ol.Name = BusinessUtility.GetString(dr["Bedtitle"]);
                    lResult.Add(ol);
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        //public static List<Bed> ToList(DataTable dt)
        //{
        //    List<Bed> lResult = new List<Bed>();

        //    foreach (DataRow r in dt.Rows)
        //    {
        //        Bed b = new Bed();
        //        b.BedID = BusinessUtility.GetInt(r["BedID"]);
        //        b.BedPrice = BusinessUtility.GetDouble(r["BedPrice"]);
        //        b.BedTitle = BusinessUtility.GetString(r["BedTitle"]);
        //        b.BuildingTitle = BusinessUtility.GetString(r["BuildingTitle"]);
        //        b.CartItemID = BusinessUtility.GetInt(r["CartItemID"]);
        //        b.FromDate = BusinessUtility.GetDateTime(r["FromDate"]);
        //        b.GuestID = BusinessUtility.GetInt(r["GuestID"]);
        //        b.IsCouple = BusinessUtility.GetBool(r["IsCouple"]);
        //        b.RelationshipToPrimaryGuest = BusinessUtility.GetString(r["RelationshipToPrimaryGuest"]);
        //        b.ReservationItemID = BusinessUtility.GetInt(r["ReservationItemID"]);
        //        b.RoomTitle = BusinessUtility.GetString(r["RoomTitle"]);
        //        b.RoomType = BusinessUtility.GetInt(r["RoomType"]);
        //        b.ToDate = BusinessUtility.GetDateTime(r["ToDate"]);
        //        r["TotalDays"] = b.TotalDays;
        //        r["TotalBedPrice"] = b.TotalBedPrice;
        //        lResult.Add(b);
        //    }
        //    return lResult;
        //}

        public int GetBuildingID(DbHelper dbHelp, int bedID, out int roomID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT * FROM vw_beds WHERE BedID=@BedID";                        
            roomID = 0;
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BedID", bedID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        roomID = BusinessUtility.GetInt(dr["RoomID"]);
                        return BusinessUtility.GetInt(dr["BuildingID"]);
                    }
                }
                return 0;
            }
            catch
            {

                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
