﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic.MobileReservation
{
    public class MReservation
    {
        public bool MakeReservation(DbHelper dbHelp, string guestName, int noOfGuest, int bedID, DateTime checkInDate, DateTime checkOutDate, int partnerID, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                if ((partnerID <= 0) || (guestName == "") || (guestName == null) || (noOfGuest <= 0) || (bedID <= 0) || (checkInDate == null) || (checkOutDate == null))
                    return false;
                if (checkInDate == DateTime.MinValue)
                    return false;
                if (checkOutDate == DateTime.MinValue)
                    return false;
                if (checkInDate > checkOutDate)
                    return false;

                Partners cust = new Partners();
                cust.PopulateObject(partnerID);
                
                SysWarehouses whs = new SysWarehouses();
                whs.PopulateObject(ConfigurationManager.AppSettings["WebSaleWhsCode"]);

                SysCompanyInfo cinf = new SysCompanyInfo();
                cinf.PopulateObject(whs.WarehouseCompanyID);


                //SysCompanyInfo cinf = new SysCompanyInfo();
                //cinf.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);

                //Make Reservation 
                Reservation rsv = new Reservation();
                rsv.PrimaryPartnerID = partnerID;
                rsv.ReservationStatus = (int)StatusReservation.New;
                rsv.ReserveFor = cust.ExtendedProperties.GuestType;// 1->Guest, 2->Special Guest, 3->Staff, 4->Course Participants // (int)this.ReserveFor;
                rsv.ReservationNote = "Name:- " + guestName + Environment.NewLine + "No. Of Guest:- " + noOfGuest;// +Environment.NewLine + reservationNote;
                rsv.Insert(dbHelp, userID); //Crate Reservation

                RsvCartItem b = new RsvCartItem();
                b.PopulateBedInfo(dbHelp, bedID, checkInDate, checkOutDate, "en");

                List<ReservationItems> lstRsvItems = new List<ReservationItems>();
                //Reservation Item
                if (rsv.ReservationID > 0)
                {
                    ReservationItems ri = new ReservationItems();
                    ri.BedID = bedID;
                    ri.CheckInDate = checkInDate;
                    ri.CheckOutDate = checkOutDate;
                    ri.GuestID = partnerID;
                    ri.PricePerDay = b.ActualBedPrice;
                    ri.ReservationID = rsv.ReservationID;
                    lstRsvItems.Add(ri);

                    ReservationItems rItem = new ReservationItems();
                    rItem.AddList(null, lstRsvItems);

                    //Check beds avaialability for the last time before creating reservation
                    if (ProcessReservation.IsAccommodationAvailalbe(null, lstRsvItems) == false)
                        return false;

                    Orders ord = new Orders();

                    ord.OrdCompanyID = whs.WarehouseCompanyID;
                    ord.OrdCreatedBy = userID;
                    ord.OrdCreatedFromIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    ord.OrdCurrencyCode = cust.PartnerCurrencyCode;
                    ord.OrdCurrencyExRate = SysCurrencies.GetRelativePrice(ord.OrdCurrencyCode);
                    ord.OrdCustID = cust.PartnerID;
                    ord.OrdCustType = Globals.GetPartnerType(cust.PartnerType);
                    ord.OrderTypeCommission = (int)OrderCommission.Reservation;
                    ord.OrdLastUpdateBy = userID;
                    ord.OrdSalesRepID = userID;
                    ord.OrdSaleWeb = true;
                    ord.OrdShippingTerms = cinf.CompanyShpToTerms;
                    ord.OrdShpBlankPref = false;
                    ord.OrdShpCode = string.Empty;
                    ord.OrdShpCost = 0;
                    ord.OrdShpDate = DateTime.Now;
                    ord.OrdShpTrackNo = string.Empty;
                    ord.OrdShpWhsCode = whs.WarehouseCode;
                    ord.OrdStatus = SOStatus.IN_PROGRESS;
                    ord.OrdType = StatusSalesOrderType.QUOTATION;
                    ord.OrdVerified = true;
                    ord.OrdVerifiedBy = userID;
                    ord.QutExpDate = DateTime.Now;
                    ord.OrdNetTerms = string.Empty;
                    ord.OrderRejectReason = string.Empty;
                    ord.OrdComment = rsv.ReservationNote;
                    ord.Insert(null, userID); //Insert Order

                    if (ord.OrdID > 0)
                    {
                        OrderItems processItems = new OrderItems();
                        processItems.AddReservationItems(dbHelp, ord.OrdID, rsv.ReservationID);
                        rsv.UpdateReservationStatus(dbHelp, rsv.ReservationID, ord.OrdID, StatusReservation.Processed);
                    }
                    return true;
                }
                else return false;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public int GetUser(string loginid, string password)
        {
            string sqlvalidate = "SELECT userID FROM users WHERE userLoginId=@userLoginId AND userPassword=PASSWORD(@userPassword) AND userActive=1";
            DbHelper dbHelp = new DbHelper(true);
            int userid = -1;
            try
            {
                object scalar = dbHelp.GetValue(sqlvalidate, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("userLoginId", loginid, MyDbType.String), DbUtility.GetParameter("userPassword", password, MyDbType.String) });
                userid = BusinessUtility.GetInt(scalar);
                return userid;
            }
            catch { throw; }
            finally { dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> ReservationDetails(DbHelper dbHelp, DateTime checkInDate, DateTime checkOutDate)
        {
            List<object> lstResult = new List<object>();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @" SELECT DISTINCT ri.ReservationID,ri.ReservationItemID, v.BedID, v.BedTitle, v.BuildingID,  v.BuildingEn, v.BuildingFr,  v.RoomID,
                                               v.RoomEn ,v.RoomFr , v.SeqBuilding,v.SeqRoom, v.SeqBed,ce.LastName, ce.FirstName, ce.SpirtualName
                                FROM       vw_beds v
                                LEFT JOIN  z_reservation_items ri ON v.BedID = ri.BedID
                                      AND  ri.CheckInDate <= @currentDate AND ri.isCanceled =0 AND ri.CheckOutDate >= @currentDate
                                LEFT JOIN  z_customer_extended ce ON ce.PartnerID=ri.GuestID 
                               ORDER BY BedTitle,BedID";
            MySqlDataReader dr = null;
            try
            {
                int noOfDays = (checkOutDate - checkInDate).Days;

                DateTime currentDate = checkInDate;
                for (int i = 0; i <= noOfDays; i++)
                {                    
                    currentDate = checkInDate.AddDays(i);
                    dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("currentDate", currentDate, MyDbType.DateTime), });
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("ReservationItemID", BusinessUtility.GetString(dr["ReservationItemID"]));
                        dictResult.Add("BedID", BusinessUtility.GetString(dr["BedID"]));
                        dictResult.Add("BedTitle", BusinessUtility.GetString(dr["BedTitle"]));
                        dictResult.Add("BuildingID", BusinessUtility.GetString(dr["BuildingID"]));
                        dictResult.Add("BuildingEn", BusinessUtility.GetString(dr["BuildingEn"]));
                        dictResult.Add("BuildingFr", BusinessUtility.GetString(dr["BuildingFr"]));
                        dictResult.Add("RoomID", BusinessUtility.GetString(dr["RoomID"]));
                        dictResult.Add("RoomEn", BusinessUtility.GetString(dr["RoomEn"]));
                        dictResult.Add("RoomFr", BusinessUtility.GetString(dr["RoomFr"]));
                        dictResult.Add("SeqBuilding", BusinessUtility.GetString(dr["SeqBuilding"]));
                        dictResult.Add("SeqRoom", BusinessUtility.GetString(dr["SeqRoom"]));
                        dictResult.Add("SeqBed", BusinessUtility.GetString(dr["SeqBed"]));                        
                        dictResult.Add("LastName", BusinessUtility.GetString(dr["LastName"]));
                        dictResult.Add("FirstName", BusinessUtility.GetString(dr["FirstName"]));                        
                        dictResult.Add("ReservationID", BusinessUtility.GetString(dr["ReservationID"]));                       
                        dictResult.Add("Date", currentDate.ToString("yyyy-MM-dd"));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed)
                        dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally
            {
                if (!dr.IsClosed) dr.Close();
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        public List<object> ReservationDetails(DbHelper dbHelp, int reservationID)
        {
            List<object> lstResult = new List<object>();
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @"SELECT ri.CheckInDate ,ri.CheckOutDate,r.ReserveFor, r.SoID, ri.TotalNights, ri.GuestID, ri.IsCouple,
                                IF(IFNULL(ri.Sex, '') = '', ce.Sex, ri.Sex) AS Sex,
                                ce.LastName, ce.FirstName, ce.SpirtualName,ri.ReservationItemID,                              
                                r.ReservationID
                              FROM z_reservation_items ri
                              INNER JOIN z_reservation r ON  r.ReservationID=ri.ReservationID
                              INNER JOIN z_customer_extended ce ON ce.PartnerID=ri.GuestID 
                              INNER JOIN partners p ON p.PartnerID=ce.PartnerID AND p.PartnerActive=1                                                            
                              WHERE  r.ReservationID=@ReservationID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int) });
                while (dr.Read())
                {
                    Dictionary<object, object> dictResult = new Dictionary<object, object>();
                    dictResult.Add("ReserveFor", BusinessUtility.GetString(dr["ReserveFor"]));
                    dictResult.Add("TotalNights", BusinessUtility.GetString(dr["TotalNights"]));
                    dictResult.Add("IsCouple", BusinessUtility.GetString(dr["IsCouple"]));
                    dictResult.Add("Sex", BusinessUtility.GetString(dr["Sex"]));
                    dictResult.Add("LastName", BusinessUtility.GetString(dr["LastName"]));
                    dictResult.Add("FirstName", BusinessUtility.GetString(dr["FirstName"]));
                    dictResult.Add("SpirtualName", BusinessUtility.GetString(dr["SpirtualName"]));
                    dictResult.Add("ReservationItemID", BusinessUtility.GetString(dr["ReservationItemID"]));                    
                    dictResult.Add("ReservationID", BusinessUtility.GetString(dr["ReservationID"]));
                    dictResult.Add("CheckInDate", BusinessUtility.GetString(dr["CheckInDate"]));
                    dictResult.Add("CheckOutDate", BusinessUtility.GetString(dr["CheckOutDate"]));
                    lstResult.Add(dictResult);
                }
                return lstResult;
            }
            catch { throw; }
            finally
            {
                if (!dr.IsClosed) dr.Close();
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}