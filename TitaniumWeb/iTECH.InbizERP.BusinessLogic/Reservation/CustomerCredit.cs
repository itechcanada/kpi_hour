﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CustomerCredit
    {
        public int CustomerCreditID { get; set; }
        public int CustomerID { get; set; }
        public double CreditAmount { get; set; }
        public int CreditVia { get; set; }
        public int ReservationItemID { get; set; }
        public DateTime CreditedOn { get; set; }
        public int CreditedBy { get; set; }
        public string RefundType { get; set; }
        public string Notes { get; set; }
        public string WhsCode { get; set; }
        public string RegCode { get; set; }

        public void Insert(int userid)
        {
            string sql = "INSERT INTO z_customer_credit (CustomerID,CreditAmount,CreditVia,RefundType,ReservationItemID,Notes,CreditedOn,CreditedBy,WhsCode,RegCode)";
            sql += " VALUES(@CustomerID,@CreditAmount,@CreditVia,@RefundType,@ReservationItemID,@Notes,@CreditedOn,@CreditedBy,@WhsCode,@RegCode)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? rcvItemID = this.ReservationItemID > 0 ? (int?)this.ReservationItemID : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                    DbUtility.GetParameter("CreditAmount", this.CreditAmount, MyDbType.Double),
                    DbUtility.GetParameter("CreditVia", this.CreditVia, MyDbType.Int),
                    DbUtility.GetIntParameter("ReservationItemID", rcvItemID),
                    DbUtility.GetParameter("CreditedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("CreditedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("RefundType", this.RefundType, MyDbType.String),
                    DbUtility.GetParameter("Notes", this.Notes, MyDbType.String),
                    DbUtility.GetParameter("WhsCode", this.WhsCode, MyDbType.String),
                    DbUtility.GetParameter("RegCode", this.RegCode, MyDbType.String)
                });
                this.CustomerCreditID = dbHelp.GetLastInsertID();
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Insert(DbHelper dbHelp, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_customer_credit (CustomerID,CreditAmount,CreditVia,RefundType,ReservationItemID,Notes,CreditedOn,CreditedBy,WhsCode,RegCode)";
            sql += " VALUES(@CustomerID,@CreditAmount,@CreditVia,@RefundType,@ReservationItemID,@Notes,@CreditedOn,@CreditedBy,@WhsCode,@RegCode)";            
            try
            {
                int? rcvItemID = this.ReservationItemID > 0 ? (int?)this.ReservationItemID : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                    DbUtility.GetParameter("CreditAmount", this.CreditAmount, MyDbType.Double),
                    DbUtility.GetParameter("CreditVia", this.CreditVia, MyDbType.Int),
                    DbUtility.GetIntParameter("ReservationItemID", rcvItemID),
                    DbUtility.GetParameter("CreditedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("CreditedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("RefundType", this.RefundType, MyDbType.String),
                    DbUtility.GetParameter("Notes", this.Notes, MyDbType.String),
                    DbUtility.GetParameter("WhsCode", this.WhsCode, MyDbType.String),
                    DbUtility.GetParameter("RegCode", this.RegCode, MyDbType.String)
                });
                this.CustomerCreditID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " UPDATE z_customer_credit SET CreditAmount = @CreditAmount, CustomerCreditID = @CustomerCreditID WHERE CustomerCreditID = @CustomerCreditID ";
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerCreditID", this.CustomerCreditID, MyDbType.Int),
                    DbUtility.GetParameter("CreditAmount", this.CreditAmount, MyDbType.Double),
                    DbUtility.GetParameter("CustomerCreditID", DateTime.Now, MyDbType.DateTime),
                });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public int GetCrdtID(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = " SELECT CustomerCreditID FROM z_customer_credit WHERE OrderID = @OrderID AND CustomerID  = @CustomerID AND PaymentMethod = @PaymentMethod";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                    DbUtility.GetParameter("CreditVia", this.CreditVia, MyDbType.Int),
                    DbUtility.GetParameter("RefundType", this.RefundType, MyDbType.String)
                });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }




        public double GetCustomerCredit(int customerID)
        {
            string sql = "SELECT SUM(c.CreditAmount) FROM z_customer_credit c WHERE c.CustomerID=@CustomerID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object o = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", customerID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(o);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<CustomerCreditView> GetCustomerCreditByOrder(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<CustomerCreditView> lResult = new List<CustomerCreditView>();
            string sqlGetDistinct = "SELECT o.ordCustID FROM orders o WHERE o.ordID=@OrderID";
            string sqlGetDistinct2 = "SELECT DISTINCT o.ordGuestID FROM orderitems o WHERE o.ordID=@OrderID AND o.ordGuestID > 0";
            string sqlCredit = "SELECT c.CustomerID, SUM(c.CreditAmount) AS Amount, ce.LastName, ce.FirstName FROM  z_customer_credit c ";
            sqlCredit += " LEFT OUTER JOIN z_customer_extended ce ON ce.PartnerID=c.CustomerID ";
            sqlCredit += " WHERE c.RefundType='C' AND c.CustomerID IN({0})";
            sqlCredit += " GROUP BY c.CustomerID";            
            
            try
            { 
                List<int> lstCust = new List<int>();
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGetDistinct, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                }))
                {                   
                    while (dr.Read())
                    {
                        lstCust.Add(BusinessUtility.GetInt(dr[0]));
                    }
                }
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGetDistinct2, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        int id = BusinessUtility.GetInt(dr[0]);
                        if (!lstCust.Contains(id))
                        {
                            lstCust.Add(id);
                        }
                    }
                }
                
                if (lstCust.Count > 0)
                {
                    sqlCredit = string.Format(sqlCredit, StringUtil.GetJoinedString(",", lstCust.ToArray()));
                    using (MySqlDataReader dr = dbHelp.GetDataReader(sqlCredit, CommandType.Text, null))
                    {
                        while (dr.Read())
                        {
                            CustomerCreditView v = new CustomerCreditView();
                            v.CreditAmount = BusinessUtility.GetDouble(dr["Amount"]);
                            v.CustomerID = BusinessUtility.GetInt(dr["CustomerID"]);
                            v.CustomerName = string.Format("{0} {1}", dr["LastName"], dr["FirstName"]);
                            if (v.CreditAmount > 0)
                            {
                                lResult.Add(v);
                            }
                        }
                    }
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public double GetAvailableCredit(int partnerID)
        {
            string sql = "SELECT SUM(c.CreditAmount) AS Amount FROM  z_customer_credit c ";           
            sql += " WHERE c.CustomerID=@CustomerID AND RefundType='C'";
            DbHelper dbHelp = new DbHelper();
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("CustomerID", partnerID, MyDbType.Int)
                });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        } 
    }

    public class CustomerCreditView
    {
        public int CustomerCreditID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public double CreditAmount { get; set; }
        public int CreditVia { get; set; }
    }
}
