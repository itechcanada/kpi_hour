﻿//This code was built using Visual Studio 2005
using System;
using System.IO;
using System.Web.Services.Protocols;
using ShipWebServiceClient.ShipServiceWebReference;

// Sample code to call the FedEx Domestic Ground Shipping Web Service
// Tested with Microsoft Visual Studio 2005 Professional Edition

namespace ShipWebServiceClient
{
    public class FeedEx
    {
        public static int OrderID { get; set; }
        // Set FeedEx Credintial
        public static string FeedExLabelPath { get; set; }
        public static string FeedExKey { get; set; }
        public static string FeedExPwd { get; set; }
        public static string FeedExAccountNumber { get; set; }
        public static string FeedExMeterNumber { get; set; }
        // Set Shipper Detail
        public static string WhsName { get; set; }
        public static string WhsPhone { get; set; }
        public static string WhsAdd1 { get; set; }
        public static string WhsAdd2 { get; set; }
        public static string WhsCity { get; set; }
        public static string WhsState { get; set; }
        public static string WhsPostalCode { get; set; }
        public static string WhsCountryCode { get; set; }
        // Set Recipient Detail
        public static string ShpPersonName { get; set; }
        public static string ShpPersonPhone { get; set; }
        public static string ShpPersonAdd1 { get; set; }
        public static string ShpPersonAdd2 { get; set; }
        public static string ShpPersonCity { get; set; }
        public static string ShpPersonState { get; set; }
        public static string ShpPersonPostalCode { get; set; }
        public static string ShpPersonCountryCode { get; set; }
        // Set Payer Detail
        public static string PayorAccountNo { get; set; }
        public static string PayorCountryCode { get; set; }
        // Set Package Detail
        public static decimal PkgWeight { get; set; }
        public static string PkgLength { get; set; }
        public static string PkgWidth { get; set; }
        public static string PkgHeight { get; set; }
        // Set Collection Detail
        public static decimal CollectionAmount { get; set; }
        public static string CollectionCurrencyCode { get; set; }

        // Return Value 
        public static string PkgDtlPdfPath { get; set; }
        public static string PkgCmpltDtlPdfPath { get; set; }
        public static string Status { get; set; }
        public static string ErrDesc { get; set; }

        public static bool ShowPDFFile { get; set; }

        static void Main(string[] args)
        {
        }

        public static void FeedexLabel()
        {

            // Set this to true to process a COD shipment and print a COD return Label
            bool isCodShipment = true;
            ProcessShipmentRequest request = CreateShipmentRequest(isCodShipment);
            //
            ShipServiceWebReference.ShipService service = new ShipServiceWebReference.ShipService();
            if (usePropertyFile())
            {
                service.Url = getProperty("endpoint");
            }
            //
            try
            {
                // Call the ship web service passing in a ProcessShipmentRequest and returning a ProcessShipmentReply
                ProcessShipmentReply reply = service.processShipment(request);
                if (reply.HighestSeverity == NotificationSeverityType.SUCCESS || reply.HighestSeverity == NotificationSeverityType.NOTE || reply.HighestSeverity == NotificationSeverityType.WARNING)
                {
                    ShowShipmentReply(isCodShipment, reply);
                    Status = "0";
                    ErrDesc = "";
                }
                else
                {
                    string sErroMessage = "";
                    foreach (Notification notifiCation in reply.Notifications)
                    {
                        sErroMessage += notifiCation.Code + "-->" + notifiCation.LocalizedMessage + "</br>";
                    }
                    Status = "1";
                    ErrDesc = Convert.ToString(sErroMessage);
                }
            }
            catch (SoapException e)
            {
                //Console.WriteLine(e.Detail.InnerText);
                Status = "1";
                ErrDesc = e.Detail.InnerText;
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                Status = "1";
                ErrDesc = e.Message;
            }
            //Console.WriteLine("\nPress any key to quit !");
            //Console.ReadKey();
        }

        private static ProcessShipmentRequest CreateShipmentRequest(bool isCodShipment)
        {
            // Build the ShipmentRequest
            ProcessShipmentRequest request = new ProcessShipmentRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = FeedExKey;// "G33gsng7PVfQfvmc"; // PRABH  BRING THIS VALUE FROM Web.config 
            request.WebAuthenticationDetail.UserCredential.Password = FeedExPwd;// "VwENlu8XTSjapI9wPbxGKq3Am"; // PRABH  BRING THIS VALUE FROM Web.config 

            //
            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = FeedExAccountNumber; // "510087860"; // // PRABH  BRING THIS VALUE FROM Web.config 
            request.ClientDetail.MeterNumber = FeedExMeterNumber; //"118622093"; //// PRABH  BRING THIS VALUE FROM Web.config 

            //
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = Convert.ToString(OrderID); //"***PRABH PUT InBIZ ORDER-NO***"; // The client will get the same value back in the response
            //
            request.Version = new VersionId();
            //
            SetShipmentDetails(request);
            //
            SetSender(request);
            //
            SetRecipient(request);
            //
            SetPayment(request);
            //
            SetLabelDetails(request);
            //
            SetPackageLineItems(request, isCodShipment);
            //
            return request;
        }

        private static void SetShipmentDetails(ProcessShipmentRequest request)
        {
            request.RequestedShipment = new RequestedShipment();
            request.RequestedShipment.ShipTimestamp = DateTime.Now; // Ship date and time
            request.RequestedShipment.ServiceType = ServiceType.FEDEX_GROUND; // Service types are FEDEX_GROUND, GROUND_HOME_DELIVERY ...
            request.RequestedShipment.PackagingType = PackagingType.YOUR_PACKAGING; // Packaging type YOUR_PACKAGING, ...
            //
            request.RequestedShipment.RateRequestTypes = new RateRequestType[1] { RateRequestType.ACCOUNT }; // Rate types requested LIST, MULTIWEIGHT, ...
            request.RequestedShipment.PackageCount = "1";
            // set HAL
            bool isHALShipment = false;
            // if(isHALShipment)
            //    SetHAL(request);
        }

        private static void SetSender(ProcessShipmentRequest request)
        {
            //request.RequestedShipment.Shipper = new Party();
            //request.RequestedShipment.Shipper.Contact = new Contact();
            //request.RequestedShipment.Shipper.Contact.PersonName = "Shipping Department";
            //request.RequestedShipment.Shipper.Contact.CompanyName = WhsName; //PRABH PRIINT WAREHOUSE NAME
            //request.RequestedShipment.Shipper.Contact.PhoneNumber = WhsPhone;  // PRABH PRINT WAREHOUSE PHONE NO
            ////
            //request.RequestedShipment.Shipper.Address = new Address();
            //request.RequestedShipment.Shipper.Address.StreetLines = new string[2] { "1202 Chalet Ln", "Do Not Delete - Test Account" };  // PRABH PRINT WAREHOUSE PHONE NO
            //request.RequestedShipment.Shipper.Address.City = "Harrison"; // PRABH PRINT WAREHOUSE PHONE NO
            //request.RequestedShipment.Shipper.Address.StateOrProvinceCode = "AR"; // PRABH PRINT WAREHOUSE PHONE NO
            //request.RequestedShipment.Shipper.Address.PostalCode = "72601"; // PRABH PRINT WAREHOUSE PHONE NO
            //request.RequestedShipment.Shipper.Address.CountryCode = "US"; // PRABH PRINT WAREHOUSE PHONE NO
            request.RequestedShipment.Shipper = new Party();
            request.RequestedShipment.Shipper.Contact = new Contact();
            request.RequestedShipment.Shipper.Contact.PersonName = "Shipping Department";
            request.RequestedShipment.Shipper.Contact.CompanyName = WhsName; //"Cava De Soi Inc."; //PRABH PRIINT WAREHOUSE NAME
            request.RequestedShipment.Shipper.Contact.PhoneNumber = WhsPhone; //"514-555-5555";  // PRABH PRINT WAREHOUSE PHONE NO
            //
            request.RequestedShipment.Shipper.Address = new Address();
            request.RequestedShipment.Shipper.Address.StreetLines = new string[2] { WhsAdd1, WhsAdd2 }; //new string[2] { "1202 Chalet Ln", "Do Not Delete - Test Account" };  // PRABH PRINT WAREHOUSE PHONE NO
            request.RequestedShipment.Shipper.Address.City = WhsCity;//"Harrison"; // PRABH PRINT WAREHOUSE PHONE NO
            request.RequestedShipment.Shipper.Address.StateOrProvinceCode = WhsState;// "AR"; // PRABH PRINT WAREHOUSE PHONE NO
            request.RequestedShipment.Shipper.Address.PostalCode = WhsPostalCode;// "72601"; // PRABH PRINT WAREHOUSE PHONE NO
            request.RequestedShipment.Shipper.Address.CountryCode = WhsCountryCode;// "US"; // PRABH PRINT WAREHOUSE PHONE NO
        }

        private static void SetRecipient(ProcessShipmentRequest request)
        {
            request.RequestedShipment.Recipient = new Party();
            request.RequestedShipment.Recipient.Contact = new Contact();
            request.RequestedShipment.Recipient.Contact.PersonName = ShpPersonName;//"Ram Avatar";  //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            //request.RequestedShipment.Recipient.Contact.CompanyName = "Recipient Company Name";
            request.RequestedShipment.Recipient.Contact.PhoneNumber = ShpPersonPhone;// "9012637906"; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            //
            request.RequestedShipment.Recipient.Address = new Address();
            request.RequestedShipment.Recipient.Address.StreetLines = new string[2] { ShpPersonAdd1, ShpPersonAdd2 }; //new string[2] { "123 Street" }; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            request.RequestedShipment.Recipient.Address.City = ShpPersonCity;//"Austin"; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            request.RequestedShipment.Recipient.Address.StateOrProvinceCode = ShpPersonState;//"TX"; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            request.RequestedShipment.Recipient.Address.PostalCode = ShpPersonPostalCode;//"73301"; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
            request.RequestedShipment.Recipient.Address.CountryCode = ShpPersonCountryCode;//"US"; //PRABH USE CUSTOMER NAME AND SHIPPING ADDRESS
        }

        private static void SetPayment(ProcessShipmentRequest request)
        {
            request.RequestedShipment.ShippingChargesPayment = new Payment();
            request.RequestedShipment.ShippingChargesPayment.PaymentType = PaymentType.SENDER;
            request.RequestedShipment.ShippingChargesPayment.Payor = new Payor();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty = new Party();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = PayorAccountNo; //"510087860"; // // PRABH  BRING THIS VALUE FROM Web.config 
            if (usePropertyFile()) //Set values from a file for testing purposes
            {
                request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = getProperty("payoraccount");
            }
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact = new Contact();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address = new Address();
            request.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.CountryCode = PayorCountryCode;//"US"; //PRABH USE WAREHOUSE COUNTRY CODE
        }

        private static void SetLabelDetails(ProcessShipmentRequest request)
        {
            request.RequestedShipment.LabelSpecification = new LabelSpecification();
            request.RequestedShipment.LabelSpecification.ImageType = ShippingDocumentImageType.PDF; // Image types PDF, PNG, DPL, ...
            request.RequestedShipment.LabelSpecification.ImageTypeSpecified = true;
            request.RequestedShipment.LabelSpecification.LabelFormatType = LabelFormatType.COMMON2D;
        }

        private static void SetPackageLineItems(ProcessShipmentRequest request, bool isCodShipment)
        {
            request.RequestedShipment.RequestedPackageLineItems = new RequestedPackageLineItem[1];
            request.RequestedShipment.RequestedPackageLineItems[0] = new RequestedPackageLineItem();
            request.RequestedShipment.RequestedPackageLineItems[0].SequenceNumber = "1";
            // Package weight information
            request.RequestedShipment.RequestedPackageLineItems[0].Weight = new Weight();
            request.RequestedShipment.RequestedPackageLineItems[0].Weight.Value = PkgWeight; //50.0M;   // PRABH  BRING THIS VALUE FROM Web.config 
            request.RequestedShipment.RequestedPackageLineItems[0].Weight.Units = WeightUnits.LB;
            //
            request.RequestedShipment.RequestedPackageLineItems[0].Dimensions = new Dimensions();
            request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Length = PkgLength;//"12"; // PRABH  BRING THIS VALUE FROM Web.config 
            request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Width = PkgWidth;//"13"; // PRABH  BRING THIS VALUE FROM Web.config 
            request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Height = PkgHeight;// "14"; // PRABH  BRING THIS VALUE FROM Web.config 
            request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Units = LinearUnits.IN;
            // Reference details
            //  request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences = new CustomerReference[3] { new CustomerReference(), new CustomerReference(), new CustomerReference() };
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences = new CustomerReference[1] { new CustomerReference() };
            //request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].CustomerReferenceType = CustomerReferenceType.CUSTOMER_REFERENCE;
            //request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].Value = "GR4567892";
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].CustomerReferenceType = CustomerReferenceType.INVOICE_NUMBER;
            request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[0].Value = "SO-" + Convert.ToString(OrderID);//"INV4567892"; // PRABH  Use Order No ... SO-12321321
            //request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[2].CustomerReferenceType = CustomerReferenceType.P_O_NUMBER;
            //request.RequestedShipment.RequestedPackageLineItems[0].CustomerReferences[2].Value = "PO4567892";
            //
            if (isCodShipment)
            {
                SetCOD(request);
            }
        }

        //private static void SetHAL(ProcessShipmentRequest request)
        //{
        //    request.RequestedShipment.SpecialServicesRequested = new ShipmentSpecialServicesRequested();
        //    request.RequestedShipment.SpecialServicesRequested.SpecialServiceTypes = new ShipmentSpecialServiceType[1];
        //    request.RequestedShipment.SpecialServicesRequested.SpecialServiceTypes[0] = ShipmentSpecialServiceType.HOLD_AT_LOCATION;
        //    //
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail = new HoldAtLocationDetail();
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.PhoneNumber = "9011234567";
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress = new ContactAndAddress();
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Contact = new Contact();
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Contact.PersonName = "Tester";
        //    //
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address = new Address();
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.StreetLines = new string[1];
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.StreetLines[0] = "45 Noblestown Road";
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.City = "Pittsburgh";
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.StateOrProvinceCode = "PA";
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.PostalCode = "15220";
        //    request.RequestedShipment.SpecialServicesRequested.HoldAtLocationDetail.LocationContactAndAddress.Address.CountryCode = "US";
        //}

        private static void SetCOD(ProcessShipmentRequest request)
        {
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested = new PackageSpecialServicesRequested();
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.SpecialServiceTypes = new PackageSpecialServiceType[1];
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.SpecialServiceTypes[0] = PackageSpecialServiceType.COD;
            //
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.CodDetail = new CodDetail();
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.CodDetail.CollectionType = CodCollectionType.GUARANTEED_FUNDS;
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.CodDetail.CodCollectionAmount = new Money();
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.CodDetail.CodCollectionAmount.Amount = CollectionAmount;//250.00M;  // PRABH GET FROM WEB.CONFIG
            request.RequestedShipment.RequestedPackageLineItems[0].SpecialServicesRequested.CodDetail.CodCollectionAmount.Currency = CollectionCurrencyCode;// "USD"; // PRABH GET FROM WEB.CONFIG
        }

        private static void ShowShipmentReply(bool isCodShipment, ProcessShipmentReply reply)
        {
            //Console.WriteLine("Shipment Reply details:");
            //Console.WriteLine("Package details\n");
            // Details for each package
            foreach (CompletedPackageDetail packageDetail in reply.CompletedShipmentDetail.CompletedPackageDetails)
            {
                ShowTrackingDetails(packageDetail.TrackingIds);
                ShowPackageRateDetails(packageDetail.PackageRating.PackageRateDetails);
                ShowBarcodeDetails(packageDetail.OperationalDetail.Barcodes);
                ShowShipmentLabels(isCodShipment, reply.CompletedShipmentDetail, packageDetail);
            }
            ShowPackageRouteDetails(reply.CompletedShipmentDetail.OperationalDetail);
        }

        private static void ShowShipmentLabels(bool isCodShipment, CompletedShipmentDetail completedShipmentDetail, CompletedPackageDetail packageDetail)
        {
            if (null != packageDetail.Label.Parts[0].Image)
            {
                // Save outbound shipping label
                string sFileName = (packageDetail.TrackingIds[0].TrackingNumber + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf").Replace(@"\\", @"\").Replace("-", "_").Replace(":", "_").Replace(" ", "_");
                string LabelFileName = (FeedExLabelPath + sFileName).Replace(@"\\", @"\");  // PRABH GET FROM WEB.CONFIG
                SaveLabel(LabelFileName, packageDetail.Label.Parts[0].Image);
                PkgDtlPdfPath = LabelFileName;
                if (isCodShipment)
                {
                    // Save COD label
                    sFileName = (completedShipmentDetail.CompletedPackageDetails[0].TrackingIds[0].TrackingNumber + DateTime.Now.ToString("yyyyMMddHHmmss") + "CR.pdf").Replace(@"\\", @"\").Replace("-", "_").Replace(":", "_").Replace(" ", "_");
                    LabelFileName = (FeedExLabelPath + sFileName).Replace(@"\\", @"\");
                    SaveLabel(LabelFileName, completedShipmentDetail.CompletedPackageDetails[0].CodReturnDetail.Label.Parts[0].Image);
                    PkgCmpltDtlPdfPath = LabelFileName;
                }
            }
        }

        private static void ShowTrackingDetails(TrackingId[] TrackingIds)
        {
            // Tracking information for each package
            //Console.WriteLine("Tracking details");
            if (TrackingIds != null)
            {
                for (int i = 0; i < TrackingIds.Length; i++)
                {
                    //Console.WriteLine("Tracking # {0} Form ID {1}", TrackingIds[i].TrackingNumber, TrackingIds[i].FormId);
                }
            }
        }

        private static void ShowPackageRateDetails(PackageRateDetail[] PackageRateDetails)
        {
            foreach (PackageRateDetail ratedPackage in PackageRateDetails)
            {
                //Console.WriteLine("\nRate details");
                //if (ratedPackage.BillingWeight != null)
                //    Console.WriteLine("Billing weight {0} {1}", ratedPackage.BillingWeight.Value, ratedPackage.BillingWeight.Units);
                //if (ratedPackage.BaseCharge != null)
                //    Console.WriteLine("Base charge {0} {1}", ratedPackage.BaseCharge.Amount, ratedPackage.BaseCharge.Currency);
                //if (ratedPackage.TotalSurcharges != null)
                //    Console.WriteLine("Total surcharge {0} {1}", ratedPackage.TotalSurcharges.Amount, ratedPackage.TotalSurcharges.Currency);
                //if (ratedPackage.Surcharges != null)
                //{
                //    // Individual surcharge for each package
                //    foreach (Surcharge surcharge in ratedPackage.Surcharges)
                //        Console.WriteLine(" {0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency);
                //}
                //if (ratedPackage.NetCharge != null)
                //    Console.WriteLine("Net charge {0} {1}", ratedPackage.NetCharge.Amount, ratedPackage.NetCharge.Currency);
            }
        }

        private static void ShowBarcodeDetails(PackageBarcodes barcodes)
        {
            // Barcode information for each package
            //Console.WriteLine("\nBarcode details");
            if (barcodes != null)
            {
                if (barcodes.StringBarcodes != null)
                {
                    for (int i = 0; i < barcodes.StringBarcodes.Length; i++)
                    {
                        //Console.WriteLine("String barcode {0} Type {1}", barcodes.StringBarcodes[i].Value, barcodes.StringBarcodes[i].Type);
                    }
                }

                if (barcodes.BinaryBarcodes != null)
                {
                    for (int i = 0; i < barcodes.BinaryBarcodes.Length; i++)
                    {
                        //Console.WriteLine("Binary barcode Type {0}", barcodes.BinaryBarcodes[i].Type);
                    }
                }
            }
        }

        private static void ShowPackageRouteDetails(ShipmentOperationalDetail routingDetail)
        {
            //Console.WriteLine("\nRouting details");
            //Console.WriteLine("URSA prefix {0} suffix {1}", routingDetail.UrsaPrefixCode, routingDetail.UrsaSuffixCode);
            //Console.WriteLine("Service commitment {0} Airport ID {1}", routingDetail.DestinationLocationId, routingDetail.AirportId);

            //if (routingDetail.DeliveryDaySpecified)
            //{
            //    Console.WriteLine("Delivery day " + routingDetail.DeliveryDay);
            //}
            //if (routingDetail.DeliveryDateSpecified)
            //{
            //    Console.WriteLine("Delivery date " + routingDetail.DeliveryDate.ToShortDateString());
            //}
            //if (routingDetail.TransitTimeSpecified)
            //{
            //    Console.WriteLine("Transit time " + routingDetail.TransitTime);
            //}
        }

        private static void SaveLabel(string labelFileName, byte[] labelBuffer)
        {
            // Save label buffer to file
            FileStream LabelFile = new FileStream(labelFileName, FileMode.Create);
            LabelFile.Write(labelBuffer, 0, labelBuffer.Length);
            LabelFile.Close();

            // Display label in Acrobat
            if (ShowPDFFile == true)
            {
                DisplayLabel(labelFileName);
            }
        }

        private static void DisplayLabel(string labelFileName)
        {
            System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(labelFileName);
            info.UseShellExecute = true;
            info.Verb = "open";
            System.Diagnostics.Process.Start(info);
        }

        private static void ShowNotifications(ProcessShipmentReply reply)
        {
            //Console.WriteLine("Notifications");
            //for (int i = 0; i < reply.Notifications.Length; i++)
            //{
            //    Notification notification = reply.Notifications[i];
            //    Console.WriteLine("Notification no. {0}", i);
            //    Console.WriteLine(" Severity: {0}", notification.Severity);
            //    Console.WriteLine(" Code: {0}", notification.Code);
            //    Console.WriteLine(" Message: {0}", notification.Message);
            //    Console.WriteLine(" Source: {0}", notification.Source);
            //}
        }
        private static bool usePropertyFile() //Set to true for common properties to be set with getProperty function.
        {
            return getProperty("usefile").Equals("True");
        }
        private static String getProperty(String propertyname) //Sets common properties for testing purposes.
        {
            try
            {
                String filename = "C:\\filepath\\filename.txt";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(filename);
                    do
                    {
                        String[] parts = sr.ReadLine().Split(',');
                        if (parts[0].Equals(propertyname) && parts.Length == 2)
                        {
                            return parts[1];
                        }
                    }
                    while (!sr.EndOfStream);
                }
                //Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
            catch (Exception e)
            {
                //Console.WriteLine("Property {0} set to default 'XXX'", propertyname);
                return "XXX";
            }
        }
    }
}